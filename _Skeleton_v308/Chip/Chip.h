// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: Chip.h
// 	   Version: 3.0
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: MCU Chip selection file
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
// 

#ifndef __CHIP_H_
#define __CHIP_H_

#include "NXP/_chip_nxp.h"
//#include "STM/_chip_stm.h"


#ifndef LOAD_SCATTER
//typedef enum {ERROR = false, SUCCESS = !ERROR} Status;							// Status type definition

typedef void (*isr_t)(void);
typedef struct 
{
    const uint32_t Initial_SP;                          							// exception stack pointer - same for CORTEX-M MCUs
    const isr_t vector_cortexm[CPU_NONISR_EXCEPTIONS];   							// extern Cortex-M vectors - depends on selected MCU
	const isr_t vector_spec[CPU_IRQ_NUMOF];
} cortexm_base_t;


typedef void * (*foo)(int*);
typedef void (*bar)(int*);


// ******************************************************************************************************
// PUBLIC Functions - harmonized
// ******************************************************************************************************
extern void 	_Chip_SystemInit(void);												// public System Init subrutina for concrete chip

extern bool _IAP_Rd_ChipID(uint32_t *dst);
extern bool _IAP_Rd_SerialNum(uint32_t *dst);
extern bool _IAP_Rd_BootCodeVersion(uint32_t *dst);


// GPIO:
extern void 	_GPIO_Init 			( uint32_t Port, uint32_t Pin);
extern void 	_GPIO_DeInit 		( uint32_t Port, uint32_t Pin);
extern bool 	_GPIO_Rd_Pin 		( uint32_t Port, uint32_t Pin);
extern void 	_GPIO_Clr_Pin 		( uint32_t Port, uint32_t Pin);
extern void 	_GPIO_Set_Pin 		( uint32_t Port, uint32_t Pin);
extern void 	_GPIO_Wr_Pin 		( uint32_t Port, uint32_t Pin, uint32_t Val);
extern void 	_GPIO_Toggle_Pin 	( uint32_t Port, uint32_t Pin);
extern void 	_GPIO_Conf_Pin 		( uint32_t Port, uint32_t Pin, uint8_t NumOfVal, ...);
//extern void 	_Chip_GPIO_Wr_Conf_Pin (uint32_t Port, uint32_t Pin, uint8_t Reg_Idx, uint8_t *Reg_Val);// write value Reg_Val into register Reg_Idx

// .. and add your own lib. functions
#endif 	// LOAD_SCATTER
#endif	//__CHIP_H_
