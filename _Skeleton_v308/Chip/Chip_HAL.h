// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: Chip_HAL.h
// 	   Version: 5.11
//		  Desc:  ARM Chip HAL basic library
// ******************************************************************************
// Changelog:
//				2024.09.04	- v5.11 - add CHAL_UART_Get_Clock_Rate function
//							- add void CHAL_ADC_Get_Data_CH function
//				2024.01.11	- v5.10 - rename ADC channels oriented functions
//				2022.11.29	- v 5.00 - clean code and fix .h and .c content
//				2021.06.18	- Extend SPI Configure
//				2021.06.15	- Add SPI Set_Bus_Speed
//				2020.10.01	- aded Get_Address function
//				2020.08.18	- moved CHAL_I2C_Xfer_t to drv_I2C.h as I2C_Xfer_t
//				2020.08.04	- I2C access divided to master, slave, monitor and timeouts
//				2020.07.15	- Added DAC section
//				2020.07.15	- Added UART FLUSH commands: CHAL_UART_Flush_.x
//				2020.07.14	- Add FIFO TH level control - CHAL_UARTFIFO_.et_.xTHLevel
//				2020.07.08	- rename Old: CHAL_UART_IRQRX_Enable ---->  CHAL_UART_IRQ_Peri_Enable
//							- rename Old: CHAL_UART_IRQTX_Enable ---->  CHAL_UART_IRQ_Peri_Enable , add IRQBitMask parameter, and define CHAL INTSTAT flags
//				2020.07.01	- rename Old: _Chip_xxx_IRQ_Enable  ----->  _Chip_xxx_Enable_IRQ_NVIC
//							- rename Old: CHAL_xxx_IRQ_Enable   ----->  CHAL_xxx_Enable_IRQ_NVIC
//							- rename Old: _Chip_xxx_IRQMask_Enable --> 	_Chip_xxx_IRQ_Peri_Enable
//							- rename Old: _Chip_xxx_IRQ_Enable_CHIRQ --> _Chip_xxx_IRQ_Peri_Enable  also parameters renamed
//							- rename Old: CHAL_I2C_Get_Status -------> CHAL_I2C_Get_Peri_Status
//				2020.06.21	- added UART FIFO IRQ 
//							- v3.0: upgrade to v3				
//				2019.06.11	- Optimalization

#ifndef __CHIP_HAL_H_
#define __CHIP_HAL_H_

#include <stdbool.h>
#include <stdint.h>
#include "Skeleton.h"


// ******************************************************************************************************
// Default CONFIGURATION
// ******************************************************************************************************
#ifndef CONF_DEBUG_SUBSYS_CHAL
#define CONF_DEBUG_SUBSYS_CHAL			0											// Default debug is off - use only if CONF_DEBUG_SUBSYS is 1 !!
#endif

// ******************************************************************************************************
// PUBLIC Enum and constants
// ******************************************************************************************************

typedef enum CHAL_RST_SRC															// Reset Sources
{
	CHAL_RST_NOT = (uint32_t) 0,
	CHAL_RST_POR = 1,																// Power On Reset
	CHAL_RST_EXT = 2,																// external Reset signal asserted
	CHAL_RST_WDT = 4,																// WatchDog Reset
	CHAL_RST_BOD = 8,																// Brown-out reset
	CHAL_RST_SYS = 16,																// System Reset
	CHAL_RST_LOCKUP = 32															// Lockup Reset
} CHAL_RST_Src_t;

typedef enum CHAL_SIG_IRQ_SENS														// gpio interrupt request sesitivity
{
	CHAL_SIG_IRQ_LEVEL_LOW = 0x00,													// low level - 0
	CHAL_SIG_IRQ_EDGE_RISE = 0x01,													// 0->1 edge
	CHAL_SIG_IRQ_EDGE_RISEORFALL = 0x22,											// 0->1 or 1->0 edges
	CHAL_SIG_IRQ_EDGE_FALL = 0x10,													// 1->0 edge
	CHAL_SIG_IRQ_LEVEL_HIGH = 0x11,													// high level - 1

	CHAL_SIG_IRQ_LEVEL_INACT = 0xA0,												// inactive level (depends on Activity settings)
	CHAL_SIG_IRQ_LEVEL_ACT	= 0xFF,													// active level (depends on Activity settings)
	CHAL_SIG_IRQ_EDGE_TOACT = 0x0F,													// inactive -> active edge (depends on Activity settings)
	CHAL_SIG_IRQ_EDGE_TOINACT = 0xF0,												// active -> inactive edge (depends on Activity settings)
	CHAL_SIG_IRQ_EDGE_ACTORINACT = 0xAA												// active -> inactive edge (depends on Activity settings)
} CHAL_SIG_IRQ_SENS_t;

typedef enum Chip_I2C_Mode															// I2C definitions - possible states
{
	CHAL_I2C_Master,
	CHAL_I2C_Slave,
	CHAL_I2C_Monitor
} CHAL_I2C_Mode_t;

typedef enum 																		// Timer definitions - possible states
{
	CHAL_Timer_Stop,																// Frequency config but timer is stopped also after enable command.
 	CHAL_Timer_OneShot,
	CHAL_Timer_Repeat,
	CHAL_Timer_PWMReload,															// timer as mian reload 
	CHAL_Timer_PWMOut,																// timer as duty cycle for main reload with Out state change
} CHAL_Timer_Mode_t;

typedef enum Chip_IO_Mode															// Pin I/O definitions - possible states
{
	CHAL_IO_Input,
	CHAL_IO_Output
} CHAL_IO_Mode_t;

typedef enum CHAL_USB_Mode															// working mode for USB peri.
{
	CHAL_USB_NONE,
	CHAL_USB_DEVICE,
	CHAL_USB_HOST,
	CHAL_USB_OTG
} CHAL_USB_Mode_t;

typedef enum Chip_SPI_XferMode														// SPI Peripherial transfer mode selection
{
	CHAL_SPI_Master,
	CHAL_SPI_Slave
} CHAL_SPI_XferMode_t;


COMP_PACKED_BEGIN
typedef struct																		// Standard IO Definition of the signal name (connected to pin)
{
	uint8_t								Port;										// cislo portu
	uint8_t								Pin;										// cislo pinu
	bool								Act;										// active in...
}CHAL_STD_Signal_t;
COMP_PACKED_END


//struct _Chip_IO_Spec_t;															// dummy, will be defined in chip_XXXzzz.h file

COMP_PACKED_BEGIN
typedef struct																		// Extended IO Definition of the signal name (connected to pin)
{
	CHAL_IO_Mode_t						Dir;										// Direction input/output - myslene hned po iniciallizacii, default stav. Definovane v _chip_xxx.h
	_Chip_IO_Spec_t						Conf;										// additional configuration - chip specific. defined in _chip_XXX.h
}CHAL_EXT_Signal_t;
COMP_PACKED_END


COMP_PACKED_BEGIN
typedef struct																		// Pin Signal IO Definition of the signal name (connected to pin)
{
	CHAL_STD_Signal_t					Std;										// Standard config
	CHAL_EXT_Signal_t					Ext;										// Extended config
}CHAL_Signal_t;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct																		// Pin Signal IO Definition of the signal name (connected to pin)
{
	uint8_t 							ADC_Count;									// Number od ADCs
	uint32_t							Channel_Mask;								// Channel mask of active analog inputs for each ADC
}CHAL_ADC_t;
COMP_PACKED_END



//extern void 							CHAL_Wr_OscRateIn(uint32_t newValue);
//extern void 							CHAL_Wr_RTCOscRateIn(uint32_t newValue);

#if (CONF_DEBUG_ITM == 1) && (__CORTEX_M >= 3)										// debug with ITM support
extern const uint32_t					CHAL_ITM_RxBuffer_Empty;
#endif


// ******************************************************************************************************
// CHIP functions
// ******************************************************************************************************
extern void 	CHAL_Chip_Reset(void);
extern void 	CHAL_Chip_SystemInit(void);
extern void 	CHAL_Chip_SWO_Init(uint32_t Main_CPU_Clock);
extern void 	CHAL_Chip_Read_ResetSource(CHAL_RST_Src_t* DstStruct);
extern bool 	CHAL_Chip_IAP_Read_ID(uint32_t *pDst);
extern bool 	CHAL_Chip_IAP_Read_SerialNum(uint32_t *pDst);
extern bool 	CHAL_Chip_IAP_Read_BootCodeVersion(uint32_t *pDst);







// ******************************************************************************************************
// CLOCK functions
// ******************************************************************************************************
extern void CHAL_Chip_Update_SystemCoreClock(void);
extern uint32_t CHAL_Chip_Read_SrcFreq(void);
extern uint32_t CHAL_Chip_Read_CoreFreq(void);
extern uint32_t CHAL_Chip_Read_RTCFreq(void);
extern uint32_t CHAL_Chip_Read_TickPerUS(void);
extern uint32_t CHAL_Chip_Read_USBFreq(void);
extern uint32_t CHAL_Chip_Read_SPIFIFreq(void);
extern uint32_t CHAL_Chip_Read_EMCFreq(void);
extern uint32_t CHAL_Chip_Read_CLKOUTFreq(void);




// ******************************************************************************************************
// GPIO Functions
// ******************************************************************************************************
extern bool 	CHAL_GPIO_Init ( uint8_t PeriIndex, uint32_t Pin);
extern bool 	CHAL_GPIO_DeInit ( uint8_t PeriIndex, uint32_t Pin);
extern bool 	CHAL_GPIO_Read_Pin ( uint8_t PeriIndex, uint32_t Pin);
extern void 	CHAL_GPIO_Clear_Pin ( uint8_t PeriIndex, uint32_t Pin);
extern void 	CHAL_GPIO_Set_Pin ( uint8_t PeriIndex, uint32_t Pin);
extern void 	CHAL_GPIO_Write_Pin ( uint8_t PeriIndex, uint32_t Pin, uint32_t Val);
extern void 	CHAL_GPIO_Toggle_Pin ( uint8_t PeriIndex, uint32_t Pin);
extern void 	CHAL_GPIO_Write_Direction( uint8_t PeriIndex, uint32_t Pin, CHAL_IO_Mode_t Dir);





// ******************************************************************************************************
// SIGNAL  Functions
// ******************************************************************************************************
extern bool 	CHAL_Signal_PINT_Enable_IRQ_NVIC(void *pPeri, uint32_t Port, uint32_t Pin, bool NewState);
extern bool 	CHAL_Signal_Read_Pin ( CHAL_STD_Signal_t Inp);
extern void 	CHAL_Signal_Set_Pin ( CHAL_STD_Signal_t Inp);
extern void 	CHAL_Signal_Clear_Pin ( CHAL_STD_Signal_t Inp);
extern void 	CHAL_Signal_Write_Pin ( CHAL_STD_Signal_t Inp, bool Val);
extern void 	CHAL_Signal_Toggle_Pin ( CHAL_STD_Signal_t Inp);
extern void 	CHAL_Signal_Write_Direction(CHAL_STD_Signal_t Inp, CHAL_IO_Mode_t Dir);
extern bool 	CHAL_Signal_Init (const CHAL_Signal_t *Inp);
extern void 	CHAL_Signal_DeInit ( const CHAL_Signal_t *Inp, void* pNeutralConf);
extern void*	CHAL_Signal_Set_GINT(const CHAL_Signal_t *Inp, uint8_t Group, CHAL_SIG_IRQ_SENS_t Sens);
extern void*	CHAL_Signal_Set_PINT(const CHAL_Signal_t *Inp, uint8_t PINTSel, CHAL_SIG_IRQ_SENS_t Sens);










// ******************************************************************************************************
// I2C Functions
// ******************************************************************************************************

typedef enum
{
	CHAL_I2C_STAT_OK					= 0x4000,
	CHAL_I2C_STAT_ERROR					= 0x8000,
	CHAL_I2C_STAT_BUSY					= 0x0001,									// in progress. wait for ~BUSY
	CHAL_I2C_STAT_BUFFFULL				= 0x0002,
	CHAL_I2C_STAT_BUFFNONE				= 0x0004,
	CHAL_I2C_STAT_NAK_ADR				= 0x0008,
	CHAL_I2C_STAT_NAK_DAT				= 0x0010,
	CHAL_I2C_STAT_ARBITR_LOSS			= 0x0020,
	CHAL_I2C_STAT_TIMEOUT				= 0x0040,
	CHAL_I2C_STAT_SCLLONG				= 0x0080,
	CHAL_I2C_STAT_STSTERR				= 0x0100,									// Start Stop error - A Start or Stop was detected at a time when it is not allowed by the
																					// I2C specification. The Master interface has stopped driving the bus and gone to an idle state, no action is required. A request for a Start
																					// could be made, or software could attempt to insure that the bus has not stalled.
} CHAL_I2C_Xfr_Status_t;

// remap CHA to direct chip defs if defined.
#ifdef CHIP_I2C_IRQSTAT_MSARBLOSS
	#define	CHAL_I2C_IRQSTAT_MSTRARBLOSS		CHIP_I2C_IRQSTAT_MSARBLOSS			// Master Arbitration Loss
#endif

#ifdef CHIP_I2C_IRQSTAT_MSSTSTPERR
	#define	CHAL_I2C_IRQSTAT_MSTSTSTPERR		CHIP_I2C_IRQSTAT_MSSTSTPERR			// Master Start/Stop Error flag
#endif

#ifdef CHIP_I2C_IRQSTAT_SCLTIMEOUT
	#define	CHAL_I2C_IRQSTAT_SCLTIMEOUT			CHIP_I2C_IRQSTAT_SCLTIMEOUT			// SCL Time-out Interrupt flag. Indicates when SCL has remained low longer than the time specific by the TIMEOUT register.
#endif

#ifdef CHIP_I2C_IRQSTAT_EVENTTIMEOUT
	#define	CHAL_I2C_IRQSTAT_EVENTTIMEOUT		CHIP_I2C_IRQSTAT_EVENTTIMEOUT		// Event Time-out Interrupt flag. Indicates when the time between events has been longer than the time specified by the TIMEOUT register.
#endif

#ifdef CHIP_I2C_IRQSTAT_MSTPENDING
	#define	CHAL_I2C_IRQSTAT_MSTPENDING			CHIP_I2C_IRQSTAT_MSTPENDING			// Master Pending. Indicates that the Master is waiting to continue communication on the I2C-bus (pending) or is idle.
#endif

#define	CHAL_I2C_IRQSTAT_MSSTATE_CHANGED		CHIP_I2C_IRQSTAT_MSSTATE_CHANGED	// MASTER State reg was changed
//#define	CHAL_I2C_IRQSTAT_MSSTATE			CHIP_I2C_STAT_MSSTATE				// Master State code. The master state code reflects the master state when the MSTPENDING bit is set, that is the master is pending or in the idle state.
#define	CHAL_I2C_PERISTAT_MSSTATE_MASK			CHIP_I2C_MSSTATE_MASK				// Master State code mask.
#define CHAL_I2C_PERISTAT_MSSTATE_RX			CHIP_I2C_MSSTATE_RX					// I2C Master - RX Data available
#define CHAL_I2C_PERISTAT_MSSTATE_TX			CHIP_I2C_MSSTATE_TX					// I2C Master - TX Ready
#define CHAL_I2C_PERISTAT_MSSTATE_NACK_ADR		CHIP_I2C_MSSTATE_NACK_ADR			// I2C Master - Address No-Acked by slave
#define CHAL_I2C_PERISTAT_MSSTATE_NACK_DATA		CHIP_I2C_MSSTATE_NACK_DATA			// I2c Master - Data No-Acked by slave



extern void*	CHAL_I2C_Init (uint8_t PeriIndex);
extern uint32_t CHAL_I2C_Get_Clock_Rate(void *pPeri);
extern bool 	CHAL_I2C_Configure(void *pPeri, uint32_t Speed_kHz, CHAL_I2C_Mode_t Mode, uint16_t Address);
extern bool 	CHAL_I2CMST_Enable ( void *pPeri, bool NewState);
extern bool 	CHAL_I2CSLV_Enable ( void *pPeri, bool NewState);
extern bool 	CHAL_I2CMON_Enable ( void *pPeri, bool NewState);
extern bool 	CHAL_I2C_Set_BusSpeed ( void *pPeri, uint32_t Speed_kHz);
extern uint32_t CHAL_I2C_Get_BusSpeed ( void *pPeri);
extern void 	CHAL_I2CMST_Put_Data(void *pPeri, uint32_t NewValue);
extern uint32_t CHAL_I2CMST_Get_Data(void *pPeri);
extern void 	CHAL_I2CMST_Set_Start(void *pPeri);
extern void 	CHAL_I2CMST_Set_Stop(void *pPeri);
extern void 	CHAL_I2CMST_Reset(void *pPeri);
extern void 	CHAL_I2CMST_Set_Cont(void *pPeri);
extern uint32_t CHAL_I2C_Get_Peri_Status(void *pPeri);
extern bool 	CHAL_I2C_Clear_Peri_Status(void *pPeri, uint32_t NewValue);
extern bool 	CHAL_I2CTimeout_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	CHAL_I2CMST_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	CHAL_I2CSLV_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	CHAL_I2CMON_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	CHAL_I2CMST_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	CHAL_I2CSLV_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	CHAL_I2CMON_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	CHAL_I2CTimeout_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern uint32_t CHAL_I2CMST_Get_IRQ_Status(void *pPeri);
extern uint32_t CHAL_I2CSLV_Get_IRQ_Status(void *pPeri);
extern uint32_t CHAL_I2CMON_Get_IRQ_Status(void *pPeri);
extern uint32_t CHAL_I2CTimeout_Get_IRQ_Status(void *pPeri);
extern bool 	CHAL_I2CMST_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	CHAL_I2CSLV_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	CHAL_I2CMON_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	CHAL_I2CTimeout_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
























// ******************************************************************************************************
// UART Functions
// ******************************************************************************************************
// remap CHA to direct chip defs. Have to by Ifdef, because some MCU didn't have to support each status flags....
#ifdef CHIP_UART_PERISTAT_TXIDLE
	#define	CHAL_UART_PERISTAT_TXIDLE		CHIP_UART_PERISTAT_TXIDLE				// peripherial status
#else
	#define	CHAL_UART_PERISTAT_TXIDLE		UINT32_MAX
#endif

#ifdef CHIP_UART_PERISTAT_RXIDLE
	#define	CHAL_UART_PERISTAT_RXIDLE		CHIP_UART_PERISTAT_RXIDLE				// peripherial status
#else 
	#define CHAL_UART_PERISTAT_RXIDLE		UINT32_MAX								// have to 0xffffffffff
#endif

#ifdef CHIP_UART_IRQSTAT_TXRDY
	#define CHAL_UART_IRQSTAT_TXRDY			CHIP_UART_IRQSTAT_TXRDY					// Transmitter transmitt character
#else
	#define CHAL_UART_IRQSTAT_TXRDY			0
#endif

#ifdef CHIP_UART_IRQSTAT_RXRDY
	#define CHAL_UART_IRQSTAT_RXRDY			CHIP_UART_IRQSTAT_RXRDY					// Receiver receive character
#else
	#define CHAL_UART_IRQSTAT_RXRDY			0
#endif

#ifdef CHIP_UART_IRQSTAT_TXIDLE
	#define CHAL_UART_IRQSTAT_TXIDLE		CHIP_UART_IRQSTAT_TXIDLE				// Transmitter in IDLE mode
#else
	#define CHAL_UART_IRQSTAT_TXIDLE		0
#endif

#ifdef CHIP_UART_IRQSTAT_RXIDLE	
	#define CHAL_UART_IRQSTAT_RXIDLE		CHIP_UART_IRQSTAT_RXIDLE				// Receiver in IDLE mode
#else
	#define CHAL_UART_IRQSTAT_RXIDLE		0
#endif

#ifdef CHIP_UART_IRQSTAT_TXERR
	#define CHAL_UART_IRQSTAT_TXERR			CHIP_UART_IRQSTAT_TXERR					// Transmitter error
#else	
	#define CHAL_UART_IRQSTAT_TXERR			0
#endif

#ifdef CHIP_UART_IRQSTAT_RXERR
	#define CHAL_UART_IRQSTAT_RXERR			CHIP_UART_IRQSTAT_RXERR					// Receiver error
#else	
	#define CHAL_UART_IRQSTAT_RXERR			0
#endif




#ifdef CHIP_UARTFIFO_IRQSTAT_TXRDY
	#define CHAL_UARTFIFO_IRQSTAT_TXRDY		CHIP_UARTFIFO_IRQSTAT_TXRDY				// Transmitter FIFO transmitted character
#else
	#define CHAL_UARTFIFO_IRQSTAT_TXRDY		0
#endif

#ifdef CHIP_UARTFIFO_IRQSTAT_RXRDY
	#define CHAL_UARTFIFO_IRQSTAT_RXRDY		CHIP_UARTFIFO_IRQSTAT_RXRDY				// Receiver FIFO receive character
#else
	#define CHAL_UARTFIFO_IRQSTAT_RXRDY		0
#endif

#ifdef CHIP_UARTFIFO_IRQSTAT_TXERR
	#define CHAL_UARTFIFO_IRQSTAT_TXERR		CHIP_UARTFIFO_IRQSTAT_TXERR				// Transmit FIFO error
#else
	#define CHAL_UARTFIFO_IRQSTAT_TXERR		0
#endif

#ifdef CHIP_UARTFIFO_IRQSTAT_RXERR
	#define CHAL_UARTFIFO_IRQSTAT_RXERR		CHIP_UARTFIFO_IRQSTAT_RXERR				// Receive FIFO error
#else	
	#define CHAL_UARTFIFO_IRQSTAT_RXERR		0
#endif

#ifdef CHIP_UARTFIFO_STAT_RXNOEMPTY
	#define CHAL_UARTFIFO_STAT_RXNOEMPTY	CHIP_UARTFIFO_STAT_RXNOEMPTY			// Receive FIFO NOT Empty (still contains received characters)
#else	
	#define CHAL_UARTFIFO_STAT_RXNOEMPTY	0
#endif





extern void*	CHAL_UART_Init (uint8_t PeriIndex);
extern uint32_t CHAL_UART_Get_Clock_Rate(void *pPeri);
extern bool 	CHAL_UART_Enable (void *pPeri, bool NewState);
extern void 	CHAL_UART_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	CHAL_UART_Enable_IRQ_NVIC (void *pPeri, bool NewState);
extern void 	CHAL_UART_Put_Data(void *pPeri, uint32_t WrData);
extern uint32_t CHAL_UART_Get_Data(void *pPeri);
extern uint32_t CHAL_UART_Get_Peri_Status(void *pPeri);
extern void 	CHAL_UART_Clear_Peri_Status(void *pPeri, uint32_t IRQBitMask);
extern uint32_t CHAL_UART_Get_IRQ_Status(void *pPeri);
extern void 	CHAL_UART_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask);
extern void 	CHAL_UART_Flush_Tx(void *pPeri);
extern void 	CHAL_UART_Flush_Rx(void *pPeri);
extern bool 	CHAL_UART_Set_Conf(void *pPeri, uint32_t BaudRate, uint8_t DataLen, uint8_t Parity, uint8_t StopBits);
extern bool 	CHAL_UART_Set_Address(void *pPeri, uint8_t NewAddress);
extern uint8_t 	CHAL_UART_Get_Address(void *pPeri);
extern bool 	CHAL_UART_Set_RTS_Conf(void* pPeri, const CHAL_Signal_t *sRTS);
extern bool 	CHAL_UART_Set_CTS_Conf(void* pPeri, const CHAL_Signal_t *sCTS);
extern bool 	CHAL_UART_Set_DIRDE_Conf(void* pPeri, const CHAL_Signal_t *sDIRDE);
extern bool 	CHAL_UART_Set_RE_Conf(void* pPeri, const CHAL_Signal_t *sRE);
extern bool 	CHAL_UART_Set_AdrDet(void* pPeri, bool NewState);
extern uint32_t CHAL_UART_Get_Baud (void *pPeri);
extern void 	CHAL_UARTFIFO_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState);
extern uint32_t CHAL_UARTFIFO_Get_Peri_Status(void *pPeri);
extern void 	CHAL_UARTFIFO_Clear_Peri_Status(void *pPeri, uint32_t IRQBitMask);
extern uint32_t CHAL_UARTFIFO_Get_IRQ_Status(void *pPeri);
extern void 	CHAL_UARTFIFO_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask);
extern uint32_t CHAL_UARTFIFO_Get_TxTHLevel (void *pPeri);
extern uint32_t CHAL_UARTFIFO_Get_RxTHLevel (void *pPeri);
extern void 	CHAL_UARTFIFO_Set_TxTHLevel (void *pPeri, uint32_t NewValue);
extern void 	CHAL_UARTFIFO_Set_RxTHLevel (void *pPeri, uint32_t NewValue);













// ******************************************************************************************************
// SPI Functions
// ******************************************************************************************************
#define	CHAL_SPI_STAT_SSA				CHIP_SPI_STAT_SSA							// Slave Select Assert
#define	CHAL_SPI_STAT_SSD				CHIP_SPI_STAT_SSD							// Slave Select Deassert
#define	CHAL_SPI_STAT_MSTIDLE			CHIP_SPI_STAT_MSTIDLE						// Master transmitter idle

#define	CHAL_SPI_IRQSTAT_SSA			CHIP_SPI_IRQSTAT_SSA						// Slave Select Assert
#define	CHAL_SPI_IRQSTAT_SSD			CHIP_SPI_IRQSTAT_SSD						// Slave Select Deassert
#define	CHAL_SPI_IRQSTAT_MSTIDLE		CHIP_SPI_IRQSTAT_MSTIDLE					// Master Transmitter idle

#define	CHAL_SPI_IRQEN_SSA				CHIP_SPI_IRQEN_SSA							// Slave Select Assert IRQ enable
#define	CHAL_SPI_IRQEN_SSD				CHIP_SPI_IRQEN_SSD							// Slave Select Deassert IRQ enable
#define	CHAL_SPI_IRQEN_MSTIDLE			CHIP_SPI_IRQEN_MSTIDLE						// Master transmitter Idle IRQ enable

#if defined (_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
 #define	CHAL_SPIFIFO_IRQSTAT_RXDONE	CHIP_SPIFIFO_IRQSTAT_RXDONE					// Receiver Done IRQ Flag
 #define	CHAL_SPIFIFO_IRQSTAT_RXOV	CHIP_SPIFIFO_IRQSTAT_RXOV					// Receiver Overrun or error IRQ Flag
 #define	CHAL_SPIFIFO_IRQSTAT_TXRDY	CHIP_SPIFIFO_IRQSTAT_TXRDY					// Transmitter Ready value
 #define	CHAL_SPIFIFO_IRQSTAT_TXUR	CHIP_SPIFIFO_IRQSTAT_TXUR					// Transmitter underrun or error IRQ Flag
 #define	CHAL_SPIFIFO_IRQEN_RXOV		CHIP_SPIFIFO_IRQEN_RXOV						// FIFO Receiver Overrun IRQ enable
 #define	CHAL_SPIFIFO_IRQEN_RXDONE	CHIP_SPIFIFO_IRQEN_RXDONE					// FIFO Receiver Done IRQ enable
 #define	CHAL_SPIFIFO_IRQEN_TXRDY	CHIP_SPIFIFO_IRQEN_TXRDY					// FIFO Transmitter Ready IRQ enable
 #define	CHAL_SPIFIFO_IRQEN_TXUR		CHIP_SPIFIFO_IRQEN_TXUR						// FIFO Transmitter underrun IRQ enable 
#else 
 #define	CHAL_SPI_IRQSTAT_RXDONE		CHIP_SPI_IRQSTAT_RXDONE						// Receiver Done IRQ Flag
 #define	CHAL_SPI_IRQSTAT_RXOV		CHIP_SPI_IRQSTAT_RXOV						// Receiver Overrun or error IRQ Flag
 #define	CHAL_SPI_IRQSTAT_TXRDY		CHIP_SPI_IRQSTAT_TXRDY						// Transmitter Ready value
 #define	CHAL_SPI_IRQSTAT_TXUR		CHIP_SPI_IRQSTAT_TXUR						// Transmitter underrun or error IRQ Flag
 #define	CHAL_SPI_IRQEN_RXDONE		CHIP_SPI_IRQEN_RXDONE						// Receiver Done IRQ enable
 #define	CHAL_SPI_IRQEN_RXOV			CHIP_SPI_IRQEN_RXOV							// Receiver Overrun IRQ enable
 #define	CHAL_SPI_IRQEN_TXRDY		CHIP_SPI_IRQEN_TXRDY						// Transmitter Ready IRQ enable
 #define	CHAL_SPI_IRQEN_TXUR			CHIP_SPI_IRQEN_TXUR							// Transmitter underrun IRQ enable
#endif



typedef enum
{
	CHAL_SPI_STAT_OK					= 0x4000,
	CHAL_SPI_STAT_ERROR					= 0x8000,
	CHAL_SPI_STAT_BUSY					= 0x0001,									// in progress. wait for ~BUSY
	CHAL_SPI_STAT_BUFFFULL				= 0x0002,
	CHAL_SPI_STAT_BUFFNONE				= 0x0004,
	CHAL_SPI_STAT_TIMEOUT				= 0x0040,	
} CHAL_SPI_Xfr_Status_t;

typedef enum
{
	CHAL_SPI_CMD_READ					= 0x03,										// Read operation
	CHAL_SPI_CMD_WRITE					= 0x02,										// Write operation
	CHAL_SPI_CMD_BYTE					= 0x00,										// Byte operation
	CHAL_SPI_CMD_PAGE					= 0x80,										// Page operation
	CHAL_SPI_CMD_SEQ					= 0x40,										// Sequential operation
} CHAL_SPI_CMD_t;		// opcode je zalezitost konkretneho Device !!!



extern void*	CHAL_SPI_Init (uint8_t PeriIndex);
inline bool 	CHAL_SPI_Configure(void *pPeri, uint32_t Speed_Hz, CHAL_SPI_XferMode_t Mode, bool CPHA, bool CPOL);
extern void 	CHAL_SPI_Put_Data(void *pPeri, uint32_t NewValue, uint8_t NumOfBits, uint32_t CtrlFlags, uint8_t SSel);
extern uint32_t CHAL_SPI_Get_Data(void *pPeri);
extern void 	CHAL_SPI_SendEOT(void *pPeri);
extern void 	CHAL_SPI_SendEOF(void *pPeri);
extern bool 	CHAL_SPI_Enable ( void *pPeri, bool NewState);
extern uint32_t CHAL_SPI_Get_Peri_Status(void *pPeri);
extern bool 	CHAL_SPI_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	CHAL_SPI_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern uint32_t CHAL_SPI_Get_IRQ_Status(void *pPeri);
extern bool 	CHAL_SPI_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	CHAL_SPI_Clear_Peri_Status(void *pPeri, uint32_t NewValue);
extern bool 	CHAL_SPI_Set_BusSpeed ( void *pPeri, uint32_t Speed_Hz);
extern uint32_t CHAL_SPI_Get_BusSpeed ( void *pPeri);
extern void 	CHAL_SPI_Flush_Rx(void *pPeri);
extern void 	CHAL_SPI_Flush_Tx(void *pPeri);
extern void 	CHAL_SPIFIFO_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState);
extern uint32_t CHAL_SPIFIFO_Get_Peri_Status(void *pPeri);
extern void 	CHAL_SPIFIFO_Clear_Peri_Status(void *pPeri, uint32_t BitMask);
extern uint32_t CHAL_SPIFIFO_Get_IRQ_Status(void *pPeri);
extern void 	CHAL_SPIFIFO_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask);
extern uint32_t CHAL_SPIFIFO_Get_TxTHLevel (void *pPeri);
extern uint32_t CHAL_SPIFIFO_Get_RxTHLevel (void *pPeri);
extern void 	CHAL_SPIFIFO_Set_TxTHLevel (void *pPeri, uint32_t NewValue);
extern void 	CHAL_SPIFIFO_Set_RxTHLevel (void *pPeri, uint32_t NewValue);









// ******************************************************************************************************
// EEPROM Functions
// ******************************************************************************************************

extern void*	CHAL_IEEPROM_Init (uint8_t PeriIndex);
extern bool 	CHAL_IEEPROM_Enable (void *pPeri, bool NewState);
extern bool 	CHAL_IEEPROM_Power (void *pPeri, bool NewState);
extern bool 	CHAL_IEEPROM_Write(void *pPeri, uint32_t EEDst, uint8_t *pInBuff, size_t byteswrt);
extern bool 	CHAL_IEEPROM_Read(void *pPeri, uint32_t EESrc, uint8_t *pOutBuff, size_t bytesrd);






// ******************************************************************************************************
// USB Functions
// ******************************************************************************************************
extern void*	CHAL_USB_Init ( uint8_t PeriIndex);
extern bool 	CHAL_USB_Power(void *pPeri, bool NewState);
extern bool 	CHAL_USB_Configure(void *pPeri, CHAL_USB_Mode_t Mode);
extern uint32_t CHAL_USB_Get_IRQ_Status(void *pPeri);









// ******************************************************************************************************
// ADC Functions
// ******************************************************************************************************
extern void*	CHAL_ADC_Init ( uint8_t PeriIndex);
extern bool 	CHAL_ADC_Power ( void *pPeri, bool NewState);
extern void 	CHAL_ADC_Start_CH ( void *pPeri, uint32_t ChannelBitMask);
extern bool 	CHAL_ADC_Enable_CH ( void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool 	CHAL_ADC_Configure_CH(void *pPeri, uint32_t ChannelBitMask, uint32_t Speed_kHz, bool RepeatMode, bool DMAMode);
extern uint32_t CHAL_ADC_Read_Active_Channels(void* pPeri);
extern uint32_t	CHAL_ADC_Get_Data_CH( void *pPeri, uint8_t Channel);
extern bool 	CHAL_ADC_Get_DataValidFlag_CH( void *pPeri, uint8_t Channel);
extern bool 	CHAL_ADC_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	CHAL_ADC_Enable_IRQ_Peri(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern uint32_t CHAL_ADC_Get_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask);
extern bool 	CHAL_ADC_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask);
extern bool 	CHAL_TEMP_Enable(bool NewState);










// ******************************************************************************************************
// TIMER Functions - Timer, CTimer
// ******************************************************************************************************
extern void*	CHAL_Timer_Init (uint8_t PeriIndex);
extern bool 	CHAL_Timer_Enable_CH (void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool 	CHAL_Timer_Enable_IRQ_NVIC (void *pPeri, bool NewState);
extern bool 	CHAL_Timer_Configure(void *pPeri, uint8_t Channel, uint32_t RateHz, CHAL_Timer_Mode_t Mode);
extern bool 	CHAL_Timer_IRQ_Peri_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern uint32_t CHAL_Timer_Get_IRQ_Status(void *pPeri);
extern bool 	CHAL_Timer_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask);

















// ******************************************************************************************************
// PWM Functions
// ******************************************************************************************************

extern void*	CHAL_PWM_Init (uint8_t PeriIndex);
extern bool 	CHAL_PWM_Enable_IRQ_NVIC (void *pPeri, bool NewState);
















// ******************************************************************************************************
// DMA Functions
// ******************************************************************************************************
typedef _Chip_DMA_Xfer_t	CHAL_DMA_Xfer_t;

extern void*	CHAL_DMA_Init (uint8_t PeriIndex);
extern bool 	CHAL_DMA_Enable_CH (void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool 	CHAL_DMA_Configure(void *pPeri, uint8_t Channel, CHAL_DMA_Xfer_t *Xfer);
extern uint32_t CHAL_DMA_Get_CHIRQ_Status (void *pPeri);
extern uint32_t CHAL_DMA_Get_CHControl (void *pPeri, uint32_t Channel);
extern bool 	CHAL_DMA_Clear_CHIRQ_Status (void *pPeri, uint8_t ChannelNum);
	
















// ******************************************************************************************************
// SCT Functions
// ******************************************************************************************************
extern void*	CHAL_SCT_Init (uint8_t PeriIndex);
extern bool 	CHAL_SCT_Enable (void *pPeri, bool NewState);
extern bool 	CHAL_SCT_Configure (void *pPeri);









// ******************************************************************************************************
// ETH Functions
// ******************************************************************************************************
extern void*	CHAL_ETH_Init ( uint8_t PeriIndex);
extern bool 	CHAL_ETH_Enable ( void *pPeri, bool NewState);

















// ******************************************************************************************************
// LCD Functions
// ******************************************************************************************************

extern void*	CHAL_LCD_Init ( uint8_t PeriIndex);
extern bool 	CHAL_LCD_Enable ( void *pPeri, bool NewState);
extern bool 	CHAL_LCD_Configure (void *pPeri);
extern bool 	CHAL_LCD_IRQ_Peri_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void 	CHAL_LCD_Clear_IRQ_Status(void *pPeri, uint32_t ClearMask);
extern uint32_t CHAL_LCD_Get_IRQ_Status(void *pPeri);
extern void*	CHAL_WDT_Init ( uint8_t PeriIndex);
extern bool 	CHAL_WDT_Configure(void *pPeri, uint32_t WinMinVal, uint32_t WinMaxVal, uint32_t WarningVal, bool HardRst);
extern bool 	CHAL_WDT_Enable ( void *pPeri, bool NewState);
extern void 	CHAL_WDT_Reset (void *pPeri);
extern uint32_t CHAL_WDT_Get_IRQ_Status(void *pPeri);
extern void 	CHAL_WDT_Clear_IRQ_Status(void *pPeri, uint32_t ClearMask);







// ******************************************************************************************************
// DAC Functions
// ******************************************************************************************************
extern void *CHAL_DAC_Init (uint8_t PeriIndex);
extern bool CHAL_DAC_Enable (void *pPeri, bool NewState);
extern void CHAL_DAC_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool CHAL_DAC_Enable_IRQ_NVIC (void *pPeri, bool NewState);
extern void CHAL_DAC_Put_Data(void *pPeri, uint32_t WrData);
extern uint32_t CHAL_DAC_Get_Peri_Status(void *pPeri);
extern void CHAL_DAC_Clear_Peri_Status(void *pPeri, uint32_t IRQBitMask);
extern uint32_t CHAL_DAC_Get_IRQ_Status(void *pPeri);
extern void CHAL_DAC_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask);
extern bool CHAL_DAC_Configure(void *pPeri);





// ******************************************************************************************************
// WDT Functions
// ******************************************************************************************************
extern void *CHAL_WDT_Init ( uint8_t PeriIndex);
extern bool CHAL_WDT_Configure(void *pPeri, uint32_t WinMinVal, uint32_t WinMaxVal, uint32_t WarningVal, bool HardRst);
extern bool CHAL_WDT_Enable ( void *pPeri, bool NewState);
extern void CHAL_WDT_Reset (void *pPeri);
extern uint32_t CHAL_WDT_Get_IRQ_Status(void *pPeri);
extern void CHAL_WDT_Clear_IRQ_Status(void *pPeri, uint32_t ClearMask);










// ******************************************************************************************************
// SWM Functions
// ******************************************************************************************************



// ******************************************************************************************************
// MRT Functions - MRTx
// ******************************************************************************************************

extern void *CHAL_MRT_Init (uint8_t PeriIndex);
extern bool CHAL_MRT_Enable_CH (void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool CHAL_MRT_Configure(void *pPeri, uint8_t Channel, uint32_t RateHz, CHAL_Timer_Mode_t Mode);
extern void CHAL_MRT_Set_TmrVal(void *pPeri, uint8_t Channel, uint32_t TmrValue);
extern void CHAL_MRT_Reset_Tmr_CH(void *pPeri, uint32_t ChannelBitMask);
extern uint32_t CHAL_MRT_Get_TmrVal(void *pPeri, uint8_t Channel);
extern uint32_t CHAL_MRT_Get_IRQ_Status_CH(void *pPeri);
extern bool CHAL_MRT_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask);
extern void CHAL_MRT_Enable_IRQ_CH (void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool CHAL_MRT_Enable_IRQ_NVIC (void *pPeri, bool NewState);
#endif	// __CHIP_HAL_H_


