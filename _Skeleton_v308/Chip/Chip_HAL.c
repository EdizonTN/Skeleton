// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: Chip_HAL.c
// 	   Version: 5.22
//		  Desc:  ARM Chip HAL basic library
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//				2024.09.11	- v5.22 - fix _Chip_Clock_Get_SourceRTC_Rate calling
//							- add CHAL_ADC_Get_Data_CH function
//				2024.09.04	- v5.21 - add CHAL_UART_Get_Clock_Rate function
//				2024.01.26	- v5.20	- add Rx Flush
//				2024.01.11	- v5.10 - rename ADC channels oriented functions
//				2023.12.07	- v 5.01 - fix some I2C functiuon for _CHIP_HAVE_I2C to _CHIP_I2C_COUNT
//							- global rename all function with ChannelBitMask paramteter, to _CH as suffix
//				2022.11.29	- v 5.00 - clean code and fix .h and .c content
//				2021.06.18	- extend SPI Configure
//				2021.03.09	- Add I2C slave mode
//				2020.08.04	- I2C access divided to master, slave, monitor and timeouts
//				2020.07.15	- Added DAC section
//				2020.03.21	- v3.0: upgrade to v3
//				2019.06.11	- Optimalization


#include "Skeleton.h"

#include 	"Chip\NXP\LPC8XX\_chip_lpc8xx.c"										// load LPC8xx HAL, if defined
#include 	"Chip\NXP\LPC11XX\_chip_lpc11xx.c"										// load LPC11xx HAL, if defined
#include 	"Chip\NXP\LPC15XX\_chip_lpc15xx.c"										// load LPC15xx HAL, if defined
#include 	"Chip\NXP\LPC17XX\_chip_lpc17xx.c"										// load LPC17xx HAL, if defined
#include	"Chip\NXP\LPC546XX\_chip_lpc546xx.c"
//	.. add some new chip file here...
//ex: #include 	"Chip\NXP\LPC17XX\_chip_lpc17xx.c"									// load LPC17xx HAL, if defined

// function from this file, will call ONLY functions from included c file (selected device) as static inline.


#include	<stdarg.h>


// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************
#if defined( BSP_EXQUARTZFREQ )
uint32_t 	OscRateIn = BSP_EXQUARTZFREQ;											// needs for clock_xxxxx.h
#endif

#if defined( BSP_RTCQUARTZFREQ )
uint32_t 	RTCOscRateIn = BSP_RTCQUARTZFREQ;
#endif

#if defined( BSP_EXRATEIN )
uint32_t 	ExtRateIn = BSP_EXRATEIN;
#endif









// ******************************************************************************************************
// CHIP functions
// ******************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// Generate reset for MCU
void CHAL_Chip_Reset(void)
{
	NVIC_SystemReset();
}

// ------------------------------------------------------------------------------------------------------
// Call Chip_Init for exact MCU
inline void CHAL_Chip_SystemInit(void) 
{
	_Chip_SystemInit ();															// volaj system init pre konkretny chip - inlining
}

// ------------------------------------------------------------------------------------------------------
// init ITM and SWO, basd on CONF_DEBUG_ITM and BSP_ALLOW_SWO
inline void CHAL_Chip_SWO_Init(uint32_t Main_CPU_Clock)
{
#if (CONF_DEBUG_ITM == 1) && (__CORTEX_M >= 3)										// debug with ITM support
	_Chip_SWO_Init(Main_CPU_Clock);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Return source of Reset signal
inline void CHAL_Chip_Read_ResetSource(CHAL_RST_Src_t* DstStruct)
{
	_Chip_Read_ResetSource((uint32_t*)DstStruct);
}


// ------------------------------------------------------------------------------------------------------
// Read Chip ID
bool CHAL_Chip_IAP_Read_ID(uint32_t *pDst)
{
	return(_Chip_IAP_Read_ID(pDst));
}


// ------------------------------------------------------------------------------------------------------
// Read Chip serial number UID
bool CHAL_Chip_IAP_Read_SerialNum(uint32_t *pDst)
{
	return(_Chip_IAP_Read_SerialNum(pDst));
}


// ------------------------------------------------------------------------------------------------------
// Read Chip BootLoader Version
bool CHAL_Chip_IAP_Read_BootCodeVersion(uint32_t *pDst)
{
	return(_Chip_IAP_Read_BootCodeVersion(pDst));
}













// ******************************************************************************************************
// CLOCK functions
// ******************************************************************************************************
void CHAL_Chip_Update_SystemCoreClock(void)
{
	SystemCoreClock = _Chip_Clock_Get_SysClk_Rate();								// SystemCoreClock reload for backward compatibilty / used in IAP /
}


// ------------------------------------------------------------------------------------------------------
//  Read System Clock (SRC) value - input clock into MCU (Ex. quartz frequency, or internal RC frequency,...)
uint32_t CHAL_Chip_Read_SrcFreq(void)
{
	return(_Chip_Clock_Get_SourceClk_Rate());
}

// ------------------------------------------------------------------------------------------------------
// Read CPU core clock frequency
uint32_t CHAL_Chip_Read_CoreFreq(void)		// was: CHAL_Chip_Read_CPUFreq
{
	return(_Chip_Clock_Get_CoreClk_Rate());											// get freqency of CPU core
}


// ------------------------------------------------------------------------------------------------------
// Read system RTC clock frequency
uint32_t CHAL_Chip_Read_RTCFreq(void)
{
#if defined (_CHIP_RTC_COUNT) && (_CHIP_RTC_COUNT > 0)
	return(_Chip_Clock_Get_SourceRTC_Rate());											// get freqencuy of RTC block inputs
#else
	return(0);																		// this MCU hasn't RTC
#endif
}


// ------------------------------------------------------------------------------------------------------
// how many instructions (ticks) CPU do in one microseconds
uint32_t CHAL_Chip_Read_TickPerUS(void)
{
	return(_Chip_Clock_Get_CoreClk_Rate() / 1000000);								// number of CPU ticks per 1 microseconds
}


// ------------------------------------------------------------------------------------------------------
// Read USB_Clock - input frequency to USB block
uint32_t CHAL_Chip_Read_USBFreq(void)
{
#if defined (_CHIP_USB_COUNT) && (_CHIP_USB_COUNT > 0)
	return(_Chip_Clock_Get_USBClk_Rate());											// get USB input frequency
#else
	return(0);																		// this MCU hasn't USB block
#endif
}


// ------------------------------------------------------------------------------------------------------
// Read SPIFI_Clock - input frequency for SPIFI block
uint32_t CHAL_Chip_Read_SPIFIFreq(void)
{
#if defined (_CHIP_SPIFI_COUNT) && (_CHIP_SPIFI_COUNT > 0)
	return(_Chip_Clock_Get_SPIFIClk_Rate());										// get SPIFI block freqency
#else
	return(0);																		// this MCU hasn't SPIFI block
#endif
}

// ------------------------------------------------------------------------------------------------------
// Read SPIFI_Clock - input frequency for EMC block
uint32_t CHAL_Chip_Read_EMCFreq(void)
{
#if defined (_CHIP_EMC_COUNT) && (_CHIP_EMC_COUNT > 0)
	return(_Chip_Clock_Get_EMCClk_Rate());											// get EMC block freqency
#else
	return(0);																		// this MCU hasn't EMC block
#endif
}

// ------------------------------------------------------------------------------------------------------
// Read CLKOUT_Clock - CLKOUT pint output frequency
uint32_t CHAL_Chip_Read_CLKOUTFreq(void)
{
	return(_Chip_Clock_Get_ClkOut_Rate());											// get CLKOUT pin freqency
}











// ******************************************************************************************************
// SIGNAL Functions
// ******************************************************************************************************


// ------------------------------------------------------------------------------------------------------
// Signal IRQ Enable/Disable
// result is true:successfull otherwise false
bool CHAL_Signal_PINT_Enable_IRQ_NVIC(void *pPeri, uint32_t Port, uint32_t Pin, bool NewState)
{
#if defined (_CHIP_PINT_COUNT) && (_CHIP_PINT_COUNT > 0)
	return(_Chip_GPIO_PINT_Enable_IRQ_NVIC(pPeri, Port, Pin, NewState));
#else
	return(false);
#endif	
}


// ------------------------------------------------------------------------------------------------------
// Read Signalu state
// Depends of signal activity
bool CHAL_Signal_Read_Pin ( CHAL_STD_Signal_t Inp)
{
	if((Inp.Port == 0xff) && (Inp.Pin == 0x00)) 									// pin neexistuje - sme na virtualnom pine
	{
		return (false);
	}
	
	if(Inp.Act) return( _Chip_GPIO_GetPinState( _Chip_Get_GPIO_Ptr(Inp.Port), Inp.Port, Inp.Pin));
	else 		return( !_Chip_GPIO_GetPinState( _Chip_Get_GPIO_Ptr(Inp.Port), Inp.Port, Inp.Pin));
}

// ------------------------------------------------------------------------------------------------------
// Set Signal
// Depends of signal activity
void CHAL_Signal_Set_Pin ( CHAL_STD_Signal_t Inp)
{
	if((Inp.Port == 0xff) && (Inp.Pin == 0x00)) 									// pin neexistuje - sme na virtualnom pine
	{
		return;		
	}
	
	if(!Inp.Act) 	{ _Chip_GPIO_SetPinOutLow( _Chip_Get_GPIO_Ptr(Inp.Port), Inp.Port, Inp.Pin );}
	else 			{ _Chip_GPIO_SetPinOutHigh( _Chip_Get_GPIO_Ptr(Inp.Port), Inp.Port, Inp.Pin );}
}

// ------------------------------------------------------------------------------------------------------
// Clear Signal
// Depends of signal activity
void CHAL_Signal_Clear_Pin ( CHAL_STD_Signal_t Inp)
{
	if((Inp.Port == 0xff) && (Inp.Pin == 0x00)) 									// pin neexistuje - sme na virtualnom pine
	{
		return;		
	}

	if(Inp.Act)		_Chip_GPIO_SetPinOutLow( _Chip_Get_GPIO_Ptr(Inp.Port), Inp.Port, Inp.Pin );
	else 			_Chip_GPIO_SetPinOutHigh( _Chip_Get_GPIO_Ptr(Inp.Port), Inp.Port, Inp.Pin );
}


// ------------------------------------------------------------------------------------------------------
// Write Value to Signal
// Depends of signal activity
void CHAL_Signal_Write_Pin ( CHAL_STD_Signal_t Inp, bool Val)
{
	if((Inp.Port == 0xff) && (Inp.Pin == 0x00)) 									// pin neexistuje - sme na virtualnom pine
	{
		return;		
	}
	
	if(Inp.Act)		CHAL_GPIO_Write_Pin((uint32_t) Inp.Port, Inp.Pin, Val);
	else 			CHAL_GPIO_Write_Pin((uint32_t) Inp.Port, Inp.Pin, !Val);
}


// ------------------------------------------------------------------------------------------------------
// Toggle of Signal state
// Depends of signal activity
void CHAL_Signal_Toggle_Pin ( CHAL_STD_Signal_t Inp)
{
	if((Inp.Port == 0xff) && (Inp.Pin == 0x00)) 									// pin neexistuje - sme na virtualnom pine
	{
		return;		
	}
	
	_Chip_GPIO_SetPinToggle( _Chip_Get_GPIO_Ptr(Inp.Port), Inp.Port, Inp.Pin);
}

// ------------------------------------------------------------------------------------------------------
// Set direction of the Signal
void CHAL_Signal_Write_Direction(CHAL_STD_Signal_t Inp, CHAL_IO_Mode_t Dir)
{
	if((Inp.Port == 0xff) && (Inp.Pin == 0x00)) 									// pin neexistuje - sme na virtualnom pine
	{
		return;
	}

	if(Dir == CHAL_IO_Output )
	{
		_Chip_GPIO_Set_PinDir( _Chip_Get_GPIO_Ptr(Inp.Port), Inp.Port, 1 << Inp.Pin, true );
    }
	else
	{
		_Chip_GPIO_Set_PinDir( _Chip_Get_GPIO_Ptr(Inp.Port), Inp.Port, 1 << Inp.Pin, false );
	}
}






// ------------------------------------------------------------------------------------------------------
// Initialize Signal with appropriate port
bool CHAL_Signal_Init (const CHAL_Signal_t *Inp)
{
	//ToDo: parametre tu rozbi ale volaj standartne _chip funkcie.....
	if((Inp->Std.Port == 0xff) && (Inp->Std.Pin == 0x00)) 							// pin neexistuje - sme na virtualnom pine
	{
		return (true);
	}

	if((Inp->Std.Port == 0xff) && (Inp->Std.Pin == 0xff)) 							// pin neexistuje - sme na zle definovanom pine
	{
#if defined(CONF_DEBUG_SUBSYS_CHAL) && (CONF_DEBUG_SUBSYS_CHAL == 1)		
		sys_Err("(Chip_HAL) Port:%d, Pin:%d: Init Fail.(BAD PIN DECLARATION!)", (uint8_t) Inp->Std.Port, (uint8_t) Inp->Std.Pin);
#endif		
		return (false);
	}

	CHAL_GPIO_Init ( Inp->Std.Port, Inp->Std.Pin);
	
	_Chip_GPIO_Wr_Conf_Pin (Inp->Std.Port, Inp->Std.Pin, 4, (_Chip_IO_Spec_t*) &Inp->Ext.Conf);		// First, set alternative function if needs
	
	CHAL_Signal_Clear_Pin( Inp->Std);												// next, set inactive value
	CHAL_Signal_Write_Direction (Inp->Std, Inp->Ext.Dir);							// finnaly, write direction

	return(true);
}


// ------------------------------------------------------------------------------------------------------
// DeInitialization of Signal and port
// pNeutralConf - ptr to unconfigured structure of IOCON,....
void CHAL_Signal_DeInit ( const CHAL_Signal_t *Inp, void* pNeutralConf)
{
	if((Inp->Std.Port == 0xff) && (Inp->Std.Pin == 0x00)) 							// pin neexistuje - sme na virtualnom pine
	{
		return;		
	}

	_Chip_GPIO_Wr_Conf_Pin (Inp->Std.Port, Inp->Std.Pin, 4, pNeutralConf);			// unconfigure pin
	
	CHAL_Signal_Write_Direction (Inp->Std, Inp->Ext.Dir);							// set as default
	CHAL_Signal_Clear_Pin( Inp->Std);												// deactivate pin
}


// ------------------------------------------------------------------------------------------------------
// set signal condition for interrupt generation and enable IRQ
// return is poinetr to GINT
void *CHAL_Signal_Set_GINT(const CHAL_Signal_t *Inp, uint8_t Group, CHAL_SIG_IRQ_SENS_t Sens)
{
	void *res = NULL;
#if defined(_CHIP_GINT) && (_CHIP_HAVE_GINT == 1)
	switch(Sens)
	{
		case (_SIG_IRQ_LEVEL_LOW):
				res = _Chip_GPIO_GINT_Conf(Group, Inp->Std.Port, Inp->Std.Pin, 0x00); break;
		case (_SIG_IRQ_LEVEL_HIGH):
				res = _Chip_GPIO_GINT_Conf(Group, Inp->Std.Port, Inp->Std.Pin, 0x11); break;
		case (_SIG_IRQ_EDGE_RISE): 
				res = _Chip_GPIO_GINT_Conf(Group, Inp->Std.Port, Inp->Std.Pin, 0x01); break;
		case (_SIG_IRQ_EDGE_ACTORINACT):
		case (_SIG_IRQ_EDGE_RISEORFALL):
				res = _Chip_GPIO_GINT_Conf(Group, Inp->Std.Port, Inp->Std.Pin, 0x22); break;
		case (_SIG_IRQ_EDGE_FALL):
				res = _Chip_GPIO_GINT_Conf(Group, Inp->Std.Port, Inp->Std.Pin, 0x10); break;
		case (_SIG_IRQ_LEVEL_INACT):
		{
				if(Inp->Std.Act == true)res = _Chip_GPIO_GINT_Conf(Group, Inp->Std.Port, Inp->Std.Pin, 0x00);
				else res = _Chip_GPIO_GINT_Conf(Group, Inp->Std.Port, Inp->Std.Pin, 0x11);
				break;
		}
		case (_SIG_IRQ_LEVEL_ACT):
		{
				if(Inp->Std.Act == true)res = _Chip_GPIO_GINT_Conf(Group, Inp->Std.Port, Inp->Std.Pin, 0x11);
				else res = _Chip_GPIO_GINT_Conf(Group, Inp->Std.Port, Inp->Std.Pin, 0x00);
				break;
		}
		case (_SIG_IRQ_EDGE_TOACT):
		{
				if(Inp->Std.Act == true)res = _Chip_GPIO_GINT_Conf(Group, Inp->Std.Port, Inp->Std.Pin, 0x01);
				else res = _Chip_GPIO_GINT_Conf(Group, Inp->Std.Port, Inp->Std.Pin, 0x10);
				break;
		}
		case (_SIG_IRQ_EDGE_TOINACT):
		{
				if(Inp->Std.Act == true)res = _Chip_GPIO_GINT_Conf(Group, Inp->Std.Port, Inp->Std.Pin, 0x10);
				else res = _Chip_GPIO_GINT_Conf(Group, Inp->Std.Port, Inp->Std.Pin, 0x01);
				break;
		}
		default: break;
	}
#endif
	return(res);
}

// ------------------------------------------------------------------------------------------------------
//	PINT - Pin interrupts/pattern match engine
//	set signal condition for interrupt generation and enable IRQ
// return is poinetr to PINT
void *CHAL_Signal_Set_PINT(const CHAL_Signal_t *Inp, uint8_t PINTSel, CHAL_SIG_IRQ_SENS_t Sens)
{
	void *res = NULL;
#if defined(_CHIP_HAVE_PINT) && (_CHIP_HAVE_PINT == 1)	
	switch(Sens)
	{
		case (CHAL_SIG_IRQ_LEVEL_LOW):
				res = _Chip_GPIO_PINT_Conf(PINTSel, Inp->Std.Port, Inp->Std.Pin, 0x00); break;
		case (CHAL_SIG_IRQ_LEVEL_HIGH):
				res = _Chip_GPIO_PINT_Conf(PINTSel, Inp->Std.Port, Inp->Std.Pin, 0x11); break;
		case (CHAL_SIG_IRQ_EDGE_RISE): 
				res = _Chip_GPIO_PINT_Conf(PINTSel, Inp->Std.Port, Inp->Std.Pin, 0x01); break;
		case (CHAL_SIG_IRQ_EDGE_RISEORFALL): 
		case (CHAL_SIG_IRQ_EDGE_ACTORINACT):
				res = _Chip_GPIO_PINT_Conf(PINTSel, Inp->Std.Port, Inp->Std.Pin, 0x22); break;
		case (CHAL_SIG_IRQ_EDGE_FALL):
				res = _Chip_GPIO_PINT_Conf(PINTSel, Inp->Std.Port, Inp->Std.Pin, 0x10); break;
		case (CHAL_SIG_IRQ_LEVEL_INACT):
		{
				if(Inp->Std.Act == true)res = _Chip_GPIO_PINT_Conf(PINTSel, Inp->Std.Port, Inp->Std.Pin, 0x00);
				else res = _Chip_GPIO_PINT_Conf(PINTSel, Inp->Std.Port, Inp->Std.Pin, 0x11);
				break;
		}
		case (CHAL_SIG_IRQ_LEVEL_ACT):
		{
				if(Inp->Std.Act == true)res = _Chip_GPIO_PINT_Conf(PINTSel, Inp->Std.Port, Inp->Std.Pin, 0x11);
				else res = _Chip_GPIO_PINT_Conf(PINTSel, Inp->Std.Port, Inp->Std.Pin, 0x00);
				break;
		}
		case (CHAL_SIG_IRQ_EDGE_TOACT):
		{
				if(Inp->Std.Act == true)res = _Chip_GPIO_PINT_Conf(PINTSel, Inp->Std.Port, Inp->Std.Pin, 0x01);
				else res = _Chip_GPIO_PINT_Conf(PINTSel, Inp->Std.Port, Inp->Std.Pin, 0x10);
				break;
		}
		case (CHAL_SIG_IRQ_EDGE_TOINACT):
		{
				if(Inp->Std.Act == true)res = _Chip_GPIO_PINT_Conf(PINTSel, Inp->Std.Port, Inp->Std.Pin, 0x10);
				else res = _Chip_GPIO_PINT_Conf(PINTSel, Inp->Std.Port, Inp->Std.Pin, 0x01);
				break;
		}		
		
		default: break;
	}
#endif
	return(res);
}






// ******************************************************************************************************
// GPIO Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Initialization of Port and Pin
// result is true:successfull otherwise false

bool CHAL_GPIO_Init ( uint8_t PeriIndex, uint32_t Pin)
{
	return(_Chip_GPIO_Init ( PeriIndex, Pin));
}

// ------------------------------------------------------------------------------------------------------
// DeInitialization of whole port
// result is true:successfull otherwise false
bool CHAL_GPIO_DeInit ( uint8_t PeriIndex, uint32_t Pin)
{
	return(_Chip_GPIO_DeInit ( PeriIndex, Pin));
}

// ------------------------------------------------------------------------------------------------------
// Read Pin State
// result is pin state
bool CHAL_GPIO_Read_Pin ( uint8_t PeriIndex, uint32_t Pin)
{
	return(_Chip_GPIO_GetPinState( _Chip_Get_GPIO_Ptr(PeriIndex), PeriIndex, Pin));
}

// ------------------------------------------------------------------------------------------------------
// Direct Clear pin
void CHAL_GPIO_Clear_Pin ( uint8_t PeriIndex, uint32_t Pin)
{
	_Chip_GPIO_SetPinOutLow( _Chip_Get_GPIO_Ptr(PeriIndex), PeriIndex, Pin );
}

// ------------------------------------------------------------------------------------------------------
// Direct Set pin
void CHAL_GPIO_Set_Pin ( uint8_t PeriIndex, uint32_t Pin)
{
	_Chip_GPIO_SetPinOutHigh( _Chip_Get_GPIO_Ptr(PeriIndex), PeriIndex, Pin );
}

// ------------------------------------------------------------------------------------------------------
// Direct write Val. 
void CHAL_GPIO_Write_Pin ( uint8_t PeriIndex, uint32_t Pin, uint32_t Val)
{
	if(Val) 		{ _Chip_GPIO_SetPinOutHigh( _Chip_Get_GPIO_Ptr(PeriIndex), PeriIndex, Pin );}
	else 			{ _Chip_GPIO_SetPinOutLow( _Chip_Get_GPIO_Ptr(PeriIndex), PeriIndex, Pin );}
}

// ------------------------------------------------------------------------------------------------------
// Toggle value of port and pin
void CHAL_GPIO_Toggle_Pin ( uint8_t PeriIndex, uint32_t Pin)
{
	_Chip_GPIO_SetPinToggle( _Chip_Get_GPIO_Ptr(PeriIndex), PeriIndex, Pin);
}

// ------------------------------------------------------------------------------------------------------
// Set direction of port and pin
void CHAL_GPIO_Write_Direction( uint8_t PeriIndex, uint32_t Pin, CHAL_IO_Mode_t Dir)
{
	if(Dir == CHAL_IO_Output)
	{
		_Chip_GPIO_Set_PinDir( _Chip_Get_GPIO_Ptr(PeriIndex), PeriIndex, 1 << Pin, true );			// call set port dir from gpio_ZZZxxxx.c - set as output
    }
	else
	{
		_Chip_GPIO_Set_PinDir( _Chip_Get_GPIO_Ptr(PeriIndex), PeriIndex, 1 << Pin, false );			// call set port dir from gpio_ZZZxxxx.c - set as input
	}
}












// ******************************************************************************************************
// I2C Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Initialization of I2C
// result is pointer to periphery. otherwise NULL
void *CHAL_I2C_Init (uint8_t PeriIndex)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2C_Init(PeriIndex));
#else
	return(NULL);
#endif	
}



// ------------------------------------------------------------------------------------------------------
// Get I2C block input clock rate in kHz
uint32_t CHAL_I2C_Get_Clock_Rate(void *pPeri)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_Clock_Get_I2CClk_Rate( _Chip_I2C_Get_PeriIndex(pPeri)));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Configuration
bool CHAL_I2C_Configure(void *pPeri, uint32_t Speed_kHz, CHAL_I2C_Mode_t Mode, uint16_t Address)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)	
	if(Mode == CHAL_I2C_Master) return(_Chip_I2C_Configure( pPeri, Speed_kHz, true, false, false, 0));		// master mode
	if(Mode == CHAL_I2C_Slave) return(_Chip_I2C_Configure( pPeri, Speed_kHz, false, true, false, Address));		// slave mode
	if(Mode == CHAL_I2C_Monitor) return(_Chip_I2C_Configure( pPeri, Speed_kHz, false, false, true, Address));	// monitor mode
	return(false);
#else
	return(false);
#endif	
}


//// ------------------------------------------------------------------------------------------------------
//// I2C Complette Enable/Disable
//extern bool CHAL_I2C_Enable ( void *pPeri, bool NewState)
//{
//#if defined(_CHIP_HAVE_I2C) && (_CHIP_HAVE_I2C == 1)
//	return(_Chip_I2C_Enable(pPeri, NewState));
//#else
//	return(false);
//#endif	
//}

// ------------------------------------------------------------------------------------------------------
// I2C Master Enable/Disable
bool CHAL_I2CMST_Enable ( void *pPeri, bool NewState)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CMST_Enable(pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C Slave Enable/Disable
bool CHAL_I2CSLV_Enable ( void *pPeri, bool NewState)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CSLV_Enable(pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C Monitor MEnable/Disable
bool CHAL_I2CMON_Enable ( void *pPeri, bool NewState)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CMON_Enable(pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C Set Bus Speed
bool CHAL_I2C_Set_BusSpeed ( void *pPeri, uint32_t Speed_kHz)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2C_Set_BusSpeed(pPeri, Speed_kHz));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C Get Bus Speed
uint32_t CHAL_I2C_Get_BusSpeed ( void *pPeri)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2C_Get_BusSpeed(pPeri));
#else
	return(0);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C - write data into I2C
void CHAL_I2CMST_Put_Data(void *pPeri, uint32_t NewValue)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)	
	_Chip_I2CMST_Put_Data(pPeri, NewValue);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C - Read data from I2C
uint32_t CHAL_I2CMST_Get_Data(void *pPeri)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)	
	return(_Chip_I2CMST_Get_Data(pPeri));
#else
	return(0);
#endif
}

// ------------------------------------------------------------------------------------------------------
// I2C - Create START condition 
void CHAL_I2CMST_Set_Start(void *pPeri)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)	
	_Chip_I2CMST_Set_Start(pPeri);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C - Create STOP condition
void CHAL_I2CMST_Set_Stop(void *pPeri)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)	
	_Chip_I2CMST_Set_Stop(pPeri);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C - Reset communication
void CHAL_I2CMST_Reset(void *pPeri)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)	
	_Chip_I2CMST_Reset(pPeri);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C - Master Continue
void CHAL_I2CMST_Set_Cont(void *pPeri)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	_Chip_I2CMST_Set_Cont(pPeri);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get I2C Status register
uint32_t CHAL_I2C_Get_Peri_Status(void *pPeri)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2C_Get_Peri_Status( pPeri));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Clear I2C Status register
bool CHAL_I2C_Clear_Peri_Status(void *pPeri, uint32_t NewValue)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2C_Clear_Peri_Status( pPeri, NewValue));
#else
	return(false);
#endif	
}


//// ------------------------------------------------------------------------------------------------------
//// I2C Complette Enable/Disable
//// result is true:successfull otherwise false
//extern bool CHAL_I2C_Enable_IRQ_NVIC(void *pPeri, bool NewState)
//{
//#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
//	return(_Chip_I2C_Enable_IRQ_NVIC(pPeri, NewState));
//#else
//	return(false);
//#endif	
//	
//}

// ------------------------------------------------------------------------------------------------------
// I2C Timeout Enable/Disable NVIC Interrupt
// result is true:successfull otherwise false
bool CHAL_I2CTimeout_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CTimeout_Enable_IRQ_NVIC(pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C Master Enable/Disable NVIC Interrupt
// result is true:successfull otherwise false
bool CHAL_I2CMST_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CMST_Enable_IRQ_NVIC(pPeri, NewState));
#else
	return(false);
#endif	
}	

// ------------------------------------------------------------------------------------------------------
// I2C Slave Enable/Disable NVIC Interrupt
// result is true:successfull otherwise false
bool CHAL_I2CSLV_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CSLV_Enable_IRQ_NVIC(pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C Monitor Enable/Disable NVIC Interrupt
// result is true:successfull otherwise false
bool CHAL_I2CMON_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CMON_Enable_IRQ_NVIC(pPeri, NewState));
#else
	return(false);
#endif	
}

//// ------------------------------------------------------------------------------------------------------
//// I2C Enable/disable selected interrupts
//// result is true:successfull otherwise false
//extern bool CHAL_I2C_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
//{
//#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
//	return(_Chip_I2C_Enable_IRQ(pPeri, IRQBitMask, NewState));
//#else
//	return(false);
//#endif	
//}

// ------------------------------------------------------------------------------------------------------
// I2C Enable/disable selected MASTER interrupts in Periphery
// result is true:successfull otherwise false
bool CHAL_I2CMST_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CMST_Enable_IRQ(pPeri, IRQBitMask, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C Enable/disable selected SLAVE interrupts in Periphery
// result is true:successfull otherwise false
bool CHAL_I2CSLV_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CSLV_Enable_IRQ(pPeri, IRQBitMask, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C Enable/disable selected MONITOR interrupts in Periphery
// result is true:successfull otherwise false
bool CHAL_I2CMON_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CMON_Enable_IRQ(pPeri, IRQBitMask, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// I2C Enable/disable selected Timeout interrupts in Periphery
// result is true:successfull otherwise false
bool CHAL_I2CTimeout_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CTimeout_Enable_IRQ(pPeri, IRQBitMask, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get I2C MASTER Interrupt Status register
uint32_t CHAL_I2CMST_Get_IRQ_Status(void *pPeri)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CMST_Get_IRQ_Status( pPeri));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get I2C SLAVE Interrupt Status register
uint32_t CHAL_I2CSLV_Get_IRQ_Status(void *pPeri)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CSLV_Get_IRQ_Status( pPeri));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get I2C MONITOR Interrupt Status register
uint32_t CHAL_I2CMON_Get_IRQ_Status(void *pPeri)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CMON_Get_IRQ_Status( pPeri));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get I2C Timeout Interrupt Status register
uint32_t CHAL_I2CTimeout_Get_IRQ_Status(void *pPeri)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CTimeout_Get_IRQ_Status( pPeri));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Clear I2C MASTER Interrupt Status register
bool CHAL_I2CMST_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CMST_Clear_IRQ_Status( pPeri, NewValue));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Clear I2C SLAVE Interrupt Status register
bool CHAL_I2CSLV_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CSLV_Clear_IRQ_Status( pPeri, NewValue));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Clear I2C MONITOR Interrupt Status register
bool CHAL_I2CMON_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CMON_Clear_IRQ_Status( pPeri, NewValue));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Clear I2C Timeout Interrupt Status register
bool CHAL_I2CTimeout_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
#if defined (_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
	return(_Chip_I2CTimeout_Clear_IRQ_Status( pPeri, NewValue));
#else
	return(false);
#endif	
}







// ******************************************************************************************************
// UART Functions
// ******************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// Initialization
// result is true:successfull otherwise false
void *CHAL_UART_Init (uint8_t PeriIndex)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	return(_Chip_UART_Init(PeriIndex));
#else
	return(NULL);
#endif
}

// ------------------------------------------------------------------------------------------------------
// Get I2C block input clock rate in kHz
uint32_t CHAL_UART_Get_Clock_Rate(void *pPeri)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	return(_Chip_Clock_Get_UARTClk_Rate( _Chip_UART_Get_PeriIndex(pPeri)));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// UART Complette UART Enable/Disable
// result is true:successfull otherwise false
bool CHAL_UART_Enable (void *pPeri, bool NewState)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	return(_Chip_UART_Enable(pPeri, NewState));
#else
	return(false);
#endif
}

// ------------------------------------------------------------------------------------------------------
// UART Interrupt RX Enable/Disable
inline void CHAL_UART_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	_Chip_UART_Enable_IRQ (pPeri, IRQBitMask, NewState);							// enable/disable UARTs interrupts
#endif	
}

// ------------------------------------------------------------------------------------------------------
// UART Interrupt Enable/Disable n NVIC
bool CHAL_UART_Enable_IRQ_NVIC (void *pPeri, bool NewState)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	return(_Chip_UART_Enable_IRQ_NVIC (pPeri, NewState));							// enable/disable interrupt
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Write char to OutBuff
inline void CHAL_UART_Put_Data(void *pPeri, uint32_t WrData)
{
	_Chip_UART_Put_Data( pPeri, WrData);
}

// ------------------------------------------------------------------------------------------------------
// Read char from InBuff
inline uint32_t CHAL_UART_Get_Data(void *pPeri)
{
	return(_Chip_UART_Get_Data( pPeri));
}

// ------------------------------------------------------------------------------------------------------
// Get UART Peri Status register
uint32_t CHAL_UART_Get_Peri_Status(void *pPeri)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	return(_Chip_UART_Get_Peri_Status( pPeri));
#else
	return(false);
#endif	
}
// ------------------------------------------------------------------------------------------------------
// Clear UART Peri Status register
inline void CHAL_UART_Clear_Peri_Status(void *pPeri, uint32_t IRQBitMask)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	_Chip_UART_Clear_Peri_Status( pPeri, IRQBitMask);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get UART Interrupt Status register
inline uint32_t CHAL_UART_Get_IRQ_Status(void *pPeri)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	return(_Chip_UART_Get_IRQ_Status( pPeri));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Clear UART Interrupt Status register
inline void CHAL_UART_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	_Chip_UART_Clear_IRQ_Status( pPeri, IRQBitMask);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Flush UART Tx
void CHAL_UART_Flush_Tx(void *pPeri)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	_Chip_UART_Flush_Tx( pPeri);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Flush UART Rx
inline void CHAL_UART_Flush_Rx(void *pPeri)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	_Chip_UART_Flush_Rx( pPeri);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Configuration
// DataLen: 7,8,9
// Parity: 0-none, 1-Space, 2-Odd, 3-Even, 4-Mark
// Stopbits: 0-1, 1-2
// result is true:successfull otherwise false
bool CHAL_UART_Set_Conf(void *pPeri, uint32_t BaudRate, uint8_t DataLen, uint8_t Parity, uint8_t StopBits)
{
	bool res = false;
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	res = _Chip_UART_Set_Conf(pPeri, DataLen, Parity, StopBits);					// try to configure
	if (res == true) res = _Chip_UART_Set_Baud(pPeri, BaudRate);					// and write baudrate
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Set new Address
// result is true:successfull otherwise false
bool CHAL_UART_Set_Address(void *pPeri, uint8_t NewAddress)
{
	bool res = false;
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	res = _Chip_UART_Set_Address(pPeri, NewAddress);							// try to set address
#endif
	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Get new Address
// result Address
uint8_t CHAL_UART_Get_Address(void *pPeri)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	return(_Chip_UART_Get_Address(pPeri));											// try to get address
#else
	return(0x00);																	// function not applied
#endif
	
}

// ------------------------------------------------------------------------------------------------------
// Configure RTS Pin
// result is true:successfull otherwise false
bool CHAL_UART_Set_RTS_Conf(void* pPeri, const CHAL_Signal_t *sRTS)
{
	return(_Chip_UART_Set_RTS_Conf(pPeri, sRTS->Std.Port, sRTS->Std.Pin));
}

// ------------------------------------------------------------------------------------------------------
// Configure CTS Pin
// result is true:successfull otherwise false
bool CHAL_UART_Set_CTS_Conf(void* pPeri, const CHAL_Signal_t *sCTS)
{
	return(_Chip_UART_Set_CTS_Conf(pPeri, sCTS->Std.Port, sCTS->Std.Pin));
}

// ------------------------------------------------------------------------------------------------------
// Configure DIR/DE Pin
// result is true:successfull otherwise false
bool CHAL_UART_Set_DIRDE_Conf(void* pPeri, const CHAL_Signal_t *sDIRDE)
{
	return(_Chip_UART_Set_DIRDE_Conf(pPeri, sDIRDE->Std.Port, sDIRDE->Std.Pin, sDIRDE->Std.Act));
}

// ------------------------------------------------------------------------------------------------------
// Configure RE Pin
// result is true:successfull otherwise false
bool CHAL_UART_Set_RE_Conf(void* pPeri, const CHAL_Signal_t *sRE)
{
	return(_Chip_UART_Set_RE_Conf(pPeri, sRE->Std.Port, sRE->Std.Pin, sRE->Std.Act));
}

// ------------------------------------------------------------------------------------------------------
// ADR DETECTION Control
bool CHAL_UART_Set_AdrDet(void* pPeri, bool NewState)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	return(_Chip_UART_Set_AdrDet( pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// UART Get Bus Speed
uint32_t CHAL_UART_Get_Baud (void *pPeri)
{
#if defined (_CHIP_UART_COUNT) && (_CHIP_UART_COUNT > 0)
	return(_Chip_UART_Get_Baud(pPeri));
#else
	return(0);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// UART Interrupt RX Enable/Disable
void CHAL_UARTFIFO_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState)
{
#if defined (_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
	_Chip_UARTFIFO_Enable_IRQ(pPeri, IRQBitMask, NewState);							// enable/disable UARTs interrupts
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get UART FIFO Peri Status register
uint32_t CHAL_UARTFIFO_Get_Peri_Status(void *pPeri)
{
#if defined (_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
	return(_Chip_UARTFIFO_Get_Peri_Status( pPeri));
#else
	return(false);
#endif	
}
// ------------------------------------------------------------------------------------------------------
// Clear UARTFIFO Peri Status register
void CHAL_UARTFIFO_Clear_Peri_Status(void *pPeri, uint32_t IRQBitMask)
{
#if defined (_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
	_Chip_UARTFIFO_Clear_Peri_Status( pPeri, IRQBitMask);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get UART FIFO Interrupt Status register
uint32_t CHAL_UARTFIFO_Get_IRQ_Status(void *pPeri)
{
#if defined (_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
	return(_Chip_UARTFIFO_Get_IRQ_Status( pPeri));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Clear UART FIFO Interrupt Status register
void CHAL_UARTFIFO_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask)
{
#if defined (_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
	_Chip_UARTFIFO_Clear_IRQ_Status( pPeri, IRQBitMask);
#endif	
}
// ------------------------------------------------------------------------------------------------------
// UART Get Actual Tx FIFO threshold Size
uint32_t CHAL_UARTFIFO_Get_TxTHLevel (void *pPeri)
{
#if defined (_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
	return(_Chip_UARTFIFO_Get_TxTHLevel(pPeri));
#else
	return(1);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// UART Get Actual Rx FIFO threshold Size
uint32_t CHAL_UARTFIFO_Get_RxTHLevel (void *pPeri)
{
#if defined (_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
	return(_Chip_UARTFIFO_Get_RxTHLevel(pPeri));
#else
	return(1);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// UART Set New Tx FIFO threshold Size and enable threshold
void CHAL_UARTFIFO_Set_TxTHLevel (void *pPeri, uint32_t NewValue)
{
#if defined (_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
	_Chip_UARTFIFO_Set_TxTHLevel(pPeri, NewValue);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// UART Set New Rx FIFO threshold Size and enable threshold
void CHAL_UARTFIFO_Set_RxTHLevel (void *pPeri, uint32_t NewValue)
{
#if defined (_CHIP_UARTFIFO_COUNT) && (_CHIP_UARTFIFO_COUNT > 0)
	_Chip_UARTFIFO_Set_RxTHLevel(pPeri, NewValue);
#endif	
}










// ******************************************************************************************************
// SPI Functions
// ******************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// Initialization of SPI
// result is pointer to periphery. otherwise NULL
void *CHAL_SPI_Init (uint8_t PeriIndex)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	return(_Chip_SPI_Init(PeriIndex));
#else
	return(NULL);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// SPI Configuration
bool CHAL_SPI_Configure(void *pPeri, uint32_t Speed_Hz, CHAL_SPI_XferMode_t Mode, bool CPHA, bool CPOL)
{
#if defined(_CHIP_HAVE_SPI) && (_CHIP_HAVE_SPI == 1)
	
	if(Mode == CHAL_SPI_Master) return(_Chip_SPI_Configure( pPeri, Speed_Hz, true, false, false, CPHA, CPOL));		// master mode
	if(Mode == CHAL_SPI_Slave) return(_Chip_SPI_Configure( pPeri, Speed_Hz, false, true, false, CPHA, CPOL));		// slave mode
	//if(Mode == CHAL_SPI_Monitor) return(_Chip_SPI_Configure( pPeri, Speed_Hz, false, false, true));	// monitor mode
#endif	
	return(false);
}

// ------------------------------------------------------------------------------------------------------
// SPI - write data into SPI
void CHAL_SPI_Put_Data(void *pPeri, uint32_t NewValue, uint8_t NumOfBits, uint32_t CtrlFlags, uint8_t SSel)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	_Chip_SPI_Put_Data(pPeri, NewValue, NumOfBits, CtrlFlags, SSel);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// SPI - Read data from SPI
uint32_t CHAL_SPI_Get_Data(void *pPeri)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	return(_Chip_SPI_Get_Data(pPeri));
#else
	return(0);
#endif		
}

// ------------------------------------------------------------------------------------------------------
// SPI - Create End Of Transfer condition 
void CHAL_SPI_SendEOT(void *pPeri)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	_Chip_SPI_SendEOT(pPeri);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// SPI - Create End Of Frame condition  
void CHAL_SPI_SendEOF(void *pPeri)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	_Chip_SPI_SendEOF(pPeri);
#endif	
}


// ------------------------------------------------------------------------------------------------------
// SPI - Create Receive Ignore condition 
//void CHAL_SPI_ReceiveIgnore(void *pPeri)
//{
//#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
//	_Chip_SPI_ReceiveIgnore(pPeri);
//#endif	
//}

// ------------------------------------------------------------------------------------------------------
// SPI - Complette SPI Enable/Disable
bool CHAL_SPI_Enable ( void *pPeri, bool NewState)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	return(_Chip_SPI_Enable(pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get Peri Status register
uint32_t CHAL_SPI_Get_Peri_Status(void *pPeri)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	return(_Chip_SPI_Get_Peri_Status( pPeri));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// SPI Complette IRQ Enable/Disable
// result is true:successfull otherwise false
bool CHAL_SPI_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	return(_Chip_SPI_Enable_IRQ_NVIC(pPeri, NewState));
#else
	return(false);
#endif	
	
}

// ------------------------------------------------------------------------------------------------------
// SPI Enable/disable selected interrupts
// result is true:successfull otherwise false
bool CHAL_SPI_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	return(_Chip_SPI_Enable_IRQ(pPeri, IRQBitMask, NewState));
#else
	return(false);
#endif	
	
}

// ------------------------------------------------------------------------------------------------------
// Get SPI Interrupt Status register
uint32_t CHAL_SPI_Get_IRQ_Status(void *pPeri)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	return(_Chip_SPI_Get_IRQ_Status( pPeri));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Clear SPI Interrupt Status register
bool CHAL_SPI_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	return(_Chip_SPI_Clear_IRQ_Status( pPeri, NewValue));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Clear SPI Peripherial Status register
bool CHAL_SPI_Clear_Peri_Status(void *pPeri, uint32_t NewValue)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	return(_Chip_SPI_Clear_Peri_Status( pPeri, NewValue));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// SPI Set Bus Speed
bool CHAL_SPI_Set_BusSpeed ( void *pPeri, uint32_t Speed_Hz)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	return(_Chip_SPI_Set_BusSpeed(pPeri, Speed_Hz));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// SPI Get Bus Speed
uint32_t CHAL_SPI_Get_BusSpeed ( void *pPeri)
{
#if defined (_CHIP_SPI_COUNT) && (_CHIP_SPI_COUNT > 0)
	return(_Chip_SPI_Get_BusSpeed(pPeri));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Flush receive reg
void CHAL_SPI_Flush_Rx(void *pPeri)
{
#if defined (_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
	_Chip_SPI_Flush_Rx(pPeri);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Flush transsmit reg
void CHAL_SPI_Flush_Tx(void *pPeri)
{
#if defined (_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
	_Chip_SPI_Flush_Tx(pPeri);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// FIFO Interrupt RX Enable/Disable
void CHAL_SPIFIFO_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState)
{
#if defined (_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)	
	_Chip_SPIFIFO_Enable_IRQ(pPeri, IRQBitMask, NewState);							// enable/disable peri interrupts
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get FIFO Peri Status register
uint32_t CHAL_SPIFIFO_Get_Peri_Status(void *pPeri)
{
#if defined (_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)	
	return(_Chip_SPIFIFO_Get_Peri_Status( pPeri));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Clear FIFO Peri Status register
void CHAL_SPIFIFO_Clear_Peri_Status(void *pPeri, uint32_t BitMask)
{
#if defined (_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)	
	_Chip_SPIFIFO_Clear_Peri_Status( pPeri, BitMask);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get FIFO Interrupt Status register
uint32_t CHAL_SPIFIFO_Get_IRQ_Status(void *pPeri)
{
#if defined (_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)	
	return(_Chip_SPIFIFO_Get_IRQ_Status( pPeri));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Clear FIFO Interrupt Status register
void CHAL_SPIFIFO_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask)
{
#if defined (_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
	_Chip_SPIFIFO_Clear_IRQ_Status( pPeri, IRQBitMask);
#endif	
}
// ------------------------------------------------------------------------------------------------------
// Get Actual Tx FIFO threshold Size
uint32_t CHAL_SPIFIFO_Get_TxTHLevel (void *pPeri)
{
#if defined (_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
	return(_Chip_SPIFIFO_Get_TxTHLevel(pPeri));
#else
	return(1);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get Actual Rx FIFO threshold Size
uint32_t CHAL_SPIFIFO_Get_RxTHLevel (void *pPeri)
{
#if defined (_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
	return(_Chip_SPIFIFO_Get_RxTHLevel(pPeri));
#else
	return(1);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Set New Tx FIFO threshold Size
void CHAL_SPIFIFO_Set_TxTHLevel (void *pPeri, uint32_t NewValue)
{
#if defined (_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
	_Chip_SPIFIFO_Set_TxTHLevel(pPeri, NewValue);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Set New Rx FIFO threshold Size
void CHAL_SPIFIFO_Set_RxTHLevel (void *pPeri, uint32_t NewValue)
{
#if defined (_CHIP_SPIFIFO_COUNT) && (_CHIP_SPIFIFO_COUNT > 0)
	_Chip_SPIFIFO_Set_RxTHLevel(pPeri, NewValue);
#endif	
}






// ******************************************************************************************************
// EEPROM Functions		(internal)
// ******************************************************************************************************
//static bool CHAL_EEPROM_Emulated;																		// for local purposes only


// ------------------------------------------------------------------------------------------------------
// Internal EEPROM Init
void *CHAL_IEEPROM_Init (uint8_t PeriIndex)
{
#if defined(_CHIP_IEEPROM)
	return(_Chip_EEPROM_Init(PeriIndex));
#elif defined (_CHIP_EMUL_EEPROM_SIZE)
	return(_Chip_Emulated_EEPROM_Init(PeriIndex));
#else	
	return(NULL);	
#endif	
}

// ------------------------------------------------------------------------------------------------------
// EEPROM Complette Enable/Disable
bool CHAL_IEEPROM_Enable (void *pPeri, bool NewState)
{
#if defined(_CHIP_IEEPROM)
	return(_Chip_EEPROM_Enable(pPeri, NewState));
#elif defined (_CHIP_EMUL_EEPROM_SIZE)
	return(_Chip_Emulated_EEPROM_Enable(pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// EEPROM Power On/Off
bool CHAL_IEEPROM_Power (void *pPeri, bool NewState)
{
#if defined(_CHIP_IEEPROM)
	return(_Chip_EEPROM_Power(pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// EEPROM Write data
bool CHAL_IEEPROM_Write(void *pPeri, uint32_t EEDst, uint8_t *pInBuff, size_t byteswrt)
{
#if defined(_CHIP_IEEPROM)
	return( _Chip_EEPROM_Write(pPeri, EEDst, pInBuff, byteswrt));
#elif defined (_CHIP_EMUL_EEPROM_SIZE)	
	return( _Chip_Emulated_EEPROM_Write(pPeri, EEDst, pInBuff, byteswrt));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// EEPROM Read data
bool CHAL_IEEPROM_Read(void *pPeri, uint32_t EESrc, uint8_t *pOutBuff, size_t bytesrd)
{
#if defined(_CHIP_IEEPROM)
	return(_Chip_EEPROM_Read(pPeri, EESrc, pOutBuff, bytesrd));
#elif defined (_CHIP_EMUL_EEPROM_SIZE)		
	return(_Chip_Emulated_EEPROM_Read(pPeri, EESrc, pOutBuff, bytesrd));
#else
	return(false);
#endif
}













// ******************************************************************************************************
// USB Functions
// ******************************************************************************************************
#if defined (_CHIP_USBD_COUNT) && (_CHIP_USBD_COUNT > 0)							// if chip have Device USB
 #define _CHIP_USB_COUNT 		_CHIP_USBD_COUNT									// define it
#endif

#if defined (_CHIP_USBH_COUNT) && (_CHIP_USBH_COUNT > 0)							// if chip have Host USB
 #define _CHIP_USB_COUNT 		_CHIP_USBH_COUNT									// define it
#endif

//#if defined (_CHIP_USB_COUNT) && (_CHIP_USB_COUNT > 0)
//typedef enum
//{
//	CHAL_USB_STAT_OK					= 0x0000,
//	CHAL_USB_STAT_ERROR					= 0x8000,
//	CHAL_USB_STAT_BUSY					= 0x0001,									// in progress. wait for ~BUSY
//	CHAL_USB_STAT_BUFFFULL				= 0x0002,
//	CHAL_USB_STAT_BUFFNONE				= 0x0004,	
//	CHAL_USB_STAT_TIMEOUT				= 0x0040,
//} CHAL_USB_Xfr_Status_t;

//typedef struct
//{
//	const uint8_t 						*pTxBuff;									// Pointer to array of bytes to be transmitted
//	uint8_t 							*pRxBuff;									// Pointer memory where bytes received from I2C be stored
//	uint16_t 							TxSize;										// Number of bytes in transmit array, if 0 only receive transfer will be carried on
//	uint16_t 							RxBuffSize;									// Number of bytes for received bytes, if 0 only transmission we be carried on. After received, value is decrement
//	volatile CHAL_USB_Xfr_Status_t		Status;										// Status of the current I2C transfer
//} CHAL_USB_Xfer_t;
//#endif

// ------------------------------------------------------------------------------------------------------
// Initialization of Port and Pin
// result is true:successfull otherwise false
void *CHAL_USB_Init ( uint8_t PeriIndex )
{
#if defined (_CHIP_USB_COUNT) && (_CHIP_USB_COUNT > 0)
	return(_Chip_USB_Init(PeriIndex));
#else
	return(NULL);
#endif	
}

// ------------------------------------------------------------------------------------------------
// Power down one or more blocks or peripherals
bool CHAL_USB_Power(void *pPeri, bool NewState)
{
#if defined (_CHIP_USB_COUNT) && (_CHIP_USB_COUNT > 0)
	return(_Chip_USB_Power(pPeri, NewState));
#else
	return(NULL);
#endif
}
// ------------------------------------------------------------------------------------------------------
// Configuration
bool CHAL_USB_Configure(void *pPeri, CHAL_USB_Mode_t Mode)
{
#if defined (_CHIP_USB_COUNT) && (_CHIP_USB_COUNT > 0)
	if(Mode == CHAL_USB_DEVICE) return(_Chip_USB_Configure( pPeri, true, false, false));// master mode
	if(Mode == CHAL_USB_HOST) return(_Chip_USB_Configure( pPeri, false, true, false));	// slave mode
	if(Mode == CHAL_USB_OTG) return(_Chip_USB_Configure( pPeri, false, false, true));	// monitor mode
#endif	
	return(false);
}

// ------------------------------------------------------------------------------------------------------
// Get USB Interrupt Status rtegister
uint32_t CHAL_USB_Get_IRQ_Status(void *pPeri)
{
#if defined (_CHIP_USB_COUNT) && (_CHIP_USB_COUNT > 0)
	return(_Chip_USB_Get_IRQStatus( pPeri));
#else
	return(0);
#endif	
}









// ******************************************************************************************************
// ADC Functions
// ******************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// Initialization
// result is true:successfull otherwise false
void *CHAL_ADC_Init ( uint8_t PeriIndex)
{
#if defined (_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
	return(_Chip_ADC_Init(PeriIndex));
#else
	return(NULL);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get actual ADC Value for selected channel
// result is readed conversion value
uint32_t CHAL_ADC_Get_Data_CH( void *pPeri, uint8_t Channel)
{
#if defined(_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
	return(_Chip_ADC_Get_Data_CH( pPeri, Channel));									// read a reload value
#else
	return(0);
#endif

}

// ------------------------------------------------------------------------------------------------------
// Get actual ADC Value Validity flag for selected channel
// result is readed conversion value
bool CHAL_ADC_Get_DataValidFlag_CH( void *pPeri, uint8_t Channel)
{
#if defined(_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
	return(_Chip_ADC_Get_DataValidFlag_CH( pPeri, Channel));									// read a reload value
#else
	return(0);
#endif

}


// ------------------------------------------------------------------------------------------------------
// ADC Power on / off
bool CHAL_ADC_Power ( void *pPeri, bool NewState)
{
#if defined (_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
	return(_Chip_ADC_Power(pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// ADC Start
void CHAL_ADC_Start_CH ( void *pPeri, uint32_t ChannelBitMask)
{
#if defined (_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
	_Chip_ADC_Start_CH(pPeri, ChannelBitMask);
#else
	__nop();
#endif	
}

// ------------------------------------------------------------------------------------------------------
// ADC Complette UART Enable/Disable
bool CHAL_ADC_Enable_CH ( void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
#if defined (_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
	return(_Chip_ADC_Enable_CH(pPeri, ChannelBitMask, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Configuration of selected channel
bool CHAL_ADC_Configure_CH(void *pPeri, uint32_t ChannelBitMask, uint32_t Speed_kHz, bool RepeatMode, bool DMAMode)
{
#if defined (_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
	return(_Chip_ADC_Configure_CH( pPeri, ChannelBitMask, Speed_kHz, RepeatMode, DMAMode));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Check for active channel on ADC[num]
// read from pin configuration registers if analog function is active for each pin.
// result is num of ADC and mask of active channels for each ADC
uint32_t CHAL_ADC_Read_Active_Channels(void* pPeri)
{
#if defined (_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
	return(_Chip_ADC_Get_ChannelMask(pPeri));
#else
	return(0);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// ADC Complette NVIC IRQ Enable/Disable
// result is true:successfull otherwise false
bool CHAL_ADC_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
#if defined (_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
	return(_Chip_ADC_Enable_IRQ_NVIC(pPeri, NewState));
#else
	return(false);
#endif	
	
}

//void CHAL_ADC_Clear_IRQFlags(void *pPeri, uint32_t ClearMask)
//{
//#if defined (_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)	
//	_Chip_ADC_Clear_IRQFlags(pPeri, ClearMask);
//#endif	
//}


//uint32_t CHAL_ADC_Get_IRQFlags(void *pPeri)
//{
//#if defined (_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)	
//	return(_Chip_ADC_Get_IRQFlags(pPeri));
//#else
//	return(0);
//#endif	
//}

// ------------------------------------------------------------------------------------------------------
// ADC Ena/Disa Channel Interrupt for selected ChannelBitMask channels
bool CHAL_ADC_Enable_IRQ_Peri(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
#if defined (_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
	return(_Chip_ADC_IRQ_Peri_Enable_CH(pPeri, IRQBitMask, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// ADC Get Channel Interrupt state
// result is channel interrupt state for selected channel
uint32_t CHAL_ADC_Get_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask)
{
#if defined (_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
	return(_Chip_ADC_Get_IRQ_Status_CH(pPeri, ChannelBitMask));
#else
	return(0);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// ADC Clear Channel Interrupt state for selected ChannelBitMask channels
// result is true:successfull otherwise false
bool CHAL_ADC_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask)
{
#if defined (_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
	return(_Chip_ADC_Clear_IRQ_Status_CH(pPeri, ChannelBitMask));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Enable Read temperature from internal sensor
bool CHAL_TEMP_Enable(bool NewState)
{
#if defined (_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
	return(_Chip_Temp_Enable(NewState));											// enable/disable temperature sensor.
#else
	return(false);
#endif	
}









// ******************************************************************************************************
// TIMER Functions 
// ******************************************************************************************************
// Initialization
// result is true:successfull otherwise false
void *CHAL_Timer_Init (uint8_t PeriIndex)
{
#if (defined(_CHIP_TIMER_COUNT) || defined(_CHIP_CTIMER_COUNT)) && ((_CHIP_TIMER_COUNT > 0) |(_CHIP_CTIMER_COUNT > 0))
	return(_Chip_Timer_Init(PeriIndex));
#else
	return(NULL);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Timer Complette Enable/Disable
// result is true:successfull otherwise false
bool CHAL_Timer_Enable_CH (void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
#if (defined(_CHIP_TIMER_COUNT) || defined(_CHIP_CTIMER_COUNT)) && ((_CHIP_TIMER_COUNT > 0) |(_CHIP_CTIMER_COUNT > 0))
	return(_Chip_Timer_Enable_CH(pPeri, ChannelBitMask, NewState));
#else
	return(false);
#endif
}

// ------------------------------------------------------------------------------------------------------
// Timer IRQ Enable/Disable
// result is true:successfull otherwise false
bool CHAL_Timer_Enable_IRQ_NVIC (void *pPeri, bool NewState)
{
#if (defined(_CHIP_TIMER_COUNT) || defined(_CHIP_CTIMER_COUNT)) && ((_CHIP_TIMER_COUNT > 0) |(_CHIP_CTIMER_COUNT > 0))
	return(_Chip_Timer_Enable_IRQ_NVIC(pPeri, NewState));
#else
	return(false);
#endif
}	

// ------------------------------------------------------------------------------------------------------
// Configuration
// RateHz: uint32_t
// Mode: 0-1, 1-2
// result is true:successfull otherwise false
bool CHAL_Timer_Configure(void *pPeri, uint8_t Channel, uint32_t RateHz, CHAL_Timer_Mode_t Mode)
{
#if (defined(_CHIP_TIMER_COUNT) || defined(_CHIP_CTIMER_COUNT)) && ((_CHIP_TIMER_COUNT > 0) |(_CHIP_CTIMER_COUNT > 0))
	if(Mode == CHAL_Timer_Repeat)
		return(_Chip_Timer_Configure(pPeri, Channel, RateHz, true));				// try to configure as main timer with reload
	if(Mode == CHAL_Timer_PWMReload) 
		return(_Chip_Timer_Configure(pPeri, Channel, RateHz, true));				// try to configure as main PWM base reload timer
	
	if(Mode == CHAL_Timer_PWMOut) 
		return(_Chip_Timer_Configure(pPeri, Channel, RateHz, true));				// try to configure as PWM Out match timer

	if(Mode == CHAL_Timer_OneShot) 
		return(_Chip_Timer_Configure(pPeri, Channel, RateHz, false));				// try to configure as One shot timer
	
	return(_Chip_Timer_Configure(pPeri, Channel, RateHz, false));					// CHAL_Timer_Repeat: try to configure as repeat timer
#else
	return(false);
#endif
}

//// ------------------------------------------------------------------------------------------------------
//// Start
//void CHAL_Timer_Run(void *pPeri, uint32_t ChannelBitMask, bool NewState)
//{
//#if (defined(_CHIP_TIMER_COUNT) || defined(_CHIP_CTIMER_COUNT)) && ((_CHIP_TIMER_COUNT > 0) |(_CHIP_CTIMER_COUNT > 0))
//	_Chip_Timer_Run(pPeri, ChannelBitMask, NewState);								// Start timer. Timer must be configured already.
//#endif	
//}
	
//// ------------------------------------------------------------------------------------------------------
//// Function select
//// Functione: 0-1, 1-2
//// result is true:successfull otherwise false
//bool CHAL_Timer_Function(void *pPeri, uint8_t Channel)
//{
//#if (defined(_CHIP_TIMER_COUNT) || defined(_CHIP_CTIMER_COUNT)) && ((_CHIP_TIMER_COUNT > 0) |(_CHIP_CTIMER_COUNT > 0))
//	return(_Chip_Timer_Function(pPeri, Channel));									// try to function select
//#else
//	return(false);
//#endif
//}


// ------------------------------------------------------------------------------------------------------
// Timer Ena/Disa Channel Interrupt for selected BitMasked channels
bool CHAL_Timer_IRQ_Peri_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
#if (defined(_CHIP_TIMER_COUNT) || defined(_CHIP_CTIMER_COUNT)) && ((_CHIP_TIMER_COUNT > 0) |(_CHIP_CTIMER_COUNT > 0))
	return(_Chip_Timer_IRQ_Peri_Enable(pPeri, IRQBitMask, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Timer Get Channel Interrupt state for selected BitMasked channels
// result is channel interrupt state for selected channel
uint32_t CHAL_Timer_Get_CHIRQ_Status(void *pPeri)
{
#if (defined(_CHIP_TIMER_COUNT) || defined(_CHIP_CTIMER_COUNT)) && ((_CHIP_TIMER_COUNT > 0) |(_CHIP_CTIMER_COUNT > 0))
	return(_Chip_Timer_Get_CHIRQ_Status(pPeri));
#else
	return(0);
#endif	
}


// ------------------------------------------------------------------------------------------------------
// Timer Clear Channel Interrupt state for selected BitMasked channels
// result is true:successfull otherwise false
bool CHAL_Timer_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask)
{
#if (defined(_CHIP_HAVE_TIMER) || defined(_CHIP_HAVE_CTIMER)) && ((_CHIP_HAVE_TIMER == 1) |(_CHIP_HAVE_CTIMER == 1))
	return(_Chip_Timer_Clear_IRQ_Status_CH(pPeri, ChannelBitMask));
#else
	return(false);
#endif	
}










// ******************************************************************************************************
// PWM Functions
// ******************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// PWM Init
void *CHAL_PWM_Init (uint8_t PeriIndex)
{
#if defined(_CHIP_PWM_COUNT) && (_CHIP_PWM_COUNT > 0)
	return(_Chip_PWM_Init(PeriIndex));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// PWM Interrupt Enable/Disable
bool CHAL_PWM_Enable_IRQ_NVIC (void *pPeri, bool NewState)
{
#if defined(_CHIP_PWM_COUNT) && (_CHIP_PWM_COUNT > 0)
	return(_Chip_PWM_Enable_IRQ_NVIC (pPeri, NewState));								// enable/disable interrupt
#else
	return(false);
#endif	
}










// ******************************************************************************************************
// DMA Functions
// ******************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// Initialization
// result is true:successfull otherwise false
void *CHAL_DMA_Init (uint8_t PeriIndex)
{
#if defined(_CHIP_DMA_COUNT) && (_CHIP_DMA_COUNT > 0)
	return(_Chip_DMA_Init(PeriIndex));
#else
	return(NULL);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// DMA Complette Enable/Disable for Selected Channels
// result is true:successfull otherwise false
bool CHAL_DMA_Enable_CH (void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
#if defined(_CHIP_DMA_COUNT) && (_CHIP_DMA_COUNT > 0)
	return(_Chip_DMA_Enable_CH(pPeri, ChannelBitMask, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// DMA Channel Configuration
// CHAL_DMA_Xfer_t - structure with xfer parameters
// result is true:successfull otherwise false
bool CHAL_DMA_Configure(void *pPeri, uint8_t Channel, CHAL_DMA_Xfer_t *Xfer)
{
#if defined(_CHIP_DMA_COUNT) && (_CHIP_DMA_COUNT > 0)
	return(_Chip_DMA_Configure(pPeri, Channel, Xfer));								// try to configure
#else
	return(false);
#endif
}

// ------------------------------------------------------------------------------------------------------
// DMA Get Channel Interrupt state for selected BitMasked channels
// result is true:successfull otherwise false
uint32_t CHAL_DMA_Get_CHIRQ_Status (void *pPeri)
{
#if defined(_CHIP_DMA_COUNT) && (_CHIP_DMA_COUNT > 0)	
	return(_Chip_DMA_Get_CHIRQ_Status(pPeri));
#else
	return(false);
#endif	
}


// ------------------------------------------------------------------------------------------------------
// DMA - Get channel control word
uint32_t CHAL_DMA_Get_CHControl (void *pPeri, uint32_t Channel)
{
#if defined(_CHIP_DMA_COUNT) && (_CHIP_DMA_COUNT > 0)	
	return(_Chip_DMA_Get_CHControl(pPeri, Channel));
#else
	return(false);
#endif	
}
	

// ------------------------------------------------------------------------------------------------------
// DMA Clear Channel Interrupt state for selected BitMasked channels
// result is true:successfull otherwise false
bool CHAL_DMA_Clear_IRQ_Status_CH (void *pPeri, uint8_t ChannelNum)
{
#if defined(_CHIP_DMA_COUNT) && (_CHIP_DMA_COUNT > 0)	
	return(_Chip_DMA_Clear_IRQ_Status_CH(pPeri, ChannelNum));
#else
	return(false);
#endif	
}











// ******************************************************************************************************
// SCT Functions
// ******************************************************************************************************
void *CHAL_SCT_Init (uint8_t PeriIndex)
{
#if defined(_CHIP_SCT_COUNT) && (_CHIP_SCT_COUNT > 0)
	return(_Chip_SCT_Init(PeriIndex));
#else
	return(NULL);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Complette Enable/Disable
// result is true:successfull otherwise false
bool CHAL_SCT_Enable (void *pPeri, bool NewState)
{
#if defined(_CHIP_SCT_COUNT) && (_CHIP_SCT_COUNT > 0)
	return(_Chip_SCT_Enable(pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Complette Configuration
bool CHAL_SCT_Configure (void *pPeri)
{
#if defined(_CHIP_SCT_COUNT) && (_CHIP_SCT_COUNT > 0)
	return(_Chip_SCT_Configure(pPeri));
#else
	return(false);
#endif	
}









// ******************************************************************************************************
// ETH Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Initialization of Port and Pin
// result is true:successfull otherwise false
void *CHAL_ETH_Init ( uint8_t PeriIndex )
{
#if defined(_CHIP_ETH_COUNT) && (_CHIP_ETH_COUNT > 0)
	return(_Chip_ETH_Init(PeriIndex));
#else
	return(NULL);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Complette  Enable/Disable
bool CHAL_ETH_Enable ( void *pPeri, bool NewState)
{
#if defined(_CHIP_ETH_COUNT) && (_CHIP_ETH_COUNT > 0)
	return(_Chip_ETH_Enable(pPeri, NewState));
#else
	return(false);
#endif	
}


// ------------------------------------------------------------------------------------------------------
// Configuration
//extern bool CHAL_ETH_Configure(void *pPeri, CHAL_ETH_Mode_t Mode)
//{
//#if defined(_CHIP_ETH_COUNT) && (_CHIP_ETH_COUNT > 0)
//	if(Mode == CHAL_USB_DEVICE) return(_Chip_USB_Configure( pPeri, true, false, false));// master mode
//	if(Mode == CHAL_USB_HOST) return(_Chip_USB_Configure( pPeri, false, true, false));	// slave mode
//	if(Mode == CHAL_USB_OTG) return(_Chip_USB_Configure( pPeri, false, false, true));	// monitor mode
//#endif	
//	return(false);
//}









// ******************************************************************************************************
// LCD Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Initialization of Port and Pin
// result is true:successfull otherwise false
void *CHAL_LCD_Init ( uint8_t PeriIndex )
{
#if defined(_CHIP_LCD_COUNT) && (_CHIP_LCD_COUNT > 0)
	return(_Chip_LCD_Init(PeriIndex));
#else
	return(NULL);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Complette  Enable/Disable
bool CHAL_LCD_Enable ( void *pPeri, bool NewState)
{
#if defined(_CHIP_LCD_COUNT) && (_CHIP_LCD_COUNT > 0)
	return(_Chip_LCD_Enable(pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Basic Configuration
bool CHAL_LCD_Configure (void *pPeri)
{
#if defined(_CHIP_LCD_COUNT) && (_CHIP_LCD_COUNT > 0)
	return(_Chip_LCD_Configure(pPeri));
#else
	return(false);
#endif	
}


// ------------------------------------------------------------------------------------------------------
// Enable/disable selected interrupts
// result is true:successfull otherwise false
bool CHAL_LCD_IRQ_Peri_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
#if defined(_CHIP_LCD_COUNT) && (_CHIP_LCD_COUNT > 0)
	return(_Chip_LCD_IRQ_Peri_Enable(pPeri, IRQBitMask, NewState));
#else
	return(false);
#endif	
} 

// ------------------------------------------------------------------------------------------------------
// Clear interrupt status
void CHAL_LCD_Clear_IRQ_Status(void *pPeri, uint32_t ClearMask)
{
#if defined(_CHIP_LCD_COUNT) && (_CHIP_LCD_COUNT > 0)
	_Chip_LCD_Clear_IRQ_Status(pPeri, ClearMask);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get interrupt status
uint32_t CHAL_LCD_Get_IRQ_Status(void *pPeri)
{
#if defined(_CHIP_LCD_COUNT) && (_CHIP_LCD_COUNT > 0)
	return(_Chip_LCD_Get_IRQ_Status(pPeri));
#else
	return(0);
#endif	
}












// ******************************************************************************************************
// DAC Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Initialization
// result is true:successfull otherwise false
void *CHAL_DAC_Init (uint8_t PeriIndex)
{
#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 0)
	return(_Chip_DAC_Init(PeriIndex));
#else
	return(NULL);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// DAC Complette DAC Enable/Disable
// result is true:successfull otherwise false
bool CHAL_DAC_Enable (void *pPeri, bool NewState)
{
#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 0)
	return(_Chip_DAC_Enable(pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// DAC Interrupt Enable/Disable
void CHAL_DAC_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState)
{
#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 0)
	_Chip_DAC_Enable_IRQ (pPeri, IRQBitMask, NewState);							// enable/disable DACs interrupts
#endif	
}

// ------------------------------------------------------------------------------------------------------
// DAC Interrupt Enable/Disable n NVIC
bool CHAL_DAC_Enable_IRQ_NVIC (void *pPeri, bool NewState)
{
#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 0)
	return(_Chip_DAC_Enable_IRQ_NVIC (pPeri, NewState));							// enable/disable interrupt
#else
	return(false);
#endif	
}


// ------------------------------------------------------------------------------------------------------
// Write data to DAC
void CHAL_DAC_Put_Data(void *pPeri, uint32_t WrData)
{
#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 0)
	_Chip_DAC_Put_Data( pPeri, WrData);
#endif	
}


// ------------------------------------------------------------------------------------------------------
// Get DAC Peri Status register
uint32_t CHAL_DAC_Get_Peri_Status(void *pPeri)
{
#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 0)
	return(_Chip_DAC_Get_Peri_Status( pPeri));
#else
	return(false);
#endif	
}
// ------------------------------------------------------------------------------------------------------
// Clear DAC Peri Status register
void CHAL_DAC_Clear_Peri_Status(void *pPeri, uint32_t IRQBitMask)
{
#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 0)
	_Chip_DAC_Clear_Peri_Status( pPeri, IRQBitMask);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get DAC Interrupt Status register
uint32_t CHAL_DAC_Get_IRQ_Status(void *pPeri)
{
#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 0)	
	return(_Chip_DAC_Get_IRQ_Status( pPeri));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Clear DAC Interrupt Status register
void CHAL_DAC_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask)
{
#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 0)
	_Chip_DAC_Clear_IRQ_Status( pPeri, IRQBitMask);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Configuration
// result is true:successfull otherwise false
bool CHAL_DAC_Configure(void *pPeri)
{
	bool res = false;
#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 0)
	res = _Chip_DAC_Configure(pPeri);					// try to configure
#endif	
	return(res);
}















// ******************************************************************************************************
// WDT Functions
// ******************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// Initialization of WatchDog
// result is true:successfull otherwise false
void *CHAL_WDT_Init ( uint8_t PeriIndex )
{
#if defined(_CHIP_HAVE_WDT) && (_CHIP_HAVE_WDT == 1)
	return(_Chip_WDT_Init(PeriIndex));
#else
	return(NULL);
#endif	
}

bool CHAL_WDT_Configure(void *pPeri, uint32_t WinMinVal, uint32_t WinMaxVal, uint32_t WarningVal, bool HardRst)
{
#if defined(_CHIP_HAVE_WDT) && (_CHIP_HAVE_WDT == 1)
	return(_Chip_WDT_Configure( pPeri, WinMinVal, WinMaxVal, WarningVal, HardRst));
#else
	return(false);
#endif		
}

// ------------------------------------------------------------------------------------------------------
// Complette  Enable/Disable
bool CHAL_WDT_Enable ( void *pPeri, bool NewState)
{
#if defined(_CHIP_WDT_COUNT) && (_CHIP_WDT_COUNT > 0)
	return(_Chip_WDT_Enable(pPeri, NewState));
#else
	return(false);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Complette  Enable/Disable
void CHAL_WDT_Reset (void *pPeri)
{
#if defined(_CHIP_WDT_COUNT) && (_CHIP_WDT_COUNT > 0)
	_Chip_WDT_Reset(pPeri);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Get interrupt status
uint32_t CHAL_WDT_Get_IRQ_Status(void *pPeri)
{
#if defined(_CHIP_WDT_COUNT) && (_CHIP_WDT_COUNT > 0)
	return(_Chip_WDT_GET_IRQStatus(pPeri));
#else
	return(0);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Clear interrupt status
void CHAL_WDT_Clear_IRQ_Status(void *pPeri, uint32_t ClearMask)
{
#if defined(_CHIP_WDT_COUNT) && (_CHIP_WDT_COUNT > 0)
	_Chip_WDT_Clear_IRQStatus(pPeri, ClearMask);
#endif	
}










// ******************************************************************************************************
// SWM Functions
// ******************************************************************************************************





// ******************************************************************************************************
// MRT Functions - MRTx
// ******************************************************************************************************

// Initialization
// result is true:successfull otherwise false
void *CHAL_MRT_Init (uint8_t PeriIndex)
{
#if defined(_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 0)
	return(_Chip_MRT_Init(PeriIndex));
#else
	return(NULL);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// MRT Complette Enable/Disable
// result is true:successfull otherwise false
bool CHAL_MRT_Enable_CH (void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
#if defined(_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 0)
	return(_Chip_MRT_Enable_CH(pPeri, ChannelBitMask, NewState));
#else
	return(false);
#endif
}


// ------------------------------------------------------------------------------------------------------
// Configuration
// RateHz: uint32_t
// Mode: 0-1, 1-2
// result is true:successfull otherwise false
bool CHAL_MRT_Configure(void *pPeri, uint8_t Channel, uint32_t RateHz, CHAL_Timer_Mode_t Mode)
{
#if defined(_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 0)
	if(Mode == CHAL_Timer_Repeat) return(_Chip_MRT_Configure(pPeri, Channel, RateHz, true));// try to configure
	else return(_Chip_MRT_Configure(pPeri, Channel, RateHz, false));						// try to configure
#else
	return(false);
#endif
}

// ------------------------------------------------------------------------------------------------------
// Reet timer ReloadValue
// result is nothing 
void CHAL_MRT_Reset_Tmr_CH(void *pPeri, uint32_t ChannelBitMask)
{
#if defined(_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 0)
	_Chip_MRT_Reset_Tmr_CH(pPeri, ChannelBitMask);											// activate reload timer value from reload reg
#else
	return;
#endif
}


// ------------------------------------------------------------------------------------------------------
// Set a new timer ReloadValue
// result is true:successfull otherwise false
void CHAL_MRT_Set_TmrVal(void *pPeri, uint8_t Channel, uint32_t TmrValue)
{
#if defined(_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 0)
	_Chip_MRT_Set_TmrVal(pPeri, Channel, TmrValue);									// try to write a new value
#else
	return;
#endif
}

// ------------------------------------------------------------------------------------------------------
// Get actual timer ReloadValue
// result is readed timer reload value
uint32_t CHAL_MRT_Get_TmrVal(void *pPeri, uint8_t Channel)
{
#if defined(_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 0)
	return(_Chip_MRT_Get_TmrVal(pPeri, Channel));									// read a reload value
#else
	return(0);
#endif
}

// ------------------------------------------------------------------------------------------------------
// MRT Get Channel Interrupt state for selected BitMasked channels
// result is channel interrupt state for selected channel
uint32_t CHAL_MRT_Get_IRQ_Status_CH(void *pPeri)
{
#if defined(_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 0)
	return(_Chip_MRT_Get_IRQ_Status_CH(pPeri));
#else
	return(false);
#endif	
}


// ------------------------------------------------------------------------------------------------------
// MRT Clear Channel Interrupt state for selected BitMasked channels
// result is true:successfull otherwise false
bool CHAL_MRT_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask)
{
#if defined(_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 0)
	return(_Chip_MRT_Clear_IRQ_Status_CH(pPeri, ChannelBitMask));
#else
	return(false);
#endif	
}


// ------------------------------------------------------------------------------------------------------
// UART Interrupt RX Enable/Disable
inline void CHAL_MRT_Enable_IRQ_CH (void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
#if defined (_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 0)
	_Chip_MRT_Enable_IRQ_CH (pPeri, ChannelBitMask, NewState);							// enable/disable UARTs interrupts
#endif	
}

// ------------------------------------------------------------------------------------------------------
// UART Interrupt Enable/Disable n NVIC
bool CHAL_MRT_Enable_IRQ_NVIC (void *pPeri, bool NewState)
{
#if defined (_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 0)
	return(_Chip_MRT_Enable_IRQ_NVIC (pPeri, NewState));							// enable/disable interrupt
#else
	return(false);
#endif	
}

