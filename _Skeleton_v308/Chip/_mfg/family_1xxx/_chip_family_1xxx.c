// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _chip_family_1xxx.c
// 	   Version: 1.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: MCU xxx Chip description/configuration file. Same for all MCUs of this family.
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//


// void *_Chip_XXX_Init(int32_t PeriIndex)
// bool _Chip_XXX_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState)
// void _Chip_XXX_Enable_IRQ_NVIC(void *pPeri, bool NewState)

// static inline  	int32_t 	_Chip_XXX_Get_Index(void *pPeri)
// static inline 	void 		*_Chip_XXX_Get_Ptr(uint8_t PeriIndex)
// extern inline 	uint32_t 	_Chip_XXX_Get_Status(void *pPeri)	
// extern inline 	bool 		_Chip_XXX_Clear_Status(void *pPeri, uint32_t NewValue)
// extern inline	void 		_Chip_XXX_Put_Data(void *pPeri, uint32_t WrData)
// extern inline 	uint32_t 	_Chip_XXX_Get_Data(void *pPeri)
// extern inline 	void 		_Chip_XXX_Flush(void *pPeri)

// extern inline 	bool 		_Chip_XXX_Enable_INT(void *pPeri, uint32_t IRQMask, bool NewState)
// extern inline 	uint32_t 	_Chip_XXX_Get_Status_INT(void *pPeri)
// extern inline 	bool 		_Chip_XXX_Clear_Status_INT(void *pPeri,uint32_t IRQBitMask)



//// ************** MUSTER ******************

//// ------------------------------------------------------------------------------------------------
//// return ptr to periphary
//void *_Chip_XXX_Init(int32_t PeriIndex)
//{
//	// Check:
//	SYS_ASSERT(pPeri != NULL);														// Check

//	_CHIP_XXX_T *pPeri;																// create local ptr to peri

//	switch (PeriIndex)																// Get peri based on Index
//	{
//		.... etc
//		case 1: pPeri = XXX1; break;
//		case 0: pPeri = XXX0; break;
//		default: pPeri = NULL;
//	}
//	
//	
//	// Power:
//	SYSCON->PDRUNCFG |= SYSCON_PDRUNCFG_PDEN_xxx(1);								// Enable system clock for xxx
//	
//	// Clock:
//	SYSCON->AHBCLKCTRLn |= SYSCON_AHBCLKCTRL_xxx(1);								// Enable system clock for xxx
//	
//	// Reset:
//	SYSCON->PRESETCTRLn |= SYSCON_PRESETCTRL_xxx_RST(1);__nop();					// Activate reset xxx
//	
//	// Clear IRQ:
//	NVIC_ClearPendingIRQ(xxx_IRQn);													// Clear pending interrupt flag

//	// Enable IRQ:
//	NVIC_EnableIRQ(xxx_IRQn);														// Enable interrupt flag

//	// some other init code:

//	
//	return(pPeri);																	// return pointer to perihery
//}


//// ------------------------------------------------------------------------------------------------------
//// XXX - Enable/Disable periphery
//// if Channel is UINT32_MAX, enable while periphery without channels, otherwise ena/disa selected channel
//// result is true:successfull otherwise false
//bool _Chip_XXX_Enable(void *pPeri, uint32_t ChannelBitMask, bool NewState)
//{
//	SYS_ASSERT( pPeri != NULL);														// Check
//	int32_t PeriIndex = _Chip_XXX_Get_PeriIndex(pPeri);								// Return Index based on ptr

//	if(NewState == true)
//	{
//		// some bitwise enable code ...
//		
//		((_CHIP_XXX_T *)pPeri)->CFG |= XXX_CFG_ENABLE_MASK;							// Enable XXX

//		// Enable peripherial interrupt (Non-NVIC):
//		_Chip_XXX_Enable_IRQ_Peri(pPeri, XXX_TX | XXX_RX, true);							// Enable RX and Rx XXX
//	}
//	else
//	{
//		// some bitwise disable code

//		((_CHIP_XXX_T *)pPeri)->CFG &= ~XXX_CFG_ENABLE_MASK;							// Disable XXX
//		
//		// Disable peripherial interrupt (Non-NVIC):
//		_Chip_XXX_Enable_IRQ(pPeri, XXX_TX | XXX_RX, false);						// Disable RX and Rx XXX
//	}
//	
//	return(true);
//}

//// ------------------------------------------------------------------------------------------------
//// XXX NVIC Interrupt - enable/disable
//void _Chip_XXX_Enable_IRQ_NVIC(void *pPeri, bool NewState)
//{
//	SYS_ASSERT( pPeri != NULL);														// Check
//	
//	int32_t PeriIndex = _Chip_XXX_Get_PeriIndex(pPeri);								// Return Index based on ptr
//	
//	if(NewState == true)
//	{
//		// Enable peripherial interrupt (Non-NVIC):
//		NVIC_ClearPendingIRQ (XXX_IRQn);											// Clear NVIC interrupt flag
//		NVIC_EnableIRQ(XXX_IRQn);													// Enable NVIC interrupt
//	}
//	else
//	{
//		NVIC_DisableIRQ(XXX_IRQn);													// Disable NVIC interrupt
//	}
//}
