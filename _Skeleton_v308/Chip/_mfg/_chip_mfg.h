// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _chip_mfg.h
// 	   Version: x.y
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: MCU MFGxxxx Chip selection file. 
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#ifndef __CHIP_MFG_H_
#define __CHIP_MFG_H_
                  
#if defined  ( CONF_CHIP_ID_MFGdevicecodename1 )                                // MFGdevicecodename1: unique chip id codename. ex: LPC810M021FN8 
	#include "family_1xxx\_chip_family_1xxx.h"                                  // family_1xxx: Chip Family prefix. ex: LPC8xx
#elif defined( CONF_CHIP_ID_MFGdevicecodename2 )                                // ex: LPC811M001JDH16
	#include "family_1xxx\_chip_family_1xxx.h"                                  // 

#elif defined ( CONF_CHIP_ID_MFGdevicecodename3 )                               // ex: LPC1517JBD48
	#include "family_2xxx\_chip_family_2xxx.h"                                  // another family. ex:family_2xxx --> LPC15xx
#elif defined ( CONF_CHIP_ID_MFGdevicecodename3 )                               // ex: LPC1517JBD64
	#include "family_2xxx\_chip_family_2xxx.h"                                  // another family. ex:family_2xxx --> LPC15xx
#else
	#error "Unknown selected device!"
#endif

#endif	//__CHIP_MFG_H_
