// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _serie_lpc8xx.h
// 	   Version: 3.22
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: MCU of series LPC8xx description/configuration file. Same for all MCUs of this family!
//              Special, device depents features, can be included outside of this file (in parent header)
// ******************************************************************************
// Usage:
// 
// ToDo:	viz _Chip_UART_Set_AdrDet
// 
// Changelog:   
//				2024.09.12	- v3.22 - add read data and valit flag from ADC
//									- change IRQ handler names (_CHIP_I2C0_IRQ_Handler to _Chip_I2C0_IRQ_Handler)
//				2024.03.07	- v3.21	- harmonize PIN_INT5_IRQn as PIN_INT5_DAC1_IRQn, PIN_INT6_IRQn as PIN_INT6_USART3_IRQn, PIN_INT7_IRQn as PIN_INT7_USART4_IRQn
//				2024.01.11	- v3.20 - Rename ADC channels functions
//				2023.11.12	- v3.10	- Finalize LPC82x, 83x and 84x serie
//									- correct other MCUs
//				2023.11.12	- v2.1 - change define periphery to peri count.
//				2023.02.13	- v2.1 - _IRQSTAT_ renamed to _IRQSTAT_, _IRQEN_ renamed to _IRQEN_
//              2018.05.06	- v2.0 - chip manufacturer rename, skeleton system re-directories 
//				2014.05.01	- v1.0 - first revision. 
//				2019.06.11	- Optimalization
//				2022.10.17	- change def.: _CHIP_HAVE_XXX to _CHIP_XXX_COUNT, due to store number of periphery in MCU. 0: no periphery




// ************************************************************** CHIP CONFIGURATION *******************
// In your project, please define next constants:

//#define CONF_CHIP_MAINCLK_SEL 		0      										// 00 = fro (reset value)
//                                   												// 01 = external_clk
//                                   												// 10 = lposc_clk
//                                   												// 11 = fro_div

//#define CONF_CHIP_SYSAHBCLKDIV_VAL 	1         									// 0x00 = system_ahb_clk disabled (use with caution)
//                                  												// 0x01 = divide_by_1 (reset value)
//                                   												// 0x02 = divide_by_2
//                                   												// 0xFF = divide_by_255

//#define CONF_CHIP_EXTRATEIN 			12000000     								// External Clock (CLKIN) frequency [Hz] must be in the range of  1 MHz to  25 MHz     

//#define EXT_CLOCK_FORCE_ENABLE 		0   										// Force config. and enable of external_clk for use by other than main_clk
//                                   												// 0 = external_clk will be configured and enabled only if needed by main_clk or sys_pll0_clk.
//                                   												// 1 = external_clk will be configured and enabled (available for other, e.g. clock out).
// *******************************************************************************************************



#ifndef __SERIE_LPC8XX_H_
#define __SERIE_LPC8XX_H_


// Configuration
#ifndef CONF_SYS_WATCHDOG_USED
	#define	CONF_SYS_WATCHDOG_USED			0										// Default Off
#endif


#ifndef LOAD_SCATTER
    extern uint32_t SystemCoreClock;												// -//- pre IAP - System Clock Frequency (Core Clock)
    extern uint32_t OscRateIn;														// 
    extern uint32_t ExtRateIn;														// 
    extern uint32_t RTCOscRateIn;

    // for compatibility with mfg's header file
    typedef enum {ERROR = 0, SUCCESS = !ERROR} Status;
    typedef enum {RESET = 0, SET = !RESET} FlagStatus, IntStatus, SetState;
    typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;


    #define CORE_M0							1
	
	// Call CMSIS Peripheral Access Layer:
	#if   defined (CONF_CHIP_ID_LPC802M001JDH16) || \
		defined (CONF_CHIP_ID_LPC802M001JDH20) || \
		defined (CONF_CHIP_ID_LPC802M001JHI33) || \
		defined (CONF_CHIP_ID_LPC802M011JDH20)
		#include    "LPC802.h" 														// CMSIS Peripheral Access Layer
	#elif defined (CONF_CHIP_ID_LPC804M101JBD16) || \
		defined (CONF_CHIP_ID_LPC804M101JDH20) || \
		defined (CONF_CHIP_ID_LPC804M101JDH24) || \
		defined (CONF_CHIP_ID_LPC804M111JDH24) || \
		defined (CONF_CHIP_ID_LPC804M101JHI33)
		#include    "LPC804.h" 														// CMSIS Peripheral Access Layer
	#elif defined (CONF_CHIP_ID_LPC810M021FN8)
		#include    "LPC810.h" 														// CMSIS Peripheral Access Layer	
	// harmonizing names between 80x and 82x headers (fuck NXP !!!!)
		#define SYSAHBCLKCTRL0						SYSAHBCLKCTRL
		#define SYSCON_SYSAHBCLKCTRL0_GPIO			SYSCON_SYSAHBCLKCTRL_GPIO
		#define SYSCON_SYSAHBCLKCTRL0_I2C0			SYSCON_SYSAHBCLKCTRL_I2C0
		#define SYSCON_SYSAHBCLKCTRL0_I2C1			SYSCON_SYSAHBCLKCTRL_I2C1
		#define SYSCON_SYSAHBCLKCTRL0_I2C2			SYSCON_SYSAHBCLKCTRL_I2C2
		#define SYSCON_SYSAHBCLKCTRL0_I2C3			SYSCON_SYSAHBCLKCTRL_I2C3
		#define SYSCON_SYSAHBCLKCTRL0_UART0			SYSCON_SYSAHBCLKCTRL_UART0
		#define SYSCON_SYSAHBCLKCTRL0_UART1			SYSCON_SYSAHBCLKCTRL_UART1
		#define SYSCON_SYSAHBCLKCTRL0_UART2			SYSCON_SYSAHBCLKCTRL_UART2
		#define SYSCON_SYSAHBCLKCTRL0_SPI0			SYSCON_SYSAHBCLKCTRL_SPI0
		#define SYSCON_SYSAHBCLKCTRL0_SPI1			SYSCON_SYSAHBCLKCTRL_SPI1
		#define SYSCON_SYSAHBCLKCTRL0_CRC0			SYSCON_SYSAHBCLKCTRL_CRC
		#define SYSCON_SYSAHBCLKCTRL0_ADC0			SYSCON_SYSAHBCLKCTRL_ADC
		#define SYSCON_SYSAHBCLKCTRL0_MRT			SYSCON_SYSAHBCLKCTRL_MRT
		#define SYSCON_SYSAHBCLKCTRL0_SCT			SYSCON_SYSAHBCLKCTRL_SCT
		#define SYSCON_SYSAHBCLKCTRL0_DMA			SYSCON_SYSAHBCLKCTRL_DMA
		#define SYSCON_SYSAHBCLKCTRL0_IOCON			SYSCON_SYSAHBCLKCTRL_IOCON
		#define SYSCON_PRESETCTRL0_GPIO_RST_N		SYSCON_PRESETCTRL_GPIO_RST_N
		#define SYSCON_PRESETCTRL0_I2C0_RST_N		SYSCON_PRESETCTRL_I2C0_RST_N
		#define SYSCON_PRESETCTRL0_I2C1_RST_N		SYSCON_PRESETCTRL_I2C1_RST_N
		#define SYSCON_PRESETCTRL0_I2C2_RST_N		SYSCON_PRESETCTRL_I2C2_RST_N
		#define SYSCON_PRESETCTRL0_I2C3_RST_N		SYSCON_PRESETCTRL_I2C3_RST_N
		#define SYSCON_PRESETCTRL0_UART0_RST_N		SYSCON_PRESETCTRL_UART0_RST_N
		#define SYSCON_PRESETCTRL0_UART1_RST_N		SYSCON_PRESETCTRL_UART1_RST_N
		#define SYSCON_PRESETCTRL0_UART2_RST_N		SYSCON_PRESETCTRL_UART2_RST_N
		#define SYSCON_PRESETCTRL0_SPI0_RST_N		SYSCON_PRESETCTRL_SPI0_RST_N
		#define SYSCON_PRESETCTRL0_SPI1_RST_N		SYSCON_PRESETCTRL_SPI1_RST_N
		#define SYSCON_PRESETCTRL0_ADC_RST_N		SYSCON_PRESETCTRL_ADC_RST_N
		#define SYSCON_PRESETCTRL0_MRT0_RST_N		SYSCON_PRESETCTRL_MRT_RST_N
		#define SYSCON_PRESETCTRL0_SCT_RST_N		SYSCON_PRESETCTRL_SCT_RST_N
		#define SYSCON_PRESETCTRL0_DMA_RST_N		SYSCON_PRESETCTRL_DMA_RST_N
		#define SYSCON_SYSAHBCLKCTRL0_SWM			SYSCON_SYSAHBCLKCTRL_SWM
		#define SYSCON_SYSAHBCLKCTRL0_GPIO_INT		SYSCON_SYSAHBCLKCTRL_GPIO_INT
		#define SYSCON_SYSAHBCLKCTRL0_ADC			SYSCON_SYSAHBCLKCTRL_ADC
		#define SYSCON_SYSAHBCLKCTRL0_SWM_MASK		SYSCON_SYSAHBCLKCTRL_SWM_MASK
		#define SYSCON_PRESETCTRL0_MRT_RST_N		SYSCON_PRESETCTRL_MRT_RST_N
		#define PRESETCTRL0							PRESETCTRL			
	#elif defined (CONF_CHIP_ID_LPC811M001JDH16)
		#include    "LPC811.h" 														// CMSIS Peripheral Access Layer	
// harmonizing names between 80x and 82x headers (fuck NXP !!!!)
		#define SYSAHBCLKCTRL0						SYSAHBCLKCTRL
		#define SYSCON_SYSAHBCLKCTRL0_GPIO			SYSCON_SYSAHBCLKCTRL_GPIO
		#define SYSCON_SYSAHBCLKCTRL0_I2C0			SYSCON_SYSAHBCLKCTRL_I2C0
		#define SYSCON_SYSAHBCLKCTRL0_I2C1			SYSCON_SYSAHBCLKCTRL_I2C1
		#define SYSCON_SYSAHBCLKCTRL0_I2C2			SYSCON_SYSAHBCLKCTRL_I2C2
		#define SYSCON_SYSAHBCLKCTRL0_I2C3			SYSCON_SYSAHBCLKCTRL_I2C3
		#define SYSCON_SYSAHBCLKCTRL0_UART0			SYSCON_SYSAHBCLKCTRL_UART0
		#define SYSCON_SYSAHBCLKCTRL0_UART1			SYSCON_SYSAHBCLKCTRL_UART1
		#define SYSCON_SYSAHBCLKCTRL0_UART2			SYSCON_SYSAHBCLKCTRL_UART2
		#define SYSCON_SYSAHBCLKCTRL0_SPI0			SYSCON_SYSAHBCLKCTRL_SPI0
		#define SYSCON_SYSAHBCLKCTRL0_SPI1			SYSCON_SYSAHBCLKCTRL_SPI1
		#define SYSCON_SYSAHBCLKCTRL0_CRC0			SYSCON_SYSAHBCLKCTRL_CRC
		#define SYSCON_SYSAHBCLKCTRL0_ADC0			SYSCON_SYSAHBCLKCTRL_ADC
		#define SYSCON_SYSAHBCLKCTRL0_MRT			SYSCON_SYSAHBCLKCTRL_MRT
		#define SYSCON_SYSAHBCLKCTRL0_SCT			SYSCON_SYSAHBCLKCTRL_SCT
		#define SYSCON_SYSAHBCLKCTRL0_DMA			SYSCON_SYSAHBCLKCTRL_DMA
		#define SYSCON_SYSAHBCLKCTRL0_IOCON			SYSCON_SYSAHBCLKCTRL_IOCON
		#define SYSCON_PRESETCTRL0_GPIO_RST_N		SYSCON_PRESETCTRL_GPIO_RST_N
		#define SYSCON_PRESETCTRL0_I2C0_RST_N		SYSCON_PRESETCTRL_I2C0_RST_N
		#define SYSCON_PRESETCTRL0_I2C1_RST_N		SYSCON_PRESETCTRL_I2C1_RST_N
		#define SYSCON_PRESETCTRL0_I2C2_RST_N		SYSCON_PRESETCTRL_I2C2_RST_N
		#define SYSCON_PRESETCTRL0_I2C3_RST_N		SYSCON_PRESETCTRL_I2C3_RST_N
		#define SYSCON_PRESETCTRL0_UART0_RST_N		SYSCON_PRESETCTRL_UART0_RST_N
		#define SYSCON_PRESETCTRL0_UART1_RST_N		SYSCON_PRESETCTRL_UART1_RST_N
		#define SYSCON_PRESETCTRL0_UART2_RST_N		SYSCON_PRESETCTRL_UART2_RST_N
		#define SYSCON_PRESETCTRL0_SPI0_RST_N		SYSCON_PRESETCTRL_SPI0_RST_N
		#define SYSCON_PRESETCTRL0_SPI1_RST_N		SYSCON_PRESETCTRL_SPI1_RST_N
		#define SYSCON_PRESETCTRL0_ADC_RST_N		SYSCON_PRESETCTRL_ADC_RST_N
		#define SYSCON_PRESETCTRL0_MRT0_RST_N		SYSCON_PRESETCTRL_MRT_RST_N
		#define SYSCON_PRESETCTRL0_SCT_RST_N		SYSCON_PRESETCTRL_SCT_RST_N
		#define SYSCON_PRESETCTRL0_DMA_RST_N		SYSCON_PRESETCTRL_DMA_RST_N
		#define SYSCON_SYSAHBCLKCTRL0_SWM			SYSCON_SYSAHBCLKCTRL_SWM
		#define SYSCON_SYSAHBCLKCTRL0_GPIO_INT		SYSCON_SYSAHBCLKCTRL_GPIO_INT
		#define SYSCON_SYSAHBCLKCTRL0_ADC			SYSCON_SYSAHBCLKCTRL_ADC
		#define SYSCON_SYSAHBCLKCTRL0_SWM_MASK		SYSCON_SYSAHBCLKCTRL_SWM_MASK
		#define SYSCON_PRESETCTRL0_MRT_RST_N		SYSCON_PRESETCTRL_MRT_RST_N
		#define PRESETCTRL0							PRESETCTRL					
	#elif defined (CONF_CHIP_ID_LPC812M101JDH16) || \
		defined (CONF_CHIP_ID_LPC812M101JD20) || \
		defined (CONF_CHIP_ID_LPC812M101JDH20) || \
		defined (CONF_CHIP_ID_LPC812M101JTB16)
		#include    "LPC812.h" 														// CMSIS Peripheral Access Layer	
// harmonizing names between 80x and 82x headers (fuck NXP !!!!)
		#define SYSAHBCLKCTRL0						SYSAHBCLKCTRL
		#define SYSCON_SYSAHBCLKCTRL0_GPIO			SYSCON_SYSAHBCLKCTRL_GPIO
		#define SYSCON_SYSAHBCLKCTRL0_I2C0			SYSCON_SYSAHBCLKCTRL_I2C0
		#define SYSCON_SYSAHBCLKCTRL0_I2C1			SYSCON_SYSAHBCLKCTRL_I2C1
		#define SYSCON_SYSAHBCLKCTRL0_I2C2			SYSCON_SYSAHBCLKCTRL_I2C2
		#define SYSCON_SYSAHBCLKCTRL0_I2C3			SYSCON_SYSAHBCLKCTRL_I2C3
		#define SYSCON_SYSAHBCLKCTRL0_UART0			SYSCON_SYSAHBCLKCTRL_UART0
		#define SYSCON_SYSAHBCLKCTRL0_UART1			SYSCON_SYSAHBCLKCTRL_UART1
		#define SYSCON_SYSAHBCLKCTRL0_UART2			SYSCON_SYSAHBCLKCTRL_UART2
		#define SYSCON_SYSAHBCLKCTRL0_SPI0			SYSCON_SYSAHBCLKCTRL_SPI0
		#define SYSCON_SYSAHBCLKCTRL0_SPI1			SYSCON_SYSAHBCLKCTRL_SPI1
		#define SYSCON_SYSAHBCLKCTRL0_CRC0			SYSCON_SYSAHBCLKCTRL_CRC
		#define SYSCON_SYSAHBCLKCTRL0_ADC0			SYSCON_SYSAHBCLKCTRL_ADC
		#define SYSCON_SYSAHBCLKCTRL0_MRT			SYSCON_SYSAHBCLKCTRL_MRT
		#define SYSCON_SYSAHBCLKCTRL0_SCT			SYSCON_SYSAHBCLKCTRL_SCT
		#define SYSCON_SYSAHBCLKCTRL0_DMA			SYSCON_SYSAHBCLKCTRL_DMA
		#define SYSCON_SYSAHBCLKCTRL0_IOCON			SYSCON_SYSAHBCLKCTRL_IOCON
		#define SYSCON_PRESETCTRL0_GPIO_RST_N		SYSCON_PRESETCTRL_GPIO_RST_N
		#define SYSCON_PRESETCTRL0_I2C0_RST_N		SYSCON_PRESETCTRL_I2C0_RST_N
		#define SYSCON_PRESETCTRL0_I2C1_RST_N		SYSCON_PRESETCTRL_I2C1_RST_N
		#define SYSCON_PRESETCTRL0_I2C2_RST_N		SYSCON_PRESETCTRL_I2C2_RST_N
		#define SYSCON_PRESETCTRL0_I2C3_RST_N		SYSCON_PRESETCTRL_I2C3_RST_N
		#define SYSCON_PRESETCTRL0_UART0_RST_N		SYSCON_PRESETCTRL_UART0_RST_N
		#define SYSCON_PRESETCTRL0_UART1_RST_N		SYSCON_PRESETCTRL_UART1_RST_N
		#define SYSCON_PRESETCTRL0_UART2_RST_N		SYSCON_PRESETCTRL_UART2_RST_N
		#define SYSCON_PRESETCTRL0_SPI0_RST_N		SYSCON_PRESETCTRL_SPI0_RST_N
		#define SYSCON_PRESETCTRL0_SPI1_RST_N		SYSCON_PRESETCTRL_SPI1_RST_N
		#define SYSCON_PRESETCTRL0_ADC_RST_N		SYSCON_PRESETCTRL_ADC_RST_N
		#define SYSCON_PRESETCTRL0_MRT0_RST_N		SYSCON_PRESETCTRL_MRT_RST_N
		#define SYSCON_PRESETCTRL0_SCT_RST_N		SYSCON_PRESETCTRL_SCT_RST_N
		#define SYSCON_PRESETCTRL0_DMA_RST_N		SYSCON_PRESETCTRL_DMA_RST_N
		#define SYSCON_SYSAHBCLKCTRL0_SWM			SYSCON_SYSAHBCLKCTRL_SWM
		#define SYSCON_SYSAHBCLKCTRL0_GPIO_INT		SYSCON_SYSAHBCLKCTRL_GPIO_INT
		#define SYSCON_SYSAHBCLKCTRL0_ADC			SYSCON_SYSAHBCLKCTRL_ADC
		#define SYSCON_SYSAHBCLKCTRL0_SWM_MASK		SYSCON_SYSAHBCLKCTRL_SWM_MASK
		#define SYSCON_PRESETCTRL0_MRT_RST_N		SYSCON_PRESETCTRL_MRT_RST_N
		#define PRESETCTRL0							PRESETCTRL					
	#elif defined (CONF_CHIP_ID_LPC822M101JHI33) || \
		defined (CONF_CHIP_ID_LPC822M101JDH20)
		#include    "LPC822.h" 														// CMSIS Peripheral Access Layer			
		#define SYSAHBCLKCTRL0						SYSAHBCLKCTRL
		#define SYSCON_SYSAHBCLKCTRL0_GPIO			SYSCON_SYSAHBCLKCTRL_GPIO
		#define SYSCON_SYSAHBCLKCTRL0_I2C0			SYSCON_SYSAHBCLKCTRL_I2C0
		#define SYSCON_SYSAHBCLKCTRL0_I2C1			SYSCON_SYSAHBCLKCTRL_I2C1
		#define SYSCON_SYSAHBCLKCTRL0_I2C2			SYSCON_SYSAHBCLKCTRL_I2C2
		#define SYSCON_SYSAHBCLKCTRL0_I2C3			SYSCON_SYSAHBCLKCTRL_I2C3
		#define SYSCON_SYSAHBCLKCTRL0_UART0			SYSCON_SYSAHBCLKCTRL_UART0
		#define SYSCON_SYSAHBCLKCTRL0_UART1			SYSCON_SYSAHBCLKCTRL_UART1
		#define SYSCON_SYSAHBCLKCTRL0_UART2			SYSCON_SYSAHBCLKCTRL_UART2
		#define SYSCON_SYSAHBCLKCTRL0_SPI0			SYSCON_SYSAHBCLKCTRL_SPI0
		#define SYSCON_SYSAHBCLKCTRL0_SPI1			SYSCON_SYSAHBCLKCTRL_SPI1
		#define SYSCON_SYSAHBCLKCTRL0_CRC0			SYSCON_SYSAHBCLKCTRL_CRC
		#define SYSCON_SYSAHBCLKCTRL0_ADC0			SYSCON_SYSAHBCLKCTRL_ADC
		#define SYSCON_SYSAHBCLKCTRL0_MRT			SYSCON_SYSAHBCLKCTRL_MRT
		#define SYSCON_SYSAHBCLKCTRL0_SCT			SYSCON_SYSAHBCLKCTRL_SCT
		#define SYSCON_SYSAHBCLKCTRL0_DMA			SYSCON_SYSAHBCLKCTRL_DMA
		#define SYSCON_SYSAHBCLKCTRL0_IOCON			SYSCON_SYSAHBCLKCTRL_IOCON
		#define SYSCON_PRESETCTRL0_GPIO_RST_N		SYSCON_PRESETCTRL_GPIO_RST_N
		#define SYSCON_PRESETCTRL0_I2C0_RST_N		SYSCON_PRESETCTRL_I2C0_RST_N
		#define SYSCON_PRESETCTRL0_I2C1_RST_N		SYSCON_PRESETCTRL_I2C1_RST_N
		#define SYSCON_PRESETCTRL0_I2C2_RST_N		SYSCON_PRESETCTRL_I2C2_RST_N
		#define SYSCON_PRESETCTRL0_I2C3_RST_N		SYSCON_PRESETCTRL_I2C3_RST_N
		#define SYSCON_PRESETCTRL0_UART0_RST_N		SYSCON_PRESETCTRL_UART0_RST_N
		#define SYSCON_PRESETCTRL0_UART1_RST_N		SYSCON_PRESETCTRL_UART1_RST_N
		#define SYSCON_PRESETCTRL0_UART2_RST_N		SYSCON_PRESETCTRL_UART2_RST_N
		#define SYSCON_PRESETCTRL0_SPI0_RST_N		SYSCON_PRESETCTRL_SPI0_RST_N
		#define SYSCON_PRESETCTRL0_SPI1_RST_N		SYSCON_PRESETCTRL_SPI1_RST_N
		#define SYSCON_PRESETCTRL0_ADC_RST_N		SYSCON_PRESETCTRL_ADC_RST_N
		#define SYSCON_PRESETCTRL0_MRT0_RST_N		SYSCON_PRESETCTRL_MRT_RST_N
		#define SYSCON_PRESETCTRL0_SCT_RST_N		SYSCON_PRESETCTRL_SCT_RST_N
		#define SYSCON_PRESETCTRL0_DMA_RST_N		SYSCON_PRESETCTRL_DMA_RST_N
		#define SYSCON_SYSAHBCLKCTRL0_SWM			SYSCON_SYSAHBCLKCTRL_SWM
		#define SYSCON_SYSAHBCLKCTRL0_GPIO_INT		SYSCON_SYSAHBCLKCTRL_GPIO_INT
		#define SYSCON_SYSAHBCLKCTRL0_ADC			SYSCON_SYSAHBCLKCTRL_ADC
		#define SYSCON_SYSAHBCLKCTRL0_SWM_MASK		SYSCON_SYSAHBCLKCTRL_SWM_MASK
		#define SYSCON_PRESETCTRL0_MRT_RST_N		SYSCON_PRESETCTRL_MRT_RST_N
		#define PRESETCTRL0							PRESETCTRL					
	#elif defined (CONF_CHIP_ID_LPC824M201JHI33) || \
		defined (CONF_CHIP_ID_LPC824M201JDH20)
		#include    "LPC824.h" 														// CMSIS Peripheral Access Layer			
		#define SYSAHBCLKCTRL0						SYSAHBCLKCTRL
		#define SYSCON_SYSAHBCLKCTRL0_GPIO			SYSCON_SYSAHBCLKCTRL_GPIO
		#define SYSCON_SYSAHBCLKCTRL0_I2C0			SYSCON_SYSAHBCLKCTRL_I2C0
		#define SYSCON_SYSAHBCLKCTRL0_I2C1			SYSCON_SYSAHBCLKCTRL_I2C1
		#define SYSCON_SYSAHBCLKCTRL0_I2C2			SYSCON_SYSAHBCLKCTRL_I2C2
		#define SYSCON_SYSAHBCLKCTRL0_I2C3			SYSCON_SYSAHBCLKCTRL_I2C3
		#define SYSCON_SYSAHBCLKCTRL0_UART0			SYSCON_SYSAHBCLKCTRL_UART0
		#define SYSCON_SYSAHBCLKCTRL0_UART1			SYSCON_SYSAHBCLKCTRL_UART1
		#define SYSCON_SYSAHBCLKCTRL0_UART2			SYSCON_SYSAHBCLKCTRL_UART2
		#define SYSCON_SYSAHBCLKCTRL0_SPI0			SYSCON_SYSAHBCLKCTRL_SPI0
		#define SYSCON_SYSAHBCLKCTRL0_SPI1			SYSCON_SYSAHBCLKCTRL_SPI1
		#define SYSCON_SYSAHBCLKCTRL0_CRC0			SYSCON_SYSAHBCLKCTRL_CRC
		#define SYSCON_SYSAHBCLKCTRL0_ADC0			SYSCON_SYSAHBCLKCTRL_ADC
		#define SYSCON_SYSAHBCLKCTRL0_MRT			SYSCON_SYSAHBCLKCTRL_MRT
		#define SYSCON_SYSAHBCLKCTRL0_SCT			SYSCON_SYSAHBCLKCTRL_SCT
		#define SYSCON_SYSAHBCLKCTRL0_DMA			SYSCON_SYSAHBCLKCTRL_DMA
		#define SYSCON_SYSAHBCLKCTRL0_IOCON			SYSCON_SYSAHBCLKCTRL_IOCON
		#define SYSCON_PRESETCTRL0_GPIO_RST_N		SYSCON_PRESETCTRL_GPIO_RST_N
		#define SYSCON_PRESETCTRL0_I2C0_RST_N		SYSCON_PRESETCTRL_I2C0_RST_N
		#define SYSCON_PRESETCTRL0_I2C1_RST_N		SYSCON_PRESETCTRL_I2C1_RST_N
		#define SYSCON_PRESETCTRL0_I2C2_RST_N		SYSCON_PRESETCTRL_I2C2_RST_N
		#define SYSCON_PRESETCTRL0_I2C3_RST_N		SYSCON_PRESETCTRL_I2C3_RST_N
		#define SYSCON_PRESETCTRL0_UART0_RST_N		SYSCON_PRESETCTRL_UART0_RST_N
		#define SYSCON_PRESETCTRL0_UART1_RST_N		SYSCON_PRESETCTRL_UART1_RST_N
		#define SYSCON_PRESETCTRL0_UART2_RST_N		SYSCON_PRESETCTRL_UART2_RST_N
		#define SYSCON_PRESETCTRL0_SPI0_RST_N		SYSCON_PRESETCTRL_SPI0_RST_N
		#define SYSCON_PRESETCTRL0_SPI1_RST_N		SYSCON_PRESETCTRL_SPI1_RST_N
		#define SYSCON_PRESETCTRL0_ADC_RST_N		SYSCON_PRESETCTRL_ADC_RST_N
		#define SYSCON_PRESETCTRL0_MRT0_RST_N		SYSCON_PRESETCTRL_MRT_RST_N
		#define SYSCON_PRESETCTRL0_SCT_RST_N		SYSCON_PRESETCTRL_SCT_RST_N
		#define SYSCON_PRESETCTRL0_DMA_RST_N		SYSCON_PRESETCTRL_DMA_RST_N
		#define SYSCON_SYSAHBCLKCTRL0_SWM			SYSCON_SYSAHBCLKCTRL_SWM
		#define SYSCON_SYSAHBCLKCTRL0_GPIO_INT		SYSCON_SYSAHBCLKCTRL_GPIO_INT
		#define SYSCON_SYSAHBCLKCTRL0_ADC			SYSCON_SYSAHBCLKCTRL_ADC
		#define SYSCON_SYSAHBCLKCTRL0_SWM_MASK		SYSCON_SYSAHBCLKCTRL_SWM_MASK
		#define SYSCON_SYSAHBCLKCTRL0_GPIO0			SYSCON_SYSAHBCLKCTRL0_GPIO
		#define SYSCON_PRESETCTRL0_MRT_RST_N		SYSCON_PRESETCTRL_MRT_RST_N
		#define SYSCON_PRESETCTRL0_GPIO0_RST_N		SYSCON_PRESETCTRL0_GPIO_RST_N
		#define PRESETCTRL0							PRESETCTRL					
	#elif defined (CONF_CHIP_ID_LPC832M101FDH20) || \
		defined (CONF_CHIP_ID_LPC834M101FHI33)
		#include    "LPC83x.h" 														// CMSIS Peripheral Access Layer			
	#elif defined (CONF_CHIP_ID_LPC844M201JHI33) || \
		defined (CONF_CHIP_ID_LPC844M201JBD48) || \
		defined (CONF_CHIP_ID_LPC844M201JHI48) || \
		defined (CONF_CHIP_ID_LPC844M201JBD64)
		#include    "LPC844.h" 														// CMSIS Peripheral Access Layer
		#define SYSCON_SYSAHBCLKCTRL0_CRC0			SYSCON_SYSAHBCLKCTRL0_CRC
		#define SYSCON_PRESETCTRL0_MRT0_RST_N		SYSCON_PRESETCTRL0_MRT_RST_N
		#define SYSCON_PRESETCTRL0_SCT0_RST_N		SYSCON_PRESETCTRL0_SCT_RST_N
		#define SYSCON_PRESETCTRL0_DMA0_RST_N		SYSCON_PRESETCTRL0_DMA_RST_N

		#define DAC1_IRQn							PIN_INT5_DAC1_IRQn
		#define PIN_INT5_IRQn						PIN_INT5_DAC1_IRQn
		#define USART3_IRQn							PIN_INT6_USART3_IRQn
		#define PIN_INT6_IRQn						PIN_INT6_USART3_IRQn
		#define USART4_IRQn 						PIN_INT7_USART4_IRQn
		#define PIN_INT7_IRQn						PIN_INT7_USART4_IRQn		
	#elif defined (CONF_CHIP_ID_LPC845M301JHI33) || \
		defined (CONF_CHIP_ID_LPC845M301JBD48) || \
		defined (CONF_CHIP_ID_LPC845M301JHI48) || \
		defined (CONF_CHIP_ID_LPC845M301JBD64)
		#include    "LPC845.h" 														// CMSIS Peripheral Access Layer			
		#define SYSCON_SYSAHBCLKCTRL0_CRC0			SYSCON_SYSAHBCLKCTRL0_CRC
		#define SYSCON_PRESETCTRL0_MRT0_RST_N		SYSCON_PRESETCTRL0_MRT_RST_N
		#define SYSCON_PRESETCTRL0_SCT0_RST_N		SYSCON_PRESETCTRL0_SCT_RST_N
		#define SYSCON_PRESETCTRL0_DMA0_RST_N		SYSCON_PRESETCTRL0_DMA_RST_N
		
		#define DAC1_IRQn							PIN_INT5_DAC1_IRQn
		#define PIN_INT5_IRQn						PIN_INT5_DAC1_IRQn
		#define USART3_IRQn							PIN_INT6_USART3_IRQn
		#define PIN_INT6_IRQn						PIN_INT6_USART3_IRQn
		#define USART4_IRQn 						PIN_INT7_USART4_IRQn
		#define PIN_INT7_IRQn						PIN_INT7_USART4_IRQn
	#elif defined (CONF_CHIP_ID_LPC8N04FHI24)
		#include    "LPC8N04.h"														// CMSIS Peripheral Access Layer		
	#else
		#error "Unknown selected device!"
	#endif
#endif


// ************************************************************************************************
// PUBLIC Varioables for all LPC8XX MCU's
// ************************************************************************************************

#define	_CHIP_IRC_FREQUENCY				12000000UL									// Internal Oscilator: 12MHz

// for whole family LPC8xx:
#define  _CHIP_WWDT_COUNT				1											// Windowed Watchdog Timer (WWDT)
#define  _CHIP_WKT_COUNT				1											// Self-WakeUp timer
#define  _CHIP_SWMBLOCK_COUNT			1											// Switch Matrix (SWM)
#define  _CHIP_CRC_COUNT				1											// CRC peri

#define IAP_ENTRY_LOCATION        		0X1FFF1FF1UL								// Pointer to ROM IAP entry functions





// Specific parameters for each chip from this series:
#if defined (CONF_CHIP_ID_LPC802M001JDH16)
#elif defined (CONF_CHIP_ID_LPC802M001JDH20)
#elif defined (CONF_CHIP_ID_LPC802M001JHI33)
#elif defined (CONF_CHIP_ID_LPC802M011JDH20)
#elif defined (CONF_CHIP_ID_LPC804M101JBD16)
    #define  CHIP_ID                    	0x00008040                  			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x1000									// size of internal RAM (datasheet value) - 4kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x8000									// size of internal FLASH (datasheet value) - 32kB
#elif defined (CONF_CHIP_ID_LPC804M101JDH20)
    #define  CHIP_ID                    	0x00008041                  			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x1000									// size of internal RAM (datasheet value) - 4kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x8000									// size of internal FLASH (datasheet value) - 32kB
#elif defined (CONF_CHIP_ID_LPC804M101JDH24)
    #define  CHIP_ID                    	0x00008042                    			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x1000									// size of internal RAM (datasheet value) - 4kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x8000									// size of internal FLASH (datasheet value) - 32kB
#elif defined (CONF_CHIP_ID_LPC804M111JDH24)
    #define  CHIP_ID                    	0x00008043                 				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x1000									// size of internal RAM (datasheet value) - 4kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x8000									// size of internal FLASH (datasheet value) - 32kB
#elif defined (CONF_CHIP_ID_LPC804M101JHI33)
    #define  CHIP_ID                    	0x00008044                 				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x1000									// size of internal RAM (datasheet value) - 4kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x8000									// size of internal FLASH (datasheet value) - 32kB
	
#elif defined (CONF_CHIP_ID_LPC810M021FN8)

#elif defined (CONF_CHIP_ID_LPC811M001JDH16)
	
#elif defined (CONF_CHIP_ID_LPC812M101JDH16)
	
#elif defined (CONF_CHIP_ID_LPC812M101JD20)
	
#elif defined (CONF_CHIP_ID_LPC812M101JDH20)
	
#elif defined (CONF_CHIP_ID_LPC812M101JTB16)
	
#elif defined (CONF_CHIP_ID_LPC822M101JHI33)
	
#elif defined (CONF_CHIP_ID_LPC822M101JDH20)

#elif defined (CONF_CHIP_ID_LPC824M201JHI33)
    #define  CHIP_ID                    	0x00008241                      		// Chip ID wroted in silicone

#elif defined (CONF_CHIP_ID_LPC824M201JDH20)
    #define  CHIP_ID                    	0x00008242                      		// Chip ID wroted in silicone
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
    #define	 _CHIP_IRAM						0x2000									// size of internal RAM (datasheet value) - 8kB	
	#define	 _CHIP_IFLASH					0x8000									// size of internal FLASH (datasheet value) - 32kB
	
	#define  _CHIP_DMA_COUNT				1										// Number of Direct Memory Access periphery
	#define	 _CHIP_DMA_CHANNELS_COUNT		18										// number of channels per DMA
	#define  _CHIP_ADC_COUNT				1										// number of Analog-to-Digital Converter (ADC)
	#define	 _CHIP_ADC_CHANNELS_COUNT		12										// number of channels per ADC
	#define  _CHIP_DAC_COUNT				0
	#define	 _CHIP_MRT_COUNT				1										// number of MultiRate Timer
	#define	 _CHIP_MRT_CHANNELS_COUNT		4										// number of channels per MRT	
	#define  _CHIP_I2C_COUNT				4										// Number of I2C Periphery
	#define  _CHIP_SCT_COUNT				1										// Number of SCT Periphery
	#define  _CHIP_SPI_COUNT				2										// Number of Serial Peripheral Interface
	#define  _CHIP_UART_COUNT				3										// Number of Universal asynchronous receiver-transmitter

	#define	 _CHIP_EMUL_EEPROM_START		0x00007C00								// Start at page of Flash 496
	#define	 _CHIP_EMUL_EEPROM_PAGESIZE		64										// minimal page size for IAP FLASH erase/write
	#define	 _CHIP_EMUL_EEPROM_PAGESPERSECTOR	16									// number of pages per sector
	#define  _CHIP_EMUL_EEPROM_PAGE_NUM 	4										// Virtual EEPROM pages number. Last two are not available because of the boot block.
	#define	 _CHIP_EMUL_EEPROM_SIZE			(_CHIP_EMUL_EEPROM_PAGESIZE * _CHIP_EMUL_EEPROM_PAGE_NUM) // byte size of emulated EEPROM in FLASH
	
	#define	 _CHIP_LPOSC_VALUE				1000000									// Low Power oscillator frequency value
	#define ROM_DRIVER_BASE 				(0x1FFF1FF8UL)
	#define  _CHIP_GPIO_COUNT				1										// General Purpose Input/Output periphery number
				// GPIO 0.				  	  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28
	#define  _CHIP_IOCON_REMAP				{17,11, 6, 5, 4, 3,16,15,14,13, 8, 7, 2, 1,18,10, 9, 0,30,29,28,27,26,25,24,23,22,21,20};	// IOCON registers is not in ascending !!!
	#define	 _CHIP_GPIO_LAST_PIN			28
																		// count all GPIUOs! Its independend from p[ackage variant!
	//#define  _CHIP_GPIO_PIN_COUNT			16											// General Purpose Input/Output Pins number
	
	#define	 IRQHANDLER_16					_Chip_SPI0_IRQ_Handler
	#define	 IRQHANDLER_17					_Chip_SPI1_IRQ_Handler
	#define	 IRQHANDLER_18					_Chip_Default_IRQ_Handler					// this IRQ num is unused
	#define	 IRQHANDLER_19					_Chip_UART0_IRQ_Handler
	#define	 IRQHANDLER_20					_Chip_UART1_IRQ_Handler
	#define	 IRQHANDLER_21					_Chip_UART2_IRQ_Handler
	#define	 IRQHANDLER_22					_Chip_Default_IRQ_Handler					// this IRQ num is unused
	#define	 IRQHANDLER_23					_Chip_I2C1_IRQ_Handler
	#define	 IRQHANDLER_24					_Chip_I2C0_IRQ_Handler
	#define	 IRQHANDLER_25					_Chip_SCT0_IRQ_Handler
	#define	 IRQHANDLER_26					_Chip_MRT0_IRQ_Handler
	#define	 IRQHANDLER_27					_Chip_CMP0_IRQ_Handler
	#define	 IRQHANDLER_28					_Chip_WDT0_IRQ_Handler
	#define  IRQHANDLER_29					_Chip_BOD0_IRQ_Handler
	#define	 IRQHANDLER_30					_Chip_FLASH0_IRQ_Handler
	#define	 IRQHANDLER_31					_Chip_WKT0_IRQ_Handler
	#define	 IRQHANDLER_32					_Chip_ADC0_SEQA_IRQ_Handler
	#define	 IRQHANDLER_33					_Chip_ADC0_SEQB_IRQ_Handler
	#define	 IRQHANDLER_34					_Chip_ADC0_THCMP_IRQ_Handler
	#define	 IRQHANDLER_35					_Chip_ADC0_OVR_IRQ_Handler
	#define	 IRQHANDLER_36					_Chip_DMA0_IRQ_Handler
	#define	 IRQHANDLER_37					_Chip_I2C2_IRQ_Handler
	#define	 IRQHANDLER_38					_Chip_I2C3_IRQ_Handler
	#define	 IRQHANDLER_39					_Chip_Default_IRQ_Handler					// this IRQ num is unused
	#define	 IRQHANDLER_40					_Chip_PINT0_IRQ_Handler
	#define	 IRQHANDLER_41					_Chip_PINT1_IRQ_Handler
	#define	 IRQHANDLER_42					_Chip_PINT2_IRQ_Handler
	#define	 IRQHANDLER_43					_Chip_PINT3_IRQ_Handler
	#define	 IRQHANDLER_44					_Chip_PINT4_IRQ_Handler
	#define	 IRQHANDLER_45					_Chip_PINT5_IRQ_Handler
	#define	 IRQHANDLER_46					_Chip_PINT6_IRQ_Handler
	#define	 IRQHANDLER_47					_Chip_PINT7_IRQ_Handler

#elif defined (CONF_CHIP_ID_LPC832M101FDH20)

#elif defined (CONF_CHIP_ID_LPC834M101FHI33)

#elif defined (CONF_CHIP_ID_LPC844M201JHI33)
	#define  CHIP_ID                    	0x00008444                     			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x2000									// size of internal RAM (datasheet value) - 8kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x10000									// size of internal FLASH (datasheet value) - 64kB
	
	#define  _CHIP_GPIO_COUNT				1										// General Purpose Input/Output periphery number
				// GPIO 0.				  	  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28
	#define  _CHIP_IOCON_REMAP				{17,11, 6, 5, 4, 3,16,15,14,13, 8, 7, 2, 1,18,10, 9, 0,30,29,28,27,26,25,24,23,22,21,20};	// IOCON registers is not in ascending !!!
	#define	 _CHIP_GPIO_LAST_PIN			28
#elif defined (CONF_CHIP_ID_LPC844M201JBD48)
	#define  CHIP_ID                    	0x00008442                 				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x2000									// size of internal RAM (datasheet value) - 8kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x10000									// size of internal FLASH (datasheet value) - 64kB

	#define  _CHIP_GPIO_COUNT				2										// General Purpose Input/Output periphery number
				// GPIO 0.				  	  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,
	#define  _CHIP_IOCON_REMAP				{17,11, 6, 5, 4, 3,16,15,14,13, 8, 7, 2, 1,18,10, 9, 0,30,29,28,27,26,25,24,23,22,21,20,51,52,35, \
											 36,37,38,41,42,43,46,50,31,32};		// IOCON registers is not in ascending !!!
	#define	 _CHIP_GPIO_LAST_PIN			41										// Last GPIO PIN in the array above
	
#elif defined (CONF_CHIP_ID_LPC844M201JHI48)
	#define  CHIP_ID                    	0x00008442                 				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x2000									// size of internal RAM (datasheet value) - 8kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x10000									// size of internal FLASH (datasheet value) - 64kB
	
	#define  _CHIP_GPIO_COUNT				2										// General Purpose Input/Output periphery number
				// GPIO 0.				  	  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,
	#define  _CHIP_IOCON_REMAP				{17,11, 6, 5, 4, 3,16,15,14,13, 8, 7, 2, 1,18,10, 9, 0,30,29,28,27,26,25,24,23,22,21,20,51,52,35, \
											 36,37,38,41,42,43,46,50,31,32};		// IOCON registers is not in ascending !!!
	#define	 _CHIP_GPIO_LAST_PIN			41										// Last GPIO PIN in the array above
	
#elif defined (CONF_CHIP_ID_LPC844M201JBD64)
	#define  CHIP_ID                    	0x00008441                 				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x2000									// size of internal RAM (datasheet value) - 8kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x10000									// size of internal FLASH (datasheet value) - 64kB
	#define  _CHIP_GPIO_COUNT				2										// General Purpose Input/Output periphery number
				// GPIO 0.				  	  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,
	#define  _CHIP_IOCON_REMAP				{17,11, 6, 5, 4, 3,16,15,14,13, 8, 7, 2, 1,18,10, 9, 0,30,29,28,27,26,25,24,23,22,21,20,51,52,35, \
											 36,37,38,41,42,43,46,50,31,32,56,55,33,34,39,40,44,45,47,49,53,54};	// IOCON registers is not in ascending !!!
	#define	 _CHIP_GPIO_LAST_PIN			53									// Last GPIO PIN in the array above
#elif defined (CONF_CHIP_ID_LPC845M301JHI33)
	#define  CHIP_ID                    	0x00008454                     			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x4000									// size of internal RAM (datasheet value) - 16kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x10000									// size of internal FLASH (datasheet value) - 64kB

	#define	 IRQHANDLER_16					_Chip_SPI0_IRQ_Handler
	#define	 IRQHANDLER_17					_Chip_SPI1_IRQ_Handler
	#define	 IRQHANDLER_18					_Chip_DAC0_IRQ_Handler
	#define	 IRQHANDLER_19					_Chip_UART0_IRQ_Handler
	#define	 IRQHANDLER_20					_Chip_UART1_IRQ_Handler
	#define	 IRQHANDLER_21					_Chip_UART2_IRQ_Handler
	#define	 IRQHANDLER_22					_Chip_Default_IRQ_Handler				// this IRQ num is unused
	#define	 IRQHANDLER_23					_Chip_I2C1_IRQ_Handler
	#define	 IRQHANDLER_24					_Chip_I2C0_IRQ_Handler
	#define	 IRQHANDLER_25					_Chip_SCT0_IRQ_Handler
	#define	 IRQHANDLER_26					_Chip_MRT0_IRQ_Handler
	#define	 IRQHANDLER_27					_Chip_CMP0_IRQ_Handler					// this IRQ num is unused
	#define	 IRQHANDLER_28					_Chip_WDT0_IRQ_Handler
	#define  IRQHANDLER_29					_Chip_BOD0_IRQ_Handler
	#define  IRQHANDLER_30					_Chip_FLASH0_IRQ_Handler
	#define	 IRQHANDLER_31					_Chip_WKT0_IRQ_Handler
	#define	 IRQHANDLER_32					_Chip_ADC0_SEQA_IRQ_Handler
	#define	 IRQHANDLER_33					_Chip_ADC0_SEQB_IRQ_Handler
	#define	 IRQHANDLER_34					_Chip_ADC0_THCMP_IRQ_Handler
	#define	 IRQHANDLER_35					_Chip_ADC0_OVR_IRQ_Handler
	#define	 IRQHANDLER_36					_Chip_DMA0_IRQ_Handler
	#define	 IRQHANDLER_37					_Chip_I2C2_IRQ_Handler
	#define	 IRQHANDLER_38					_Chip_I2C3_IRQ_Handler
	#define	 IRQHANDLER_39					_Chip_CT32B0_IRQ_Handler
	#define	 IRQHANDLER_40					_Chip_PINT0_IRQ_Handler
	#define	 IRQHANDLER_41					_Chip_PINT1_IRQ_Handler
	#define	 IRQHANDLER_42					_Chip_PINT2_IRQ_Handler
	#define	 IRQHANDLER_43					_Chip_PINT3_IRQ_Handler
	#define	 IRQHANDLER_44					_Chip_PINT4_IRQ_Handler
	#define	 IRQHANDLER_45					_Chip_PINT5_IRQ_Handler
	#define	 IRQHANDLER_46					_Chip_PINT6_IRQ_Handler
	#define	 IRQHANDLER_47					_Chip_PINT7_IRQ_Handler

	#define _CHIP_CLOCK_FAIM_BASE 			(0x50010000U)
	#define	IAP_ENTRY_LOCATION        		0x0F001FF1								// Pointer to ROM IAP entry functions

	#define  _CHIP_DAC_COUNT				1										// Digital-to-Analog Converter (DAC)
	#define  _CHIP_I2C_COUNT				4										// A typical I2C-bus	
	#define  _CHIP_SPI_COUNT				2										// Serial Peripheral Interface
	#define  _CHIP_UART_COUNT				5										// Universal asynchronous receiver-transmitter
	#define  _CHIP_CAPTOUCH_COUNT			0										// Capacitive Touch
	#define  _CHIP_DMA_COUNT				1										// Number of Direct Memory Access periphery
	#define	 _CHIP_DMA_CHANNELS_COUNT		25										// number of channels per DMA
	#define  _CHIP_ADC_COUNT				1										// number of Analog-to-Digital Converter (ADC)
	#define	 _CHIP_ADC_CHANNELS_COUNT		12										// number of channels per ADC
	#define	 _CHIP_MRT_COUNT				1										// number of MultiRate Timer
	#define	 _CHIP_MRT_CHANNELS_COUNT		4										// number of channels per MRT	
	#define	 _CHIP_SCT_COUNT				1										// SC Timer
	
	#define	 _CHIP_EMUL_EEPROM_START		0x0000F000								// Start at page of Flash 960
	#define	 _CHIP_EMUL_EEPROM_PAGESIZE		64										// minimal page size for IAP FLASH erase/write
	#define	 _CHIP_EMUL_EEPROM_PAGESPERSECTOR	16									// number of pages per sector
	#define  _CHIP_EMUL_EEPROM_PAGE_NUM		4										// Virtual EEPROM pages number. Last two are not available because of the boot block.
	#define	 _CHIP_EMUL_EEPROM_SIZE			(_CHIP_EMUL_EEPROM_PAGESIZE * _CHIP_EMUL_EEPROM_PAGE_NUM) // byte size of emulated EEPROM in FLASH

	#define  _CHIP_GPIO_COUNT				1										// General Purpose Input/Output periphery number
				// GPIO n.				  	  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28
	#define  _CHIP_IOCON_REMAP				{17,11, 6, 5, 4, 3,16,15,14,13, 8, 7, 2, 1,18,10, 9, 0,30,29,28,27,26,25,24,23,22,21,20};	// IOCON registers is not in ascending !!!
	#define	 _CHIP_GPIO_LAST_PIN			28										// Last GPIO PIN
	
#elif defined (CONF_CHIP_ID_LPC845M301JBD48)
	#define  CHIP_ID                    	0x00008452                    			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x4000									// size of internal RAM (datasheet value) - 16kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x10000									// size of internal FLASH (datasheet value) - 64kB

	#define	 IRQHANDLER_16					_Chip_SPI0_IRQ_Handler
	#define	 IRQHANDLER_17					_Chip_SPI1_IRQ_Handler
	#define	 IRQHANDLER_18					_Chip_DAC0_IRQ_Handler
	#define	 IRQHANDLER_19					_Chip_UART0_IRQ_Handler
	#define	 IRQHANDLER_20					_Chip_UART1_IRQ_Handler
	#define	 IRQHANDLER_21					_Chip_UART2_IRQ_Handler
	#define	 IRQHANDLER_22					_Chip_Default_IRQ_Handler				// this IRQ num is unused
	#define	 IRQHANDLER_23					_Chip_I2C1_IRQ_Handler
	#define	 IRQHANDLER_24					_Chip_I2C0_IRQ_Handler
	#define	 IRQHANDLER_25					_Chip_SCT0_IRQ_Handler
	#define	 IRQHANDLER_26					_Chip_MRT0_IRQ_Handler
	#define	 IRQHANDLER_27					_Chip_CMP0_IRQ_Handler					// this IRQ num is unused
	#define	 IRQHANDLER_28					_Chip_WDT0_IRQ_Handler
	#define  IRQHANDLER_29					_Chip_BOD0_IRQ_Handler
	#define  IRQHANDLER_30					_Chip_FLASH0_IRQ_Handler
	#define	 IRQHANDLER_31					_Chip_WKT0_IRQ_Handler
	#define	 IRQHANDLER_32					_Chip_ADC0_SEQA_IRQ_Handler
	#define	 IRQHANDLER_33					_Chip_ADC0_SEQB_IRQ_Handler
	#define	 IRQHANDLER_34					_Chip_ADC0_THCMP_IRQ_Handler
	#define	 IRQHANDLER_35					_Chip_ADC0_OVR_IRQ_Handler
	#define	 IRQHANDLER_36					_Chip_DMA0_IRQ_Handler
	#define	 IRQHANDLER_37					_Chip_I2C2_IRQ_Handler
	#define	 IRQHANDLER_38					_Chip_I2C3_IRQ_Handler
	#define	 IRQHANDLER_39					_Chip_CT32B0_IRQ_Handler
	#define	 IRQHANDLER_40					_Chip_PINT0_IRQ_Handler
	#define	 IRQHANDLER_41					_Chip_PINT1_IRQ_Handler
	#define	 IRQHANDLER_42					_Chip_PINT2_IRQ_Handler
	#define	 IRQHANDLER_43					_Chip_PINT3_IRQ_Handler
	#define	 IRQHANDLER_44					_Chip_PINT4_IRQ_Handler
	#define	 IRQHANDLER_45					_Chip_PINT5_IRQ_Handler
	#define	 IRQHANDLER_46					_Chip_PINT6_IRQ_Handler
	#define	 IRQHANDLER_47					_Chip_PINT7_IRQ_Handler
	
	#define _CHIP_CLOCK_FAIM_BASE 			(0x50010000U)
	#define	IAP_ENTRY_LOCATION        		0x0F001FF1								// Pointer to ROM IAP entry functions

	#define  _CHIP_DAC_COUNT				2										// Digital-to-Analog Converter (DAC)
	#define  _CHIP_I2C_COUNT				4										// A typical I2C-bus	
	#define  _CHIP_SPI_COUNT				2										// Serial Peripheral Interface
	#define  _CHIP_UART_COUNT				5										// Universal asynchronous receiver-transmitter
	#define  _CHIP_CAPTOUCH_COUNT			0										// Capacitive Touch
	#define  _CHIP_DMA_COUNT				1										// Number of Direct Memory Access periphery
	#define	 _CHIP_DMA_CHANNELS_COUNT		25										// number of channels per DMA
	#define  _CHIP_ADC_COUNT				1										// number of Analog-to-Digital Converter (ADC)
	#define	 _CHIP_ADC_CHANNELS_COUNT		12										// number of channels per ADC
	#define	 _CHIP_MRT_COUNT				1										// number of MultiRate Timer
	#define	 _CHIP_MRT_CHANNELS_COUNT		4										// number of channels per MRT	
	#define	 _CHIP_SCT_COUNT				1										// SC Timer
	
	#define	 _CHIP_EMUL_EEPROM_START		0x0000F000								// Start at page of Flash 960
	#define	 _CHIP_EMUL_EEPROM_PAGESIZE		64										// minimal page size for IAP FLASH erase/write
	#define	 _CHIP_EMUL_EEPROM_PAGESPERSECTOR	16									// number of pages per sector
	#define  _CHIP_EMUL_EEPROM_PAGE_NUM		4										// Virtual EEPROM pages number. Last two are not available because of the boot block.
	#define	 _CHIP_EMUL_EEPROM_SIZE			(_CHIP_EMUL_EEPROM_PAGESIZE * _CHIP_EMUL_EEPROM_PAGE_NUM) // byte size of emulated EEPROM in FLASH
	
	
	#define  _CHIP_GPIO_COUNT				2										// General Purpose Input/Output periphery number
				// GPIO 0.				  	  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,
	#define  _CHIP_IOCON_REMAP				{17,11, 6, 5, 4, 3,16,15,14,13, 8, 7, 2, 1,18,10, 9, 0,30,29,28,27,26,25,24,23,22,21,20,51,52,35, \
											 36,37,38,41,42,43,46,50,31,32};		// IOCON registers is not in ascending !!!
	#define	 _CHIP_GPIO_LAST_PIN			41										// Last GPIO PIN in the array above
	
#elif defined (CONF_CHIP_ID_LPC845M301JHI48)
	#define  CHIP_ID                    	0x00008453                 				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x4000									// size of internal RAM (datasheet value) - 16kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x10000									// size of internal FLASH (datasheet value) - 64kB
	
	#define	 IRQHANDLER_16					_Chip_SPI0_IRQ_Handler
	#define	 IRQHANDLER_17					_Chip_SPI1_IRQ_Handler
	#define	 IRQHANDLER_18					_Chip_DAC0_IRQ_Handler
	#define	 IRQHANDLER_19					_Chip_UART0_IRQ_Handler
	#define	 IRQHANDLER_20					_Chip_UART1_IRQ_Handler
	#define	 IRQHANDLER_21					_Chip_UART2_IRQ_Handler
	#define	 IRQHANDLER_22					_Chip_Default_IRQ_Handler				// this IRQ num is unused
	#define	 IRQHANDLER_23					_Chip_I2C1_IRQ_Handler
	#define	 IRQHANDLER_24					_Chip_I2C0_IRQ_Handler
	#define	 IRQHANDLER_25					_Chip_SCT0_IRQ_Handler
	#define	 IRQHANDLER_26					_Chip_MRT0_IRQ_Handler
	#define	 IRQHANDLER_27					_Chip_CMP0_IRQ_Handler					// this IRQ num is unused
	#define	 IRQHANDLER_28					_Chip_WDT0_IRQ_Handler
	#define  IRQHANDLER_29					_Chip_BOD0_IRQ_Handler
	#define  IRQHANDLER_30					_Chip_FLASH0_IRQ_Handler
	#define	 IRQHANDLER_31					_Chip_WKT0_IRQ_Handler
	#define	 IRQHANDLER_32					_Chip_ADC0_SEQA_IRQ_Handler
	#define	 IRQHANDLER_33					_Chip_ADC0_SEQB_IRQ_Handler
	#define	 IRQHANDLER_34					_Chip_ADC0_THCMP_IRQ_Handler
	#define	 IRQHANDLER_35					_Chip_ADC0_OVR_IRQ_Handler
	#define	 IRQHANDLER_36					_Chip_DMA0_IRQ_Handler
	#define	 IRQHANDLER_37					_Chip_I2C2_IRQ_Handler
	#define	 IRQHANDLER_38					_Chip_I2C3_IRQ_Handler
	#define	 IRQHANDLER_39					_Chip_CT32B0_IRQ_Handler
	#define	 IRQHANDLER_40					_Chip_PINT0_IRQ_Handler
	#define	 IRQHANDLER_41					_Chip_PINT1_IRQ_Handler
	#define	 IRQHANDLER_42					_Chip_PINT2_IRQ_Handler
	#define	 IRQHANDLER_43					_Chip_PINT3_IRQ_Handler
	#define	 IRQHANDLER_44					_Chip_PINT4_IRQ_Handler
	#define	 IRQHANDLER_45					_Chip_PINT5_IRQ_Handler
	#define	 IRQHANDLER_46					_Chip_PINT6_IRQ_Handler
	#define	 IRQHANDLER_47					_Chip_PINT7_IRQ_Handler
	
	#define _CHIP_CLOCK_FAIM_BASE 			(0x50010000U)
	#define	IAP_ENTRY_LOCATION        		0x0F001FF1								// Pointer to ROM IAP entry functions

	#define  _CHIP_DAC_COUNT				2										// Digital-to-Analog Converter (DAC)
	#define  _CHIP_I2C_COUNT				4										// A typical I2C-bus	
	#define  _CHIP_SPI_COUNT				2										// Serial Peripheral Interface
	#define  _CHIP_UART_COUNT				5										// Universal asynchronous receiver-transmitter
	#define  _CHIP_CAPTOUCH_COUNT			0										// Capacitive Touch
	#define  _CHIP_DMA_COUNT				1										// Number of Direct Memory Access periphery
	#define	 _CHIP_DMA_CHANNELS_COUNT		25										// number of channels per DMA
	#define  _CHIP_ADC_COUNT				1										// number of Analog-to-Digital Converter (ADC)
	#define	 _CHIP_ADC_CHANNELS_COUNT		12										// number of channels per ADC
	#define	 _CHIP_MRT_COUNT				1										// number of MultiRate Timer
	#define	 _CHIP_MRT_CHANNELS_COUNT		4										// number of channels per MRT	
	#define	 _CHIP_SCT_COUNT				1										// SC Timer
	
	#define	 _CHIP_EMUL_EEPROM_START		0x0000F000								// Start at page of Flash 960
	#define	 _CHIP_EMUL_EEPROM_PAGESIZE		64										// minimal page size for IAP FLASH erase/write
	#define	 _CHIP_EMUL_EEPROM_PAGESPERSECTOR	16									// number of pages per sector
	#define  _CHIP_EMUL_EEPROM_PAGE_NUM		4										// Virtual EEPROM pages number. Last two are not available because of the boot block.
	#define	 _CHIP_EMUL_EEPROM_SIZE			(_CHIP_EMUL_EEPROM_PAGESIZE * _CHIP_EMUL_EEPROM_PAGE_NUM) // byte size of emulated EEPROM in FLASH
	
	
	#define  _CHIP_GPIO_COUNT				2										// General Purpose Input/Output periphery number
				// GPIO 0.				  	  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,
	#define  _CHIP_IOCON_REMAP				{17,11, 6, 5, 4, 3,16,15,14,13, 8, 7, 2, 1,18,10, 9, 0,30,29,28,27,26,25,24,23,22,21,20,51,52,35, \
											 36,37,38,41,42,43,46,50,31,32};		// IOCON registers is not in ascending !!!
	#define	 _CHIP_GPIO_LAST_PIN			41										// Last GPIO PIN in the array above
	
#elif defined (CONF_CHIP_ID_LPC845M301JBD64)
	#define  CHIP_ID                    	0x00008451                 				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x4000									// size of internal RAM (datasheet value) - 16kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x10000									// size of internal FLASH (datasheet value) - 64kB
	#define  _CHIP_GPIO_COUNT				2										// General Purpose Input/Output periphery number
				// GPIO 0.				  	  0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,
	#define  _CHIP_IOCON_REMAP				{17,11, 6, 5, 4, 3,16,15,14,13, 8, 7, 2, 1,18,10, 9, 0,30,29,28,27,26,25,24,23,22,21,20,51,52,35, \
											 36,37,38,41,42,43,46,50,31,32,56,55,33,34,39,40,44,45,47,49,53,54};	// IOCON registers is not in ascending !!!
	#define	 _CHIP_GPIO_LAST_PIN			31+21									// Last GPIO PIN	
#elif defined (CONF_CHIP_ID_LPC8N04FHI24)
    #define  CHIP_ID                    	0x00008044                     			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM						0x1000									// size of internal RAM (datasheet value) - 4kB
	#define	 _CHIP_IRAMSTART				0x10000000								// Start of RAM
	#define	 _CHIP_IFLASH					0x8000									// size of internal FLASH (datasheet value) - 32kB
#else
	#error CONF_CHIP_ID_XXXXXXX not defined!
#endif

	

	
	
	
	
	
	
	
	
	
	
	
	
	// NOT LOADED from Scatter file :	
#ifndef LOAD_SCATTER	
	
#define PIN_INT5_IRQn                		PIN_INT5_DAC1_IRQn
#define PIN_INT6_IRQn						PIN_INT6_USART3_IRQn
#define PIN_INT7_IRQn						PIN_INT7_USART4_IRQn
	
	
extern uint32_t _Chip_Clock_Get_Fro_Rate(void);
extern uint32_t _Chip_Clock_Get_UARTClk_Rate(uint32_t UART_Index);
extern uint32_t _Chip_Clock_Get_I2CClk_Rate(uint32_t I2C_Index);
		
extern uint32_t _Chip_Clock_Get_SysPLL0_Inp_Rate(void);
extern uint32_t _Chip_Clock_Get_SysPLL0_Clk_Rate(void);
extern uint32_t _Chip_Clock_Get_ADCClk_Rate(uint32_t ADC_Index);
	
	
// defined (CONF_CHIP_ID_LPC844M201JHI33) || \
//	defined (CONF_CHIP_ID_LPC844M201JBD48) || \
//	defined (CONF_CHIP_ID_LPC844M201JHI48) || \
//	defined (CONF_CHIP_ID_LPC844M201JBD64) || \
//	defined (CONF_CHIP_ID_LPC845M301JHI33) || \
//	defined (CONF_CHIP_ID_LPC845M301JBD48) || \
//	defined (CONF_CHIP_ID_LPC845M301JHI48) || \
//	defined (CONF_CHIP_ID_LPC845M301JBD64)
	
		

//----------------------------------------------------------------------------
// Validate the the user's selctions
//----------------------------------------------------------------------------
#define CHECK_RANGE(val, min, max)			((val < min) || (val > max))
#define CHECK_RSVD(val, mask)				(val & mask)

//#if (CHECK_RANGE((CONF_CHIP_FROFREQ_VAL), 18, 30))
//   #error "CONF_CHIP_FROFREQ_VAL: Value out of range."
//#endif

#if (CHECK_RANGE(CONF_CHIP_EXTRATEIN, 1000000, 25000000)) && (CONF_CHIP_EXTRATEIN != 0)
   #error "CLKIN frequency is out of bounds"
#endif



// ******************************************************************************************************
// PUBLIC Values for all LPC8XX MCU's
// ******************************************************************************************************

#define CPU_NONISR_EXCEPTIONS   	    	(15)									// Cortex M0 exceptions /without SP!/
#define CPU_IRQ_NUMOF 				    	(32)									// Vendor and family specific external interrupts. See users manual!


// harmonizing names
// Some manufacturers header files, using different peripherial structure names. Now, we harmonize it into one names.
// now used from chip header file:		Harmonization to:
typedef GPIO_Type       					_CHIP_GPIO_T;
typedef ACOMP_Type            				_CHIP_ACMP_T;

typedef ADC_Type           					_CHIP_ADC_T;

#if defined(_CHIP_CAPT_COUNT) && (_CHIP_CAPT_COUNT > 0)
typedef CAPT_Type          					_CHIP_CAPT_T;
#endif

#if defined(_CHIP_CRC_COUNT) && (_CHIP_CRC_COUNT > 0)
typedef CRC_Type		             		_CHIP_CRC_T;
#endif

#if defined(_CHIP_CTIMER_COUNT) && (_CHIP_CTIMER_COUNT > 0)
typedef CTIMER_Type	            			_CHIP_TIMER_T;
#endif

#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 0)
typedef DAC_Type             				_CHIP_DAC_T;
#endif

#if defined(_CHIP_DMA_COUNT) && (_CHIP_DMA_COUNT > 0)
typedef	DMA_Type							_CHIP_DMA_T;
#endif

#if defined(_CHIP_SCT_COUNT) && (_CHIP_SCT_COUNT > 0)
typedef	SCT_Type							_CHIP_SCT_T;
#endif

#if defined(_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
typedef I2C_Type           					_CHIP_I2C_T;
#endif

#if defined(_CHIP_MRT_COUNT) && (_CHIP_MRT_COUNT > 0)
typedef MRT_Type            				_CHIP_MRT_T;
#endif

typedef IOCON_Type	          				_CHIP_IOCON_T;
typedef PINT_Type	        				_CHIP_PINT_T;

#if defined(_CHIP_PLU_COUNT) && (_CHIP_PLU_COUNT == 1)
typedef PLU_Type            				_CHIP_PLU_T;
#endif

typedef PMU_Type	            			_CHIP_PMU_T;
typedef SPI_Type            				_CHIP_SPI_T;
typedef SWM_Type	            			_CHIP_SWM_T;
typedef SYSCON_Type         				_CHIP_SYSCON_T;
typedef USART_Type          				_CHIP_UART_T;
typedef WKT_Type             				_CHIP_WKT_T;
typedef WWDT_Type           				_CHIP_WWDT_T;





// ******************************************************************************************************
// CHIP functions
// ******************************************************************************************************

enum	_swo_protocol 
{
  kSWO_ProtocolManchester = 				1U,
  kSWO_ProtocolNrz = 						2U
};

// Public functions exactly for this series of MCU, but uniform with CHAL System:
extern void _Chip_SWO_Init(uint32_t Main_CPU_Clock);
extern void _Chip_SystemInit(void);
extern void _Chip_Read_ResetSource(uint32_t *dst);




// ******************************************************************************************************
// IAP CHIP functions
// ******************************************************************************************************
#define _CHIP_IAP_PREWRRITE_CMD           	50										// Prepare sector for write operation command
#define _CHIP_IAP_WRISECTOR_CMD           	51										// Write Sector command
#define _CHIP_IAP_ERSSECTOR_CMD           	52										// Erase Sector command
#define _CHIP_IAP_BLANK_CHECK_SECTOR_CMD  	53										// Blank check sector
#define _CHIP_IAP_REPID_CMD               	54										// Read PartID command
#define _CHIP_IAP_READ_BOOT_CODE_CMD      	55										// Read Boot code version
#define _CHIP_IAP_COMPARE_CMD             	56										// Compare two RAM address locations
#define _CHIP_IAP_REINVOKE_ISP_CMD        	57										// Reinvoke ISP
#define _CHIP_IAP_READ_UID_CMD            	58										// Read UID
#define _CHIP_IAP_ERASE_PAGE_CMD          	59										// Erase page


#if	defined	( CONF_CHIP_ID_LPC844M201JBD64 ) || \
 	defined	( CONF_CHIP_ID_LPC844M201JBD48 ) || \
 	defined	( CONF_CHIP_ID_LPC844M201JHI48 ) || \
 	defined	( CONF_CHIP_ID_LPC844M201JHI33 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JBD64 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JBD48 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JHI48 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JHI33 )
#define	_CHIP_IAP_READFLASHSIGN_CMD			73										// Read Signature of the FLASH
#define	_CHIP_IAP_READFAIM_CMD				80										// Read FAIM Page
#define	_CHIP_IAP_WRITEFAIM_CMD				81										// Write FAIM Page

#define CLOCK_FRO_SETTING_API_ROM_ADDRESS 	(0x0F0026F5U)							// FRO clock setting API address in ROM.
extern inline void _Chip_IAP_SetFroOscFreq(uint32_t freq_in_khz);

extern bool _Chip_IAP_ReadFlashSign(uint32_t StartFlash, uint32_t EndFlash, uint32_t WaitStates);
extern bool _Chip_IAP_ReadFAIMPage(uint32_t PageNum, uint32_t *ptrFaimMirror);
extern bool _Chip_IAP_WriteFAIMPage(uint32_t PageNum, uint32_t *ptrFaimMirror);
#endif



typedef void (*IAP_ENTRY_T)(unsigned int[], unsigned int[]);

extern inline void _Chip_IAP_Entry(unsigned int cmd_param[], unsigned int status_result[])
{
	((IAP_ENTRY_T) IAP_ENTRY_LOCATION)(cmd_param, status_result);
}


// Public functions exactly for this series of MCU
extern bool _Chip_IAP_Read_ID(uint32_t *pDst);
extern bool _Chip_IAP_Read_SerialNum(uint32_t *pDst);
extern bool _Chip_IAP_Read_BootCodeVersion(uint32_t *pDst);
extern bool _Chip_IAP_PrepareSector(uint32_t strSector, uint32_t endSector);










 
// ******************************************************************************************************
// CLOCK functions
// ******************************************************************************************************
#if defined(CONF_SYS_WATCHDOG_USED) && (CONF_SYS_WATCHDOG_USED == 1)
static const uint8_t wdtFreqLookup[32] = {0,  8,  12, 15, 18, 20, 24, 26, 28, 30, 32, 34, 36, 38, 40, 41,
                                          42, 44, 45, 46, 48, 49, 50, 52, 53, 54, 56, 57, 58, 59, 60, 61};
#endif




// ------------------------------------------------------------------------------------------------
// "mainclk"
// return clock source for core sysclk
extern uint32_t	_Chip_Clock_Get_MainClk_Rate(void);
extern uint32_t _Chip_Clock_Get_SysClk_Rate(void);									// read system clock
extern uint32_t _Chip_Clock_Get_SourceClk_Rate(void);								// Get input source clock rate.
extern uint32_t _Chip_Clock_Get_CoreClk_Rate(void);
extern uint32_t _Chip_Clock_Get_ClkOut_Rate(void);

extern uint32_t  SystemCoreClock;													// for IAP - System Clock Frequency (Core Clock)
extern uint32_t _Chip_Clock_Get_SysPLL_Out_Rate(void);								// Get system pll output rate
extern uint32_t _Chip_Clock_Get_Wdt_Inp_Rate(void);
extern uint32_t _Chip_Clock_Get_SysPLL_In_Rate(void);								// Get system pll input rate
//extern uint32_t _Chip_Clock_Get_ADCClk_Rate(void);






// ******************************************************************************************************
// GPIO Functions
// ******************************************************************************************************
#define IOCON_MODE_PULLDOWN     			(0x1 << 3)								// Selects pull-down function
#define IOCON_MODE_PULLUP       			(0x2 << 3)								// Selects pull-up function
#define IOCON_MODE_REPEATER     			(0x3 << 3)								// Selects pin repeater function
#define IOCON_HYS_EN            			(0x1 << 5)								// Enables hysteresis
#define IOCON_INV_EN            			(0x1 << 6)								// Enables invert function on input
#define IOCON_OPENDRAIN_EN      			(0x1 << 10)								// Enables open-drain function
#define IOCON_S_MODE_0CLK       			(0x0 << 11)								// Bypass input filter
#define IOCON_S_MODE_1CLK       			(0x1 << 11)								// Input pulses shorter than 1 filter clock are rejected
#define IOCON_S_MODE_2CLK      		 		(0x2 << 11)								// Input pulses shorter than 2 filter clock2 are rejected
#define IOCON_S_MODE_3CLK       			(0x3 << 11)								// Input pulses shorter than 3 filter clock2 are rejected
#define IOCON_S_MODE(clks)      			((clks & 0x03) << 11)					// Select clocks for digital input filter mode
#define IOCON_CLKDIV(div)       			((div & 0x07) << 13)					// Select peripheral clock divider for input filter sampling clock, 2^n, n=0-6 



#define CHIP_PINSWMUNUSED					0xff, 0xff								// PIN not used - write 0xff into PINENABLE - default state
#define CHIP_PINENABLE(x)					x, 0xff									// write value x into PINENABLE
#define CHIP_PINASSIGN(x,y)					x, y									// write value y into PINASSIGN(x)


COMP_PACKED_BEGIN
typedef struct	_chip_IO_Spec														// Additional GPIO structure based on chip specific requirements */
{
	uint16_t								IOCON_Reg;								// IOCON register hodnota. pouziva sa len 16 bit. Inak IOCON je 32 bitovy	 */
	uint8_t									SWM_RegBlock;							// SWM - movable function/fixed function: vyber cisla registra ASSIGN/FIXED */
	uint8_t									SWM_PinNum;								// SWM - movable function/fixed function: Hodnota registra Assign. Ak 0xFF porom pis do FIXED */
}_Chip_IO_Spec_t;
COMP_PACKED_END


extern void*	_Chip_GPIO_Init ( int32_t PeriIndex, uint32_t Pin);
extern bool 	_Chip_GPIO_DeInit ( int32_t PeriIndex, uint32_t Pin);
extern void 	_Chip_GPIO_Wr_Conf_Pin (uint32_t Port, uint32_t Pin, uint8_t Reg_Idx,  _Chip_IO_Spec_t *SWMRegs);
extern void*	_Chip_GPIO_PINT_Conf(uint8_t PINTSel, uint32_t Port, uint32_t Pin, uint8_t Sens);
extern void*	_Chip_Get_GPIO_Ptr(int32_t PeriIndex);
extern void 	_Chip_GPIO_Set_PinDir(_CHIP_GPIO_T *pGPIO, uint8_t portNum, uint32_t bitValue, uint8_t out);
extern bool 	_Chip_GPIO_GetPinState(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin);
extern void 	_Chip_GPIO_SetPinOutLow(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin);
extern void 	_Chip_GPIO_SetPinOutHigh(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin);
extern void 	_Chip_GPIO_SetPinToggle(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin);











// ******************************************************************************************************
// I2C Functions
// ******************************************************************************************************

// Interrupt status flag defines:
#define	CHIP_I2C_IRQSTAT_MSTPENDING			(1 << 0)								// Master Pending flag
#define	CHIP_I2C_IRQSTAT_MSARBLOSS			(1 << 4)								// Master Arbitration Loss flag.
#define	CHIP_I2C_IRQSTAT_MSSTSTPERR			(1 << 6)								// Master Start/Stop Error flag
#define	CHIP_I2C_IRQSTAT_MSSTATE_CHANGED	5										// MSSTATE was changed - this flag mus be out of mask !!!
// namiesto MSTCHANGED by sa mal pouzit MSTPENDING
#define	CHIP_I2C_MSSTATE_MASK				0x0e
#define CHIP_I2C_MSSTATE_RX					(1 << 1)
#define CHIP_I2C_MSSTATE_TX					(2 << 1)
#define CHIP_I2C_MSSTATE_NACK_ADR			(3 << 1)
#define CHIP_I2C_MSSTATE_NACK_DATA			(4 << 1)
#define CHIP_I2C_IRQSTAT_MONRDY				(1 << 16)								// Monitor Ready Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_MONOV				(1 << 17)								// Monitor Overflow Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_MONIDLE			(1 << 19)								// Monitor Idle Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_SLVDESEL			(1 << 15)								// Slave Deselect Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_SLVNOTSTR			(1 << 11)								// Slave not stretching Clock Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_SLVPENDING			(1 << 8)								// Slave Pending Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_EVENTTIMEOUT		(1 << 24)								// Event Timeout Interrupt Flag
#define CHIP_I2C_IRQSTAT_SCLTIMEOUT			(1 << 25)								// SCL Timeout Interrupt Flag


typedef struct 
{
	const uint8_t 							*pTxBuff;								// Pointer to array of bytes to be transmitted
	uint8_t 								*pRxBuff;								// Pointer memory where bytes received from I2C be stored
	uint16_t 								TxSz;									// Number of bytes in transmit array, if 0 only receive transfer will be carried on
	uint16_t 								RxSz;									// Number of bytes to received, if 0 only transmission we be carried on
	uint16_t 								Status;									// Status of the current I2C transfer
	uint8_t 								SlaveAddr;								// 7-bit I2C Slave address
} _Chip_I2CM_XFER_T;


extern int32_t 	_Chip_I2C_Get_PeriIndex(void *pPeri);
extern void*	_Chip_I2C_Init(int32_t PeriIndex);									// init I2C by periphary number. Return is pointer to periphery if success
extern uint32_t _Chip_I2C_Get_BusSpeed(void *pPeri);								// Return Current bus speed
extern bool 	_Chip_I2C_Set_BusSpeed(void *pPeri, uint32_t Speed_kHz);			// Max Speed: 100kHz, 400kHz, 1MHz,...
extern bool 	_Chip_I2C_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool MonitorMode, uint16_t Address);
extern uint32_t _Chip_I2C_Get_Peri_Status(void *pPeri);
extern bool 	_Chip_I2C_Clear_Peri_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_I2CMST_Get_IRQ_Status(void *pPeri);
	
extern bool 	_Chip_I2CMST_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_I2CSLV_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_I2CMON_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_I2CMST_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_I2CSLV_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_I2CMON_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_I2CTimeout_Enable_IRQ_NVIC(void *pPeri, bool NewState);

extern uint32_t _Chip_I2CSLV_Get_IRQ_Status(void *pPeri);
extern uint32_t _Chip_I2CMON_Get_IRQ_Status(void *pPeri);
extern uint32_t _Chip_I2CTimeout_Get_IRQ_Status(void *pPeri);
extern bool 	_Chip_I2CMST_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CSLV_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CMON_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CTimeout_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CMST_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_I2CSLV_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_I2CMON_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_I2CTimeout_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void 	_Chip_I2CMST_Put_Data(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_I2CMST_Get_Data(void *pPeri);
extern void 	_Chip_I2CMST_Set_Start(void *pPeri);								// create start impulse
extern void 	_Chip_I2CMST_Set_Stop(void *pPeri);
extern void 	_Chip_I2CMST_Set_Cont(void *pPeri);
extern void 	_Chip_I2CMST_Reset(void *pPeri);







// ******************************************************************************************************
// UART Functions
// ******************************************************************************************************

// UART Interrupt STAT register definitions
#define CHIP_UART_IRQSTAT_RXRDY				USART_INTSTAT_RXRDY_MASK				// Receiver ready
#define CHIP_UART_IRQSTAT_TXRDY				USART_INTSTAT_TXRDY_MASK				// Transmitter ready for data
#define CHIP_UART_IRQSTAT_TXIDLE			USART_INTSTAT_TXIDLE_MASK				// Transmitter idle
//#define CHIP_UART_IRQSTAT_RXIDLE			USART_INTSTAT_RXIDLE_MASK				// Receiver idle

#define	CHIP_UART_PERISTAT_RXIDLE			USART_STAT_RXIDLE_MASK					// Periphery status bit
#define	CHIP_UART_PERISTAT_TXIDLE			USART_STAT_TXIDLE_MASK					// Periphery status bit
		




extern int32_t 	_Chip_UART_Get_PeriIndex(void *pPeri);
extern bool 	_Chip_UART_Set_AdrDet(void* pPeri, bool NewVal);
extern bool 	_Chip_UART_Set_Address(void* pPeri, uint8_t NewAddress);
extern uint8_t 	_Chip_UART_Get_Address(void* pPeri);
extern uint32_t _Chip_UART_Get_Peri_Status(void *pPeri);
extern bool 	_Chip_UART_Clear_Peri_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_UART_Get_IRQ_Status(void *pPeri);
extern bool 	_Chip_UART_Clear_IRQ_Status(void *pPeri,uint32_t IRQBitMask);
extern void 	_Chip_UART_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void 	_Chip_UART_Put_Data(void *pPeri, uint32_t WrData);
extern uint32_t _Chip_UART_Get_Data(void *pPeri);
extern void 	_Chip_UART_Flush_Tx(void *pPeri);
extern void 	_Chip_UART_Flush_Rx(void *pPeri);
extern void*	_Chip_UART_Init(int32_t PeriIndex);
extern bool 	_Chip_UART_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_UART_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_UART_Set_Baud(void* pPeri, uint32_t NewBaud);
extern uint32_t _Chip_UART_Get_Baud(void* pPeri);
extern bool 	_Chip_UART_Set_Conf(void* pPeri, uint8_t DataLen, uint8_t Parity, uint8_t StopBits);
extern bool 	_Chip_UART_Set_RTS_Conf(void *pPeri, uint8_t sRTS_Port, uint8_t sRTS_Pin);
extern bool 	_Chip_UART_Set_CTS_Conf(void *pPeri, uint8_t sCTS_Port, uint8_t sCTS_Pin);
extern bool 	_Chip_UART_Set_DIRDE_Conf(void *pPeri, uint8_t DIRDE_Port, uint8_t DIRDE_Pin, bool DIRDE_H_Active);
extern bool 	_Chip_UART_Set_RE_Conf(void *pPeri, uint8_t RE_Port, uint8_t RE_Pin, bool RE_H_Active);










// ******************************************************************************************************
// SPI Functions
// ******************************************************************************************************
#define	CHIP_SPI_STAT_MSTIDLE

#define	CHIP_SPI_IRQSTAT_MSTIDLE			SPI_INTSTAT_MSTIDLE_MASK
#define	CHIP_SPI_IRQSTAT_RXDONE				SPI_INTSTAT_RXRDY_MASK					// Receiver Done Flag new Data available
#define	CHIP_SPI_IRQSTAT_TXRDY				SPI_INTSTAT_TXRDY_MASK					// Transmitter Ready Flag
#define	CHIP_SPI_IRQSTAT_RXOV				SPI_INTSTAT_RXOV_MASK					// Receiver Overrun IRQ Flag
#define	CHIP_SPI_IRQSTAT_TXUR				SPI_INTSTAT_TXUR_MASK					// Transmitter underrun IRQ Flag
#define	CHIP_SPI_IRQSTAT_SSA				SPI_INTSTAT_SSA_MASK					// Slave Select Assert
#define	CHIP_SPI_IRQSTAT_SSD				SPI_INTSTAT_SSD_MASK					// Slave Select Deassert

#define	CHIP_SPI_IRQEN_RXDONE				SPI_INTENSET_RXRDYEN_MASK								// Receiver Done IRQ enable - new data available
#define	CHIP_SPI_IRQEN_TXRDY				SPI_INTENSET_TXRDYEN_MASK 								// Transmitter Ready IRQ enable - ready before send
#define	CHIP_SPI_IRQEN_RXOV					SPI_INTENSET_RXOVEN_MASK								// Receiver Overrun IRQ enable
#define	CHIP_SPI_IRQEN_TXUR					SPI_INTENSET_TXUREN_MASK								// Transmitter underrun IRQ enable
#define	CHIP_SPI_IRQEN_SSA					SPI_INTENSET_SSAEN_MASK								// Slave Select Assert IRQ enable
#define	CHIP_SPI_IRQEN_SSD					SPI_INTENSET_SSDEN_MASK								// Slave Select Deassert IRQ enable
#define CHIP_SPI_IRQEN_MSTIDLE				SPI_INTENSET_MSTIDLEEN_MASK

extern void*	_Chip_SPI_Init(int32_t PeriIndex);
extern bool 	_Chip_SPI_Enable(void *pPeri, bool NewState);
extern void 	_Chip_SPI_Set_ClockDiv(void *pPeri, uint32_t clkdiv);
extern bool 	_Chip_SPI_Set_BusSpeed(void *pPeri, uint32_t Speed_Hz);
extern uint32_t _Chip_SPI_Get_BusSpeed(void *pPeri);
extern bool 	_Chip_SPI_Configure(void *pPeri, uint32_t Speed_Hz, bool MasterMode, bool SlaveMode, bool LSB_First, bool CPHA, bool CPOL);
extern bool 	_Chip_SPI_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern int32_t 	_Chip_SPI_Get_PeriIndex(void *pPeri);
extern void 	_Chip_SPI_Put_Data(void *pPeri, uint32_t NewValue, uint8_t NumOfBits, uint32_t CtrlFlags, uint8_t SSel);
extern uint32_t _Chip_SPI_Get_Data(void *pPeri);
extern uint32_t _Chip_SPI_Get_IRQ_Status(void *pPeri);
extern bool 	_Chip_SPI_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_SPI_Get_Peri_Status(void *pPeri);
extern bool 	_Chip_SPI_Clear_Peri_Status(void *pPeri, uint32_t BitMask);
extern bool 	_Chip_SPI_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void 	_Chip_SPI_SendEOT(void *pPeri);
extern void 	_Chip_SPI_SendEOF(void *pPeri);
extern void 	_Chip_SPI_ReceiveIgnore(void *pPeri);
extern void 	_Chip_SPI_Flush_Tx(void *pPeri);
extern void 	_Chip_SPI_Flush_Rx(void *pPeri);





// ******************************************************************************************************
// USB Functions
// ******************************************************************************************************
// - not used -








typedef enum 
{
	ADC_SEQA_IDX,
	ADC_SEQB_IDX
} ADC_SEQ_IDX_T;


extern int32_t 	_Chip_ADC_Get_PeriIndex(void *pPeri);
extern void*	_Chip_ADC_Init(int32_t PeriIndex);
extern uint32_t _Chip_ADC_Get_ChannelMask(void *pPeri);
extern bool _Chip_ADC_Get_DataValidFlag_CH( void *pPeri, uint8_t Channel);
extern uint32_t _Chip_ADC_Get_Data_CH( void *pPeri, uint8_t Channel);
extern bool 	_Chip_ADC_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_ADC_Power(void *pPeri, bool NewState);

extern bool 	_Chip_ADC_Enable_CH(void* pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool 	_Chip_ADC_Configure_CH (void *pPeri, uint32_t ChannelBitMask, uint32_t Speed_kHz, bool RepeatMode, bool DMAMode);
extern uint32_t _Chip_ADC_Get_IRQ_Status_CH(_CHIP_ADC_T *pADC, uint32_t ChannelBitMask);
extern bool 	_Chip_ADC_Clear_IRQ_Status_CH(_CHIP_ADC_T *pADC, uint32_t ChannelBitMask);
extern void 	_Chip_ADC_Start_CH(void *pPeri, uint32_t ChannelBitMask);
extern bool 	_Chip_ADC_IRQ_Peri_Enable_CH(void *pPeri, uint32_t IRQBitMask, bool NewState);

extern bool 	_Chip_Temp_Enable(bool NewState);




// ******************************************************************************************************
// DAC Functions
// ******************************************************************************************************
extern int32_t 	_Chip_DAC_Get_PeriIndex(void *pPeri);
extern void*	_Chip_DAC_Init(int32_t PeriIndex);
extern bool 	_Chip_DAC_Enable(void* pPeri, bool NewState);
extern bool 	_Chip_DAC_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern void 	_Chip_DAC_Put_Data(void *pPeri, uint32_t WrData);
extern uint32_t _Chip_DAC_Get_Peri_Status(void *pPeri);
extern void 	_Chip_DAC_Clear_Peri_Status(void *pPeri, uint32_t IRQBitMask);
extern uint32_t _Chip_DAC_Get_IRQ_Status(void *pPeri);
extern void 	_Chip_DAC_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask);
extern bool 	_Chip_DAC_Configure(void *pPeri);









// ******************************************************************************************************
// TIMER Functions - MRTx
// ******************************************************************************************************
extern void*	_Chip_MRT_Init(int32_t PeriIndex);
extern bool 	_Chip_MRT_Enable_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool 	_Chip_MRT_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_MRT_Configure (void *pPeri, uint8_t Channel, uint32_t RateHz, bool RepeatMode);
extern void 	_Chip_MRT_Set_TmrVal(void *pPeri, uint8_t Channel, uint32_t TmrValue);
extern void 	_Chip_MRT_Reset_Tmr_CH(void *pPeri, uint32_t ChannelBitMask);
extern uint32_t _Chip_MRT_Get_TmrVal(void *pPeri, uint8_t Channel);
extern int32_t 	_Chip_MRT_Get_PeriIndex(void *pPeri);
extern bool 	_Chip_MRT_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask);
extern uint32_t _Chip_MRT_Get_IRQ_Status_CH(void *pPeri);
extern bool 	_Chip_MRT_Enable_IRQ_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState);

	
	
	
	







// ******************************************************************************************************
// DMA Functions
// ******************************************************************************************************
typedef struct
{
	uint32_t								SrcAdr;									// Source address
	uint16_t								SrcBSize;								// Source Burst size		  1/2/4 x Word
	uint8_t									SrcWSize;								// Source width: 8/16/32-bit
	uint8_t									SrcIncr;								// Source Increment
	uint32_t								SrcPeri;								// Source peripherial	
	uint32_t								DstAdr;									// Destination address
	uint16_t								DstBSize;								// Destination Burst size		  1/2/4 x Word
	uint8_t									DstWSize;								// Destination width: 8/16/32-bit
	uint8_t									DstIncr;								// Destination Increment
	uint32_t								DstPeri;								// Destination peripherial	
	uint8_t									XferIRQ;								// Select Interrupt generated after transfer
	uint32_t	 							XferLen;								// length of transfer
	uint8_t									XferType;								// transfer type
	uint8_t									XferPrio;								// transfer priority
	uint32_t 								XferNextDescriptor;						// address of next linked descriptor
} _Chip_DMA_Xfer_t;

// DMA channel source/address/next descriptor
typedef struct 
{
	uint32_t 								xfercfg;								// Transfer configuration (only used in linked lists and ping-pong configs)
	uint32_t  								source;									// DMA transfer source end address
	uint32_t  								dest;									// DMA transfer desintation end address
	uint32_t  								next;									// Link to next DMA descriptor, must be 16 byte aligned
} _Chip_DMA_CHDesc_t;

// DMA SRAM table - this can be optionally used with the Chip_DMA_SetSRAMBase()
//   function if a DMA SRAM table is needed.

extern void*	_Chip_DMA_Init(int32_t PeriIndex);
extern bool 	_Chip_DMA_Enable_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool 	_Chip_DMA_Configure (void *pPeri, uint8_t Channel, _Chip_DMA_Xfer_t *Xfer );
extern int32_t 	_Chip_DMA_Get_PeriIndex(void *pPeri);
extern bool 	_Chip_DMA_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask);
extern uint32_t _Chip_DMA_Get_IRQ_Status(void *pPeri);
extern uint32_t _Chip_DMA_Get_CHControl(void *pPeri, uint32_t Channel);
extern bool 	_Chip_DMA_Enable_IRQ_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState);






// ************************************************************************************************
// PWM Functions 
// ************************************************************************************************
// - not used -









// ******************************************************************************************************
// EEPROM Functions
// ******************************************************************************************************

// Emulated EEPROM 
typedef struct	Emul_EEPROM
{
	void									*pPeri;									// ptr to emulated Peripherial
	bool									Init;									// flag - initialization
	bool									Enabled;								// flag - enable/disabled
	bool									Busy;									// flag - busy, In used - wait for release
	uint32_t								OnePage[];								// page array in RAM - needs to read/modify/write access
} _Chip_Emul_EEPROM_t;

extern _Chip_Emul_EEPROM_t	Emul_EEPROM;

extern void*	_Chip_Emulated_EEPROM_Init(uint32_t PeriIndex);
extern bool 	_Chip_Emulated_EEPROM_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_Emulated_EEPROM_IRQ_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_Emulated_EEPROM_Write(void *pPeri, uint32_t EEDst, uint8_t *pInBuff, size_t byteswr);
extern bool 	_Chip_Emulated_EEPROM_Read(void *pPeri, uint32_t EESrc, uint8_t *pOutBuff, size_t bytesrd);










// ------------------------------------------------------------------------------------------------
// SCT Functions
// ------------------------------------------------------------------------------------------------

extern int32_t _Chip_SCT_Get_PeriIndex(void *pPeri);
extern void* _Chip_SCT_Init(int32_t PeriIndex);
extern bool _Chip_SCT_Enable(void *pPeri, bool NewState);
extern bool _Chip_SCT_Configure(void *pPeri);
	




// ******************************************************************************************************
// SWM Functions
// ******************************************************************************************************
extern void _Chip_SWM_MovablePinAssign(uint8_t Reg_NumBlock, uint8_t NewRegVal);
extern void _Chip_SWM_EnableFixedPin(uint8_t pin);
extern void _Chip_SWM_DisableFixedPin(uint8_t pin);
extern void _Chip_SWM_FixedPinEnable(uint8_t pin, bool enable);
extern bool _Chip_SWM_IsFixedPinEnabled(uint8_t pin);

// ******************************************************************************************************
// CRC Functions
// ******************************************************************************************************
extern void* _Chip_CRC_Init(int32_t PeriIndex);

#endif 	// ! LOAD_SCATTER

#endif	//__SERIE_LPC8XX_H_
