/*
  Header file LPC8xx.h is obsolete.

  Please include new device header file.
  Please note that new device header file is not backward compatible.
 */
 
#error "Header file LPC8xx.h is obsolete. Please include new device header file"
