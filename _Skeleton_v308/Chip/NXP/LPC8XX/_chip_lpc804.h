// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _chip_lpc804.h
// 	   Version: 2.0
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Special feature device depents, are included outside of this file (in parent header)        
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:   01.05.2014  - v1.0: first revision. 
//              06.05.2018  - v2.0: chip manufacturer rename, skeleton system re-directories 

#ifndef __CHIP_LPC804_H_
#define __CHIP_LPC804_H_


#ifndef LOAD_SCATTER
  #include    "LPC804.h" 														// Peripheral Access Layer Header File - provided by manufacturer (modified)
#endif

// ************************************************************************************************
// DEVICE depents Variables for LPC804 MCU's - for all packages
// ************************************************************************************************




#endif	//__CHIP_LPC804_H_
