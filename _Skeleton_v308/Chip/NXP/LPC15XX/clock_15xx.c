// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: clock_15xx.c
// 	   Version: 1.0
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: MCU LPC15xx Chip description/configuration file. Same for all MCUs of this family!
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//

/* Compute a PLL frequency */
STATIC uint32_t Chip_Clock_GetPLLFreq(uint32_t PLLReg, uint32_t inputRate)
{
	uint32_t msel = ((PLLReg & 0x3F) + 1);

	return inputRate * msel;
}

/* Return a PLL input (common) */
STATIC uint32_t Chip_Clock_GetPLLInClockRate(uint32_t reg)
{
	uint32_t clkRate;

	switch ((CHIP_SYSCON_PLLCLKSRC_T) (reg & 0x3)) {
	case SYSCON_PLLCLKSRC_IRC:
		clkRate = Chip_Clock_GetIntOscRate();
		break;

	case SYSCON_PLLCLKSRC_MAINOSC:
		clkRate = Chip_Clock_GetMainOscRate();
		break;

	default:
		clkRate = 0;
	}

	return clkRate;
}

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************


/* Return System PLL input clock rate */
uint32_t Chip_Clock_GetSystemPLLInClockRate(void)
{
	return Chip_Clock_GetPLLInClockRate(LPC_SYSCON->SYSPLLCLKSEL);
}

/* Return System PLL output clock rate */
uint32_t Chip_Clock_GetSystemPLLOutClockRate(void)
{
	return Chip_Clock_GetPLLFreq(LPC_SYSCON->SYSPLLCTRL,
								 Chip_Clock_GetSystemPLLInClockRate());
}

/* Return USB PLL input clock rate */
uint32_t Chip_Clock_GetUSBPLLInClockRate(void)
{
	return Chip_Clock_GetPLLInClockRate(LPC_SYSCON->USBPLLCLKSEL);
}

/* Return USB PLL output clock rate */
uint32_t Chip_Clock_GetUSBPLLOutClockRate(void)
{
	return Chip_Clock_GetPLLFreq(LPC_SYSCON->USBPLLCTRL,
								 Chip_Clock_GetUSBPLLInClockRate());
}

/* Return SCT PLL input clock rate */
uint32_t Chip_Clock_GetSCTPLLInClockRate(void)
{
	return Chip_Clock_GetPLLInClockRate(LPC_SYSCON->SCTPLLCLKSEL);
}

/* Return SCT PLL output clock rate */
uint32_t Chip_Clock_GetSCTPLLOutClockRate(void)
{
	return Chip_Clock_GetPLLFreq(LPC_SYSCON->SCTPLLCTRL,
								 Chip_Clock_GetSCTPLLInClockRate());
}

/* Return main A clock rate */
uint32_t Chip_Clock_GetMain_A_ClockRate(void)
{
	uint32_t clkRate = 0;

	switch (Chip_Clock_GetMain_A_ClockSource()) {
	case SYSCON_MAIN_A_CLKSRC_IRC:
		clkRate = Chip_Clock_GetIntOscRate();
		break;

	case SYSCON_MAIN_A_CLKSRCA_MAINOSC:
		clkRate = Chip_Clock_GetMainOscRate();
		break;

	case SYSCON_MAIN_A_CLKSRCA_WDTOSC:
		clkRate = Chip_Clock_GetWDTOSCRate();
		break;

	default:
		clkRate = 0;
		break;
	}

	return clkRate;
}

/* Return main B clock rate */
uint32_t Chip_Clock_GetMain_B_ClockRate(void)
{
	uint32_t clkRate = 0;

	switch (Chip_Clock_GetMain_B_ClockSource()) {
	case SYSCON_MAIN_B_CLKSRC_MAINCLKSELA:
		clkRate = Chip_Clock_GetMain_A_ClockRate();
		break;

	case SYSCON_MAIN_B_CLKSRC_SYSPLLIN:
		clkRate = Chip_Clock_GetSystemPLLInClockRate();
		break;

	case SYSCON_MAIN_B_CLKSRC_SYSPLLOUT:
		clkRate = Chip_Clock_GetSystemPLLOutClockRate();
		break;

	case SYSCON_MAIN_B_CLKSRC_RTC:
		clkRate = Chip_Clock_GetRTCOscRate();
		break;
	}

	return clkRate;
}

/* Set main system clock source */
void Chip_Clock_SetMainClockSource(CHIP_SYSCON_MAINCLKSRC_T src)
{
	uint32_t clkSrc = (uint32_t) src;

	if (clkSrc >= 4) {
		/* Main B source only, not using main A */
		Chip_Clock_SetMain_B_ClockSource((CHIP_SYSCON_MAIN_B_CLKSRC_T) (clkSrc - 4));
	}
	else {
		/* Select main A clock source and set main B source to use main A */
		Chip_Clock_SetMain_A_ClockSource((CHIP_SYSCON_MAIN_A_CLKSRC_T) clkSrc);
		Chip_Clock_SetMain_B_ClockSource(SYSCON_MAIN_B_CLKSRC_MAINCLKSELA);
	}
}

/* Returns the main clock source */
CHIP_SYSCON_MAINCLKSRC_T Chip_Clock_GetMainClockSource(void)
{
	CHIP_SYSCON_MAIN_B_CLKSRC_T srcB;
	uint32_t clkSrc;

	/* Get main B clock source */
	srcB = Chip_Clock_GetMain_B_ClockSource();
	if (srcB == SYSCON_MAIN_B_CLKSRC_MAINCLKSELA) {
		/* Using source A, so return source A */
		clkSrc = (uint32_t) Chip_Clock_GetMain_A_ClockSource();
	}
	else {
		/* Using source B */
		clkSrc = 4 + (uint32_t) srcB;
	}

	return (CHIP_SYSCON_MAINCLKSRC_T) clkSrc;
}

/* Return main clock rate */
uint32_t Chip_Clock_GetMainClockRate(void)
{
	uint32_t clkRate;

	if (Chip_Clock_GetMain_B_ClockSource() == SYSCON_MAIN_B_CLKSRC_MAINCLKSELA) {
		/* Return main A clock rate */
		clkRate = Chip_Clock_GetMain_A_ClockRate();
	}
	else {
		/* Return main B clock rate */
		clkRate = Chip_Clock_GetMain_B_ClockRate();
	}

	return clkRate;
}

/* Return ADC asynchronous clock rate */
uint32_t Chip_Clock_GetADCASYNCRate(void)
{
	uint32_t clkRate = 0;

	switch (Chip_Clock_GetADCASYNCSource()) {
	case SYSCON_ADCASYNCCLKSRC_IRC:
		clkRate = Chip_Clock_GetIntOscRate();
		break;

	case SYSCON_ADCASYNCCLKSRC_SYSPLLOUT:
		clkRate = Chip_Clock_GetSystemPLLOutClockRate();
		break;

	case SYSCON_ADCASYNCCLKSRC_USBPLLOUT:
		clkRate = Chip_Clock_GetUSBPLLOutClockRate();
		break;

	case SYSCON_ADCASYNCCLKSRC_SCTPLLOUT:
		clkRate = Chip_Clock_GetSCTPLLOutClockRate();
		break;
	}

	return clkRate;
}

/**
 * @brief	Set CLKOUT clock source and divider
 * @param	src	: Clock source for CLKOUT
 * @param	div	: divider for CLKOUT clock
 * @return	Nothing
 * @note	Use 0 to disable, or a divider value of 1 to 255. The CLKOUT clock
 * rate is the clock source divided by the divider. This function will
 * also toggle the clock source update register to update the clock
 * source.
 */
void Chip_Clock_SetCLKOUTSource(CHIP_SYSCON_CLKOUTSRC_T src, uint32_t div)
{
	uint32_t srcClk = (uint32_t) src;

	/* Use a clock A source? */
	if (src >= 4) {
		/* Not using a CLKOUT A source */
		LPC_SYSCON->CLKOUTSELB = srcClk - 4;
	}
	else {
		/* Using a clock A source, select A and then switch B to A */
		LPC_SYSCON->CLKOUTSELA = srcClk;
		LPC_SYSCON->CLKOUTSELB = 0;
	}

	LPC_SYSCON->CLKOUTDIV = div;
}

/* Enable a system or peripheral clock */
void Chip_Clock_EnablePeriphClock(CHIP_SYSCON_CLOCK_T clk)
{
	uint32_t clkEnab = (uint32_t) clk;

	if (clkEnab >= 32) {
		LPC_SYSCON->SYSAHBCLKCTRL1 |= (1 << (clkEnab - 32));
	}
	else {
		LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << clkEnab);
	}
}

/* Disable a system or peripheral clock */
void Chip_Clock_DisablePeriphClock(CHIP_SYSCON_CLOCK_T clk)
{
	uint32_t clkEnab = (uint32_t) clk;

	if (clkEnab >= 32) {
		LPC_SYSCON->SYSAHBCLKCTRL1 &= ~(1 << (clkEnab - 32));
	}
	else {
		LPC_SYSCON->SYSAHBCLKCTRL0 &= ~(1 << clkEnab);
	}
}


/* Return a system or peripheral clock */
bool Chip_Clock_GetPeriphClock(CHIP_SYSCON_CLOCK_T clk)
{
	uint32_t clkEnab = (uint32_t) clk;
	
	if (clkEnab >= 32) 
	{
		return ((LPC_SYSCON->SYSAHBCLKCTRL1 & (1 << (clkEnab - 32))) >> (clkEnab - 32));
	}
	else {
		return ((LPC_SYSCON->SYSAHBCLKCTRL0 & (1 << clkEnab)) >> (1 << clkEnab));
	}
}

/* Returns the system tick rate as used with the system tick divider */
uint32_t Chip_Clock_GetSysTickClockRate(void)
{
	uint32_t sysRate, div;

	div = Chip_Clock_GetSysTickClockDiv();

	/* If divider is 0, the system tick clock is disabled */
	if (div == 0) {
		sysRate = 0;
	}
	else {
		sysRate = Chip_Clock_GetMainClockRate() / div;
	}

	return sysRate;
}

/* Get UART base rate */
uint32_t Chip_Clock_GetUARTBaseClockRate(void)
{
	uint64_t inclk;
	uint32_t div;

	div = (uint32_t) Chip_Clock_GetUARTCLKDivider();
	if (div == 0) {
		/* Divider is 0 so UART clock is disabled */
		inclk = 0;
	}
	else {
		uint32_t mult, divmult;

		/* Input clock into FRG block is the divided main system clock */
		inclk = (uint64_t) (Chip_Clock_GetMainClockRate() / div);

		divmult = LPC_SYSCON->FRGCTRL & 0xFFFF;
		if ((divmult & 0xFF) == 0xFF) {
			/* Fractional part is enabled, get multiplier */
			mult = (divmult >> 8) & 0xFF;

			/* Get fractional error */
			inclk = (inclk * 256) / (uint64_t) (256 + mult);
		}
	}

	return (uint32_t) inclk;
}

/* Set UART base rate */
uint32_t Chip_Clock_SetUARTBaseClockRate(uint32_t rate, bool fEnable)
{
	uint32_t div, inclk;

	/* Input clock into FRG block is the main system cloock */
	inclk = Chip_Clock_GetMainClockRate();

	/* Get integer divider for coarse rate */
	div = inclk / rate;
	if (div == 0) {
		div = 1;
	}

	/* Approximated rate with only integer divider */
	Chip_Clock_SetUARTCLKDivider((uint8_t) div);

	if (fEnable) {
		uint32_t err;
		uint64_t uart_fra_multiplier;

		err = inclk - (rate * div);
		uart_fra_multiplier = ((uint64_t) err  * 256) / (uint64_t) (rate * div);

		/* Enable fractional divider and set multiplier */
		LPC_SYSCON->FRGCTRL = 0xFF | ((uart_fra_multiplier & 0xFF) << 8);
	}
	else {
		/* Disable fractional generator and use integer divider only */
		LPC_SYSCON->FRGCTRL = 0;
	}

	return Chip_Clock_GetUARTBaseClockRate();
}

/* Bypass System Oscillator and set oscillator frequency range */
void Chip_Clock_SetPLLBypass(bool bypass, bool highfr)
{
	uint32_t ctrl = 0;

	if (bypass) {
		ctrl |= (1 << 0);
	}
	if (highfr) {
		ctrl |= (1 << 1);
	}

	LPC_SYSCON->SYSOSCCTRL = ctrl;
}

/* Return system clock rate */
uint32_t Chip_Clock_GetSystemClockRate(void)
{
	/* No point in checking for divide by 0 */
	return Chip_Clock_GetMainClockRate() / LPC_SYSCON->SYSAHBCLKDIV;
}
