// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: adc_15xx.h
// 	   Version: 1.0
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: MCU LPC15xx Chip description/configuration file. Same for all MCUs of this family!
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//

// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************

/* Set ADC interrupt bits (safe) */
void Chip_ADC_SetIntBits(_CHIP_ADC_T *pADC, uint32_t intMask)
{
	uint32_t temp;

	/* Read and write values may not be the same, write 0 to undefined bits */
	temp = pADC->INTEN & 0x07FFFFFF;

	pADC->INTEN = temp | intMask;
}

/* Clear ADC interrupt bits (safe) */
void Chip_ADC_ClearIntBits(_CHIP_ADC_T *pADC, uint32_t intMask)
{
	uint32_t temp;

	/* Read and write values may not be the same, write 0 to undefined bits */
	temp = pADC->INTEN & 0x07FFFFFF;

	pADC->INTEN = temp & ~intMask;
}

/* Set ADC threshold selection bits (safe) */
void Chip_ADC_SetTHRSELBits(_CHIP_ADC_T *pADC, uint32_t mask)
{
	uint32_t temp;

	/* Read and write values may not be the same, write 0 to undefined bits */
	temp = pADC->CHAN_THRSEL & 0x00000FFF;

	pADC->CHAN_THRSEL = temp | mask;
}

/* Clear ADC threshold selection bits (safe) */
void Chip_ADC_ClearTHRSELBits(_CHIP_ADC_T *pADC, uint32_t mask)
{
	uint32_t temp;

	/* Read and write values may not be the same, write 0 to undefined bits */
	temp = pADC->CHAN_THRSEL & 0x00000FFF;

	pADC->CHAN_THRSEL = temp & ~mask;
}

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/* Initialize the ADC peripheral */
void Chip_ADC_Init(_CHIP_ADC_T *pADC, uint32_t flags)
{
	/* Power up ADC and enable ADC base clock */
	if (pADC == LPC_ADC0) {
		Chip_SYSCON_PowerUp(SYSCON_POWERDOWN_ADC0_PD);
		Chip_Clock_EnablePeriphClock(SYSCON_CLOCK_ADC0);
		Chip_SYSCON_PeriphReset(RESET_ADC0);
	}
	else {
		Chip_SYSCON_PowerUp(SYSCON_POWERDOWN_ADC1_PD);
		Chip_Clock_EnablePeriphClock(SYSCON_CLOCK_ADC1);
		Chip_SYSCON_PeriphReset(RESET_ADC1);
	}

	/* Disable ADC interrupts */
	pADC->INTEN = 0;

	/* Set ADC control options */
	pADC->CTRL = flags;
}

/* Shutdown ADC */
void Chip_ADC_DeInit(_CHIP_ADC_T *pADC)
{
	pADC->INTEN = 0;
	pADC->CTRL = 0;

	/* Stop ADC clock and then power down ADC */
	if (pADC == LPC_ADC0) {
		Chip_Clock_DisablePeriphClock(SYSCON_CLOCK_ADC0);
		Chip_SYSCON_PowerDown(SYSCON_POWERDOWN_ADC0_PD);
	}
	else {
		Chip_Clock_DisablePeriphClock(SYSCON_CLOCK_ADC1);
		Chip_SYSCON_PowerDown(SYSCON_POWERDOWN_ADC1_PD);
	}
}

/* Set ADC clock rate */
void Chip_ADC_SetClockRate(_CHIP_ADC_T *pADC, uint32_t rate)
{
	uint32_t div;

	/* Get ADC clock source to determine base ADC rate. IN sychronous mode,
	   the ADC base clock comes from the system clock. In ASYNC mode, it
	   comes from the ASYNC ADC clock and this function doesn't work. */
	div = Chip_Clock_GetSystemClockRate() / rate;
	if (div == 0) {
		div = 1;
	}

	Chip_ADC_SetDivider(pADC, (uint8_t) div - 1);
}

/* Start ADC calibration */
void Chip_ADC_StartCalibration(_CHIP_ADC_T *pADC)
{
	/* Set calibration mode */
	pADC->CTRL |= ADC_CR_CALMODEBIT;

	/* Clear ASYNC bit */
	pADC->CTRL &= ~ADC_CR_ASYNMODE;

	/* Setup ADC for about 500KHz (per UM) */
	Chip_ADC_SetClockRate(pADC, 500000);

	/* Clearn low power bit */
	pADC->CTRL &= ~ADC_CR_LPWRMODEBIT;

	/* Calibration is only complete when ADC_CR_CALMODEBIT bit has cleared */
}

/* Helper function for safely setting ADC sequencer register bits */
void Chip_ADC_SetSequencerBits(_CHIP_ADC_T *pADC, ADC_SEQ_IDX_T seqIndex, uint32_t bits)
{
	uint32_t temp;

	/* Read sequencer register and mask off bits 20..25 */
	temp = pADC->SEQ_CTRL[seqIndex] & ~(0x3F << 20);

	/* OR in passed bits */
	pADC->SEQ_CTRL[seqIndex] = temp | bits;
}

/* Helper function for safely clearing ADC sequencer register bits */
void Chip_ADC_ClearSequencerBits(_CHIP_ADC_T *pADC, ADC_SEQ_IDX_T seqIndex, uint32_t bits)
{
	uint32_t temp;

	/* Read sequencer register and mask off bits 20..25 */
	temp = pADC->SEQ_CTRL[seqIndex] & ~(0x3F << 20);

	/* OR in passed bits */
	pADC->SEQ_CTRL[seqIndex] = temp & ~bits;
}

/* Enable interrupts in ADC (sequencers A/B and overrun) */
void Chip_ADC_EnableInt(_CHIP_ADC_T *pADC, uint32_t intMask)
{
	Chip_ADC_SetIntBits(pADC, intMask);
}

/* Disable interrupts in ADC (sequencers A/B and overrun) */
void Chip_ADC_DisableInt(_CHIP_ADC_T *pADC, uint32_t intMask)
{
	Chip_ADC_ClearIntBits(pADC, intMask);
}

/* Enable a threshold event interrupt in ADC */
void Chip_ADC_SetThresholdInt(_CHIP_ADC_T *pADC, uint8_t ch, ADC_INTEN_THCMP_T thInt)
{
	int shiftIndex = 3 + (ch * 2);

	/* Clear current bits first */
	Chip_ADC_ClearIntBits(pADC, (ADC_INTEN_CMP_MASK << shiftIndex));

	/* Set new threshold interrupt type */
	Chip_ADC_SetIntBits(pADC, ((uint32_t) thInt << shiftIndex));
}

/* Select threshold 0 values for comparison for selected channels */
void Chip_ADC_SelectTH0Channels(_CHIP_ADC_T *pADC, uint32_t channels)
{
	Chip_ADC_ClearTHRSELBits(pADC, channels);
}

/* Select threshold 1 value for comparison for selected channels */
void Chip_ADC_SelectTH1Channels(_CHIP_ADC_T *pADC, uint32_t channels)
{
	Chip_ADC_SetTHRSELBits(pADC, channels);
}
