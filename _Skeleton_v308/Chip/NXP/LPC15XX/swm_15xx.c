// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: swm_15xx.c
// 	   Version: 1.0
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//	  Desc: SWM
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//

// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************
#define PINASSIGN_IDX(movable)  (((movable) >> 4))
#define PINSHIFT(movable)       (8 * ((movable) & (0xF)))

// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************

/* Assign movable pin function to physical pin in Switch Matrix */
void Chip_SWM_MovablePinAssign(CHIP_SWM_PIN_MOVABLE_T movable, uint8_t pin)
{
	uint32_t temp;
	int pinshift = PINSHIFT(movable), regIndex = PINASSIGN_IDX(movable);

//	temp = *(&LPC_SWM->PINASSIGN0 + (regIndex * sizeof(uint32_t))) & (~(0xFF << pinshift));
//	*(&LPC_SWM->PINASSIGN0 + (regIndex * sizeof(uint32_t))) = temp | (pin << pinshift);
	temp = *(&LPC_SWM->PINASSIGN0 + (regIndex )) & (~(0xFF << pinshift));
	*(&LPC_SWM->PINASSIGN0 + (regIndex )) = temp | (pin << pinshift);
	
}

/* Enables a fixed function pin in the Switch Matrix */
void Chip_SWM_EnableFixedPin(CHIP_SWM_PIN_FIXED_T pin)
{
	uint32_t regOff, pinPos;

	pinPos = ((uint32_t) pin) & 0x1F;
	regOff = ((uint32_t) pin) >> 7;

	/* Set low to enable fixed pin */
	//*(&LPC_SWM->PINENABLE0 + (regOff * sizeof(uint32_t))) &= ~(1 << pinPos);
	*(&LPC_SWM->PINENABLE0 + (regOff)) &= ~(1 << pinPos);
}

/* Disables a fixed function pin in the Switch Matrix */
void Chip_SWM_DisableFixedPin(CHIP_SWM_PIN_FIXED_T pin)
{
	uint32_t regOff, pinPos;

	pinPos = ((uint32_t) pin) & 0x1F;
	regOff = ((uint32_t) pin) >> 7;

	/* Set low to enable fixed pin */
	//*(&LPC_SWM->PINENABLE0 + (regOff * sizeof(uint32_t))) |= (1 << pinPos);
	*(&LPC_SWM->PINENABLE0 + (regOff)) |= (1 << pinPos);
}

/* Enables or disables a fixed function pin in the Switch Matrix */
void Chip_SWM_FixedPinEnable(CHIP_SWM_PIN_FIXED_T pin, bool enable)
{
	if (enable) {
		Chip_SWM_EnableFixedPin(pin);
	}
	else {
		Chip_SWM_DisableFixedPin(pin);
	}
}

/* Tests whether a fixed function pin is enabled or disabled in the Switch Matrix */
bool Chip_SWM_IsFixedPinEnabled(CHIP_SWM_PIN_FIXED_T pin)
{
	uint32_t regOff, pinPos;

	pinPos = ((uint32_t) pin) & 0x1F;
	regOff = ((uint32_t) pin) >> 7;

	//return (bool) ((*(&LPC_SWM->PINENABLE0 + (regOff * sizeof(uint32_t))) & (1 << pinPos)) == 0);
	return (bool) ((*(&LPC_SWM->PINENABLE0 + (regOff)) & (1 << pinPos)) == 0);
}
