// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _serie_lpc15xx.h
// 	   Version: 3.04
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: MCU of series LPC8xx description/configuration file. Same for all MCUs of this family!
//              Special, device depents features, can be included outside of this file (in parent header)        
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:   
//				- 2024.09.12 - v3.04 - Fixed upgrade to Skeleton v3
//				- 2020.05.23 - v3.03 - added WatchDog
//				- 2020.05.16 - 		 - UART bit definition renamed
//				- 2020.04.24 - v3.00 - upgrade to v3
//              - 2018.05.06 - v2.00 - chip manufacturer rename, skeleton system re-directories 
//				- 2014.05.01 - v1.00 - first revision. 





#ifndef __SERIE_15XX_H_
#define __SERIE_15XX_H_

#ifndef LOAD_SCATTER
    extern uint32_t SystemCoreClock;													// for IAP - System Clock Frequency (Core Clock)
    extern uint32_t	OscRateIn;
    extern uint32_t RTCOscRateIn;

    // for compatibility with mfg's header file
    typedef enum {ERROR = 0, SUCCESS = !ERROR} Status;
    typedef enum {RESET = 0, SET = !RESET} FlagStatus, IntStatus, SetState;
    typedef enum {DISABLE = 0, ENABLE = !DISABLE} FunctionalState;
    #define CORE_M3							1

    #include    "LPC15xx.h" 														// Peripheral Access Layer Header File - provided by manufacturer (modified)
#endif


	
	
	
	
	
	
// ------------------------------------------------------------------------------------------------
// PUBLIC Const Values for all LPC15XX MCU's
// ------------------------------------------------------------------------------------------------

#define	_CHIP_IRC_FREQUENCY				12000000UL									// Internal Osciltor: 12MHz
#define _CHIP_WDT_FREQUENCY				503000										// Fixed 503kHz +/- 40%

#define	 _CHIP_IRAMSTART				0x02000000									// Start of RAM
#define	 _CHIP_IEEPROM_START			0x03200000									// internal EEPROM start address
	
// for whole family LPC15xx:
#define  _CHIP_DAC_COUNT				1											// Digital-to-Analog Converter (DAC)
#define  _CHIP_I2C_COUNT				1											// A typical I2C-bus
#define  _CHIP_SPI_COUNT				2											// Serial Peripheral Interface
#define  _CHIP_UART_COUNT				3											// Universal asynchronous receiver-transmitter
#define  _CHIP_CAPTOUCH_COUNT			0											// Capacitive Touch
#define  _CHIP_DMA_COUNT				1											// Direct Memory Access
#define	 _CHIP_DMA_CHANNELS_COUNT		18											// number of channels per DMA
#define  _CHIP_ADC_COUNT				2											// Number of Analog-to-Digital Converters (ADC)
#define	 _CHIP_ADC_CHANNELS_COUNT		12											// number of channels per ADC
#define	 _CHIP_MRT_COUNT				1											// number of MultiRate Timer
#define	 _CHIP_MRT_CHANNELS_COUNT		4											// number of channels per MRT	
#define  _CHIP_SCT_COUNT				4											// State-Configurable Timers
#define  _CHIP_RTC_COUNT				1											// Real Time Clock (RTC)
//#define  _CHIP_RIT_COUNT				1											// Repetitive Interrupt Timer
//#define  _CHIP_HAVE_WDT					1											// Windowed Watchdog Timer (WWDT)
//#define  _CHIP_HAVE_QEI					1											// Quadrature Encoder Interface (QEI)
//#define  _CHIP_HAVE_CAN					1											// Controller Area Network (CAN) is the definition of a high performance communication protocol for serial data communication.
//#define  _CHIP_HAVE_ACMP				1											// Analog Comparator
//#define  _CHIP_HAVE_TEMP				1											// Temperature sensor
//#define  _CHIP_HAVE_CRC					1											// Cyclic Redundancy Check (CRC) generator
//#define  _CHIP_HAVE_SWMBLOCK			1											// Switch Matrix (SWM)


#define  _CHIP_MAXIMUM_CPU_CLK_FREQ		(72000000UL)								// maximum clock freq: 72 MHz
#define IAP_ENTRY_LOCATION        		0x03000205UL




// Specific parameters for each chip from this series:
#if defined( CONF_CHIP_ID_LPC1517JBD48 )
	// for LPC1517JBD48
    #define  CHIP_ID                    0x00001517                      			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x3000										// size of internal RAM (datasheet value) - 12kB
	#define	 _CHIP_IFLASH				0x10000										// size of internal FLASH (datasheet value) - 64kB
	#define	 _CHIP_IEEPROM				0x1000										// size of internal EEPROM (datasheet value) - 4kB
	#define  _CHIP_GPIO_COUNT			1											// General purpose IO	
	#define  _CHIP_USBD_COUNT			0											// Universal Serial Bus - Device
#elif defined( CONF_CHIP_ID_LPC1517JBD64 )
	// for LPC1517JBD64
    #define  CHIP_ID                    0x00001517                      			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x3000										// size of internal RAM (datasheet value) - 12kB
	#define	 _CHIP_IFLASH				0x10000										// size of internal FLASH (datasheet value) - 64kB
	#define	 _CHIP_IEEPROM				0x1000										// size of internal EEPROM (datasheet value) - 4kB
	#define  _CHIP_GPIO_COUNT			2											// General purpose IO	
	#define  _CHIP_USBD_COUNT			0											// Universal Serial Bus - Device
#elif defined( CONF_CHIP_ID_LPC1518JBD64 )
	// for LPC1518JBD64
    #define  CHIP_ID                    0x00001518                      			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x5000										// size of internal RAM (datasheet value) - 20kB
	#define	 _CHIP_IFLASH				0x20000										// size of internal FLASH (datasheet value) - 128kB
	#define	 _CHIP_IEEPROM				0x1000										// size of internal EEPROM (datasheet value) - 4kB
	#define  _CHIP_GPIO_COUNT			2											// General purpose IO	
	#define  _CHIP_USBD_COUNT			0											// Universal Serial Bus - Device
#elif defined( CONF_CHIP_ID_LPC1518JBD100 )
	// for LPC1518JBD100
    #define  CHIP_ID                    0x00001518                      			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x5000										// size of internal RAM (datasheet value) - 20kB
	#define	 _CHIP_IFLASH				0x20000										// size of internal FLASH (datasheet value) - 128kB
	#define	 _CHIP_IEEPROM				0x1000										// size of internal EEPROM (datasheet value) - 4kB	
	#define  _CHIP_GPIO_COUNT			3											// General purpose IO	
	#define  _CHIP_USBD_COUNT			0											// Universal Serial Bus - Device
#elif defined( CONF_CHIP_ID_LPC1519JBD64 )
	// for LPC1519JBD64
    #define  CHIP_ID                    0x00001519                      			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x9000										// size of internal RAM (datasheet value) - 36kB
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 _CHIP_IEEPROM				0x1000										// size of internal EEPROM (datasheet value) - 4kB
	#define  _CHIP_GPIO_COUNT			2											// General purpose IO	
	#define  _CHIP_USBD_COUNT			0											// Universal Serial Bus - Device
#elif defined( CONF_CHIP_ID_LPC1519JBD100 )
	// for LPC1519JBD100
    #define  CHIP_ID                    0x00001519                      			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x9000										// size of internal RAM (datasheet value) - 36kB
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 _CHIP_IEEPROM				0x1000										// size of internal EEPROM (datasheet value) - 4kB
	#define  _CHIP_GPIO_COUNT			3											// General purpose IO
	#define  _CHIP_USBD_COUNT			0											// Universal Serial Bus - Device
#elif defined( CONF_CHIP_ID_LPC1547JBD64 )
	// for LPC1547JBD64
    #define  CHIP_ID                    0x00001547                      			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x3000										// size of internal RAM (datasheet value) - 12kB
	#define	 _CHIP_IFLASH				0x10000										// size of internal FLASH (datasheet value) - 64kB
	#define	 _CHIP_IEEPROM				0x1000										// size of internal EEPROM (datasheet value) - 4kB
	#define  _CHIP_GPIO_COUNT			2											// General purpose IO	
	#define  _CHIP_USBD_COUNT			1											// Universal Serial Bus - Device
#elif defined( CONF_CHIP_ID_LPC1548JBD64 )
	// for LPC1548JBD64
    #define  CHIP_ID                    0x00001548                      			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x5000										// size of internal RAM (datasheet value) - 20kB
	#define	 _CHIP_IFLASH				0x20000										// size of internal FLASH (datasheet value) - 128kB
	#define	 _CHIP_IEEPROM				0x1000										// size of internal EEPROM (datasheet value) - 4kB
	#define  _CHIP_GPIO_COUNT			2											// General purpose IO	
	#define  _CHIP_USBD_COUNT			1											// Universal Serial Bus - Device
#elif defined( CONF_CHIP_ID_LPC1548JBD100 )
	// for LPC1548JBD100
    #define  CHIP_ID                    0x00001548                      			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x5000										// size of internal RAM (datasheet value) - 20kB
	#define	 _CHIP_IFLASH				0x20000										// size of internal FLASH (datasheet value) - 128kB
	#define	 _CHIP_IEEPROM				0x1000										// size of internal EEPROM (datasheet value) - 4kB
	#define  _CHIP_GPIO_COUNT			3											// General purpose IO
	#define  _CHIP_USBD_COUNT			1											// Universal Serial Bus - Device
#elif defined( CONF_CHIP_ID_LPC1549JBD48 )
	// for LPC1549JBD48
    #define  CHIP_ID                    0x00001549                      			// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x9000										// size of internal RAM (datasheet value) - 36kB
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB	
	#define	 _CHIP_IEEPROM				0x1000										// size of internal EEPROM (datasheet value) - 4kB
	#define  _CHIP_GPIO_COUNT			1											// General purpose IO
	#define  _CHIP_USBD_COUNT			1											// Universal Serial Bus - Device
#elif defined( CONF_CHIP_ID_LPC1549JBD64 )
	// for LPC1549JBD64
    #define  CHIP_ID                    0x00001549                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x9000										// size of internal RAM (datasheet value) - 36kB
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB	
	#define	 _CHIP_IEEPROM				0x1000										// size of internal EEPROM (datasheet value) - 4kB
	#define  _CHIP_GPIO_COUNT			2											// General purpose IO	
	#define  _CHIP_USBD_COUNT			1											// Universal Serial Bus - Device
#elif defined( CONF_CHIP_ID_LPC1549JBD100 )
	// for LPC1549JBD100
    #define  CHIP_ID                    0x00001549                     				// Chip ID wroted in silicone
	#define	 _CHIP_IRAM					0x9000										// size of internal RAM (datasheet value) - 36kB
	#define	 _CHIP_IFLASH				0x40000										// size of internal FLASH (datasheet value) - 256kB
	#define	 _CHIP_IEEPROM				0x1000										// size of internal EEPROM (datasheet value) - 4kB
	#define  _CHIP_GPIO_COUNT			3											// General purpose IO
	#define  _CHIP_USBD_COUNT			1											// Universal Serial Bus - Device
#else
	#error "Unknown selected device!"
#endif

	#define	 IRQHANDLER_16				_Chip_WDT0_IRQ_Handler
	#define	 IRQHANDLER_17				_Chip_BOD0_IRQ_Handler
	#define	 IRQHANDLER_18				_Chip_FLASH0_IRQ_Handler
	#define	 IRQHANDLER_19				_Chip_EEPROM0_IRQ_Handler
	#define	 IRQHANDLER_20				_Chip_DMA0_IRQ_Handler
	#define	 IRQHANDLER_21				_Chip_GINT0_IRQ_Handler
	#define	 IRQHANDLER_22				_Chip_GINT1_IRQ_Handler
	#define	 IRQHANDLER_23				_Chip_PINT0_IRQ_Handler
	#define	 IRQHANDLER_24				_Chip_PINT1_IRQ_Handler
	#define	 IRQHANDLER_25				_Chip_PINT2_IRQ_Handler
	#define	 IRQHANDLER_26				_Chip_PINT3_IRQ_Handler
	#define	 IRQHANDLER_27				_Chip_PINT4_IRQ_Handler
	#define	 IRQHANDLER_28				_Chip_PINT5_IRQ_Handler
	#define  IRQHANDLER_29				_Chip_PINT6_IRQ_Handler
	#define	 IRQHANDLER_30				_Chip_PINT7_IRQ_Handler
	#define	 IRQHANDLER_31				_Chip_RIT0_IRQ_Handler
	#define	 IRQHANDLER_32				_Chip_SCT0_IRQ_Handler
	#define	 IRQHANDLER_33				_Chip_SCT1_IRQ_Handler
	#define	 IRQHANDLER_34				_Chip_SCT2_IRQ_Handler
	#define	 IRQHANDLER_35				_Chip_SCT3_IRQ_Handler
	#define	 IRQHANDLER_36				_Chip_MRT0_IRQ_Handler
	#define	 IRQHANDLER_37				_Chip_UART0_IRQ_Handler
	#define	 IRQHANDLER_38				_Chip_UART1_IRQ_Handler
	#define	 IRQHANDLER_39				_Chip_UART2_IRQ_Handler
	#define	 IRQHANDLER_40				_Chip_I2C0_IRQ_Handler
	#define	 IRQHANDLER_41				_Chip_SPI0_IRQ_Handler
	#define	 IRQHANDLER_42				_Chip_SPI1_IRQ_Handler
	#define	 IRQHANDLER_43				_Chip_CAN0_IRQ_Handler
	#define	 IRQHANDLER_44				_Chip_USB0_IRQ_Handler
	#define	 IRQHANDLER_45				_Chip_USB0_FIQ_IRQ_Handler
	#define	 IRQHANDLER_46				_Chip_USB0_WAKE_IRQ_Handler
	#define	 IRQHANDLER_47				_Chip_ADC0_SEQA_IRQ_Handler
	#define	 IRQHANDLER_48				_Chip_ADC0_SEQB_IRQ_Handler
	#define	 IRQHANDLER_49				_Chip_ADC0_THCMP_IRQ_Handler
	#define	 IRQHANDLER_50				_Chip_ADC0_OVR_IRQ_Handler
	#define	 IRQHANDLER_51				_Chip_ADC1_SEQA_IRQ_Handler
	#define	 IRQHANDLER_52				_Chip_ADC1_SEQB_IRQ_Handler
	#define	 IRQHANDLER_53				_Chip_ADC1_THCMP_IRQ_Handler
	#define	 IRQHANDLER_54				_Chip_ADC1_OVR_IRQ_Handler	
	#define	 IRQHANDLER_55				_Chip_DAC0_IRQ_Handler
	#define	 IRQHANDLER_56				_Chip_CMP0_IRQ_Handler
	#define	 IRQHANDLER_57				_Chip_CMP1_IRQ_Handler
	#define	 IRQHANDLER_58				_Chip_CMP2_IRQ_Handler
	#define	 IRQHANDLER_59				_Chip_CMP3_IRQ_Handler
	#define	 IRQHANDLER_60				_Chip_QEI0_IRQ_Handler
	#define	 IRQHANDLER_61				_Chip_RTC0_ALARM_IRQ_Handler
	#define	 IRQHANDLER_62				_Chip_RTC0_WAKE_IRQ_Handler

#ifndef LOAD_SCATTER

// ------------------------------------------------------------------------------------------------
// Validate the the user's selctions
// ------------------------------------------------------------------------------------------------
#ifndef CONF_CHIP_OSCRATEIN
	#error "CONF_CHIP_OSCRATEIN not defined"
#endif
#ifndef CONF_CHIP_EXTRATEIN
	#error "CONF_CHIP_EXTRATEIN not defined"
#endif
#ifndef CONF_CHIP_RTCRATEIN
	#error "CONF_CHIP_RTCRATEIN not defined"
#endif

// ------------------------------------------------------------------------------------------------
// PUBLIC Values for all LPC15XX MCU's
// ------------------------------------------------------------------------------------------------

#define CPU_NONISR_EXCEPTIONS   	    (15)										// Cortex M3 exceptions /without SP!/
#define CPU_IRQ_NUMOF 				    (47)										// Vendor and family specific external interrupts. See users manual!


// harmonizing names
// Some manufacturers header files, using different peripherial structure names. Now, we harmonize it into one names.
// now used from chip header file:		Harmonization to:

typedef LPC_ACMP_Type            		_CHIP_ACMP_T;
typedef LPC_ADC0_Type            		_CHIP_ADC_T;
typedef LPC_C_CAN0_Type          		_CHIP_CAN_T;
typedef LPC_CRC_Type             		_CHIP_CRC_T;
typedef LPC_DAC_Type             		_CHIP_DAC_T;
typedef LPC_DMA_T	             		_CHIP_DMA_T;
typedef LPC_FLASHCTRL_Type       		_CHIP_FLASHCON_T;
typedef LPC_GINT0_Type           		_CHIP_GINT_T;
typedef LPC_GPIO_PORT_Type       		_CHIP_GPIO_T;
typedef LPC_I2C0_Type            		_CHIP_I2C_T;
typedef LPC_INMUX_Type           		_CHIP_INMUX_T;
typedef LPC_IOCON_Type           		_CHIP_IOCON_T;
typedef LPC_MRT_Type             		_CHIP_MRT_T;
typedef LPC_PINT_Type            		_CHIP_PINT_T;
typedef LPC_PMU_Type             		_CHIP_PMU_T;
typedef LPC_QEI_Type             		_CHIP_QEI_T;
typedef LPC_RIT_Type             		_CHIP_RIT_T;
typedef LPC_RTC_Type             		_CHIP_RTC_T;
typedef LPC_SCT_T            			_CHIP_SCT_T;
typedef LPC_SCTIPU_Type          		_CHIP_SCTIPU_T;
typedef LPC_SPI0_Type            		_CHIP_SPI_T;
typedef LPC_SWM_Type             		_CHIP_SWM_T;
typedef LPC_SYSCON_Type          		_CHIP_SYSCON_T;
typedef SysTick_Type					_CHIP_SYSTICK_T;
typedef LPC_USART0_Type          		_CHIP_UART_T;
typedef LPC_USB_Type             		_CHIP_USB_T;
typedef LPC_WWDT_Type            		_CHIP_WWDT_T;



//	Load peripherial low-level routines and reg/fun declarations:
//#include "romapi_15xx.h"															// ROM API declarations and functions
//#include "clock_15xx.h"																// CLOCK
//#include "iocon_15xx.h"																// IOCON
//#include "swm_15xx.h"																// SWM
//#include "gpio_15xx.h"																// GPIO
//#include "iap_15xx.h"																// IAP
//#include "uart_15xx.h"																// UART
//#include "adc_15xx.h"																// ADC
//#include "sct_15xx.h"																// SCT
//#include "i2c_15xx.h"																// I2C


// ------------------------------------------------------------------------------------------------
// IAP CHIP functions
// ------------------------------------------------------------------------------------------------

// IAP command definitions
#define _CHIP_IAP_PREWRRITE_CMD         50											// Prepare sector for write operation command
#define _CHIP_IAP_WRISECTOR_CMD         51											// Write Sector command
#define _CHIP_IAP_ERSSECTOR_CMD         52											// Erase Sector command
#define _CHIP_IAP_BLANK_CHECK_SECTOR_CMD  53										// Blank check sector
#define _CHIP_IAP_REPID_CMD             54											// Read PartID command
#define _CHIP_IAP_READ_BOOT_CODE_CMD    55											// Read Boot code version
#define _CHIP_IAP_COMPARE_CMD           56											// Compare two RAM address locations
#define _CHIP_IAP_REINVOKE_ISP_CMD      57											// Reinvoke ISP
#define _CHIP_IAP_READ_UID_CMD          58											// Read UID
#define _CHIP_IAP_ERASE_PAGE_CMD        59											// Erase page
#define _CHIP_IAP_EEPROM_WRITE_CMD      61											// EEPROM Write command
#define _CHIP_IAP_EEPROM_READ_CMD       62											// EEPROM READ command

//// IAP response definitions
//#define IAP_CMD_SUCCESS             	0											// Command is executed successfully
//#define IAP_INVALID_COMMAND         	1											// Invalid command
//#define IAP_SRC_ADDR_ERROR          	2											// Source address is not on word boundary
//#define IAP_DST_ADDR_ERROR          	3											// Destination address is not on a correct boundary
//#define IAP_SRC_ADDR_NOT_MAPPED     	4											// Source address is not mapped in the memory map
//#define IAP_DST_ADDR_NOT_MAPPED     	5											// Destination address is not mapped in the memory map
//#define IAP_COUNT_ERROR             	6											// Byte count is not multiple of 4 or is not a permitted value
//#define IAP_INVALID_SECTOR          	7											// Sector number is invalid or end sector number is greater than start sector number
//#define IAP_SECTOR_NOT_BLANK        	8											// Sector is not blank
//#define IAP_SECTOR_NOT_PREPARED     	9											// Command to prepare sector for write operation was not executed
//#define IAP_COMPARE_ERROR           	10											// Source and destination data not equal
//#define IAP_BUSY                    	11											// Flash programming hardware interface is busy
//#define IAP_PARAM_ERROR             	12											// Insufficient number of parameters or invalid parameter
//#define IAP_ADDR_ERROR              	13											// Address is not on word boundary
//#define IAP_ADDR_NOT_MAPPED         	14											// Address is not mapped in the memory map

//#define IAP_CMD_LOCKED              	15											// Command is locked
//#define IAP_INVALID_CODE            	16											// Unlock code is invalid
//#define IAP_INVALID_BAUD_RATE       	17											// Invalid baud rate setting
//#define IAP_INVALID_STOP_BIT        	18											// Invalid stop bit setting
//#define IAP_CRP_ENABLED             	19											// Code read protection enabled

// IAP_ENTRY API function type
typedef void (*IAP_ENTRY_T)(unsigned int[], unsigned int[]);

static inline void _Chip_IAP_Entry(unsigned int cmd_param[], unsigned int status_result[])
{
	((IAP_ENTRY_T) IAP_ENTRY_LOCATION)(cmd_param, status_result);
}

// ------------------------------------------------------------------------------------------------
// CHIP functions
// ------------------------------------------------------------------------------------------------
// Public functions exactly for this series of MCU
extern bool _Chip_IAP_Read_ID(uint32_t *pDst);
extern bool _Chip_IAP_Read_SerialNum(uint32_t *pDst);
extern bool _Chip_IAP_Read_BootCodeVersion(uint32_t *pDst);
extern uint32_t _Chip_Clock_Get_MainClk_Rate(void);


// ------------------------------------------------------------------------------------------------
// init ITM Cortex Engine
extern inline void _Chip_SWO_Init(uint32_t Main_CPU_Clock)
{
// DEBUG INIT:
#if (CONF_DEBUG_ITM == 1) && (__CORTEX_M >= 3)										// debug with ITM support
	ITM_RxBuffer = ITM_RXBUFFER_EMPTY;  											//  CMSIS Debug Input

	LPC_SYSCON->TRACECLKDIV = 1;													// ARM trace clock divider register set to 1 - run it.

	CoreDebug->DEMCR = CoreDebug_DEMCR_TRCENA_Msk;									// Enable Trace (Debug Exception and Monitor Control Registe

	ITM->LAR = 0xC5ACCE55;															// Enable write to ITM registers
	
	if(~(ITM->LSR & 0x02))															// Accesss unlocked?
	{
		ITM->TPR = 0;																// allow unprivilege access
		ITM->TCR = ITM_TCR_ITMENA_Msk | ITM_TCR_SYNCENA_Msk| ITM_TCR_TraceBusID_Msk | ITM_TCR_DWTENA_Msk
#if defined(BSP_DEBUG_SWO) && (BSP_DEBUG_SWO == 1)
		| ITM_TCR_SWOENA_Msk;
		ITM->TER = 1U << 0;															// ITM Port #0 enable for SWO
#else
		;
		ITM->TER = 0;																// ITM Port #0 disable for SWO
#endif		
		
	}
#endif //CONF_DEBUG_ITM
	
}









// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// Call System Init - in reset vector!
extern inline void _Chip_SystemInit(void)											// inserted into SystemInit
{
//	SCnSCB->ACTLR |= 1 << SCnSCB_ACTLR_DISDEFWBUF_Pos;								// disable write buffering - DEBUG ONLY !!!! 

//	SCB->SHCSR |= SCB_SHCSR_USGFAULTENA_Msk
//		| SCB_SHCSR_BUSFAULTENA_Msk
//		| SCB_SHCSR_MEMFAULTENA_Msk; 												// enable Usage-, Bus-, and MMU Fault	

	
	// ISR napln vector table !!
	LPC_SYSCON->SYSAHBCLKCTRL0 |= 1<<1;												// Turn ON ROM
	LPC_SYSCON->SYSAHBCLKCTRL0 |= 1<<3;												// Turn ON SRAM1
	LPC_SYSCON->SYSAHBCLKCTRL0 |= 1<<4;												// Turn ON SRAM2
	
	// SWD and Trace are default off
	LPC_SYSCON->SYSAHBCLKCTRL0 |= 1<<12;											// activate CLK for SWM - bit 12
	
	// SWM RESET :
	LPC_SWM->PINASSIGN0 = 0xffffffff;
	LPC_SWM->PINASSIGN1 = 0xffffffff;
	LPC_SWM->PINASSIGN2 = 0xffffffff;
	LPC_SWM->PINASSIGN3 = 0xffffffff;
	LPC_SWM->PINASSIGN4 = 0xffffffff;
	LPC_SWM->PINASSIGN5 = 0xffffffff;
	LPC_SWM->PINASSIGN6 = 0xffffffff;
	LPC_SWM->PINASSIGN7 = 0xffffffff;
	LPC_SWM->PINASSIGN8 = 0xffffffff;
	LPC_SWM->PINASSIGN9 = 0xffffffff;
	LPC_SWM->PINASSIGN10 = 0xffffffff;
	LPC_SWM->PINASSIGN11 = 0xffffffff;
	LPC_SWM->PINASSIGN12 = 0xffffffff;
	LPC_SWM->PINASSIGN13 = 0xffffffff;
	LPC_SWM->PINASSIGN14 = 0xffffffff;
	LPC_SWM->PINASSIGN15 = 0xffffffff;
	LPC_SWM->PINENABLE0 = 0xffffffff;
	//LPC_SWM->PINENABLE1 = 0xffffffff;
	LPC_SWM->PINENABLE1 = 0xff1fffff;
	//LPC_SWM->PINENABLE1 &= ~(1<<21);												// Enable RESET pin
	
	LPC_SYSCON->SYSAHBCLKCTRL0 &= ~(1<<12);											// deactivate CLK for SWM - bit 12

}

// ------------------------------------------------------------------------------------------------
// get reset signal sources
extern inline void _Chip_Read_ResetSource(uint32_t *dst)							// Get reset sources (Chip_RST_SRC)
{
    *dst = LPC_SYSCON->SYSRSTSTAT;													// format as CHAL_RST_SRC_t
	LPC_SYSCON->SYSRSTSTAT |= *dst;													// clear reset sources.
}


// ------------------------------------------------------------------------------------------------
// CLOCK functions
// ------------------------------------------------------------------------------------------------
extern void _Chip_Clock_Set_USB_PLLFreq(uint32_t pllM, uint32_t pllP);






// ------------------------------------------------------------------------------------------------
// GPIO functions
// ------------------------------------------------------------------------------------------------

// IOCON function and mode selection definitions
// See the User Manual for specific modes and functions supported by the
// various LPC15XX pins.
#define IOCON_FUNC0             		0x0											// Selects pin function 0
#define IOCON_FUNC1             		0x1											// Selects pin function 1
#define IOCON_FUNC2             		0x2											// Selects pin function 2
#define IOCON_MODE_INACT        		(0x0 << 3)									// No addition pin function
#define IOCON_MODE_PULLDOWN     		(0x1 << 3)									// Selects pull-down function
#define IOCON_MODE_PULLUP       		(0x2 << 3)									// Selects pull-up function
#define IOCON_MODE_REPEATER     		(0x3 << 3)									// Selects pin repeater function
#define IOCON_HYS_EN            		(0x1 << 5)									// Enables hysteresis
#define IOCON_INV_EN            		(0x1 << 6)									// Enables invert function on input
#define IOCON_ADMODE_EN         		(0x0 << 7)									// Enables analog input function (analog pins only)
#define IOCON_DIGMODE_EN        		(0x1 << 7)									// Enables digital function (analog pins only)
#define IOCON_SFI2C_EN          		(0x0 << 8)									// I2C standard mode/fast-mode
#define IOCON_STDI2C_EN         		(0x1 << 8)									// I2C standard I/O functionality
#define IOCON_FASTI2C_EN        		(0x2 << 8)									// I2C Fast-mode Plus
#define IOCON_OPENDRAIN_EN      		(0x1 << 10)									// Enables open-drain function
#define IOCON_S_MODE_0CLK       		(0x0 << 11)									// Bypass input filter
#define IOCON_S_MODE_1CLK       		(0x1 << 11)									// Input pulses shorter than 1 filter clock are rejected
#define IOCON_S_MODE_2CLK      		 	(0x2 << 11)									// Input pulses shorter than 2 filter clock2 are rejected
#define IOCON_S_MODE_3CLK       		(0x3 << 11)									// Input pulses shorter than 3 filter clock2 are rejected
#define IOCON_S_MODE(clks)      		((clks) << 11)								// Select clocks for digital input filter mode
#define IOCON_CLKDIV(div)       		((div) << 13)								// Select peripheral clock divider for input filter sampling clock, 2^n, n=0-6 

#define PFUN_GPIO						0x0000

// For IOCON Block, Pin function
#define CHIP_PINSWMUNUSED				0xff, 0xff									// PIN not used - write 0xff into PINENABLE - default state
#define CHIP_PINENABLE(x)				x, 0xff										// write value x into PINENABLE
#define CHIP_PINASSIGN(x,y)				x, y										// write value y into PINASSIGN(x)

COMP_PACKED_BEGIN
typedef struct	_chip_IO_Spec														// Additional GPIO structure based on chip specific requirements.
{
	uint16_t							IOCON_Reg;									// IOCON register hodnota. pouziva sa len 16 bit. Inak IOCON je 32 bitovy	.
	uint8_t								SWM_Reg;									// SWM - movable function/fixed function: vyber cisla registra ASSIGN/FIXED.
	uint8_t								SWM_Pin;									// SWM - movable function/fixed function: Hodnota registra Assign. Ak 0xFF porom pis do FIXED.
}_Chip_IO_Spec_t;
COMP_PACKED_END

extern void *_Chip_GPIO_Init ( int32_t PeriIndex, uint32_t Pin);
extern bool _Chip_GPIO_DeInit ( int32_t PeriIndex, uint32_t Pin);
extern void *_Chip_GPIO_IRQ_Conf( uint8_t Group, uint32_t Port, uint32_t Pin, uint8_t Sens);
extern bool _Chip_GPIO_IRQ_Enable(void *pPeri, uint32_t Port, uint32_t Pin, bool NewState);

extern void *_Chip_GPIO_GINT_Conf(uint8_t Group, uint32_t Port, uint32_t Pin, uint8_t Sens);
extern void *_Chip_GPIO_PINT_Conf(uint8_t PINTSel, uint32_t Port, uint32_t Pin, uint8_t Sens);

// ------------------------------------------------------------------------------------------------
// Get ptr from index
static inline void *_Chip_Get_GPIO_Ptr(uint8_t PeriIndex)							// vrat pointer na konkretnu GPIO periferiu - LPC15xx pouziva iba jeten ptr
{
    return(LPC_GPIO_PORT);                                                          // LPC15xx have only one port	
}


// ------------------------------------------------------------------------------------------------
// Set Direction for a GPIO port 
extern inline void _Chip_GPIO_Set_PinDir(_CHIP_GPIO_T *pGPIO, uint8_t portNum, uint32_t bitValue, uint8_t out)
{
	if (out) pGPIO->DIR[portNum] |= bitValue;
	else pGPIO->DIR[portNum] &= ~bitValue;
}

// ------------------------------------------------------------------------------------------------
// Read Pin Value
extern inline bool _Chip_GPIO_GetPinState(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin)
{
	if(pGPIO->B[(port * 32) + pin]) return(true);
	else return false;
}

// ------------------------------------------------------------------------------------------------
// Clear Pin
extern inline void _Chip_GPIO_SetPinOutLow(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin)
{
	pGPIO->CLR[port] = (1 << pin);
}

// ------------------------------------------------------------------------------------------------
// Set Pin
extern inline void _Chip_GPIO_SetPinOutHigh(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin)
{
	pGPIO->SET[port] = (1 << pin);
}

// ------------------------------------------------------------------------------------------------
// Toggle Pin
extern inline void _Chip_GPIO_SetPinToggle(_CHIP_GPIO_T *pGPIO, uint8_t port, uint8_t pin)
{
	pGPIO->NOT[port] = (1 << pin);
}

// ------------------------------------------------------------------------------------------------
// Enable/Disable PINT IRQ
extern inline bool _Chip_GPIO_PINT_IRQ_Enable(void *pPeri, uint32_t Port, uint32_t Pin, bool NewState)
{
	ERR_BREAK;			// needs to complette this function
	return(false);
}



// ******************************************************************************************************
// I2C Functions
// ******************************************************************************************************

//// I2C Configuration register Bit definition
//#define I2C_CFG_MSTEN             		(1 << 0)									// Master Enable/Disable Bit
//#define I2C_CFG_SLVEN             		(1 << 1)									// Slave Enable/Disable Bit
//#define I2C_CFG_MONEN             		(1 << 2)									// Monitor Enable/Disable Bit
//#define I2C_CFG_TIMEOUTEN         		(1 << 3)									// Timeout Enable/Disable Bit
//#define I2C_CFG_MONCLKSTR         		(1 << 4)									// Monitor Clock Stretching Bit
//#define I2C_CFG_MASK              		((uint32_t) 0x1F)							// Configuration register Mask

// Interrupt status flag defines:
#define	CHIP_I2C_IRQSTAT_MSTPENDING		(1 << 0)									// Master Pending flag
#define	CHIP_I2C_IRQSTAT_MSARBLOSS		(1 << 4)									// Master Arbitration Loss flag.
#define	CHIP_I2C_IRQSTAT_MSSTSTPERR		(1 << 6)									// Master Start/Stop Error flag
#define	CHIP_I2C_IRQSTAT_MSSTATE_CHANGED	5										// MSSTATE was changed - this flag mus be out of mask !!!
// namiesto MSTCHANGED by sa mal pouzit MSTPENDING
#define	CHIP_I2C_MSSTATE_MASK	0x0e
#define CHIP_I2C_MSSTATE_RX				(1 << 1)
#define CHIP_I2C_MSSTATE_TX				(2 << 1)
#define CHIP_I2C_MSSTATE_NACK_ADR		(3 << 1)
#define CHIP_I2C_MSSTATE_NACK_DATA		(4 << 1)
#define CHIP_I2C_IRQSTAT_MONRDY			(1 << 16)									// Monitor Ready Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_MONOV			(1 << 17)									// Monitor Overflow Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_MONIDLE		(1 << 19)									// Monitor Idle Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_SLVDESEL		(1 << 15)									// Slave Deselect Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_SLVNOTSTR		(1 << 11)									// Slave not stretching Clock Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_SLVPENDING		(1 << 8)									// Slave Pending Interrupt Status Bit
#define CHIP_I2C_IRQSTAT_EVENTTIMEOUT	(1 << 24)									// Event Timeout Interrupt Flag
#define CHIP_I2C_IRQSTAT_SCLTIMEOUT		(1 << 25)									// SCL Timeout Interrupt Flag

extern int32_t 	_Chip_I2C_Get_PeriIndex(void *pPeri);
extern void*	_Chip_I2C_Init(int32_t PeriIndex);									// init I2C by periphary number. Return is pointer to periphery if success
extern uint32_t _Chip_I2C_Get_BusSpeed(void *pPeri);								// Return Current bus speed
extern bool 	_Chip_I2C_Set_BusSpeed(void *pPeri, uint32_t Speed_kHz);			// Max Speed: 100kHz, 400kHz, 1MHz,...
extern bool 	_Chip_I2C_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool MonitorMode, uint16_t Address);
extern uint32_t _Chip_I2C_Get_Peri_Status(void *pPeri);
extern bool 	_Chip_I2C_Clear_Peri_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_I2CMST_Get_IRQ_Status(void *pPeri);
	
extern bool 	_Chip_I2CMST_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_I2CSLV_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_I2CMON_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_I2CMST_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_I2CSLV_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_I2CMON_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_I2CTimeout_Enable_IRQ_NVIC(void *pPeri, bool NewState);

extern uint32_t _Chip_I2CSLV_Get_IRQ_Status(void *pPeri);
extern uint32_t _Chip_I2CMON_Get_IRQ_Status(void *pPeri);
extern uint32_t _Chip_I2CTimeout_Get_IRQ_Status(void *pPeri);
extern bool 	_Chip_I2CMST_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CSLV_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CMON_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CTimeout_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern bool 	_Chip_I2CMST_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_I2CSLV_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_I2CMON_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_I2CTimeout_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void 	_Chip_I2CMST_Put_Data(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_I2CMST_Get_Data(void *pPeri);
extern void 	_Chip_I2CMST_Set_Start(void *pPeri);								// create start impulse
extern void 	_Chip_I2CMST_Set_Stop(void *pPeri);
extern void 	_Chip_I2CMST_Set_Cont(void *pPeri);
extern void 	_Chip_I2CMST_Reset(void *pPeri);

typedef struct 
{
	const uint8_t 						*pTxBuff;									// Pointer to array of bytes to be transmitted
	uint8_t 							*pRxBuff;									// Pointer memory where bytes received from I2C be stored
	uint16_t 							TxSz;										// Number of bytes in transmit array, if 0 only receive transfer will be carried on
	uint16_t 							RxSz;										// Number of bytes to received, if 0 only transmission we be carried on
	uint16_t 							Status;										// Status of the current I2C transfer
	uint8_t 							SlaveAddr;									// 7-bit I2C Slave address
} _Chip_I2CM_XFER_T;






// ******************************************************************************************************
// UART Functions
// ******************************************************************************************************

// UART Interrupt STAT register definitions
#define CHIP_UART_IRQSTAT_RXRDY			(0x01 << 0)									// Receiver ready
#define CHIP_UART_IRQSTAT_TXRDY			(0x01 << 2)									// Transmitter ready for data
#define CHIP_UART_IRQSTAT_TXIDLE		(0x01 << 3)									// Transmitter idle
#define CHIP_UART_IRQSTAT_RXIDLE		CHIP_UART_IRQSTAT_TXIDLE					// Receiver idle

#define CHIP_UART_IRQSTAT_DELTACTS		(0x01 << 5)									// Change in CTS state
#define CHIP_UART_IRQSTAT_TXDISINT		(0x01 << 6)									// Transmitter disabled
#define CHIP_UART_IRQSTAT_OVERRUNINT	(0x01 << 8)									// Overrun Error interrupt flag.
#define CHIP_UART_IRQSTAT_DELTARXBRK	(0x01 << 11)								// Change in receive break detection
#define CHIP_UART_IRQSTAT_START			(0x01 << 12)								// Start detected
#define CHIP_UART_IRQSTAT_FRAMERRINT	(0x01 << 13)								// Framing Error interrupt flag
#define CHIP_UART_IRQSTAT_PARITYERRINT	(0x01 << 14)								// Parity Error interrupt flag
#define CHIP_UART_IRQSTAT_RXNOISEINT	(0x01 << 15)								// Received Noise interrupt flag
#define CHIP_UART_IRQSTAT_ABERRINT		(0x01 << 16)								// AutoBaud err interrupt flag

// UART Interrupt enable/disable register definitions
#define CHIP_UART_IRQEN_RXRDY			(0x01 << 0)									// Receiver ready
#define CHIP_UART_IRQEN_TXRDY			(0x01 << 2)									// Transmitter ready for data
#define CHIP_UART_IRQEN_TXIDLE			(0x01 << 3)									// Transmitter idle
#define CHIP_UART_IRQEN_RXIDLE			CHIP_UART_IRQEN_TXIDLE						// Receiverer idle

#define CHIP_UART_IRQEN_DELTACTS		(0x01 << 5)									// Change in CTS state
#define CHIP_UART_IRQEN_TXDISINT		(0x01 << 6)									// Transmitter disabled
#define CHIP_UART_IRQEN_OVERRUNINT		(0x01 << 8)									// Overrun Error interrupt flag.
#define CHIP_UART_IRQEN_DELTARXBRK		(0x01 << 11)								// Change in receive break detection
#define CHIP_UART_IRQEN_START			(0x01 << 12)								// Start detected
#define CHIP_UART_IRQEN_FRAMERRINT		(0x01 << 13)								// Framing Error interrupt flag
#define CHIP_UART_IRQEN_PARITYERRINT	(0x01 << 14)								// Parity Error interrupt flag
#define CHIP_UART_IRQEN_RXNOISEINT		(0x01 << 15)								// Received Noise interrupt flag
#define CHIP_UART_IRQEN_ABERRINT		(0x01 << 16)								// AutoBaud err interrupt flag

// Interrupt status flag defines:
#define	CHIP_UART_IRQSTAT_TXREADY			0x0001									// TX ready before transsmiting
#define	CHIP_UART_IRQSTAT_TXDONE			0x0002									// TX Data was Transsmited
#define	CHIP_UART_IRQSTAT_RXREADY			0x0004									// RX ready before receiving
#define	CHIP_UART_IRQSTAT_RXDONE			0x0008									// RX new data available

extern int32_t 	_Chip_UART_Get_PeriIndex(void *pPeri);
extern bool 	_Chip_UART_Set_AdrDet(void* pPeri, bool NewVal);
extern bool 	_Chip_UART_Set_Address(void* pPeri, uint8_t NewAddress);
extern uint8_t 	_Chip_UART_Get_Address(void* pPeri);
extern uint32_t _Chip_UART_Get_Peri_Status(void *pPeri);
extern bool 	_Chip_UART_Clear_Peri_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_UART_Get_IRQ_Status(void *pPeri);
extern bool 	_Chip_UART_Clear_IRQ_Status(void *pPeri,uint32_t IRQBitMask);
extern void 	_Chip_UART_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void 	_Chip_UART_Put_Data(void *pPeri, uint32_t WrData);
extern uint32_t _Chip_UART_Get_Data(void *pPeri);
extern void 	_Chip_UART_Flush_Tx(void *pPeri);
extern void 	_Chip_UART_Flush_Rx(void *pPeri);
extern void*	_Chip_UART_Init(int32_t PeriIndex);
extern bool 	_Chip_UART_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_UART_Enable(void *pPeri, bool NewState);
extern bool 	_Chip_UART_Set_Baud(void* pPeri, uint32_t NewBaud);
extern uint32_t _Chip_UART_Get_Baud(void* pPeri);
extern bool 	_Chip_UART_Set_Conf(void* pPeri, uint8_t DataLen, uint8_t Parity, uint8_t StopBits);
extern bool 	_Chip_UART_Set_RTS_Conf(void *pPeri, uint8_t sRTS_Port, uint8_t sRTS_Pin);
extern bool 	_Chip_UART_Set_CTS_Conf(void *pPeri, uint8_t sCTS_Port, uint8_t sCTS_Pin);
extern bool 	_Chip_UART_Set_DIRDE_Conf(void *pPeri, uint8_t DIRDE_Port, uint8_t DIRDE_Pin, bool DIRDE_H_Active);
extern bool 	_Chip_UART_Set_RE_Conf(void *pPeri, uint8_t RE_Port, uint8_t RE_Pin, bool RE_H_Active);











//// ------------------------------------------------------------------------------------------------
//static inline void _Chip_UART_IRQTx_Enable (void *pPeri, bool NewValue)				// Enable/Disable Tx Interrupts
//{
//	if(NewValue)
//	{
//		((_CHIP_UART_T*)pPeri)->INTENSET |= (1 << 2);								// Enable TX interrupt
//	}
//	else
//	{
//		((_CHIP_UART_T*)pPeri)->INTENCLR |= (1 << 2);								// Disable TX interrupt
//	}
//}

//// ------------------------------------------------------------------------------------------------
//static inline void _Chip_UART_IRQRx_Enable (void *pPeri, bool NewValue)				// Enable/Disable Rx Interrupts 
//{
//	if(NewValue)
//	{
//		((_CHIP_UART_T*)pPeri)->INTENSET |= (1 << 0);								// Enable RX interrupt
//	}
//	else
//	{
//		((_CHIP_UART_T*)pPeri)->INTENCLR |= (1 << 0);								// Disable RX interrupt
//	}
//}


//// ToDo: rozlis FIFO a neFIFO MCU....
//// ------------------------------------------------------------------------------------------------
//// Get UART FIFO interrupt status
//extern inline uint32_t _Chip_UART_Get_FIFOIRQStatus(void *pPeri)
//{
//	return(0);
//}

//// ------------------------------------------------------------------------------------------------
//// Clear UART FIFO interrupt status - clear by disable and reenable selected interrupts.
//extern inline bool _Chip_UART_Clear_FIFOIRQStatus(void *pPeri, uint32_t NewValue)
//{
//	return(true);																	// 
//}




// ******************************************************************************************************
// SPI Functions
// ******************************************************************************************************
#define	CHIP_SPI_IRQSTAT_RXDONE							(1 << 0)					// Receiver Done Flag new Data available
#define	CHIP_SPI_IRQSTAT_TXRDY							(1 << 1)					// Transmitter Ready Flag
#define	CHIP_SPI_IRQSTAT_RXOV							(1 << 2)					// Receiver Overrun IRQ Flag
#define	CHIP_SPI_IRQSTAT_TXUR							(1 << 3)					// Transmitter underrun IRQ Flag
#define	CHIP_SPI_IRQSTAT_SSA							(1 << 4)					// Slave Select Assert
#define	CHIP_SPI_IRQSTAT_SSD							(1 << 5)					// Slave Select Deassert

#define	CHIP_SPI_IRQEN_RXDONE							(1 << 0)					// Receiver Done IRQ enable - new data available
#define	CHIP_SPI_IRQEN_TXRDY							(1 << 1) 					// Transmitter Ready IRQ enable - ready before send
#define	CHIP_SPI_IRQEN_RXOV								(1 << 2)					// Receiver Overrun IRQ enable
#define	CHIP_SPI_IRQEN_TXUR								(1 << 3)					// Transmitter underrun IRQ enable
#define	CHIP_SPI_IRQEN_SSA								(1 << 4)					// Slave Select Assert IRQ enable
#define	CHIP_SPI_IRQEN_SSD								(1 << 5)					// Slave Select Deassert IRQ enable


extern void*	_Chip_SPI_Init(int32_t PeriIndex);
extern bool 	_Chip_SPI_Enable(void *pPeri, bool NewState);
extern void 	_Chip_SPI_Set_ClockDiv(void *pPeri, uint32_t clkdiv);
extern bool 	_Chip_SPI_Set_BusSpeed(void *pPeri, uint32_t Speed_Hz);
extern uint32_t _Chip_SPI_Get_BusSpeed(void *pPeri);
extern bool 	_Chip_SPI_Configure(void *pPeri, uint32_t Speed_Hz, bool MasterMode, bool SlaveMode, bool LSB_First, bool CPHA, bool CPOL);
extern bool 	_Chip_SPI_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern int32_t 	_Chip_SPI_Get_PeriIndex(void *pPeri);
extern void 	_Chip_SPI_Put_Data(void *pPeri, uint32_t NewValue, uint8_t NumOfBits, uint32_t CtrlFlags, uint8_t SSel);
extern uint32_t _Chip_SPI_Get_Data(void *pPeri);
extern uint32_t _Chip_SPI_Get_IRQ_Status(void *pPeri);
extern bool 	_Chip_SPI_Clear_IRQ_Status(void *pPeri, uint32_t NewValue);
extern uint32_t _Chip_SPI_Get_Peri_Status(void *pPeri);
extern bool 	_Chip_SPI_Clear_Peri_Status(void *pPeri, uint32_t BitMask);

extern bool 	_Chip_SPI_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern void 	_Chip_SPI_SendEOT(void *pPeri);
extern void 	_Chip_SPI_SendEOF(void *pPeri);
extern void 	_Chip_SPI_ReceiveIgnore(void *pPeri);
extern void 	_Chip_SPI_Flush_Tx(void *pPeri);
extern void 	_Chip_SPI_Flush_Rx(void *pPeri);



// ------------------------------------------------------------------------------------------------
// USB Functions
// ------------------------------------------------------------------------------------------------
extern void *_Chip_USB_Init(int32_t PeriIndex);
extern bool _Chip_USB_Enable(void *pPeri, bool NewState);
extern bool _Chip_USB_Configure(void *pPeri, bool DeviceMode, bool HostMode, bool OTGMode);
extern uint32_t _Chip_USB_Get_IRQStatus(void *pPeri);

// ------------------------------------------------------------------------------------------------
// return USB periphery index
static inline int32_t _Chip_USB_Get_PeriIndex(void *pPeri)							// Return Index from pointer
{
	if((_CHIP_USB_T*) pPeri == LPC_USB) return(0);
	return(-1);																		// otherwise error
}


// ------------------------------------------------------------------------------------------------
// Power down / up
extern inline bool _Chip_USB_Power(void *pPeri, bool NewState)
{
	if(NewState) LPC_SYSCON->PDRUNCFG &= !(1 << 9);									// Enable Power for periphery
	else  LPC_SYSCON->PDRUNCFG |= (1 << 9);											// Disable Power for periphery
	
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Read USB interrupt status
extern inline uint32_t _Chip_USB_Get_IRQStatus(void *pPeri)
{
	return(LPC_USB->INTSTAT);
}






// ******************************************************************************************************
// ADC Functions
// ******************************************************************************************************
// SEQ_CTRL register bit fields 
#define ADC_SEQ_CTRL_HWTRIG_POLPOS		(1 << 18)									// HW trigger polarity - positive edge
#define ADC_SEQ_CTRL_HWTRIG_SYNCBYPASS	(1 << 19)									// HW trigger bypass synchronisation
#define ADC_SEQ_CTRL_START				(1 << 26)									// Start conversion enable bit
#define ADC_SEQ_CTRL_BURST				(1 << 27)									// Repeated conversion enable bit
#define ADC_SEQ_CTRL_SINGLESTEP			(1 << 28)									// Single step enable bit
#define ADC_SEQ_CTRL_LOWPRIO			(1 << 29)									// High priority enable bit (regardless of name)
#define ADC_SEQ_CTRL_MODE_EOS			(1 << 30)									// Mode End of sequence enable bit
#define ADC_SEQ_CTRL_SEQ_ENA			(1UL << 31)									// Sequence enable bit

// ADC Control register bit fields.
#define ADC_CR_CLKDIV_MASK      		(0xFF << 0)									// Mask for Clock divider value
#define ADC_CR_CLKDIV_BITPOS    		(0)											// Bit position for Clock divider value
#define ADC_CR_ASYNMODE         		(1 << 8)									// Asynchronous mode enable bit
#define ADC_CR_MODE10BIT        		(1 << 9)									// 10-bit mode enable bit
#define ADC_CR_LPWRMODEBIT      		(1 << 10)									// Low power mode enable bit
#define ADC_CR_CALMODEBIT       		(1 << 30)									// Self calibration cycle enable bit
#define ADC_CR_BITACC(n)        		((((n) & 0x1) << 9))						// 12-bit or 10-bit ADC accuracy
#define ADC_CR_CLKDIV(n)        		((((n) & 0xFF) << 0))						// The APB clock (PCLK) is divided by (this value plus one) to produce the clock for the A/D
#define ADC_SAMPLE_RATE_CONFIG_MASK (ADC_CR_CLKDIV(0xFF) | ADC_CR_BITACC(0x01))

// ADC hardware trigger sources in SEQ_CTRL for ADC0 only. These sources should
// only be used when selecting trigger sources for ADC0.
#define ADC0_SEQ_CTRL_HWTRIG_ADC0_PIN_TRIG0 (0 << 12)								// HW trigger input - ADC0_PIN_TRIG0.
#define ADC0_SEQ_CTRL_HWTRIG_ADC0_PIN_TRIG1 (1 << 12)								// HW trigger input - ADC0_PIN_TRIG1.
#define ADC0_SEQ_CTRL_HWTRIG_SCT0_OUT7	(2 << 12)									// HW trigger input - SCT0_OUT7.
#define ADC0_SEQ_CTRL_HWTRIG_SCT0_OUT9	(3 << 12)									// HW trigger input - SCT0_OUT9.
#define ADC0_SEQ_CTRL_HWTRIG_SCT1_OUT7	(4 << 12)									// HW trigger input - SCT1_OUT7.
#define ADC0_SEQ_CTRL_HWTRIG_SCT1_OUT9	(5 << 12)									// HW trigger input - SCT1_OUT9.
#define ADC0_SEQ_CTRL_HWTRIG_SCT2_OUT3	(6 << 12)									// HW trigger input - SCT2_OUT3.
#define ADC0_SEQ_CTRL_HWTRIG_SCT2_OUT4	(7 << 12)									// HW trigger input - SCT2_OUT4.
#define ADC0_SEQ_CTRL_HWTRIG_SCT3_OUT3	(8 << 12)									// HW trigger input - SCT3_OUT3.
#define ADC0_SEQ_CTRL_HWTRIG_SCT3_OUT4	(9 << 12)									// HW trigger input - SCT3_OUT4.
#define ADC0_SEQ_CTRL_HWTRIG_ACMP0_O	(10 << 12)									// HW trigger input - ACMP0_O.
#define ADC0_SEQ_CTRL_HWTRIG_ACMP1_O	(11 << 12)									// HW trigger input - ACMP1_O.
#define ADC0_SEQ_CTRL_HWTRIG_ACMP2_O	(12 << 12)									// HW trigger input - ACMP2_O.
#define ADC0_SEQ_CTRL_HWTRIG_ACMP3_O	(13 << 12)									// HW trigger input - ACMP3_O.
#define ADC0_SEQ_CTRL_HWTRIG_MASK		(0x3F << 12)								// HW trigger input bitfield mask.

// ADC hardware trigger sources in SEQ_CTRL for ADC1 only. These sources should
// only be used when selecting trigger sources for ADC1.
#define ADC1_SEQ_CTRL_HWTRIG_ADC1_PIN_TRIG0 (0 << 12)								// HW trigger input - ADC1_PIN_TRIG0.
#define ADC1_SEQ_CTRL_HWTRIG_ADC1_PIN_TRIG1 (1 << 12)								// HW trigger input - ADC1_PIN_TRIG1.
#define ADC1_SEQ_CTRL_HWTRIG_SCT0_OUT6	(2 << 12)									// HW trigger input - SCT0_OUT6.
#define ADC1_SEQ_CTRL_HWTRIG_SCT0_OUT9	(3 << 12)									// HW trigger input - SCT0_OUT9.
#define ADC1_SEQ_CTRL_HWTRIG_SCT1_OUT8	(4 << 12)									// HW trigger input - SCT1_OUT8.
#define ADC1_SEQ_CTRL_HWTRIG_SCT1_OUT9	(5 << 12)									// HW trigger input - SCT1_OUT9.
#define ADC1_SEQ_CTRL_HWTRIG_SCT2_OUT2	(6 << 12)									// HW trigger input - SCT2_OUT2.
#define ADC1_SEQ_CTRL_HWTRIG_SCT2_OUT5	(7 << 12)									// HW trigger input - SCT2_OUT5.
#define ADC1_SEQ_CTRL_HWTRIG_SCT3_OUT2	(8 << 12)									// HW trigger input - SCT3_OUT2.
#define ADC1_SEQ_CTRL_HWTRIG_SCT3_OUT5	(9 << 12)									// HW trigger input - SCT3_OUT5.
#define ADC1_SEQ_CTRL_HWTRIG_ACMP0_O	(10 << 12)									// HW trigger input - ACMP0_O.
#define ADC1_SEQ_CTRL_HWTRIG_ACMP1_O	(11 << 12)									// HW trigger input - ACMP1_O.
#define ADC1_SEQ_CTRL_HWTRIG_ACMP2_O	(12 << 12)									// HW trigger input - ACMP2_O.
#define ADC1_SEQ_CTRL_HWTRIG_ACMP3_O	(13 << 12)									// HW trigger input - ACMP3_O.
#define ADC1_SEQ_CTRL_HWTRIG_MASK		(0x3F << 12)								// HW trigger input bitfield mask.

#define ADC_DAT_RESULT_MASK                      (0xFFF0U)
#define ADC_DAT_RESULT_SHIFT                     (4U)
#define ADC_DAT_DATAVALID_MASK                   (0x80000000U)
#define ADC_DAT_DATAVALID_SHIFT                  (31U)


extern int32_t 	_Chip_ADC_Get_PeriIndex(void *pPeri);
extern void*	_Chip_ADC_Init(int32_t PeriIndex);
extern uint32_t _Chip_ADC_Get_ChannelMask(void *pPeri);
extern bool _Chip_ADC_Get_DataValidFlag_CH( void *pPeri, uint8_t Channel);
extern uint32_t _Chip_ADC_Get_Data_CH( void *pPeri, uint8_t Channel);
extern bool 	_Chip_ADC_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_ADC_Power(void *pPeri, bool NewState);
extern bool 	_Chip_ADC_Enable_CH(void* pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool 	_Chip_ADC_Configure_CH (void *pPeri, uint32_t ChannelBitMask, uint32_t Speed_kHz, bool RepeatMode, bool DMAMode);
extern uint32_t _Chip_ADC_Get_IRQ_Status_CH(_CHIP_ADC_T *pADC, uint32_t ChannelBitMask);
extern bool 	_Chip_ADC_Clear_IRQ_Status_CH(_CHIP_ADC_T *pADC, uint32_t ChannelBitMask);
extern void 	_Chip_ADC_Start_CH(void *pPeri, uint32_t ChannelBitMask);
extern bool 	_Chip_ADC_IRQ_Peri_Enable_CH(void *pPeri, uint32_t IRQBitMask, bool NewState);
extern bool 	_Chip_Temp_Enable(bool NewState);
extern bool 	_Chip_IREF_Power(bool NewState);
extern bool 	_Chip_TS_Power(bool NewState);




// ******************************************************************************************************
// DAC Functions
// ******************************************************************************************************
extern int32_t 	_Chip_DAC_Get_PeriIndex(void *pPeri);
extern void*	_Chip_DAC_Init(int32_t PeriIndex);
extern bool 	_Chip_DAC_Enable(void* pPeri, bool NewState);
extern bool 	_Chip_DAC_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern void 	_Chip_DAC_Put_Data(void *pPeri, uint32_t WrData);
extern uint32_t _Chip_DAC_Get_Peri_Status(void *pPeri);
extern void 	_Chip_DAC_Clear_Peri_Status(void *pPeri, uint32_t IRQBitMask);
extern uint32_t _Chip_DAC_Get_IRQ_Status(void *pPeri);
extern void 	_Chip_DAC_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask);
extern bool 	_Chip_DAC_Configure(void *pPeri);







// ------------------------------------------------------------------------------------------------
// TIMER Functions - MRTx
// LPC15xx has one MRT timer with 4 independent channels
// ------------------------------------------------------------------------------------------------
extern void*	_Chip_MRT_Init(int32_t PeriIndex);
extern bool 	_Chip_MRT_Enable_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool 	_Chip_MRT_Enable_IRQ_NVIC(void *pPeri, bool NewState);
extern bool 	_Chip_MRT_Configure (void *pPeri, uint8_t Channel, uint32_t RateHz, bool RepeatMode);
extern void 	_Chip_MRT_Set_TmrVal(void *pPeri, uint8_t Channel, uint32_t TmrValue);
extern void 	_Chip_MRT_Reset_Tmr_CH(void *pPeri, uint32_t ChannelBitMask);
extern uint32_t _Chip_MRT_Get_TmrVal(void *pPeri, uint8_t Channel);
extern int32_t 	_Chip_MRT_Get_PeriIndex(void *pPeri);
extern bool 	_Chip_MRT_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask);
extern uint32_t _Chip_MRT_Get_IRQ_Status_CH(void *pPeri);
extern bool 	_Chip_MRT_Enable_IRQ_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState);




// ------------------------------------------------------------------------------------------------
// DMA Functions
// ------------------------------------------------------------------------------------------------
// Support definitions for setting the configuration of a DMA channel. You
//   will need to get more information on these options from the User manual..
#define _CHIP_DMA_CFG_PERIPHREQEN     (1 << 0)	// Enables Peripheral DMA requests.
#define _CHIP_DMA_CFG_HWTRIGEN        (1 << 1)	// Use hardware triggering via imput mux.
#define _CHIP_DMA_CFG_TRIGPOL_LOW     (0 << 4)	// Hardware trigger is active low or falling edge.
#define _CHIP_DMA_CFG_TRIGPOL_HIGH    (1 << 4)	// Hardware trigger is active high or rising edge.
#define _CHIP_DMA_CFG_TRIGTYPE_EDGE   (0 << 5)	// Hardware trigger is edge triggered.
#define _CHIP_DMA_CFG_TRIGTYPE_LEVEL  (1 << 5)	// Hardware trigger is level triggered.
#define _CHIP_DMA_CFG_TRIGBURST_SNGL  (0 << 6)	// Single transfer. Hardware trigger causes a single transfer.
#define _CHIP_DMA_CFG_TRIGBURST_BURST (1 << 6)	// Burst transfer (see UM).
#define _CHIP_DMA_CFG_BURSTPOWER_1    (0 << 8)	// Set DMA burst size to 1 transfer.
#define _CHIP_DMA_CFG_BURSTPOWER_2    (1 << 8)	// Set DMA burst size to 2 transfers.
#define _CHIP_DMA_CFG_BURSTPOWER_4    (2 << 8)	// Set DMA burst size to 4 transfers.
#define _CHIP_DMA_CFG_BURSTPOWER_8    (3 << 8)	// Set DMA burst size to 8 transfers.
#define _CHIP_DMA_CFG_BURSTPOWER_16   (4 << 8)	// Set DMA burst size to 16 transfers.
#define _CHIP_DMA_CFG_BURSTPOWER_32   (5 << 8)	// Set DMA burst size to 32 transfers.
#define _CHIP_DMA_CFG_BURSTPOWER_64   (6 << 8)	// Set DMA burst size to 64 transfers.
#define _CHIP_DMA_CFG_BURSTPOWER_128  (7 << 8)	// Set DMA burst size to 128 transfers.
#define _CHIP_DMA_CFG_BURSTPOWER_256  (8 << 8)	// Set DMA burst size to 256 transfers.
#define _CHIP_DMA_CFG_BURSTPOWER_512  (9 << 8)	// Set DMA burst size to 512 transfers.
#define _CHIP_DMA_CFG_BURSTPOWER_1024 (10 << 8)	// Set DMA burst size to 1024 transfers.
#define _CHIP_DMA_CFG_BURSTPOWER(n)   ((n) << 8)	// Set DMA burst size to 2^n transfers, max n=10.
#define _CHIP_DMA_CFG_SRCBURSTWRAP    (1 << 14)	// Source burst wrapping is enabled for this DMA channel.
#define _CHIP_DMA_CFG_DSTBURSTWRAP    (1 << 15)	// Destination burst wrapping is enabled for this DMA channel.
#define _CHIP_DMA_CFG_CHPRIORITY(p)   ((p) << 16)	// Sets DMA channel priority, min 0 (highest), max 3 (lowest).

// DMA channel transfer configuration registers definitions.
#define _CHIP_DMA_XFERCFG_CFGVALID        (1 << 0)	// Configuration Valid flag.
#define _CHIP_DMA_XFERCFG_RELOAD          (1 << 1)	// Indicates whether the channels control structure will be reloaded when the current descriptor is exhausted.
#define _CHIP_DMA_XFERCFG_SWTRIG          (1 << 2)	// Software Trigger.
#define _CHIP_DMA_XFERCFG_CLRTRIG         (1 << 3)	// Clear Trigger.
#define _CHIP_DMA_XFERCFG_SETINTA         (1 << 4)	// Set Interrupt flag A for this channel to fire when descriptor is complete.
#define _CHIP_DMA_XFERCFG_SETINTB         (1 << 5)	// Set Interrupt flag B for this channel to fire when descriptor is complete.
#define _CHIP_DMA_XFERCFG_WIDTH_8         (0 << 8)	// 8-bit transfers are performed.
#define _CHIP_DMA_XFERCFG_WIDTH_16        (1 << 8)	// 16-bit transfers are performed.
#define _CHIP_DMA_XFERCFG_WIDTH_32        (2 << 8)	// 32-bit transfers are performed.
#define _CHIP_DMA_XFERCFG_SRCINC_0        (0 << 12)	// DMA source address is not incremented after a transfer.
#define _CHIP_DMA_XFERCFG_SRCINC_1        (1 << 12)	// DMA source address is incremented by 1 (width) after a transfer.
#define _CHIP_DMA_XFERCFG_SRCINC_2        (2 << 12)	// DMA source address is incremented by 2 (width) after a transfer.
#define _CHIP_DMA_XFERCFG_SRCINC_4        (3 << 12)	// DMA source address is incremented by 4 (width) after a transfer.
#define _CHIP_DMA_XFERCFG_DSTINC_0        (0 << 14)	// DMA destination address is not incremented after a transfer.
#define _CHIP_DMA_XFERCFG_DSTINC_1        (1 << 14)	// DMA destination address is incremented by 1 (width) after a transfer.
#define _CHIP_DMA_XFERCFG_DSTINC_2        (2 << 14)	// DMA destination address is incremented by 2 (width) after a transfer.
#define _CHIP_DMA_XFERCFG_DSTINC_4        (3 << 14)	// DMA destination address is incremented by 4 (width) after a transfer.
#define _CHIP_DMA_XFERCFG_XFERCOUNT(n)    ((n - 1) << 16)	// DMA transfer count in 'transfers', between (0)1 and (1023)1024.

typedef struct
{
	uint32_t							SrcAdr;										// Source address
	uint16_t							SrcBSize;									// Source Burst size		  1/2/4 x Word
	uint8_t								SrcWSize;									// Source width: 8/16/32-bit
	uint8_t								SrcIncr;									// Source Increment
	uint32_t							SrcPeri;									// Source peripherial	
	uint32_t							DstAdr;										// Destination address
	uint16_t							DstBSize;									// Destination Burst size		  1/2/4 x Word
	uint8_t								DstWSize;									// Destination width: 8/16/32-bit
	uint8_t								DstIncr;									// Destination Increment
	uint32_t							DstPeri;									// Destination peripherial	
	uint8_t								XferIRQ;									// Select Interrupt generated after transfer
	uint32_t 							XferLen;									// length of transfer
	uint8_t								XferType;									// transfer type
	uint8_t								XferPrio;									// transfer priority
	uint32_t 							XferNextDescriptor;							// address of next linked descriptor
} _Chip_DMA_Xfer_t;

// DMA channel source/address/next descriptor
typedef struct {
	uint32_t  xfercfg;																// Transfer configuration (only used in linked lists and ping-pong configs)
	uint32_t  source;																// DMA transfer source end address
	uint32_t  dest;																	// DMA transfer desintation end address
	uint32_t  next;																	// Link to next DMA descriptor, must be 16 byte aligned
} _Chip_DMA_CHDesc_t;

// DMA SRAM table - this can be optionally used with the Chip_DMA_SetSRAMBase()
//   function if a DMA SRAM table is needed.
extern _Chip_DMA_CHDesc_t _Chip_DMA_Table[_CHIP_DMA_CHANNELS_COUNT];

extern void*	_Chip_DMA_Init(int32_t PeriIndex);
extern bool 	_Chip_DMA_Enable_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState);
extern bool 	_Chip_DMA_Configure (void *pPeri, uint8_t Channel, _Chip_DMA_Xfer_t *Xfer );
extern int32_t 	_Chip_DMA_Get_PeriIndex(void *pPeri);
extern bool 	_Chip_DMA_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask);
extern uint32_t _Chip_DMA_Get_IRQ_Status(void *pPeri);
extern uint32_t _Chip_DMA_Get_CHControl(void *pPeri, uint32_t Channel);
extern bool 	_Chip_DMA_Enable_IRQ_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState);




// ******************************************************************************************************
// EEPROM Functions
// ******************************************************************************************************
extern void *_Chip_EEPROM_Init(int32_t PeriIndex);
extern bool _Chip_EEPROM_Enable(void *pPeri, bool NewState);
//extern bool _Chip_EEPROM_IRQ_Enable(void *pPeri, bool NewState);
extern bool _Chip_EEPROM_Write(void *pPeri, uint32_t EEDst, uint8_t *pInBuff, size_t byteswr);
extern bool _Chip_EEPROM_Read(void *pPeri, uint32_t EESrc, uint8_t *pOutBuff, size_t bytesrd);




// ------------------------------------------------------------------------------------------------
// SWM Functions
// ------------------------------------------------------------------------------------------------
// LPC15XX Switch Matrix Movable pins
typedef enum CHIP_SWM_PIN_MOVABLE  {
	SWM_UART0_TXD_O         = 0x00,		// PINASSIGN0 - UART0 TXD Output.
	SWM_UART0_RXD_I         = 0x01,		// PINASSIGN0 - UART0 RXD Input.
	SWM_UART0_RTS_O         = 0x02,		// PINASSIGN0 - UART0 RTS Output.
	SWM_UART0_CTS_I         = 0x03,		// PINASSIGN0 - UART0 CTS Input.
	SWM_UART0_SCLK_IO       = 0x10,		// PINASSIGN1 - UART0 SCLK I/O.
	SWM_UART1_TXD_O         = 0x11,		// PINASSIGN1 - UART1 TXD Output.
	SWM_UART1_RXD_I         = 0x12,		// PINASSIGN1 - UART1 RXD Input.
	SWM_UART1_RTS_O         = 0x13,		// PINASSIGN1 - UART1 RTS Output.
	SWM_UART1_CTS_I         = 0x20,		// PINASSIGN2 - UART1 CTS Input.
	SWM_UART1_SCLK_IO       = 0x21,		// PINASSIGN2 - UART1 SCLK I/O.
	SWM_UART2_TXD_O         = 0x22,		// PINASSIGN2 - UART2 TXD Output.
	SWM_UART2_RXD_I         = 0x23,		// PINASSIGN2 - UART2 RXD Input.
	SWM_UART2_SCLK_IO       = 0x30,		// PINASSIGN3 - UART2 SCLK I/O.
	SWM_SSP0_SCK_IO         = 0x31,		// PINASSIGN3 - SSP0 SCK I/O.
	SWM_SPI0_SCK_IO         = SWM_SSP0_SCK_IO,
	SWM_SSP0_MOSI_IO        = 0x32,		// PINASSIGN3 - SSP0 MOSI I/O.
	SWM_SPI0_MOSI_IO        = SWM_SSP0_MOSI_IO,
	SWM_SSP0_MISO_IO        = 0x33,		// PINASSIGN3 - SSP0 MISO I/O.
	SWM_SPI0_MISO_IO        = SWM_SSP0_MISO_IO,
	SWM_SSP0_SSELSN_0_IO    = 0x40,
	SWM_SPI0_SSELSN_0_IO    = SWM_SSP0_SSELSN_0_IO,
	SWM_SSP0_SSELSN_1_IO    = 0x41,
	SWM_SPI0_SSELSN_1_IO    = SWM_SSP0_SSELSN_1_IO,
	SWM_SSP0_SSELSN_2_IO    = 0x42,
	SWM_SPI0_SSELSN_2_IO    = SWM_SSP0_SSELSN_2_IO,
	SWM_SSP0_SSELSN_3_IO    = 0x43,
	SWM_SPI0_SSELSN_3_IO    = SWM_SSP0_SSELSN_3_IO,
	SWM_SSP1_SCK_IO         = 0x50,		// PINASSIGN5 - SPI1 SCK I/O.
	SWM_SPI1_SCK_IO         = SWM_SSP1_SCK_IO,
	SWM_SSP1_MOSI_IO        = 0x51,		// PINASSIGN5 - SPI1 MOSI I/O.
	SWM_SPI1_MOSI_IO        = SWM_SSP1_MOSI_IO,
	SWM_SSP1_MISO_IO        = 0x52,		// PINASSIGN5 - SPI1 MISO I/O.
	SWM_SPI1_MISO_IO        = SWM_SSP1_MISO_IO,
	SWM_SSP1_SSELSN_0_IO    = 0x53,		// PINASSIGN5 - SPI1 SSEL I/O.
	SWM_SPI1_SSELSN_0_IO    = SWM_SSP1_SSELSN_0_IO,
	SWM_SSP1_SSELSN_1_IO    = 0x60,
	SWM_SPI1_SSELSN_1_IO    = SWM_SSP1_SSELSN_1_IO,
	SWM_CAN_TD1_O           = 0x61,
	SWM_CAN_RD1_I           = 0x62,
	SWM_USB_VBUS_I          = 0x70,
	SWM_SCT0_OUT0_O         = 0x71,
	SWM_SCT0_OUT1_O         = 0x72,
	SWM_SCT0_OUT2_O         = 0x73,
	SWM_SCT1_OUT0_O         = 0x80,
	SWM_SCT1_OUT1_O         = 0x81,
	SWM_SCT1_OUT2_O         = 0x82,
	SWM_SCT2_OUT0_O         = 0x83,
	SWM_SCT2_OUT1_O         = 0x90,
	SWM_SCT2_OUT2_O         = 0x91,
	SWM_SCT3_OUT0_O         = 0x92,
	SWM_SCT3_OUT1_O         = 0x93,
	SWM_SCT3_OUT2_O         = 0xA0,
	SWM_SCT_ABORT0_I        = 0xA1,
	SWM_SCT_ABORT1_I        = 0xA2,
	SWM_ADC0_PIN_TRIG0_I    = 0xA3,
	SWM_ADC0_PIN_TRIG1_I    = 0xB0,
	SWM_ADC1_PIN_TRIG0_I    = 0xB1,
	SWM_ADC1_PIN_TRIG1_I    = 0xB2,
	SWM_DAC_PIN_TRIG_I      = 0xB3,
	SWM_DAC_SHUTOFF_I       = 0xC0,
	SWM_ACMP0_OUT_O         = 0xC1,
	SWM_ACMP1_OUT_O         = 0xC2,
	SWM_ACMP2_OUT_O         = 0xC3,
	SWM_ACMP3_OUT_O         = 0xD0,
	SWM_CLK_OUT_O           = 0xD1,
	SWM_ROSC0_O             = 0xD2,
	SWM_ROSC_RST0_I         = 0xD3,
	SWM_USB_FRAME_TOG_O     = 0xE0,
	SWM_QEI0_PHA_I          = 0xE1,
	SWM_QEI0_PHB_I          = 0xE2,
	SWM_QEI0_IDX_I          = 0xE3,
	SWM_GPIO_INT_BMATCH_O   = 0xF0,
	SWM_SWO_O               = 0xF1,
} CHIP_SWM_PIN_MOVABLE_T;

// LPC15XX Switch Matrix Fixed pins
typedef enum CHIP_SWM_PIN_FIXED    {
	SWM_FIXED_ADC0_0    = 0x00,	// ADC0_0 fixed pin enable/disable on pin P0_8.
	SWM_FIXED_ADC0_1    = 0x01,	// ADC0_1 fixed pin enable/disable on pin P0_7.
	SWM_FIXED_ADC0_2    = 0x02,	// ADC0_2 fixed pin enable/disable on pin P0_6.
	SWM_FIXED_ADC0_3    = 0x03,	// ADC0_3 fixed pin enable/disable on pin P0_5.
	SWM_FIXED_ADC0_4    = 0x04,	// ADC0_4 fixed pin enable/disable on pin P0_4.
	SWM_FIXED_ADC0_5    = 0x05,	// ADC0_5 fixed pin enable/disable on pin P0_3.
	SWM_FIXED_ADC0_6    = 0x06,	// ADC0_6 fixed pin enable/disable on pin P0_2.
	SWM_FIXED_ADC0_7    = 0x07,	// ADC0_7 fixed pin enable/disable on pin P0_1.
	SWM_FIXED_ADC0_8    = 0x08,	// ADC0_8 fixed pin enable/disable on pin P1_0.
	SWM_FIXED_ADC0_9    = 0x09,	// ADC0_9 fixed pin enable/disable on pin P0_31.
	SWM_FIXED_ADC0_10   = 0x0A,	// ADC0_10 fixed pin enable/disable on pin P0_0.
	SWM_FIXED_ADC0_11   = 0x0B,	// ADC0_11 fixed pin enable/disable on pin P0_30.
	SWM_FIXED_ADC1_0    = 0x0C,	// ADC1_0 fixed pin enable/disable/disable on pin P1_1.
	SWM_FIXED_ADC1_1    = 0x0D,	// ADC1_1 fixed pin enable/disable on pin P0_9.
	SWM_FIXED_ADC1_2    = 0x0E,	// ADC1_2 fixed pin enable/disable on pin P0_10.
	SWM_FIXED_ADC1_3    = 0x0F,	// ADC1_3 fixed pin enable/disable on pin P0_11.
	SWM_FIXED_ADC1_4    = 0x10,	// ADC1_4 fixed pin enable/disable on pin P1_2.
	SWM_FIXED_ADC1_5    = 0x11,	// ADC1_5 fixed pin enable/disable on pin P1_3.
	SWM_FIXED_ADC1_6    = 0x12,	// ADC1_6 fixed pin enable/disable on pin P0_13.
	SWM_FIXED_ADC1_7    = 0x13,	// ADC1_7 fixed pin enable/disable on pin P0_14.
	SWM_FIXED_ADC1_8    = 0x14,	// ADC1_8 fixed pin enable/disable on pin P0_15.
	SWM_FIXED_ADC1_9    = 0x15,	// ADC1_9 fixed pin enable/disable on pin P0_16.
	SWM_FIXED_ADC1_10   = 0x16,	// ADC1_10 fixed pin enable/disable on pin P1_4.
	SWM_FIXED_ADC1_11   = 0x17,	// ADC1_11 fixed pin enable/disable on pin P1_5.
	SWM_FIXED_DAC_OUT   = 0x18,	// DAC_OUT fixed pin enable/disable on pin P0_12.
	SWM_FIXED_ACMP_I1   = 0x19,	// ACMP input 1 (common input) fixed pin enable/disable on pin P0_27.
	SWM_FIXED_ACMP_I2   = 0x1A,	// ACMP input 1 (common input) fixed pin enable/disable on pin P1_6.
	SWM_FIXED_ACMP0_I3  = 0x1B,	// ACMP comparator 0 input 3 fixed pin enable/disable on pin P0_26.
	SWM_FIXED_ACMP0_I4  = 0x1C,	// ACMP comparator 0 input 4 fixed pin enable/disable on pin P0_25.
	SWM_FIXED_ACMP1_I3  = 0x1D,	// ACMP comparator 1 input 3 fixed pin enable/disable on pin P0_28.
	SWM_FIXED_ACMP1_I4  = 0x1E,	// ACMP comparator 1 input 4 fixed pin enable/disable on pin P1_10.
	SWM_FIXED_ACMP2_I3  = 0x1F,	// ACMP comparator 2 input 3 fixed pin enable/disable on pin P0_29.
	SWM_FIXED_ACMP2_I4  = 0x80,	// ACMP comparator 2 input 4 fixed pin enable/disable on pin P1_9.
	SWM_FIXED_ACMP3_I3  = 0x81,	// ACMP comparator 3 input 3 fixed pin enable/disable on pin P1_8.
	SWM_FIXED_ACMP3_I4  = 0x82,	// ACMP comparator 3 input 4 fixed pin enable/disable on pin P1_7.
	SWM_FIXED_I2C0_SDA  = 0x83,	// I2C0_SDA fixed pin enable/disable on pin P0_23.
	SWM_FIXED_I2C0_SCL  = 0x84,	// I2C0_SCL fixed pin enable/disable on pin P0_22.
	SWM_FIXED_SCT0_OUT3 = 0x85,	// SCT0_OUT3 fixed pin enable/disable on pin P0_0.
	SWM_FIXED_SCT0_OUT4 = 0x86,	// SCT0_OUT4 fixed pin enable/disable on pin P0_1.
	SWM_FIXED_SCT0_OUT5 = 0x87,	// SCT0_OUT5 fixed pin enable/disable on pin P0_18.
	SWM_FIXED_SCT0_OUT6 = 0x88,	// SCT0_OUT6 fixed pin enable/disable on pin P0_24.
	SWM_FIXED_SCT0_OUT7 = 0x89,	// SCT0_OUT7 fixed pin enable/disable on pin P1_14.
	SWM_FIXED_SCT1_OUT3 = 0x8A,	// SCT1_OUT3 fixed pin enable/disable on pin P0_2.
	SWM_FIXED_SCT1_OUT4 = 0x8B,	// SCT1_OUT4 fixed pin enable/disable on pin P0_3.
	SWM_FIXED_SCT1_OUT5 = 0x8C,	// SCT1_OUT5 fixed pin enable/disable on pin P0_14.
	SWM_FIXED_SCT1_OUT6 = 0x8D,	// SCT1_OUT6 fixed pin enable/disable on pin P0_20.
	SWM_FIXED_SCT1_OUT7 = 0x8E,	// SCT1_OUT7 fixed pin enable/disable on pin P1_17.
	SWM_FIXED_SCT2_OUT3 = 0x8F,	// SCT2_OUT3 fixed pin enable/disable on pin P0_6.
	SWM_FIXED_SCT2_OUT4 = 0x90,	// SCT2_OUT4 fixed pin enable/disable on pin P0_29.
	SWM_FIXED_SCT2_OUT5 = 0x91,	// SCT2_OUT5 fixed pin enable/disable on pin P1_20.
	SWM_FIXED_SCT3_OUT3 = 0x92,	// SCT3_OUT3 fixed pin enable/disable on pin P0_26.
	SWM_FIXED_SCT3_OUT4 = 0x93,	// SCT3_OUT4 fixed pin enable/disable on pin P1_8.
	SWM_FIXED_SCT3_OUT5 = 0x94,	// SCT3_OUT5 fixed pin enable/disable on pin P1_24.
	SWM_FIXED_RESETN    = 0x95,	// RESETN fixed pin enable/disable on pin P0_21.
	SWM_FIXED_SWCLK_TCK = 0x96,	// SWCLK_TCK fixed pin enable/disable on pin P0_19.
	SWM_FIXED_SWDIO     = 0x97,	// SWDIO fixed pin enable/disable on pin P0_20.
} CHIP_SWM_PIN_FIXED_T;

extern bool _Chip_SWM_Enable (void);
extern void _Chip_SWM_EnableFixedPin(CHIP_SWM_PIN_FIXED_T pin);
extern void _Chip_SWM_DisableFixedPin(CHIP_SWM_PIN_FIXED_T pin);
extern void _Chip_SWM_FixedPinEnable(CHIP_SWM_PIN_FIXED_T pin, bool enable);
extern bool _Chip_SWM_IsFixedPinEnabled(CHIP_SWM_PIN_FIXED_T pin);
extern void _Chip_SWM_MovablePinAssign(CHIP_SWM_PIN_MOVABLE_T movable, uint8_t pin);

// ------------------------------------------------------------------------------------------------
// SCT Functions
// ------------------------------------------------------------------------------------------------
extern void* _Chip_SCT_Init(int32_t PeriIndex);
extern bool _Chip_SCT_Enable(void *pPeri, bool NewState);
extern bool _Chip_SCT_Configure(void *pPeri);

// ------------------------------------------------------------------------------------------------
// Return Index from pointer
static inline int32_t _Chip_SCT_Get_PeriIndex(void *pPeri)
{
	if((LPC_SCT_T*) pPeri == LPC_SCT3) return(3);
	if((LPC_SCT_T*) pPeri == LPC_SCT2) return(2);
	if((LPC_SCT_T*) pPeri == LPC_SCT1) return(1);
	if((LPC_SCT_T*) pPeri == LPC_SCT0) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------
// WDT Functions
// ------------------------------------------------------------------------------------------------
void *_Chip_WDT_Init(int32_t PeriIndex);
extern bool _Chip_WDT_Enable(void *pPeri, bool NewState);
extern bool _Chip_WDT_IRQ_Enable(void *pPeri, bool NewState);	
extern bool _Chip_WDT_Configure(void *pPeri, uint32_t WinMinVal, uint32_t WinMaxVal, uint32_t WarningVal, bool HardRst);


// ------------------------------------------------------------------------------------------------------
// WDT Reset - call periodically for WDT counter reset
// result is true:successfull otherwise false
static inline void _Chip_WDT_Reset(void *pPeri)
{
	// feed:
	((_CHIP_WWDT_T*)pPeri)->FEED = 0xAA;
	((_CHIP_WWDT_T*)pPeri)->FEED = 0x55;
}

// ------------------------------------------------------------------------------------------------
// Read interrupt status
static inline uint32_t _Chip_WDT_GET_IRQStatus(void *pPeri)
{
	return(((_CHIP_WWDT_T*)pPeri)->MOD & (0x03 << 2));								// return WDINT flag or Warning INT flag
}

// ------------------------------------------------------------------------------------------------
// Clear interrupt status
static inline bool _Chip_WDT_Clear_IRQStatus(void *pPeri, uint32_t ClearMask)
{
	((_CHIP_WWDT_T*)pPeri)->MOD &= ~(ClearMask & (0x03 << 2));						// Clear WDINT flag or Warning INT flag - this bit was cleared by 0!!!
	return(true);																	
}












// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// ROM API extension:

#include    "romapi_15XX.h" 														// ROM API extension




#endif	// ! LOAD_SCATTER
#endif	//__SERIE_15XX_H_
