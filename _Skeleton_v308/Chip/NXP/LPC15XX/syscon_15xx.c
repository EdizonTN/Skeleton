// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: syscon_15xx.c
// 	   Version: 1.0
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//	  Desc: SYSCON
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//

// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************
/* PDWAKECFG register mask */
#define PDWAKEUPUSEMASK 0x00000000
#define PDWAKEUPMASKTMP 0x01FFFF78

/* PDRUNCFG register mask */
#define PDRUNCFGUSEMASK 0x00000000
#define PDRUNCFGMASKTMP 0x01FFFF78


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************

/* Returns the computed value for a frequency measurement cycle */
uint32_t Chip_SYSCON_GetCompFreqMeas(uint32_t refClockRate)
{
	uint32_t capval;
	uint64_t clkrate = 0;

	/* Get raw capture value */
	capval = Chip_SYSCON_GetRawFreqMeasCapval();

	/* Limit CAPVAL check */
	if (capval > 2) {
		clkrate = (((uint64_t) capval - 2) * (uint64_t) refClockRate) / 0x4000;
	}

	return (uint32_t) clkrate;
}

/* De-assert reset for a peripheral */
void Chip_SYSCON_AssertPeriphReset(CHIP_SYSCON_PERIPH_RESET_T periph)
{
	if (periph >= 32) {
		LPC_SYSCON->PRESETCTRL1 |= (1 << ((uint32_t) periph - 32));
	}
	else {
		LPC_SYSCON->PRESETCTRL0 |= (1 << (uint32_t) periph);
	}
}

/* Assert reset for a peripheral */
void Chip_SYSCON_DeassertPeriphReset(CHIP_SYSCON_PERIPH_RESET_T periph)
{
	if (periph >= 32) {
		LPC_SYSCON->PRESETCTRL1 &= ~(1 << ((uint32_t) periph - 32));
	}
	else {
		LPC_SYSCON->PRESETCTRL0 &= ~(1 << (uint32_t) periph);
	}
}

/* Setup wakeup behaviour from deep sleep */
void Chip_SYSCON_SetWakeup(uint32_t wakeupmask)
{
	/* Update new value */
	LPC_SYSCON->PDAWAKECFG = PDWAKEUPUSEMASK | (wakeupmask & PDWAKEUPMASKTMP);
}

/* Power down one or more blocks or peripherals */
void Chip_SYSCON_PowerDown(uint32_t powerdownmask)
{
	uint32_t pdrun;

	pdrun = LPC_SYSCON->PDRUNCFG & PDRUNCFGMASKTMP;
	pdrun |= (powerdownmask & PDRUNCFGMASKTMP);

	LPC_SYSCON->PDRUNCFG = (pdrun | PDRUNCFGUSEMASK);
}

/* Power up one or more blocks or peripherals */
void Chip_SYSCON_PowerUp(uint32_t powerupmask)
{
	uint32_t pdrun;

	pdrun = LPC_SYSCON->PDRUNCFG & PDRUNCFGMASKTMP;
	pdrun &= ~(powerupmask & PDRUNCFGMASKTMP);

	LPC_SYSCON->PDRUNCFG = (pdrun | PDRUNCFGUSEMASK);
}

