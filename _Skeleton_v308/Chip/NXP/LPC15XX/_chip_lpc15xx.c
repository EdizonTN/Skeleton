// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _chip_lpc15xx.c
// 	   Version: 3.05
//		  Desc: MCU LPC15xx Chip description/configuration file. Same for all MCUs of this family!
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//				2024.09.11	- v3.05 - add _Chip_Clock_Get_SourceRTC_Rate
//									- fixed upgrade to Skeleton v3
//									- rename _CHIP_xxx_Handler to _Chip_xxx_Handler
//									- each XXX_Init have to disabled NVIC IRQ !!!!!!
//				2023.11.20	- v3.04 - rename handler to sys_xxx_handler
//				2020.07.08	- rename Pld: _Chip_UART_IRQRx_Enable ---> _Chip_UART_IRQ_Peri_Enable
//							- rename Pld: _Chip_UART_IRQTx_Enable ---> _Chip_UART_IRQ_Peri_Enable, add parameter
//				2020.05.23 	- v3.03 - added WatchDog
//				2020.05.22 	- I2C_Enable was reversed

#include "Skeleton.h"


#if defined (CONF_CHIP_ID_LPC1549JBD64) 	|| \
	defined (CONF_CHIP_ID_LPC1549JBD100) 	|| \
	defined (CONF_CHIP_ID_LPC1549JBD48)


#if defined(COMP_TYPE_XPR)		// LPCXPRESSO/MCUXpresso
	#include	"XPR\crp.c"
#endif

ATTR_WEAK extern void 	__valid_user_code_checksum(void);

#if defined (__REDLIB__)
extern void __main(void);
#endif
extern int main(void);


void _Chip_Default_IRQ_Handler(void) 
{
	dbgprint("\r\n.Unhandlered exception! .\r\n");
	// NVIC->IABR[0-8];		// active interrupt flag
}


void sys_Reset_Handler					(void) __WEAK_("_Chip_Default_IRQ_Handler");
void sys_HardFault_Handler				(void) __WEAK_("_Chip_Default_IRQ_Handler");

void _Chip_WDT0_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_BOD0_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_FLASH0_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_EEPROM0_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_DMA0_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_GINT0_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_GINT1_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT0_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT1_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT2_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT3_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT4_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT5_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT6_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_PINT7_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_RIT0_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_SCT0_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_SCT1_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_SCT2_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_SCT3_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_MRT0_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_UART0_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_UART1_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_UART2_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_I2C0_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_SPI0_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_SPI1_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_CAN0_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_USB0_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_USB0_FIQ_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_USB0_WAKE_IRQ_Handler	(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ADC0_SEQA_IRQ_Handler	(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ADC0_SEQB_IRQ_Handler	(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ADC0_THCMP_IRQ_Handler	(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ADC0_OVR_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ADC1_SEQA_IRQ_Handler	(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ADC1_SEQB_IRQ_Handler	(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ADC1_THCMP_IRQ_Handler	(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_ADC1_OVR_IRQ_Handler		(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_DAC0_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_CMP0_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_CMP1_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_CMP2_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_CMP3_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_QEI0_IRQ_Handler			(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_RTC0_ALARM_IRQ_Handler	(void) __WEAK_("_Chip_Default_IRQ_Handler");
void _Chip_RTC0_WAKE_IRQ_Handler	(void) __WEAK_("_Chip_Default_IRQ_Handler");

extern const uint32_t *StatckInit;

// define Cortex-M base interrupt vectors 
const cortexm_base_t cortex_vector_base __SECTION_IRQVECTORS  = 
{
	CONF_STACK_START+CONF_STACK_SIZE,
    {
        [ 0] = sys_Reset_Handler,														// entry point of the program 
        [ 1] = sys_NMI_Handler,															// [-14] non maskable interrupt handler
        [ 2] = sys_HardFault_Handler,													// [-13] hard fault exception
        [ 3] = sys_MemManage_Handler,													// [-12] memory manage exception
        [ 4] = sys_BusFault_Handler,													// [-11] bus fault exception
        [ 5] = sys_UsageFault_Handler,													// [-10] usage fault exception
        [ 6] = (isr_t)0, 															// LPC MCU Checksum - checksum.exe will write it here !!!!
		[ 7] = (isr_t)0,															// reserved
		[ 8] = (isr_t)0,															// reserved
		[ 9] = (isr_t)0,															// reserved
        [10] = sys_SVC_Handler,															// [-5] SW interrupt, use it, for triggering context switches
        [11] = sys_DebugMon_Handler,													// [-4] debug monitor exception
		[12] = (isr_t)0,															// reserved
        [13] = sys_PendSV_Handler,														// [-2] pendSV interrupt, use it, to do the actual context switch
        [14] = sys_SysTick_Handler,														// [-1] SysTick interrupt - Used for system counters and alarms
    },	
	{
		[ 0] = IRQHANDLER_16,														// WDT
		[ 1] = IRQHANDLER_17,														// BOD
		[ 2] = IRQHANDLER_18,														// FLASH
		[ 3] = IRQHANDLER_19,														// EEPROM
		[ 4] = IRQHANDLER_20,														// DMA
		[ 5] = IRQHANDLER_21,														// GINT 0
		[ 6] = IRQHANDLER_22,														// GINT 1
		[ 7] = IRQHANDLER_23,														// PIN_INT 0
		[ 8] = IRQHANDLER_24,														// PIN_INT 1
		[ 9] = IRQHANDLER_25,														// PIN_INT 2
		[10] = IRQHANDLER_26,														// PIN_INT 3
		[11] = IRQHANDLER_27,														// PIN_INT 4
		[12] = IRQHANDLER_28,														// PIN_INT 5
		[13] = IRQHANDLER_29,														// PIN_INT 6
		[14] = IRQHANDLER_30,														// PIN_INT 7
		[15] = IRQHANDLER_31,														// RIT
		[16] = IRQHANDLER_32,														// SCT 0
		[17] = IRQHANDLER_33,														// SCT 1
		[18] = IRQHANDLER_34,														// SCT 2
		[19] = IRQHANDLER_35,														// SCT 3
		[20] = IRQHANDLER_36,														// MRT
		[21] = IRQHANDLER_37,														// UART 0
		[22] = IRQHANDLER_38,														// UART 1
		[23] = IRQHANDLER_39,														// UART 2
		[24] = IRQHANDLER_40,														// I2C0
		[25] = IRQHANDLER_41,														// SPI 0
		[26] = IRQHANDLER_42,														// SPI 1
		[27] = IRQHANDLER_43,														// C_CAN 0
		[28] = IRQHANDLER_44,														// USB
		[29] = IRQHANDLER_45,														// USB_FIQ
		[30] = IRQHANDLER_46,														// USB_WAKEUP
		[31] = IRQHANDLER_47,														// ADC 0 _SEQ A
		[32] = IRQHANDLER_48,														// ADC 0 _SEQ B
		[33] = IRQHANDLER_49,														// ADC 0 _THCMP
		[34] = IRQHANDLER_50,														// ADC 0 _OVR
		[35] = IRQHANDLER_51,														// ADC 1 _SEQ A
		[36] = IRQHANDLER_52,														// ADC 1 _SEQ B
		[37] = IRQHANDLER_53,														// ADC 1 _THCMP
		[38] = IRQHANDLER_54,														// ADC 1 _OVR
		[39] = IRQHANDLER_55,														// DAC
		[40] = IRQHANDLER_56,														// CMP 0
		[41] = IRQHANDLER_57,														// CMP 1
		[42] = IRQHANDLER_58,														// CMP 2
		[43] = IRQHANDLER_59,														// CMP 3
		[44] = IRQHANDLER_60,														// QEI
		[45] = IRQHANDLER_61,														// RTC_ALARM
		[46] = IRQHANDLER_62														// RTC_WAKE
	}
};

// ------------------------------------------------------------------------------------------------
// CHIP functions
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// Prepare sector for write operation - This command must be executed before executing "Copy RAM to flash" or "Erase Sector(s)" command. 
//					Successful execution of the "Copy RAM to flash" or "Erase Sector(s)" command causes relevant sectors to be protected again. 
//					The boot sector can not be prepared by this command. To prepare a single sector use the same "Start" and "End" sector numbers.
//return 	result[0] : Status Code
bool _Chip_IAP_PrepareSector(uint32_t strSector, uint32_t endSector)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_PREWRRITE_CMD;
	Command[1] = strSector;
	Command[2] = endSector;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__enable_irq();
	
	if(Result[0] == 0) return(true);
	return (false);	
}


// ------------------------------------------------------------------------------------------------
// Copy RAM to flash - This command is used to program the flash memory. 
//					The affected sectors should be prepared first by calling "Prepare Sector for Write Operation" command. 
//					The affected sectors are automatically protected again once the copy command is successfully executed. 
//					The boot sector can not be written by this command. Also see Section 34.4.3 for the number of bytes that can be written.
//					Param3 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
//					Remark: All user code must be written in such a way that no master accesses the flash while this command is executed and the flash is programmed.
//return 	result[0] : Status Code
bool _Chip_IAP_CopyRamToFlash(uint32_t DstAdd, uint32_t *pSrc, uint32_t byteswrt)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_WRISECTOR_CMD;
	Command[1] = DstAdd;
	Command[2] = (uint32_t) pSrc;
	Command[3] = byteswrt;															// Should be 256 | 512 | 1024 | 4096.
	Command[4] = SystemCoreClock / 1000;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__enable_irq();
	
	if(Result[0] == 0) return(true);
	return (false);	
}

// ------------------------------------------------------------------------------------------------
// Erase Page - This command is used to erase a page or multiple pages of on-chip flash memory. 
//				To erase a single page use the same "Start" and "End" page numbers.
//				Param2 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
//				Remark: All user code must be written in such a way that no master accesses the flash while this command is executed and the flash is erased.
//return 	result[0] : Status Code
bool _Chip_IAP_ErasePage(uint32_t strPage, uint32_t endPage)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_ERASE_PAGE_CMD;
	Command[1] = strPage;
	Command[2] = endPage;
	Command[3] = SystemCoreClock / 1000;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__enable_irq();
	
	if(Result[0] == 0) return(true);
	return (false);
}

// ------------------------------------------------------------------------------------------------
// Blank check sector - This command is used to blank check a sector or multiple sectors of on-chip flash memory. 
//					To blank check a single sector use the same "Start" and "End" sector numbers.
// Result is True if sector/s is blank. Otherwise false
bool _Chip_IAP_BlankCheckSector(uint32_t strSector, uint32_t endSector)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_BLANK_CHECK_SECTOR_CMD;
	Command[1] = strSector;
	Command[2] = endSector;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__enable_irq();
	
	if(Result[0] == 0) return(true);
	return (false);
}

// ------------------------------------------------------------------------------------------------
// Read_MCUChipID
// Read MCU ID 
// Readed ID is saved into memory at address dst
// Result is true for Success otherwise false
bool _Chip_IAP_Read_ID(uint32_t *pDst)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_REPID_CMD;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__enable_irq();

	if(Result[0] == 0)
	{
		*pDst = Result[1];															// copy readed value to Dst
		return(true);
	}
	else
	{
		*pDst = NULL;
		return (false);
	}
}

// ------------------------------------------------------------------------------------------------
// Read_MCUSerialNum
// Read MCU serial Number
// Readed SerialNumber is saved into memory at address dst
// Result is true for Success otherwise false
bool _Chip_IAP_Read_SerialNum(uint32_t *pDst)
{
		uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_READ_UID_CMD;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__enable_irq();
	
	if(Result[0] == 0)																// Function call SUCCESS?
	{
		*pDst++ = Result[1];														// save readed value
		*pDst++ = Result[2];														// save readed value
		*pDst++ = Result[3];														// save readed value
		*pDst = Result[4];															// save readed value
		return(true);
	}
	else
	{
		*pDst = 0xFFFFFFFF;
		return (false);
	}
}
	
// ------------------------------------------------------------------------------------------------
// Read BootLoader version in MCU
// Result is true for Success otherwise false
bool _Chip_IAP_Read_BootCodeVersion(uint32_t *pDst)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_READ_BOOT_CODE_CMD;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__enable_irq();
	
	if(Result[0] == 0)																// Function call SUCCESS?
	{
		*pDst = Result[1];															// save readed value
		return(true);
	}
	else
	{
		*pDst = NULL;
		return (false);
	}
}

// ------------------------------------------------------------------------------------------------
// Write data to EEPROM - Data is copied from the RAM address to the EEPROM address.
//			Param3 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
//			Remark: The top 64 bytes of the EEPROM memory are reserved and cannot be written to.
// return 	result[0] : Status Code
bool _Chip_IAP_Write_EEPROM(uint32_t EEDst, uint32_t RAMSrc, uint32_t byteswrt)
{
	uint32_t Command[5], Result[5];
	
	Command[0] = _CHIP_IAP_EEPROM_WRITE_CMD;
	Command[1] = EEDst;
	Command[2] = RAMSrc;
	Command[3] = byteswrt;															// Write one page only
	Command[4] = SystemCoreClock / 1000;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__enable_irq();

	//return result[0];
	if(Result[0] == 0) return(true);
	return (false);
}

// ------------------------------------------------------------------------------------------------
// Read data from EEPROM - Data is copied from the EEPROM address to the RAM address.
//			Param3 is overwritten by the fixed value of 12 MHz, which is the IRC reference clock used by the flash controller.
// return 	result[0] : Status Code
bool _Chip_IAP_Read_EEPROM(uint32_t EESrc, uint32_t RAMDst, uint32_t bytesrd)
{
	uint32_t Command[5], Result[5];

	Command[0] = _CHIP_IAP_EEPROM_READ_CMD;
	Command[1] = EESrc;
	Command[2] = RAMDst;
	Command[3] = bytesrd;
	Command[4] = SystemCoreClock / 1000;
	__disable_irq();
	_Chip_IAP_Entry(Command, Result);
	__enable_irq();
	//return result[0];
	if(Result[0] == 0) return(true);
	return (false);
}












// ****************************************************************************************************** 
// CLOCK Functions
// ****************************************************************************************************** 

 uint32_t  SystemCoreClock;															// for IAP - System Clock Frequency (Core Clock)

// ------------------------------------------------------------------------------------------------
// "sysclk"
static inline uint32_t _Chip_Clock_Get_SysClk_Rate(void)							// read system clock
{
	return(_Chip_Clock_Get_MainClk_Rate() / LPC_SYSCON->SYSAHBCLKDIV);										
}

// ------------------------------------------------------------------------------------------------
// CPU core clock source rate, also called as "cclk" or "cpuclk"					// CPU clock rate
static inline uint32_t _Chip_Clock_Get_CoreClk_Rate(void)
{
	return(_Chip_Clock_Get_SysClk_Rate());											// same as "sysclk"
}


// ------------------------------------------------------------------------------------------------
// return clock freq of CLKOUTSELA clock
static inline uint32_t _Chip_Clock_Get_ClkOutSelA_Rate(void)
{
	uint32_t Result;

	switch(LPC_SYSCON->CLKOUTSELA & 0x02)
	{
		case 0x00:	Result = _CHIP_IRC_FREQUENCY;									// Internal RC oscillator  
					break;
		case 0x01: 	Result = CONF_CHIP_OSCRATEIN;									// source is system oscillator
					break;
		case 0x02:	Result = _CHIP_WDT_FREQUENCY;									// source is WatchDog 
					break;
		case 0x03:	Result = _Chip_Clock_Get_MainClk_Rate();						// mainclk
					break;
		default: 	Result = 0;														// unknown state
	}


	return (Result);
	
}

// ------------------------------------------------------------------------------------------------
// "USB PLL Out"
// return USB frate from USB PLL
static inline uint32_t _Chip_Clock_Get_USBPLLOut_Rate(void)							// Get USB pll output rate
{
	uint32_t USB_PLLIn;
	
	switch(LPC_SYSCON->USBPLLCLKSEL & 0x02)
	{
		case 0x00: USB_PLLIn = _CHIP_IRC_FREQUENCY;									// Internal RC oscillator  
					break;
		case 0x01: USB_PLLIn = CONF_CHIP_OSCRATEIN;									// System Oscillator (SYSOSC)
					break;
		default: USB_PLLIn = 0;														// unknown state
	}
	
	return( USB_PLLIn * (((LPC_SYSCON->USBPLLCTRL & 0x3f)+1) / (LPC_SYSCON->USBPLLCTRL & 0xc0)) );
}


// ------------------------------------------------------------------------------------------------
// "SCT PLL Out"
// return SCT rate from SCT PLL
static inline int32_t _Chip_Clock_Get_SCTPLLOut_Rate(void)							// Get SCT pll output rate
{
	uint32_t SCT_PLLIn;
	
	switch(LPC_SYSCON->USBPLLCLKSEL & 0x02)
	{
		case 0x00: SCT_PLLIn = _CHIP_IRC_FREQUENCY;									// Internal RC oscillator  
					break;
		case 0x01: SCT_PLLIn = CONF_CHIP_OSCRATEIN;									// System Oscillator (SYSOSC)
					break;
		default: SCT_PLLIn = 0;														// unknown state
	}
	
	return( SCT_PLLIn * (((LPC_SYSCON->SCTPLLCTRL & 0x3f)+1) / (LPC_SYSCON->SCTPLLCTRL & 0xc0)) );
}


// ------------------------------------------------------------------------------------------------
// return clock freq of CLKOUT pin - decode CLKOUTSELB reg
static inline uint32_t _Chip_Clock_Get_ClkOut_Rate(void)
{
	uint32_t Result;

	switch(LPC_SYSCON->CLKOUTSELB & 0x02)
	{
		case 0x00:	Result = _Chip_Clock_Get_ClkOutSelA_Rate();						// clk selected by CLKOUTSELA
					break;
		case 0x01: 	Result = _Chip_Clock_Get_USBPLLOut_Rate();						// USB PLL output
					break;
		case 0x02:	Result = _Chip_Clock_Get_SCTPLLOut_Rate();						// SCT PLL output
					break;
		case 0x03:	Result = CONF_CHIP_RTCRATEIN;									// RTC oscillator
					break;
		default: 	Result = 0;														// unknown state
	}
	
	return (Result);
}


// ------------------------------------------------------------------------------------------------
// Find encoded PDEC value for raw P value, max P = PVALMAX
static inline uint32_t _Chip_pllEncodeP(uint32_t P)
{
     switch (P)																		// Find PDec
    {
        case 1U:
            return(0x00);
        case 2U:
            return(0x01);
        case 4U:
            return(0x02);
        case 8U:
            return(0x03);
        default:
            break;
    }
	return(0x04);																	// wrong setting detected
}

// ------------------------------------------------------------------------------------------------
// Find decoded P value for raw PDEC value
static inline uint32_t _Chip_pllDecodeP(uint32_t PDEC)
{
    switch (PDEC)																	// Find PDec
    {
        case 0x00:
            return(1U);
        case 0x01:
            return(2U);
        case 0x02:
            return(4U);
        case 0x03:
            return(8U);
        default:
            break;
    }

    return (0L);																	// wrong setting detected
}


// ------------------------------------------------------------------------------------------------
// Set PLL frequency.
// Input clock have to active! 
// All PLL are identical (System, USB and SCT)
//
// UM10736: page 74
// Check that the selected settings meet all of the PLL requirements:
//	� Fin is in the range of 32 kHz to 25 MHz.
//	� Fcco is in the range of 156 MHz to 320 MHz.
//	� Fclkout is to 100 MHz.
//	� The post-divider is either bypassed, or P is in the range of 2 to 32.
//	� M is in the range of 3 to 32,768.

void _Chip_Clock_Set_PLLFreq(uint32_t pllM, uint32_t pllP)
{
	if(pllM == 0 ) pllM = 1;
	
	// Set Output 2xP divider and feedback's M multiplier
    LPC_SYSCON->SYSPLLCTRL = (_Chip_pllEncodeP(pllP) << 6) | (pllM-1) ;
	
	// Wait for Lock - skoncime tu!!!! WHY ????
    while ((LPC_SYSCON->SYSPLLSTAT & 0x01) == 0UL)									// Wait for PLL Lock
    {
    }

}

// ------------------------------------------------------------------------------------------------
// Set USB PLL frequency.
// Input clock have to active! 
// All PLL are identical (System, USB and SCT)
//
// UM10736: page 74
// Check that the selected settings meet all of the PLL requirements:
//	� Fin is in the range of 32 kHz to 25 MHz.
//	� Fcco is in the range of 156 MHz to 320 MHz.
//	� Fclkout is to 100 MHz.
//	� The post-divider is either bypassed, or P is in the range of 2 to 32.
//	� M is in the range of 3 to 32,768.

void _Chip_Clock_Set_USB_PLLFreq(uint32_t pllM, uint32_t pllP)
{
	if(pllM == 0 ) pllM = 1;
	
	// Set Output 2xP divider and feedback's M multiplier
    LPC_SYSCON->USBPLLCTRL = (_Chip_pllEncodeP(pllP) << 6) | (pllM-1) ;
	
	// Wait for Lock - skoncime tu!!!! WHY ????
    while ((LPC_SYSCON->USBPLLSTAT & 0x01) == 0UL)									// Wait for USB PLL Lock
    {
    }

}
// ------------------------------------------------------------------------------------------------
// return clock frequency for core sysclk
uint32_t _Chip_Clock_Get_SourceClk_Rate(void)										// Get input source clock rate.
{
	uint32_t Result;

	switch (LPC_SYSCON->MAINCLKSELB & 0x02)
	{
		case 0x00: 
		{
			switch( LPC_SYSCON->MAINCLKSELA & 0x02)									// check MAINCLKSELA
			{
				case 0x00: Result = _CHIP_IRC_FREQUENCY;
							break;
				case 0x01: Result = CONF_CHIP_OSCRATEIN;							// source is system oscillator
							break;
				case 0x02: Result = _CHIP_WDT_FREQUENCY;							// source is WatchDog 
							break;
				default: Result = 0;
			}
			
		}
		case 0x01:																	// Option 2 and 3 uses same source
		case 0x02:
		{
			switch( LPC_SYSCON->SYSPLLCLKSEL & 0x02)								// check SYSPLLCLKSEL
			{
				case 0x00: Result = _CHIP_IRC_FREQUENCY;
							break;
				case 0x01: Result = CONF_CHIP_OSCRATEIN;
							break;
				default: Result = 0;
			}
		}
		case 0x03: Result = CONF_CHIP_RTCRATEIN;
					break;
		default: Result = 0;														// unknown state
	}

	return (Result);
}

// ------------------------------------------------------------------------------------------------
// "mainclkselA"
// return clock source for core sysclk
uint32_t _Chip_Clock_Get_ClkSelA_Rate(void)											// Get Clock Selected A
{
	uint32_t Result =0;
	
	switch (LPC_SYSCON->MAINCLKSELA & 0x02)
	{
		case 0x00:	Result = _CHIP_IRC_FREQUENCY;									// Internal RC oscillator 
					break;
		case 0x01:	Result = CONF_CHIP_OSCRATEIN;									// System Oscillator (SYSOSC)
					break;
		case 0x02:	Result = _CHIP_WDT_FREQUENCY;									// Watchdog oscillator
					break;
		default: 	Result = 0;														// unknown state
	}
	
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// "syspllin"
// return clock source for core sysclk
uint32_t _Chip_Clock_Get_SysPLLIn_Rate(void)										// Get system pll input rate
{
	switch(LPC_SYSCON->SYSPLLCLKSEL)
	{
		case 0x00: return(_CHIP_IRC_FREQUENCY);										// IRC osc as source
		case 0x01: return(CONF_CHIP_OSCRATEIN);										// system oscillator input (SYSOSC)
		default: return(0);															// unknown state
	}
}

// ------------------------------------------------------------------------------------------------
// "syspllin"
// return clock source for core sysclk
uint32_t _Chip_Clock_Get_SysPLLOut_Rate(void)										// Get system pll output rate
{
	return((_Chip_Clock_Get_SysPLLIn_Rate() * ((LPC_SYSCON->SYSPLLCTRL & 0x3f)+1)) / (1 << ((LPC_SYSCON->SYSPLLCTRL >> 6) & 0x03)));
}

// ------------------------------------------------------------------------------------------------
// "mainclk"
// return clock source for core sysclk
uint32_t _Chip_Clock_Get_MainClk_Rate(void)											// Get MainClock
{
	uint32_t Result;

	switch(LPC_SYSCON->MAINCLKSELB & 0x03)
	{
		case 0x00:	Result = _Chip_Clock_Get_ClkSelA_Rate();						// clk selected by MAINCLKSELA
					break;
		case 0x01: 	Result = _Chip_Clock_Get_SysPLLIn_Rate();						// System PLL input
					break;
		case 0x02:	Result = _Chip_Clock_Get_SysPLLOut_Rate();						// System PLL output
					break;
		case 0x03:	Result = CONF_CHIP_RTCRATEIN;									// RTC oscillator
					break;
		default: 	Result = 0;														// unknown state
	}
	
	return (Result);
}	



// ------------------------------------------------------------------------------------------------
// Get I2C Clk
// Return Input Frequency for I2C
uint32_t _Chip_Clock_Get_I2CClk_Rate(uint32_t I2C_Index)
{
    uint32_t Result = 0;

	Result = _Chip_Clock_Get_SysClk_Rate();											// Periphery is clocked from SysClock
	Result = Result / LPC_I2C0->CLKDIV;												// LPC has one I2C Divider
		
    return (Result);
}

// ------------------------------------------------------------------------------------------------
// Return USB Clock source
static uint32_t _Chip_Clock_Get_USBClk_Rate(void)
{
	uint32_t Result;
		
	switch(LPC_SYSCON->USBCLKSEL &  0x02)
	{
		case 0x00: Result = _CHIP_IRC_FREQUENCY;									// Internal RC oscillator 
					break;
		case 0x01: Result = CONF_CHIP_OSCRATEIN;									// System Oscillator
					break;
		case 0x02: Result = _Chip_Clock_Get_USBPLLOut_Rate();						// USB PLL Out
					break;
		case 0x03: Result = _Chip_Clock_Get_MainClk_Rate();							// Main Clock B out
					break;
		default: Result = 0;
	}
	
	return (Result);	
}

// ------------------------------------------------------------------------------------------------
// Return ADC Async Clock Rate
static uint32_t _Chip_Clock_Get_ADCClk_Rate(void)
{
	uint32_t Result;

	switch(LPC_SYSCON->ADCASYNCCLKSEL & 0x02)
	{
		case 0x00:																	// Internal RC oscillator 
			Result = _CHIP_IRC_FREQUENCY;
			break;
		case 0x01:																	// SYS PLL Out
			Result = _Chip_Clock_Get_SysPLLOut_Rate();
			break;
		case 0x02:																	// USB PLL Out
			Result = _Chip_Clock_Get_USBPLLOut_Rate();
			break;
		case 0x03:																	// SCT PLL Out
			Result = _Chip_Clock_Get_SCTPLLOut_Rate();									
			break;
		default:
			Result = 0;
			break;
	}
	
	if(LPC_SYSCON->ADCASYNCCLKDIV & 0xff)Result = Result / (LPC_SYSCON->ADCASYNCCLKDIV & 0xff);
	else Result = 0;
	
	return (Result);	
	
}

// ------------------------------------------------------------------------------------------------
// Return UART Clock rate
// Return Input Frequency for UART[n]
// Index is not used in LPC15xx
uint32_t _Chip_Clock_Get_UARTClk_Rate(uint32_t UART_Index)
{
	uint32_t Result = 0;
	uint32_t mult, divmult;
	
	
	if(LPC_SYSCON->UARTCLKDIV & 0xff) Result = _Chip_Clock_Get_MainClk_Rate() / (LPC_SYSCON->UARTCLKDIV & 0xff);
	
	// Accept Fractal divider and multiplier:
	divmult = LPC_SYSCON->FRGCTRL & 0xFFFF;
	if ((divmult & 0xFF) == 0xFF) 
	{
		mult = (divmult >> 8) & 0xFF;												// Fractional part is enabled, get multiplier
		Result = (Result * 256) / (uint64_t) (256 + mult);							// Get fractional error
	}
	
	return (Result);	
	
}

// ------------------------------------------------------------------------------------------------
// Return RTC Clock rate
static uint32_t _Chip_Clock_Get_SourceRTC_Rate(void)
{
	return(CONF_CHIP_RTCRATEIN);
}








// ------------------------------------------------------------------------------------------------
// GPIO Functions 
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// HW Initialization
// Initialize port and pin
void *_Chip_GPIO_Init ( int32_t PeriIndex, uint32_t Pin)
{
	void *res = NULL;
	
	switch(PeriIndex)
	{
		case 2: 
			{
				//Chip_Clock_EnablePeriphClock(SYSCON_CLOCK_GPIO2);
				LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 16);							// Enable system clock for GPIO2
				res = LPC_GPIO_PORT;
				break;
			}
		case 1: 
			{
				//Chip_Clock_EnablePeriphClock(SYSCON_CLOCK_GPIO1);
				LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 15);							// Enable system clock for GPIO1
				res = LPC_GPIO_PORT;
				break;
			}
		case 0: 
			{
				//Chip_Clock_EnablePeriphClock(SYSCON_CLOCK_GPIO0);
				LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 14);							// Enable system clock for GPIO0
				res = LPC_GPIO_PORT;
				break;
			}
		default: return(NULL);
		
	}

//	// check for this pin is remaped to another peripherial 
//#if 0																				// check off - can be disconnected JTAG or SWD !!												
//	//Chip_Clock_EnablePeriphClock(SYSCON_CLOCK_SWM);
//	LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 12);										// Enable system clock for SWM

//	for(int32_t i = 0; i <= 15; i++)
//	{
//		for(uint8_t j = 0; j<=(3 - ((i/15)*2)); j++)								// 15 register check for byte 0 and byte 1
//		{
//			PinAssignReg = *(&LPC_SWM->PINASSIGN0+(i));								// read pinassign value for this pin
//			if (((PinAssignReg & (0xff << (j*8))) >> (j*8)) == pinpos) 				// bit 7:0
//			{
//#if defined(DEBUG_HAL) && (DEBUG_HAL == 1)
//				__Debug("\r\n\t GPIO p%d.%d Occupied! PINASSIGN[%d]->Byte:%d.\r\n", PeriIndex, Pin, i, j);
//#endif				
////				Chip_Clock_EnablePeriphClock(SYSCON_CLOCK_SWM);						// run SWM
//				*(&LPC_SWM->PINASSIGN0+(i*4)) = 0xff;								// release pin
////				Chip_Clock_DisablePeriphClock(SYSCON_CLOCK_SWM);					// stop SWM
//				sys_Err("GPIO Init - Collision. GPIO p%d.%d Occupied by: PINASSIGN[%d]->Byte:%d.", PeriIndex, Pin, i, j);
//				res = false;
//			}
//		}
//	}
//	//Chip_Clock_DisablePeriphClock(SYSCON_CLOCK_SWM);
//	LPC_SYSCON->SYSAHBCLKCTRL0 &= ~(1 << 12);										// Disable system clock for SWM
//#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------
// Hardware periphery Deinitialization
bool _Chip_GPIO_DeInit ( int32_t PeriIndex, uint32_t Pin)
{
    if(PeriIndex < 0) return(false);
	LPC_SYSCON->SYSAHBCLKCTRL0 &= ~(1 << (14 + PeriIndex));							// Disable system clock for GPIOx
	return(true);
}


// ------------------------------------------------------------------------------------------------
// Pin configure - for LPC15xx (with SWM Block)
// Index- target register
// 	0	- IOCON : bit 7-0
// 	1	- IOCON : bit 15-8
//	2	- SWM_ENABLE:. SWM_ASSIGN

// if SWM_Pin = 0xfe - write into SWM reset value 0xff = unuse SWM
// if SWM Pin = 0xff - write into pinenable reg, not pinmovable
// write into register Reg_Idx value of the Reg_Val
void _Chip_GPIO_Wr_Conf_Pin (uint32_t Port, uint32_t Pin, uint8_t Reg_Idx, const _Chip_IO_Spec_t *Reg_Val)		
{
	uint32_t tmp = 0;

	LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 13);										// Enable system clock for IOCON
	LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 12);										// Enable system clock for SWM
	
	tmp = *((&LPC_IOCON->PIO0_0) + (Port * 32) + Pin);								// read original value of the IOCON
	tmp &= 0xffff0000;																// clear lower 2 bytes
	tmp |= Reg_Val->IOCON_Reg;														// save - 0 byte 

	*((&LPC_IOCON->PIO0_0)+ (Port * 32) + Pin) = tmp;								// and write to IOCON
	
	
	uint8_t reg1 = Reg_Val->SWM_Reg;												// read first SWM value
	uint8_t reg2 = Reg_Val->SWM_Pin;												// read second SWM value
	if(reg2 == 0xff)						// write to pinenable					// ifd second value of the SWM = 0xff, we will write into PIN_ENABLE register
	{
		if ( reg1 != 0xff)															// if isn't active = 0xff, nothing to do
			_Chip_SWM_EnableFixedPin ((CHIP_SWM_PIN_FIXED_T)reg1);					// if is pin active - write vaulues into LPC_SWM->PINENABLEx (will clear entered bit also)
	} 
	else									// pin movable
	{ 
		if( reg2 == 0xfe)
			_Chip_SWM_MovablePinAssign((CHIP_SWM_PIN_MOVABLE_T) reg1, 0xff);		// de-re-map pin from Reg_Val[2] to pin from Reg_Val[3]
		else
			_Chip_SWM_MovablePinAssign((CHIP_SWM_PIN_MOVABLE_T) reg1, reg2);		// re-maping pin from Reg_Val[2] to pin from Reg_Val[3]
	}

	LPC_SYSCON->SYSAHBCLKCTRL0 &= ~(1 << 13);										// Disable system clock for IOCON
	LPC_SYSCON->SYSAHBCLKCTRL0 &= ~(1 << 12);										// Disable system clock for SWM
}


// ------------------------------------------------------------------------------------------------
// Pin configure - interrupt generation conditions - Pin change interrupt
//	Sens (for whole group!):
// 	0x00	- level - low
// 	0x11	- level - high
// 	0x01	- edge - low to high
// 	0x10	- edge - high to low
// return is pointer to PINT
void *_Chip_GPIO_PINT_Conf(uint8_t PINTSel, uint32_t Port, uint32_t Pin, uint8_t Sens)
{
	if(PINTSel > 7) return(NULL);													// LPC15xx has PINTSEL 0 - 7 only
	
	LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 18);										// Enable system clock for PINT

	LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 11);										// Enable system clock for MUX
	LPC_INMUX->PINTSEL[PINTSel] = (Port * 32) + Pin;								// select port and pin to input MUX
	LPC_SYSCON->SYSAHBCLKCTRL0 &= ~(1 << 11);										// Disable system clock for MUX
	
	
	switch(Sens)
	{
		case 0x00: 						// low level interrupt
			{
				LPC_PINT->ISEL |= (1 << PINTSel);									// select LEVEL mode
				LPC_PINT->SIENR |= (1 << PINTSel);									// Enable level interrupt
				LPC_PINT->CIENF |= (1 << PINTSel);									// select low-level
				break;
			}
		case 0x01:						// Rising edge interrupt
			{
				LPC_PINT->ISEL &= ~(1 << PINTSel);									// select rising EDGE mode
				LPC_PINT->SIENR |= (1 << PINTSel);									// Enable rising edge interrupt
				LPC_PINT->CIENF |= (1 << PINTSel);									// Disable falling edge interrupt
				
				break;
			}
		case 0x10:						// Falling edge interrupt
			{
				LPC_PINT->ISEL &= ~(1 << PINTSel);									// select falling EDGE mode
				LPC_PINT->CIENR |= (1 << PINTSel);									// Disable rising edge interrupt
				LPC_PINT->SIENF |= (1 << PINTSel);									// Enable falling edge interrupt
				
				break;
			}
		case 0x22:						// Rising or Falling edge interrupt
			{
				LPC_PINT->ISEL &= ~(1 << PINTSel);									// select rising EDGE mode
				LPC_PINT->SIENR |= (1 << PINTSel);									// Enable rising edge interrupt
				LPC_PINT->SIENF |= (1 << PINTSel);									// Enable falling edge interrupt
				
				break;
			}
		case 0x11: 						// High level interrupt
			{
				LPC_PINT->ISEL |= (1 << PINTSel);									// select LEVEL mode
				LPC_PINT->SIENR |= (1 << PINTSel);									// Enable level interrupt
				LPC_PINT->SIENF |= (1 << PINTSel);									// select high level
			
				break;
			}
		default: return(false);
	}
	LPC_PINT->RISE |= (1 << PINTSel);												// clear rise flag
	LPC_PINT->FALL |= (1 << PINTSel);												// clear fall flag
	NVIC_ClearPendingIRQ((IRQn_Type) ((uint32_t) PIN_INT0_IRQn + PINTSel) );		// clear pending interrupt flag
	return(LPC_PINT);
}

// ------------------------------------------------------------------------------------------------
// Pin configure - interrupt generation conditions - Grouped interrupt
//	Sens (for whole group!):
// 	0x00	- level - low
// 	0x11	- level - high
// 	0x01	- edge - low to high
// 	0x10	- edge - high to low
// return is pointer to GINT
void *_Chip_GPIO_GINT_Conf(uint8_t Group, uint32_t Port, uint32_t Pin, uint8_t Sens)
{
	if(Group > 1) return(NULL);														// LPC15xx has group 0,1 only
	
	LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 19);										// Enable system clock for GINT

	LPC_GINT[Group].CTRL &= ~GPIOGR_COMB;											// GPIO interrupt OR(0)/AND(1) mode bit - set OR condition
	
	switch(Sens)
	{
		case 0x00: 
			{
				LPC_GINT[Group].PORT_POL[Port] &= ~(1 << Pin);						// set polarity to 0
				LPC_GINT[Group].CTRL |= GPIOGR_TRIG;								// select LEVEL mode
				break;
			}
		case 0x01:
			{
				LPC_GINT[Group].PORT_POL[Port] |= (1 << Pin);						// set polarity to 1
				LPC_GINT[Group].CTRL &= ~GPIOGR_TRIG;								// select EDGE mode
				break;
			}
		case 0x10:
			{
				LPC_GINT[Group].PORT_POL[Port] &= ~(1 << Pin);						// set polarity to 0
				LPC_GINT[Group].CTRL &= ~GPIOGR_TRIG;								// select EDGE mode
				break;
			}
		case 0x11: 
			{
				LPC_GINT[Group].PORT_POL[Port] |= (1 << Pin);						// set polarity to 1
				LPC_GINT[Group].CTRL |= GPIOGR_TRIG;								// select LEVEL mode
				break;
			}
		default: return(false);
				
	}
	
	// Clear IRQ:
	// Enable IRQ:
	
	LPC_GINT[Group].PORT_ENA[Port] |= 1 << Pin;										// Enable Pin Interrupt
	LPC_GINT[Group].CTRL |= 1 << Pin;												// clear pending Interrupt
	if(Group ==0)
	{
		NVIC_ClearPendingIRQ(GINT0_IRQn);											// clear pending interrupt flag
		NVIC_EnableIRQ( (IRQn_Type) (GINT0_IRQn));								// Enable GINT0 interrupt
		return(&LPC_GINT[0]);
	}
	else
	{
		NVIC_ClearPendingIRQ(GINT1_IRQn);											// clear pending interrupt flag
		NVIC_EnableIRQ( (IRQn_Type) (GINT1_IRQn));								// Enable GINT1 interrupt		
		return(&LPC_GINT[0]);
	}
}


// ------------------------------------------------------------------------------------------------
// Pin configure - enable/disable interrupt generation from selectet Port/Pin
bool _Chip_GPIO_IRQ_Enable(void *pPeri, uint32_t Port, uint32_t Pin, bool NewState)
{
	if(NewState == true)
	{
		NVIC_EnableIRQ( (IRQn_Type) (GINT0_IRQn));									// Enable GINT0 interrupt
	}
	else
	{
		NVIC_DisableIRQ( (IRQn_Type) (GINT0_IRQn));									// Disable GINT0 interrupt
	}
	return(true);
}








// ************************************************************************************************
// I2C Functions 
// ************************************************************************************************

// ------------------------------------------------------------------------------------------------
// return periphery index
inline int32_t _Chip_I2C_Get_PeriIndex(void *pPeri)									// Return Index from pointer
{
	if((_CHIP_I2C_T*) pPeri == LPC_I2C0) return(0);
	return(-1);																		// otherwise error
}


// ------------------------------------------------------------------------------------------------------
// I2C(Num) Initialization
// result is true:successfull otherwise false
void *_Chip_I2C_Init(int32_t PeriIndex)
{
	// Check:
	if(PeriIndex != 0) return(NULL);												// LPC15xx have only one I2C

	// Power:
	// Nothing to power On/Off
	
	// Clock:
	LPC_SYSCON->SYSAHBCLKCTRL1 |= (1 << 13); 										// enable I2C clock
	
	// Reset:
	LPC_SYSCON->PRESETCTRL1 |= (1 << 13);											// release reset
	LPC_SYSCON->PRESETCTRL1 &= ~(1 << 13);											// generate reset
	
	//if(LPC_I2C0->STAT & 0x0001) return(NULL);			                           	// I2C not ready? Missing PULLUP?
	
	// Clear IRQ:
	NVIC_ClearPendingIRQ(I2C0_IRQn);												// clear pending interrupt flag
	
	// Disable IRQ
	NVIC_DisableIRQ((I2C0_IRQn));													// Enable I2C interrupt
	return((void *) LPC_I2C0);
}

// ------------------------------------------------------------------------------------------------
// Set SCL Low and High level time
inline void _Chip_I2CM_SetDutyCycle(void *pPeri, uint16_t sclH, uint16_t sclL)
{
	((_CHIP_I2C_T*)pPeri)->MSTTIME = (((sclH - 2) & 0x07) << 4) | ((sclL - 2) & 0x07);
}


// ------------------------------------------------------------------------------------------------
// Set clock divisor
inline void _Chip_I2C_SetClockDiv(void *pPeri, uint32_t clkdiv)
{
	if ((clkdiv >= 1) && (clkdiv <= 0x10000)) ((_CHIP_I2C_T*)pPeri)->CLKDIV = clkdiv - 1;
	else ((_CHIP_I2C_T*)pPeri)->CLKDIV = 0;
}


// ------------------------------------------------------------------------------------------------
// Read I2C status
inline uint32_t _Chip_I2C_Get_Peri_Status(void *pPeri)
{
	return(((_CHIP_I2C_T*)pPeri)->STAT);
}

// ------------------------------------------------------------------------------------------------
// Clear I2C MASTER status - clear by disable and reenable selected interrupts.
inline bool _Chip_I2C_Clear_Peri_Status(void *pPeri, uint32_t NewValue)
{
	((_CHIP_I2C_T*)pPeri)->STAT |= NewValue;
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Read I2C MASTER interrupt status
uint32_t _Chip_I2CMST_Get_IRQ_Status(void *pPeri)
{
	uint32_t Result;
	uint32_t rdstat = ((_CHIP_I2C_T*)pPeri)->INTSTAT;								// read chip interrupt status
	
	switch (rdstat & 0xf8)															// recode chip status to CHAL simples status. See UM10470, Table 512
	{
		case (1 << 4): Result |= CHIP_I2C_IRQSTAT_MSARBLOSS; break;					// Master Arbitration Loss Interrupt Status Bit
		case (1 << 6): Result |= CHIP_I2C_IRQSTAT_MSSTSTPERR; break;				// Master Start Stop Error Interrupt Status Bit
		case (1 << 25): Result |= CHIP_I2C_IRQSTAT_SCLTIMEOUT; break;				// SCL Timeout Interrupt Status Bit
		case (1 << 24): Result |= CHIP_I2C_IRQSTAT_EVENTTIMEOUT; break;					// Event Timeout Interrupt Status Bit
		case (1 << 0): Result |= CHIP_I2C_IRQSTAT_MSSTATE_CHANGED; 					// Master Pending Interrupt Status Bit
			{
				uint32_t i2cstat = ((_CHIP_I2C_T*)pPeri)->STAT;						// read chip i2c status
				switch((i2cstat >> 1) & 0x07)
				{
					case 0x01: Result |= CHIP_I2C_MSSTATE_RX; break;		// RX Ready
					case 0x02: Result |= CHIP_I2C_MSSTATE_TX; break;		// TX Ready
					case 0x03: Result |= CHIP_I2C_MSSTATE_NACK_ADR; break;	// Addres non ACK
					case 0x04: Result |= CHIP_I2C_MSSTATE_NACK_DATA; break; // Data non ACK
					
				}
				if (i2cstat & (1 << 4)) Result |= CHIP_I2C_IRQSTAT_MSARBLOSS;		// Master Arbitration Loss Interrupt Status Bit
				if (i2cstat & (1 << 6)) Result |= CHIP_I2C_IRQSTAT_MSSTSTPERR;		// Start/Stop error
				break;
			}
		case (1 << 16): Result |= CHIP_I2C_IRQSTAT_MONRDY; break;					// Monitor Ready Interrupt Status Bit
		case (1 << 17): Result |= CHIP_I2C_IRQSTAT_MONOV; break;					// Monitor Overflow Interrupt Status Bit
		case (1 << 19): Result |= CHIP_I2C_IRQSTAT_MONIDLE; break;					// Monitor Idle Interrupt Status Bit
		
		case (1 << 15): Result |= CHIP_I2C_IRQSTAT_SLVDESEL; break;					// Slave Deselect Interrupt Status Bit
		case (1 << 11): Result |= CHIP_I2C_IRQSTAT_SLVNOTSTR; break;				// Slave not stretching Clock Interrupt Status Bit
		case (1 << 8): Result |= CHIP_I2C_IRQSTAT_SLVPENDING; break;				// Slave Pending Interrupt Status Bit
			
		default:  Result = 0;
	}
	return(Result);
}


// ------------------------------------------------------------------------------------------------
// Read I2C SLAVE interrupt status
uint32_t _Chip_I2CSLV_Get_IRQ_Status(void *pPeri)
{
	uint32_t Result = 0;
	uint32_t rdstat = ((((_CHIP_I2C_T*)pPeri)->INTSTAT) & (((_CHIP_I2C_T *)pPeri)->INTENSET));	// read chip interrupt status
	
	switch (rdstat) // & 0xf8)														// recode chip status to CHAL simples status. See UM10470, Table 512
	{
		case (1 << 15): Result |= CHIP_I2C_IRQSTAT_SLVDESEL; break;					// Slave Deselect Interrupt Status Bit
		case (1 << 11): Result |= CHIP_I2C_IRQSTAT_SLVNOTSTR; break;				// Slave not stretching Clock Interrupt Status Bit
		case (1 << 8): Result |= CHIP_I2C_IRQSTAT_SLVPENDING; break;				// Slave Pending Interrupt Status Bit
			
		default:  Result = 0;
	}
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// Read I2C MONITOR interrupt status
uint32_t _Chip_I2CMON_Get_IRQ_Status(void *pPeri)
{
	uint32_t Result = 0;
	uint32_t rdstat = ((((_CHIP_I2C_T*)pPeri)->INTSTAT) & (((_CHIP_I2C_T *)pPeri)->INTENSET));	// read chip interrupt status
	
	switch (rdstat) // & 0xf8)														// recode chip status to CHAL simples status. See UM10470, Table 512
	{
		case (1 << 16): Result |= CHIP_I2C_IRQSTAT_MONRDY; break;					// Monitor Ready Interrupt Status Bit
		case (1 << 17): Result |= CHIP_I2C_IRQSTAT_MONOV; break;					// Monitor Overflow Interrupt Status Bit
		case (1 << 19): Result |= CHIP_I2C_IRQSTAT_MONIDLE; break;					// Monitor Idle Interrupt Status Bit
			
		default:  Result = 0;
	}
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// Read I2C TIMEOUT interrupt status
uint32_t _Chip_I2CTimeout_Get_IRQ_Status(void *pPeri)
{
	uint32_t Result = 0;
	uint32_t rdstat = ((((_CHIP_I2C_T*)pPeri)->INTSTAT) & (((_CHIP_I2C_T *)pPeri)->INTENSET));	// read chip interrupt status
	
	switch (rdstat) // & 0xf8)														// recode chip status to CHAL simples status. See UM10470, Table 512
	{
		case (1 << 25): Result |= CHIP_I2C_IRQSTAT_SCLTIMEOUT; break;				// SCL Timeout Interrupt Status Bit
		case (1 << 24): Result |= CHIP_I2C_IRQSTAT_EVENTTIMEOUT; break;				// Event Timeout Interrupt Status Bit
			
		default:  Result = 0;
	}
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// Clear I2C MASTER interrupt status
inline bool _Chip_I2CMST_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Clear I2C SLAVE interrupt status
inline bool _Chip_I2CSLV_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Clear I2C MONITOR interrupt status
inline bool _Chip_I2CMON_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Clear I2C Timeout interrupt status
inline bool _Chip_I2CTimeout_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	((_CHIP_I2C_T*)pPeri)->STAT |= (1<<24);											// clear ststus flags
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected MASTER interrupts
inline bool _Chip_I2CMST_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_I2C_T*)pPeri)->INTENSET = IRQBitMask & 0x7f;
	else ((_CHIP_I2C_T*)pPeri)->INTENCLR = IRQBitMask  & 0x7f;
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected SLAVE interrupts
inline bool _Chip_I2CSLV_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_I2C_T*)pPeri)->INTENSET = IRQBitMask & 0x8900;
	else ((_CHIP_I2C_T*)pPeri)->INTENCLR = IRQBitMask  & 0x8900;
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected MONITOR interrupts
inline bool _Chip_I2CMON_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_I2C_T*)pPeri)->INTENSET = IRQBitMask & 0x000B0000;
	else ((_CHIP_I2C_T*)pPeri)->INTENCLR = IRQBitMask  & 0x000B0000;
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Enable/disable selected Timeout interrupts
inline bool _Chip_I2CTimeout_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_I2C_T*)pPeri)->INTENSET = IRQBitMask & 0x03000000;
	else ((_CHIP_I2C_T*)pPeri)->INTENCLR = IRQBitMask  & 0x03000000;
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// I2C - write data into I2C
inline void _Chip_I2CMST_Put_Data(void *pPeri, uint32_t NewValue)
{
	((_CHIP_I2C_T *)pPeri)->MSTDAT = (uint32_t) NewValue;							// write data to I2C
}

// ------------------------------------------------------------------------------------------------
// I2C - Read Data 
inline uint32_t _Chip_I2CMST_Get_Data(void *pPeri)
{
	return(((_CHIP_I2C_T *)pPeri)->MSTDAT);											// read data from I2C
}

// ------------------------------------------------------------------------------------------------
// I2C - Create START condition 
inline void _Chip_I2CMST_Set_Start(void *pPeri)								// create start impulse
{
	((_CHIP_I2C_T *)pPeri)->MSTCTL = (1 << 1);
}

// ------------------------------------------------------------------------------------------------
// I2C - Create STOP condition
inline void _Chip_I2CMST_Set_Stop(void *pPeri)
{
	((_CHIP_I2C_T *)pPeri)->MSTCTL = (1 << 2);										// create stop impulse
}

// ------------------------------------------------------------------------------------------------
// I2C - Master Continue
inline void _Chip_I2CMST_Set_Cont(void *pPeri)
{
	((_CHIP_I2C_T *)pPeri)->MSTCTL = (1 << 0);										// ACK
}

// ------------------------------------------------------------------------------------------------
// I2C - Reset comm.
inline void _Chip_I2CMST_Reset(void *pPeri)
{	// LPC546xx has no normal reset. Better solution is disable and reenable I2C periphery.
	((_CHIP_I2C_T *)pPeri)->CFG &= ~(1 << 0);										// Disable I2C
	((_CHIP_I2C_T *)pPeri)->CFG |= (1 << 0);										// Enable I2C
}

// ------------------------------------------------------------------------------------------------------
// I2C Master Enable/disable
// result is true:successfull otherwise false
bool _Chip_I2CMST_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		((_CHIP_I2C_T*) pPeri)->CFG |= (1 << 0);									// enable master functions
	}
	else
	{
		((_CHIP_I2C_T*) pPeri)->CFG &= ~(1 << 0);									// disable Master functions
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// I2C Slave Enable/disable
// result is true:successfull otherwise false
bool _Chip_I2CSLV_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		((_CHIP_I2C_T*) pPeri)->CFG |= (1 << 1);							// enable Slave functions
	}
	else
	{
		((_CHIP_I2C_T*) pPeri)->CFG &= ~(1 << 1);							// disable Slave functions
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// I2C Monitor Enable/disable
// result is true:successfull otherwise false
bool _Chip_I2CMON_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		((_CHIP_I2C_T*) pPeri)->CFG |= (1 << 2);							// enable Monitor functions
	}
	else
	{
		((_CHIP_I2C_T*) pPeri)->CFG &= ~(1 << 2);							// disable Monitor functions
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------
// I2C Interrupt - enable/disable
bool _Chip_I2CMST_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	int32_t PeriIndex = _Chip_I2C_Get_PeriIndex(pPeri);
	
	if(PeriIndex < 0) return (false);
	
	if(NewState == true)
	{
		NVIC_ClearPendingIRQ (I2C0_IRQn);											// Clear NVIC interrupt flag
		NVIC_EnableIRQ(I2C0_IRQn);													// Enable NVIC interrupt
	}
	else
	{
		NVIC_DisableIRQ(I2C0_IRQn);													// Enable NVIC interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------
// I2C Slave Interrupt - enable/disable
inline bool _Chip_I2CSLV_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	return(_Chip_I2CMST_Enable_IRQ_NVIC(pPeri, NewState));							// this MCU used same IRQ for all modes
}

// ------------------------------------------------------------------------------------------------
// I2C Monitor Interrupt - enable/disable
inline bool _Chip_I2CMON_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	return(_Chip_I2CMST_Enable_IRQ_NVIC(pPeri, NewState));							// this MCU used same IRQ for all modes
}

// ------------------------------------------------------------------------------------------------
// I2C Interrupt - enable/disable
inline bool _Chip_I2CTimeout_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	return(_Chip_I2CMST_Enable_IRQ_NVIC(pPeri, NewState));							// this MCU used same IRQ for all modes
}

// ------------------------------------------------------------------------------------------------------
// I2C Set I2C interface speed in kHz
// result is true:successfull otherwise false
bool _Chip_I2C_Set_BusSpeed(void *pPeri, uint32_t Speed_kHz)						// Max Speed: 100kHz, 400kHz, 1MHz
{
	uint8_t scl;
	uint32_t CLKDiv;
	
	CLKDiv = 1;																		// start at divider = 1
		
	do
	{	
		//Chip_I2C_SetClockDiv((_CHIP_I2C_T*)pPeri, CLKDiv);						// set new peripherial divider
		((_CHIP_I2C_T*)pPeri)->CLKDIV = (CLKDiv - 1) & 0x0000ffff;
		
		scl = _Chip_Clock_Get_SysClk_Rate() / ((  ((_CHIP_I2C_T*)pPeri)->CLKDIV + 1) * Speed_kHz);
		CLKDiv ++;
		if(CLKDiv > 65535) return(false);
	}while((scl < 1) || (scl > 8));													// recalculate while scl is out of range
	
	_Chip_I2CM_SetDutyCycle((_CHIP_I2C_T*)pPeri, (scl >> 1), (scl - (scl >> 1)));
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// I2C Get I2C interface speed in kHz
uint32_t _Chip_I2C_Get_BusSpeed(void *pPeri)										// Return Current bus speed
{
	uint32_t SCLL = (((_CHIP_I2C_T*)pPeri)->MSTTIME & 0x000007) + 2;				// get SCL Low
	uint32_t SCLH = (((_CHIP_I2C_T*)pPeri)->MSTTIME & 0x000070) + 2;				// get SCL High
	return((_Chip_Clock_Get_SysClk_Rate() / ((_CHIP_I2C_T*)pPeri)->CLKDIV + 1) / (SCLL + SCLH) / 1000); //return in kHz
}

// ------------------------------------------------------------------------------------------------------
// I2C Configure I2C
// result is true:successfull otherwise false
bool _Chip_I2C_Configure(void *pPeri, uint32_t Speed_kHz, bool MasterMode, bool SlaveMode, bool MonitorMode, uint16_t Address)
{
	bool res = false;
	
	SYS_ASSERT(pPeri != NULL);
	if(pPeri == NULL) return(false);
	
//	26.7.1.1 Rate calculations
//		SCL high time (in I2C function clocks) = (CLKDIV + 1) * (MSTSCLHIGH + 2)
//		SCL low time (in I2C function clocks) = (CLKDIV + 1) * (MSTSCLLOW + 2)
//		Nominal SCL rate = I2C function clock rate / (SCL high time + SCL low time)

	if (MasterMode == true)
	{
		res = _Chip_I2C_Set_BusSpeed(pPeri, Speed_kHz);
	}
	
	return(res);
}


//// ------------------------------------------------------------------------------------------------
//// Enable/disable selected interrupts
//extern inline bool _Chip_I2C_IRQMask_Enable(void *pPeri, uint32_t IRQBitMask, bool NewState)
//{
//	if(NewState) ((_CHIP_I2C_T*)pPeri)->INTENSET |= IRQBitMask;
//	else ((_CHIP_I2C_T*)pPeri)->INTENCLR |= IRQBitMask;
//	
//	return(true);
//}

//// ------------------------------------------------------------------------------------------------
//// Clear I2C MASTER interrupt status
//static inline bool _Chip_I2CM_Clear_IRQStatus(void *pPeri, uint32_t NewValue)
//{
//	return(true);																	// IRQ Status can't clear
//}

////// ------------------------------------------------------------------------------------------------------
////// I2C Enable/disable
////// result is true:successfull otherwise false
////bool _Chip_I2C_Enable(void *pPeri, bool NewState)
////{
////	if(pPeri == NULL ) return(false);
////	
////	if(NewState)
////	{
////		((_CHIP_I2C_T*) pPeri)->CFG |= I2C_CFG_MSTEN | I2C_CFG_SLVEN | I2C_CFG_MONEN;	// enable all functions
////	}
////	else
////	{
////		((_CHIP_I2C_T*) pPeri)->CFG &= ~(I2C_CFG_MSTEN | I2C_CFG_SLVEN | I2C_CFG_MONEN);// disable all functions
////	}
////	return(true);
////}

////// ------------------------------------------------------------------------------------------------
////// I2C Interrupt - enable/disable
////bool _Chip_I2C_IRQ_Enable(void *pPeri, bool NewState)
////{
////	if(NewState == true)
////	{
////		NVIC_ClearPendingIRQ(I2C0_IRQn);										// Clear I2C0 interrupt flag
////		NVIC_EnableIRQ(I2C0_IRQn);													// Enable I2C0 interrupt
////	}
////	else
////	{
////		NVIC_DisableIRQ(I2C0_IRQn);													// Enable I2C0 interrupt
////	}
////	return(true);
////}








// ------------------------------------------------------------------------------------------------
// UART Functions
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// Return periphery index
int32_t _Chip_UART_Get_PeriIndex(void *pPeri)										// Return Index from pointer
{
#if _CHIP_UART_COUNT >= 4
	if((_CHIP_UART_T*) pPeri == LPC_USART3) return(3);
#endif	
#if _CHIP_UART_COUNT >= 3
	if((_CHIP_UART_T*) pPeri == LPC_USART2) return(2);
#endif	
#if _CHIP_UART_COUNT >= 2
	if((_CHIP_UART_T*) pPeri == LPC_USART1) return(1);
#endif	
#if _CHIP_UART_COUNT >= 1
	if((_CHIP_UART_T*) pPeri == LPC_USART0) return(0);
#endif	
	return(-1);
}

// ------------------------------------------------------------------------------------------------
// ADR DETECTION Control
inline bool _Chip_UART_Set_AdrDet(void* pPeri, bool NewVal)
{
	if(NewVal)
	{
		((_CHIP_UART_T *)pPeri)->CFG |= 1 << 19;									// AUTOADDR bit set
		((_CHIP_UART_T *)pPeri)->CTRL |= 1 << 2;									// ADDRDET bit set
	}
	else 
	{
		((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 19);									// AUTOADDR bit clear
		((_CHIP_UART_T *)pPeri)->CTRL &= ~(1 << 2);									// ADDRDET bit clear
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Write New Address 
// result is true:successfull otherwise false
inline bool _Chip_UART_Set_Address(void* pPeri, uint8_t NewAddress)
{
	((_CHIP_UART_T *)pPeri)->ADDR = NewAddress;										// Save address

	if(NewAddress)
	{
		_Chip_UART_Set_AdrDet(pPeri, true);											// activate ADRDET
	}
	else
	{
		_Chip_UART_Set_AdrDet(pPeri, false);										// deactivate ADRDET
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Read Address 
inline uint8_t _Chip_UART_Get_Address(void* pPeri)
{
	return(((_CHIP_UART_T *)pPeri)->ADDR & 0x00ff);
}

// ------------------------------------------------------------------------------------------------
// Read UART status
inline uint32_t _Chip_UART_Get_Peri_Status(void *pPeri)
{
	return(((_CHIP_UART_T*)pPeri)->STAT);
}


// ------------------------------------------------------------------------------------------------
// Clear UART status - clear by disable and reenable selected interrupts.
inline bool _Chip_UART_Clear_Peri_Status(void *pPeri, uint32_t NewValue)
{
	((_CHIP_UART_T*)pPeri)->STAT = NewValue;
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Get UART interrupt status
inline uint32_t _Chip_UART_Get_IRQ_Status(void *pPeri)
{
	return( (((_CHIP_UART_T *)pPeri)->INTSTAT) & (((_CHIP_UART_T *)pPeri)->INTENSET) );	// status masked with Enabled
}

// ------------------------------------------------------------------------------------------------
// Clear UART interrupt status - clear by disable and reenable selected interrupts.
inline bool _Chip_UART_Clear_IRQ_Status(void *pPeri,uint32_t IRQBitMask)
{
	return(true);																	// 
}

// ------------------------------------------------------------------------------------------------
inline void _Chip_UART_Enable_IRQ (void *pPeri, uint32_t IRQBitMask, bool NewState)		// Enable/Disable UARTs Interrupts 
{
	if(NewState) ((_CHIP_UART_T*)pPeri)->INTENSET = IRQBitMask;
	else ((_CHIP_UART_T*)pPeri)->INTENCLR = IRQBitMask;
}

// ------------------------------------------------------------------------------------------------
// Write char to OutBuff
inline void _Chip_UART_Put_Data(void *pPeri, uint32_t WrData)
{
	((_CHIP_UART_T *)pPeri)->TXDATA = WrData & 0x1FF;								// write data to UART - 9-bit value
}

// ------------------------------------------------------------------------------------------------
// Read char from InBuff
inline uint32_t _Chip_UART_Get_Data(void *pPeri)
{
	return(((_CHIP_UART_T *)pPeri)->RXDATA & 0x1ff);								// read data from UART - 9-bit value
}

// ------------------------------------------------------------------------------------------------
// Flush Uart Tx
inline void _Chip_UART_Flush_Tx(void *pPeri)
{
	
}

// ------------------------------------------------------------------------------------------------
// Flush Uart Rx
inline void _Chip_UART_Flush_Rx(void *pPeri)
{
	
}


// ------------------------------------------------------------------------------------------------------
// UART(Num) Initialization
// return is pointer to periphery, otherwise NULL
void *_Chip_UART_Init(int32_t PeriIndex)
{
	void *res = NULL;
	
	NVIC_DisableIRQ( (IRQn_Type) (UART0_IRQn + PeriIndex));							// Disable UART interrupt
	switch(PeriIndex)
	{
		case 2:
		{
			LPC_SYSCON->SYSAHBCLKCTRL1 |= (1 << 19);								// Enable system clock for UART2
			LPC_SYSCON->PRESETCTRL1 |= (1 << 19);__nop();							// Activate reset
			LPC_SYSCON->PRESETCTRL1 &= ~(1 << 19);									// Deactivate reset			
			res = LPC_USART2;														// return pointer to periphery
			break;
		}
		case 1:
		{
			LPC_SYSCON->SYSAHBCLKCTRL1 |= (1 << 18);								// Enable system clock for UART1
			LPC_SYSCON->PRESETCTRL1 |= (1 << 18);__nop();							// Activate reset
			LPC_SYSCON->PRESETCTRL1 &= ~(1 << 18);									// Deactivate reset
			
			res = LPC_USART1;														// return pointer to periphery
			break;
		}
		case 0:
		{
			LPC_SYSCON->SYSAHBCLKCTRL1 |= (1 << 17);								// Enable system clock for UART0
			LPC_SYSCON->PRESETCTRL1 |= (1 << 17);__nop();							// Activate reset
			LPC_SYSCON->PRESETCTRL1 &= ~(1 << 17);									// Deactivate reset
			
			res = LPC_USART0;														// return pointer to periphery
			break;
		}
		default:
		{
			res = NULL;																// bad index of periphery
		}
	}
	return(res);
}

// ------------------------------------------------------------------------------------------------
// UART Interrupt - enable/disable
bool _Chip_UART_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	int32_t PeriIndex = _Chip_UART_Get_PeriIndex(pPeri);

	// Check:
	SYS_ASSERT( pPeri != NULL);												// check
	
	if(PeriIndex < 0) return (false);
	
	if(NewState == true)
	{
		NVIC_ClearPendingIRQ ((IRQn_Type) (UART0_IRQn + PeriIndex));		// Clear UART interrupt flag
		NVIC_EnableIRQ((IRQn_Type) (UART0_IRQn + PeriIndex));				// Enable UART interrupt
	}
	else
	{
		NVIC_DisableIRQ((IRQn_Type) (UART0_IRQn + PeriIndex));				// Enable UART interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Enable/Disable UART
bool _Chip_UART_Enable(void *pPeri, bool NewState)
{	
//	// Check:
	SYS_ASSERT(pPeri != NULL);														// check
	//if(pPeri == NULL ) return(false);
	
	uint8_t PeriIndex = _Chip_UART_Get_PeriIndex(pPeri);

	if(NewState == true)
	{
		uint16_t waitfor = 5000;
		do
		{
			if(waitfor) waitfor --;
			else 
			{
				if((((_CHIP_UART_T *)pPeri)->CFG & 0x01) == NewState) return(true);	// Change was accepted?
				else return(false);
			}
		}while((((_CHIP_UART_T *)pPeri)->STAT & (CHAL_UART_IRQSTAT_RXIDLE | CHAL_UART_IRQSTAT_TXIDLE)) != (CHAL_UART_IRQSTAT_RXIDLE | CHAL_UART_IRQSTAT_TXIDLE));			// wait for ones
		
		((_CHIP_UART_T *)pPeri)->CFG |= 0x01;										// Enable UART
		_Chip_UART_Enable_IRQ(pPeri, CHIP_UART_IRQEN_TXRDY | CHIP_UART_IRQEN_RXRDY, true);	// Enable RX and Rx Interrupt
	}
	else
	{
		((_CHIP_UART_T *)pPeri)->CFG &= ~0x01;										// Disable UART
		_Chip_UART_Enable_IRQ(pPeri, CHIP_UART_IRQEN_TXRDY | CHIP_UART_IRQEN_RXRDY, false);	// Disable RX and Rx Interrupt
	}
	return(false);
}


// ------------------------------------------------------------------------------------------------------
// Baud Rate settings. In case of overflow, increased divider from MainCLK
// result is true:successfull otherwise false
bool _Chip_UART_Set_Baud(void* pPeri, uint32_t NewBaud)
{
	uint32_t Div = 0;
	uint32_t baudRateGenerator;
	
	if(pPeri == NULL ) return(false);
	
	LPC_SYSCON->FRGCTRL = 0;		// fractal divider zet to zero;
	do
	{
		
		LPC_SYSCON->UARTCLKDIV = Div++;
		if( Div > 255)
		{
			sys_Err ("Chip_Clock_SetUARTFRGDivider failed!");
			return(false);															// cannot set divider!
		}
		baudRateGenerator = _Chip_Clock_Get_UARTClk_Rate(0) / (16 * NewBaud);		// Parameter 0 - LPC15xx have onlu one clk src for all UARTs
		if(baudRateGenerator) baudRateGenerator -= 1;								// new baud rate
	}while((baudRateGenerator == 0) || baudRateGenerator >= UINT16_MAX);			// still overflow?
	((_CHIP_UART_T *)pPeri)->BRG = baudRateGenerator ;								// write a new BaudRate
	
	// spatny prepocet a jemna korekcia FRG registrom:
	float tmpFRG = (float)((float)_Chip_Clock_Get_UARTClk_Rate(0) / (float)(baudRateGenerator * (16 * NewBaud))) - 1;
	LPC_SYSCON->FRGCTRL = (uint8_t) (tmpFRG * 256);
	
	return(true);
}


// ------------------------------------------------------------------------------------------------------
// Baud Rate settings. Get interface speed in baud
// result is true:successfull otherwise false
uint32_t _Chip_UART_Get_Baud(void* pPeri)
{
    uint32_t srcClock_Hz = _Chip_Clock_Get_UARTClk_Rate(0);							// Parameter 0 - LPC15xx have onlu one clk src for all UARTs
    uint32_t osrval, brgval, baudrate;

    // check arguments
    if ((pPeri == NULL) || (srcClock_Hz == 0U))
    {
        return (0);
    }

	osrval = ((_CHIP_UART_T *)pPeri)->OSR;
	brgval = ((_CHIP_UART_T *)pPeri)->BRG;

	
	
    // If synchronous master mode is enabled, only configure the BRG value.
    if ((((_CHIP_UART_T *)pPeri)->CFG & (1 << 11)) != 0U)							// Synchro or asynchro oiperation ?
    {
        if ((((_CHIP_UART_T *)pPeri)->CFG & (1 << 14)) != 0U)  						// Synchro master mode ?
        {
			
			baudrate = srcClock_Hz / (brgval + 1U);
            //brgval    = srcClock_Hz / NewBaud;
            ((_CHIP_UART_T *)pPeri)->BRG = brgval - 1U;
        }
    }
    else
    {
		baudrate = srcClock_Hz / ((osrval + 1U) * (brgval + 1U));
    }

    return (baudrate);
}


// ------------------------------------------------------------------------------------------------------
// Write configuration into UART
// DataLen: 7,8,9
// Parity: 0-none, 2-Odd, 3-Even
// Stopbits: 0-1, 1-2
//bool _Chip_UART_ConfigData(void* pPeri, uint8_t DataLen, uint8_t Parity, uint8_t StopBits, const CHAL_Signal_t *sCTS, const CHAL_Signal_t *sRTS)
bool _Chip_UART_Set_Conf(void* pPeri, uint8_t DataLen, uint8_t Parity, uint8_t StopBits)

{
	uint32_t Config;
	
	Config = (((DataLen - 7) << 2) & (3 << 2)) |  ((Parity << 4) & (3 << 4)) | (StopBits & (1 << 6));
	((_CHIP_UART_T*)pPeri)->CFG &= ~0x01;											// disable uart
	((_CHIP_UART_T*)pPeri)->CFG &= ~(0x7c);											// clear old settings
	((_CHIP_UART_T*)pPeri)->CFG |= Config;											// write new one
	
//	if(sCTS) ((_CHIP_UART_T *)pPeri)->CFG |= 1 << 9;								// Enable CTS
//	else ((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 9);									// Disable CTS	
	
	return(true);
}

//// ------------------------------------------------------------------------------------------------------
//// Write configuration into UART for RS485 mode
//bool _Chip_UART_RS485_ConfigData(void* pPeri, uint8_t Address, const CHAL_Signal_t *sDIR)
//{
//	((_CHIP_UART_T *)pPeri)->CFG |= 1 << 18;										// OETA - RS485 turnaround
//	
//	if(sDIR) 
//	{
//		((_CHIP_UART_T *)pPeri)->CFG |= (1 << 20);									// Output Enable selectby RTS
//		if(sDIR->Std.Act == 1) ((_CHIP_UART_T *)pPeri)->CFG |= 1 << 21;				// OEPOLarity depends on Signal Active state active High
//		else ((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 21);							// OEPOLarity depends on Signal Active state active Low
//	}
//	else ((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 20);								// only RTS

//	if(Address)
//	{
//		((_CHIP_UART_T *)pPeri)->ADDR = Address;									// Save address
//		((_CHIP_UART_T *)pPeri)->CFG |= 1 << 19;									// AUTOADDR set
//	}
//	
//	return(true);
//}

// ------------------------------------------------------------------------------------------------------
// Configure RTS as flow control for UART
// result is true:successfull otherwise false
bool _Chip_UART_Set_RTS_Conf(void* pPeri, uint8_t sRTS_Port, uint8_t sRTS_Pin)
{
	((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 18);										// OETA - RS485 OFF
	((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 20);										// Output Disable. Use only RTS

	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure CTS as flow control for UART
// result is true:successfull otherwise false
bool _Chip_UART_Set_CTS_Conf(void* pPeri, uint8_t sCTS_Port, uint8_t sCTS_Pin)
{
	((_CHIP_UART_T *)pPeri)->CFG |= 1 << 9;											// Enable CTS
//	else ((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 9);									// Disable CTS	

	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure DIRDE as flow control for RS485
// result is true:successfull otherwise false
// LPC17xx used RTS or DTR as OutputEnable
bool _Chip_UART_Set_DIRDE_Conf(void *pPeri, uint8_t DIRDE_Port, uint8_t DIRDE_Pin, bool DIRDE_H_Active)
{
	((_CHIP_UART_T *)pPeri)->CFG |= 1 << 18;										// OETA - RS485 turnaround
	
	((_CHIP_UART_T *)pPeri)->CFG |= (1 << 20);										// Output Enable select by RTS
	if(DIRDE_H_Active == 1) ((_CHIP_UART_T *)pPeri)->CFG |= 1 << 21;				// OEPOLarity depends on Signal Active state active High
	else ((_CHIP_UART_T *)pPeri)->CFG &= ~(1 << 21);								// OEPOLarity depends on Signal Active state active Low

	return(true);
}


// ------------------------------------------------------------------------------------------------------
// Configure RE as receive enable control in RS485 mode
// result is true:successfull otherwise false
bool _Chip_UART_Set_RE_Conf(void* pPeri, uint8_t RE_Port, uint8_t RE_Pin, bool RE_H_Active)
{
	return(false);																	// LPC15xx hasn't RE dedicated pin
}






// ------------------------------------------------------------------------------------------------
// SPI Functions 
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// SPI - Get peripharial index by ptr
int32_t _Chip_SPI_Get_PeriIndex(void *pPeri)										// Return Index from pointer
{
	SYS_ASSERT(pPeri != NULL);
	if(((_CHIP_SPI_T*) pPeri) == LPC_SPI1) return(1);
	if(((_CHIP_SPI_T*) pPeri) == LPC_SPI0) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------------
// SPI(Num) Initialization
// result is true:successfull otherwise false
void *_Chip_SPI_Init(int32_t PeriIndex)
{
	// Check:
	if(PeriIndex > 1) return(NULL);													// LPC15xx have only two SPI
	
	switch(PeriIndex)
	{
		case 0:
		{
			// Power:
			// Nothing to power On/Off
			
			// Clock:
			LPC_SYSCON->SYSAHBCLKCTRL1 |= (1 << 9); 								// enable SPI0 clock
			
			// Reset:
			LPC_SYSCON->PRESETCTRL1 |= (1 << 9);									// generate reset
			LPC_SYSCON->PRESETCTRL1 &= ~(1 << 9);									// release reset
			
			// Clear IRQ:
			NVIC_ClearPendingIRQ(SPI0_IRQn);										// clear pending interrupt flag
	
			// Disable IRQ
			NVIC_DisableIRQ(SPI0_IRQn);												// Disable SPI0 interrupt
			return((void *) LPC_SPI0);												// return address of periphery
		}
		case 1:
		{
			// Power:
			// Nothing to power On/Off
			
			// Clock:
			LPC_SYSCON->SYSAHBCLKCTRL1 |= (1 << 10); 								// enable SPI1 clock
			
			// Reset:
			LPC_SYSCON->PRESETCTRL1 |= (1 << 10);									// generate reset
			LPC_SYSCON->PRESETCTRL1 &= ~(1 << 10);									// release reset

			// Clear IRQ:
			NVIC_ClearPendingIRQ(SPI1_IRQn);										// clear pending interrupt flag
	
			// Disable IRQ
			NVIC_DisableIRQ(SPI1_IRQn);												// Disable SPI0 interrupt
			return((void *) LPC_SPI1);												// return address of periphery
		}
		default: return(NULL);
	}
}

// ------------------------------------------------------------------------------------------------------
// SPI Enable/disable
// result is true:successfull otherwise false
bool _Chip_SPI_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	uint8_t PeriIndex = _Chip_SPI_Get_PeriIndex(pPeri);								// return peripherial index
	IRQn_Type PeriIRQ;
	
	if(PeriIndex > 1) return(false);												// periferia neexistuje

	switch(_Chip_SPI_Get_PeriIndex(pPeri))
	{
		case 1: PeriIRQ = SPI1_IRQn;												// select apropriate interrupt number
				break;
		case 0: PeriIRQ = SPI0_IRQn;												// select apropriate interrupt number
				break;
		default: return(false);
	}

	
	if(NewState == true) 
	{
		NVIC_ClearPendingIRQ(PeriIRQ);												// clear pending interrupt flag
		((_CHIP_SPI_T *)pPeri)->CFG |= (1 << 0);									// SPI Enable
	}
	else
	{		
		((_CHIP_SPI_T *)pPeri)->CFG &= ~(1 << 0);									// Disable
	}
	
	return (true);
}

// ------------------------------------------------------------------------------------------------
// SPI - Set clock divisor
void _Chip_SPI_Set_ClockDiv(void *pPeri, uint32_t clkdiv)
{
	if ((clkdiv >= 1) && (clkdiv <= 0x10000)) ((_CHIP_SPI_T *)pPeri)->DIV = clkdiv - 1;				// write to CLK DIV register
	else ((_CHIP_SPI_T *)pPeri)->DIV = 0;
}

// ------------------------------------------------------------------------------------------------
// SPI Set interface speed in Hz
// result is true:successfull otherwise false
bool _Chip_SPI_Set_BusSpeed(void *pPeri, uint32_t Speed_Hz)
{
	uint32_t CLKDiv;
	
	CLKDiv = (_Chip_Clock_Get_SysClk_Rate() / Speed_Hz) + 1;						// +1: speed maximal to Speed_Hz or LOWer!
	if(CLKDiv > 0x10000) {sys_Err("Cannot set SPI Divider!"); return(false);} 
	_Chip_SPI_Set_ClockDiv((_CHIP_SPI_T*) pPeri, CLKDiv);							// set new peripherial divider
	return(true);
}

// ------------------------------------------------------------------------------------------------
// SPI Get SPI interface speed in kHz
uint32_t _Chip_SPI_Get_BusSpeed(void *pPeri)										// Return Current bus speed
{
	return((_Chip_Clock_Get_SysClk_Rate() / 1000) / ((_CHIP_SPI_T *)pPeri)->DIV + 1); 				// return in kHz
}

// ------------------------------------------------------------------------------------------------------
// SPI Configure
// result is true:successfull otherwise false
bool _Chip_SPI_Configure(void *pPeri, uint32_t Speed_Hz, bool MasterMode, bool SlaveMode, bool LSB_First, bool CPHA, bool CPOL)
{
	SYS_ASSERT(pPeri != NULL);
	bool res = false;
	
	if(pPeri == NULL) return(false);
	if((MasterMode == true) && (SlaveMode == true)) return(false);					// only one mode set check....
	if((MasterMode == false) && (SlaveMode == false)) return(false);
	
	((_CHIP_SPI_T*)pPeri)->CFG = 0;													// clear old config
	
	if(CPHA == true) ((_CHIP_SPI_T*) pPeri)->CFG |= 1 << 4;							// set CPHA 1
	if(CPOL == true) ((_CHIP_SPI_T*) pPeri)->CFG |= 1 << 5;							// set CPOL 1
	
	res = _Chip_SPI_Set_BusSpeed (pPeri, Speed_Hz);									// set speed
	if (MasterMode == true)
	{
		((_CHIP_SPI_T*) pPeri)->CFG |= (1 << 2);									// set MASTER	
	}
	else
	{
		((_CHIP_SPI_T*) pPeri)->CFG &= ~(1 << 2);									// clear MASTER
	}
	
	if(LSB_First) ((_CHIP_SPI_T*) pPeri)->CFG |= (1 << 3);							// set LSBF
	else ((_CHIP_SPI_T*) pPeri)->CFG &= ~(1 << 3);									// clear LSBF
	
	((_CHIP_SPI_T*) pPeri)->DLY = 0x00002222;										// add a minimal time to delay registers 
	
	return(res);																	// set speed

}

// ------------------------------------------------------------------------------------------------
// enable/disable interrupt generation from selectet SPI
bool _Chip_SPI_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	bool res = false;
	IRQn_Type PeriIRQ;

	switch(_Chip_SPI_Get_PeriIndex(pPeri))
	{
		case 1: 
			{
				PeriIRQ = SPI1_IRQn;												// select apropriate interrupt number
				res = true;
				break;
			}
		case 0: 
			{
				PeriIRQ = SPI0_IRQn;												// select apropriate interrupt number
				res = true;					
				break;
			}				
		default:res = false;														// Bad input
	}	
	
	if((NewState == true) && (res == true))
	{
		NVIC_ClearPendingIRQ ( (IRQn_Type) (PeriIRQ));								// Clear SPIx interrupt flag
		NVIC_EnableIRQ( (IRQn_Type) (PeriIRQ));										// Enable SPIx interrupt
	}
	else
	{
		NVIC_DisableIRQ( (IRQn_Type) (PeriIRQ));									// Disable SPIx interrupt
		((_CHIP_SPI_T *)pPeri)->TXCTL = 0;											// clear Controls
	}
	
	return(res);
}

// ------------------------------------------------------------------------------------------------
// SPI - write data into SPI datareg
inline void _Chip_SPI_Put_Data(void *pPeri, uint32_t NewValue, uint8_t NumOfBits, uint32_t CtrlFlags, uint8_t SSel)
{
	SYS_ASSERT(pPeri != NULL);
//	SSel = SSel & 0x0f;																// maximum SSEL
//	((_CHIP_SPI_T *)pPeri)->TXCTL |= (0x000f0000);									// clear SSEL
//	((_CHIP_SPI_T *)pPeri)->TXCTL &= ~(0x0f000000);									// clear LEN
	
	// SSEL are inverted:
	((_CHIP_SPI_T *)pPeri)->TXCTL = ((~SSel & 0x0f) << 16) | ((NumOfBits & 0x0f) << 24) | (CtrlFlags << 20);
	((_CHIP_SPI_T *)pPeri)->TXDAT = (uint32_t) NewValue;
}

// ------------------------------------------------------------------------------------------------
// SPI - Read Data from datareg
inline uint32_t _Chip_SPI_Get_Data(void *pPeri)
{
	SYS_ASSERT(pPeri != NULL);
	return(((_CHIP_SPI_T *)pPeri)->RXDAT & 0xffff);
}

// ------------------------------------------------------------------------------------------------
// Read SPI interrupt status
inline uint32_t _Chip_SPI_Get_IRQ_Status(void *pPeri)
{
	SYS_ASSERT(pPeri != NULL);
	return(((_CHIP_SPI_T *)pPeri)->INTSTAT);
}

// ------------------------------------------------------------------------------------------------
// Clear SPI interrupt status
inline bool _Chip_SPI_Clear_IRQ_Status(void *pPeri, uint32_t NewValue)
{
	SYS_ASSERT(pPeri != NULL);
	((_CHIP_SPI_T *)pPeri)->INTENCLR |= NewValue;
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Read SPI status
inline uint32_t _Chip_SPI_Get_Peri_Status(void *pPeri)
{
	return(((_CHIP_SPI_T*)pPeri)->STAT);
}

// ------------------------------------------------------------------------------------------------
// clear peri status
bool _Chip_SPI_Clear_Peri_Status(void *pPeri, uint32_t BitMask)
{
	((_CHIP_SPI_T*)pPeri)->STAT |= BitMask;
	return true;
}


// ------------------------------------------------------------------------------------------------
// Enable/disable selected interrupts
inline bool _Chip_SPI_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_SPI_T*)pPeri)->INTENSET = IRQBitMask;
	else ((_CHIP_SPI_T*)pPeri)->INTENCLR = IRQBitMask;
	
	return(true);

}

// ------------------------------------------------------------------------------------------------
// SPI - Create End Of Transfer condition 
inline void _Chip_SPI_SendEOT(void *pPeri)
{
	SYS_ASSERT(pPeri != NULL);
	((_CHIP_SPI_T *)pPeri)->TXCTL |= (1 << 20);	
}

// ------------------------------------------------------------------------------------------------
// SPI - Create End Of Frame condition 
inline void _Chip_SPI_SendEOF(void *pPeri)
{
	SYS_ASSERT(pPeri != NULL);
	((_CHIP_SPI_T *)pPeri)->TXCTL |= (1 << 21);
}

// ------------------------------------------------------------------------------------------------
// SPI - Create Receive Ignore condition 
inline void _Chip_SPI_ReceiveIgnore(void *pPeri)
{
	SYS_ASSERT(pPeri != NULL);
	((_CHIP_SPI_T *)pPeri)->TXCTL |= (1 << 22);
}

// ------------------------------------------------------------------------------------------------
// Flush SPI Tx
inline void _Chip_SPI_Flush_Tx(void *pPeri)
{
																					// Not yet implemented	
}

// ------------------------------------------------------------------------------------------------
// Flush SPI Rx
inline void _Chip_SPI_Flush_Rx(void *pPeri)
{
																					// Not yet implemented	
}





// ------------------------------------------------------------------------------------------------
// USB Functions 
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------
// USB(Num) Initialization
// result is true:successfull otherwise false
void *_Chip_USB_Init(int32_t PeriIndex)
{
	// Check:
	if(PeriIndex != 0) return( NULL );												// LPC15xx have only oone USB
	
	// Power:
	LPC_SYSCON->PDRUNCFG &= ~(1 << 24);												// Enable system clock for USB PLL
	LPC_SYSCON->PDRUNCFG &= ~(1 << 9);												// Enable system clock for USB PHY
	
	// Clock:
	LPC_SYSCON->SYSAHBCLKCTRL1 |= (1 << 23);										// Enable system clock for USB
	
	// Reset:
	LPC_SYSCON->PRESETCTRL1 |= (1 << 23);__nop();									// Activate reset USB
	LPC_SYSCON->PRESETCTRL1 &= ~(1 << 23);											// Deativate reset USB
	
	// Clear IRQ:
	NVIC_ClearPendingIRQ(USB_IRQ_IRQn);												// clear pending interrupt flag

	// Disable IRQ:
	NVIC_DisableIRQ(USB_IRQ_IRQn);													// Enable interrupt flag

	// prepis z drv_USBD.c
	return (LPC_USB);
}	
	
// ------------------------------------------------------------------------------------------------------
// USB Enable/disable
// result is true:successfull otherwise false
bool _Chip_USB_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	
	if(NewState)
	{
		NVIC_ClearPendingIRQ(USB_IRQ_IRQn);											// clear pending interrupt flag
		NVIC_EnableIRQ((IRQn_Type) (USB_IRQ_IRQn));									// Enable USB interrupt
	}
	else
	{
		NVIC_DisableIRQ((IRQn_Type) (USB_IRQ_IRQn));								// Disable USB interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Configure USB
// result is true:successfull otherwise false
bool _Chip_USB_Configure(void *pPeri, bool DeviceMode, bool HostMode, bool OTGMode)
{
	// prepis z drv_USBD.c
	
//	LPC_SYSCON->SYSAHBCLKCTRL1 |= (0x01 < 23);										// Enable system clock for USB

//	LPC_SYSCON->PRESETCTRL1 |= (0x01 << 23);__nop();								// Activate reset
//	LPC_SYSCON->PRESETCTRL1 &= ~(0x01 << 23);										// Deactivate reset

	return(true);
}





// ------------------------------------------------------------------------------------------------
// ADC Functions
// ------------------------------------------------------------------------------------------------
bool _Chip_ADC_DMA_Mode;
bool _Chip_ADC_Repeat_Mode;
bool _Chip_ADC_Temp_Sensor;


// ------------------------------------------------------------------------------------------------
// return ptr to periphary
int32_t _Chip_ADC_Get_PeriIndex(void *pPeri)
{
	if(((_CHIP_ADC_T*) pPeri) == LPC_ADC1) return(1);
	if(((_CHIP_ADC_T*) pPeri) == LPC_ADC0) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------------
// ADC(Num) Initialization (PowerUp, Enable CLK, Init)
// result is true:successfull otherwise false
void *_Chip_ADC_Init(int32_t PeriIndex)
{
	// Check:
	SYS_ASSERT((PeriIndex < _CHIP_ADC_COUNT) && (PeriIndex >= 0));
	
	switch(PeriIndex)
	{
		case 1:
		{
			// Power:
			// Nothing to power On/Off
			
			// Clock:
			LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 28);								// enable ADC1 clock

			// Reset:
			LPC_SYSCON->PRESETCTRL0 |= (1 << 28);__nop();							// Activate reset
			LPC_SYSCON->PRESETCTRL0 &= ~(1 << 28);									// Deactivate reset
			
			// Clear IRQ:
			NVIC_ClearPendingIRQ(ADC1_SEQA_IRQn);									// clear pending interrupt flag
			NVIC_ClearPendingIRQ(ADC1_SEQB_IRQn);									// clear pending interrupt flag
			NVIC_ClearPendingIRQ(ADC1_THCMP_IRQn);									// clear pending interrupt flag
			NVIC_ClearPendingIRQ(ADC1_OVR_IRQn);									// clear pending interrupt flag

			// Disable IRQ:
			NVIC_DisableIRQ((IRQn_Type) (ADC1_SEQA_IRQn));							// Enable ADC 1 - Sequencer A interrupt
			NVIC_DisableIRQ((IRQn_Type) (ADC1_SEQB_IRQn));							// Enable ADC 1 - Sequencer B interrupt
			NVIC_DisableIRQ((IRQn_Type) (ADC1_THCMP_IRQn));							// Enable ADC 1 - Threshold-Compare Out-of-Range Interrupt
			NVIC_DisableIRQ((IRQn_Type) (ADC1_OVR_IRQn));							// Enable ADC 1 - Data Overrun Interrupt
			LPC_ADC1->SEQ_CTRL[0] &= ~(1UL << 31);									// disable sequencer A
			LPC_ADC1->SEQ_CTRL[1] &= ~(1UL << 31);									// disable sequencer B
			return(LPC_ADC1);
		}
		case 0:
		{
			// Power:
			// Nothing to power On/Off
			
			// Clock:
			LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 27);								// enable ADC0 clock

			// Reset:
			LPC_SYSCON->PRESETCTRL0 |= (1 << 27);__nop();							// Activate reset
			LPC_SYSCON->PRESETCTRL0 &= ~(1 << 27);									// Deactivate reset
			
			// Clear IRQ:
			NVIC_ClearPendingIRQ(ADC0_SEQA_IRQn);									// clear pending interrupt flag
			NVIC_ClearPendingIRQ(ADC0_SEQB_IRQn);									// clear pending interrupt flag
			NVIC_ClearPendingIRQ(ADC0_THCMP_IRQn);									// clear pending interrupt flag
			NVIC_ClearPendingIRQ(ADC0_OVR_IRQn);									// clear pending interrupt flag
			
			// Enable IRQ:
			NVIC_DisableIRQ((IRQn_Type) (ADC0_SEQA_IRQn));							// Enable ADC 0 - Sequencer A interrupt
			NVIC_DisableIRQ((IRQn_Type) (ADC0_SEQB_IRQn));							// Enable ADC 0 - Sequencer B interrupt
			NVIC_DisableIRQ((IRQn_Type) (ADC0_THCMP_IRQn));							// Enable ADC 0 - Threshold-Compare Out-of-Range Interrupt
			NVIC_DisableIRQ((IRQn_Type) (ADC0_OVR_IRQn));							// Enable ADC 0 - Data Overrun Interrupt
			LPC_ADC0->SEQ_CTRL[0] &= ~(1UL << 31);									// disable sequencer A
			LPC_ADC0->SEQ_CTRL[1] &= ~(1UL << 31);									// disable sequencer B			
			return(LPC_ADC0);
		}
		default: return(NULL);
	}
}


// ------------------------------------------------------------------------------------------------------
// Read conversion result validity flag
bool _Chip_ADC_Get_DataValidFlag_CH( void *pPeri, uint8_t Channel)
{
	return((((_CHIP_ADC_T*) pPeri)->DR[Channel] & ADC_DAT_DATAVALID_MASK) >> ADC_DAT_DATAVALID_SHIFT);
}

// ------------------------------------------------------------------------------------------------------
// Read conversion result on seleected channel
uint32_t _Chip_ADC_Get_Data_CH( void *pPeri, uint8_t Channel)
{
	return((((_CHIP_ADC_T*) pPeri)->DR[Channel] & ADC_DAT_RESULT_MASK) >> ADC_DAT_RESULT_SHIFT);
}


// ------------------------------------------------------------------------------------------------------
// Read active channel on ADC
// read from SWM configuration for each ADC Channel
uint32_t _Chip_ADC_Get_ChannelMask(void *pPeri)
{
	SYS_ASSERT(pPeri != NULL);														// check
	
	uint32_t res = 0;
	uint8_t PeriIndex = _Chip_ADC_Get_PeriIndex(pPeri);								// return peripherial index
	uint32_t SWM_Old_Clk_State = LPC_SYSCON->SYSAHBCLKCTRL0 & (1 << 12);			//  read current SWM clock
		
	LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 12);										// Enable SWM

	switch(PeriIndex)
	{
		case 1:
		{
			for(uint8_t bit=13 ; bit<=23 ; bit++ )									// depend on each MCU!
			{
				if(_Chip_SWM_IsFixedPinEnabled((CHIP_SWM_PIN_FIXED_T) bit) == true )	// fixed pin function is enabled
				{
					res |= (1 << (bit - 13));
				}
			}
		}
			
		default:			// ADC = 0
		{
			for(uint8_t bit=0 ; bit<=11 ; bit++ )									// depend on each MCU!
			{
				if(_Chip_SWM_IsFixedPinEnabled((CHIP_SWM_PIN_FIXED_T)bit) == true )	// fixed pin function is enabled
				{
					res |= 1 << bit;
				}
			}
		}
	}
	
	if(SWM_Old_Clk_State == 0) 														// CLK was disabled?
			LPC_SYSCON->SYSAHBCLKCTRL0 &= ~(1 << 12);										// Disable system clock for SWM
	return(res);
}

// ------------------------------------------------------------------------------------------------
// ADC Interrupt - enable/disable
bool _Chip_ADC_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);														// check

	uint8_t PeriIndex = _Chip_ADC_Get_PeriIndex(pPeri);								// return peripherial index
	
	switch(PeriIndex)
	{
		case 0:
		{
			if(NewState == true)
			{
				NVIC_ClearPendingIRQ(ADC0_SEQA_IRQn);										// clear pending NVIC interrupt flag
				NVIC_ClearPendingIRQ(ADC0_SEQB_IRQn);										// clear pending NVIC interrupt flag
				NVIC_ClearPendingIRQ(ADC0_THCMP_IRQn);										// clear pending NVIC interrupt flag
				
				// Enable IRQ:
				NVIC_EnableIRQ(ADC0_SEQA_IRQn);												// Enable NVIC interrupt
				NVIC_EnableIRQ(ADC0_SEQB_IRQn);												// Enable NVIC interrupt
				NVIC_EnableIRQ(ADC0_THCMP_IRQn);											// Enable NVIC interrupt		
			}
			else
			{
				NVIC_DisableIRQ(ADC0_SEQA_IRQn);											// Disable NVIC interrupt
				NVIC_DisableIRQ(ADC0_SEQB_IRQn);											// Disable NVIC interrupt
				NVIC_DisableIRQ(ADC0_THCMP_IRQn);											// Disable NVIC interrupt
			}
			return(true);
		}
		case 1:
		{		
			if(NewState == true)
			{
				NVIC_ClearPendingIRQ(ADC1_SEQA_IRQn);										// clear pending NVIC interrupt flag
				NVIC_ClearPendingIRQ(ADC1_SEQB_IRQn);										// clear pending NVIC interrupt flag
				NVIC_ClearPendingIRQ(ADC1_THCMP_IRQn);										// clear pending NVIC interrupt flag
				
				// Enable IRQ:
				NVIC_EnableIRQ(ADC1_SEQA_IRQn);												// Enable NVIC interrupt
				NVIC_EnableIRQ(ADC1_SEQB_IRQn);												// Enable NVIC interrupt
				NVIC_EnableIRQ(ADC1_THCMP_IRQn);											// Enable NVIC interrupt		
			}
			else
			{
				NVIC_DisableIRQ(ADC1_SEQA_IRQn);											// Disable NVIC interrupt
				NVIC_DisableIRQ(ADC1_SEQB_IRQn);											// Disable NVIC interrupt
				NVIC_DisableIRQ(ADC1_THCMP_IRQn);											// Disable NVIC interrupt
			}			
			return(true);
		}
		default: return(false);
	}
}

// ------------------------------------------------------------------------------------------------
// Power down / up
inline bool _Chip_ADC_Power(void *pPeri, bool NewState)
{
	int32_t PeriIndex = _Chip_ADC_Get_PeriIndex(pPeri);
	
	if((PeriIndex > 1) || (PeriIndex < 0)) return(false);
	if(NewState) LPC_SYSCON->PDRUNCFG &= ~(1 << (10 + PeriIndex));					// Enable Power for periphery
	else  LPC_SYSCON->PDRUNCFG |= (1 << (10 + PeriIndex));							// Disable Power for periphery
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Power down / up - temperature sensor
inline bool _Chip_TS_Power(bool NewState)
{
	if(NewState) LPC_SYSCON->PDRUNCFG &= ~(1 << 18);								// Enable Power for periphery
	else  LPC_SYSCON->PDRUNCFG |= (1 << 18);										// Disable Power for periphery
	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// Power down / up - internal voltage
inline bool _Chip_IREF_Power(bool NewState)
{
	if(NewState) LPC_SYSCON->PDRUNCFG &= ~(1 << 17);								// Enable Power for periphery
	else  LPC_SYSCON->PDRUNCFG |= (1 << 17);										// Disable Power for periphery
	
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Enable/Disable ADC
// if Channel is UINT32_MAX, enable while periphery without channels, otherwise ena/disa selected channel
// result is true:successfull otherwise false
bool _Chip_ADC_Enable_CH(void* pPeri, uint32_t ChannelBitMask, bool NewState)
{
	if(pPeri == NULL ) return(false);
	uint8_t PeriIndex = _Chip_ADC_Get_PeriIndex(pPeri);								// return peripherial index

	switch(PeriIndex)
	{
		case 0:
		{
			if(NewState == true)
			{
				NVIC_ClearPendingIRQ(ADC0_SEQA_IRQn);										// clear pending interrupt flag
				NVIC_ClearPendingIRQ(ADC0_SEQB_IRQn);										// clear pending interrupt flag
				NVIC_ClearPendingIRQ(ADC0_THCMP_IRQn);										// clear pending interrupt flag
				NVIC_ClearPendingIRQ(ADC0_OVR_IRQn);										// clear pending interrupt flag
				
				((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[ADC_SEQA_IDX] |= ADC_SEQ_CTRL_SEQ_ENA | ADC_SEQ_CTRL_START;	// Enable and Start Sequencer A
				((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[ADC_SEQB_IDX] |= ADC_SEQ_CTRL_SEQ_ENA | ADC_SEQ_CTRL_START;	// Enable and Start Sequencer B
				_Chip_ADC_Power(pPeri, true);												// Power Up
			}
			else
			{
				((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[ADC_SEQA_IDX] &= ~ADC_SEQ_CTRL_SEQ_ENA;	// Disable Sequencer A
				((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[ADC_SEQB_IDX] &= ~ADC_SEQ_CTRL_SEQ_ENA;	// Disable Sequencer B				
				_Chip_ADC_Power(pPeri, false);												// Power Down
			}
			return (true);
		}
		case 1:
		{
			if(NewState == true)
			{
				NVIC_ClearPendingIRQ(ADC1_SEQA_IRQn);										// clear pending interrupt flag
				NVIC_ClearPendingIRQ(ADC1_SEQB_IRQn);										// clear pending interrupt flag
				NVIC_ClearPendingIRQ(ADC1_THCMP_IRQn);										// clear pending interrupt flag
				NVIC_ClearPendingIRQ(ADC1_OVR_IRQn);										// clear pending interrupt flag
				
				((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[ADC_SEQA_IDX] |= ADC_SEQ_CTRL_SEQ_ENA | ADC_SEQ_CTRL_START;	// Enable and Start Sequencer A
				((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[ADC_SEQB_IDX] |= ADC_SEQ_CTRL_SEQ_ENA | ADC_SEQ_CTRL_START;	// Enable and Start Sequencer B
				_Chip_ADC_Power(pPeri, true);												// Power Up
			}
			else
			{
				((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[ADC_SEQA_IDX] &= ~ADC_SEQ_CTRL_SEQ_ENA;	// Disable Sequencer A
				((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[ADC_SEQB_IDX] &= ~ADC_SEQ_CTRL_SEQ_ENA;	// Disable Sequencer B				
				_Chip_ADC_Power(pPeri, false);												// Power Down
			}
			return (true);
		}
		default: return(false);
	}
}

// ------------------------------------------------------------------------------------------------------
// ADC[x](ChannelBitMask) Configure whole ADC
// result is true:successfull otherwise false
bool _Chip_ADC_Configure_CH (void *pPeri, uint32_t ChannelBitMask, uint32_t Speed_kHz, bool RepeatMode, bool DMAMode)
{
	int8_t i = _CHIP_ADC_CHANNELS_COUNT-1;
	uint32_t clksrc_rate;
	
	SYS_ASSERT(pPeri != NULL);														// check
	SYS_ASSERT(ChannelBitMask > 0);													// check

	
	((_CHIP_ADC_T *)pPeri)->INTEN = 0;												// diasable all interrupt requests
	((_CHIP_ADC_T *)pPeri)->CTRL = 0;												// Clear control  and stop ADC

	// Autocalibration:
	((_CHIP_ADC_T *)pPeri)->CTRL = (1 << 30);										// Set CALMODE bit
	((_CHIP_ADC_T *)pPeri)->CTRL |= ADC_CR_CLKDIV(0xff);							// up to min. conversion speed
	{}while(((_CHIP_ADC_T *)pPeri)->CTRL == (1 << 30));								// Wait for CALMODE bit is set

	// next configure ADC
	((_CHIP_ADC_T *)pPeri)->CTRL = 0;												// Clear control  and stop ADC
	((_CHIP_ADC_T *)pPeri)->SEQ_CTRL[0] = (ChannelBitMask & 0x0fff);				// enable channels

	if(((_CHIP_ADC_T *)pPeri)->CTRL & (1 << 8)) clksrc_rate = _Chip_Clock_Get_ADCClk_Rate(); // if ASYNC mode selected, reat input freq
	else clksrc_rate = _Chip_Clock_Get_SysClk_Rate();								// else system_clock as source
		
	((_CHIP_ADC_T *)pPeri)->CTRL |= ADC_CR_CLKDIV((clksrc_rate / (Speed_kHz*1000)) & 0xff); // AD CLK must be lower or equal to 12.4 MHz. Src is PCLK.

	((_CHIP_ADC_T *)pPeri)->CTRL |= ADC_SEQ_CTRL_MODE_EOS;							// end of sequence result
	((_CHIP_ADC_T *)pPeri)->CTRL |= ADC_SEQ_CTRL_HWTRIG_POLPOS | ADC0_SEQ_CTRL_HWTRIG_SCT2_OUT3;		// ADC startuje SCT2 Out 3 na nabeznu hranu HW Trig.
	((_CHIP_ADC_T *)pPeri)->CTRL |= ADC_SEQ_CTRL_SEQ_ENA;							// enable sequencer A of ADC0
		
	if(DMAMode) 																	// in DMA mode, set IRQ from last active channel, and global IRQ Disable
	{
		_Chip_ADC_DMA_Mode = true;													// never enable global IRQ !!!
		do
		{
			if(ChannelBitMask & (1 << i)) {((_CHIP_ADC_T *)pPeri)->INTEN |= (1<<i); break;} // find biggest ective channel and enable IRQ
		}while(i--);
	}
	else 
	{
		_Chip_ADC_DMA_Mode = false;
		((_CHIP_ADC_T *)pPeri)->INTEN |= ChannelBitMask;							// Enable IRQ for each channel
	}
	
	if(RepeatMode) 
	{
		_Chip_ADC_Repeat_Mode = true;
		((_CHIP_ADC_T *)pPeri)->CTRL &= ~(1 << 26);									// disable SW and HW Start
		((_CHIP_ADC_T *)pPeri)->CTRL |= (1 << 27);									// Set burst mode, otherwise single mode
	}
	
	
	return (true);
}

// ------------------------------------------------------------------------------------------------
// Get interrupt flags
inline uint32_t _Chip_ADC_Get_IRQ_Status_CH(_CHIP_ADC_T *pADC, uint32_t ChannelBitMask)
{
	return pADC->FLAGS & ChannelBitMask;
}


// ------------------------------------------------------------------------------------------------
// clear interrupt flags
inline bool _Chip_ADC_Clear_IRQ_Status_CH(_CHIP_ADC_T *pADC, uint32_t ChannelBitMask)
{
	pADC->FLAGS = ChannelBitMask;													// nothing to clear
	return(true);	
}

// ------------------------------------------------------------------------------------------------
// ADC Conversion start
inline void _Chip_ADC_Start_CH(void *pPeri, uint32_t ChannelBitMask)
{
	((_CHIP_ADC_T*)pPeri)->CTRL |= (1 << 24);										// start immediatelly
}


// ------------------------------------------------------------------------------------------------
// ADC channel interrupt enable/disable for selected channel
// result is true:successfull otherwise false
inline bool _Chip_ADC_IRQ_Peri_Enable_CH(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	if(NewState) ((_CHIP_ADC_T*)pPeri)->INTEN |= IRQBitMask;						// set bitmask
	else ((_CHIP_ADC_T*)pPeri)->INTEN &= ~IRQBitMask;								// clear bitmask
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// Enable/Disable on-chip temperature sensor on ADC0.CH0
// result is true:successfull otherwise false
bool _Chip_Temp_Enable(bool NewState)
{
	if(NewState == true)
	{
		_Chip_IREF_Power(true);														// enable power to internal reference
		_Chip_TS_Power(true);														// enable power to temperature sensor
		LPC_ADC0->INSEL |= 0x03;													// enable temperature sensor connect to ADC0.CH0
	}
	else
	{
		_Chip_IREF_Power(false);													// disable power to internal reference
		_Chip_TS_Power(false);														// disable power to temperature sensor
		LPC_ADC0->INSEL &= 0x0f;													// enable ext. channel 0 on ADMUX
	}
	return(true);
}








// ******************************************************************************************************
// DAC
// ******************************************************************************************************
// LPC15xx has one 12-bit DAC

// ------------------------------------------------------------------------------------------------
// return ptr to periphary
inline int32_t _Chip_DAC_Get_PeriIndex(void *pPeri)
{
	if((_CHIP_DAC_T*) pPeri == LPC_DAC) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------
// Initialize DAC
void *_Chip_DAC_Init(int32_t PeriIndex)
{
	void *ResPtr = NULL;
	
	// Check:	
	SYS_ASSERT((PeriIndex < _CHIP_DAC_COUNT) && (PeriIndex >= 0));
	if((PeriIndex < 0) || (PeriIndex >= _CHIP_DAC_COUNT)) return(false);			// check or available periphery for this MCU
	
	// Power:
	LPC_SYSCON->PDRUNCFG &= ~(1 << 12);													// Enable system clock for DAC0
	// Clock:
	LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 29);											// Enable system clock for DAC0
	// Reset:
	// nothing	
	// Feedback?
	// nothing
	NVIC_ClearPendingIRQ(DAC_IRQn);													// clear pending interrupt flag
	
	NVIC_DisableIRQ((IRQn_Type) (DAC_IRQn));											// Enable interrupt
	ResPtr = (void *) LPC_DAC;														// return address of periphery
	
	return(ResPtr);																	// return pointer to perihery
}

// ------------------------------------------------------------------------------------------------------
// DAC Enable/Disable
inline bool _Chip_DAC_Enable(void *pPeri, bool NewState)
{
	return(false);
}

// ------------------------------------------------------------------------------------------------------
// DAC Enable/Disable IRQ
inline bool _Chip_DAC_Enable_IRQ(void *pPeri, uint32_t IRQBitMask, bool NewState)
{
	return(false);
}

// ------------------------------------------------------------------------------------------------
// DAC NVIC Interrupt - enable/disable
bool _Chip_DAC_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	NVIC_ClearPendingIRQ(DAC_IRQn);												// clear pending interrupt flag
	if(NewState == true) NVIC_EnableIRQ(DAC_IRQn);								// Enable interrupt
	else NVIC_DisableIRQ(DAC_IRQn);												// Disable interrupt
	return(true);
}

// ------------------------------------------------------------------------------------------------
// DAC - write data into DAC
extern inline void _Chip_DAC_Put_Data(void *pPeri, uint32_t NewValue)
{
	
}

// ------------------------------------------------------------------------------------------------
// Read DAC status
inline uint32_t _Chip_DAC_Get_Peri_Status(void *pPeri)
{
	return(0);
}

// ------------------------------------------------------------------------------------------------
// Clear DAC status - clear by disable and reenable selected interrupts.
inline void _Chip_DAC_Clear_Peri_Status(void *pPeri, uint32_t IRQBitMask)
{

}

// ------------------------------------------------------------------------------------------------
// Read DAC interrupt status
inline uint32_t _Chip_DAC_Get_IRQ_Status(void *pPeri)
{
	return(0);
}

// ------------------------------------------------------------------------------------------------
// Clear DAC interrupt status
inline void _Chip_DAC_Clear_IRQ_Status(void *pPeri, uint32_t IRQBitMask)
{

}

// ------------------------------------------------------------------------------------------------
// DAC Configuration
inline bool _Chip_DAC_Configure(void *pPeri)
{
	return(true);
}

//// ------------------------------------------------------------------------------------------------
//// Power down / up
//inline bool _Chip_DAC_Power(void *pPeri, bool NewState)
//{
//	if(NewState) LPC_SYSCON->PDRUNCFG &= ~(1 << 12);								// Enable Power for periphery
//	else  LPC_SYSCON->PDRUNCFG |= (1 << 12);										// Disable Power for periphery
//	
//	return(true);
//}







// ******************************************************************************************************
// TIMER Functions - Timerx Or MRTx
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------
// return index to periphery Timer(pointer to peri)
inline int32_t _Chip_MRT_Get_PeriIndex(void *pPeri)
{
	if((_CHIP_MRT_T*) pPeri == LPC_MRT) return(0);
	return(-1);																		// otherwise error
}	

// ------------------------------------------------------------------------------------------------------
// MRT(Num) Initialization
// result is true:successfull otherwise false
// Index is 0 for LPC15xx, Channels are 0-3
void *_Chip_MRT_Init(int32_t PeriIndex)
{
	// Check:
	if(PeriIndex != 0) return(NULL);

	// Power:
	// Nothing to power On/Off
	
	// Clock:
	LPC_SYSCON->SYSAHBCLKCTRL1 |= (1 << 0);											// Enable system clock for MRT
	
	// Reset:
	LPC_SYSCON->PRESETCTRL1 |= (1 << 0);__nop();									// Activate reset
	LPC_SYSCON->PRESETCTRL1 &= ~(1 << 0);											// Deactivate reset
	
	// Clear IRQ:
	NVIC_ClearPendingIRQ(MRT_IRQn);												// clear pending interrupt flag

	// Disable IRQ:
	NVIC_DisableIRQ(MRT_IRQn);														// Enable interrupt

	return(LPC_MRT);																// nothing to check. return Seccess
}

// ------------------------------------------------------------------------------------------------------
// MRT(Num) Enable/disable
// result is true:successfull otherwise false
// if ChannelBitMask is set, selected channel are enabled/disabled
// 		otherwise whole timer as periphery is switched
bool _Chip_MRT_Enable_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res = false;
	
	if(pPeri == NULL ) return(false);
	NVIC_ClearPendingIRQ(MRT_IRQn);													// clear pending interrupt flag
	
	res = _Chip_MRT_Enable_IRQ_CH(pPeri, ChannelBitMask, NewState);
	_Chip_MRT_Reset_Tmr_CH(pPeri, ChannelBitMask);									// reload value and start counting

//	if(NewState)	
//	{
//		if(ChannelBitMask)
//		{
//			if(ChannelBitMask && 0x01){((_CHIP_MRT_T*)pPeri)->TMR[0].CTRL |= 1;}	// enable timer channel 0 interrupt
//			if(ChannelBitMask && 0x02){((_CHIP_MRT_T*)pPeri)->TMR[1].CTRL |= 1;}	// enable timer channel 1 interrupt
//			if(ChannelBitMask && 0x04){((_CHIP_MRT_T*)pPeri)->TMR[2].CTRL |= 1;}	// enable timer channel 2 interrupt
//			if(ChannelBitMask && 0x08){((_CHIP_MRT_T*)pPeri)->TMR[3].CTRL |= 1;}	// enable channel timer 3 interrupt
//		}
//		NVIC_EnableIRQ((IRQn_Type) (MRT_IRQn));										// Enable whole Timer interrupt
//	}
//	else
//	{
//		if(ChannelBitMask)
//		{
//			if(ChannelBitMask && 0x01){((_CHIP_MRT_T*)pPeri)->TMR[0].CTRL &= ~1;}	// disable timer channel 0 interrupt
//			if(ChannelBitMask && 0x02){((_CHIP_MRT_T*)pPeri)->TMR[1].CTRL &= ~1;}	// disable timer channel 1 interrupt
//			if(ChannelBitMask && 0x04){((_CHIP_MRT_T*)pPeri)->TMR[2].CTRL &= ~1;}	// disable timer channel 2 interrupt
//			if(ChannelBitMask && 0x08){((_CHIP_MRT_T*)pPeri)->TMR[3].CTRL &= ~1;}	// disable timer channel timer 3 interrupt
//		}
//		if( (((_CHIP_MRT_T*)pPeri)->TMR[0].CTRL & 0x01) && 	\
//			(((_CHIP_MRT_T*)pPeri)->TMR[1].CTRL & 0x01) &&	\
//			(((_CHIP_MRT_T*)pPeri)->TMR[2].CTRL & 0x01) &&	\
//			(((_CHIP_MRT_T*)pPeri)->TMR[3].CTRL & 0x01) ) __nop();
//		else NVIC_DisableIRQ((IRQn_Type) (MRT_IRQn));								// Disable whole Timer interrupt
//	}
//	res = true;

	return(res);																	// nothing to check. return Seccess
}

// ------------------------------------------------------------------------------------------------
// NVIC Interrupt - enable/disable
bool _Chip_MRT_Enable_IRQ_NVIC(void *pPeri, bool NewState)
{
	SYS_ASSERT(pPeri != NULL);
	
	if(NewState == true)
	{
		NVIC_ClearPendingIRQ (MRT_IRQn);											// Clear NVIC interrupt flag
		NVIC_EnableIRQ(MRT_IRQn);													// Enable NVIC interrupt
	}
	else
	{
		NVIC_DisableIRQ(MRT_IRQn);													// Enable NVIC interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// MRT(Channel) Configure
// Channel 0-3
// if repeat==true, autoreload timer will executed, otherwise oneshot only
// result is true:successfull otherwise false
bool _Chip_MRT_Configure (void *pPeri, uint8_t Channel, uint32_t RateHz, bool RepeatMode)
{
	bool res = false;
	uint32_t reg;
	
	SYS_ASSERT(pPeri != NULL && (Channel < _CHIP_MRT_CHANNELS_COUNT));

	if (Channel >= _CHIP_MRT_CHANNELS_COUNT) return(false);							// LPC8xx have only channels 0-3
	
	uint32_t tmp = _Chip_Clock_Get_SysClk_Rate();									// Get System Clock
	tmp = tmp / RateHz;
	if(tmp <= 0x00ffffff)															// Cannot set Rate! Out of Range!!
	{
		//tmp = tmp | ((uint32_t) 1 << 31);
		((_CHIP_MRT_T*)pPeri)->TMR[Channel].INTVAL = (tmp - 1); 
	
		reg = ((_CHIP_MRT_T*)pPeri)->TMR[Channel].CTRL & ~0x06;						// read all nor mode
		if(RepeatMode == true) 	((_CHIP_MRT_T*)pPeri)->TMR[Channel].CTRL = reg | (0x00 << 1);	
		else					((_CHIP_MRT_T*)pPeri)->TMR[Channel].CTRL = reg | (0x01 << 1);
		res = _Chip_MRT_Clear_IRQ_Status_CH(pPeri, 1 << Channel);
	}

//	if(res == true) res = _Chip_MRT_Enable(pPeri, 1 << Channel, true);			// enable interrupt for channel
//	else 			res = _Chip_MRT_Enable(pPeri, 1 << Channel, false);			// disable interrupt for channel
	return (res);
}

// ------------------------------------------------------------------------------------------------------
// MRT(Channel) - write reload value
// Channel 0-3
inline void _Chip_MRT_Set_TmrVal(void *pPeri, uint8_t Channel, uint32_t TmrValue) 			// write a new reload value
{
	((_CHIP_MRT_T*)pPeri)->TMR[Channel].INTVAL = (TmrValue | (1UL << 31UL));	// save new value and force reload
}

// ------------------------------------------------------------------------------------------------------
// MRT(Channel) - reset timer counter
// Channel 0-3
inline void _Chip_MRT_Reset_Tmr_CH(void *pPeri, uint32_t ChannelBitMask) 						// reload value
{
	
	if(ChannelBitMask & 0x01){((_CHIP_MRT_T*)pPeri)->TMR[0].INTVAL |= (1UL << 31);}	// Force reload
	if(ChannelBitMask & 0x02){((_CHIP_MRT_T*)pPeri)->TMR[1].INTVAL |= (1UL << 31);}	// Force reload
	if(ChannelBitMask & 0x04){((_CHIP_MRT_T*)pPeri)->TMR[2].INTVAL |= (1UL << 31);}	// Force reload
	if(ChannelBitMask & 0x08){((_CHIP_MRT_T*)pPeri)->TMR[3].INTVAL |= (1UL << 31);}	// Force reload
}

// ------------------------------------------------------------------------------------------------------
// MRT(Channel) - write reload value
// Channel 0-3
inline uint32_t _Chip_MRT_Get_TmrVal(void *pPeri, uint8_t Channel)							// read a reload value
{
	return(((_CHIP_MRT_T*)pPeri)->TMR[Channel].INTVAL);							// return value
}

// ------------------------------------------------------------------------------------------------
// Timer Clear channel interrupt Flag for selected channel
// result is true:successfull otherwise false
inline bool _Chip_MRT_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask)
{
	bool res = false;
	
	if(ChannelBitMask && 0x01){((_CHIP_MRT_T*)pPeri)->TMR[0].STAT |= 1;res = true;}	// Clear Int Stat for channel
	if(ChannelBitMask && 0x02){((_CHIP_MRT_T*)pPeri)->TMR[1].STAT |= 1;res = true;}	// Clear Int Stat for channel
	if(ChannelBitMask && 0x04){((_CHIP_MRT_T*)pPeri)->TMR[2].STAT |= 1;res = true;}	// Clear Int Stat for channel
	if(ChannelBitMask && 0x08){((_CHIP_MRT_T*)pPeri)->TMR[3].STAT |= 1;res = true;}	// Clear Int Stat for channel
	return(res);
}

// ------------------------------------------------------------------------------------------------
// Timer Get channel interrupt Flag for all channels
// result is bit position for active IRQ
inline uint32_t _Chip_MRT_Get_IRQ_Status_CH(void *pPeri)
{
	uint32_t Result = 0;
	if(((_CHIP_MRT_T*)pPeri)->TMR[0].STAT & 0x01) Result |= 0x01;					// read INTFLAG for channel 0
	if(((_CHIP_MRT_T*)pPeri)->TMR[1].STAT & 0x01) Result |= 0x02;					// read INTFLAG for channel 1
	if(((_CHIP_MRT_T*)pPeri)->TMR[2].STAT & 0x01) Result |= 0x04;					// read INTFLAG for channel 2
	if(((_CHIP_MRT_T*)pPeri)->TMR[3].STAT & 0x01) Result |= 0x08;					// read INTFLAG for channel 3
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// Timer channel interrupt enable/disable for selected channel
// result is true:successfull otherwise false
inline bool _Chip_MRT_Enable_IRQ_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res = false;
	if(NewState)																	// Enable channel interrupt
	{
		if(ChannelBitMask && 0x01){((_CHIP_MRT_T*)pPeri)->TMR[0].CTRL |= 1;res = true;}	// enable timer 0 interrupt
		if(ChannelBitMask && 0x02){((_CHIP_MRT_T*)pPeri)->TMR[1].CTRL |= 1;res = true;}	// enable timer 1 interrupt
		if(ChannelBitMask && 0x04){((_CHIP_MRT_T*)pPeri)->TMR[2].CTRL |= 1;res = true;}	// enable timer 2 interrupt
		if(ChannelBitMask && 0x08){((_CHIP_MRT_T*)pPeri)->TMR[3].CTRL |= 1;res = true;}	// enable timer 3 interrupt		
	}
	else 																			// disable channel interrupt
	{
		if(ChannelBitMask && 0x01){((_CHIP_MRT_T*)pPeri)->TMR[0].CTRL &= ~1;res = true;}// disable timer 0 interrupt
		if(ChannelBitMask && 0x02){((_CHIP_MRT_T*)pPeri)->TMR[1].CTRL &= ~1;res = true;}// disable timer 1 interrupt
		if(ChannelBitMask && 0x04){((_CHIP_MRT_T*)pPeri)->TMR[2].CTRL &= ~1;res = true;}// disable timer 2 interrupt
		if(ChannelBitMask && 0x08){((_CHIP_MRT_T*)pPeri)->TMR[3].CTRL &= ~1;res = true;}// disable timer 3 interrupt
	}
	
	return(res);
}














// ******************************************************************************************************
// DMA Functions
// ******************************************************************************************************

// DMA SRAM table - this can be optionally used with the Chip_DMA_SetSRAMBase()
// function if a DMA SRAM table is needed. This table is correctly aligned for
// the DMA controller.
ATTR_ALIGNED(512) _Chip_DMA_CHDesc_t _Chip_DMA_Table[_CHIP_DMA_CHANNELS_COUNT];
uint32_t DMA_CH_XFERLEN[_CHIP_DMA_CHANNELS_COUNT];

// ------------------------------------------------------------------------------------------------
// return pointer to periphery DMA(PeriIndex)
inline int32_t _Chip_DMA_Get_PeriIndex(void *pPeri)
{
	if((_CHIP_DMA_T*) pPeri == LPC_DMA) return(0);
	return(-1);																		// otherwise error
}

// ------------------------------------------------------------------------------------------------------
// DMA(Num) Initialization
// result is true:successfull otherwise false
void *_Chip_DMA_Init(int32_t PeriIndex)
{
	// Check:
	if(PeriIndex != 0) return(false);												// LPC15xx have only one DMA
	
	// Power:
	// Nothing to power On/Off
	
	// Clock:
	LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 20);										// Enable system clock for DMA
	
	// Reset:
	LPC_SYSCON->PRESETCTRL0 |= (1 << 20);__nop();									// Activate reset
	LPC_SYSCON->PRESETCTRL0 &= ~(1 << 20);											// Deactivate reset
	
	// Clear IRQ:
	LPC_DMA->DMACOMMON[0].ENABLECLR |= 0xffffffff;									// disable all channels
	NVIC_ClearPendingIRQ(DMA_IRQn);													// clear pending interrupt flag

	// Disnable IRQ
	NVIC_DisableIRQ((IRQn_Type) (DMA_IRQn));											// Enable DMA interrupt
	
	return((void *) LPC_DMA);														// return
}

// ------------------------------------------------------------------------------------------------------
// DMA(Num) Enable/disable
// result is true:successfull otherwise false
bool _Chip_DMA_Enable_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	if(pPeri == NULL ) return(false);

	if(NewState)
	{
		((_CHIP_DMA_T*)pPeri)->DMACOMMON[0].ENABLESET |= ChannelBitMask;			// enable selected channels
		if(ChannelBitMask == UINT32_MAX) NVIC_EnableIRQ((IRQn_Type) (DMA_IRQn));	// enable IRQ
	}
	else
	{
		((_CHIP_DMA_T*)pPeri)->DMACOMMON[0].ENABLECLR |= ChannelBitMask;			// disable selected channels
		if(ChannelBitMask == UINT32_MAX) NVIC_DisableIRQ((IRQn_Type) (DMA_IRQn));	// disable IRQ
	}
	return(true);																	// nothing to check. return Seccess
}

// ------------------------------------------------------------------------------------------------------
// DMA[x](Channel) Configure
// Channel 0-17
// CHAL_DMA_Xfer_t - structure with xfer parameters
// result is true:successfull otherwise false
inline bool _Chip_DMA_Configure (void *pPeri, uint8_t Channel, _Chip_DMA_Xfer_t *Xfer )
{
	return(false);
}

// ------------------------------------------------------------------------------------------------
// DMA Clear channel interrupt state for selected BitMasked channels
// result is true:successfull otherwise false
bool _Chip_DMA_Clear_IRQ_Status_CH(void *pPeri, uint32_t ChannelBitMask)
{
	((_CHIP_DMA_T*)pPeri)->DMACOMMON[0].INTA |= (ChannelBitMask & 0x0003ffff);
	return(true);
}

// ------------------------------------------------------------------------------------------------
// DMA Get channel interrupt Flag for all channels
// result is bit position for active IRQ
inline uint32_t _Chip_DMA_Get_CHIRQ_Status(void *pPeri)
{
	return(((_CHIP_DMA_T*)pPeri)->DMACOMMON[0].INTA);
}


// ------------------------------------------------------------------------------------------------
// DMA - Get channel control word
extern inline uint32_t _Chip_DMA_Get_CHControl(void *pPeri, uint32_t Channel)
{
	return(((_CHIP_DMA_T*)pPeri)->DMACH[Channel].CTLSTAT | DMA_CH_XFERLEN[Channel]);
}


// ------------------------------------------------------------------------------------------------
// DMA channel interrupt enable/disable for selected channel
// result is true:successfull otherwise false
inline bool _Chip_DMA_Enable_IRQ_CH(void *pPeri, uint32_t ChannelBitMask, bool NewState)
{
	bool res = false;
		
	if(NewState)
	{
		((_CHIP_DMA_T*)pPeri)->DMACOMMON[0].INTENSET |= ChannelBitMask & 0x0003ffff;				// enable interrupt for channels
	}
	else 
	{
		((_CHIP_DMA_T*)pPeri)->DMACOMMON[0].INTENCLR |= ChannelBitMask & 0x0003ffff;				// disable interrupt for channels
	}
	
	return(res);
}	












// ************************************************************************************************
// PWM Functions 
// ************************************************************************************************
// - not used -







// ******************************************************************************************************
// EEPROM Functions
// ******************************************************************************************************
union onepage
{
	uint32_t word[128/4];
	uint8_t	 byte[128];
} ;

uint32_t EEPROM_VIRTUAL_PERI;
 
// ------------------------------------------------------------------------------------------------------
// UART(Num) Initialization
void *_Chip_EEPROM_Init(int32_t PeriIndex)
{
	if(PeriIndex != 0) return(NULL);

	NVIC_DisableIRQ( (IRQn_Type) (EE_IRQn));										// Disable EEPROM interrupt

	//Chip_Clock_EnablePeriphClock(SYSCON_CLOCK_EEPROM);							// enable UART clock
	LPC_SYSCON->SYSAHBCLKCTRL0 |= (1 << 9);											// Enable system clock for EEPROM
	
	LPC_SYSCON->PRESETCTRL0 |= (1 << 9);__nop();									// Activate reset
	LPC_SYSCON->PRESETCTRL0 &= ~(1 << 9);											// Deactivate reset
	
	NVIC_ClearPendingIRQ(EE_IRQn);													// clear pending interrupt flag

	NVIC_DisableIRQ( (IRQn_Type) (EE_IRQn));											// Enable EEPROM interrupt

	return(&EEPROM_VIRTUAL_PERI);
}

// ------------------------------------------------------------------------------------------------------
// EEPROM Enable/Disable
bool _Chip_EEPROM_Enable(void *pPeri, bool NewState)
{
	return(true);
}

//// ------------------------------------------------------------------------------------------------------
//// EMUL_EEPROM Enable/Disable interrupt
//bool _Chip_EEPROM_IRQ_Enable(void *pPeri, bool NewState)
//{
//	
//}

// ------------------------------------------------------------------------------------------------
// Write data to EEPROM
// LPC15xx must use address aligned to 4 !!
bool _Chip_EEPROM_Write(void *pPeri, uint32_t EEDst, uint8_t *pInBuff, size_t byteswrt)
{
//	uint32_t Destination;
//	uint32_t Source;
//	uint32_t TotalBytes = byteswrt;
//	uint32_t Address;
//	uint32_t Alignn;
//	bool res = false;
//	size_t	i = 0;	
//	
//	union onepage	SrcBuff;
//	union onepage	DstBuff;

//	
//	Alignn = EEDst % 4;
//	if(Alignn)																		// dstAddress is not arranget to 4
//	{
//		DstBuff.byte[Alignn] = (uint8_t) EEDst;
//		Destination = DstBuff.word[0];
//	} else Destination = EEDst;

//	
//	Address = (uint32_t) pInBuff;
//	Alignn = Address % 4;
//	
//	while(TotalBytes)
//	{
//		if(Alignn)																	// Source address is not arranged to 4
//		{					// rozsekaj pbuf na tmp buff
//			// copy pbuff to aligned buff - one page
//			while((i < sizeof(SrcBuff.byte)) && TotalBytes)
//			{
//				SrcBuff.byte[i++] = (uint8_t) *pInBuff++;
//				TotalBytes --;
//			}
//			Source = (uint32_t) &SrcBuff.word[0];
//		}
//		else 			// prenes cely buf na jeden zatah
//		{	
//			i = byteswrt;
//			Source = (uint32_t) &pInBuff;
//		}
//		
//		res = Chip_IAP_EEPROM_Write(Destination, Source, (uint32_t) i);

//		if(res == false) break;														// error - exit
//		else TotalBytes -= i;														// minus correctly writes
//	}
//	return (res);


	return(_Chip_IAP_Write_EEPROM(EEDst, (uint32_t) pInBuff, (uint32_t) byteswrt));
}

// ------------------------------------------------------------------------------------------------------
// Read data from EEPROM - using IAP
bool _Chip_EEPROM_Read(void *pPeri, uint32_t EESrc, uint8_t *pOutBuff, size_t bytesrd)
{
	return(_Chip_IAP_Read_EEPROM(EESrc, (uint32_t) pOutBuff, (uint32_t) bytesrd));
}

// ------------------------------------------------------------------------------------------------
// Power down / up
extern inline bool _Chip_EEPROM_Power(void *pPeri, bool NewState)
{
	if(NewState) LPC_SYSCON->PDRUNCFG &= ~(1 << 6);									// Enable Power for periphery
	else  LPC_SYSCON->PDRUNCFG |= (1 << 6);											// Disable Power for periphery
	
	return(true);
}


// ------------------------------------------------------------------------------------------------
// SCT Functions 
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------------
// SCT(Num) Initialization
// result is true:successfull otherwise false
void* _Chip_SCT_Init(int32_t PeriIndex)
{
	// Check:	
	if((PeriIndex > 4) && (PeriIndex < 0)) return(NULL);

	// Power:
	// Nothing to power On/Off	
	
	// Clock:
	LPC_SYSCON->SYSAHBCLKCTRL1 |= (0x04 < PeriIndex);								// Enable system clock for SCTn

	// Reset:
	LPC_SYSCON->PRESETCTRL0 |= (0x04 << PeriIndex);__nop();							// Activate reset
	LPC_SYSCON->PRESETCTRL0 &= ~(0x04 << PeriIndex);								// Deactivate reset

	// Configure
	//Chip_Clock_SetSCTPLLSource(SYSCON_PLLCLKSRC_MAINOSC);
	LPC_SYSCON->SCTPLLCLKSEL  = 0x01;
	
	//Chip_Clock_SetupSCTPLL(1, 3);
	LPC_SYSCON->SCTPLLCTRL = (1 & 0x3F) | ((3 & 0x3) << 6);

	LPC_SYSCON->PDRUNCFG &= ~(1 << 24);												// Power Up
	while ((LPC_SYSCON->SCTPLLSTAT & 1) == 0) {}									// Wait for PLL to lock

	switch(PeriIndex)
	{
		case 3:	
		{
			// Clear IRQ:
			NVIC_ClearPendingIRQ(SCT3_IRQn);										// clear pending interrupt flag

			// Disable IRQ:
			NVIC_DisableIRQ(SCT3_IRQn);												// Enable interrupt flag
			
			return(LPC_SCT3);
		}
		case 2:	
		{
			// Clear IRQ:
			NVIC_ClearPendingIRQ(SCT2_IRQn);										// clear pending interrupt flag

			// Enable IRQ:
			NVIC_DisableIRQ(SCT2_IRQn);												// Enable interrupt flag
			
			return(LPC_SCT2);
		}			
		case 1:
		{
			// Clear IRQ:
			NVIC_ClearPendingIRQ(SCT1_IRQn);										// clear pending interrupt flag

			// Enable IRQ:
			NVIC_DisableIRQ(SCT1_IRQn);												// Enable interrupt flag
			
			return(LPC_SCT1);
		}			
		case 0:	
		{
			// Clear IRQ:
			NVIC_ClearPendingIRQ(SCT0_IRQn);										// clear pending interrupt flag

			// Enable IRQ:
			NVIC_DisableIRQ(SCT0_IRQn);												// Enable interrupt flag
			
			return(LPC_SCT0);
		}			
		default: return(NULL);
	}		
}

// ------------------------------------------------------------------------------------------------------
// SCT(Num) Enable/disable
// result is true:successfull otherwise false
bool _Chip_SCT_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	if(NewState)
	{
		((_CHIP_SCT_T*)pPeri)->CTRL_U &= ~((1 << 18) | (1 << 2));					// Halt SCT
	}
	else
	{
		((_CHIP_SCT_T*)pPeri)->CTRL_U |= ((1 << 18) | (1 << 2));					// unHalt SCT
	}	
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// SCT(PeriIndex) Configure
bool _Chip_SCT_Configure(void *pPeri)
{
	//_CHIP_SCT_T *pPeri = _Chip_SCT_Get_pPeri(PeriIndex);
	return(true);
}

// ------------------------------------------------------------------------------------------------
// 										SWM Functions 
// ------------------------------------------------------------------------------------------------
#define PINASSIGN_IDX(movable)  (((movable) >> 4))
#define PINSHIFT(movable)       (8 * ((movable) & (0xF)))

// ------------------------------------------------------------------------------------------------------
//
bool _Chip_SWM_Enable (void)
{
	LPC_SYSCON->SYSAHBCLKCTRL0 |= 0x03;												// switch clock for SWM Block
	if(LPC_SYSCON->SYSAHBCLKCTRL0 & 0x03) return(true);								// enable was success
	else return (false);															// otherwise no
}

// ------------------------------------------------------------------------------------------------------
// Assign movable pin function to physical pin in Switch Matrix
void _Chip_SWM_MovablePinAssign(CHIP_SWM_PIN_MOVABLE_T movable, uint8_t pin)
{
	uint32_t temp;
	int pinshift = PINSHIFT(movable), regIndex = PINASSIGN_IDX(movable);

	temp = *(&LPC_SWM->PINASSIGN0 + (regIndex )) & (~(0xFF << pinshift));
	*(&LPC_SWM->PINASSIGN0 + (regIndex )) = temp | (pin << pinshift);
	
}

// ------------------------------------------------------------------------------------------------------
// Disables a fixed function pin in the Switch Matrix
void _Chip_SWM_DisableFixedPin(CHIP_SWM_PIN_FIXED_T pin)
{
	uint32_t regOff, pinPos;

	pinPos = ((uint32_t) pin) & 0x1F;
	regOff = ((uint32_t) pin) >> 7;

	*(&LPC_SWM->PINENABLE0 + (regOff)) |= (1 << pinPos);							// Set low to enable fixed pin
}

// ------------------------------------------------------------------------------------------------------
// Enables or disables a fixed function pin in the Switch Matrix
void _Chip_SWM_FixedPinEnable(CHIP_SWM_PIN_FIXED_T pin, bool enable)
{
	if (enable) {
		_Chip_SWM_EnableFixedPin(pin);
	}
	else {
		_Chip_SWM_DisableFixedPin(pin);
	}
}

// ------------------------------------------------------------------------------------------------------
// Tests whether a fixed function pin is enabled or disabled in the Switch Matrix
bool _Chip_SWM_IsFixedPinEnabled(CHIP_SWM_PIN_FIXED_T pin)
{
	uint32_t regOff, pinPos;

	pinPos = ((uint32_t) pin) & 0x1F;
	regOff = ((uint32_t) pin) >> 7;

	return (bool) ((*(&LPC_SWM->PINENABLE0 + (regOff)) & (1 << pinPos)) == 0);
}

// ------------------------------------------------------------------------------------------------------
// Enables a fixed function pin in the Switch Matrix
void _Chip_SWM_EnableFixedPin(CHIP_SWM_PIN_FIXED_T pin)
{
	uint32_t regOff, pinPos;

	pinPos = ((uint32_t) pin) & 0x1F;
	regOff = ((uint32_t) pin) >> 7;

	
	*(&LPC_SWM->PINENABLE0 + (regOff)) &= ~(1 << pinPos);							// Set low to enable fixed pin
}
























































































































// ------------------------------------------------------------------------------------------------
// WDT Functions 
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------------
// WDT(Num) Initialization
// result is true:successfull otherwise false
void *_Chip_WDT_Init(int32_t PeriIndex)
{
	// Check:
	if(PeriIndex != 0) return( NULL );												// Check for valid index of periphery

	// Power:
	LPC_SYSCON->PDRUNCFG &= ~(1 << 20);												// Enable powwer for watchdog oscillator
	
	// Clock:
	LPC_SYSCON->SYSAHBCLKCTRL0 |= 1 << 22;											// Enable system clock for WWDT
	
	// Reset:
	// nothing
	
	// Clear IRQ:
	NVIC_ClearPendingIRQ(WDT_IRQn);													// clear pending interrupt flag

	// Disable IRQ:
	NVIC_DisableIRQ(WDT_IRQn);														// Enable interrupt flag
	
	return(LPC_WWDT);																// return pointer to perihery
}


// ------------------------------------------------------------------------------------------------------
// WDT(Num) Enable/disable
// result is true:successfull otherwise false
bool _Chip_WDT_Enable(void *pPeri, bool NewState)
{
	if(pPeri == NULL ) return(false);
	if(NewState)
	{
		((_CHIP_WWDT_T*)pPeri)->MOD |= (1 << 0);									// Enable WWDT
	}
	else
	{
		((_CHIP_WWDT_T*)pPeri)->MOD &= ~(1 << 0);									// Disable WWDT
	}	
	return(true);
}

// ------------------------------------------------------------------------------------------------
// WDT Interrupt - enable/disable
bool _Chip_WDT_IRQ_Enable(void *pPeri, bool NewState)
{
	if(NewState == true)
	{
		NVIC_ClearPendingIRQ(WDT_IRQn);												// Clear WDT interrupt flag
		NVIC_EnableIRQ(WDT_IRQn);													// Enable WDT interrupt
	}
	else
	{
		NVIC_DisableIRQ(WDT_IRQn);													// Enable WDT interrupt
	}
	return(true);
}

// ------------------------------------------------------------------------------------------------------
// WDT Configure
// HardRst will cause direct chip reset 
// result is true:successfull otherwise false
bool _Chip_WDT_Configure(void *pPeri, uint32_t WinMinVal, uint32_t WinMaxVal, uint32_t WarningVal, bool HardRst)
{
	uint32_t MODMirror = ((_CHIP_WWDT_T*)pPeri)->MOD;
	if(pPeri == NULL ) return(false);
	
	if(WarningVal == 0) MODMirror &= ~(1 << 3);					// Disable Warning Interrupt
	else MODMirror |= (1 << 3);									// Enable Warning Interrupt
	if(HardRst) MODMirror |= (1 << 1);							// Enable chip reset
	else MODMirror &= ~(1 << 1);									// Disable chip reset Warning Interrupt	

	// before MOD register modify, we need perform feed action!
	((_CHIP_WWDT_T*)pPeri)->FEED = 0xAA;
	((_CHIP_WWDT_T*)pPeri)->FEED = 0x55;
	((_CHIP_WWDT_T*)pPeri)->MOD = MODMirror;
	
	((_CHIP_WWDT_T*)pPeri)->WARNINT = WarningVal;
	((_CHIP_WWDT_T*)pPeri)->WINDOW = WinMaxVal;
	
	return(true);
}

#endif
