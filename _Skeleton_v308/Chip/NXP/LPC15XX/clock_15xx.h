// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: clock_15xx.h
// 	   Version: 1.0
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: MCU LPC15xx Chip description/configuration file. Same for all MCUs of this family!
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//

#ifndef __CLOCK_15XX_H_
#define __CLOCK_15XX_H_
#define								OBJECTNAME_CLOCK_15XX			"CLOCK_15XX"


// ******************************************************************************************************
// CONFIGURATION
// ******************************************************************************************************
#ifndef DEBUG_CLOCK_15XX
#define 	DEBUG_CLOCK_15XX							0								// debug bloku default OFF
#endif

// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************

/** Internal oscillator frequency */
#define SYSCON_IRC_FREQ (12000000)

/** Internal watchdog oscillator frequency */
#define SYSCON_WDTOSC_FREQ (503000)

/**
 * Clock source selections for only the main A system clock. The main A system
 * clock is used as an input into the main B system clock selector. Main clock A
 * only needs to be setup if the main clock A input is used in the main clock
 * system selector.
 */
typedef enum CHIP_SYSCON_MAIN_A_CLKSRC {
	SYSCON_MAIN_A_CLKSRC_IRC = 0,		/*!< Internal oscillator */
	SYSCON_MAIN_A_CLKSRCA_MAINOSC,		/*!< Crystal (main) oscillator in */
	SYSCON_MAIN_A_CLKSRCA_SYSOSC = SYSCON_MAIN_A_CLKSRCA_MAINOSC,
	SYSCON_MAIN_A_CLKSRCA_WDTOSC,		/*!< Watchdog oscillator rate */
	SYSCON_MAIN_A_CLKSRCA_RESERVED,
} CHIP_SYSCON_MAIN_A_CLKSRC_T;

/**
 * Clock sources for only main B system clock
 */
typedef enum CHIP_SYSCON_MAIN_B_CLKSRC {
	SYSCON_MAIN_B_CLKSRC_MAINCLKSELA = 0,	/*!< main clock A */
	SYSCON_MAIN_B_CLKSRC_SYSPLLIN,			/*!< System PLL input */
	SYSCON_MAIN_B_CLKSRC_SYSPLLOUT,			/*!< System PLL output */
	SYSCON_MAIN_B_CLKSRC_RTC,				/*!< RTC oscillator 32KHz output */
} CHIP_SYSCON_MAIN_B_CLKSRC_T;

/**
 * Clock sources for main system clock. This is a mix of both main clock A
 * and B seelctions.
 */
typedef enum CHIP_SYSCON_MAINCLKSRC {
	SYSCON_MAINCLKSRC_IRC = 0,			/*!< Internal oscillator */
	SYSCON_MAINCLKSRCA_MAINOSC,			/*!< Crystal (main) oscillator in */
	SYSCON_MAINCLKSRCA_SYSOSC = SYSCON_MAINCLKSRCA_MAINOSC,
	SYSCON_MAINCLKSRCA_WDTOSC,			/*!< Watchdog oscillator rate */
	SYSCON_MAINCLKSRC_SYSPLLIN = 5,		/*!< System PLL input */
	SYSCON_MAINCLKSRC_SYSPLLOUT,		/*!< System PLL output */
	SYSCON_MAINCLKSRC_RTC,				/*!< RTC oscillator 32KHz output */
} CHIP_SYSCON_MAINCLKSRC_T;

/**
 * @brief   Returns the main clock source
 * @return	Returns which clock is used for the main clock source
 * @note	This functions handles both A and B main clock sources.
 */
CHIP_SYSCON_MAINCLKSRC_T Chip_Clock_GetMainClockSource(void);

/**
 * Clock sources for USB (usb_clk)
 */
typedef enum CHIP_SYSCON_USBCLKSRC {
	SYSCON_USBCLKSRC_IRC = 0,		/*!< Internal oscillator */
	SYSCON_USBCLKSRC_MAINOSC,		/*!< Crystal (main) oscillator in */
	SYSCON_USBCLKSRC_SYSOSC = SYSCON_USBCLKSRC_MAINOSC,
	SYSCON_USBCLKSRC_PLLOUT,		/*!< USB PLL out */
	SYSCON_USBCLKSRC_MAINSYSCLK,	/*!< Main system clock (B) */
} CHIP_SYSCON_USBCLKSRC_T;


/**
 * Clock sources for ADC asynchronous clock source select
 */
typedef enum CHIP_SYSCON_ADCASYNCCLKSRC {
	SYSCON_ADCASYNCCLKSRC_IRC = 0,		/*!< Internal oscillator */
	SYSCON_ADCASYNCCLKSRC_SYSPLLOUT,	/*!< System PLL out */
	SYSCON_ADCASYNCCLKSRC_USBPLLOUT,	/*!< USB PLL out */
	SYSCON_ADCASYNCCLKSRC_SCTPLLOUT		/*!< SCT PLL out */
} CHIP_SYSCON_ADCASYNCCLKSRC_T;


/**
 * Clock sources for CLKOUT
 */
typedef enum CHIP_SYSCON_CLKOUTSRC {
	SYSCON_CLKOUTSRC_IRC = 0,		/*!< Internal oscillator for CLKOUT */
	SYSCON_CLKOUTSRC_MAINOSC,		/*!< Main oscillator for CLKOUT */
	SYSCON_CLKOUTSRC_SYSOSC = SYSCON_CLKOUTSRC_MAINOSC,
	SYSCON_CLKOUTSRC_WDTOSC,		/*!< Watchdog oscillator for CLKOUT */
	SYSCON_CLKOUTSRC_MAINSYSCLK,	/*!< Main (B) system clock for CLKOUT */
	SYSCON_CLKOUTSRC_USBPLLOUT = 5,	/*!< USB PLL out */
	SYSCON_CLKOUTSRC_SCTPLLOUT,		/*!< SCT PLL out */
	SYSCON_CLKOUTSRC_RTC32K			/*!< RTC 32 kHz output */
} CHIP_SYSCON_CLKOUTSRC_T;

/**
 * Clock sources for system, USB, and SCT PLLs
 */
typedef enum CHIP_SYSCON_PLLCLKSRC {
	SYSCON_PLLCLKSRC_IRC = 0,		/*!< Internal oscillator in (may not work for USB) */
	SYSCON_PLLCLKSRC_MAINOSC,		/*!< Crystal (main) oscillator in */
	SYSCON_PLLCLKSRC_SYSOSC = SYSCON_PLLCLKSRC_MAINOSC,
	SYSCON_PLLCLKSRC_RESERVED1,		/*!< Reserved */
	SYSCON_PLLCLKSRC_RESERVED2,		/*!< Reserved */
} CHIP_SYSCON_PLLCLKSRC_T;


/**
 * System and peripheral clocks
 */
typedef enum CHIP_SYSCON_CLOCK {
	/* Peripheral clock enables for SYSAHBCLKCTRL0 */
	SYSCON_CLOCK_SYS = 0,				/*!< System clock */
	SYSCON_CLOCK_ROM,					/*!< ROM clock */
	SYSCON_CLOCK_SRAM1 = 3,				/*!< SRAM1 clock */
	SYSCON_CLOCK_SRAM2,					/*!< SRAM2 clock */
	SYSCON_CLOCK_FLASH = 7,				/*!< FLASH controller clock */
	SYSCON_CLOCK_EEPROM = 9,			/*!< EEPROM controller clock */
	SYSCON_CLOCK_MUX = 11,				/*!< Input mux clock */
	SYSCON_CLOCK_SWM,					/*!< Switch matrix clock */
	SYSCON_CLOCK_IOCON,					/*!< IOCON clock */
	SYSCON_CLOCK_GPIO0,					/*!< GPIO0 clock */
	SYSCON_CLOCK_GPIO1,					/*!< GPIO1 clock */
	SYSCON_CLOCK_GPIO2,					/*!< GPIO2 clock */
	SYSCON_CLOCK_PININT = 18,			/*!< PININT clock */
	SYSCON_CLOCK_GINT,					/*!< grouped pin interrupt block clock */
	SYSCON_CLOCK_DMA,					/*!< DMA clock */
	SYSCON_CLOCK_CRC,					/*!< CRC clock */
	SYSCON_CLOCK_WDT,					/*!< WDT clock */
	SYSCON_CLOCK_RTC,					/*!< RTC clock */
	SYSCON_CLOCK_ADC0 = 27,				/*!< ADC0 clock */
	SYSCON_CLOCK_ADC1,					/*!< ADC1 clock */
	SYSCON_CLOCK_DAC,					/*!< DAC clock */
	SYSCON_CLOCK_ACMP,					/*!< ACMP clock */
	/* Peripheral clock enables for SYSAHBCLKCTRL1 */
	SYSCON_CLOCK_MRT = 32,				/*!< multi-rate timer clock */
	SYSCON_CLOCK_RIT,					/*!< repetitive interrupt timer clock */
	SYSCON_CLOCK_SCT0,					/*!< SCT0 clock */
	SYSCON_CLOCK_SCT1,					/*!< SCT1 clock */
	SYSCON_CLOCK_SCT2,					/*!< SCT2 clock */
	SYSCON_CLOCK_SCT3,					/*!< SCT3 clock */
	SYSCON_CLOCK_SCTIPU,				/*!< SCTIPU clock */
	SYSCON_CLOCK_CAN,					/*!< CAN clock */
	SYSCON_CLOCK_SPI0 = 32 + 9,			/*!< SPI0 clock */
	SYSCON_CLOCK_SPI1,					/*!< SPI1 clock */
	SYSCON_CLOCK_I2C0 = 32 + 13,		/*!< I2C0 clock */
	SYSCON_CLOCK_UART0 = 32 + 17,		/*!< UART0 clock */
	SYSCON_CLOCK_UART1,					/*!< UART1 clock */
	SYSCON_CLOCK_UART2,					/*!< UART2 clock */
	SYSCON_CLOCK_QEI = 32 + 21,			/*!< QEI clock */
	SYSCON_CLOCK_USB = 32 + 23,			/*!< USB clock */
} CHIP_SYSCON_CLOCK_T;


/**
 * @brief	Set main A system clock source
 * @param	src	: Clock source for main A
 * @return	Nothing
 * @note	This function only neesd to be setup if main clock A will be
 * selected in the Chip_Clock_GetMain_B_ClockRate() function.
 */
static INLINE void Chip_Clock_SetMain_A_ClockSource(CHIP_SYSCON_MAIN_A_CLKSRC_T src)
{
	LPC_SYSCON->MAINCLKSELA = (uint32_t) src;
}

/**
 * @brief   Returns the main A clock source
 * @return	Returns which clock is used for the main A
 */
static INLINE CHIP_SYSCON_MAIN_A_CLKSRC_T Chip_Clock_GetMain_A_ClockSource(void)
{
	return (CHIP_SYSCON_MAIN_A_CLKSRC_T) (LPC_SYSCON->MAINCLKSELA);
}

/**
 * @brief	Set main B system clock source
 * @param	src	: Clock source for main B
 * @return	Nothing
 */
static INLINE void Chip_Clock_SetMain_B_ClockSource(CHIP_SYSCON_MAIN_B_CLKSRC_T src)
{
	LPC_SYSCON->MAINCLKSELB = (uint32_t) src;
}

/**
 * @brief   Returns the main B clock source
 * @return	Returns which clock is used for the main B
 */
static INLINE CHIP_SYSCON_MAIN_B_CLKSRC_T Chip_Clock_GetMain_B_ClockSource(void)
{
	return (CHIP_SYSCON_MAIN_B_CLKSRC_T) (LPC_SYSCON->MAINCLKSELB);
}

/**
 * @brief	Set USB clock source and divider
 * @param	src	: Clock source for USB
 * @param	div	: divider for USB clock
 * @return	Nothing
 * @note	Use 0 to disable, or a divider value of 1 to 255. The USB clock
 * rate is either the main system clock or USB PLL output clock divided
 * by this value. This function will also toggle the clock source
 * update register to update the clock source.
 */
static INLINE void Chip_Clock_SetUSBClockSource(CHIP_SYSCON_USBCLKSRC_T src, uint32_t div)
{
	LPC_SYSCON->USBCLKSEL = (uint32_t) src;
	LPC_SYSCON->USBCLKDIV = div;
}

/**
 * @brief	Set the ADC asynchronous clock source
 * @param	src	: ADC asynchronous clock source
 * @return	Nothing
 */
static INLINE void Chip_Clock_SetADCASYNCSource(CHIP_SYSCON_ADCASYNCCLKSRC_T src)
{
	LPC_SYSCON->ADCASYNCCLKSEL = (uint32_t) src;
}

/**
 * @brief   Returns the ADC asynchronous clock source
 * @return	Returns which clock is used for the ADC asynchronous clock source
 */
static INLINE CHIP_SYSCON_ADCASYNCCLKSRC_T Chip_Clock_GetADCASYNCSource(void)
{
	return (CHIP_SYSCON_ADCASYNCCLKSRC_T) (LPC_SYSCON->ADCASYNCCLKSEL);
}

/**
 * @brief	Read SCT PLL lock status
 * @return	true of the PLL is locked. false if not locked
 */
static INLINE bool Chip_Clock_IsSCTPLLLocked(void)
{
	return (bool) ((LPC_SYSCON->SCTPLLSTAT & 1) != 0);
}

/**
 * @brief	Set System PLL clock source
 * @param	src	: Clock source for system PLL
 * @return	Nothing
 */
static INLINE void Chip_Clock_SetSystemPLLSource(CHIP_SYSCON_PLLCLKSRC_T src)
{
	LPC_SYSCON->SYSPLLCLKSEL  = (uint32_t) src;
}

/**
 * @brief	Set System PLL divider values
 * @param	msel    : PLL feedback divider value. M = msel + 1.
 * @param	psel    : PLL post divider value. P =  (1<<psel).
 * @return	Nothing
 * @note	See the user manual for how to setup the PLL.
 */
static INLINE void Chip_Clock_SetupSystemPLL(uint8_t msel, uint8_t psel)
{
	LPC_SYSCON->SYSPLLCTRL = (msel & 0x3F) | ((psel & 0x3) << 6);
}

/**
 * @brief	Read System PLL lock status
 * @return	true of the PLL is locked. false if not locked
 */
static INLINE bool Chip_Clock_IsSystemPLLLocked(void)
{
	return (bool) ((LPC_SYSCON->SYSPLLSTAT & 1) != 0);
}

/**
 * @brief	Set USB PLL clock source
 * @param	src	: Clock source for USB PLL
 * @return	Nothing
 */
static INLINE void Chip_Clock_SetUSBPLLSource(CHIP_SYSCON_PLLCLKSRC_T src)
{
	LPC_SYSCON->USBPLLCLKSEL  = (uint32_t) src;
}

/**
 * @brief	Set USB PLL divider values
 * @param	msel    : PLL feedback divider value. M = msel + 1.
 * @param	psel    : PLL post divider value. P = (1<<psel).
 * @return	Nothing
 * @note	See the user manual for how to setup the PLL.
 */
static INLINE void Chip_Clock_SetupUSBPLL(uint8_t msel, uint8_t psel)
{
	LPC_SYSCON->USBPLLCTRL = (msel & 0x3F) | ((psel & 0x3) << 6);
}

/**
 * @brief	Read USB PLL lock status
 * @return	true of the PLL is locked. false if not locked
 */
static INLINE bool Chip_Clock_IsUSBPLLLocked(void)
{
	return (bool) ((LPC_SYSCON->USBPLLSTAT & 1) != 0);
}

/**
 * @brief	Set SCT PLL clock source
 * @param	src	: Clock source for SCT PLL
 * @return	Nothing
 */
static INLINE void Chip_Clock_SetSCTPLLSource(CHIP_SYSCON_PLLCLKSRC_T src)
{
	LPC_SYSCON->SCTPLLCLKSEL  = (uint32_t) src;
}

/**
 * @brief	Set SCT PLL divider values
 * @param	msel    : PLL feedback divider value. M = msel + 1.   	0 - 63
* @param	psel    : PLL post divider value. P = (1<<psel).		0 - 3: 1, 2, 4, 8
 * @return	Nothing
 * @note	See the user manual for how to setup the PLL.
 */
static INLINE void Chip_Clock_SetupSCTPLL(uint8_t msel, uint8_t psel)
{
	LPC_SYSCON->SCTPLLCTRL = (msel & 0x3F) | ((psel & 0x3) << 6);
}

/**
 * @brief	Set system clock divider
 * @param	div	: divider for system clock
 * @return	Nothing
 * @note	Use 0 to disable, or a divider value of 1 to 255. The system clock
 * rate is the main system clock divided by this value.
 */
static INLINE void Chip_Clock_SetSysClockDiv(uint32_t div)
{
	LPC_SYSCON->SYSAHBCLKDIV  = div;
}


static INLINE void Chip_Clock_SetSysTickClockDiv(uint32_t div)
{
	LPC_SYSCON->SYSTICKCLKDIV = div;
}

/**
 * @brief	Returns system tick clock divider
 * @return	system tick clock divider
 */
static INLINE uint32_t Chip_Clock_GetSysTickClockDiv(void)
{
	return LPC_SYSCON->SYSTICKCLKDIV;
}


/**
 * @brief	Set IOCON glitch filter clock divider value
 * @param	div		: value for IOCON filter divider
 * @return	Nothing
 * @note	Use 0 to disable, or a divider value of 1 to 255.
 */
static INLINE void Chip_Clock_SetIOCONFiltClockDiv(uint32_t div)
{
	LPC_SYSCON->IOCONCLKDIV  = div;
}

/**
 * @brief	Return IOCON glitch filter clock divider value
 * @return	IOCON glitch filter clock divider value
 */
static INLINE uint32_t Chip_Clock_GetIOCONFiltClockDiv(void)
{
	return LPC_SYSCON->IOCONCLKDIV;
}

/**
 * @brief	Set Asynchronous ADC clock divider value
 * @param	div	: value for UART fractional generator multiplier value
 * @return	Nothing
 * @note	Use 0 to disable, or a divider value of 1 to 255.
 */
static INLINE void Chip_Clock_SetADCASYNCClockDiv(uint32_t div)
{
	LPC_SYSCON->ADCASYNCCLKDIV  = div;
}

/**
 * @brief	Return Asynchronous ADC clock divider value
 * @return	Asynchronous ADC clock divider value
 */
static INLINE uint32_t Chip_Clock_GetADCASYNCClockDiv(void)
{
	return LPC_SYSCON->ADCASYNCCLKDIV;
}


/**
 * @brief	Set The UART Fractional Generator Divider (all UARTs)
 * @param   div  :  Fractional Generator Divider value, should be 0xFF
 * @return	Nothing
 */
static INLINE void Chip_Clock_SetUARTCLKDivider(uint8_t div)
{
	LPC_SYSCON->UARTCLKDIV = (uint32_t) div;
}

/**
 * @brief	Get The UART Fractional Generator Divider (all UARTs)
 * @return	Value of UART Fractional Generator Divider
 */
static INLINE uint32_t Chip_Clock_GetUARTCLKDivider(void)
{
	return LPC_SYSCON->UARTCLKDIV;
}

/**
 * @brief	Enable the RTC 32KHz output
 * @return	Nothing
 * @note	This clock can be used for the main clock directly, but
 *			do not use this clock with the system PLL.
 */
static INLINE void Chip_Clock_EnableRTCOsc(void)
{
	LPC_SYSCON->RTCOSCCTRL  = 1;
}

/**
 * @brief	Disable the RTC 32KHz output
 * @return	Nothing
 */
static INLINE void Chip_Clock_DisableRTCOsc(void)
{
	LPC_SYSCON->RTCOSCCTRL  = 0;
}

/**
 * @brief	Returns the main oscillator clock rate
 * @return	main oscillator clock rate in Hz
 */
static INLINE uint32_t Chip_Clock_GetMainOscRate(void)
{
	return CONF_CHIP_OSCRATEIN;
}

/**
 * @brief	Returns the internal oscillator (IRC) clock rate
 * @return	internal oscillator (IRC) clock rate in Hz
 */
static INLINE uint32_t Chip_Clock_GetIntOscRate(void)
{
	return SYSCON_IRC_FREQ;
}

/**
 * @brief	Returns the RTC clock rate
 * @return	RTC oscillator clock rate in Hz
 */
static INLINE uint32_t Chip_Clock_GetRTCOscRate(void)
{
	return CONF_CHIP_RTCRATEIN;
}

/**
 * @brief	Return estimated watchdog oscillator rate
 * @return	Estimated watchdog oscillator rate
 * @note	This rate is accurate to plus or minus 40%.
 */
static INLINE uint32_t Chip_Clock_GetWDTOSCRate(void)
{
	return SYSCON_WDTOSC_FREQ;
}

// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************

/**
 * @brief	Return main A clock rate
 * @return	main A clock rate in Hz
 */
uint32_t Chip_Clock_GetMain_A_ClockRate(void);

/**
 * @brief	Return main B clock rate
 * @return	main B clock rate
 */
uint32_t Chip_Clock_GetMain_B_ClockRate(void);

/**
 * @brief	Set main system clock source
 * @param	src	: Main clock source
 * @return	Nothing
 * @note	This functions handles setup of both A and B main clock sources.
 */
void Chip_Clock_SetMainClockSource(CHIP_SYSCON_MAINCLKSRC_T src);

/**
 * @brief	Return main clock rate
 * @return	main clock rate
 */
uint32_t Chip_Clock_GetMainClockRate(void);

/**
 * @brief	Return system clock rate
 * @return	system clock rate
 */
uint32_t Chip_Clock_GetSystemClockRate(void);

/**
 * @brief	Return ADC asynchronous clock rate
 * @return	ADC asynchronous clock rate (not including divider)
 */
uint32_t Chip_Clock_GetADCASYNCRate(void);

/**
 * @brief	Set CLKOUT clock source and divider
 * @param	src	: Clock source for CLKOUT
 * @param	div	: divider for CLKOUT clock
 * @return	Nothing
 * @note	Use 0 to disable, or a divider value of 1 to 255. The CLKOUT clock
 * rate is the clock source divided by the divider. This function will
 * also toggle the clock source update register to update the clock
 * source.
 */
void Chip_Clock_SetCLKOUTSource(CHIP_SYSCON_CLKOUTSRC_T src, uint32_t div);

/**
 * @brief	Return System PLL input clock rate
 * @return	System PLL input clock rate
 */
uint32_t Chip_Clock_GetSystemPLLInClockRate(void);

/**
 * @brief	Return System PLL output clock rate
 * @return	System PLL output clock rate
 */
uint32_t Chip_Clock_GetSystemPLLOutClockRate(void);


/**
 * @brief	Return USB PLL input clock rate
 * @return	USB PLL input clock rate
 */
uint32_t Chip_Clock_GetUSBPLLInClockRate(void);

/**
 * @brief	Return USB PLL output clock rate
 * @return	USB PLL output clock rate
 */
uint32_t Chip_Clock_GetUSBPLLOutClockRate(void);


/**
 * @brief	Return SCT PLL input clock rate
 * @return	SCT PLL input clock rate
 */
uint32_t Chip_Clock_GetSCTPLLInClockRate(void);

/**
 * @brief	Return SCT PLL output clock rate
 * @return	SCT PLL output clock rate
 */
uint32_t Chip_Clock_GetSCTPLLOutClockRate(void);

/**
 * @brief	Enable a system or peripheral clock
 * @param	clk	: Clock to enable
 * @return	Nothing
 */
void Chip_Clock_EnablePeriphClock(CHIP_SYSCON_CLOCK_T clk);

/**
 * @brief	Disable a system or peripheral clock
 * @param	clk	: Clock to disable
 * @return	Nothing
 */

// return current state of selected clock
bool Chip_Clock_GetPeriphClock(CHIP_SYSCON_CLOCK_T clk);


void Chip_Clock_DisablePeriphClock(CHIP_SYSCON_CLOCK_T clk);
/**
 * @brief	Returns the system tick rate as used with the system tick divider
 * @return	the system tick rate
 */

uint32_t Chip_Clock_GetSysTickClockRate(void);
/**
 * @brief	Set system tick clock divider
 * @param	div	: divider for system clock
 * @return	Nothing
 * @note	Use 0 to disable, or a divider value of 1 to 255. The system tick
 * rate is the main system clock divided by this value. Use caution when using
 * the CMSIS SysTick_Config() functions as they typically use SystemCoreClock
 * for setup.
 */
 
/**
 * @brief	Bypass System Oscillator and set oscillator frequency range
 * @param	bypass	: Flag to bypass oscillator
 * @param	highfr	: Flag to set oscillator range from 15-25 MHz
 * @return	Nothing
 * @note	Sets the PLL input to bypass the oscillator. This would be
 * used if an external clock that is not an oscillator is attached
 * to the XTALIN pin.
 */
void Chip_Clock_SetPLLBypass(bool bypass, bool highfr);
/**
 * @brief	Set UART base rate base rate (up to main clock rate) (all UARTs)
 * @param	rate	: Desired rate for fractional divider/multipler output
 * @param	fEnable	: true to use fractional clocking, false for integer clocking
 * @return	Actual rate generated
 * @note	All UARTs use the same base clock for their baud rate
 *			basis. This function is used to generate that clock, while the
 *			UART driver's SetBaud functions will attempt to get the closest
 *			baud rate from this base clock without altering it. This needs
 *			to be setup prior to individual UART setup.<br>
 *			UARTs need a base clock 16x faster than the baud rate, so if you
 *			need a 115.2Kbps baud rate, you will need a clock rate of at
 *			least (115.2K * 16). The UART base clock is generated from the
 *			main system clock, so fractional clocking may be the only
 *			possible choice when using a low main system clock frequency.
 *			Do not alter the FRGCTRL or UARTCLKDIV registers after this call.
 */
uint32_t Chip_Clock_SetUARTBaseClockRate(uint32_t rate, bool fEnable);

/**
 * @brief	Get UART base rate (all UARTs)
 * @return	UART base rate in Hz
 */
uint32_t Chip_Clock_GetUARTBaseClockRate(void);

// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************


#endif 		//__CLOCK_15XX_H_
