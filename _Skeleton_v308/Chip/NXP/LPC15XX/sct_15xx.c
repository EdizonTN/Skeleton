// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: sct_15xx.c
// 	   Version: 1.0
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//	  Desc: SWM
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//

/*****************************************************************************
 * Private types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Public types/enumerations/variables
 ****************************************************************************/

/*****************************************************************************
 * Private functions
 ****************************************************************************/

/*****************************************************************************
 * Public functions
 ****************************************************************************/

/* Initialize SCT */
void Chip_SCT_Init(LPC_SCT_T *pSCT)
{
	uint32_t index = (uint32_t) pSCT;
	index = ((index >> 14) & 0xF) - 6;
	Chip_Clock_EnablePeriphClock((CHIP_SYSCON_CLOCK_T)(SYSCON_CLOCK_SCT0 + index));
	Chip_SYSCON_PeriphReset((CHIP_SYSCON_PERIPH_RESET_T)(RESET_SCT0 + index));
}

/* Shutdown SCT */
void Chip_SCT_DeInit(LPC_SCT_T *pSCT)
{
	uint32_t index = (uint32_t) pSCT;
	index = ((index >> 14) & 0xF) - 6;
	Chip_Clock_DisablePeriphClock((CHIP_SYSCON_CLOCK_T)(SYSCON_CLOCK_SCT0 + index));
}

/* Set/Clear SCT control register */
void Chip_SCT_SetClrControl(LPC_SCT_T *pSCT, uint32_t value, bool NewState)
{
	if (NewState == true) {
		Chip_SCT_SetControl(pSCT, value);
	}
	else {
		Chip_SCT_ClearControl(pSCT, value);
	}
}

/* Set Conflict resolution */
void Chip_SCT_SetConflictResolution(LPC_SCT_T *pSCT, uint8_t outnum, uint8_t value)
{
	uint32_t tem;

	tem = pSCT->RES & (~(0x03 << (2 * outnum)));
	pSCT->RES = tem | (value << (2 * outnum));
}
