// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: iap_15xx.h
// 	   Version: 1.0
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//	  Desc: IAP
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//

#ifndef IAP_15XX_H_
#define IAP_15XX_H_
#define								OBJECTNAME_IAP_15XX			"IAP_15xx"


// ******************************************************************************************************
// CONFIGURATION
// ******************************************************************************************************
#ifndef DEBUG_IAP_15XX
#define 	DEBUG_IAP_15XX							0								// debug bloku default OFF
#endif

// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************

/* IAP command definitions */
#define IAP_PREWRRITE_CMD           50	/*!< Prepare sector for write operation command */
#define IAP_WRISECTOR_CMD           51	/*!< Write Sector command */
#define IAP_ERSSECTOR_CMD           52	/*!< Erase Sector command */
#define IAP_BLANK_CHECK_SECTOR_CMD  53	/*!< Blank check sector */
#define IAP_REPID_CMD               54	/*!< Read PartID command */
#define IAP_READ_BOOT_CODE_CMD      55	/*!< Read Boot code version */
#define IAP_COMPARE_CMD             56	/*!< Compare two RAM address locations */
#define IAP_REINVOKE_ISP_CMD        57	/*!< Reinvoke ISP */
#define IAP_READ_UID_CMD            58	/*!< Read UID */
#define IAP_ERASE_PAGE_CMD          59	/*!< Erase page */
#define IAP_EEPROM_WRITE            61	/*!< EEPROM Write command */
#define IAP_EEPROM_READ             62	/*!< EEPROM READ command */

/* IAP response definitions */
#define IAP_CMD_SUCCESS             0	/*!< Command is executed successfully */
#define IAP_INVALID_COMMAND         1	/*!< Invalid command */
#define IAP_SRC_ADDR_ERROR          2	/*!< Source address is not on word boundary */
#define IAP_DST_ADDR_ERROR          3	/*!< Destination address is not on a correct boundary */
#define IAP_SRC_ADDR_NOT_MAPPED     4	/*!< Source address is not mapped in the memory map */
#define IAP_DST_ADDR_NOT_MAPPED     5	/*!< Destination address is not mapped in the memory map */
#define IAP_COUNT_ERROR             6	/*!< Byte count is not multiple of 4 or is not a permitted value */
#define IAP_INVALID_SECTOR          7	/*!< Sector number is invalid or end sector number is greater than start sector number */
#define IAP_SECTOR_NOT_BLANK        8	/*!< Sector is not blank */
#define IAP_SECTOR_NOT_PREPARED     9	/*!< Command to prepare sector for write operation was not executed */
#define IAP_COMPARE_ERROR           10	/*!< Source and destination data not equal */
#define IAP_BUSY                    11	/*!< Flash programming hardware interface is busy */
#define IAP_PARAM_ERROR             12	/*!< nsufficient number of parameters or invalid parameter */
#define IAP_ADDR_ERROR              13	/*!< Address is not on word boundary */
#define IAP_ADDR_NOT_MAPPED         14	/*!< Address is not mapped in the memory map */
#define IAP_CMD_LOCKED              15	/*!< Command is locked */
#define IAP_INVALID_CODE            16	/*!< Unlock code is invalid */
#define IAP_INVALID_BAUD_RATE       17	/*!< Invalid baud rate setting */
#define IAP_INVALID_STOP_BIT        18	/*!< Invalid stop bit setting */
#define IAP_CRP_ENABLED             19	/*!< Code read protection enabled */

/* IAP_ENTRY API function type */
typedef void (*IAP_ENTRY_T)(unsigned int[], unsigned int[]);


// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************

extern bool Chip_IAP_PreSectorForReadWrite(uint32_t strSector, uint32_t endSector);
extern bool Chip_IAP_CopyRamToFlash(uint32_t DstAdd, uint32_t *pSrc, uint32_t byteswrt);
extern bool Chip_IAP_EraseSector(uint32_t strSector, uint32_t endSector);
extern bool Chip_IAP_BlankCheckSector(uint32_t strSector, uint32_t endSector);
extern bool Chip_IAP_Read_PID(uint32_t *pDst);
extern bool Chip_IAP_Read_BootCode(uint32_t *pDst);
extern bool Chip_IAP_Compare(uint32_t dstAdd, uint32_t srcAdd, uint32_t bytescmp);
extern bool Chip_IAP_ReinvokeISP(uint8_t ISPMode);
extern bool Chip_IAP_Read_UID(uint32_t* pDst);
extern bool Chip_IAP_ErasePage(uint32_t strPage, uint32_t endPage);
extern bool Chip_IAP_EEPROM_Write(uint32_t EEDst, uint32_t RAMSrc, uint32_t byteswrt);
extern bool Chip_IAP_EEPROM_Read(uint32_t EESrc, uint32_t RAMDst, uint32_t bytesrd);

// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************


#endif 		//IAP_15XX_H_
