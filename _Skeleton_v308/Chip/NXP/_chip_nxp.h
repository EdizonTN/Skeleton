// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: _chip_nxp.h
// 	   Version: 3.8
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: NXP's MCU Chip selection file. 
// ******************************************************************************
// Usage: CONF_CHIP_ID_xxxxxxxxxx   must be defined in bsp_XXXXX_Config.h. All others are automatic. 
// 
// ToDo:
// 
// Changelog:   
//				2023.11.12	- v3.8	- Add a LPC844, LPC845
//				2023.05.01	- v3.7 	- Add a LPC11xx family
//				2022.10.14	- v3.1 	- combine ifdef conditions
//              2018.05.06  - v2.0 	- chip manufacturer rename, skeleton system re-directories 
//				2014.05.01	- v1.0 	- first revision. 

#ifndef __CHIP_NXP_H_
#define __CHIP_NXP_H_

//                                                      LPC8xx family
#if defined ( CONF_CHIP_ID_LPC804M101JDH20 ) || \
	defined ( CONF_CHIP_ID_LPC810M021FN8 )   || \
	defined	( CONF_CHIP_ID_LPC811M001JDH16 ) || \
	defined	( CONF_CHIP_ID_LPC812M101JDH16 ) || \
	defined	( CONF_CHIP_ID_LPC812M101JDH20 ) || \
	defined	( CONF_CHIP_ID_LPC812M101JTB16 ) || \
	defined	( CONF_CHIP_ID_LPC824M201JHI33 ) || \
	defined	( CONF_CHIP_ID_LPC824M201JDH20 ) || \
	defined	( CONF_CHIP_ID_LPC822M101JHI33 ) || \
	defined	( CONF_CHIP_ID_LPC822M101JDH20 ) || \
	defined	( CONF_CHIP_ID_LPC844M201JBD64 ) || \
 	defined	( CONF_CHIP_ID_LPC844M201JBD48 ) || \
 	defined	( CONF_CHIP_ID_LPC844M201JHI48 ) || \
 	defined	( CONF_CHIP_ID_LPC844M201JHI33 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JBD64 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JBD48 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JHI48 ) || \
 	defined	( CONF_CHIP_ID_LPC845M301JHI33 )

	#include "LPC8XX\_serie_lpc8xx.h"
//                                                      LPC1100 family
#elif defined(CONF_CHIP_ID_LPC1114FBD48_301)
	#include "LPC11XX\_serie_lpc11xx.h"
//                                                      LPC15xx family
#elif defined( CONF_CHIP_ID_LPC1517JBD48 ) || \
	defined( CONF_CHIP_ID_LPC1517JBD64 ) || \
	defined( CONF_CHIP_ID_LPC1518JBD64 ) || \
	defined( CONF_CHIP_ID_LPC1518JBD100 ) || \
	defined( CONF_CHIP_ID_LPC1519JBD64 ) || \
	defined( CONF_CHIP_ID_LPC1519JBD100 ) || \
	defined( CONF_CHIP_ID_LPC1547JBD64 ) || \
	defined( CONF_CHIP_ID_LPC1548JBD64 ) || \
	defined( CONF_CHIP_ID_LPC1548JBD100 ) || \
	defined( CONF_CHIP_ID_LPC1549JBD48 ) || \
	defined( CONF_CHIP_ID_LPC1549JBD64 ) || \
	defined( CONF_CHIP_ID_LPC1549JBD100 )
	#include "LPC15XX\_serie_lpc15xx.h"
//                                                      LPC17xx family
#elif defined( CONF_CHIP_ID_LPC1774FBD144 ) || \
	defined( CONF_CHIP_ID_LPC1774FBD208 ) || \
	defined( CONF_CHIP_ID_LPC1776FET180 ) || \
	defined( CONF_CHIP_ID_LPC1776FBD208 ) || \
	defined( CONF_CHIP_ID_LPC1777FBD208 ) || \
	defined( CONF_CHIP_ID_LPC1778FBD144 ) || \
	defined( CONF_CHIP_ID_LPC1778FET180 ) || \
	defined( CONF_CHIP_ID_LPC1778FET208 ) || \
	defined( CONF_CHIP_ID_LPC1778FBD208 ) || \
	defined( CONF_CHIP_ID_LPC1785FBD208 ) || \
	defined( CONF_CHIP_ID_LPC1786FBD208 ) || \
	defined( CONF_CHIP_ID_LPC1787FBD208 ) || \
	defined( CONF_CHIP_ID_LPC1788FBD144 ) || \
	defined( CONF_CHIP_ID_LPC1788FET180 ) || \
	defined( CONF_CHIP_ID_LPC1788FET208 ) || \
	defined( CONF_CHIP_ID_LPC1788FBD208 )
	#include "LPC17XX\_serie_lpc17xx.h"
#elif defined( CONF_CHIP_ID_LPC54618J512BD208) || \
	defined( CONF_CHIP_ID_LPC54628J512ET180)
	#include "LPC546XX\_serie_lpc546xx.h"
#else
    #error "Chip not defined: CONF_CHIP_ID_LPCxxxx"
#endif


#endif	//__CHIP_NXP_H_
