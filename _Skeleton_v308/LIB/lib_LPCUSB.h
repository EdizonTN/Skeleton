// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_LPCUSB.h
// 	   Version: 1.0
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: based on NXP Semiconductors, 2012 and Dean Camera, 2011, 2012
//
// Notice: 
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//


#ifndef __LIB_LPCUSB_H_
#define __LIB_LPCUSB_H_

#include "Skeleton.h"

#include "3rd_STACK\lpcUSBLib\Drivers\USB\USB.h"										// Load LPCUSB stack
	
// ******************************************************************************************************
// CONFIGURATION
// ******************************************************************************************************
#define CDC_NOTIFICATION_EPNUM         				1								// Logical Endpoint number of the CDC device-to-host notification IN endpoint.
#define CDC_TX_EPNUM                   				2								// Logical Endpoint number of the CDC device-to-host data IN endpoint.
#if defined(__LPC175X_6X__) || defined(__LPC177X_8X__) || defined(__LPC407X_8X__)
	#define CDC_RX_EPNUM               				5								// Logical Endpoint number of the CDC host-to-device data OUT endpoint.
#else
	#define CDC_RX_EPNUM               				3								// Logical Endpoint number of the CDC host-to-device data OUT endpoint.
#endif
#define CDC_NOTIFICATION_EPSIZE        				64								// Size in bytes of the CDC device-to-host notification IN endpoint.
#define CDC_TXRX_EPSIZE                				64								// Size in bytes of the CDC data IN and OUT endpoints.

#define ECHO_CHARACTER_TASK							(0)
#define CDC_BRIDGE_TASK         					(ECHO_CHARACTER_TASK + 1)

#define CDC_TASK_SELECT ECHO_CHARACTER_TASK





// Dependecies:
//void brd_USB_Init(uint8_t corenum, uint8_t mode);
extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;

// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************
struct lib_LPCUSB_If;

// library interface:
// struktura API funkcii pre lpcUSBLib bridgu. 
typedef struct lib_LPCUSB_Api															// struktura API funkcii pre bdg_lpcUSBLib
{
	bool		(*Init) (struct lib_LPCUSB_If* const pLPCUSB_If, _drv_If_t* pConnectedDrv);	// Inicializacia
//	void	(*Init_HW)(struct lib_LPCUSB_If *pIf, bool All);							// init HW - bez viditelnej zmeny.
//	void 	(*Init_SW)(struct lib_LPCUSB_If *pIf, bool All);							// init SW - inicializacia premennych a ich odoslanie
//	void	(*Enable)(struct lib_LPCUSB_If *pIf, bool NewState);						// Enable/Disable zariadenia
////	void	(*Write_Config)(struct lib_LPCUSB_If *pIf, bool All);				// Zmena funkcnych parametrov zapisom novej konfiguracie - meni chovanie zariadenia
////	void	(*Read_Config)(struct lib_LPCUSB_If *pIf); 									// citanie konfiguracie zo zariadenia	
	uint32_t 	(*Write_Data)(struct lib_LPCUSB_If* const pLPCUSB_If, const uint8_t *pbuff, uint32_t buflen); 	// zapis data - zmena vlastnosti pripojeneho HW zariadenia - jas LED, otacky motora,...
////	uint32_t(*Read_Data)(struct lib_LPCUSB_If *pIf, uint8_t *pbuff, uint32_t maxlen); 	// citaj data
////	void    (*CallBack_ChangedState)(struct lib_LPCUSB_If *pIf); 						// callback na f-ciu - zmenil sa stav
} lib_LPCUSB_Api_t;


#define		USB_STATE_BIT_CONNECTED		1											// USB Connected
#define		USB_STATE_BIT_DISCONNECTED	0											// USB Disconnected
#define		USB_STATE_BIT_CONFCHANGED	2											// config changed	- clear by usr App
#define 	USB_STATE_BIT_CONFIGURED	4											// Configuration saved and active


typedef struct lib_LPCUSB_If
{																					
	char					Name[CONF_DEBUG_STRING_NAME_SIZE];						// interface name - text - only for ID
	_drv_If_t				*pParentDrv;											// which driver send and receive data?
	volatile uint8_t		USB_State;												// stav USB - enumerated, connected,....
	bool 					Initialized;											// USB lib is initialized?
	lib_LPCUSB_Api_t		*pApi;													// pointer to appropriate module functions
	// striktne Stackove premenne:
		  uint8_t			USB_Dev_Address;
	USB_ClassInfo_CDC_Device_t	*pExStack;											// komunikacny interface - pointer na externy stack
} lib_LPCUSB_If_t;


extern lib_LPCUSB_Api_t lib_LPCUSB_Api;
extern lib_LPCUSB_If_t 	lib_LPCUSB_If;

//extern uint32_t lib_LPCUSB_Write_Data (lib_LPCUSB_If_t* const pLPCUSB_If, void *pbuff, uint32_t buflen);
//extern bool 	 lib_LPCUSB_Init (lib_LPCUSB_If_t* const pLPCUSB_If, _drv_If_t* pConnectedDrv);




//typedef bool (*fn_write_rx_data_t)(uint8_t *pDataBuffer, size_t BufferSize, uint32_t *pWriteToAddress, size_t *pLength);
//typedef bool (*fn_read_tx_data_t) (uint8_t *pDataBuffer, size_t BufferSize, uint32_t *pReadFromAddress, size_t *pLength);

//struct bdg_lpcUSBLib_If;															// empty declaration

//// struktura API funkcii pre lpcUSBLib bridgu. 
//typedef struct lpcUSBLib_Api														// struktura API funkcii pre bdg_lpcUSBLib
//{
//	void	(*Init_HW)(struct bdg_lpcUSBLib_If *pIf, bool All);						// init HW - bez viditelnej zmeny.
//	void 	(*Init_SW)(struct bdg_lpcUSBLib_If *pIf, bool All);						// init SW - inicializacia premennych a ich odoslanie
//	void	(*Enable)(struct bdg_lpcUSBLib_If *pIf, bool NewState);					// Enable/Disable zariadenia
////	void	(*Write_Config)(struct bdg_lpcUSBLib_If *pIf, bool All);				// Zmena funkcnych parametrov zapisom novej konfiguracie - meni chovanie zariadenia
////	void	(*Read_Config)(struct bdg_lpcUSBLib_If *pIf); 							// citanie konfiguracie zo zariadenia	
//	void 	(*Write_Data)(struct bdg_lpcUSBLib_If *pIf, uint8_t *pbuff, uint32_t buflen); // zapis data - zmena vlastnosti pripojeneho HW zariadenia - jas LED, otacky motora,...
////	uint32_t(*Read_Data)(struct bdg_lpcUSBLib_If *pIf, uint8_t *pbuff, uint32_t maxlen); 	// citaj data
////	void    (*CallBack_ChangedState)(struct bdg_lpcUSBLib_If *pIf); 				// callback na f-ciu - zmenil sa stav
//} bdg_lpcUSBLib_Api_t;


//typedef struct bdg_lpcUSBLib_If
//{																					
//	char						Name[STRING_NAME_SIZE];								// interface name - text - only for ID
//	struct bdg_lpcUSBLib_If		*pFirst;											// pointer na prvu struct
//	struct bdg_lpcUSBLib_If		*pNext;												// pointer na dalsiu struct - pri definicii pouzijem dummy structure...
//	struct _Peri_Status			Stat;												// struktura so stavom
//	bdg_lpcUSBLib_Api_t			*pApi;												// pointer to appropriate module functions
//	// striktne Stackove premenne:
//	const uint8_t				USB_Mode;											// nastaveny rezim USB	
//	void						(*CallBack_StateChanged)();							// pointer to callback function - StateChaged
//	USB_ClassInfo_CDC_Device_t	*pExStack;											// komunikacny interface - pointer na externy stack
//} bdg_lpcUSBLib_If_t;



// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************
typedef struct {
	USB_Descriptor_Configuration_Header_t    Config;
	USB_Descriptor_Interface_t               CDC_CCI_Interface;
	USB_CDC_Descriptor_FunctionalHeader_t    CDC_Functional_Header;
	USB_CDC_Descriptor_FunctionalACM_t       CDC_Functional_ACM;
	USB_CDC_Descriptor_FunctionalUnion_t     CDC_Functional_Union;
	USB_Descriptor_Endpoint_t                CDC_NotificationEndpoint;
	USB_Descriptor_Interface_t               CDC_DCI_Interface;
	USB_Descriptor_Endpoint_t                CDC_DataOutEndpoint;
	USB_Descriptor_Endpoint_t                CDC_DataInEndpoint;
	unsigned char                            CDC_Termination;
} USB_Descriptor_Configuration_t;

// ******************************************************************************************************
// Functions Prototype - pre kompatibilitu s origo kniznicou (bola prekladana s prepinacom --gnu)
// ******************************************************************************************************
// MassStorageClassDevice.c
static bool MS_Device_ReadInCommandBlock(USB_ClassInfo_MS_Device_t* const MSInterfaceInfo);
static void MS_Device_ReturnCommandStatus(USB_ClassInfo_MS_Device_t* const MSInterfaceInfo);
//RNDISClassDevice.c
void RNDIS_Device_ProcessRNDISControlMessage(USB_ClassInfo_RNDIS_Device_t* const RNDISInterfaceInfo);
static bool RNDIS_Device_ProcessNDISQuery(USB_ClassInfo_RNDIS_Device_t* const RNDISInterfaceInfo, const uint32_t OId, void* const QueryData, const uint16_t QuerySize, void* ResponseData, uint16_t* const ResponseSize);
static bool RNDIS_Device_ProcessNDISSet(USB_ClassInfo_RNDIS_Device_t* const RNDISInterfaceInfo, const uint32_t OId, const void* SetData, const uint16_t SetSize);
// AudioClassHost.c
static uint8_t DCOMP_Audio_Host_NextAudioInterfaceDataEndpoint(void* CurrentDescriptor);
static uint8_t DCOMP_Audio_Host_NextAudioStreamInterface(void* CurrentDescriptor);
static uint8_t DCOMP_Audio_Host_NextAudioControlInterface(void* CurrentDescriptor);
// CDCClassHost.c
static uint8_t DCOMP_CDC_Host_NextCDCInterfaceEndpoint(void* const CurrentDescriptor);
static uint8_t DCOMP_CDC_Host_NextCDCDataInterface(void* const CurrentDescriptor);
static uint8_t DCOMP_CDC_Host_NextCDCControlInterface(void* const CurrentDescriptor);
// HIDClassHost.c
static uint8_t DCOMP_HID_Host_NextHIDInterfaceEndpoint(void* const CurrentDescriptor);
static uint8_t DCOMP_HID_Host_NextHIDInterface(void* const CurrentDescriptor);
static uint8_t DCOMP_HID_Host_NextHIDDescriptor(void* const CurrentDescriptor);
// MassStorageClassHost.c
static uint8_t DCOMP_MS_Host_NextMSInterfaceEndpoint(void* const CurrentDescriptor);
static uint8_t DCOMP_MS_Host_NextMSInterface(void* const CurrentDescriptor);
static uint8_t MS_Host_SendReceiveData(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo, MS_CommandBlockWrapper_t* const SCSICommandBlock, void* BufferPtr);
static uint8_t MS_Host_GetReturnedStatus(USB_ClassInfo_MS_Host_t* const MSInterfaceInfo, MS_CommandStatusWrapper_t* const SCSICommandStatus);
// MIDIClassHost.c
static uint8_t DCOMP_MIDI_Host_NextMIDIStreamingDataEndpoint(void* const CurrentDescriptor);
static uint8_t DCOMP_MIDI_Host_NextMIDIStreamingInterface(void* const CurrentDescriptor);
// PrinterClassHost.c
static uint8_t DCOMP_PRNT_Host_NextPRNTInterfaceEndpoint(void* CurrentDescriptor);
static uint8_t DCOMP_PRNT_Host_NextPRNTInterface(void* CurrentDescriptor);
// RNDISClassHost.c
static uint8_t DCOMP_RNDIS_Host_NextRNDISInterfaceEndpoint(void* const CurrentDescriptor);
static uint8_t DCOMP_RNDIS_Host_NextRNDISDataInterface(void* const CurrentDescriptor);
static uint8_t DCOMP_RNDIS_Host_NextRNDISControlInterface(void* const CurrentDescriptor);

// DeviceStandardReq.c	
void USB_Device_GetStatus(uint8_t corenum);
void USB_Device_ClearSetFeature(uint8_t corenum);
void USB_Device_SetAddress(uint8_t corenum);
void USB_Device_GetDescriptor(uint8_t corenum);
void USB_Device_GetConfiguration(uint8_t corenum);
void USB_Device_SetConfiguration(uint8_t corenum);
void USB_Init_Device(uint8_t corenum);
void USB_Init_Host(uint8_t corenum);
void USB_DeviceTask(uint8_t corenum);
void USB_HostTask(uint8_t corenum);




#endif  //__LIB_LPCUSB_H_

