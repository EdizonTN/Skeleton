// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_RingBuffer.c
// 	   Version: 2.0
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: Circular buffer (Ring) library
//
// Notice: 
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//



#include "Skeleton.h"

#if defined(CONF_USE_LIB_RINGBUFFER) && (CONF_USE_LIB_RINGBUFFER == 1)

#define	LIB_RINGBUFFER_OBJECTNAME		"lib_RingBuffer"
#include <stdlib.h>



// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************

// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************

// ******************************************************************************************************
// Globals
// ******************************************************************************************************

// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************
bool lib_RB_Init (lib_RB_If_t* const pRB_If);											// Reset and init RB - allocate memory also

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

COMP_PUSH_OPTIONS
COMP_OPTIONS_O3
// ------------------------------------------------------------------------------------------------------
// Elements was writed outside this module (ex.:DMA). Pointers must be corrected.
// return: none
// ------------------------------------------------------------------------------------------------------
void lib_RB_Write_External(lib_RB_If_t* const pRB_If, uint32_t ElementsWrite)
{
	__ISB();
	if(!pRB_If->Initialized) return;
	SYS_CPUCRIT_START();
	pRB_If->pWrite = (char*)pRB_If->pWrite + (ElementsWrite * pRB_If->elSize);
	if(pRB_If->pWrite >= pRB_If->pBuff_End) pRB_If->pWrite = pRB_If->pBuff_Start;
	pRB_If->elCount = lib_RB_Get_elCount(pRB_If);									// read current element count
	SYS_CPUCRIT_END();
}


// ------------------------------------------------------------------------------------------------------
// Elements was readed outside this module (ex.:DMA). Pointers must be corrected.
// return: none
// ------------------------------------------------------------------------------------------------------
void lib_RB_Read_External(lib_RB_If_t* const pRB_If, uint32_t ElementsRead)
{
	if(!pRB_If->Initialized) return;
	SYS_CPUCRIT_START();
	pRB_If->pRead = (char*)pRB_If->pRead + (ElementsRead * pRB_If->elSize);
	if(pRB_If->pRead >= pRB_If->pBuff_End) pRB_If->pRead = pRB_If->pBuff_Start;
	pRB_If->elCount = lib_RB_Get_elCount(pRB_If);									// read current element count
	SYS_CPUCRIT_END();
}


// ------------------------------------------------------------------------------------------------------
// RingBuffer initialization
// ------------------------------------------------------------------------------------------------------
bool lib_RB_Init (lib_RB_If_t* const pRB_If)
{
	bool res = false;
	uint32_t MemAllocatedBytes = (uint32_t) (pRB_If->elCapacity); 
	MemAllocatedBytes =  MemAllocatedBytes * (pRB_If->elSize);

#if defined(CONF_DEBUG_LIB_RINGBUFFER) && (CONF_DEBUG_LIB_RINGBUFFER == 1)
	dbgprint("\r\n("LIB_RINGBUFFER_OBJECTNAME") ID[%s]: Init Buff. Element size:%u, Max.Elements:%u", pRB_If->Name, (uint32_t) pRB_If->elSize, (uint32_t) pRB_If->elCapacity);
#endif
	
    if((pRB_If) && (pRB_If->elCapacity) && (pRB_If->elSize))
	{
		if(pRB_If->Initialized != true)
		{
			pRB_If->pBuff_Start = sys_malloc(MemAllocatedBytes);				// allocate memory
		}
#if defined(CONF_DEBUG_LIB_RINGBUFFER) && (CONF_DEBUG_LIB_RINGBUFFER == 1)		
		if(pRB_If->pBuff_Start == NULL) sys_Err ("lib_RB_Init ID[%s]: Not enought memory for allocating %d bytes!", pRB_If->Name, MemAllocatedBytes);
#endif		
		pRB_If->pWrite = pRB_If->pBuff_Start;
        pRB_If->pRead = pRB_If->pBuff_Start;
		pRB_If->elCount = 0;
		pRB_If->pBuff_End = (char *)pRB_If->pBuff_Start + (pRB_If->elCapacity * pRB_If->elSize);	// save position of last element
		pRB_If->Initialized = true;
        res = true;
    }

#if defined(CONF_DEBUG_LIB_RINGBUFFER) && (CONF_DEBUG_LIB_RINGBUFFER == 1)
	dbgprint("\r\n("LIB_RINGBUFFER_OBJECTNAME") ID[%s]: @Mem: %p, MemSize: %d bytes", pRB_If->Name, pRB_If->pBuff_Start, MemAllocatedBytes);	
	dbgprint("\r\n("LIB_RINGBUFFER_OBJECTNAME") ID[%s]: Init Buff :%d", pRB_If->Name, res);
#endif		
	
    return (res);	
}


// ------------------------------------------------------------------------------------------------------
// Copy area to RB
// return: Element Number succesfully copied
// ------------------------------------------------------------------------------------------------------
bool lib_RB_Copy2RB(lib_RB_If_t* const pRB_If, void* pSrc, uint32_t elCount)
{
	uint32_t write_bytes, elFreeSpace, elCopied = 0;
	bool res = false;
	if(!pRB_If->Initialized) return(res);

	if( lib_RB_Get_FreeElement(pRB_If) < elCount) return(res);						// have enough space?
	
	do
	{
		elFreeSpace = lib_RB_Get_Write_elCountLinear((lib_RB_If_t*) pRB_If);			// Get linear free space
		if(elFreeSpace == 0) 
		{
			return(res);
		}
		if(elFreeSpace >= (elCount - elCopied)) write_bytes = (elCount - elCopied) * pRB_If->elSize;
		else write_bytes = elFreeSpace * pRB_If->elSize;
		SYS_CPUCRIT_START();
		memcpy(pRB_If->pWrite,(uint8_t*) (pSrc) + (elCopied * pRB_If->elSize), write_bytes);
		lib_RB_Write_External((lib_RB_If_t*) pRB_If, write_bytes / pRB_If->elSize);	// recalculate pionters Head and Tail
		SYS_CPUCRIT_END();
		
		elCopied += (write_bytes / pRB_If->elSize);
	}while (elCopied < elCount);
	res = true;
	
	return(res);
}
COMP_POP_OPTIONS

#endif	// CONF_USE_LIB_RINGBUFFER
