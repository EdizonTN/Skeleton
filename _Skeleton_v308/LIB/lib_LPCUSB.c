// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_LPCUSB.c
// 	   Version: 1.0
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: based on NXP Semiconductors, 2012 and Dean Camera, 2011, 2012
//
// Notice: 
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//

#include "Skeleton.h"

#if defined(CONF_USE_LIB_LPCUSB) && (CONF_USE_LIB_LPCUSB == 1)

#define	LIB_LPCUSB_OBJECTNAME			"lib_LPCUSB"
#include <stdlib.h>

// ******************************************************************************************************
// LOCAL Prototype
// ******************************************************************************************************
uint32_t lib_LPCUSB_Write_Data (lib_LPCUSB_If_t* const pLPCUSB_If, const uint8_t *pbuff, uint32_t buflen);
bool lib_LPCUSB_Init (lib_LPCUSB_If_t* const pLPCUSB_If, _drv_If_t* pConnectedDrv);


// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
lib_LPCUSB_Api_t 		lib_LPCUSB_Api 		__SECTION_DATA 	=
{
 	.Init			=	lib_LPCUSB_Init,
	.Write_Data		=	lib_LPCUSB_Write_Data,
//	.Events			=	ADC_Events,
};


lib_LPCUSB_If_t			lib_LPCUSB_If 		__SECTION_DATA	= 
{
	.pApi	=	&lib_LPCUSB_Api,
};
// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************


// ******************************************************************************************************
// Stack Import
// ******************************************************************************************************
// On some devices, there is a factory set internal serial number which can be automatically sent to the host as
// the device's serial number when the Device Descriptor's .SerialNumStrIndex entry is set to USE_INTERNAL_SERIAL.
// This allows the host to track a device across insertions on different ports, allowing them to retain allocated
// resources like COM port numbers and drivers. On demos using this feature, give a warning on unsupported devices
// so that the user can supply their own serial number descriptor instead or remove the USE_INTERNAL_SERIAL value
// from the Device Descriptor (forcing the host to generate a serial number for each device from the VID, PID and
// port location).

extern USB_Descriptor_Device_t DeviceDescriptor;
extern USB_Descriptor_Configuration_t ConfigurationDescriptor;
extern uint8_t LanguageString[];
USB_Descriptor_String_t *LanguageStringPtr = (USB_Descriptor_String_t *) LanguageString;
extern uint8_t ManufacturerString[];
USB_Descriptor_String_t *ManufacturerStringPtr = (USB_Descriptor_String_t *) ManufacturerString;
extern uint8_t ProductString[];
USB_Descriptor_String_t *ProductStringPtr = (USB_Descriptor_String_t *) ProductString;

// Class:::::::::::::::::::::::
// Device
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Device/CDCClassDevice.c"
//Host
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Common/HIDParser.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Device/AudioClassDevice.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Device/HIDClassDevice.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Device/MassStorageClassDevice.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Device/MIDIClassDevice.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Device/RNDISClassDevice.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Host/AudioClassHost.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Host/CDCClassHost.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Host/HIDClassHost.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Host/MassStorageClassHost.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Host/MIDIClassHost.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Host/PrinterClassHost.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Class/Host/RNDISClassHost.c"

//Core:
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/ConfigDescriptor.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/Device.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/DeviceStandardReq.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/Endpoint.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/EndpointStream.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/Events.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/Host.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/HostStandardReq.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/Pipe.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/PipeStream.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/USBController.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/USBMemory.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/USBTask.c"

//HAL:
//#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/HAL/LPC11UXX/HAL_LPC11Uxx.c"
//#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/HAL/LPC17XX/HAL_LPC17xx.c"
//#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/HAL/LPC18XX/HAL_LPC18xx.c"

//HCD:
//#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/HCD/EHCI/EHCI.c"
//#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/HCD/OHCI/OHCI.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/HCD/HCD.c"

//DCD:
//#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/DCD/LPC11UXX/Endpoint_LPC11Uxx.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/DCD/LPC17XX/Endpoint_LPC17xx.c"
//#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/DCD/LPC18XX/Endpoint_LPC18xx.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/DCD/USBRom/usbd_cdc.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/DCD/USBRom/usbd_hid.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/DCD/USBRom/usbd_msc.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/DCD/USBRom/usbd_rom.c"
#include "../3rd_STACK/lpcUSBlib/Drivers/USB/Core/DCD/USBRom/usbd_adcuser.c"

//Config:
#include "../3rd_STACK/lpcUSBlib/LPCUSBlibConfig.h"


//Application/user filled descriptors:
//#include "./Application/App_lpcUSBLib_Descriptors.c"



// ******************************************************************************************************
// PUBLIC  Tools 
// ******************************************************************************************************


// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
void Data_Drain( struct _drv_If *pDrvIf, _EventType_t EventType);
inline void Data_Drain( struct _drv_If *pDrvIf, _EventType_t EventType) 			// rutina pre event system pre driver poskytujuci proijate data
{
														// kedze sme presmerovali event system, nevytvori sa task na read data a tak mame driver vo vyhradnom rezime.
	if(EventType == EV_DataReceived)
	{
	}
}


// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Write data
// ------------------------------------------------------------------------------------------------------
uint32_t lib_LPCUSB_Write_Data (lib_LPCUSB_If_t* const pLPCUSB_If, const uint8_t *pbuff, uint32_t buflen)// Write data
{
	uint8_t res;
	res = CDC_Device_SendData(pLPCUSB_If->pExStack,(const char*) pbuff, buflen);
	if(res == ENDPOINT_RWSTREAM_NoError) return(buflen);
	else return(0);
}


// ------------------------------------------------------------------------------------------------------
// Initialize LPCUSB - init and connect with some driver for IO data (e.g.: UART)
// ------------------------------------------------------------------------------------------------------
bool lib_LPCUSB_Init (lib_LPCUSB_If_t* const pLPCUSB_If, _drv_If_t* pConnectedDrv)	// Reset and init LPCUSB - allocate memory also
{
#if defined(CONF_DEBUG_LIB_LPCUSB) && (CONF_DEBUG_LIB_LPCUSB == 1)
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME") ID[%s]: Init LPCUSB.", pLPCUSB_If->Name);
#endif

	if(pLPCUSB_If == NULL) return (false);

	pLPCUSB_If->pParentDrv = pConnectedDrv;										// set output channel (driver)
	
	pLPCUSB_If->USB_State = 0;
	pLPCUSB_If->Initialized = true;
	
		
//#if defined(USB_CAN_BE_DEVICE)			
//			HAL_Reset(0);
//#endif
//	//USBController.c:
//	//void USB_ResetInterface(uint8_t corenum, uint8_t mode)
//	USB_ResetInterface(0, 0);
	return(true);
}


// ******************************************************************************************************
// PUBLIC Functions - insert an user code!
// ******************************************************************************************************

// CALLBACKs +++++++++++++++++++++++++++++
// definovane CallBacky s jednotlivych zariadeni. Dopln osetrenie (priklady su uvedene) alebo prelinkuj na svoju funkciu.

// ------------------------------------------------------------------------------------------------------
// Audio Device
// Example: ..\lpcopen-master\lpcopen\applications\lpc17xx_40xx\examples\LPCUSBlib\lpcusblib_AudioOutputDevice\AudioOutput.c
// ------------------------------------------------------------------------------------------------------
bool CALLBACK_Audio_Device_GetSetEndpointProperty(USB_ClassInfo_Audio_Device_t *const AudioInterfaceInfo, const uint8_t EndpointProperty, const uint8_t EndpointAddress, const uint8_t EndpointControl, uint16_t *const DataLength, uint8_t *Data)
{
	return true;
}

// ------------------------------------------------------------------------------------------------------
// HID Device
// Example: ..\lpcopen-master\lpcopen\applications\LPCUSBlib\lpcusblib_KeyboardHost\KeyboardHost.c
// ------------------------------------------------------------------------------------------------------
bool CALLBACK_HIDParser_FilterHIDReportItem(HID_ReportItem_t *const CurrentItem)
{
	return true;
}

// ------------------------------------------------------------------------------------------------------
// Example: ..\lpcopen-master\lpcopen\applications\LPCUSBlib\lpcusblib_GenericHIDDevice\GenericHID.c
// ------------------------------------------------------------------------------------------------------
bool CALLBACK_HID_Device_CreateHIDReport(USB_ClassInfo_HID_Device_t *const HIDInterfaceInfo, uint8_t *const ReportID, const uint8_t ReportType, void *ReportData, uint16_t *const ReportSize)
{
	return true;
} 

// ------------------------------------------------------------------------------------------------------
// Example: ..\lpcopen-master\lpcopen\applications\LPCUSBlib\lpcusblib_GenericHIDDevice\GenericHID.c
// ------------------------------------------------------------------------------------------------------
void CALLBACK_HID_Device_ProcessHIDReport(USB_ClassInfo_HID_Device_t *const HIDInterfaceInfo, const uint8_t ReportID, const uint8_t ReportType, const void *ReportData, const uint16_t ReportSize)
{ 
}

// ------------------------------------------------------------------------------------------------------
// MassStorage Device
// Example: ..\lpcopen-master\lpcopen\applications\LPCUSBlib\lpcusblib_MassStorageDevice\MassStorage.c
// ------------------------------------------------------------------------------------------------------
bool CALLBACK_MS_Device_SCSICommandReceived(USB_ClassInfo_MS_Device_t *const MSInterfaceInfo)
{
	return true;
} 



// ------------------------------------------------------------------------------------------------------
// This function is called by the library when in device mode, and must be overridden (see library "USB Descriptors"
// documentation) by the application code so that the address and size of a requested descriptor can be given
// to the USB library. When the device receives a Get Descriptor request on the control endpoint, this function
// is called so that the descriptor details can be passed back and the appropriate descriptor sent back to the
// USB host.
// ------------------------------------------------------------------------------------------------------
uint16_t CALLBACK_USB_GetDescriptor(uint8_t corenum, const uint16_t wValue, const uint8_t wIndex, const void * *const DescriptorAddress)
{
	const uint8_t  DescriptorType   = (wValue >> 8);
	const uint8_t  DescriptorNumber = (wValue & 0xFF);

	const void *Address = NULL;
	uint16_t    Size    = NO_DESCRIPTOR;

	switch (DescriptorType) 
	{
	case DTYPE_Device:
		Address = &DeviceDescriptor;
		Size    = sizeof(USB_Descriptor_Device_t);
		break;

	case DTYPE_Configuration:
		Address = &ConfigurationDescriptor;
		Size    = sizeof(USB_Descriptor_Configuration_t);
		break;

	case DTYPE_String:
		switch (DescriptorNumber) 
		{
		case 0x00:
			Address = LanguageStringPtr;
			Size    = pgm_read_byte(&LanguageStringPtr->Header.Size);
			break;

		case 0x01:
			Address = ManufacturerStringPtr;
			Size    = pgm_read_byte(&ManufacturerStringPtr->Header.Size);
			break;

		case 0x02:
			Address = ProductStringPtr;
			Size    = pgm_read_byte(&ProductStringPtr->Header.Size);
			break;
		}

		break;
	}

	*DescriptorAddress = Address;
	return Size;
} 







// ------------------------------------------------------------------------------------------------------
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  A U D I O   D E V I C E
// Example: ..\lpcopen-master\lpcopen\applications\lpc17xx_40xx\examples\LPCUSBlib\lpcusblib_AudioOutputDevice\AudioOutput.c
// ------------------------------------------------------------------------------------------------------
void EVENT_Audio_Device_StreamStartStop(USB_ClassInfo_Audio_Device_t *const AudioInterfaceInfo)
{
#if CONF_DEBUG_LIB_LPCUSB
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME")->Audio Device streamStartStop");
#endif
} 



// ------------------------------------------------------------------------------------------------------
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  C D C   D E V I C E
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\Events.h
// ------------------------------------------------------------------------------------------------------
void EVENT_USB_Device_ControlRequest(void)
{
#if CONF_DEBUG_LIB_LPCUSB
//	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME")->Device Control Request");
#endif	
	
	//if(lib_LPCUSB_If.pParentDrv->pAPI_USR->Events) lib_LPCUSB_If.pParentDrv->pAPI_USR->Events (lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	//else lib_LPCUSB_If.pParentDrv->pAPI_STD->Events(lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	
	CDC_Device_ProcessControlRequest(&VirtualSerial_CDC_Interface);
}


#if !defined(USB_DEVICE_ROM_DRIVER)
// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Class\Device\CDCClassDevice.h
// ------------------------------------------------------------------------------------------------------
void EVENT_CDC_Device_LineEncodingChanged(USB_ClassInfo_CDC_Device_t *const CDCInterfaceInfo)
{
	/*TODO: add LineEncoding processing here
	 * this is just a simple statement, only Baud rate is set */
	// Serial_Init(CDCInterfaceInfo->State.LineEncoding.BaudRateBPS, false);
	
	if(lib_LPCUSB_If.pParentDrv->pAPI_USR->Events) lib_LPCUSB_If.pParentDrv->pAPI_USR->Events (lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	else lib_LPCUSB_If.pParentDrv->pAPI_STD->Events(lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	
#if CONF_DEBUG_LIB_LPCUSB
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME")->CDC Device Set Line Encoding Event");
#endif	
}
#else
// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
void EVENT_UsbdCdc_SetLineCode(CDC_LINE_CODING *line_coding)
{
	//Serial_Init(VirtualSerial_CDC_Interface.State.LineEncoding.BaudRateBPS, false);
	
#if CONF_DEBUG_LIB_LPCUSB
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME")->CDC Device Set Line Code Event");
#endif
}
#endif


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Class\Device\CDCClassDevice.h
// ------------------------------------------------------------------------------------------------------
void EVENT_CDC_Device_BreakSent(USB_ClassInfo_CDC_Device_t* const CDCInterfaceInfo, const uint8_t Duration)
{
	if(lib_LPCUSB_If.pParentDrv->pAPI_USR->Events) lib_LPCUSB_If.pParentDrv->pAPI_USR->Events (lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	else lib_LPCUSB_If.pParentDrv->pAPI_STD->Events(lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	
#if CONF_DEBUG_LIB_LPCUSB
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME")->CDC Device Break Send Event");
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Class\Device\CDCClassDevice.h
// ------------------------------------------------------------------------------------------------------
void EVENT_CDC_Device_ControLineStateChanged(USB_ClassInfo_CDC_Device_t* const CDCInterfaceInfo)
{
//	static uint16_t Old_HostToDevice;
	
	if(lib_LPCUSB_If.pParentDrv->pAPI_USR->Events) lib_LPCUSB_If.pParentDrv->pAPI_USR->Events (lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	else lib_LPCUSB_If.pParentDrv->pAPI_STD->Events(lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	
//	//bdg_CDC_States.CDC_DTE = CDCInterfaceInfo->State.ControlLineStates.HostToDevice;
//	if(Old_HostToDevice != lib_LPCUSB_If.pExStack->State.ControlLineStates.HostToDevice)
//	{
//		if((lib_LPCUSB_If.pParentDrv->pAPI_USR) && (lib_LPCUSB_If.pParentDrv->pAPI_USR->Events)) lib_LPCUSB_If.pParentDrv->pAPI_USR->Events( lib_LPCUSB_If.pParentDrv, EV_StateChanged);	// if is set USR event system, call it
//		else
//		{
//			if(lib_LPCUSB_If.pParentDrv->pAPI_STD->Events) lib_LPCUSB_If.pParentDrv->pAPI_STD->Events( lib_LPCUSB_If.pParentDrv, EV_StateChanged);	// if is set STD event system, call it
//		}			
//		//lib_LPCUSB_If.ConnectedDrv->pPeri_Api->Events( lib_LPCUSB_If.ConnectedDrv, EV_StateChanged);
//		//sys_Execute_Callback( lib_LPCUSB_If.CallBack_StateChanged, LIB_LPCUSB_OBJECTNAME, "__LINE__");
//	}
//	
//	Old_HostToDevice = lib_LPCUSB_If.pExStack->State.ControlLineStates.HostToDevice;
	
#if CONF_DEBUG_LIB_LPCUSB
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME")->CDC Device Control Line State Event");
#endif
}


// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
void EVENT_CDC_Host_ControLineStateChanged(USB_ClassInfo_CDC_Host_t* const CDCInterfaceInfo)
{
	if(lib_LPCUSB_If.pParentDrv->pAPI_USR->Events) lib_LPCUSB_If.pParentDrv->pAPI_USR->Events (lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	else lib_LPCUSB_If.pParentDrv->pAPI_STD->Events(lib_LPCUSB_If.pParentDrv, EV_StateChanged);
#if CONF_DEBUG_LIB_LPCUSB
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME")->CDC Host Control Line State Changed Event");
#endif	
}



// ------------------------------------------------------------------------------------------------------------------------
// USB Event management
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\Events.h

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  U S B   D E V I C E      E V E N T S :


// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
 #if defined(USB_CAN_BE_DEVICE) && !defined(DEVICE_STATE_AS_GPIOR)
	extern volatile uint8_t     USB_DeviceState[MAX_USB_CORE];
 #endif

void EVENT_USB_Device_ConfigurationChanged(void)
{
#if CONF_DEBUG_LIB_LPCUSB
//	dbgprint("\r\nUSB->Device Config Changed");
#endif
	bool ConfigSuccess = true;
	lib_LPCUSB_If.USB_State = USB_DeviceState[0];
	
	ConfigSuccess &= CDC_Device_ConfigureEndpoints(&VirtualSerial_CDC_Interface); 
	
	if(lib_LPCUSB_If.pParentDrv->pAPI_USR->Events) lib_LPCUSB_If.pParentDrv->pAPI_USR->Events (lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	else lib_LPCUSB_If.pParentDrv->pAPI_STD->Events(lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	
	

#if CONF_DEBUG_LIB_LPCUSB
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME") Device Config Changed.");
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Event handler for the library USB Connection event.
// ------------------------------------------------------------------------------------------------------
void EVENT_USB_Device_Connect(void)
{
	lib_LPCUSB_If.USB_State = USB_DeviceState[0];
	
	if(lib_LPCUSB_If.pParentDrv->pAPI_USR->Events) lib_LPCUSB_If.pParentDrv->pAPI_USR->Events (lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	else lib_LPCUSB_If.pParentDrv->pAPI_STD->Events(lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	
#if CONF_DEBUG_LIB_LPCUSB
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME")->Device Connected");
#endif	
}


// ------------------------------------------------------------------------------------------------------
// Event handler for the library USB Disconnection event.
// ------------------------------------------------------------------------------------------------------
void EVENT_USB_Device_Disconnect(void)
{
	lib_LPCUSB_If.USB_State = USB_DeviceState[0];
	
	if(lib_LPCUSB_If.pParentDrv->pAPI_USR->Events) lib_LPCUSB_If.pParentDrv->pAPI_USR->Events (lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	else lib_LPCUSB_If.pParentDrv->pAPI_STD->Events(lib_LPCUSB_If.pParentDrv, EV_StateChanged);
	
#if CONF_DEBUG_LIB_LPCUSB
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME")->Device DisConnected");
#endif		
}



// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  H O S T 

// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\Events.h
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Class\Host\CDCClassHost.h

// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
void EVENT_USB_Host_DeviceAttached(const uint8_t corenum)
{
#if CONF_DEBUG_LIB_LPCUSB
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME")->Host Device Attached");
#endif	
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\Events.h
// ------------------------------------------------------------------------------------------------------
void EVENT_USB_Host_DeviceEnumerationComplete(const uint8_t corenum)
{
#if CONF_DEBUG_LIB_LPCUSB
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME")->Host Device Enumeration Complette");
#endif	
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\Events.h
// ------------------------------------------------------------------------------------------------------
void EVENT_USB_Host_DeviceEnumerationFailed(const uint8_t corenum, const uint8_t ErrorCode, const uint8_t SubErrorCode)
{
#if CONF_DEBUG_LIB_LPCUSB
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME")->Host Device Enumeration Failed");
#endif	
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\Events.h
// ------------------------------------------------------------------------------------------------------
void EVENT_USB_Host_DeviceUnattached(const uint8_t corenum)
{
#if CONF_DEBUG_LIB_LPCUSB
	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME")->Host Device Unattached");
#endif	
}


// ------------------------------------------------------------------------------------------------------
// HOST CONTROLLER INTERFACE
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\HCD\HCD.h
// ------------------------------------------------------------------------------------------------------
HCD_STATUS HcdClearEndpointHalt(uint32_t PipeHandle)
{
	return( HCD_STATUS_OK);
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\HCD\HCD.h
// ------------------------------------------------------------------------------------------------------
HCD_STATUS HcdClosePipe(uint32_t PipeHandle)
{
	return( HCD_STATUS_OK);
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\HCD\HCD.h
// ------------------------------------------------------------------------------------------------------
HCD_STATUS HcdControlTransfer(uint32_t PipeHandle, const USB_Request_Header_t *const pDeviceRequest, uint8_t *const buffer)
{
	return( HCD_STATUS_OK);
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\HCD\HCD.h
// ------------------------------------------------------------------------------------------------------
HCD_STATUS HcdDataTransfer(uint32_t PipeHandle, uint8_t *const buffer, uint32_t const length, uint16_t *const pActualTransferred)
{
	return( HCD_STATUS_OK);
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\HCD\HCD.h
// ------------------------------------------------------------------------------------------------------
HCD_STATUS HcdDeInitDriver(uint8_t HostID)
{
	return( HCD_STATUS_OK);
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\HCD\HCD.h
// ------------------------------------------------------------------------------------------------------
HCD_STATUS HcdGetDeviceSpeed(uint8_t HostID, HCD_USB_SPEED *DeviceSpeed)
{
	return( HCD_STATUS_OK);
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\HCD\HCD.h
// ------------------------------------------------------------------------------------------------------
uint32_t HcdGetFrameNumber(uint8_t HostID)
{
	return( 0 );
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\HCD\HCD.h
// ------------------------------------------------------------------------------------------------------
HCD_STATUS HcdGetPipeStatus(uint32_t PipeHandle)
{
	return( HCD_STATUS_OK);
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\HCD\HCD.h
// ------------------------------------------------------------------------------------------------------
HCD_STATUS HcdInitDriver (uint8_t HostID)
{
	return( HCD_STATUS_OK);
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\HCD\HCD.h
// ------------------------------------------------------------------------------------------------------
HCD_STATUS HcdOpenPipe(uint8_t HostID, uint8_t DeviceAddr, HCD_USB_SPEED DeviceSpeed, uint8_t EndpointNo, HCD_TRANSFER_TYPE TransferType, HCD_TRANSFER_DIR TransferDir, uint16_t MaxPacketSize, uint8_t Interval, uint8_t Mult, uint8_t HSHubDevAddr, uint8_t HSHubPortNum, uint32_t *const PipeHandle)
{
	return( HCD_STATUS_OK);
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\HCD\HCD.h
// ------------------------------------------------------------------------------------------------------
HCD_STATUS HcdRhPortReset(uint8_t HostID)
{
	return( HCD_STATUS_OK);
}


// ------------------------------------------------------------------------------------------------------
// Definicia je v: ..\LPCUSBLib\Drivers\USB\Core\HCD\HCD.h
// ------------------------------------------------------------------------------------------------------
void HcdSetStreamPacketSize(uint32_t PipeHandle, uint16_t packetsize)
{
}



// ------------------------------------------------------------------------------------------------------
// Inicializacia USB jadra corenum
// povodne volana z controller.c:USB_Init(uint8_t corenum, uint8_t mode)
// pre zachovanie kompatibily je pouyzita aj teraz
// v pripade bridgu je volana z nadradeneho drivera_hw_init
// ------------------------------------------------------------------------------------------------------
void HAL_USBInit(uint8_t corenum)
{
//#if CONF_DEBUG_LIB_LPCUSB
//		dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME") core[%d]: HAL_USBInit", corenum);
//#endif	
//			LPC_SC->RSTCON0 |=  (1UL << 31);										// reset USB - potom je zakazana periferia !!
//			LPC_SC->RSTCON0 &=  !(1UL << 31);										// zhod reset reset USB 
//			
//			// zvol zdroj USB z PLL1
//			
//			LPC_SC->PLL1CON   = 0x00;             									// PLL1 disable
//			LPC_SC->USBCLKSEL = CPU_USBDIV_BITS;									// usb_clk - clock pre USB
//			LPC_SC->USBCLKSEL |= (0x02 << 8);										// usb_clk - src pre clock pre USB. ideme z PLL1
////			USBCLKSEL sa smie pripnut na PLL len ak je PLL zastaveny !!!!
//			
//			// nastav CLOCK USB do periferie musi ist 48MHz - ideme cez PLL1
//			// Nastav teda PLL1:
//			LPC_SC->PLL1CFG   = CPU_FREQ_PLL1_M_BITS | (CPU_FREQ_PLL1_P_BITS << 5);	// nastav M a P pomer pre PLL1 - ideme na alt_pll_clk = 48MHz
//			LPC_SC->PLL1CON   = 0x01;             									// PLL1 Enable
//			LPC_SC->PLL1FEED  = 0xAA;
//			LPC_SC->PLL1FEED  = 0x55;
//			while (!(LPC_SC->PLL1STAT & (1UL << 10)));								// Wait for PLOCK1

//			System.Part->Clock_Refresh();											// refresni clock informacie o host MCU

//			// Nastav IO piny periferie
//			drv_GPIO_Init (sig_USB_P);
//			drv_GPIO_Init (sig_USB_N);
//			drv_GPIO_Init (sig_P2_26);												// zapinanie 1k5 PullUp
//			// a nateraz vypni Pull-Up
//			// drv_GPIO_Wr_Direction (sig_P2_26, IO_INPUT);							// PullUp bude ako vstup. Aktivuje sa Internym PullUpom
//			drv_GPIO_ConfPin(sig_P2_26, eIO_Input, 0x00, sig_P2_26.Pin_Funct);		// GPIO nastav bez pull-up/dn interneho rezistora

//			LPC_SC->PCONP |=  (1UL << 31);											// pusti clock na USB periferiu
//		
//#if defined(USB_CAN_BE_DEVICE)			
//			LPC_USB->USBClkCtrl = 0x12;												// Dev, PortSel, AHB clock enable
//			while ((LPC_USB->USBClkSt & 0x12) != 0x12) ;
//			HAL_Reset(corenum);
//#endif

//#if CONF_DEBUG_LIB_LPCUSB
//	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME") core[%d]: HAL_USBInit Done.", corenum);
//#endif	
}


// ------------------------------------------------------------------------------------------------------
// deinicializacia USB jadra corenum
// ------------------------------------------------------------------------------------------------------
void HAL_USBDeInit(uint8_t corenum, uint8_t mode)
{
//#if CONF_DEBUG_LIB_LPCUSB
//	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME") core[%d]: HAL_USBDeInit. Mode:%d", corenum, mode);
//#endif	
//	NVIC_DisableIRQ(USB_IRQn);													/* disable USB interrupt */
//	LPC_SC->PCONP &= (~(1UL << 31));								/* disable USB Per.      */	
//	LPC_SC->USBCLKSEL = 0;															// Disable PLL1
//#if CONF_DEBUG_LIB_LPCUSB
//	dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME") core[%d]: HAL_USBDeInit Done.", corenum);
//#endif
}


// ------------------------------------------------------------------------------------------------------
// enable USB Interrupt
// ------------------------------------------------------------------------------------------------------
void HAL_EnableUSBInterrupt(uint8_t corenum)
{
	NVIC_EnableIRQ(USB_IRQn);					/* enable USB interrupt */
}


// ------------------------------------------------------------------------------------------------------
// disable USB Interrupt
// ------------------------------------------------------------------------------------------------------
void HAL_DisableUSBInterrupt(uint8_t corenum)
{
	NVIC_DisableIRQ(USB_IRQn);					/* enable USB interrupt */
}


// ------------------------------------------------------------------------------------------------------
// USB Connect
// ------------------------------------------------------------------------------------------------------
void HAL_USBConnect(uint8_t corenum, uint32_t con)
{
	if (USB_CurrentMode[corenum] == USB_MODE_Device) 
	{
#if defined(USB_CAN_BE_DEVICE)
		HAL17XX_USBConnect(con);
#endif
		
#if CONF_DEBUG_LIB_LPCUSB
		dbgprint("\r\n("LIB_LPCUSB_OBJECTNAME") core[%d]: HAL_USBConnected. Con:%d.", corenum, con);
#endif		
	}
}

#endif	// CONF_USE_LIB_LPCUSB
