#ifndef winFidonetFGHITerminal10x188x18_FONT_H
#define winFidonetFGHITerminal10x188x18_FONT_H

#include "lpc_fonts.h"

#if defined (__cplusplus)
extern "C"
{
#endif

/***********************************************************************
 * Externally available font information structure
 **********************************************************************/

extern const FONT_T font_winFidonetFGHITerminal10x188x18;

#if defined (__cplusplus)
}
#endif /*__cplusplus */

#endif /* winFidonetFGHITerminal10x188x18_FONT_H */
