// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_NanoModbus.c
// 	   Version: 1.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Modbus RTU/TCP library based on https://github.com/debevv/nanoMODBUS
// ******************************************************************************
// Notice:
//
// Usage:
//			
// ToDo:
//
// Changelog:
//

//	Usually, Modbus Server Devices are measurement devices that measure and store data. 
//	On the other hand, Modbus Client Devices are usually supervisory systems used to monitor and control the Modbus Server devices.
//	A single Modbus client device initiates commands (requests for information), sending them to one or more Modbus server devices on the same network. 
//	Only the Modbus client can initiate communications. 
//	Modbus servers, in turn, remain silent, communicating only when responding to requests from the Modbus client.

#include "Skeleton.h"

#if defined(CONF_USE_LIB_MODBUS) && (CONF_USE_LIB_MODBUS == 1)
#define	LIB_SWIM_OBJECTNAME							"lib_Modbus"

// Modbus Server Devices	- Node device for measurement or action element
// Modbus Client Devices	- Initiate communication - ex. Operator Panel, PLC,...
// arg = must contain ptr to current lib structure interface


// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************


// ------------------------------------------------------------------------------------------------------
// nanoModBuus Error Handler
void onError(lib_Modbus_If_t* pLib_If) 
{
	// modbus error handler
	//while (true) 
	{
		 //sys_Err("ModBus Error!");
	}
	drv_UART_If_Spec_t* pDrvIf_Spec = (drv_UART_If_Spec_t*) pLib_If->pCommDrv->pDrvSpecific;
	pDrvIf_Spec->XFer_Rec.RxCount = 0;
	pDrvIf_Spec->XFer_Rec.TxEstimated = 0;
	pDrvIf_Spec->XFer_Rec.TxLenght = 0;
	pDrvIf_Spec->XFer_Rec.Status = DRV_UART_STAT_IDLE;
	
}

    
// modbus.read	- called from nanomodbus library
// start timeout timer and wait for 'count' character received from communication interface
// arg - containt ptr to root library structure
// Possible results:
//	if ret == count -> NMBS_ERROR_NONE
//	if ret < 0 		-> NMBS_ERROR_TRANSPORT
//	if ret < count	-> NMBS_ERROR_TIMEOUT
//	else 			-> NMBS_ERROR_TRANSPORT
int32_t lib_Modbus_Read (uint8_t* buf, uint16_t count, int32_t byte_timeout_ms, void* arg) 	// Bytes read transport function pointer
{
	//uint32_t IRQ_Channel_ActiveMask;
	if(arg == NULL) return(NMBS_ERROR_TRANSPORT);
	
	drv_UART_If_Spec_t* pComm_Spec = (drv_UART_If_Spec_t*) (((lib_Modbus_If_t*) arg)->pCommDrv->pDrvSpecific);	// pointer for specific struct 
	//drv_MRT_If_Spec_t*  pTimer_Spec = ((drv_MRT_If_Spec_t*) pLib_If->pTimerDrvSpec);
	
	//pTimer_Spec->pRootDrvIf->pAPI_STD->Disable( NULL, pTimer_Spec->pRootDrvIf, 1 << pTimer_Spec->Channel);		// stop timer before config
	//CHAL_MRT_Configure( pTimer_Spec->pPeri, pTimer_Spec->Channel, 1000/byte_timeout_ms, CHAL_Timer_OneShot);	// Configure it
	//pTimer_Spec->pRootDrvIf->pAPI_STD->Enable( NULL, pTimer_Spec->pRootDrvIf, 1 << pTimer_Spec->Channel);		// start timer
	volatile uint_fast32_t TickCount = sys_System.Cnts->SysTickCount + byte_timeout_ms;
	do
	{
		//IRQ_Channel_ActiveMask = CHAL_MRT_Get_IRQ_Status_CH(pTimer_Spec->pPeri); 	// read active channel
	
		//if(IRQ_Channel_ActiveMask & (1 << pTimer_Spec->Channel))					// Timeout fired?
		if(sys_System.Cnts->SysTickCount > TickCount)
		{
			//pTimer_Spec->pRootDrvIf->pAPI_STD->Disable( NULL, pTimer_Spec->pRootDrvIf, pTimer_Spec->Channel);		// stop timer
			//pTimer_Spec->pRootDrvIf->Sys.Stat.ChannelStatus &=  ~(pTimer_Spec->Channel);	// clear flag
			//pTimer_Spec->pRootDrvIf->Sys.Stat.LastEvent = EV_NoChange;
			return(pComm_Spec->XFer_Rec.RxCount);
		}
	} 
	while( pComm_Spec->XFer_Rec.RxCount < count);										// wait for all receive
	//pTimer_Spec->pRootDrvIf->pAPI_STD->Disable( NULL, pTimer_Spec->pRootDrvIf, 1 << pTimer_Spec->Channel);		// stop timer

	sys_memcpy( buf, pComm_Spec->XFer_Rec.pRxBuff, count);
	pComm_Spec->XFer_Rec.RxCount -= count;
	
	return(count);
}

// modbus.write	- called from nanomodbus library
// start timeout timer and wait for 'count' character sent to communication interface
// arg - containt ptr to root library structure
// Possible results:
//	if ret == count -> NMBS_ERROR_NONE
//	if ret < 0 		-> NMBS_ERROR_TRANSPORT
//	if ret < count	-> NMBS_ERROR_TIMEOUT
//	else 			-> NMBS_ERROR_TRANSPORT
int32_t lib_Modbus_Write (const uint8_t* buf, uint16_t count, int32_t byte_timeout_ms, void* arg) // Bytes write transport function pointer
{
	//uint32_t IRQ_Channel_ActiveMask;
	
	if(arg == NULL) return(NMBS_ERROR_TRANSPORT);
	
	drv_UART_If_Spec_t* pComm_Spec = (drv_UART_If_Spec_t*) ((lib_Modbus_If_t*) arg)->pCommDrv->pDrvSpecific;	// pointer for specific struct 
	//drv_MRT_If_Spec_t*  pTimer_Spec = ((drv_MRT_If_Spec_t*) pLib_If->pTimerDrvSpec);
	
	//pTimer_Spec->pRootDrvIf->pAPI_STD->Disable( NULL, pTimer_Spec->pRootDrvIf, 1 << pTimer_Spec->Channel);		// stop timer before config
	//CHAL_MRT_Configure( pTimer_Spec->pPeri, pTimer_Spec->Channel, 1000/byte_timeout_ms, CHAL_Timer_OneShot);	// Configure it
	//pTimer_Spec->pRootDrvIf->pAPI_STD->Enable( NULL, pTimer_Spec->pRootDrvIf, 1 << pTimer_Spec->Channel);		// start timer
	
	volatile uint_fast32_t TickCount = sys_System.Cnts->SysTickCount + byte_timeout_ms;
	do
	{
	}while (sys_System.Cnts->SysTickCount < TickCount);
	
	((drv_UART_Ext_API_t*) ((lib_Modbus_If_t*) arg)->pCommDrv->pAPI_STD->Ext)->RxFlush(((lib_Modbus_If_t*) arg)->pCommDrv);
	((lib_Modbus_If_t*) arg)->pCommDrv->pAPI_STD->Write_Data( NULL, ((lib_Modbus_If_t*) arg)->pCommDrv, (sys_Buffer_t *) buf, count, XFer_Blocking);	// send data
	do
	{
		//IRQ_Channel_ActiveMask = CHAL_MRT_Get_IRQ_Status_CH(pTimer_Spec->pPeri); 	// read active channel
	
		//if(IRQ_Channel_ActiveMask & (1 << pTimer_Spec->Channel))					// Timeout fired?
		if(sys_System.Cnts->SysTickCount > TickCount)
		{
			//pTimer_Spec->pRootDrvIf->pAPI_STD->Disable( NULL, pTimer_Spec->pRootDrvIf, pTimer_Spec->Channel);		// stop timer
			//pTimer_Spec->pRootDrvIf->Sys.Stat.ChannelStatus &=  ~(pTimer_Spec->Channel);	// clear flag
			//pTimer_Spec->pRootDrvIf->Sys.Stat.LastEvent = EV_NoChange;
			return(count - pComm_Spec->XFer_Rec.TxEstimated);
		}
	} 
	while( pComm_Spec->XFer_Rec.Status == DRV_UART_STAT_TXBUSY);					// wait for all send
	//pTimer_Spec->pRootDrvIf->pAPI_STD->Disable( NULL, pTimer_Spec->pRootDrvIf, 1 << pTimer_Spec->Channel);		// stop timer
	
	return(count);																	// return - all OK
}

// ------------------------------------------------------------------------------------------------------
// STD lib Modbus set current server address
// ------------------------------------------------------------------------------------------------------
void lib_Modbus_Set_RTUID(lib_Modbus_If_t* pLib_If, uint8_t New_Addr)
{
	pLib_If->nmbs.address_rtu = New_Addr;
}


// ------------------------------------------------------------------------------------------------------
// STD lib Modbus library initialization
// ------------------------------------------------------------------------------------------------------
bool lib_Modbus_Init( lib_Modbus_If_t* pLib_If, uint8_t Address)
{
	bool res = true;
	
#if defined(CONF_DEBUG_LIB_NANOMODBUS) && (CONF_DEBUG_LIB_NANOMODBUS == 1)
	FN_DEBUG_ENTRY(pLib_If->Name);
#endif	

	if(pLib_If == NULL) return(false);
	pLib_If->Stat.Loaded = true;

	pLib_If->nmbs.platform.arg = pLib_If;
	pLib_If->nmbs.platform.transport = NMBS_TRANSPORT_RTU;
	pLib_If->nmbs.platform.read = &lib_Modbus_Read;									// set communication interface read function callback
	pLib_If->nmbs.platform.write = &lib_Modbus_Write;								// set communication interface write function callback
	pLib_If->nmbs.platform.arg = pLib_If;
	
#ifndef NMBS_CLIENT_DISABLED	
	nmbs_error err;

	// set nanomodbus library callback functions:

	err = nmbs_client_create(&pLib_If->nmbs, &pLib_If->nmbs.platform);
	if (err != NMBS_ERROR_NONE) 
	{
		res = false;
		onError(pLib_If);
	}

	nmbs_set_destination_rtu_address(&pLib_If->nmbs, Address);
#endif
	
#ifndef NMBS_SERVER_DISABLED
	nmbs_callbacks callbacks = {0};
	callbacks.arg = pLib_If;
	

#ifndef NMBS_SERVER_READ_COILS_DISABLED	
	callbacks.read_coils = app_handle_read_coils;
#endif

#ifndef NMBS_SERVER_WRITE_SINGLE_COIL_DISABLED	
	callbacks.write_single_coil = app_write_single_coil;
#endif
	
#ifndef NMBS_SERVER_READ_DISCRETE_INPUTS_DISABLED
	callbacks.read_discrete_inputs = app_handle_read_discrete_inputs;
#endif
	
#ifndef NMBS_SERVER_READ_HOLDING_REGISTERS_DISABLED	
	callbacks.read_holding_registers = app_handler_read_holding_registers;
#endif

#ifndef NMBS_SERVER_READ_INPUT_REGISTERS_DISABLED
	callbacks.read_input_registers = app_handle_read_input_registers;
#endif

#ifndef NMBS_SERVER_WRITE_MULTIPLE_COILS_DISABLED	
	callbacks.write_multiple_coils = app_handle_write_multiple_coils;
#endif	
	
#ifndef	NMBS_SERVER_WRITE_MULTIPLE_REGISTERS_DISABLED
	callbacks.write_multiple_registers = app_handle_write_multiple_registers;
#endif
	
#ifndef	NMBS_SERVER_WRITE_SINLE_REGISTER_DISABLED
	callbacks.write_single_register = app_handle_write_single_register;
#endif

	// Create the modbus server
	nmbs_error err = nmbs_server_create(&pLib_If->nmbs, Address, &pLib_If->nmbs.platform, &callbacks);
	if (err != NMBS_ERROR_NONE) { onError(pLib_If); res = false;}
#endif
		
#if defined(CONF_DEBUG_LIB_NANOMODBUS) && (CONF_DEBUG_LIB_NANOMODBUS == 1)
    FN_DEBUG_EXIT(pLib_If->Name)													// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	

	nmbs_set_read_timeout(&pLib_If->nmbs, CONF_APP_MODBUS_READ_TIMEOUT);
	nmbs_set_byte_timeout(&pLib_If->nmbs, CONF_APP_MODBUS_BYTE_TIMEOUT);
	
	pLib_If->Stat.Initialized = res;
	pLib_If->Stat.Enabled = res;
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// STD lib - write client data to server dest.
// ------------------------------------------------------------------------------------------------------
#ifndef NMBS_CLIENT_DISABLED
bool lib_Modbus_Write_toDest(lib_Modbus_If_t* pLib_If)
{
	bool res = true;
	nmbs_error err;
	
	// Write 2 coils from address 64
	//pLib_If->coils = {0}; 
	//sys_memset_zero(pLib_If->coils, sizeof(pLib_If->coils));
	
//	nmbs_bitfield_write(pLib_If->coils, 0, 1);
//	nmbs_bitfield_write(pLib_If->coils, 1, 1);
//	err = nmbs_write_multiple_coils(&pLib_If->nmbs, 64, 2, pLib_If->coils);
//	if (err != NMBS_ERROR_NONE)	{ onError(pLib_If); res = false;}

//	// Read 3 coils from address 64
//	nmbs_bitfield_reset(pLib_If->coils);    // Reset whole bitfield to zero
//	err = nmbs_read_coils(&pLib_If->nmbs, 64, 3, pLib_If->coils);
//	if (err != NMBS_ERROR_NONE) { onError(pLib_If); res = false;}

	// Write 12 holding registers at address 0	- RTC
	uint16_t w_regs[2] = { 0,0};
	
	//sys_memcpy( &w_regs[0], (const void*) sys_System.Cnts->RTC_Hour, 1);
	//sys_memcpy( &w_regs[1], (const void*) sys_System.Cnts->RTC_Min, 1);
	//sys_memcpy( &w_regs[2], (const void*) sys_System.Cnts->RTC_Sec, 1);
	
	w_regs[0] = sys_System.Cnts->SysTickCount >> 16;
	w_regs[1] = sys_System.Cnts->SysTickCount & 0x0000ffff;
	//sys_memcpy( &w_regs[0], (const void*) sys_System.Cnts->SysTickCount, 4);
	
	err = nmbs_write_multiple_registers(&pLib_If->nmbs, 0, 2, w_regs);
	if (err != NMBS_ERROR_NONE) { onError(pLib_If); res = false;}

//	// Read 2 holding registers from address 26
//	uint16_t r_regs[2];
//	err = nmbs_read_holding_registers(&pLib_If->nmbs, 26, 2, r_regs);
//	if (err != NMBS_ERROR_NONE) { onError(pLib_If); res = false;}
	
	return(res);
}
#endif


// ------------------------------------------------------------------------------------------------------
// STD lib - library initialization
// ------------------------------------------------------------------------------------------------------
void lib_Modbus_Poll( lib_Modbus_If_t* pLib_If)
{
#ifndef NMBS_SERVER_DISABLED	
	
	nmbs_server_poll(&pLib_If->nmbs);

	//	err = nmbs_server_poll(&pLib_If->nmbs);
	//	if (err == NMBS_ERROR_TRANSPORT)
	//		break;
	

  // Write 2 coils from address 64
//  nmbs_bitfield coils = {0};
//  nmbs_bitfield_write(coils, 0, 1);
//  nmbs_bitfield_write(coils, 1, 1);
//  nmbs_error err = nmbs_write_multiple_coils(&pLib_If.nmbs, 64, 2, coils);
//  if (err != NMBS_ERROR_NONE)
//    onError(pLib_If);

//  // Read 3 coils from address 64
//  nmbs_bitfield_reset(coils);    // Reset whole bitfield to zero
//  err = nmbs_read_coils(&pLib_If.nmbs, 64, 3, coils);
//  if (err != NMBS_ERROR_NONE)
//    onError(pLib_If);

//  // Write 2 holding registers at address 26
//  uint16_t w_regs[2] = {123, 124};
//  err = nmbs_write_multiple_registers(&pLib_If.nmbs, 26, 2, w_regs);
//  if (err != NMBS_ERROR_NONE)
//    onError(pLib_If);

//  // Read 2 holding registers from address 26
//  uint16_t r_regs[2];
//  err = nmbs_read_holding_registers(&pLib_If.nmbs, 26, 2, r_regs);
//  if (err != NMBS_ERROR_NONE)
//    onError(pLib_If);
#endif

  
 
}

#include "NanoModbus\nanomodbus.c"

#endif	// CONF_USE_LIB_MODBUS

