// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_CLI.c
// 	   Version: 2.0
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: Command Line Interface library
//		 Process CLI on some char in/out device
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//

#include "Skeleton.h"

#if defined(CONF_USE_LIB_CLI) && (CONF_USE_LIB_CLI == 1)

#define	LIB_CLI_OBJECTNAME		"lib_CLI"
#include <stdlib.h>

// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************
//systmr_t xmodem_tmr;

extern const cli_cmd_list_item_t cli_cmd_list[];									// Externally defined command list that must be defined using CLI_CMD_LIST_CREATE macro
cli_argv_val_t cli_argv_val;														// Converted argument value using cli_util_argv_to_...() conversion function
static char cli_line_buf[CLI_CFG_LINE_LENGTH_MAX];									// Buffer for command line
static uint8_t cli_line_buf_index;
static uint8_t cli_autocomplete_start_index;										// Autocomplete index that is used to mark start of word used for match
static uint8_t cli_autocomplete_end_index;											// Autocomplete index that is used to mark end of word used for match

#if CLI_CFG_HISTORY_SIZE
static char cli_hist_circ_buf[CLI_CFG_HISTORY_SIZE];								// Circular buffer to store history of cmd line strings entered by user.
 #if (CLI_CFG_HISTORY_SIZE <= 256)
	typedef uint8_t cli_hist_size_t;
 #else
	typedef uint16_t cli_hist_size_t;
 #endif
 static cli_hist_size_t cli_hist_index_last;										// Zero terminated *END* of last cmd line string stored in circular buffer
 static cli_hist_size_t cli_hist_index_now;											// Zero terminated *START* of current cmd line string displayed
#endif

static char* cli_argv[CLI_CFG_ARGV_MAX];											// List of pointers to strings (command line string broken up into words)
static const cli_cmd_list_item_t * cli_tree[CLI_CFG_TREE_DEPTH_MAX];				// Current command list item tree being processed
static uint8_t cli_tree_index;														// Index of current command list item tree being processed
static const cli_cmd_list_item_t * cli_cmd_list_item;								// Current command list item being processed

static bool           cli_cmd_get_item            (const cli_cmd_list_item_t * item);
static bool           cli_cmd_item_get_root       (void);
static bool           cli_cmd_item_get_parent     (void);
static bool           cli_cmd_item_get_child      (void);
static bool           cli_cmd_item_get_first      (void);
static bool           cli_cmd_item_get_next       (void);

static uint8_t             cli_cmd_line_to_args        (void);

#if CLI_CFG_HISTORY_SIZE
static cli_hist_size_t  cli_hist_circ_buf_index_prev(cli_hist_size_t index) ATTR_CONST;
static cli_hist_size_t  cli_hist_circ_buf_index_next(cli_hist_size_t index) ATTR_CONST;
static void             cli_hist_copy               (lib_CLI_If_t* const pCLI_If);
static void             cli_hist_save_cmd           (void);
static void             cli_hist_load_older_cmd     (lib_CLI_If_t* const pCLI_If);
static void             cli_hist_load_newer_cmd     (lib_CLI_If_t* const pCLI_If);
#endif

static void             cli_autocomplete_reset      (void);
static bool           	cli_autocomplete            (lib_CLI_If_t* const pCLI_If);
static void             cli_cmd_exe                 (lib_CLI_If_t* const pCLI_If);


// ******************************************************************************************************
// TIMEOUT FUNCTIONS
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------------
//	    Start a timer for timeout check.
//	systmr:			Pointer to a timer object
// delay_in_ticks:	Delay in timer ticks
// ------------------------------------------------------------------------------------------------------------
void systmr_start(systmr_t *systmr, const uint_fast64_t delay_in_ticks)
{
    // Save delay in case timer is restarted
    systmr->delay_in_ticks = delay_in_ticks;

    // Store start tick
    systmr->start_tick = SysTickCnt; //sysclk_get_tick_count();

    // Set state to indicate that timer has started
    systmr->state = SYSTMR_STARTED;
}


// ------------------------------------------------------------------------------------------------------------
//	    See if a timer has expired.
//	systmr   Pointer to a timer object
// return: 	true    timer expired
//			false	timer not expired or timer stopped
// ------------------------------------------------------------------------------------------------------------
bool systmr_has_expired(systmr_t* systmr)
{
    uint_fast64_t tick;

    // See if timer has been stopped
    if (systmr->state == SYSTMR_STOPPED) 
    {
        return false;
    }

    // See if timer has already expired
    if (systmr->state == SYSTMR_EXPIRED) 
    {
        return true;
    }

    // Fetch current time
    tick = SysTickCnt; //sysclk_get_tick_count();

    // Timer expire test
    if( (tick - systmr->start_tick) < systmr->delay_in_ticks )
    {
        return false;
    }

    // Set state to indicate that timer has expired
    systmr->state = SYSTMR_EXPIRED;

    return true;
}


// ------------------------------------------------------------------------------------------------------------
// PUBLIC  Tools 
// ------------------------------------------------------------------------------------------------------------
void Data_Drain( struct _drv_If *pDrvIf, _EventType_t EventType);
inline void Data_Drain( struct _drv_If *pDrvIf, _EventType_t EventType) 			// rutina pre event system pre driver poskytujuci proijate data
{
														// kedze sme presmerovali event system, nevytvori sa task na read data a tak mame driver vo vyhradnom rezime.
	if(EventType == EV_DataReceived)
	{
	}
}


// ******************************************************************************************************
// 												KERMIT
// ******************************************************************************************************
#if defined(CONF_USE_LIB_EKERMIT) && (CONF_USE_LIB_EKERMIT == 1)
// ------------------------------------------------------------------------------------------------------
//	bridge between CLI fn and kermit libs
#endif

// ******************************************************************************************************
// 												YMODEM
// ******************************************************************************************************
#if defined(CONF_CLI_USE_YMODEM) && (CONF_CLI_USE_YMODEM == 1)
// ------------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------------
static uint16_t crc16(lib_CLI_If_t* const pCLI_If, const uint8_t *Buff, uint32_t count)
{
    uint8_t  i;
    uint8_t  j;
    uint8_t  data;
    uint16_t crc = 0x0000;

    // Repeat until all the data has been processed...
    for(i=0; i < count; i++)
    {
        //data = pCLI_If->XModem.Packet.PayLoad[i];
		data = *Buff++;

        // XOR high byte of CRC with 8-bit data
        crc = crc ^ (((uint16_t)data)<<8);

        // Repeat 8 times (for each bit)
        for(j=8; j!=0; j--)
        {
            // Is highest bit set?
            if((crc & (1<<15)) != 0)
            {
                // Shift left and XOR with polynomial CRC16-CCITT (x^16 + x^12 + x^5 + x^0)
                crc = (crc << 1) ^ 0x1021;
            }
            else
            {
                // Shift left
                crc = (crc << 1);
            }
        }
    }
	return crc;
}

// ------------------------------------------------------------------------------------------------------------
// Returns 0 on success, 1 on corrupt packet, -1 on error (timeout):
// ------------------------------------------------------------------------------------------------------------
static int ymodem_receive_packet(lib_CLI_If_t* const pCLI_If, char *data, int *length)
{
	int i, c;
	unsigned int packet_size;

	*length = 0;
	
	//c = _getchar(YMODEM_PACKET_TIMEOUT);
	if(wait_rx_char(pCLI_If, (uint8_t*) &c, false, YMODEM_PACKET_TIMEOUT) == false)
	//if (c < 0) 
	{
		return -1;
	}

	switch(c) 
	{
	case YMODEM_SOH:
		packet_size = YMODEM_PACKET_SIZE;
		break;
	case YMODEM_STX:
		packet_size = YMODEM_PACKET_1K_SIZE;
		break;
	case YMODEM_EOT:
		return 0;
	case YMODEM_CAN:
		//c = _getchar(YMODEM_PACKET_TIMEOUT);
		wait_rx_char(pCLI_If, (uint8_t*) &c, false, YMODEM_PACKET_TIMEOUT);
		if (c == YMODEM_CAN) 
		{
			*length = -1;
			return 0;
		}
	default:
		/* This case could be the result of corruption on the first octet
		* of the packet, but it's more likely that it's the user banging
		* on the terminal trying to abort a transfer. Technically, the
		* former case deserves a NAK, but for now we'll just treat this
		* as an abort case.
		*/
		*length = -1;
		return 0;
	}

	*data = (char)c;

	//for(i = 1; i < (packet_size + YMODEM_PACKET_OVERHEAD); ++i) 
	for(i = 1; i < sizeof(pCLI_If->YModem.Packet); ++i) 
	{
		//c = _getchar(YMODEM_PACKET_TIMEOUT);
		if(wait_rx_char(pCLI_If, (uint8_t*) &c, false, YMODEM_PACKET_TIMEOUT) == false) return (-1);				// timeout ??
//		if (c < 0) 
//		{
//			return -1;
//		}
		//data[i] = (char)c;
		pCLI_If->YModem.RAW[i] = (char)c;
	}

	/* Just a sanity check on the sequence number/complement value.
	 * Caller should check for in-order arrival.
	 */
	if (data[YMODEM_PACKET_SEQNO_INDEX] != ((data[YMODEM_PACKET_SEQNO_COMP_INDEX] ^ 0xff) & 0xff)) {
		return 1;
	}

	if (crc16(pCLI_If, data + YMODEM_PACKET_HEADER, packet_size + YMODEM_PACKET_TRAILER) != 0) 
	{
		return 1;
	}
	*length = packet_size;

	return 0;
}


// ------------------------------------------------------------------------------------------------------------
// Returns the length of the file received, or 0 on error:
// ------------------------------------------------------------------------------------------------------------
unsigned long ymodem_receive_file(lib_CLI_If_t* const pCLI_If, unsigned char *buf, unsigned long length)
{
	unsigned char packet_data[YMODEM_PACKET_1K_SIZE + YMODEM_PACKET_OVERHEAD];
	int packet_length, i, file_done, session_done, crc_tries, crc_nak;
	unsigned int packets_received, errors, first_try = 1;
	char file_name[YMODEM_FILE_NAME_LENGTH], file_size[YMODEM_FILE_SIZE_LENGTH], *file_ptr;
	unsigned char *buf_ptr;
	unsigned long size = 0;

	printf("Ymodem rcv:\n");
	file_name[0] = 0;

	for (session_done = 0, errors = 0; ; ) {
		crc_tries = crc_nak = 1;
		if (!first_try) 
		{
			//_putchar(YMODEM_CRC);
			lib_CLI_Output_Char( pCLI_If, YMODEM_CRC);
		}
		first_try = 0;
		for (packets_received = 0, file_done = 0, buf_ptr = buf; ; ) {
			switch (ymodem_receive_packet(pCLI_If, packet_data, &packet_length)) 
			{

			case 0:
				errors = 0;
				switch (packet_length) {
					case -1:  /* abort */
						//_putchar(YMODEM_ACK);
						lib_CLI_Output_Char( pCLI_If, YMODEM_ACK);
						return 0;
					case 0:   /* end of transmission */
						//_putchar(YMODEM_ACK);
						lib_CLI_Output_Char( pCLI_If, YMODEM_ACK);
						/* Should add some sort of sanity check on the number of
						 * packets received and the advertised file length.
						 */
						file_done = 1;
						break;
					default:  /* normal packet */
					if ((packet_data[YMODEM_PACKET_SEQNO_INDEX] & 0xff) != (packets_received & 0xff)) {
						//_putchar(YMODEM_NAK);
						lib_CLI_Output_Char( pCLI_If, YMODEM_NAK);
					} else {
						if (packets_received == 0) {
							/* The spec suggests that the whole data section should
							 * be zeroed, but I don't think all senders do this. If
							 * we have a NULL filename and the first few digits of
							 * the file length are zero, we'll call it empty.
							 */
							for (i = YMODEM_PACKET_HEADER; i < YMODEM_PACKET_HEADER + 4; i++) {
								if (packet_data[i] != 0) {
									break;
								}
							}
							if (i < YMODEM_PACKET_HEADER + 4) {  /* filename packet has data */
								for (file_ptr = packet_data + YMODEM_PACKET_HEADER, i = 0; *file_ptr && i < YMODEM_FILE_NAME_LENGTH; ) {
									file_name[i++] = *file_ptr++;
								}
								file_name[i++] = '\0';
								for (++file_ptr, i = 0; *file_ptr != ' ' && i < YMODEM_FILE_SIZE_LENGTH; ) {
									file_size[i++] = *file_ptr++;
								}
								file_size[i++] = '\0';
								size = str_to_u32(file_size);
								if (size > length) {
									//_putchar(YMODEM_CAN);
									lib_CLI_Output_Char( pCLI_If, YMODEM_CAN);
									//_putchar(YMODEM_CAN);
									lib_CLI_Output_Char( pCLI_If, YMODEM_CAN);
									_sleep(1);
									printf("\nrcv buffer too small (0x%08x vs 0x%08x)\n", length, size);
									return 0;
								}
								//_putchar(YMODEM_ACK);
								lib_CLI_Output_Char( pCLI_If, YMODEM_ACK);
								//_putchar(crc_nak ? YMODEM_CRC : YMODEM_NAK);
								lib_CLI_Output_Char( pCLI_If, crc_nak ? YMODEM_CRC : YMODEM_NAK);
								crc_nak = 0;
							} else {  /* filename packet is empty; end session */
								//_putchar(YMODEM_ACK);
								lib_CLI_Output_Char( pCLI_If, YMODEM_ACK);
								file_done = 1;
								session_done = 1;
								break;
							}
						} else {
							/* This shouldn't happen, but we check anyway in case the
							 * sender lied in its filename packet:
							 */
							if ((buf_ptr + packet_length) - buf > length) {
								//_putchar(YMODEM_CAN);
								lib_CLI_Output_Char( pCLI_If, YMODEM_CAN);
								//_putchar(YMODEM_CAN);
								lib_CLI_Output_Char( pCLI_If, YMODEM_CAN);
								_sleep(1);
								printf("\nbuffer overflow: %d\n", length);
								return 0;
							}
							for (i=0; i<packet_length; i++) {
								buf_ptr[i] = packet_data[YMODEM_PACKET_HEADER+i];
							}
							buf_ptr += packet_length;
							//_putchar(YMODEM_ACK);
							lib_CLI_Output_Char( pCLI_If, YMODEM_ACK);
						}
						++packets_received;
					}  /* sequence number ok */
				}
				break;

			default:
				if (packets_received != 0) {
					if (++errors >= YMODEM_MAX_ERRORS) 
					{
						//_putchar(YMODEM_CAN);
						lib_CLI_Output_Char( pCLI_If, YMODEM_CAN);
						//_putchar(YMODEM_CAN);
						lib_CLI_Output_Char( pCLI_If, YMODEM_CAN);
						_sleep(1);
						printf("\ntoo many errors - aborted.\n");
						return 0;
					}
				}
				//_putchar(YMODEM_CRC);
				lib_CLI_Output_Char( pCLI_If, YMODEM_CRC);
			}
			if (file_done) 
			{
				break;
			}
		}  /* receive packets */

		if (session_done)
			break;

	}  /* receive files */

	printf("\n");
	if (size > 0) {
		printf("read:%s\n", file_name);
#if defined(YMODEM_WITH_CRC32) && (YMODEM_WITH_CRC32 == 1)
		printf("crc32:0x%08x, len:0x%08x\n", crc32(buf, size), size);
#else
		printf("len:0x%08x\n", size);
#endif
	}
	return size;
}


// ------------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------------
static void ymodem_send_packet(lib_CLI_If_t* const pCLI_If, unsigned char *data, int block_no)
{
	int count, crc, packet_size;

	/* We use a short packet for block 0 - all others are 1K */
	if (block_no == 0) {
		packet_size = YMODEM_PACKET_SIZE;
	} else {
		packet_size = YMODEM_PACKET_1K_SIZE;
	}
	crc = crc16(pCLI_If, data, packet_size);
	/* 128 byte packets use SOH, 1K use STX */
	//_putchar((block_no==0)? YMODEM_SOH : YMODEM_STX);
	lib_CLI_Output_Char( pCLI_If, (block_no==0)? YMODEM_SOH : YMODEM_STX);
	//_putchar(block_no & 0xFF);
	lib_CLI_Output_Char( pCLI_If, block_no & 0xFF);
	//_putchar(~block_no & 0xFF);
	lib_CLI_Output_Char( pCLI_If, ~block_no & 0xFF);
	
	for (count=0; count<packet_size; count++) 
	{
		//_putchar(data[count]);
		lib_CLI_Output_Char( pCLI_If, data[count]);
	}
	//_putchar((crc >> 8) & 0xFF);
	lib_CLI_Output_Char( pCLI_If, (crc >> 8) & 0xFF);
	
	//_putchar(crc & 0xFF);
	lib_CLI_Output_Char( pCLI_If, crc & 0xFF);

	//lib_CLI_Output_Data(pCLI_If, (const char*) &pCLI_If->XModem.RAW[0], sizeof(pCLI_If->XModem.RAW));
}



// ------------------------------------------------------------------------------------------------------------
// Send block 0 (the filename block). filename might be truncated to fit.
// ------------------------------------------------------------------------------------------------------------
static void ymodem_send_packet0(lib_CLI_If_t* const pCLI_If, char* filename, unsigned long size)
{
	unsigned long count = 0;
	unsigned char block[YMODEM_PACKET_SIZE];
	const char* num;

	if (filename) {
		while (*filename && (count < YMODEM_PACKET_SIZE - YMODEM_FILE_SIZE_LENGTH - 2)) {
			block[count++] = *filename++;
		}
		block[count++] = 0;

		num = u32_to_str(size);
		while(*num) {
			block[count++] = *num++;
		}
	}

	while (count < YMODEM_PACKET_SIZE) {
		block[count++] = 0;
	}

	ymodem_send_packet(pCLI_If, block, 0);
}


// ------------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------------
static void ymodem_send_data_packets(lib_CLI_If_t* const pCLI_If, unsigned char* data, unsigned long size)
{
	int blockno = 1;
	unsigned long send_size;
	int ch;

	while (size > 0) {
		if (size > YMODEM_PACKET_1K_SIZE) 
		{
			send_size = YMODEM_PACKET_1K_SIZE;
		}
		else 
		{
			send_size = size;
		}

		ymodem_send_packet(pCLI_If, data, blockno);
		//ch = _getchar(YMODEM_PACKET_TIMEOUT);
		wait_rx_char(pCLI_If, (uint8_t*) &ch, false, YMODEM_PACKET_TIMEOUT)
			
		if (ch == YMODEM_ACK) 
		{
			blockno++;
			data += send_size;
			size -= send_size;
		}
		else 
		{
			if((ch == YMODEM_CAN) || (ch == -1)) 
			{
				return;
			}
		}
	}

	do 
	{
		//_putchar(YMODEM_EOT);
		lib_CLI_Output_Char( pCLI_If, YMODEM_EOT);
		
		//ch = _getchar(YMODEM_PACKET_TIMEOUT);
		wait_rx_char(pCLI_If, (uint8_t*) &ch, false, YMODEM_PACKET_TIMEOUT)
	} while((ch != YMODEM_ACK) && (ch != -1));

	/* Send last data packet */
	if (ch == YMODEM_ACK) 
	{
		//ch = _getchar(YMODEM_PACKET_TIMEOUT);
		wait_rx_char(pCLI_If, (uint8_t*) &ch, false, YMODEM_PACKET_TIMEOUT)
		if (ch == YMODEM_CRC) 
		{
			do 
			{
				ymodem_send_packet0(pCLI_If, 0, 0);
				//ch = _getchar(YMODEM_PACKET_TIMEOUT);
				wait_rx_char(pCLI_If, (uint8_t*) &ch, false, YMODEM_PACKET_TIMEOUT)
			} while((ch != YMODEM_ACK) && (ch != -1));
		}
	}
}

// ------------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------------
unsigned long ymodem_send_file(lib_CLI_If_t* const pCLI_If, unsigned char* buf, unsigned long size, char* filename)
{
	int ch, crc_nak = 1;

	printf("Ymodem send:\n");

	/* Flush the RX FIFO, after a cool off delay */
	_sleep(1);
	while (serial_read() >= 0);

	/* Not in the specs, just for balance */
	do 
	{
		//_putchar(YMODEM_CRC);
		lib_CLI_Output_Char( pCLI_If, YMODEM_CRC);
		//ch = _getchar(1);
		wait_rx_char(pCLI_If, (uint8_t*) &ch, false, YMODEM_PACKET_TIMEOUT);
			
	} while (ch == 0);

	if (ch == YMODEM_CRC) 
	{
		do {
			ymodem_send_packet0(pCLI_If, filename, size);
			/* When the receiving program receives this block and successfully
			 * opened the output file, it shall acknowledge this block with an ACK
			 * character and then proceed with a normal XMODEM file transfer
			 * beginning with a "C" or NAK tranmsitted by the receiver.
			 */
			//ch = _getchar(YMODEM_PACKET_TIMEOUT);
			wait_rx_char(pCLI_If, (uint8_t*) &ch, false, YMODEM_PACKET_TIMEOUT)

			if (ch == YMODEM_ACK) 
			{
				//ch = _getchar(YMODEM_PACKET_TIMEOUT);
				wait_rx_char(pCLI_If, (uint8_t*) &ch, false, YMODEM_PACKET_TIMEOUT);
					
				if (ch == YMODEM_CRC) 
				{
					ymodem_send_data_packets(pCLI_If, buf, size);
					printf("\nsent:%s\n", filename);
#if defined(YMODEM_WITH_CRC32) && (YMODEM_WITH_CRC32 == 1)
					printf("crc32:0x%08x, len:0x%08x\n", crc32(buf, size), size);
#else
					printf("len:0x%08x\n", size);
#endif
					return size;
				}
			}
			else if ((ch == YMODEM_CRC) && (crc_nak)) 
			{
				crc_nak = 0;
				continue;
			} 
			else if ((ch != YMODEM_NAK) || (crc_nak)) 
			{
				break;
			}
		} while(1);
	}
	//_putchar(YMODEM_CAN);
	lib_CLI_Output_Char( pCLI_If, YMODEM_CAN);
	//_putchar(YMODEM_CAN);
	lib_CLI_Output_Char( pCLI_If, YMODEM_CAN);
	
	_sleep(1);
	printf("\naborted.\n");
	return 0;
}
#endif






// ******************************************************************************************************
// 												XMODEM
// ******************************************************************************************************
#if defined(CONF_CLI_USE_XMODEM) && (CONF_CLI_USE_XMODEM == 1)

//// ------------------------------------------------------------------------------------------------------
////	block run and wait for receiving a character
//static bool xmodem_wait_rx_char(lib_CLI_If_t* const pCLI_If, uint8_t *data, bool PrintCountDown)
//{
//	uint16_t countdown = pCLI_If->tmr.delay_in_ticks / CONF_SYSTICK_FREQ;		// countdown in second...
//	char Buff[15];
//	
//    // See if character has been received
//    while(!xmodem_rd_u8(pCLI_If, data))
//    {
//		// if(XMODEM_CFG_TMR_HAS_EXPIRED())
//		if(systmr_has_expired(&pCLI_If->tmr))
//		{
//			return false;
//		}
//		
//		if((System.Cnts->SecondChangeFlag == true) && (PrintCountDown == true))
//		{
//			if(countdown)
//			{
//				sprintf(Buff, "\r ... %d  \r", countdown --);										// print timeout
//				lib_CLI_Output_String( pCLI_If, Buff);
//			}
//		}
//		
//    }
//    return true;
//}


// ------------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------------
static bool xmodem_verify_checksum(lib_CLI_If_t* const pCLI_If, uint16_t crc)
{
    // Compare received CRC with calculated value
    if(pCLI_If->XModem.Packet.Struct.CRC16_hi8 != U16_HI8(crc))
    {    
        return false;
    }
    if(pCLI_If->XModem.Packet.Struct.CRC16_lo8 != U16_LO8(crc))
    {    
        return false;
    }
    return true;
}


// ------------------------------------------------------------------------------------------------------------
// Blocking function with a timeout that tries to receive an XMODEM packet
// retval true    Packet correctly received
// retval false   Packet error
// ------------------------------------------------------------------------------------------------------------
static bool xmodem_rx_packet(lib_CLI_If_t* const pCLI_If, uint32_t timeout)
{
    uint8_t  i = 0; 
    uint8_t  data;

    // Start packet timeout
    //xmodem_tmr_start(XMODEM_TIMEOUT_MS);
	//systmr_start(&pCLI_If->tmr, SYSTMR_MS_TO_TICKS(timeout));

    // Repeat until whole packet has been received
    for(i = 0; i < sizeof(pCLI_If->XModem.Packet.RAW); i++)
    {
        // See if character has been received
        if(!lib_CLI_Input_Char_Wait(pCLI_If, &data, 1, false, timeout))
        {
            // Timeout
            return false;
        }
        // Store received data in buffer
        pCLI_If->XModem.Packet.RAW[i] = data;
        
		// Restart timer
        //xmodem_tmr_start(XMODEM_TIMEOUT_MS);
		//systmr_start(&pCLI_If->tmr, SYSTMR_MS_TO_TICKS(timeout));

		
        // See if this is the first byte of a packet received (xmodem_packet.packet.start)
        if(i == 0)
        {
            // See if End Of Transmission has been received
            if(data == XMODEM_EOT)
            {
                return true;
            }                
        }
    }
    // See if whole packet was received
    if(i != sizeof(pCLI_If->XModem.Packet.RAW))
    {
        return false;
    }
    // See if correct header was received
    if(pCLI_If->XModem.Packet.Struct.Start != XMODEM_SOH)
    {
        return false;
    }
    // Check packet number checksum
    if((pCLI_If->XModem.Packet.Struct.Packet_nr + pCLI_If->XModem.Packet.Struct.Packet_nr_inv) != 255)
    {
        return false;
    }
    // Verify Checksum
    return xmodem_verify_checksum(pCLI_If, crc16(pCLI_If, (uint8_t *) &pCLI_If->XModem.Packet.Struct.PayLoad[0], XMODEM_DATA_SIZE));
}


// ------------------------------------------------------------------------------------------------------------
// 	Create a packed and send
// ------------------------------------------------------------------------------------------------------------
static void xmodem_tx_packet(lib_CLI_If_t* const pCLI_If)
{
	uint16_t crc;

    pCLI_If->XModem.Packet.Struct.Start = XMODEM_SOH;								// Start Of Header
    pCLI_If->XModem.Packet.Struct.Packet_nr = pCLI_If->XModem.packet_nr;			// Packet number
    pCLI_If->XModem.Packet.Struct.Packet_nr_inv = 255 - pCLI_If->XModem.packet_nr;	// Inverse packet number
    crc = crc16(pCLI_If, (uint8_t *) &pCLI_If->XModem.Packet.Struct.PayLoad[0], XMODEM_DATA_SIZE);	// Checksum
    pCLI_If->XModem.Packet.Struct.CRC16_hi8 = U16_HI8(crc);
    pCLI_If->XModem.Packet.Struct.CRC16_lo8 = U16_LO8(crc);

	lib_CLI_Output_Data(pCLI_If, (const char*) &pCLI_If->XModem.Packet.RAW[0], sizeof(pCLI_If->XModem.Packet.RAW));
}


// ------------------------------------------------------------------------------------------------------------
// stiahnutie dat z PC a zapis do EEPROM
// od adresy StartAddress o dlzke Length
// ------------------------------------------------------------------------------------------------------------
bool xmodem_receive_file(lib_CLI_If_t* const pCLI_If, xmodem_on_rx_data_t on_rx_data, uint32_t StartAddress, size_t Length)
{
    uint8_t retry            = XMODEM_CFG_MAX_RETRIES_START;
    bool first_ack_sent = false;
	bool res = false;
	pDrvEvent_Handler_Fn SavedEventRoutine = pCLI_If->ConnectedDrv->pAPI->Events;				// odloz nastaveny Drain prijatych dat

	pCLI_If->ConnectedDrv->pAPI->Events = &Data_Drain;							// Ukradni driveru vyhradne spracovanie prijatych dat.

	
    // Reset packet number
    pCLI_If->XModem.packet_nr = 1;

	lib_CLI_Output_String( pCLI_If, "XMODEM transfer ready. Waiting for data 10 sec.\r\n");
	
    // Repeat until transfer is finished or error count is exceeded
    while(retry--)
    {

		if(!first_ack_sent)
		{
			// Send initial start character to start transfer (with CRC checking)
			pCLI_If->ConnectedDrv->pAPI->Read_Data(NULL, pCLI_If->ConnectedDrv, NULL, 0);	// clear RX Buffer /FLUSH/
			
			lib_CLI_Output_Char( pCLI_If, XMODEM_C);								// send connection 'C' char
			
			// First ACK sent
			first_ack_sent = true;
			if(!xmodem_rx_packet(pCLI_If, XMODEM_TIMEOUT_MS))						// wait for first data packet	
			{
#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)
				dbgprint("\r\n("LIB_CLI_OBJECTNAME") ID[%s]: XMODEM Recv - Trying to connect. Retry:%d", pCLI_If->Name, retry);
#endif				
				first_ack_sent = false;												// Connection again
				continue;
			} 
			else 
			{
				first_ack_sent = true;												// now are connected!
				retry = XMODEM_CFG_MAX_RETRIES;
			}
				
		}
		else
		{
			// Try to receive a packet
			if(!xmodem_rx_packet(pCLI_If, XMODEM_TIMEOUT_MS))
			{
				pCLI_If->ConnectedDrv->pAPI->Read_Data(NULL, pCLI_If->ConnectedDrv, NULL, 0);	// clear RX Buffer /FLUSH/
				lib_CLI_Output_Char( pCLI_If, XMODEM_NAK);
#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)
				dbgprint("\r\n("LIB_CLI_OBJECTNAME") ID[%s]: XMODEM Recv - Packet not received. Retry:%d", pCLI_If->Name, retry);
#endif				
				continue;
			}
		}
		
	
        // End Of Transfer received?
        if(pCLI_If->XModem.Packet.Struct.Start == XMODEM_EOT)
        {
            res = true;
			// Acknowledge EOT
            lib_CLI_Output_Char( pCLI_If, XMODEM_ACK);
            break;
        }
        // Duplicate packet received?
        if(pCLI_If->XModem.Packet.Struct.Packet_nr == (pCLI_If->XModem.packet_nr - 1))
        {
            // Acknowledge packet
			pCLI_If->ConnectedDrv->pAPI->Read_Data(NULL, pCLI_If->ConnectedDrv, NULL, 0);	// clear RX Buffer /FLUSH/
			
            lib_CLI_Output_Char( pCLI_If, XMODEM_ACK);
#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)
				dbgprint("\r\n("LIB_CLI_OBJECTNAME") ID[%s]: XMODEM Recv - Duplicate packet received. Retry:%d", pCLI_If->Name, retry);
#endif				
            continue;																// go to start of while
        }
        // Expected packet received?
        if(pCLI_If->XModem.Packet.Struct.Packet_nr != pCLI_If->XModem.packet_nr)
        {
#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)
				dbgprint("\r\n("LIB_CLI_OBJECTNAME") ID[%s]: XMODEM Recv - Another packet received. Retry:%d", pCLI_If->Name, retry);
#endif				
			pCLI_If->ConnectedDrv->pAPI->Read_Data(NULL, pCLI_If->ConnectedDrv, NULL, 0);	// clear RX Buffer /FLUSH/
			
            // NAK packet
            lib_CLI_Output_Char( pCLI_If, XMODEM_NAK);
            continue;
        }
        // Pass received data on to handler
        (*on_rx_data)(&pCLI_If->XModem.Packet.Struct.PayLoad[0], sizeof(pCLI_If->XModem.Packet.Struct.PayLoad), (uint32_t *) &StartAddress, (size_t *) &Length);	// process packet
        // Acknowledge packet
        lib_CLI_Output_Char( pCLI_If, XMODEM_ACK);
        // Next packet
        pCLI_If->XModem.packet_nr ++;
        // Reset retry count
        retry = XMODEM_CFG_MAX_RETRIES;
    }

    // Too many errors?
    if(retry == 0)
    {
#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)
		dbgprint("\r\n("LIB_CLI_OBJECTNAME") ID[%s]: XMODEM Recv - Retry count reached! PacketNr:%d", pCLI_If->Name, pCLI_If->XModem.packet_nr);
#endif
		res = false;
    }

	pCLI_If->ConnectedDrv->pAPI->Read_Data(NULL, pCLI_If->ConnectedDrv, NULL, 0);	// clear RX Buffer /FLUSH/
	
	pCLI_If->ConnectedDrv->pAPI->Events = SavedEventRoutine;					// vrat event system
	
    return (res);
}


// ------------------------------------------------------------------------------------------------------------
//  parameter je funkcia, ktora poskytuje data 
// vstupom su adresa zaciatku prenosu v pamati a tlzka v bytes
// ------------------------------------------------------------------------------------------------------------
bool xmodem_send_file(lib_CLI_If_t* const pCLI_If, fn_read_tx_data_t on_tx_data, uint32_t StartAddress, size_t Length)
{
    uint8_t retry;
	bool res = false;
	pDrvEvent_Handler_Fn SavedEventRoutine = pCLI_If->ConnectedDrv->pAPI->Events;				// odloz nastaveny Drain prijatych dat
	
	
	pCLI_If->ConnectedDrv->pAPI->Events = &Data_Drain;							// Ukradni driveru vyhradne spracovanie prijatych dat.
    
    pCLI_If->XModem.packet_nr = 1;															// Reset packet number

    // Wait for initial start character to start transfer (with CRC checking)
    lib_CLI_Output_String( pCLI_If, "Waiting for start of XMODEM(CRC) transfer...\r\n");
	
	//systmr_start(&pCLI_If->tmr, SYSTMR_MS_TO_TICKS(XMODEM_CFG_STARTWAIT_MSEC));// start waitng 10 sec...
	
    res = lib_CLI_Input_Char_Wait(pCLI_If, &pCLI_If->RxChar, 1, true, XMODEM_CFG_STARTWAIT_MSEC);

	if((res == true) && ((pCLI_If->RxChar == XMODEM_C) || (pCLI_If->RxChar == XMODEM_NAK))) 	// wait for 'C' or NAK
	{
		res = true;
	}
	else
	{
		res = false;
#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)
					dbgprint("\r\n("LIB_CLI_OBJECTNAME") ID[%s]: XMODEM Send - No 'C'!", pCLI_If->Name);
#endif						
	}

    
	if (res == true)																// Get next data block to send
	{
		while((*on_tx_data)(&pCLI_If->XModem.Packet.Struct.PayLoad[0], sizeof(pCLI_If->XModem.Packet.Struct.PayLoad), (uint32_t *) &StartAddress, (size_t *)&Length))		// read data and copy to pCLI_If->XModem.packet.data[0]. False if no new data
		{
			retry = XMODEM_CFG_MAX_RETRIES;											// Try sending error packet until error count is exceeded
			while(retry != 0)
			{
				xmodem_tx_packet(pCLI_If);											// Send packet
				
				//systmr_start(&pCLI_If->tmr, SYSTMR_MS_TO_TICKS(XMODEM_TIMEOUT_MS));	// Wait for a response (ACK, NAK or C)
				
				if(lib_CLI_Input_Char_Wait(pCLI_If, &pCLI_If->RxChar, 1, false, XMODEM_TIMEOUT_MS))// wait for received char
				{
					if(pCLI_If->RxChar == XMODEM_ACK) {res = true; break;}			// Alles gutte.....
					if(pCLI_If->RxChar == XMODEM_NAK) {res = true; break;}			// Alles gutte.....
					if(pCLI_If->RxChar == XMODEM_C) {res = true; break;}			// Alles gutte.....
					if(pCLI_If->RxChar == XMODEM_CAN) 								// CANCEL was received
					{
						res = false; 
#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)
						dbgprint("\r\n("LIB_CLI_OBJECTNAME") ID[%s]: XMODEM Send - CANCELLED!", pCLI_If->Name);
#endif						
						break;
					}
#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)
					dbgprint("\r\n("LIB_CLI_OBJECTNAME") ID[%s]: XMODEM Send - No ACK! PacketNr:%d", pCLI_If->Name, pCLI_If->XModem.packet_nr);
#endif						
				}
				
				retry--;            												// Decrement retry count
			}

			if(retry == 0)															// retry exceeded? - ABORT
			{	
				res = false; 
#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)
					dbgprint("\r\n("LIB_CLI_OBJECTNAME") ID[%s]: XMODEM Send - Retry count reached! PacketNr:%d", pCLI_If->Name, pCLI_If->XModem.packet_nr);
#endif						
				break;
			}

			pCLI_If->XModem.packet_nr ++;											// Next packet number
		}
	}
	
    if(res == true)																	// Finish transfer by sending EOT ("End Of Transfer")
	{
		retry = XMODEM_CFG_MAX_RETRIES;
		while(retry != 0)
		{
			lib_CLI_Output_Char( pCLI_If, XMODEM_EOT);								// Send "End Of Transfer"
			//systmr_start(&pCLI_If->tmr, SYSTMR_MS_TO_TICKS(XMODEM_TIMEOUT_MS));	// Wait for response
			
			if(lib_CLI_Input_Char_Wait(pCLI_If, &pCLI_If->RxChar, 1, false, XMODEM_TIMEOUT_MS))
			{
				if(pCLI_If->RxChar == XMODEM_ACK) {res = true;	break;}				// DONE!
				if(pCLI_If->RxChar == XMODEM_CAN) 									// CANCEL was received
				{
					res = false; 
#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)
					dbgprint("\r\n("LIB_CLI_OBJECTNAME") ID[%s]: XMODEM Send - CANCELLED!", pCLI_If->Name);
#endif						
					break;
				}

#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)
					dbgprint("\r\n("LIB_CLI_OBJECTNAME") ID[%s]: XMODEM Send - No ACK after EOT!", pCLI_If->Name);
#endif						
			}
			retry--;
		}
	}


	pCLI_If->ConnectedDrv->pAPI->Events = SavedEventRoutine;					// vrat event system

	return(res);
}
#endif












// ******************************************************************************************************
// 															VT100
// ******************************************************************************************************
static uint8_t vt100_state;

// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
void vt100_init( lib_CLI_If_t* const pCLI_If)
{
    vt100_state = 0;																// Reset state

    lib_CLI_Output_Char( pCLI_If, VT100_CHAR_ESC);									// Reset terminal
	lib_CLI_Output_Char( pCLI_If, 'c');
    lib_CLI_Output_Char( pCLI_If, VT100_CHAR_ESC);									// Enable line wrap
	lib_CLI_Output_Char( pCLI_If, '[');
	lib_CLI_Output_Char( pCLI_If, '7');
	lib_CLI_Output_Char( pCLI_If, 'h');
    Delay_ms(100);																	// Wait until Terminal has reset
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
vt100_state_t vt100_on_rx_char(char data)
{
    switch(vt100_state)
    {
    case 0:
        if(data == VT100_CHAR_ESC)													// Escape sequence detected
        {
            vt100_state++;
            return VT100_ESC_SEQ_BUSY;												// Indicate that received character should be discarded
        }
        if((uint8_t)data >= 0x80)															// Invalid character received
        {
            return VT100_CHAR_INVALID;            
        }
        return VT100_CHAR_NORMAL;        											// Normal character received

    case 1:
        if(data == '[')																// Escape sequence detected
        {
            vt100_state++;
            return VT100_ESC_SEQ_BUSY;												// Indicate that received character should be ignored
        }
        vt100_state = 0;															// Incorrect escape sequence
        return VT100_ESC_SEQ_BUSY;													// Indicate that received character should be ignored
    case 2:
        vt100_state = 0;															// Reset state first
        switch(data)																// Detect sequence
        {
        case 'A': return VT100_ESC_SEQ_ARROW_UP;
        case 'B': return VT100_ESC_SEQ_ARROW_DN;
        case 'C': return VT100_ESC_SEQ_ARROW_LEFT;
        case 'D': return VT100_ESC_SEQ_ARROW_RIGHT;
        default:  return VT100_CHAR_INVALID;
        }

    default:
        vt100_state = 0;															// Reset state
        return VT100_CHAR_INVALID;													// Indicate that received character should be discarded
    }
}

// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
void vt100_clr_screen(lib_CLI_If_t* const pCLI_If)
{
	lib_CLI_Output_Char( pCLI_If, VT100_CHAR_ESC);
	lib_CLI_Output_Char( pCLI_If, '[');
	lib_CLI_Output_Char( pCLI_If, '2');
	lib_CLI_Output_Char( pCLI_If, 'J');
}

// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
void vt100_erase_line(lib_CLI_If_t* const pCLI_If)
{
	lib_CLI_Output_Char( pCLI_If, VT100_CHAR_ESC);
	lib_CLI_Output_Char( pCLI_If, '[');
    lib_CLI_Output_Char( pCLI_If, '2');
    lib_CLI_Output_Char( pCLI_If, 'K');
}

// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
void vt100_del_chars(lib_CLI_If_t* const pCLI_If, uint8_t nr_of_chars)
{
    while(nr_of_chars != 0)
    {
		lib_CLI_Output_Char( pCLI_If, VT100_CHAR_BS);
		lib_CLI_Output_Char( pCLI_If, ' ');
		lib_CLI_Output_Char( pCLI_If, VT100_CHAR_BS);
        nr_of_chars--;
    }
}



// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static bool cli_cmd_get_item(const cli_cmd_list_item_t * item)
{
    cli_cmd_list_item = item;
    if(cli_cmd_list_item->cmdgr.cmd == NULL)										// End of list?
    {
        return false;
    }
    else
    {
        return true;
    }
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static bool cli_cmd_item_get_root(void)
{
    cli_tree_index = 0;
    cli_tree[0] = cli_cmd_list;
    return cli_cmd_get_item(cli_tree[0]);
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static bool cli_cmd_item_get_parent(void)
{
    if(cli_tree_index == 0)															// Already in root list?
    {
        dbgprint("Already in root");
        return false;
    }
    cli_tree_index--;																// Go back to parent list
    return cli_cmd_get_item(cli_tree[cli_tree_index]);
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static bool cli_cmd_item_get_child(void)
{
    if((cli_cmd_list_item->cmdgr.cmd == NULL) || (cli_cmd_list_item->handler != NULL))	// End of list or command item?
    {
        dbgprint("Not a group item");
        return false;
    }

    if(cli_tree_index >= (CLI_CFG_TREE_DEPTH_MAX-1))								// Maximum depth reached?
    {
        dbgprint("Maximum command depth exceeded");
        return false;
    }

    cli_tree_index++;																// Go to child list
    cli_tree[cli_tree_index] = cli_cmd_list_item->cmdgr.group->list;

    return cli_cmd_get_item(cli_tree[cli_tree_index]);
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static bool cli_cmd_item_get_first(void)
{
    if(cli_tree_index == 0)															// Root list?
    {
        cli_tree[0] = cli_cmd_list;													// Reset to start of root list
    }
    else
    {
        cli_cmd_get_item(cli_tree[cli_tree_index-1]);								// Get parent item
        cli_tree[cli_tree_index] = cli_cmd_list_item->cmdgr.group->list;			// Reset to start of list
    }
    return cli_cmd_get_item(cli_tree[cli_tree_index]);
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static bool cli_cmd_item_get_next(void)
{
    if(cli_cmd_list_item->cmdgr.cmd == NULL)										// End of list reached?
    {
        dbgprint("End of list already reached");
        return false;
    }
    cli_tree[cli_tree_index]++;														// Next item in list
    return cli_cmd_get_item(cli_tree[cli_tree_index]);
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static uint8_t cli_cmd_line_to_args(void)
{
    uint8_t   argc;
    char * str;
    bool quote_flag;

    cli_line_buf[cli_line_buf_index] = '\0';										// Zero terminate command line

    
    //   Break command line string up into separate words:
    //   Array of pointers to zero terminated strings
    argc       = 0;
    str        = cli_line_buf;
    quote_flag = false;
    while(true)
    {
        while(*str == ' ')															// Replace spaces with zero termination
        {
            *str++ = '\0';
        }
        
        if(*str == '\0')															// End of line?
        {
            break;
        }
        
        if(argc >= CLI_CFG_ARGV_MAX)												// Maximum number of arguments exceeded?
        {
            break;
        }
        
        if(*str == '"')																// Starts with a quote?
        {
            str++;																	// Discard quote character
            quote_flag = true;														// Set flag to indicate that parameter consists of a quoted string
        }
        cli_argv[argc++] = str;														// Save start of word and increment argument count

        if(quote_flag)																// Quoted string?
        {
            while((*str != '"') && (*str != '\0'))									// Move to end of quote
            {
                str++;
            }
            if(*str == '"')															// End quote found?
            {
                *str = '\0';														// Replace with zero termination
                str++;																// Next character
            }
        }

        while((*str != ' ') && (*str != '\0'))										// Move to end of word
        {
            str++;
        }
    }

    return argc;
}


#if CLI_CFG_HISTORY_SIZE
// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static cli_hist_size_t cli_hist_circ_buf_index_next(cli_hist_size_t index)
{
    if(index >= (CLI_CFG_HISTORY_SIZE-1))
    {
        return 0;
    }
    else
    {
        return (index+1);
    }
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static cli_hist_size_t cli_hist_circ_buf_index_prev(cli_hist_size_t index)
{
    if(index == 0)
    {
        return (CLI_CFG_HISTORY_SIZE-1);
    }
    else
    {
        return (index-1);
    }
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static void cli_hist_copy(lib_CLI_If_t* const pCLI_If)
{
    uint8_t            data;
    cli_hist_size_t i;
    cli_hist_size_t j;

    vt100_del_chars(pCLI_If, cli_line_buf_index);									// Delete old command from terminal

    i = 0;																			// Copy characters from history to command line
    j = cli_hist_index_now;
    while(true)
    {
        data = cli_hist_circ_buf[j];												// Fetch character from history
        if(data == '\0')															// End reached?
        {
            break;
        }
        lib_CLI_Output_Char(pCLI_If, data);											// Send character to terminal
        cli_line_buf[i++] = data;													// Copy character to cmd line buffer
        j = cli_hist_circ_buf_index_next(j);										// Next index
    }
    cli_line_buf_index = i;
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static void cli_hist_save_cmd(void)
{
    cli_hist_size_t i;
    cli_hist_size_t j;

    if(cli_line_buf_index == 0)														// Empty command?
    {
        cli_hist_index_now = cli_hist_index_last;									// Reset up/down history to end of latest saved command
        return;
    }

    i = cli_line_buf_index;															// Duplicate command?
    j = cli_hist_index_last;
    while(true)
    {
        i--;																		// Previous index
        j = cli_hist_circ_buf_index_prev(j);

        if(cli_line_buf[i] != cli_hist_circ_buf[j])									// No match?
        {
            break;																	// New command
        }
        if(i == 0)																	// Complete match?
        {
            cli_hist_index_now = cli_hist_index_last;								// Duplicate command... reset up/down history
            return;
        }
    }

    // Append command line string (except terminating zero) in history circular 
    // buffer
    i = 0;
    j = cli_hist_index_last;
    do
    {
        j = cli_hist_circ_buf_index_next(j);										// Next index
        cli_hist_circ_buf[j] = cli_line_buf[i++];        							// Append character from line buffer
    }
    while(i < cli_line_buf_index);

    j = cli_hist_circ_buf_index_next(j);											// Remember end of last saved string
    cli_hist_index_last = j;
    cli_hist_index_now = j;															// Reset up/down history to end of latest saved command

     //  Zero terminate and eat remaining characters of oldest command line in
     //  history that may have been partially overwritten.
    while(true)
    {
        if(cli_hist_circ_buf[j] == '\0')											// Terminating zero reached?
        {
            break;																	// Stop
        }
        else
        {
            cli_hist_circ_buf[j] = '\0';											// Reset to zero
        }
        j = cli_hist_circ_buf_index_next(j);										// Next index
    }
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static void cli_hist_load_older_cmd(lib_CLI_If_t* const pCLI_If)
{
    cli_hist_size_t i;

    i = cli_hist_circ_buf_index_prev(cli_hist_index_now);							// Oldest in history already displayed?
    if(i == cli_hist_index_last)
    {
        return;
    }
    i = cli_hist_circ_buf_index_prev(i);
    if(cli_hist_circ_buf[i] == '\0')
    {
        return;
    }

    while(cli_hist_circ_buf[i] != '\0')												// Find start of older cmd saved in history
    {
        i = cli_hist_circ_buf_index_prev(i);
    }
    cli_hist_index_now = cli_hist_circ_buf_index_next(i);
    cli_hist_copy(pCLI_If);    														// Replace current command line with one stored in history
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static void cli_hist_load_newer_cmd(lib_CLI_If_t* const pCLI_If)
{
    cli_hist_size_t i;

    i = cli_hist_index_now;															// Find start of newer cmd saved in history
    while(cli_hist_circ_buf[i] != '\0')
    {
        i = cli_hist_circ_buf_index_next(i);
    }

    if(i != cli_hist_index_last)													// Newest command already displayed?
    {
        i = cli_hist_circ_buf_index_next(i);										// Move index to start of string
    }
    cli_hist_index_now = i;
    cli_hist_copy(pCLI_If);    														// Replace current command line with one stored in history
}
#endif


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static void cli_autocomplete_reset(void)
{
    cli_autocomplete_start_index = 0; 												// Reset autocomplete to last typed character
    cli_autocomplete_end_index   = cli_line_buf_index;

    cli_cmd_item_get_root();														// Start at beginning of list
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static bool cli_autocomplete(lib_CLI_If_t* const pCLI_If)
{
    uint8_t         i;
    const char * name;
    const cli_cmd_list_item_t * cmd_start = cli_tree[cli_tree_index];

    i = cli_autocomplete_start_index;
    while(true)
    {
        name = cli_cmd_list_item->cmdgr.cmd->name;

        while(i < cli_autocomplete_end_index)										// Does name match line?
        {
            char line_char = cli_line_buf[i++];										// Fetch line character
            char name_char = *name++;												// Fetch name character

            if((name_char == '\0') && (line_char == ' '))							// End of name *and* end of word reached?
            {
                if(cli_cmd_list_item->handler == NULL)								// Name match... is this a group item?
                {
                    cli_cmd_item_get_child();										// Proceed to child name
                    name = cli_cmd_list_item->cmdgr.cmd->name;
                    cli_autocomplete_start_index = i;								// Set start index to start of child command
                    break;
                }
                else
                {
                    return false;													// This is a command item... no match
                }
            }
            else if(line_char != name_char)											// String and name character mismatch?
            {
                i = cli_autocomplete_start_index;									// No match.. reset to start of word
                if(!cli_cmd_item_get_next())										// Proceed to next item
                {
                    cli_cmd_item_get_first();										// Go back to start of list
                }
                i = cli_autocomplete_start_index;									// Reset to start of word
                break;
            }
        }

        if(i >= cli_autocomplete_end_index)											// Autocomplete match reached?        
		{
            break;																	// (Partial) match
        }

        if(cli_tree[cli_tree_index] == cmd_start)									// Cycled through list?
        {
            return false;															// No match
        }
    }

    vt100_del_chars(pCLI_If, cli_line_buf_index-cli_autocomplete_end_index);		// Autocomplete rest of name
    while(true)
    {
        char name_char = *name++;
        if(name_char == '\0')
        {
            break;
        }
        if(i >= (CLI_CFG_LINE_LENGTH_MAX-1))
        {
            break;
        }
        cli_line_buf[i++] = name_char;
		lib_CLI_Output_Char(pCLI_If, name_char);
    }
    cli_line_buf_index = i;

    if(!cli_cmd_item_get_next())													// Next item in list for next autocomplete
    {
        cli_cmd_item_get_first();            										// Go back to start of list
    }

    return true;
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
static void cli_cmd_exe(lib_CLI_If_t* const pCLI_If)
{
    uint8_t         argc;
    char **      argv;
    const char * report_str;

    //   Break command line string up into separate words:
    //   Array of pointers to zero terminated strings
    argc = cli_cmd_line_to_args();

    if(argc == 0)																	// Ignore empty command
    {
        return;
    }

    if(cli_argv[0][0] == '#')														// Ignore command starting with a hash (#) as it is regarded as a comment
    {
        return;
    }
	
	if(cli_argv[0][0] == VT100_CHAR_QUESTMARK)										// Received '?' character?
	{
		lib_CLI_Output_String( pCLI_If, "\r\nAvailable commands are:\n\r");
		cli_cmd_cmdlist_fn(pCLI_If, 0, NULL);										// Print commands list
	}
	else																			// else find received command in command list
	{
		cli_cmd_item_get_root();													// Find command in command list
		while(true)
		{
			if((cli_cmd_list_item->cmdgr.cmd == NULL) || (cli_tree_index >= argc))	// End of list or not enough arguments?
			{
				lib_CLI_Output_String(pCLI_If, "Unknown command!\n\r");					// Command not found in list
				return;
			}

			if(strcmp(cli_argv[cli_tree_index], cli_cmd_list_item->cmdgr.cmd->name) == 0)	// Does the argument match the command string?
			{
				if(cli_cmd_list_item->handler != NULL)								// Is this a command item?
				{
					break;															// Command match
				}
				else
				{
					cli_cmd_item_get_child();										// Group item match... proceed to child list
				}
			}
			else
			{
				cli_cmd_item_get_next();											// Next item in list
			}
		}

		argc -= (cli_tree_index+1);													// Remove command argument(s)
		argv  = &cli_argv[cli_tree_index+1];

		if((argc < cli_cmd_list_item->cmdgr.cmd->argc_min) ||(argc > cli_cmd_list_item->cmdgr.cmd->argc_max)) // Does number of parameters exceed bounds?
		{
			lib_CLI_Output_String( pCLI_If, "Wrong number of arguments!\n\r");
			return;
		}
		report_str = (*(cli_cmd_list_item->handler))(pCLI_If, argc, argv);			// Execute command with parameters
		if(report_str != NULL)														// Did handler report a string to display?
		{
			lib_CLI_Output_String(pCLI_If, report_str);								// Display string
			lib_CLI_Output_String( pCLI_If, "\n\r");								// Append newline character
		}
	}
}


// ------------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------------
void cli_init(lib_CLI_If_t* const pCLI_If, const char* startup_str)
{
#if CLI_CFG_HISTORY_SIZE

#if (CLI_CFG_HISTORY_SIZE < 256)
    uint8_t i;
#elif (CLI_CFG_HISTORY_SIZE < 65536)
    uint16_t i;
#else    
    uint32_t i;
#endif

    for(i=0; i<CLI_CFG_HISTORY_SIZE; i++)											// Clear history buffer
    {
        cli_hist_circ_buf[i] = '\0';
    }
    cli_hist_index_last = 0;
    cli_hist_index_now  = 0;
#endif

    cli_line_buf_index = 0;															// Reset
    cli_autocomplete_reset();

    vt100_init(pCLI_If);    														// Reset Terminal

    if(startup_str != NULL)															// Display startup string
    {
		lib_CLI_Output_String(pCLI_If, startup_str);
		lib_CLI_Output_String(pCLI_If, CLI_CFG_STARTUPNAME);
    }

	lib_CLI_Output_String( pCLI_If, "\r\n------------------------------------------------------");
	
	lib_CLI_Output_String( pCLI_If, "\r\nType '?' to get list of commands or type 'help' to get with help descriptions\n\r"	// Display start up help advice
			"- TAB   to cycle autocomplete cmd(s)\n\r"
#if CLI_CFG_HISTORY_SIZE
			"- UP/DN to traverse history of cmds entered\n\r"
#endif
			"- ENTER to execute cmd\n\r"
			" ex: cmd <required argument> [optional argument] \n\r");
 
// curly braces {default values}
 // parenthesis (miscellaneous info)	

	lib_CLI_Output_Char( pCLI_If, '>');												// Display prompt
}


// ------------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------------
void cli_on_rx_char(lib_CLI_If_t* const pCLI_If, char data)
{
    switch(vt100_on_rx_char(data))													// Process received character to detect ANSI Escape Sequences
    {
    case VT100_CHAR_NORMAL:
        switch(data)
        {
        case VT100_CHAR_CR:															// ENTER has been pressed
			lib_CLI_Output_Char( pCLI_If, '\n');									// Terminate line
			lib_CLI_Output_Char( pCLI_If, '\r');
#if CLI_CFG_HISTORY_SIZE
            cli_hist_save_cmd();													// Save command
#endif
			cli_cmd_exe(pCLI_If);													// Execute command
            cli_line_buf_index = 0;													// Reset command buffer
            cli_autocomplete_reset();												// Reset autocomplete
			lib_CLI_Output_Char( pCLI_If, '>');										// Display prompt
            return;
        
        case VT100_CHAR_BS:
            if(cli_line_buf_index > 0)												// Buffer not empty?
            {
                cli_line_buf_index--;												// Remove last character from buffer
                vt100_del_chars(pCLI_If, 1);										// Remove last character from terminal screen
                cli_autocomplete_reset();											// Reset autocomplete to last character
            }
            else
            {
				lib_CLI_Output_Char( pCLI_If, VT100_CHAR_BEL);						// No characters to delete
            }
            return;
    
        case VT100_CHAR_TAB:														// TAB has been pressed
            if(!cli_autocomplete(pCLI_If))
            {
				lib_CLI_Output_Char( pCLI_If, VT100_CHAR_BEL);						// Autocomplete failed
            }
            return;

        default:
            break;
        }

        if(data < 0x20)																// Ignore invalid values
        {
            return;
        }
        
        if(cli_line_buf_index < (CLI_CFG_LINE_LENGTH_MAX-1))						// Buffer not full?
        {
            cli_line_buf[cli_line_buf_index++] = data;								// Add character to line buffer
            cli_autocomplete_reset();												// Reset autocomplete to last character
			lib_CLI_Output_Char( pCLI_If, data);									// Echo character
        }
        else
        {
			lib_CLI_Output_Char( pCLI_If, VT100_CHAR_BEL);							// Buffer full
        }
        return;

    case VT100_CHAR_INVALID:														// Discard/ignore character
        return;
    case VT100_ESC_SEQ_BUSY:														// Discard/ignore character
        return;
#if CLI_CFG_HISTORY_SIZE
    case VT100_ESC_SEQ_ARROW_UP:
        cli_hist_load_older_cmd(pCLI_If);
        return;
    case VT100_ESC_SEQ_ARROW_DN:
        cli_hist_load_newer_cmd(pCLI_If);
        return;
#endif
    default:
        return;
    }    
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
const char* cli_cmd_help_fn(lib_CLI_If_t* const pCLI_If, uint8_t argc, char* argv[])
{
    uint8_t   i;
    uint8_t   len;
    bool line_break;



#if ((CLI_CFG_NAME_STR_MAX_SIZE == 0) || (CLI_CFG_PARAM_STR_MAX_SIZE == 0))
    uint8_t   name_char_cnt;
    uint8_t   param_char_cnt;
    
	// Find longest command and param length
    name_char_cnt  = CLI_CFG_NAME_STR_MAX_SIZE;
    param_char_cnt = CLI_CFG_PARAM_STR_MAX_SIZE;
	
    cli_cmd_item_get_root();
    while(true)
    {
        // End of list?
        if(cli_cmd_list_item->cmd == NULL)
        {
            // Root list?
            if(cli_tree_index == 0)
            {
                // The end has been reached
                break;
            }
            else
            {
                // Return to parent list
                cli_cmd_item_get_parent();
                // Next item
                cli_cmd_item_get_next();
                continue;
            }
        }

        // Is this a command item?
        if(cli_cmd_list_item->handler != NULL)
        {
            // Longest command string?
            len = 0;
            for(i=0; i<=cli_tree_index; i++)
            {
                cli_cmd_get_item(cli_tree[i]);
                len += strlen(cli_cmd_list_item->cmd->name) + 1;
            }
            if(name_char_cnt < len)
            {
                // Remember longest command string
                name_char_cnt = len;
            }
            // Longest param string?
            len = strlen(cli_cmd_list_item->cmd->param);
            if(param_char_cnt < len)
            {
                // Remember longest param string
                param_char_cnt = len;
            }

            // Next item in list
            cli_cmd_item_get_next();
        }
        else
        {
            // Group item... proceed to child list
            cli_cmd_item_get_child();
        }        
    }
    DBG_INFO("Max command chars = %d", name_char_cnt);
    DBG_INFO("Max param chars = %d", param_char_cnt);
#endif

    cli_cmd_item_get_root();														// Display help for each command in list
    line_break = false;
    while(true)
    {
        if(cli_cmd_list_item->cmdgr.cmd == NULL)									// End of list?
        {
            if(cli_tree_index == 0)													// Root list?
            {
                break;																// The end has been reached
            }
            else
            {
                cli_cmd_item_get_parent();											// Return to parent list
                cli_cmd_item_get_next();											// Next item
                line_break = true;													// Insert line break
                continue;
            }
        }

        if(cli_cmd_list_item->handler != NULL)										// Is this a command item?
        {
            cli_cmd_get_item(cli_tree[0]);
            if((argc == 0) || (strncmp(argv[0], cli_cmd_list_item->cmdgr.cmd->name, strlen(argv[0])) == 0))
            {
                if((argc == 0) && (line_break))										// Insert line break?
                {
                    line_break = false;
					lib_CLI_Output_Char( pCLI_If, '\n');
					lib_CLI_Output_Char( pCLI_If, '\r');
                }

                len = 0;															// Display all command strings
                for(i=0; i<=cli_tree_index; i++)
                {
                    cli_cmd_get_item(cli_tree[i]);									// Display name
					lib_CLI_Output_String( pCLI_If, cli_cmd_list_item->cmdgr.cmd->name);
					lib_CLI_Output_Char( pCLI_If, ' ');
                    len += strlen(cli_cmd_list_item->cmdgr.cmd->name) + 1;
                }
    
				lib_CLI_Output_String( pCLI_If, cli_cmd_list_item->cmdgr.cmd->param);	// Display param
				lib_CLI_Output_String( pCLI_If, "    : ");
				lib_CLI_Output_String( pCLI_If, cli_cmd_list_item->cmdgr.cmd->help);	// Display help string
				lib_CLI_Output_String( pCLI_If, "\n\r");								// Append newline character            
			}
            cli_cmd_item_get_next();												// Next item in list
        }
        else
        {
            cli_cmd_item_get_child();												// Group item... proceed to child list
            line_break = true;														// Insert line break;
        }
    }

    return NULL;
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
const char* cli_cmd_cmdlist_fn(lib_CLI_If_t* const pCLI_If, uint8_t argc, char* argv[])
{
    uint8_t   i;
    uint8_t   len;
    bool line_break;


#if ((CLI_CFG_NAME_STR_MAX_SIZE == 0) || (CLI_CFG_PARAM_STR_MAX_SIZE == 0))
    uint8_t   name_char_cnt;
    uint8_t   param_char_cnt;
    name_char_cnt  = CLI_CFG_NAME_STR_MAX_SIZE;										// Find longest command and param length
    param_char_cnt = CLI_CFG_PARAM_STR_MAX_SIZE;
	
    cli_cmd_item_get_root();
    while(true)
    {
        // End of list?
        if(cli_cmd_list_item->cmd == NULL)
        {
            // Root list?
            if(cli_tree_index == 0)
            {
                // The end has been reached
                break;
            }
            else
            {
                // Return to parent list
                cli_cmd_item_get_parent();
                // Next item
                cli_cmd_item_get_next();
                continue;
            }
        }

        // Is this a command item?
        if(cli_cmd_list_item->handler != NULL)
        {
            // Longest command string?
            len = 0;
            for(i=0; i<=cli_tree_index; i++)
            {
                cli_cmd_get_item(cli_tree[i]);
                len += strlen(cli_cmd_list_item->cmd->name) + 1;
            }
            if(name_char_cnt < len)
            {
                // Remember longest command string
                name_char_cnt = len;
            }
            // Longest param string?
            len = strlen(cli_cmd_list_item->cmd->param);
            if(param_char_cnt < len)
            {
                // Remember longest param string
                param_char_cnt = len;
            }

            // Next item in list
            cli_cmd_item_get_next();
        }
        else
        {
            // Group item... proceed to child list
            cli_cmd_item_get_child();
        }        
    }
    DBG_INFO("Max command chars = %d", name_char_cnt);
    DBG_INFO("Max param chars = %d", param_char_cnt);
#endif

    cli_cmd_item_get_root();														// Display help for each command in list
    line_break = false;
    while(true)
    {
        if(cli_cmd_list_item->cmdgr.cmd == NULL)									// End of list?
        {
            if(cli_tree_index == 0)													// Root list?
            {
                break;																// The end has been reached
            }
            else
            {
                cli_cmd_item_get_parent();											// Return to parent list
                cli_cmd_item_get_next();											// Next item
                line_break = true;													// Insert line break
                continue;
            }
        }

        if(cli_cmd_list_item->handler != NULL)										// Is this a command item?
        {
            cli_cmd_get_item(cli_tree[0]);
            if((argc == 0) || (strncmp(argv[0], cli_cmd_list_item->cmdgr.cmd->name, strlen(argv[0])) == 0))
            {
                if((argc == 0) && (line_break))										// Insert line break?
                {
                    line_break = false;
					lib_CLI_Output_Char( pCLI_If, '\n');
					lib_CLI_Output_Char( pCLI_If, '\r');
                }

                len = 0;															// Display all command strings
                for(i=0; i<=cli_tree_index; i++)
                {
                    cli_cmd_get_item(cli_tree[i]);									// Display name
					lib_CLI_Output_String( pCLI_If, cli_cmd_list_item->cmdgr.cmd->name);
					lib_CLI_Output_Char( pCLI_If, ' ');
                    len += strlen(cli_cmd_list_item->cmdgr.cmd->name) + 1;
                }
				lib_CLI_Output_String( pCLI_If, "\n\r");								// Append newline character
            }
            cli_cmd_item_get_next();												// Next item in list
        }
        else
        {
            cli_cmd_item_get_child();												// Group item... proceed to child list
            line_break = true;														// Insert line break;
        }
    }

    return NULL;
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
uint8_t cli_util_argv_to_option(uint8_t argv_index, const char* options)
{
    uint8_t index = 0;

    argv_index += cli_tree_index+1;													// Adjust index

    while(strlen(options) != 0)
    {
        if(strcmp(cli_argv[argv_index], options) == 0)
        {
            return index;
        }
        options += strlen(options) + 1;
        index++;
    }

    return 0xff;
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
bool cli_util_argv_to_u8(uint8_t argv_index, uint8_t min, uint8_t max)
{
    unsigned long i;
    char *end;

    argv_index += cli_tree_index+1;													// Adjust index
    i = strtoul(cli_argv[argv_index], &end, 0);

    if((end == cli_argv[argv_index]) || (*end != '\0'))
    {
        return false;
    }
    if((i >= min) && (i <= max) )
    {
        cli_argv_val.u8 = (uint8_t)i;
        return true;
    }
    return false;
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
bool cli_util_argv_to_u16(uint8_t argv_index, uint16_t min, uint16_t max)
{
    unsigned long i;
    char *end;

    argv_index += cli_tree_index+1;													// Adjust index

    i = strtoul(cli_argv[argv_index], &end, 0);

    if(  (end == cli_argv[argv_index]) || (*end != '\0')  )
    {
        return false;
    }
    if((i >= min) && (i <= max) )
    {
        cli_argv_val.u16 = (uint16_t)i;
        return true;
    }
    return false;
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
bool cli_util_argv_to_u32(uint8_t argv_index, uint32_t min, uint32_t max)
{
    unsigned long i;
    char *end;

    argv_index += cli_tree_index+1;													// Adjust index

    i = strtoul(cli_argv[argv_index], &end, 0);

    if(  (end == cli_argv[argv_index]) || (*end != '\0')  )
    {
        return false;
    }
    if((i >= min) && (i <= max) )
    {
        cli_argv_val.u32 = (uint32_t)i;
        return true;
    }
    return false;
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
bool cli_util_argv_to_s8(uint8_t argv_index, int8_t min, int8_t max)
{
    long i;
    char *end;

    argv_index += cli_tree_index+1;													// Adjust index

    i = strtol(cli_argv[argv_index], &end, 0);

    if(  (end == cli_argv[argv_index]) || (*end != '\0')  )
    {
        return false;
    }
    if((i >= min) && (i <= max) )
    {
        cli_argv_val.s8 = (int8_t)i;
        return true;
    }
    return false;
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
bool cli_util_argv_to_s16(uint8_t argv_index, int16_t min, int16_t max)
{
    long i;
    char *end;

    argv_index += cli_tree_index+1;													// Adjust index
    i = strtol(cli_argv[argv_index], &end, 0);

    if(  (end == cli_argv[argv_index]) || (*end != '\0')  )
    {
        return false;
    }
    if((i >= min) && (i <= max) )
    {
        cli_argv_val.s16 = (int16_t)i;
        return true;
    }
    return false;
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
bool cli_util_argv_to_s32(uint8_t argv_index, int32_t min, int32_t max)
{
    long i;
    char *end;

    argv_index += cli_tree_index+1;													// Adjust index

    i = strtol(cli_argv[argv_index], &end, 0);

    if(  (end == cli_argv[argv_index]) || (*end != '\0')  )
    {
        return false;
    }
    if((i >= min) && (i <= max) )
    {
        cli_argv_val.s32 = (int32_t)i;
        return true;
    }
    return false;
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
bool cli_util_argv_to_float(uint8_t argv_index)
{
    double i;
    char *end;

    argv_index += cli_tree_index+1;													// Adjust index

    i = strtod(cli_argv[argv_index], &end);

    if(  (end == cli_argv[argv_index]) || (*end != '\0')  )
    {
        return false;
    }
    cli_argv_val.f = (float)i;
    return true;
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
bool cli_util_argv_to_double(uint8_t argv_index)
{
    double i;
    char *end;

    argv_index += cli_tree_index+1;													// Adjust index

    i = strtod(cli_argv[argv_index], &end);

    if(  (end == cli_argv[argv_index]) || (*end != '\0')  )
    {
        return false;
    }
    cli_argv_val.d = i;
    return true;
}


// ------------------------------------------------------------------------------------------------------
// Display buffer of data
// ------------------------------------------------------------------------------------------------------
void cli_util_disp_buf(lib_CLI_If_t* const pCLI_If, const uint8_t *data, size_t nr_of_bytes, uint32_t address)
{
    size_t i, j;
    char Buff[14];
	
    for(i=0; i<nr_of_bytes; i+= 16)
    {
		
		sprintf(Buff, "0x%08X ", address + i);										// print address
		lib_CLI_Output_String( pCLI_If, Buff);
		
        for(j=i; j<(i+16); j++)
        {
            if(j<nr_of_bytes)
            {
				sprintf(Buff, "%02X ", data[j]);
				lib_CLI_Output_String( pCLI_If, Buff);
            }
            else
            {
				lib_CLI_Output_String( pCLI_If, "   ");
            }
        }
        for(j=i; j<(i+16); j++)
        {
            if(j<nr_of_bytes)
            {
                if( (data[j] >= 32) && (data[j] <= 127) )
                {
					lib_CLI_Output_Char( pCLI_If, data[j]);
					
                }
                else
                {
					lib_CLI_Output_Char( pCLI_If, '.');
                }
            }
            else
            {
				lib_CLI_Output_Char( pCLI_If, ' ');
            }
        }
		lib_CLI_Output_String( pCLI_If, "\n\r");									// Append newline character
    }
}


// ******************************************************************************************************
// PUBLIC Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Initialize CLI - init and connect with some driver for IO data (e.g.: UART)
// ------------------------------------------------------------------------------------------------------
bool lib_CLI_Init (lib_CLI_If_t* const pCLI_If, _drv_If_t* pConnectedDrv)						// Reset and init CLI - allocate memory also
{
#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)
	dbgprint("\r\n("LIB_CLI_OBJECTNAME") ID[%s]: Init CLI.", pCLI_If->Name);
#endif

	if(pCLI_If == NULL) return (false);


#if defined(CONF_SYS_INFO) && (CONF_SYS_INFO == 1)
	lib_CLI_Output_String(pCLI_If, "\r\n-------------------------------------------------");
	dbgprint("\r\nApp: %s, v: %s, %s", sys_System.App->ID, sys_System.App->Ver, sys_System.App->Target);				// Par teplych vypisov
	dbgprint("\r\n +-- Compiled by %s [%s + %s] at %s", sys_System.App->Builder_Ver, sys_System.App->Builder_Parm, sys_System.App->Builder_Lib, sys_System.App->Builder_DateTime);	
	dbgprint("\r\nBSP: %s, v: %s", sys_System.App->BSP_ID, sys_System.App->BSP_Ver);		// Par teplych vypisov
#endif
	
	sys_System.Part->Clock_Refresh();													// nacitaj /pre istotu/ clock informacie o host MCU - znova, bsp ho mohlo zmenit

#if defined(CONF_SYS_PART_INFO) && (CONF_SYS_PART_INFO == 1)
	sys_System.Part->Part_Refresh();													// nacitaj ID a SERNUM informacie o host MCU
	#if defined(CONF_SYS_INFO) && (CONF_SYS_INFO == 1)
	dbgprint("\r\nClock Src: %ld, CPU: %ld", (long int) sys_System.Part->SRC_Freq, (long int) sys_System.Part->CPU_Freq);
	dbgprint("\r\nCPUID: 0x%08X", sys_System.Part->PARTID);								// vypis CPUID
	dbgprint(" @ %dkHz", (sys_System.Part->CPU_Freq) / 1000);							// aj frekvenciu na ktorej bezi
	dbgprint("\r\nCPU SERNUM: 0x%08X %08X %08X %08X", sys_System.Part->SERNUM[3], sys_System.Part->SERNUM[2], sys_System.Part->SERNUM[1], sys_System.Part->SERNUM[0] );
	#endif
#endif


	pCLI_If->ConnectedDrv = pConnectedDrv;														// set output channel (driver)
	pCLI_If->Initialized = true;
	cli_init(pCLI_If, CONF_APP_NAME_STRING);
	return(true);
}


// ------------------------------------------------------------------------------------------------------
//	block run and wait for receiving a character
// ------------------------------------------------------------------------------------------------------
bool lib_CLI_Input_Char_Wait(lib_CLI_If_t* const pCLI_If, void *DstData, uint8_t DataSize, bool PrintCountDown, const uint_fast64_t delay_in_ticks)
{
	uint16_t countdown = pCLI_If->tmr.delay_in_ticks / CONF_SYSTICK_FREQ;		// countdown in second...
	char Buff[15];
	
	systmr_start(&pCLI_If->tmr, SYSTMR_MS_TO_TICKS(delay_in_ticks));			// restart timeout timer
    // See if character has been received

    
	
	while(! pCLI_If->ConnectedDrv->pAPI_STD->Read_Data(NULL, pCLI_If->ConnectedDrv, DstData, DataSize, XFer_Blocking) )
    {
		// if(XMODEM_CFG_TMR_HAS_EXPIRED())
		if(systmr_has_expired(&pCLI_If->tmr))
		{
			return false;
		}
		
		if((sys_System.Cnts->SecondChangeFlag == true) && (PrintCountDown == true))
		{
			if(countdown)
			{
				sprintf(Buff, "\r ... %d  \r", countdown --);										// print timeout
				lib_CLI_Output_String( pCLI_If, Buff);
			}
		}
		
    }
    return true;
}

// ------------------------------------------------------------------------------------------------------
// input data into CLI and process it...
// bool lib_CLI_Input_Data(lib_CLI_If_t* const pCLI_If, uint8_t Src)
// ------------------------------------------------------------------------------------------------------
bool lib_CLI_Input_Data(lib_CLI_If_t* const pCLI_If, uint8_t Src)
{
#if defined(CONF_DEBUG_LIB_CLI) && (CONF_DEBUG_LIB_CLI == 1)
		dbgprint("\r\n("LIB_CLI_OBJECTNAME") ID[%s] Process data", pCLI_If->Name);
#endif
	
	cli_on_rx_char(pCLI_If, (char)Src);
	return(true);
}	


// ------------------------------------------------------------------------------------------------------
// output char from CLI. 
// ------------------------------------------------------------------------------------------------------
void lib_CLI_Output_Char(lib_CLI_If_t* const pCLI_If, const uint8_t Data)
{
	if(pCLI_If->ConnectedDrv == NULL) return;
	
	pCLI_If->ConnectedDrv->pAPI_STD->Write_Data(NULL, pCLI_If->ConnectedDrv,(void *) &Data, 1, XFer_Blocking);
	
}


// ------------------------------------------------------------------------------------------------------
// output string from CLI. 
// ------------------------------------------------------------------------------------------------------
void lib_CLI_Output_String(lib_CLI_If_t* const pCLI_If, const char *OutBuff)
{
	if(pCLI_If->ConnectedDrv == NULL) return;
	
	pCLI_If->ConnectedDrv->pAPI_STD->Write_Data(NULL, pCLI_If->ConnectedDrv, (void *) OutBuff, strlen(OutBuff), XFer_Blocking);
}


// ------------------------------------------------------------------------------------------------------
// output data from CLI. 
// ------------------------------------------------------------------------------------------------------
void lib_CLI_Output_Data(lib_CLI_If_t* const pCLI_If, const char *OutBuff, size_t Length)
{
	if(pCLI_If->ConnectedDrv == NULL) return;
	
	pCLI_If->ConnectedDrv->pAPI_STD->Write_Data(NULL, pCLI_If->ConnectedDrv, (void *) OutBuff, Length, XFer_Blocking);
}

#endif	// CONF_USE_LIB_CLI

