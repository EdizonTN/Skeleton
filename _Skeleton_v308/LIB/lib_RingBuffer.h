// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_RingBuffer.h
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: Circular buffer (Ring) library
//
// Notice: 
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
//

#ifndef __LIB_RINGBUFFER_H_
#define __LIB_RINGBUFFER_H_

#include "Skeleton.h"


// ******************************************************************************************************
// CONFIGURATION
// ******************************************************************************************************
#ifndef CONF_DEBUG_LIB_RINGBUFFER
#define CONF_DEBUG_LIB_RINGBUFFER		0											// Default debug is off
#endif

// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************

// library interface:
COMP_PACKED_BEGIN
typedef struct RB_If
{
	const 	char						Name[CONF_DEBUG_STRING_NAME_SIZE];			// interface name - text - only for ID
			bool						Initialized;								// Init flag
			void 						*pBuff_Start;								// link to physical memory space - start of buffer
			void						*pBuff_End;									// link to physical memory space - end of buffer (Last element)
			void	 					*pWrite;									// Head/Write
			void						*pRead;										// Tail/Read
	volatile uint32_t  					elCount;									// Number of stored element
	const 	uint32_t 					elCapacity; 								// Init parameter - max number of stored elements
	const 	uint8_t						elSize;										// Init parameter - size of one data sample in RB	
} lib_RB_If_t;
COMP_PACKED_END

// ******************************************************************************************************
// PUBLIC 
// ******************************************************************************************************
extern bool lib_RB_Init (lib_RB_If_t* const pRB_If);									// Reset and init RB - allocate memory also
extern bool lib_RB_Read_Element(lib_RB_If_t* const pRB_If, void* pDst);
extern bool lib_RB_Copy2RB(lib_RB_If_t* const pRB_If, void* pSrc, uint32_t elCount);
extern void lib_RB_Write_External(lib_RB_If_t* const pRB_If, uint32_t ElementsWrite);
extern void lib_RB_Read_External(lib_RB_If_t* const pRB_If, uint32_t ElementsRead);
extern uint32_t lib_RB_Get_Read_elCountLinear(lib_RB_If_t* const pRB_If);
extern uint32_t lib_RB_Get_elCount(lib_RB_If_t* const pRB_If);
// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Get number of occupied datas
// return: Stored Element Number
// ------------------------------------------------------------------------------------------------------
static inline uint32_t lib_RB_Get_elCount(lib_RB_If_t* const pRB_If)
{
	void *pWrite = pRB_If->pWrite;
	void *pRead = pRB_If->pRead;
	volatile uint32_t pelCount = pRB_If->elCount;
	size_t pelSize = pRB_If->elSize;
	
	if(pWrite == pRead) return(0);
		
	if(pWrite > pRead)
	{
		pelCount = (((char*)pWrite) -  (char*)pRead) / pelSize;
	}
	else
	{
		pelCount = pRB_If->elCapacity - ((((char*)pRead -  (char*)pWrite)) / pelSize);
	}
	return(pelCount);																// return count of stored elements
}


// ------------------------------------------------------------------------------------------------------
// Get number of free space in Element number
// return: Stored Element Number
// ------------------------------------------------------------------------------------------------------
inline uint32_t lib_RB_Get_FreeElement(lib_RB_If_t* const pRB_If)
{
	if(!pRB_If->Initialized) return(0);
	return(pRB_If->elCapacity - pRB_If->elCount);									// return count of free elements
}


// ------------------------------------------------------------------------------------------------------
// Is ring buffer full?
// return: true/false
// ------------------------------------------------------------------------------------------------------
inline bool lib_RB_IsFull(lib_RB_If_t* const pRB_If)
{
	if(!pRB_If->Initialized) return(true);
	if(pRB_If->elCount == (pRB_If->elCapacity - pRB_If->elSize)) return(true);		// Check for Buffer is full
	else return(false);
}


// ------------------------------------------------------------------------------------------------------
// Is ring buffer full?
// return: true/false
// ------------------------------------------------------------------------------------------------------
inline bool lib_RB_IsEmpty(lib_RB_If_t* const pRB_If)
{
	if(!pRB_If->Initialized) return(false);
	if(pRB_If->pWrite == pRB_If->pRead) return(true);								// Check for Buffer is full
	else return(false);
}


// ------------------------------------------------------------------------------------------------------
// Get Read data counter from linear area of RB. Without rotate around... possible value for DMA transfer etc. for Read from RB
// return: Element Count for linear read
// ------------------------------------------------------------------------------------------------------
static inline uint32_t lib_RB_Get_Read_elCountLinear(lib_RB_If_t* const pRB_If)
{
	if(!pRB_If->Initialized) return(0);
	void *pRead = &pRB_If->pRead;
	void *pWrite = &pRB_If->pWrite;
	size_t elSize = pRB_If->elSize;
	
	if(pWrite <= pRead) return (((char*)pRB_If->pBuff_End - (char*)pRead) / elSize);
	else return(((char*)pWrite - (char*)pRead) / elSize);
}


// ------------------------------------------------------------------------------------------------------
// Get Write data counter from linear area of RB. Without rotate around... possible value for DMA transfer etc. for Write to RB
// return: Element Count for linear write
// ------------------------------------------------------------------------------------------------------
static inline uint32_t lib_RB_Get_Write_elCountLinear(lib_RB_If_t* const pRB_If)
{
	if(!pRB_If->Initialized) return(0);
	void *pRead = &pRB_If->pRead;
	void *pWrite = &pRB_If->pWrite;
	size_t elSize = pRB_If->elSize;
	
	if(pWrite < pRead) return (((char*)pRead - (char*)pWrite) / elSize);
	else return(((char*)pRB_If->pBuff_End - (char*)pWrite) / elSize);
}


// ------------------------------------------------------------------------------------------------------
// Write one element to RB
// return: none
// ------------------------------------------------------------------------------------------------------
static inline void lib_RB_Write_Element(lib_RB_If_t* const pRB_If, void* pElement)
{
//	if(!pRB_If->Initialized) return;
	void *pWrite = pRB_If->pWrite;
	size_t elSize = pRB_If->elSize;
	
	SYS_CPUCRIT_START();
//	if(pRB_If->elCount == pRB_If->elCapacity)
//	{
//        // handle error
//  }
	memcpy(pRB_If->pWrite, pElement, elSize);
	pWrite = (char*)pWrite + elSize;												// move write pointer
    if(pWrite >= pRB_If->pBuff_End)    pWrite = pRB_If->pBuff_Start;				// rotate around...
	pRB_If->pWrite = pWrite;
	pRB_If->elCount = lib_RB_Get_elCount(pRB_If);									// read current element count
	SYS_CPUCRIT_END();
}


// ------------------------------------------------------------------------------------------------------
// return data from RB
// ------------------------------------------------------------------------------------------------------
static inline bool lib_RB_Read_Element(lib_RB_If_t* const pRB_If, void* pDst)
{
	if(!pRB_If->Initialized) return(true);
	
	if(lib_RB_Get_elCount(pRB_If) == 0)
	{
        // handle error
		return(false);																// nothing to read
    }
	SYS_CPUCRIT_START();
//	__isb(15);
    memcpy(pDst, pRB_If->pRead, pRB_If->elSize);
    pRB_If->pRead = (char*)pRB_If->pRead + pRB_If->elSize;							// move read pointer
    if(pRB_If->pRead >= pRB_If->pBuff_End) pRB_If->pRead = pRB_If->pBuff_Start;		// rotate to around
	pRB_If->elCount = lib_RB_Get_elCount(pRB_If);									// read current element count
	SYS_CPUCRIT_END();
	return(true);
}

#endif // __LIB_RINGBUFFER_H_
