// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_SWIM.c
// 	   Version: 3.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: SWIM graphic library
// ******************************************************************************
// Notice:
//
// Usage:
//			
// ToDo:
//
// Changelog:
//				2023.11.02	- v3.1 - scroll function fixed
//

#include "Skeleton.h"

#if defined(CONF_USE_LIB_SWIM) && (CONF_USE_LIB_SWIM == 1)
#define	LIB_SWIM_OBJECTNAME							"lib_SWIM"

#include <stdarg.h>

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
#define PORTRAIT			1





// ------------------------------------------------------------------------------------------------------
// STD lib Inicializacia VRM Proto kniznice ako takej.
// ------------------------------------------------------------------------------------------------------
bool lib_SWIM_Init(lib_SWIM_If_t* pLib_If, uint8_t *LCD_FB, uint32_t LCD_H_Size, uint32_t LCD_V_Size, uint8_t LCD_BPP)
{
	bool res = false;
	
#if defined(CONF_DEBUG_LIB_SWIM) && (CONF_DEBUG_LIB_SWIM == 1)
	FN_DEBUG_ENTRY(pLib_If->Name);
#endif	

	if(pLib_If == NULL) return(false);
	
	pLib_If->fblog = (COLOR_T *) LCD_FB;
	pLib_If->WidthPx = LCD_H_Size;
	pLib_If->HeightPx = LCD_V_Size;
	
	pLib_If->MainWindow.bpp =  LCD_BPP;
	pLib_If->MainWindow.memmask = 0;
	
	for( uint8_t pos=0; pos < pLib_If->MainWindow.bpp; pos++) 						// prepare mask for applying pixel by color
	{
		pLib_If->MainWindow.memmask <<= 1;
		pLib_If->MainWindow.memmask |= 1 ;											// create memory mask
	}
	
	res = swim_window_open((SWIM_WINDOW_T* ) &pLib_If->MainWindow, BSP_LCD_WIDTH, BSP_LCD_HEIGHT, pLib_If->fblog, 0, 0, BSP_LCD_WIDTH-3, BSP_LCD_HEIGHT-3, 3, WHITE, BLACK, WHITE);
	
#if defined(CONF_DEBUG_LIB_SWIM) && (CONF_DEBUG_LIB_SWIM == 1)
    FN_DEBUG_EXIT(pLib_If->Name)													// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	pLib_If->Stat.Initialized = res;
	pLib_If->Stat.Loaded = res;
	pLib_If->Stat.Enabled = res;
	return(res);
}



// ------------------------------------------------------------------------------------------------------
// 	Create a new floating Windows. Main win must exists !
lib_SWIM_WIN_If_t *lib_SWIM_Create_Win(lib_SWIM_If_t* pLib_If, int32_t xwin_min, int32_t ywin_min, int32_t xwin_max, \
						 int32_t ywin_max, int32_t border_width, COLOR_T pcolor, COLOR_T bkcolor, COLOR_T fcolor)
{
	bool res;
	lib_SWIM_WIN_If_t *Result;
	
	Result = sys_malloc_zero( sizeof(lib_SWIM_WIN_If_t));							// allocate mem for struct
	
	if(Result)
	{
		//Result->ThisWin.memmask = Result->pRoot_If->MainWindow.memmask;			// inherit mask from root
		//Result->ThisWin.bpp = Result->pRoot_If->MainWindow.bpp;
		//Result->ThisWin.fb = Result->pRoot_If->MainWindow.fb;
		sys_memcpy(&Result->ThisWin, &pLib_If->MainWindow, sizeof(Result->pRoot_If->MainWindow));	// inherit from root
		res = swim_window_open(&Result->ThisWin, pLib_If->MainWindow.xpsize, pLib_If->MainWindow.ypsize, pLib_If->fblog, xwin_min, ywin_min, xwin_max, ywin_max, border_width, pcolor, bkcolor, fcolor);
		
		if(res == false)
		{
			sys_free(Result);														// release memory
		} else	Result->pRoot_If = pLib_If;											// save ptr to root interface
	}
	
	return (Result);
}


// ------------------------------------------------------------------------------------------------------
// Print text on position x,z
void lib_SWIM_Print_XY(lib_SWIM_WIN_If_t *Win, int32_t x, int32_t y, char* msg,...)
{
	va_list arg;
	char Buff[90];
	
	
	if((x < 0) || (y < 0)) swim_put_ltext(&Win->ThisWin, Buff);							// print to current x y
	else swim_put_text_xy(&Win->ThisWin, Buff, x, y);									// clear line
	
	va_start (arg, msg);
	vsprintf ( &Buff[0], msg, arg);
	va_end (arg);
	
	if((x < 0) || (y < 0)) swim_put_ltext(&Win->ThisWin, Buff);							// print to current x y
	else swim_put_text_xy(&Win->ThisWin, Buff, x, y);									// print text at position
}

#include "Swim\lpc_rom8x16.c"
//#include "Swim\lpc_winfreesystem14x16.c"
//#include "Swim\winFidonetFGHITerminal10x188x18.c"
//#include "Swim\lpc_rom8x8.c"
//#include "Swim\lpc_x6x13.c"
//#include "Swim\lpc_helvr10.c"
#include "Swim\lpc_x5x7.c"
#include "Swim\lpc_swim.c"
#include "Swim\lpc_swim_font.c"
#include "Swim\lpc_swim_image.c"
#include "Swim\lpc_colors.c"
#include "Swim\lpc_fonts.c"

#endif	// CONF_USE_LIB_SWIM

