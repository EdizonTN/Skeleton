// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: lib_NanoModbus.h
// 	   Version: 1.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Modbus RTU/TCP library based on https://github.com/debevv/nanoMODBUS
// ******************************************************************************
// Notice:
//
// Usage:
//			
// ToDo:
//
// Changelog:
//

#ifndef __LIB_NANOMODBUS_H_
#define __LIB_NANOMODBUS_H_

#include "Skeleton.h"

#include "NanoModbus\nanomodbus.h"

// Modbus Server Devices	- Node device for measurement or action element
// Modbus Client Devices	- Initiate communication - ex. Operator Panel, PLC,...

// ******************************************************************************************************
// DEFAULT CONFIGURATION
// ******************************************************************************************************
//__STDC_NO_VLA__

#ifndef CONF_DEBUG_LIB_NANOMODBUS
 #define CONF_DEBUG_LIB_NANOMODBUS			0										// Default debug is off
#endif


// Modbus settings = define/undefine you can find in the App_Config.h

#if defined(CONF_DEBUG_LIB_MODBUS) && (CONF_DEBUG_LIB_MODBUS == 1)
	#define NMBS_DEBUG
#else	
	#undef NMBS_DEBUG
#endif

// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************



// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************
//typedef struct Regs_t;

// Library interface structure:
COMP_PACKED_BEGIN
typedef struct
{
#if defined(CONF_DEBUG_LIB_MODBUS) && (CONF_DEBUG_LIB_MODBUS == 1)
	const char				Name[CONF_DEBUG_STRING_NAME_SIZE];						// interface name - text - only for ID
#endif	
	struct 	_If_Status					Stat;										// status of the interface
	_drv_If_t 							*pCommDrv;									// communication driver
	void								*pTimerDrvSpec;								// timeout timer driver

	// A single nmbs_bitfield variable can keep 2000 coils
//	nmbs_bitfield 						server_Coils;
//	uint16_t 							server_HoldRegs[REGS_ADDR_MAX];
//	nmbs_bitfield 						server_DisInputs;
//	uint16_t 							server_InRegs[REGS_ADDR_MAX];
	nmbs_t 								nmbs;
//	nmbs_bitfield 						coils;
	nmbs_platform_conf 					nanoModbus_Conf;
} lib_Modbus_If_t;
COMP_PACKED_END



// ******************************************************************************************************
// PUBLIC 
// ******************************************************************************************************
extern 	lib_Modbus_If_t						lib_ModBus_If;							// Modbus Interface
//extern bool lib_NanoModbus_Init(lib_SWIM_If_t* pLib_If, uint8_t *LCD_FB, uint32_t LCD_H_Size, uint32_t LCD_V_Size, uint8_t LCD_BPP); // Reset and init SWIM
extern bool lib_Modbus_Init( lib_Modbus_If_t* pLib_If, uint8_t Address);	//, struct *pReg);
extern void lib_Modbus_Poll( lib_Modbus_If_t* pLib_If);
extern void lib_Modbus_Set_RTUID(lib_Modbus_If_t* pLib_If, uint8_t New_Addr);
#ifndef NMBS_CLIENT_DISABLED
extern bool lib_Modbus_Write_toDest(lib_Modbus_If_t* pLib_If);
#endif

#endif  //__LIB_NANOMODBUS_H_
