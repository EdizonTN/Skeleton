// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: Skeleton.h
// 	   Version: 3.08
// Desc: Global Skeleton System Project Header - public variables and functione declared here.
//		Load system drivers and libraries also.
// ******************************************************************************
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:	
//				2024.06.07	- v3.08 - Add SSD1306 OLED Driver
//				2023.11.05	- v3.07	- Runtime error processing disabled by option CONF_SYS_ERROR_PROCS=0
//				2021.07.26	- v3.06 - CLI and KERMIT dependence
//				2021.05.05	- v3.05 - Add OM8009 driver
//				2020.06.08	- v3.04 - Add assertion
// 


#ifndef __SKELETON_H_
#define __SKELETON_H_

// ******************************************************************************************************
// Load Standard C definitions
// ******************************************************************************************************
#include 	<time.h>																// Standard IO declarations
#include 	<stdbool.h>																// Standard boolean declarations
#include 	<stdint.h>																// Standard Integer declarations
#include 	<string.h>																// Standard string declarations
//#include 	<assert.h>																// Standard string declarations
#include 	"System/compiler.h"														// Standard IO declarations

#if defined(CONF_SYS_ERROR_PROCS) && (CONF_SYS_ERROR_PROCS == 1)
extern void sys_Assert_Handler(const char *pCond_String, const char *pFile, const char *pFn, uint32_t line);

#define SYS_ASSERT(EX)                                                        \
if (!(EX))                                                                    \
{                                                                             \
    sys_Assert_Handler(#EX, __MODULE__,  __FUNCTION__, __LINE__);             \
}
#else
#define SYS_ASSERT(EX)														\
/*																			\
EX																			\
*/


//COMP_PUSH_OPTIONS															\
//__Pragma(diag_suppress 174)												\
// EX 																		\
//COMP_POP_OPTIONS															
#endif


// ******************************************************************************************************
// Load Compiler specific declarations and harmonize it
// ******************************************************************************************************
#include "System/Compiler.h"														// Harmonize used compiler tools


// ******************************************************************************************************
// Load Core definitions and HW specific system
// ******************************************************************************************************
#include "Application/App_Config.h"													// Application specific settings + hardware BSP + CHIP definition + recofigure system


//// ******************************************************************************************************
//// Skeleton's Path Settings
//// ******************************************************************************************************

//#define CONF_SKELETON_DIR_BSP				\BSP


// ******************************************************************************************************
// Default config
// ******************************************************************************************************
#ifndef CONF_SYS_IDIOTPROOF
 #define CONF_SYS_IDIOTPROOF					1									// Turn on some safe checks - more code, lower speed of running...
#endif


#include 	"System/System.h"														// System functions and declarations


//#if defined(CONF_USE_DEBUG) && (CONF_USE_DEBUG == 1)								// if use debug system
	#include "System/Debug.h"														// Load it
//#endif

#if defined(CONF_USE_MEM_MANAGER) && (CONF_USE_MEM_MANAGER == 1)					// if use memory manager
	#include "System/Mem_Manager.h"													// load it
#endif

#if defined(CONF_USE_IRQ_MANAGER) && (CONF_USE_IRQ_MANAGER == 1)					// if use interupt manager
	#include "System/IRQ_Manager.h"													// load it
#endif

#if defined(CONF_USE_MODDRV_MANAGER) && (CONF_USE_MODDRV_MANAGER == 1)				// if use interupt manager
	#include "System/Driver_Manager.h"												// load driver manager. Module manager is included in this file
#endif



// ******************************************************************************************************
// LIBRARIES		- Load hardware independend libraries
// ******************************************************************************************************
// Public Skeleton's Libraries:
#if defined(CONF_USE_LIB_RINGBUFFER) && (CONF_USE_LIB_RINGBUFFER == 1)
	#include "LIB/lib_RingBuffer.h"													// ring /circular/ buffer
#endif

#if defined(CONF_USE_LIB_LPCUSB) && (CONF_USE_LIB_LPCUSB == 1)
	#include "LIB/lib_LPCUSB.h"														// USB stack for LPC
#endif

#if defined(CONF_USE_LIB_CLI) && (CONF_USE_LIB_CLI == 1)
	#include "LIB/lib_CLI.h"														// Command Line Interface
 #if defined(CONF_USE_LIB_EKERMIT) && (CONF_USE_LIB_EKERMIT == 1)
	#include "LIB/lib_EKermit.h"													// + EKermit transfer protocol
 #endif
#endif																				

#if defined(CONF_USE_LIB_DFLT) && (CONF_USE_LIB_DFLT == 1)
	#include "LIB/lib_DFLT.h"														// Digital Filter
#endif

#if defined(CONF_USE_LIB_CRC) && (CONF_USE_LIB_CRC == 1)
	#include "LIB/lib_CRC.h"														// CRC
#endif

#if defined(CONF_USE_LIB_SWIM) && (CONF_USE_LIB_SWIM == 1)
	#include "LIB/lib_SWIM.h"														// SWIM GUI
#endif

#if defined(CONF_USE_LIB_MODBUS) && (CONF_USE_LIB_MODBUS == 1)
	#include "LIB/lib_Modbus.h"													// nanoModBUS comm. library
#endif

// User's Libraries:
#include 	"usr_LIB/usr_LIB.h"                                        				// Load user's private libraries



#include 	"Application/App_Modules.h"                                          	// Load Modules used in current Application


// ******************************************************************************************************
// DRIVERS		- load drivers. Drivers are depends on applicatiuon's modules! Enabled in Application's modules.
// ******************************************************************************************************
#if defined(USE_DRV_USBD) && (USE_DRV_USBD == 1)
	#include "DRV/drv_USBD.h"														// Load USBD
#endif	

#if defined(USE_DRV_UART) && (USE_DRV_UART == 1)
	#include "DRV/drv_UART.h"														// Load UART
#endif	

#if defined(USE_DRV_GPIO) && (USE_DRV_GPIO == 1)
	#include "DRV/drv_GPIO.h"														// Load GPIO
#endif

#if defined(USE_DRV_ADC) && (USE_DRV_ADC == 1)
	#include "DRV/drv_ADC.h"														// Load ADC
#endif

#if defined(USE_DRV_DAC) && (USE_DRV_DAC == 1)
	#include "DRV/drv_DAC.h"														// Load DAC
#endif

#if defined(USE_DRV_DMA) && (USE_DRV_DMA == 1)
	#include "DRV/drv_DMA.h"														// Load DMA
#endif

#if defined(USE_DRV_PWM) && (USE_DRV_PWM == 1)
	#include "DRV/drv_PWM.h"														// Load PWM driver
#endif	

#if defined(USE_DRV_SCT) && (USE_DRV_SCT == 1)
	#include "DRV/drv_SCT.h"														// Load SCT
#endif

#if defined(USE_DRV_I2C) && (USE_DRV_I2C == 1)
	#include "DRV/drv_I2C.h"														// Load I2C Bus driver
#endif

#if defined(USE_DRV_ONEWIRE) && (USE_DRV_ONEWIRE == 1)
	#include "DRV/drv_OneWire.h"													// Load OneWire Bus driver
#endif

#if defined(USE_DRV_SPI) && (USE_DRV_SPI == 1)
	#include "DRV/drv_SPI.h"														// Load SPI Bus driver
#endif

#if defined(USE_DRV_TIMER) && (USE_DRV_TIMER == 1)
	#include "DRV/drv_Timer.h"														// Load Timer driver
#endif

#if defined(USE_DRV_MRT) && (USE_DRV_MRT == 1)
	#include "DRV/drv_MRT.h"														// Load MRT driver
#endif

#if defined(USE_DRV_LCD) && (USE_DRV_LCD == 1)
	#include "DRV/drv_LCD.h"														// Load LCD driver
#endif

#if defined(USE_DRV_24XX1025) && (USE_DRV_24XX1025 == 1)
	#include "DRV/drv_24XX1025.h"													// EEPROM I2C Chip 24xx1025 series
#endif

#if defined(USE_DRV_PCA995X) && (USE_DRV_PCA995X == 1)
	#include "DRV/drv_PCA995x.h"													// LED Driver PCA995x
#endif

#if defined(USE_DRV_SSD1306) && (USE_DRV_SSD1306 == 1)
	#include "DRV/drv_SSD1306.h"													// LED Driver SSD1306
#endif

#if defined(USE_DRV_ETH) && (USE_DRV_ETH == 1)
	#include "DRV/drv_ETH.h"														// Ethernet driver
#endif

#if defined(USE_DRV_DP83640) && (USE_DRV_DP83640 == 1)
	#include "DRV/drv_DP83640.h"													// PHY MAC driver TI DP83640
#endif

#if defined(USE_DRV_OTM8009) && (USE_DRV_OTM8009 == 1)
	#include "DRV/drv_OTM8009.h"														// LCD driver OM8009
#endif

#if defined(USE_DRV_ILI9806) && (USE_DRV_ILI9806 == 1)
	#include "DRV/drv_ILI9806.h"														// LCD driver OM8009
#endif

#if defined(USE_DRV_HX711) && (USE_DRV_HX711 == 1)
	#include "DRV/drv_HX711.h"														// LCD driver OM8009
#endif

// insert new skeleton's drivers here....



// User's drivers:
#include 	"usr_DRV/usr_DRV.h"	                                        			// Load user's private drivers


// user's drivers and modules, pleae include in usr_DRV.h resp. App_Modules.h files in Application folder.



#endif	// __SKELETON_H_
