// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: Compiler.h
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: Normalization of the various names/system of compilers specific
//			Supported: KEIL, GCC
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:	04.01.2019	removed SK_EXTERN, SK_STATIC and SK_INLINE
//


#ifndef __COMPILER_H_ 
#define __COMPILER_H_			

#include 	<stdint.h>	


// http://www.nongnu.org/avr-libc/user-manual/mem_sections.html

//	text				-	contains the actual machine instructions which make up your program. This section is further subdivided by the .initN and .finiN sections
//							Include code, vector table and constants
//	+---initN			-	These sections are used to define the startup code from reset up through the start of main(). N is 0 to 9 executed from 0 to 9
//	+---finiN			-	These sections are used to define the exit code executed after return from main() or a call to exit().N is 0 to 9 executed from 9 to 0
//	bss					-	statically-allocated variables represented solely by zero-valued bits initially (uninitialized)
//	+--.zero 			-	This sections is a part of the .bss section. Will not be initialized to zero during startup as would normal .bss data.
//	data				-	contains initialized static variables, that is, global variables and static local variables.
//							Located in Flash, and after reset are copied to RAM in the startup code
//							ex:	char err_str[] = "Your program has died a horrible death!";
//								struct point pt = { 1, 1 };
//	eeprom				-	This is where eeprom variables are stored.
//	heap				-	heap area commonly begins at the end of the .bss and .data segments and grows to larger addresses from there.
//							The heap area is managed by malloc, realloc, and free, which may use the brk and sbrk system calls.
//	stack				-	stack area contains the program stack, a LIFO structure, typically located in the higher parts of memory. 
//							A "stack pointer" register tracks the top of the stack; it is adjusted each time a value is "pushed" onto the stack. 
//							The set of values pushed for one function call is termed a "stack frame". A stack frame consists at minimum of a return address. 
//							Automatic variables are also allocated on the stack.
//




// ******************************************************************************************************
// KEIL COMPILER        *********************************************************************************
// ******************************************************************************************************
#if defined ( __CC_ARM )
	#include "cmsis_armcc.h"

	#define COMP_TYPE_UV				1											// Kompiler Keil - pouzivame tento ID v aplikacii
	#ifdef __STRICT_ANSI__
		#define __NO_EMBEDDED_ASM													// vypni dodatocne embedded language support - nutne pre CMSIS core_cmInstr.h 
	#endif
	#include <string.h>	

	#define VERSION__					__ARMCC_VERSION
	#define COMPILER_STR				"ARMCC"
	extern uint32_t 					__bss_section_table;
	extern void 						__Vectors(void);							// point to __initial_sp. After reset = 0x00000000
	
#ifndef __STRICT_ANSI__
	extern uint32_t 					Image$$ARM_LIB_STACK$$Base;
	extern uint32_t 					Image$$ARM_LIB_STACK$$ZI$$Limit;
	extern uint32_t 					Image$$ARM_LIB_STACKHEAP$$ZI$$Limit;		// Initial StackPointer
	extern uint32_t 					Image$$DTCM_STACK$$Base;
	extern uint32_t 					Image$$DTCM_STACK$$ZI$$Limit;
#endif

	#define __Pragma(X)					_Pragma(#X)
	#define COMP_PACKED_BEGIN  			_Pragma("pack(1)")
    #define COMP_PACKED_END    			_Pragma("pack(8)")

	#define ATTR_WARN_UNUSED_RESULT     __attribute__ ((warn_unused_result))
	#define ATTR_CONST                  __attribute__ ((const))
	#define ATTR_NON_NULL_PTR_ARG(...)  __attribute__ ((nonnull (__VA_ARGS__)))
	#define ATTR_ALWAYS_INLINE          __attribute__ ((always_inline))
	#define ATTR_DEPRECATED             __attribute__ ((deprecated))
	#define ATTR_NON_NULL_PTR_ARG(...)  __attribute__ ((nonnull (__VA_ARGS__)))
	#define ATTR_ERROR(Message)			__attribute__ ((error(Message)))
	//#define WEAK 						__attribute__ ((weak))
	#define ATTR_ALIGNED(Bytes)         __attribute__ ((aligned(Bytes)))
	#define ATTR_IAR_PACKED
	#define ATTR_PACKED					__attribute__ ((packed))
	#define ATTR_NORETURN               __attribute__ ((noreturn))
	#define ATTR_NAKED                  __attribute__ ((naked))
	#define ATTR_USED                 	__attribute__ ((used))
	#define ATTR_UNUSED                 __attribute__ ((unused))
	#define ATTR_ALIAS(Func)            __attribute__ ((alias( #Func )))
	#define ATTR_WEAK                   __attribute__ ((weak))	
	#define ATTR_SECTION(section_str)   __attribute__ ((section (section_str)))
	#define ATTR_FORMAT_ARG(str_index)  __attribute__ ((format_arg(str_index)))

	#define __WEAK_(X)					__attribute__ ((weak, alias(X)))
	#define __WEAK_FUNC 				__attribute__ ((weak))
	
	
	#define CMP_KWD_PURE				__pure
	#define CMP_KWD_RESTRICT			__restrict
	#define CMP_KWD_PROMISE				__promise

	
	#define PRE_PACK					__packed 
	#define POST_PACK
	
	#define PRAGMA_ALIGN_4096          // not used in KEIL
	#define PRAGMA_ALIGN_2048          // not used in KEIL
	#define PRAGMA_ALIGN_256           // not used in KEIL
	#define PRAGMA_ALIGN_128           // not used in KEIL
	#define PRAGMA_ALIGN_64            // not used in KEIL
	#define PRAGMA_ALIGN_48            // not used in KEIL
	#define PRAGMA_ALIGN_32            // not used in KEIL
	#define PRAGMA_ALIGN_4             // not used in KEIL
				
	#define PRAGMA_ALWAYS_INLINE		_Pragma("inline")
	#define FALSE						false
	#define TRUE						true		
	#define _PPTOSTR_(x) 				#x

	#define EXTERN 						extern
//	#define STATIC 						static
	#define INLINE  					inline

	#define	_IRQ						__irq										// simple (not nested) interrupt 
	
	#define TO_STR(x) 					#x
	#define STR(x) 						TO_STR(x)
	#define CONCAT(a, b) 				XCAT(a,b)
	#define CONCAT3(a, b, c) 			XCAT3(a,b,c)
	#define CONCAT4(a, b, c, d) 		XCAT4(a,b,c,d)
	#define XCAT(a, b)   				a##b
	#define XCAT3(a, b, c)   			a##b##c
	#define XCAT4(a, b, c, d)			a##b##c##d
	# define STRLEN(str) 				(__extension__ (__builtin_constant_p (str) ? __builtin_strlen (str) : __strlen_g (str)))
	#define __SECTION_NOINIT			__attribute__ ((section ("NoInit"), zero_init))					// uninitialized area
	#define	__SECTION_ZEROINIT			__attribute__ ((section ("Zero")))								// zero fill area
	#define	__SECTION_FINI				__attribute__ ((section ("fini")))
	#define	__SECTION_DATA				__attribute__ ((section ("data")))
	#define	__SECTION_USBSTACK			__attribute__ ((section ("usb_stack"))) 
	#define	__SECTION_LOG				__attribute__ ((section ("log_buff")))
	
	#define __SECTION_IRQVECTORS   		__attribute__ ((used, section(".ARM.__at_0x00000000")))
	//#define ISR_VECTOR(x)	   			__attribute__((used,section(".vectors." # x )))
	//#define __SECTION_IRQVECTORS   		__attribute__ ((section (".vectortable")))					// neplatne pre KEIL

	#define	__SECTION_RODATA			__attribute__ ((section ("rodata")))
	#define	ATTR_PGM					__SECTION_RODATA
	
	#define PRAGMA_WEAK(name, vector) 	_Pragma(_PPTOSTR_(weak name=vector))
	#define COMP_IRQ_HANDLER(num)		CONCAT(IRQHandler_, #num)

	#define __WEAK_DEFAULT				__attribute__ ((weak, alias("_Chip_Default_IRQ_Handler")))
	#define	__ALIAS(fnname)				__attribute__ ((alias(fnname)))

	#if defined (__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
		#define	COMP_USE_TWOREGIONMEMORY	__asm(".global __use_two_region_memory\n\t");
		#define	COMP_NO_USE_SEMIHOSTING		__asm(".global __use_no_semihosting\n\t");
		#define	COMP_NO_USE_SEMIHOSTING_SWI	__asm(".global __use_no_semihosting_swi\n\t");
	#else
		#define	COMP_USE_TWOREGIONMEMORY	_Pragma ("import (__use_two_region_memory)")
		#define	COMP_NO_USE_SEMIHOSTING		_Pragma ("import (__use_no_semihosting)")
		#define	COMP_NO_USE_SEMIHOSTING_SWI	_Pragma ("import (__use_no_semihosting_swi)")
	#endif

	#define COMP_PUSH_OPTIONS			_Pragma ("push")
	#define COMP_POP_OPTIONS			_Pragma ("pop")
	#define COMP_OPTIONS_O0				_Pragma ("O0")
	#define COMP_OPTIONS_O1				_Pragma ("O1")
	#define COMP_OPTIONS_O2				_Pragma ("O2")
	#define COMP_OPTIONS_O3				_Pragma ("O3")
	#define	COMP_OPTIMIZE_LEVEL			__OPTIMISE_LEVEL

	#define ERR_BREAK					__ASM volatile("BKPT #01");
	
	// compatibility issues:
	#define ALIGNED(n)					ATTR_ALIGNED(n)


















// ******************************************************************************************************
// SDCC COMPILER             ****************************************************************************
// ******************************************************************************************************
#elif defined ( SDCC )
	#define __ASM           			__asm           							//!< asm keyword for iarcc
	#define __INLINE        			inline           							//!< inline keyword for iarcc. Only avaiable in High optimization mode!
	#define VERSION__					SDCC

// ******************************************************************************************************
// IAR COMPILER              ****************************************************************************
// ******************************************************************************************************
#elif defined ( __ICCARM__ ) || defined (__IAR_SYSTEMS_ICC__)
	#include "cmsis_iccarm.h"        
	#define COMP_TYPE_ICC				1											// Compiler IAR
	#define __ASM           			__asm           							//!< asm keyword for iarcc
	#define __INLINE        			inline           							//!< inline keyword for iarcc. Only avaiable in High optimization mode!
	#define VERSION__					__VER__										// ToDo: toto je iba vymyslene
	#define COMPILER_STR				"IAR C"
	#define static static
	#define inline
	#define TO_STR(x) #x
	#define STR(x) TO_STR(x)
  	#define COMP_PACKED_BEGIN  			__packed
    #define COMP_PACKED_END
	#define __SECTION_NOINIT			@ "No_Init"
	#define	__SECTION_ZEROINIT			@ "Zero_Init"

// ******************************************************************************************************
// ALTIUM VX TOOLSET COMPILER       *********************************************************************
// ******************************************************************************************************
#elif defined   (  __TASKING__  )
	#define COMP_TYPE_TSK				1											// Kompiler Tasking - pouzivame tento ID v aplikacii
	#define __ASM            			__asm           							//!< asm keyword for TASKING Compiler
	#define __INLINE         			inline         								//!< inline keyword for TASKING Compiler
	#define __VERSION__					(char)("unknown")							// ToDo: toto je iba vymyslene
		
// ******************************************************************************************************
// GNU C COMPILER       *********************************************************************************
// ******************************************************************************************************
#elif defined ( __GNUC__ ) &&  !defined ( __CODE_RED )		// GCC
	#include "cmsis_gcc.h"

	#define COMP_TYPE_GCC				1											// Kompiler GCC - pouzivame tento ID v aplikacii
	#define __ASM             			__asm          								//!< asm keyword for gcc
	#define __INLINE          			inline         								//!< inline keyword for gcc
	#define VERSION__					__GNUC_VERSION__							// ToDo: toto je iba vymyslene
	#define COMPILER_STR				"GNU GCC"
	
	extern	uint32_t					__exidx_start;
	extern	uint32_t					__exidx_end;
	extern	uint32_t					__etext;
	extern 	uint32_t 					__data_start__;
	extern 	uint32_t 					__data_end__;
	extern 	uint32_t 					__bss_start__;
	extern 	uint32_t 					__bss_end__;
	extern 	uint32_t 					__HeapLimit;
	extern 	uint32_t 					__HeapBase;
	extern 	uint32_t 					__StackLimit;
	extern 	uint32_t 					__StackTop;
	register uint32_t * 				StackPointer asm("sp");
	#include <unistd.h>
	#define	M_FLASH_LOW					(unsigned long) &__vectors_start__
	#define M_FLASH_HIGH				(unsigned long) &__top_MFlash256
	#define M_FLASH_LAST				(unsigned long) &__exidx_start - 8
	#define M_BSS_LOW					(unsigned long) &__bss_start__
	#define M_BSS_HIGH					(unsigned long) &__bss_end__
	#define M_DATA_LOW					(unsigned long) &_data
	#define M_DATA_HIGH					(unsigned long) &_edata
	#define M_HEAP_LOW					(unsigned long) &__HeapBase					// heap start address		- Low hodnota
	#define	M_HEAP_HIGH					(unsigned long) &__HeapLimit				// heap end address			- High hodnota
	#define M_STACK_LOW					(unsigned long) &__StackLimit  				//__stack end address		- Low hodnota
	#define M_STACK_HIGH				(unsigned long) &__StackTop					// stack start address		- High hodnota
	

// ******************************************************************************************************
// NXP LPCXPRESSO COMPILER       ************************************************************************
// ******************************************************************************************************
#elif defined ( __REDLIB__ ) || defined ( __NEWLIB__ ) || defined ( __CODE_RED )	// LPCXPRESSO
	#include "cmsis_gcc.h"
	#define COMP_TYPE_XPR				1											// Kompiler LPCXpresso - pouzivame tento ID v aplikacii
	#ifdef __STRICT_ANSI__
		#define __NO_EMBEDDED_ASM													// vypni dodatocne embedded language support - nutne pre CMSIS core_cmInstr.h
	#endif
//	#include <string.h>
	#define VERSION__					__STDC_VERSION__
	#define COMPILER_STR				"NXP GCC"

	#define __Pragma(X)					_Pragma(#X)
	#define COMP_PACKED_BEGIN  			_Pragma("pack(1)")
	#define COMP_PACKED_END    			_Pragma("pack(8)")
	#define	COMP_PACKED
	#define ATTR_WARN_UNUSED_RESULT     __attribute__ ((warn_unused_result))
	#define ATTR_CONST                  __attribute__ ((const))
	#define ATTR_NON_NULL_PTR_ARG(...)  __attribute__ ((nonnull (__VA_ARGS__)))
	#define ATTR_ALWAYS_INLINE          __attribute__ ((always_inline))
	#define ATTR_DEPRECATED             __attribute__ ((deprecated))
	#define ATTR_NON_NULL_PTR_ARG(...)  __attribute__ ((nonnull (__VA_ARGS__)))
	#define ATTR_ERROR(Message)			__attribute__ (( error(Message) ))
	#define ATTR_WEAK                   __attribute__ ((weak))
	#define ATTR_ALIGNED(Bytes)         __attribute__ ((aligned(Bytes)))
	//#define ATTR_PACKED               __attribute__ ((packed))
	#define ATTR_IAR_PACKED
	#define ATTR_PACKED					__attribute__ ((packed))
	#define __packed					ATTR_PACKED
	#define ATTR_NORETURN               __attribute__ ((noreturn))
	#define ATTR_NAKED                  __attribute__ ((naked))
	#define ATTR_UNUSED                 __attribute__ ((unused))
	#define ATTR_SECTION(section_str)   __attribute__ ((section (section_str)))
	#define ATTR_FORMAT_ARG(str_index)  __attribute__ ((format_arg(str_index)))

	#define PRAGMA_ALIGN_128
	#define PRAGMA_ALIGN_64
	#define PRAGMA_ALIGN_4

	#define PRE_PACK
	#define POST_PACK					ATTR_PACKED
	#define ALIGNED(n)      			ATTR_ALIGNED(n)

	#define WEAK 						__attribute__ ((weak))
	#define ALIAS(f) 					__attribute__ ((weak, alias (#f)))

	#define PRAGMA_ALWAYS_INLINE		_Pragma("inline")
	#define FALSE						false
	#define TRUE						true

	#define ATTR_ALIAS(Func)

	#define _PPTOSTR_(x) #x
	#define PRAGMA_WEAK(name, vector) 	_Pragma(_PPTOSTR_(weak name=vector))

	#define static 						static
	#define inline  					__inline
	#define	_IRQ

	#define TO_STR(x) #x
	#define STR(x) TO_STR(x)


	extern uint32_t 					__bss_section_table;
	extern uint32_t						__Vectors;       							//__vectors_start__;
	extern uint32_t 					__exidx_start;
	extern uint32_t 					__exidx_end;
	extern uint32_t 					_etext;
	extern uint32_t 					__data_section_table;
	extern uint32_t 					__data_section_table_end;
	extern uint32_t 					__bss_section_table;
	extern uint32_t 					__bss_section_table_end;
	extern uint32_t						__end_of_heap;
	extern uint32_t 					_pvHeapStart;
	//extern uint32_t					_pvHeapLimit;
	extern void							*_pvHeapLimit;
	//extern uint32_t 					_vStackTop;
	extern uint32_t						_bss;
	extern uint32_t						_data;
	extern uint32_t						_edata;
	extern uint32_t						__vectors_start__;
	extern uint32_t 					__top_MFlash256;
	register uint32_t 					*StackPointer __asm("sp");
	
	// STACK zacina na konci RAM a smeruje dole, HEAP zacina na konci obsadenej RAM a smeruje hore
	#define	M_FLASH_LOW					(unsigned long) &__vectors_start__
	#define M_FLASH_HIGH				(unsigned long) &__top_MFlash256
	#define M_FLASH_LAST				(unsigned long) &__exidx_start - 8
	#define M_BSS_LOW					(unsigned long) &_bss
	#define M_BSS_HIGH					(unsigned long) &_pvHeapStart
	#define M_DATA_LOW					(unsigned long) &_data
	#define M_DATA_HIGH					(unsigned long) &_edata
	#define M_HEAP_LOW					(unsigned long) &_pvHeapStart				// heap start address
	#define	M_HEAP_HIGH					(unsigned long) &_pvHeapLimit				// heap end address
	#define M_STACK_LOW					(unsigned long) &__HeapLimit  				//__stack end address
	#define M_STACK_HIGH				(unsigned long) &_vStackTop					// stack start address

	#define __SECTION_NOINIT			__attribute__ ((section (".no_init")))
	#define	__SECTION_ZEROINIT			__attribute__ ((section (".bss")))
	#define	__SECTION_FINI				__attribute__ ((section(".fini")))
	#define	__SECTION_DATA				__attribute__ ((section(".data")))
	#define	__SECTION_RODATA			__attribute__ ((section(".rodata")))
	#define	__SECTION_USBSTACK			__attribute__ ((section(".usb")))
	//__SECTION(data,RAM2)

	#define	COMP_USE_TWOREGIONMEMORY	_Pragma ("import (__use_two_region_memory)")
	#define	COMP_NO_USE_SEMIHOSTING		_Pragma ("import (__use_no_semihosting)")
	#define	COMP_NO_USE_SEMIHOSTING_SWI

	#define COMP_PUSH_OPTIONS			_Pragma ("GCC push_options")
	#define COMP_POP_OPTIONS			_Pragma ("GCC pop_options")
	#define COMP_OPTIONS_O0				_Pragma ("GCC optimize (O0)")
	#define COMP_OPTIONS_O1				_Pragma ("GCC optimize (O1)")
	#define COMP_OPTIONS_O2				_Pragma ("GCC optimize (O2)")
	#define COMP_OPTIONS_O3				_Pragma ("GCC optimize (O3)")
	
	#define	COMP_OPTIMIZE_LEVEL			"NaN"										// Cannot read optimalization settings
	#define CPUCRIT_VAR()  uint8_t cpuSR
	#define SYS_CPUCRIT_START()              	\
	  do {                                  \
		asm (                               \
		"MRS   R0, PRIMASK\n\t"             \
		"CPSID I\n\t"                       \
		"STRB R0, %[output]"                \
		: [output] "=m" (cpuSR) :: "r0");   \
	  } while(0)
	#define SYS_CPUCRIT_END()               	\
	  do{                                   \
		asm (                               \
		"ldrb r0, %[input]\n\t"             \
		"msr PRIMASK,r0;\n\t"               \
		::[input] "m" (cpuSR) : "r0");      \
	  } while(0)
	  
#endif

 #define 	ZERO_DIV_CHECK(x,y) 		((y == 0) ? 0: x/y)							// makro kontroluje ci sa da delit premennou y
 #define 	STDIN_FILENO    			0 	//stdin    								// standard input file descriptor
 #define 	STDOUT_FILENO   			1 	//stdout   								// standard output file descriptor
 #define 	STDERR_FILENO   			2 	//stderr   								// standard error file descriptor

	
#undef _BIT
#define 	_BIT(n) 					(1 << (n))

#ifndef _NULL
 #ifndef NULL
	#define 	NULL 					((void*) 0)
 #endif

 #define 	_NULL 						NULL
#endif


	
// ******************************************************************************************************
// Export
// ******************************************************************************************************
#define TOOLCHAIN_VERSION_STRING		COMPILER_STR" "STR(VERSION__)
#define	TOOLCHAIN_PARM_STRING			" -O"STR(COMP_OPTIMIZE_LEVEL)
#if defined(__MICROLIB)
	#define TOOLCHAIN_LIB_STRING		"MICROLIB"
#elif defined(__NEWLIB__)
	#define TOOLCHAIN_LIB_STRING		"NEWLIB"
#elif defined(__REDLIB__)
	#define TOOLCHAIN_LIB_STRING		"REDLIB"
#else
	#define TOOLCHAIN_LIB_STRING		"STDLIB"
#endif	
#define BUILD_DATETIME_STRING			__DATE__ " "__TIME__	
	
#endif // __COMPILER_H_
