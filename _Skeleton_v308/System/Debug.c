// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: Debug.c
// 	   Version: 2.0
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: Debug subsystem.
//		 Idea from: http://www.hitex.co.uk/fileadmin/uk-files/pdf/ARM%20Seminar%20Presentations%202013/
//							Feabhas%20Developing%20a%20Generic%20Hard%20Fault%20handler%20for%20ARM.pdf
// or: https://www.segger.com/downloads/application-notes/AN00016
//
// Notice:
//
// Usage:
//			
// ToDo:
//			- ako ID pri debug print pouzi makra: __MODULE__ a __func__
// 			- SWO Trace for MCUXpresso:
// 				https://www.nxp.com/docs/en/quick-reference-guide/MCUXpresso_IDE_SWO_Trace.pdf
//
// Changelog:
//


#include "Skeleton.h"

#if defined(CONF_USE_DEBUG) && (CONF_USE_DEBUG == 1)
#define DEBUG_OBJECTNAME				"DebugSys"

#include <time.h>
#include <stdio.h>


// ******************************************************************************************************
// Variables
// ******************************************************************************************************
volatile Debug_st 		_DEBUG_pIf 		__SECTION_ZEROINIT;							// clear to zero after resete!

struct __FILE { int handle; /* Add whatever you need here */ };
//FILE __stdout;
//FILE __stdin;
//FILE __stderr;

 #if defined(CONF_LOG_SIZE) && (CONF_LOG_SIZE)
	uint32_t	LastLogChar										__SECTION_LOG;
	char 		dbg_LogArea[CONF_LOG_SIZE - sizeof(uint32_t)] 	__SECTION_LOG;
 #endif	


// ******************************************************************************************************
// Function prototypes
// ******************************************************************************************************
int Debug_Register_CallbackFunction(int file, void *fnPtr)	__SECTION_FINI;
void Debug_Init(void);
extern void HardFault_Handler_C(unsigned int * hardfault_args);						// Catch hard fault handler - print regs via dbgprint
extern int sendchar (int c);
extern int getkey   (void);



#if (BSP_ALLOW_SWO == 1) && (__CORTEX_M >= 3) && (CONF_DEBUG_ITM == 1)				// debug via SWO
	#if defined(COMP_TYPE_UV)
		COMP_NO_USE_SEMIHOSTING_SWI
	#endif
volatile int32_t ITM_RxBuffer = ITM_RXBUFFER_EMPTY; 								// CMSIS Debug Input
#endif

#if defined (COMP_TYPE_XPR)
	#if defined (__NEWLIB__)
		int _write(int iFileHandle, char *pcBuffer, int iLength);
		int _read(int iFileHandle, char *pcBuffer, int iLength);
	#elif defined (__REDLIB__)
		int __sys_write(int iFileHandle, char *pcBuffer, int iLength);
		int __sys_readc(void);
	#endif
#endif



//__attribute__((weak))
//void abort(void) 
//{
//  for (;;);
//}


//// ------------------------------------------------------------------------------------------------
//// retarget __aeabi_assert
//__attribute__((weak,noreturn))
//void __aeabi_assert (const char *expr, const char *file, int line) {
//  char str[12], *p;

//  fputs("*** assertion failed: ", stderr);
//  fputs(expr, stderr);
//  fputs(", file ", stderr);
//  fputs(file, stderr);
//  fputs(", line ", stderr);

//  p = str + sizeof(str);
//  *--p = '\0';
//  *--p = '\n';
//  while (line > 0) {
//    *--p = '0' + (line % 10);
//    line /= 10;
//  }
//  fputs(p, stderr);

//  abort();
//}

//void dbg_SaveToLog(__VA_ARGS__)
//{
//	LastLogChar += sprintf(dbg_LogArea[LastLogChar], __VA_ARGS__);
//}


// ------------------------------------------------------------------------------------------------
// retarget fputc
// ------------------------------------------------------------------------------------------------------
int fputc(int c, FILE *f)
{
	int n = 0;
	_DEBUG_pIf.SendChar = c;
	
#if (BSP_ALLOW_SWO == 1) && (BSP_ALLOW_SWO == 1) && (__CORTEX_M >= 3)						// print do debug vindow in UV IDE
	int  i;
	//debug_ITM_SendChar(c);
	ITM_SendChar(c);																// call to core_cmX.h file
	c = 0x00;
	for (i = 1000; i; i--) {__NOP();}
#endif	
	
	if(_DEBUG_pIf.Callback_STDOUT.Count)											// If CallBack is set
	{
		for (n = 0; n < _DEBUG_pIf.Callback_STDOUT.Count; n++)						// installed debug out
		{
			if(_DEBUG_pIf.Callback_STDOUT.CallBackFn[n].Active == true) _DEBUG_pIf.Callback_STDOUT.CallBackFn[n].fnPtr();			// call an installed function
		}
	}
	return(c);	
}

// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
int sendchar(int c)
{
	return(c);
}

// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
int getkey(void)
{
#if (CONF_DEBUG_ITM == 1) && (CONF_DEBUG_ITM == 1) && (__CORTEX_M >= 3)				// print do debug vindow in UV IDE
	return (ITM_ReceiveChar());
#else
	return (-1);
#endif	
}

// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
#if defined(COMP_TYPE_UV)
int ferror(FILE *f)
{
  /* Your implementation of ferror */
  return EOF;
}
#endif

// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
void _ttywrch(int ch) 
{
  sendchar (ch);
}

// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------
void _sys_exit(int return_code) 
{
  	
	sys_System.Status->RunState = sEXIT;
#if (CONF_DEBUG_SUBSYS == 1)
	dbgprint("\r\n._sys_exit asserted!\r\n");
#endif	
	ERR_BREAK;
}


// ------------------------------------------------------------------------------------------------------
// sys_HardFault_Handler - Print debug registers
// ------------------------------------------------------------------------------------------------------
void HardFault_Handler_C(unsigned int * hardfault_args) // prints 8 regs saved on the stack and so on
{
	unsigned int stacked_r0;  
    unsigned int stacked_r1;  
    unsigned int stacked_r2;  
    unsigned int stacked_r3;  
    unsigned int stacked_r12;  
    unsigned int stacked_lr;  
    unsigned int stacked_pc;  
    unsigned int stacked_psr;  
     
    //Exception stack frame  
    stacked_r0 = ((unsigned long) hardfault_args[0]);  
    stacked_r1 = ((unsigned long) hardfault_args[1]);  
    stacked_r2 = ((unsigned long) hardfault_args[2]);  
    stacked_r3 = ((unsigned long) hardfault_args[3]);  
     
    stacked_r12 = ((unsigned long) hardfault_args[4]);  
    stacked_lr = ((unsigned long) hardfault_args[5]);  
    stacked_pc = ((unsigned long) hardfault_args[6]);  
    stacked_psr = ((unsigned long) hardfault_args[7]);  

    sys_System.Status->RunState = sHALT;
	
	dbgprint("\r\n.HardFault Handler! HALTED. \r\n");
    dbgprint ("\r\n R0 [MSP/PSP] = 0x%08x", stacked_r0);  
    dbgprint ("\r\n R1 [LR] = 0x%08x", stacked_r1);  
    dbgprint ("\r\n R2 = 0x%08x", stacked_r2);  
    dbgprint ("\r\n R3 = 0x%08x", stacked_r3);  
    dbgprint ("\r\n R12 = 0x%08x", stacked_r12);  
    dbgprint ("\r\n LR [R14] = 0x%08x [return address]", stacked_lr);  
    dbgprint ("\r\n PC [R15] = 0x%08x [program counter]", stacked_pc);  
    dbgprint ("\r\n PSR = 0x%08x", stacked_psr);  
#if ( __CORTEX_M >= 3)    
	// extended CORTEX-M3 and higher, debug prints
	dbgprint ("\r\n MMFAR = 0x%08lx", (unsigned long) SCB->MMFAR); 		// (0xE000ED34))));  
	if ((SCB->CFSR & SCB_CFSR_MEMFAULTSR_Msk) >> SCB_CFSR_MEMFAULTSR_Pos) dbgprint ("!");				// ak je bit MFARVALID nahodeny, jedna sa o MemManage Fault
	dbgprint ("\r\n BFAR = 0x%08lx", (unsigned long) SCB->BFAR); 		// (0xE000ED38))));  
	if ((SCB->CFSR & SCB_CFSR_BUSFAULTSR_Msk) >> SCB_CFSR_BUSFAULTSR_Pos) dbgprint ("!");				// ak je bit BFARVALID nahodeny, jedna sa o BusFault
	dbgprint ("\r\n CFSR = 0x%08lx", (unsigned long) SCB->CFSR);		// (0xE000ED28))));  	// Configurable Fault Status Register
	dbgprint ("\r\n HFSR = 0x%08lx", (unsigned long) SCB->HFSR);		// (0xE000ED2C))));  	// HardFault Status Register
	dbgprint ("\r\n DFSR = 0x%08lx", (unsigned long) SCB->DFSR);		// (0xE000ED30))));  
	dbgprint ("\r\n AFSR = 0x%08lx", (unsigned long) SCB->AFSR);		// (0xE000ED3C))));   	// Auxiliary Fault Status Register
	// ************* DECODE STATUS at:
	// 				http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.dui0552a/Cihbhdbc.html
#endif	

	ERR_BREAK;																		// breakpoint
}



















/* https://community.arm.com/developer/ip-products/system/f/embedded-forum/3257/debugging-a-cortex-m0-hard-fault

If using Cortex-M0+ processor, and if the Micro Trace Buffer (MTB) is available, then the instruction trace feature allows you to view the recent execution history. Application note covering usage of MTB in Keil MDK-Arm is available on Keil website: http://www.keil.com/appnotes/docs/apnt_259.asp

In summary, when debugging HardFaults on Cortex-M0/Cortex-M0+ processors, several pieces of information are very useful:

Extract the stacked PC (you already mentioned that)
Check the T bit in the stacked xPSR
Check the IPSR in the stacked xPSR
If the SP is pointing to an invalid memory location, then you won�t be able to extract the stack frame. In these occasions, you can:

Check if you have allocated enough stack space. Various tool chains have different way to provide the stack usage of the application code. In any case, stack usage analysis is something you should do anyway, even the program didn�t crash. Don�t forget that exception handlers also need stack spaces, and for each extra nested ISR (interrupt service routine), your need more stack space for the stack frame as well as the ISR code.
Add a few function calls in various places in your program to check for stack leaks. CMSIS-Core provides some functions to help accessing SP value (e.g. __get_MSP()), and you can use those functions to add stack checking code (e.g. the value of MSP should be the same everything when a function is called).
If you are not using an RTOS, you can use the banked stack pointer feature to separate the stack used by threads and handlers. In this way you can also add stack checking in the ISR with lowest priority level. Higher priority level ISRs cannot use this trick because the SP value can be different if there was a lower priority ISR running.
If you are using an RTOS, some of them (including Keil RTX) has optional stack checking feature.
If the SP is pointing to a valid location, then you should be able to extract some useful information from the stack frame.

If the T bit in the stacked xPSR is 0, something is trying to switch the processor into Arm state.
If the T bit in the stacked xPSR is 0 and the stacked PC is pointing to the beginning of an ISR, check the vector table (all LSB of exception vectors should be set to 1).
If the stacked IPSR (inside xPSR) is indicating an ISR is running, and the stacked PC is not inside the address range of the ISR code, then you likely to have a stack corruption in that ISR. Look out for data array accesses with unbounded index.
If the stacked PC is pointing to a memory access instruction, usually you can debug the load/store issue based on the register contents (see below):
Faults related to memory access instructions can be caused by:

Invalid address - check the address value
Data alignment issue (the processor has attempted to carried an unaligned data accesses)
For Cortex-M0+ processor, please check for memory access permission (e.g. unprivileged access to the NVIC register), or MPU permission violations.
Bus components or peripheral returned an error response for other reason.
You can also get a HardFault exception if you executed SVC instruction in an exception handler with same or higher priority than the SVC priority level. The fault happened because the current context does not have the right priority level for the SVC.
*/	




// ------------------------------------------------------------------------------------------------
// sys_HardFault_Handler - premapuj na user-defined:
// premapuj sys_HardFault_Handler na HardFault_Handler_C s parametrom Stack. Pre vypis registrov.
// ------------------------------------------------------------------------------------------------------
#if ( __CORTEX_M == 0 )	&& !defined(__CORE_CM0PLUS_H_DEPENDANT)						// pre jadro CORTEX M0
__asm void sys_HardFault_Handler(void) 
{
}	
#elif defined (__CORE_CM0PLUS_H_DEPENDANT)														// pre jadro CORTEX M0+
// tato rutina je zapisana vo vektorovej tabulke. Musis pridat IMPORT HardFault_Handler_ASM do startup.s Platne pre CMO a CMO+
// based on http://community.arm.com/thread/7419
__asm void sys_HardFault_Handler(void)  //__irq
{
	IMPORT	HardFault_Handler_C				//
	MOVS 	R0, #4							//
	MOV 	R1, LR							//
	TST 	R0, R1							//
	BEQ		_1								//
	MRS  	R0, PSP 						//
	LDR 	R1, =HardFault_Handler_C 		//
	BX		R1								//
_1											//
	MRS  	R0, MSP 						//
	LDR		R1, =HardFault_Handler_C		//
	BX		R1								//
	NOP										//		Note: http://www.keil.com/forum/57131/assembler-function-warning-padding-bytes/
	NOP
} 
#elif ( __CORTEX_M >= 3 ) 																		// pre jadro CORTEX M3
// dorob vycitavanie info podla http://www.keil.com/appnotes/files/apnt209.pdf
// tato rutina je zapisana vo vektorovej tabulke. Musis pridat IMPORT HardFault_Handler_ASM do startup.s platne pre CM3, CM4, CM7
// based on http://community.arm.com/thread/7419
#if defined(COMP_TYPE_UV)
__asm void sys_HardFault_Handler(void) 
{
	TST 	R0, R1
	ITE		EQ
	MRSEQ  	R0, MSP 
	MRSNE  	R0, PSP 
	IMPORT HardFault_Handler_C 				; doplnene !!!
	B		HardFault_Handler_C
} 
#else
void sys_HardFault_Handler(void)
{
__asm__ __volatile__
	(
		".weak  HardFault_Handler_C \n"
		"TST 	R0, R1 \n"
		"ITE		EQ \n"
		"MRSEQ  	R0, MSP \n"
		"MRSNE  	R0, PSP \n"
		"B		HardFault_Handler_C \n"
	);
}
#endif
#endif


// ******************************************************************************************************
// HW independend functions   ***************************************************************************
// ******************************************************************************************************


// ------------------------------------------------------------------------------------------------------
//	int Register_Debug_CallbackFunction(int file, void	(*fnPtr), ... )
//	file: STD channel : STDOUT, STDIN, STDERR
//	fnPtr: callback function pointer
// ------------------------------------------------------------------------------------------------------
int Debug_Register_CallbackFunction(int file, void* fnPtr)
{
	int num;
	int Result = false;

	switch (file)
	{
	case STDOUT_FILENO:
	{
		for (num = 0; num <= DEBUG_MAX_CALLBACKS_PER_CHANNELS; num++)
		{
			if (_DEBUG_pIf.Callback_STDOUT.CallBackFn[num].Active == false)
			{
#if defined(__CC_ARM)
#pragma push
	#pragma diag_suppress 513														// suppress warningu 513 in compile time : (void * vs void (*)())
				_DEBUG_pIf.Callback_STDOUT.CallBackFn[(int)_DEBUG_pIf.Callback_STDOUT.Count].fnPtr = fnPtr;
#pragma pop
#else
				_DEBUG_pIf.Callback_STDOUT.CallBackFn[(int)_DEBUG_pIf.Callback_STDOUT.Count].fnPtr = fnPtr;
#endif
				_DEBUG_pIf.Callback_STDOUT.CallBackFn[num].Active = true;
				_DEBUG_pIf.Callback_STDOUT.Count++;
				Result = true;
				return Result;
			}
		}
		break;
	}
	case STDERR_FILENO:
	{
		for (num = 0; num <= DEBUG_MAX_CALLBACKS_PER_CHANNELS; num++)
		{
#if defined(__CC_ARM)
#pragma push
#pragma diag_suppress 513															// suppress warningu 513 in compile time : (void * vs void (*)())
			if (_DEBUG_pIf.Callback_STDERR.CallBackFn[num].Active == false)
			{
				_DEBUG_pIf.Callback_STDERR.CallBackFn[(int)_DEBUG_pIf.Callback_STDERR.Count].fnPtr = fnPtr;
#pragma pop
#else
				if (_DEBUG_pIf.Callback_STDERR.CallBackFn[num].Active == false)
				{
					_DEBUG_pIf.Callback_STDERR.CallBackFn[(int)_DEBUG_pIf.Callback_STDERR.Count].fnPtr = fnPtr;
#endif
				_DEBUG_pIf.Callback_STDERR.CallBackFn[num].Active = true;
				_DEBUG_pIf.Callback_STDERR.Count++;
				Result = true;
				return Result;
			}
		}
		break;
	}
	}
	return Result;
}


#if defined(_DEBUG_STDOUT_IF)
// ------------------------------------------------------------------------------------------------------
// Debug function called of each dbgprint command - serial line - STDOUT channel
// ------------------------------------------------------------------------------------------------------
static void Debug_toUART_asSTDOUT(void)
{
	if(_DEBUG_STDOUT_IF.pApi->Write_Data ) _DEBUG_STDOUT_IF.pApi->Write_Data (&_DEBUG_STDOUT_IF, (uint8_t*) &_DEBUG_pIf.SendChar, 1);
}
#endif

#if defined(_DEBUG_STDERR_IF)
// ------------------------------------------------------------------------------------------------------
// Debug function called of each dbgprint command - serial line - STDOUT channel
// ------------------------------------------------------------------------------------------------------
static void Debug_toUART_asSTDERR(void)
{
	if(_DEBUG_STDERR_IF.pApi->Write_Data ) _DEBUG_STDERR_IF.pApi->Write_Data (&_DEBUG_STDERR_IF, (uint8_t*) &_DEBUG_pIf.SendChar, 1);
}
#endif

// ------------------------------------------------------------------------------------------------------
// Inicializacia debug systemu.
// viacmenej vynulovanie premennych a internych periferii. Externe periferii si inicializuj sam v main.c
// ------------------------------------------------------------------------------------------------------
void Debug_Init(void)
{
	_DEBUG_pIf.Callback_STDOUT.Count = 0;											// set none call back 
	_DEBUG_pIf.Callback_STDERR.Count = 0;											// set none call back 

	memset( (void *) &_DEBUG_pIf, 0x00, sizeof(_DEBUG_pIf));
	
	dbgprint("\r\n("DEBUG_OBJECTNAME"): Init");
	
#if defined ( DEBUG_VIA_UART )														// DEBUG_Driver via UART ????
#ifdef _DEBUG_STDOUT_IF																// STDOUT via which UART ???
	dbgprint("\r\n("DEBUG_OBJECTNAME"): STDOUT: Register Callback");
	Debug_Register_CallbackFunction(STDOUT_FILENO, &Debug_toUART_asSTDOUT);			// create serial debug channel for STDOUT
#endif // _DEBUG_STDOUT_IF
	
#ifdef _DEBUG_STDERR_IF																// STDERR via which UART ???
	dbgprint("\r\n("DEBUG_OBJECTNAME"): STDERR: Register Callback");
	Debug_Register_CallbackFunction(STDERR_FILENO, &Debug_toUART_asSTDERR);			// create serial debug channel for STDERR
#endif // _DEBUG_STDERR_IF
#endif // DEBUG_VIA_UART


#if defined ( DEBUG_VIA_USB )														// DEBUG_Driver via USB ????
#if defined (_DEBUG_STDOUT_IF) && (CONF_DEBUG_SUBSYS == 1)
	dbgprint("\r\n("DEBUG_OBJECTNAME"): STDOUT: Init Iface: %s", _DEBUG_STDOUT_IF.Name);
#endif	

#if defined ( _DEBUG_STDOUT_IF )
	_DEBUG_STDOUT_IF.pApi->Init_HW (&_DEBUG_STDOUT_IF, false);						// initialize only this channel
	_DEBUG_STDOUT_IF.pApi->Init_SW (&_DEBUG_STDOUT_IF, false);						// initialize only this channel
#endif

#if defined (_DEBUG_STDERR_IF) && (CONF_DEBUG_SUBSYS == 1)
	dbgprint("\r\n("DEBUG_OBJECTNAME"): STDERR: Init Iface: %s", _DEBUG_STDERR_IF.Name);
#endif	

#if defined ( _DEBUG_STDERR_IF )
	_DEBUG_STDERR_IF.pApi->Init_HW (&_DEBUG_STDERR_IF, false);						// initialize only this channel
	_DEBUG_STDERR_IF.pApi->Init_SW (&_DEBUG_STDERR_IF, false);						// initialize only this channel
	if(!_DEBUG_STDERR_IF.Stat.Enabled) _DEBUG_STDERR_IF.pApi->Enable (&_DEBUG_STDERR_IF, true); // enable interface
#endif		// _DEBUG_STDOUT_IF
#endif // DEBUG_VIA_USB



// and now enable interface
#if defined ( _DEBUG_STDOUT_IF )
	if(!_DEBUG_STDOUT_IF.Stat.Enabled) _DEBUG_STDOUT_IF.pApi->Enable (&_DEBUG_STDOUT_IF, true);	// enable interface STD OUT
#endif	
#if defined ( _DEBUG_STDERR_IF )
	if(!_DEBUG_STDERR_IF.Stat.Enabled) _DEBUG_STDOUT_IF.pApi->Enable (&_DEBUG_STDERR_IF, true);	// enable interface STD ERR
#endif	

	dbgprint("\r\n("DEBUG_OBJECTNAME"): Init Done.");
}
#endif //CONF_USE_DEBUG
