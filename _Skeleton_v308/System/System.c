// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: System.c
// 	   Version: 3.16
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Skeleton's system functions
// ******************************************************************************
// Notice:
//
// Usage:
//			
// ToDo:
//
// Changelog:	
//				2024.10.15	- v3.16	- fix user system clocking
//				2024.03.09	- v3.15 - add HalfSecondTimer Flag
//									- add sys_NULL_Function
//				2024.01.11	- v3.14 - add a sys_mem_realloc function
//									- add sys_Buffer_Create parameter - can link existing memory array. No clear buffer after create.
//									- sys_Buffer_Destroy will not clear memory anymore.
//				2023.11.05	- v3.13	- Runtime error processing disabled by option CONF_SYS_ERROR_PROCS=0
//									- systick rewrited
//									- rename handler to sys_xxx_handler
//									- sys_Buffer_Write_Bytes and sys_Buffer_Read_Bytes - if len = 0, read/write one byte and DON'T increment pointer !!
//				2022.12.09	- v3.12 - add optimalization (if-else) switches
//				2022.12.08	- v3.11 - add sys_Buffer_Copy_Bytes
//				2022.11.28	- v3.10  - optimize to sie. Chnage CONF_PART_INFO to CONF_SYS_PART_INFO, CONF_MEM_INFO to CONF_SYS_MEM_INFO
//									- Clock_Refresh move to sys_System_t
//				2020.09.16	- v3.06 - add full and empty flag. Corrected mistake with overflow. Add sys_Buffer_Flush
//				2020.06.16	- while(tmpListRecord) changed
//							- show unhandled exception with vector number
//				2020.06.08	- v3.04 - Add assertion
//				2020.05.17 	- Buffer manipulation changed - also for element parameter, copied in bytes
//							- sys_Buffer_Read_Element  - swap 1 and 2 argument 
//							- Buffer manipulation corrected - Rd had bigger than Wr
//							- Buffer overwrite corrected - wriong pEnd was set!
//							- Write Buffer rotation corrected 
//				2020.05.24	- Watch Dog added. Sys_System_Run added


#include 	<Skeleton.h>

#include 	<stdarg.h>
#include 	<stdlib.h>
#include 	<string.h>


#define	SYS_OBJECTNAME					"System"

// ******************************************************************************************************
// ******************************************************************************************************
// 						MEMORY LAYOUT
// ******************************************************************************************************
// ******************************************************************************************************
// 																							
// ONE Region Mode:
//	+--------+----------+	Stack_Start = Stack_Pos (Aftrer Reset)
//	|        |          |
//	|        v   stack  |					velkost: Stack_Start - Heap_End
//	|                   |
//	|                   |
//	|                   |
//	+-------------------+	Stack_End
//	
//	+-------------------+	Heap_End
//	|                   |
//	|                   |
//	|                   |
//	|        ^    heap  |					velkost: Heap_End - Heap_Start
//	|        |          |
//	+--------+----------+	Heap_Start		-> definovane az po linkovani
//	+-------------------+	
//	|       ZI          |
//	+--------+----------+
//	|       RW          |
//	+--------+----------+
//	|       RO          |
//	+--------+----------+	default: ......


// alebo pri volbe : "USE_TWO_REGION_MODE" (KEIL) :  STACK a HEAP zacina na adrese BASE_LIMIT a pokracuju smerom od seba. Stack klesa, Heap stupa
//	+-------------------+
//	|                   |
//	|                   |
//	|                   |
//	|                   |
//	|                   |
//	+-------------------+	Heap_Limit (Heap_End
//	|                   |
//	|                   |
//	|        ^    heap  |
//	|        |          |
//	|        |          |
//	+--------+----------+	Heap_Base	(Heap_Start)	

//	+--------+----------+	Stack_Base	(Stack_Start)	
//	|        |          |
//	|        |          |
//	|        v   stack  |	
//	|                   |
//	|                   |
//	+-------------------+	Stack_Limit (Stack_End)
//	|                   |
//	|                   |
//	|                   |
//	|                   |
//	|                   |
//	+-------------------+	

//	Stack stores:
//		- local variables
//		- return addresses
//		- function arguments
//		- compiler temporaries
//		- interrupt contexts
// 	The life span of variables on the stack is limited to the duration of the function. 
//	As soon as the function returns, the used stack memory will be free for use by subsequent function calls.

// Heap stores:
//		- Transient data objects
//		- malloc, alloc,...


void sys_Reset_Handler(void);															// prototype

#if defined(COMP_TYPE_XPR)															// NXP MCUXpresso
#if defined (__REDLIB__)
extern void __main(void);
#else
extern int main(void);																// All others
#endif


// ------------------------------------------------------------------------------------------------------
// Functions to carry out the initialization of RW and BSS data sections. These
// are written as separate functions rather than being inlined within the
// ResetISR() function in order to cope with MCUs with multiple banks of
// memory.
// ------------------------------------------------------------------------------------------------------
//__attribute__ ((section(".after_vectors")))
//void data_init(unsigned int romstart, unsigned int start, unsigned int len)
//{
//	unsigned int *pulDest = (unsigned int*) start;
//	unsigned int *pulSrc = (unsigned int*) romstart;
//	unsigned int loop;
//	for (loop = 0; loop < len; loop = loop + 4)
//		*pulDest++ = *pulSrc++;
//}


//__attribute__ ((section(".after_vectors")))
//void bss_init(unsigned int start, unsigned int len)
//{
//	unsigned int *pulDest = (unsigned int*) start;
//	unsigned int loop;
//	for (loop = 0; loop < len; loop = loop + 4)
//		*pulDest++ = 0;
//}

#endif


// ******************************************************************************************************
// Functions prototypes
// ******************************************************************************************************
void sys_Clock_Refresh(void)	__SECTION_FINI;
#if defined(CONF_SYS_PART_INFO) && (CONF_SYS_PART_INFO == 1)
void sys_Part_Refresh(void)		__SECTION_FINI;
#endif
#if defined(CONF_SYS_MEM_INFO) && (CONF_SYS_MEM_INFO == 1)
void sys_Memory_Refresh(void)	__SECTION_FINI;
#endif
void SystemInit(void)			__SECTION_FINI;
//void sys_WatchDog_Init(void);

int sys_Err(const char *fmt, ...);

uint32_t 	cpuSR[8];
uint8_t		cpuSR_Lock = 0;
volatile uint8_t IRQ_Status = 1;

// ******************************************************************************************************
// PUBLIC Variables with prefilled constants
// ******************************************************************************************************
sys_Status_t	sys_Status				__SECTION_NOINIT;							// notinit! System status
uint32_t		sys_MemMark  			__SECTION_NOINIT;							// memory mark
#if (CONF_SYS_ALARMS_USED == 1)
sys_Alarms_t 	sys_Alarms				__SECTION_ZEROINIT;							// clear at start
#endif


#if (CONF_SYS_TASKS_USED == 1)
sys_Tasks_t 	sys_Tasks				__SECTION_ZEROINIT;							// clear at start
#endif

sys_WatchDog_t	sys_WatchDog			__SECTION_ZEROINIT;							// clear at start


sys_Counters_t	sys_Cnts				__SECTION_ZEROINIT;							// System counters and time structure


#if defined(CONF_SYS_PART_INFO) && (CONF_SYS_PART_INFO == 1)
sys_Part_t		sys_Part 				__SECTION_DATA	=							// information structure about Part
{
	.IRam			= 	_CHIP_IRAM,
	.IFlash		 	= 	_CHIP_IFLASH,
#if defined(_CHIP_IEEPROM)	
	.IEeprom		= 	_CHIP_IEEPROM,
#else
	.IEeprom		= 	0,
#endif	
#ifdef _CHIP_IEEPROM_START
	.IEeprom_Start	=	_CHIP_IEEPROM_START,
#else
	.IEeprom_Start	=	0,	
#endif
	.Part_Refresh	=	sys_Part_Refresh,
};
#endif	

#if defined(CONF_SYS_APP_INFO) && (CONF_SYS_APP_INFO == 1)
const sys_App_t 		sys_App 		__SECTION_RODATA =							// Application information structure
{																					// Fill it ...
	.System_ID		= 	{SYSTEM_NAME_STRING},
	.System_Ver 	= 	{SYSTEM_VERSION_STRING},
	.ID 			= 	{CONF_APP_NAME_STRING},
	.Ver 			= 	{CONF_APP_VERSION_STRING},
	.Target			= 	{CONF_APP_TARGET},
	.BSP_ID			=	{BSP_TYPE_STRING},
	.BSP_Ver		=	{BSP_VERSION_STRING},
	.Builder_Ver 	= 	{TOOLCHAIN_VERSION_STRING},
	.Builder_Parm	=	{TOOLCHAIN_PARM_STRING},
	.Builder_Lib	=	{TOOLCHAIN_LIB_STRING},
	.Builder_DateTime=	{BUILD_DATETIME_STRING},
	.BuildDateInt	=	(BUILD_YEAR * 10000) + (BUILD_MONTH * 100) + BUILD_DAY,		// Build Date save as Integer
	.BuildTimeInt	=	(BUILD_HOUR * 10000) + (BUILD_MIN *100) + BUILD_SEC,		// Build Time save as Integer
};
#endif


sys_Clock_t		sys_Clock =	 
{
	.SRC_Freq			=	0,														// clock source frequency (crystal, IRC, ext, clock,...)
	.CPU_Freq			=	0,														// Core CPU clock - frekvencia samotneho jadra (Core, CPU,...)
	.Clock_Refresh 		=  sys_Clock_Refresh,										// reload actual chip clock information
};


const sys_System_t 	sys_System 			__SECTION_RODATA =							// load systemovej strukturhy
{
#if defined (CONF_SYS_APP_INFO) && (CONF_SYS_APP_INFO == 1)	
	.App  				= &sys_App,													// Link to App structure
#endif	
#if defined(CONF_SYS_PART_INFO) && (CONF_SYS_PART_INFO == 1)
	.Part 				= &sys_Part,												// Link to Part structure
#endif	
	.Clock				= &sys_Clock,												// Link to clock related structure
	.Status				= &sys_Status,												// Link to Status structure
	.Cnts 				= &sys_Cnts,												// Link to Counter structure
#if (CONF_SYS_TASKS_USED == 1)
	.Tasks				= &sys_Tasks,
#endif
#if (CONF_SYS_ALARMS_USED == 1)
	.Alarms				= &sys_Alarms,
#endif	
	.MemMark			= &sys_MemMark,
	.WatchDog 			= &sys_WatchDog,
#if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)
	.ModLoaded			= &modmgr_List,
#endif	
#if defined(CONF_USE_DRV_MANAGER_EXT) && (CONF_USE_DRV_MANAGER_EXT == 1)	
	.DrvLoaded			= &drvmgr_List,
#endif	
};



// ------------------------------------------------------------------------------------------------------
// NULL Function
// ------------------------------------------------------------------------------------------------------
// used as default initializer for drivers API funct.
// if user not set a new one, default NULL is called.
void _NULL_Function(void)
{
	dbgprint("\r\n.NULL function called! .\r\n");
}

void sys_NULL_Function			(void) __WEAK_("_NULL_Function");


// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************
volatile uint_fast16_t					irq_SysTickCnt;									// 10 ms counter
bool									irq_SysTickCnt_Updated;							// SysTickCnt update flag
volatile bool 							Ignore_BusFaultHandler = false;				// ignore bus fault flag.


// ******************************************************************************************************
// ******************************************************************************************************
//  					INTERRUPT HANDLERS - SYSTEM (first 16 interrupts) - Processor Exceptions
// ******************************************************************************************************
// ******************************************************************************************************
//				Next interrupt routines you cn find in App_IRQ.c file - Device Specific Vectors
// ------------------------------------------------------------------------------------------------------
// Mozem "ukradnut" RESET vector pre seba - treba zabezbecit spustenie main()
#if defined(CONF_CATCH_RESET_VECTOR) && (CONF_CATCH_RESET_VECTOR == 1)
extern int __scatterload(void);

#if defined(COMP_TYPE_UV)															// ARM KEIL Compiler
// User's definied reset Handler. Original one is in the startup_LPCxxx.s file
void sys_Reset_Handler(void) 														// IRQ 1
#elif defined(COMP_TYPE_XPR)														// NXP MCUXpresso
__attribute__ ((section(".after_vectors")))
void ResetISR(void)
#endif	
{
	//__heapstats((__heapprt)dbgprint, stdout);
	
	//CHAL_Chip_SystemInit();														// MCU Chip base init - inlining
	_Chip_SystemInit();
	
	sys_Status.RunState = sSYSINIT;
	
	bsp_Chip_Init();																// MCU Chip init based on board HW

	CHAL_Chip_Read_ResetSource(&sys_Status.Reset);									// Get reset source
	sys_Status.RunState = sRESET;

	
#if defined(COMP_TYPE_UV)															// ARM KEIL Compiler
	__scatterload();																// memory initialization - KEIL
	// clear variables, HEAP, and call to main()

#elif defined(COMP_TYPE_XPR)														// NXP MCUXpresso
	#if defined(BSP_ALLOW_SWO) && (BSP_ALLOW_SWO == 1)								// Serial Wire Output ?
		volatile unsigned int *TRACECLKDIV = (unsigned int *) 0x400740D8;
		*TRACECLKDIV = 1;
	#endif

	// Copy the data sections from flash to SRAM.
	uint32_t LoadAddr, ExeAddr, SectionLen;
	uint32_t *SectionTableAddr;

    // Load base address of Global Section Table
    SectionTableAddr = &__data_section_table;

    // Copy the data sections from flash to SRAM.
    while (SectionTableAddr < &__data_section_table_end) 
	{
        LoadAddr = *SectionTableAddr++;
        ExeAddr = *SectionTableAddr++;
        SectionLen = *SectionTableAddr++;
        data_init(LoadAddr, ExeAddr, SectionLen);
    }
    // At this point, SectionTableAddr = &__bss_section_table;
    // Zero fill the bss segment
    while (SectionTableAddr < &__bss_section_table_end) 
	{
        ExeAddr = *SectionTableAddr++;
        SectionLen = *SectionTableAddr++;
        bss_init(ExeAddr, SectionLen);
    }	
	#if defined (__REDLIB__)
     __main();																		// Call the Redlib library, which in turn calls main()
	#else
	main();
	#endif	
#endif
	
	
}
#endif

// ------------------------------------------------------------------------------------------------------
// Non Maskable Interrupt handler
// ------------------------------------------------------------------------------------------------------
void sys_NMI_Handler(void) 															// IRQ 2
{
	sys_System.Status->RunState = sHALT;
#if defined(CONF_DEBUG_SUBSYS) && (CONF_DEBUG_SUBSYS == 1)	
	dbgprint("\r\n.NMI handler asserted!\r\n");
#endif	
	ERR_BREAK;
}

// ------------------------------------------------------------------------------------------------------
// Hard Fault Interrupt handler
// ------------------------------------------------------------------------------------------------------
#if !CONF_USE_DEBUG																	// If DEBUG is used, call Hard fault in the debug.c !!!!!
void sys_HardFault_Handler(void)  													// IRQ 3
{
	sys_System.Status->RunState = sHALT;
#if defined(CONF_DEBUG_SUBSYS) && (CONF_DEBUG_SUBSYS == 1)		
	dbgprint("\r\n.HardFault handler asserted!\r\n");
#endif	
	ERR_BREAK;
}
#endif

// ------------------------------------------------------------------------------------------------------
// Mem Manage Interrupt handler
// ------------------------------------------------------------------------------------------------------
void sys_MemManage_Handler(void)  													// IRQ 4
{
	sys_System.Status->RunState = sHALT;
#if defined(CONF_DEBUG_SUBSYS) && (CONF_DEBUG_SUBSYS == 1)		
	dbgprint("\r\n.MemManage handler asserted!\r\n");
#endif	
	ERR_BREAK;
}

// ------------------------------------------------------------------------------------------------------
// BUS Fault Interrupt handler
// ------------------------------------------------------------------------------------------------------
void sys_BusFault_Handler(void)  													// IRQ 5
{
	sys_System.Status->RunState = sHALT;
#if defined(CONF_DEBUG_SUBSYS) && (CONF_DEBUG_SUBSYS == 1)		
#if (__CORTEX_M > 0)
	dbgprint("\r\n.BusFault handler asserted at[BFAR]:0x%08X with status[BFSR]:0x%04lx!\r\n", SCB->BFAR, (( SCB->CFSR & SCB_CFSR_BUSFAULTSR_Msk) >> SCB_CFSR_BUSFAULTSR_Pos));
#else
	dbgprint("\r\n.BusFault handler asserted !\r\n");
#endif	
#endif	
	ERR_BREAK;
	// ak nastava PRECISERR a zaroven aj IMPRECISERR, chod do sys_Reset_Handler a odkomentuj SCnSCB->ACTLR |= 1 << SCnSCB_ACTLR_DISDEFWBUF_Pos;
	// zakaze sa tym write buffering a dozvies sa presnu adresu instrukcie, ktora sposobila fault.
}

// ------------------------------------------------------------------------------------------------------
// Usage Fault Interrupt handler
// ------------------------------------------------------------------------------------------------------
void sys_UsageFault_Handler(void)  													// IRQ 6
{
	sys_System.Status->RunState = sHALT;
#if defined(CONF_DEBUG_SUBSYS) && (CONF_DEBUG_SUBSYS == 1)		
	dbgprint("\r\n.UsageFault handler asserted!\r\n");
#endif	
	ERR_BREAK;
}

// Reserved interrupt handler														// IRQ 7
// Reserved interrupt handler														// IRQ 8
// Reserved interrupt handler														// IRQ 9
// Reserved interrupt handler														// IRQ 10

// ------------------------------------------------------------------------------------------------------
// SVC Interrupt handler
// ------------------------------------------------------------------------------------------------------
void sys_SVC_Handler(void) 															// IRQ 11
{
	sys_System.Status->RunState = sHALT;
#if defined(CONF_DEBUG_SUBSYS) && (CONF_DEBUG_SUBSYS == 1)		
	dbgprint("\r\n.SVC exception! HALTED.\r\n");
#endif	
	ERR_BREAK;
}

// ------------------------------------------------------------------------------------------------------
// DebugMon Interrupt handler
// ------------------------------------------------------------------------------------------------------
void sys_DebugMon_Handler(void) 													// IRQ 12
{
	sys_System.Status->RunState = sHALT;
#if defined(CONF_DEBUG_SUBSYS) && (CONF_DEBUG_SUBSYS == 1)		
	dbgprint("\r\n.DebugMon handler asserted!\r\n");
#endif	
	ERR_BREAK;
}

// Reserved interrupt handler														// IRQ 13

// ------------------------------------------------------------------------------------------------------
// PendSV Interrupt handler
// ------------------------------------------------------------------------------------------------------
void sys_PendSV_Handler(void)  														// IRQ 14
{
	sys_System.Status->RunState = sHALT;
#if defined(CONF_DEBUG_SUBSYS) && (CONF_DEBUG_SUBSYS == 1)		
	dbgprint("\r\n.PENDSV exception! HALTED.\r\n");
#endif	
	ERR_BREAK;
}

// ------------------------------------------------------------------------------------------------------
// System Tick-Tack....
// ------------------------------------------------------------------------------------------------------
void sys_SysTick_Handler(void)  													// IRQ 15
{
	irq_SysTickCnt ++;																// update systick
	irq_SysTickCnt_Updated = true;													// set flag
	if(irq_SysTickCnt >= 1) sys_IRTC_Update();									// 
}

// ------------------------------------------------------------------------------------------------------
// System Tick-Tack....
// ------------------------------------------------------------------------------------------------------
void _Chip_WDT0_IRQ_Handler(void)  													// IRQ 16
{
	sys_Err("Watchdog Reset");
}



// ******************************************************************************************************
// ******************************************************************************************************
// 						BOOT FUNCTIONS
// ******************************************************************************************************
// ******************************************************************************************************
__WEAK_FUNC void bsp_Debug_Low(void);												// default function if BSP doesn't it containt
__WEAK_FUNC void bsp_Debug_Low(void)												// set debug hardware
{
}

__WEAK_FUNC void bsp_Init(void);													// default function if BSP doesn't it containt
__WEAK_FUNC void bsp_Init(void)														// set BSP Hardware
{
}

__WEAK_FUNC void bsp_Set_SystemPinDefault(void);									// default function if BSP doesn't it containt
__WEAK_FUNC void bsp_Set_SystemPinDefault(void)										// set BSP Hardware
{
}

// ------------------------------------------------------------------------------------------------------
// Print system information to debug output
// ------------------------------------------------------------------------------------------------------
void sys_System_Info_Print(void)
{
#if defined(CONF_SYS_INFO) && (CONF_SYS_INFO == 1)
	dbgprint("\r\n%s: %s Loading", SYSTEM_NAME_STRING, SYSTEM_VERSION_STRING);		// Skeleton vx.x Loading
#if defined(CONF_SYS_APP_INFO) && (CONF_SYS_APP_INFO == 1)
	dbgprint("\r\nApp: %s, v: %s, %s", sys_System.App->ID, sys_System.App->Ver, sys_System.App->Target);// Par teplych vypisov
	dbgprint("\r\n +-- Compiled by %s [%s + %s] at %s", sys_System.App->Builder_Ver, sys_System.App->Builder_Parm, sys_System.App->Builder_Lib, sys_System.App->Builder_DateTime);
	dbgprint("\r\nBSP: %s, v: %s", sys_System.App->BSP_ID, sys_System.App->BSP_Ver);// Par teplych vypisov
#endif	
	Delay_ms(100);
#endif

#if defined(CONF_SYS_PART_INFO) && (CONF_SYS_PART_INFO == 1)
	sys_System.Part->Part_Refresh();												// nacitaj ID a SERNUM informacie o host MCU
	#if CONF_SYS_INFO
	dbgprint("\r\nClock Src: %ld, CPU: %ld", (long int) sys_System.Clock->SRC_Freq, (long int) sys_System.Clock->CPU_Freq);
	dbgprint("\r\nCPUID: 0x%08X", sys_System.Part->PARTID);							// vypis CPUID
	dbgprint(" @ %dkHz", (sys_System.Clock->CPU_Freq) / 1000);						// aj frekvenciu na ktorej bezi
	dbgprint("\r\nCPU SERNUM: 0x%08X %08X %08X %08X", sys_System.Part->SERNUM[3], sys_System.Part->SERNUM[2], sys_System.Part->SERNUM[1], sys_System.Part->SERNUM[0] );
	Delay_ms(100);
	#endif
#endif

#if defined(CONF_SYS_MEM_INFO) && (CONF_SYS_MEM_INFO == 1)
	sys_System.Memory->Memory_Refresh;												// nacitaj MEM informacie o host MCU

	#if CONF_SYS_INFO
	dbgprint("\r\nMemory:");
	dbgprint("\r\n +-- OnChip: FLASH/RAM/EEPROM: 0x%X/0x%X/0x%X bytes", (uint32_t) sys_System.Part->IFlash, (uint32_t) sys_System.Part->IRam, (uint16_t) sys_System.Part->IEeprom);
	#if defined (CONF_SYS_STACKCHECKK) && (CONF_SYS_STACKCHECK == 1)
	dbgprint("\r\n +-- HEAP Size: 0x%lX (%ld) bytes", (uint16_t) System.Part->Heap_Size, (long int) System.Part->Heap_Size);
	dbgprint("\r\n +-- STACK Size: 0x%lX (%ld) bytes", (uint16_t) System.Part->Stack_Size, (long int) System.Part->Stack_Size);
	dbgprint("\r\n\t +-- Checked STACK/HEAP.");
	Delay_ms(100);
	#endif
	#endif
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Called in Main as first function
// Load _Skeleton system
// ------------------------------------------------------------------------------------------------------
void sys_System_Load(void)
{
	bsp_Set_SystemPinDefault();														// Set IO pins to preddefined state (without debug !)
	bsp_Chip_Set_SystemClocking();													// set user clocking
	
	//sys_System.Clock->Clock_Refresh();											// Reload clock information
	sys_Clock_Refresh();
	
#if CONF_USE_DEBUG
	sys_Status.RunState = sDBGINIT;
	bsp_Debug_Low();																// Switch board HW for LowLevel debug (JTAG/SWD), if needs
	
	Debug_Init();																	// Initialize Debug system (UART, LCD...)
#endif	

#if defined (CONF_USE_MEM_MANAGER) && (CONF_USE_MEM_MANAGER == 1)					// If we use memory manager
	sys_Status.RunState = sMMGRINIT;
	MemManager_Init();																// Initialize it
#endif

	sys_Status.RunState = sBSPINIT;
	bsp_Init();																		// Init board

	sys_System_Info_Print();
}

// ------------------------------------------------------------------------------------------------------
// Called in Main as last function before loop(1)
// Starting _Skeleton system
// ------------------------------------------------------------------------------------------------------
void sys_System_Start(void)
{
	sys_System.Status->RunState = sMAIN;		
	dbgprint("\r\nEntering main loop.\r\n");

	// If WDT is ready, enable it:
	if(sys_WatchDog.Initialized) sys_WatchDog.Enable = CHAL_WDT_Enable(sys_WatchDog.pPeri, true);
}

// ------------------------------------------------------------------------------------------------------
// Called in Main Loop periodically (inserted to main)
// ------------------------------------------------------------------------------------------------------
// Executing _Skeleton system
inline void sys_System_Execute(void)
{
//	SYS_CPUCRIT_START();
//	if(sys_System.WatchDog.Enable) CHAL_WDT_Reset(sys_System.WatchDog.pPeri);// reset watchdog
//	SYS_CPUCRIT_END();
//	sys_IRTC_Update();
	
#if (CONF_SYS_TASKS_USED == 1)
	sys_Task_Execute_Item_First();													// Execute Task if any.
#endif	
}


// ******************************************************************************************************
// ******************************************************************************************************
// 						TASK SYSTEM
// ******************************************************************************************************
// ******************************************************************************************************


#if (CONF_SYS_TASKS_USED == 1)

COMP_PUSH_OPTIONS
COMP_OPTIONS_O3


sys_Tasks_t 	Tasks;
void DummyTask(void *Argv);

// ------------------------------------------------------------------------------------------------------
//  Find pItem in pColList. If found - result is ptr to record. otherwise NULL
// ------------------------------------------------------------------------------------------------------
_ColListRecord_t *sys_Task_Find_Item(_ColList_t *pColList, void *pItem)
{
	void* res = NULL;
	_ColListRecord_t *tmpListRecord = pColList->pFirst;								// create local temporary Collection list
	
	
	if(pColList->pFirst == NULL) goto FindTaskExit;									// collection is empty	
	if(pItem == NULL) goto FindTaskExit;
	if(pColList == NULL) goto FindTaskExit;
	
	tmpListRecord = pColList->pFirst;												// select first item
	while(tmpListRecord)
	{
		if(tmpListRecord->pValue != NULL)
		{
			if(((sys_TaskRecord_t*) tmpListRecord->pValue)->pCode == pItem )res = tmpListRecord;	// item found. Return address to stored record
		}
		if(tmpListRecord->pNext != NULL) tmpListRecord = tmpListRecord->pNext;
		else break;
	}

FindTaskExit:
	
		
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Create an Task
// ------------------------------------------------------------------------------------------------------
CMP_KWD_PURE void sys_Task_Create_Item( const char * const Name, void (*pCode), sys_TaskArgs_t Argv)
{


#if defined(CONF_DEBUG_SUBSYS_TASK) && (CONF_DEBUG_SUBSYS_TASK == 1)
	bool res = false;	
	dbgprint("\r\n("SYS_OBJECTNAME")-Task - Create [%s]:%p", Name, pCode);
#endif
	
	if(sys_Task_Find_Item( &Tasks.TaskList, pCode) != NULL) return;					// task exist. Return

	
	sys_TaskRecord_t *TaskRecord;
	TaskRecord = (void *) sys_malloc(sizeof(sys_TaskRecord_t));						// allocate piece of memory for task

#if defined(CONF_DEBUG_STRING_NAME_SIZE) && (CONF_DEBUG_STRING_NAME_SIZE > 0)		// if text naming are enabled	
	TaskRecord->Name = Name;														// assign parameters
#endif	
	TaskRecord->pCode = pCode;														// ptr to execute code
	TaskRecord->Argv = Argv;														// arguments

#if defined(CONF_DEBUG_SUBSYS_TASK) && (CONF_DEBUG_SUBSYS_TASK == 1)
	res = sys_DynCol_Add_Item (&Tasks.TaskList, TaskRecord);						// add to collection as new task
	dbgprint("\r\n("SYS_OBJECTNAME")-Task - Create [%s]:%p = %s", Name, pCode, res==true? "Done":"Fail");
#else
	sys_DynCol_Add_Item (&Tasks.TaskList, TaskRecord);								// add to collection as new task
#endif	
}


// ------------------------------------------------------------------------------------------------------
// Execute and destroy Task
// ------------------------------------------------------------------------------------------------------
void sys_Task_Execute_Item_First(void)
{
	static sys_TaskRecord_t *tmpTaskRecord;												// create driver ptr if not exist
	void (*TargetFun)();
	static _ColListRecord_t *tmpListRecord = NULL;

	
	tmpListRecord = sys_DynCol_Get_Record(&Tasks.TaskList, tmpListRecord);			// temporary pointer to parent module list and get next to tmpListRecord
	if (tmpListRecord == NULL) return;
		
	// now, execute task:

	tmpTaskRecord = tmpListRecord->pValue;											// create driver ptr
	
	uint8_t i, _argcount = tmpTaskRecord->Argv.ArgCount;							// Read_StackHeap argument count

#if defined(CONF_DEBUG_SUBSYS_TASK) && (CONF_DEBUG_SUBSYS_TASK == 1)
	bool res = false;	
	dbgprint("\r\n("SYS_OBJECTNAME")-Task - Execute");
#if defined(CONF_DEBUG_STRING_NAME_SIZE) && (CONF_DEBUG_STRING_NAME_SIZE > 0)		// if text naming are enabled	
	dbgprint(" [%s]:%p", tmpTaskRecord->Name, tmpTaskRecord->pCode);
#endif	
#endif	
			
	if(tmpTaskRecord->Argv.ArgCount)
	{
		void *_arg[_argcount];														// create temporary argument array
		
		i = 0;
		do
		{
			_arg[i++] = tmpTaskRecord->Argv.Arg;									// copy argument to array
		}while(--_argcount);
		TargetFun = (void (*)()) tmpTaskRecord->pCode;								// assign function address
		TargetFun(_arg[0]);
	}
	else
	{	
		TargetFun = (void (*)()) tmpTaskRecord->pCode;								// assign function address			
		TargetFun();
	}
	sys_free(tmpTaskRecord);														// free memory - clear executed task

#if defined(CONF_DEBUG_SUBSYS_TASK) && (CONF_DEBUG_SUBSYS_TASK == 1)
	res  = sys_DynCol_Remove_Item (&Tasks.TaskList, tmpTaskRecord);					// remove first from collection	of tasks
 #if defined(CONF_DEBUG_STRING_NAME_SIZE) && (CONF_DEBUG_STRING_NAME_SIZE > 0)		// if text naming are enabled
	dbgprint("\r\n("SYS_OBJECTNAME")-Task - Execute: %s. RemoveTask: [%s]:%p", res==true? "Done":"Fail", tmpTaskRecord->Name, tmpTaskRecord->pCode );
 #else
	dbgprint("\r\n("SYS_OBJECTNAME")-Task - Execute: %s. RemoveTask: %p", res==true? "Done":"Fail", tmpTaskRecord->pCode );
 #endif	
#else
	sys_DynCol_Remove_Item (&Tasks.TaskList, tmpTaskRecord);						// remove first from collection	of tasks	
#endif		
}
COMP_POP_OPTIONS

#endif		// end of  if (CONF_SYS_TASKS_USED == 1)




// ******************************************************************************************************
// ******************************************************************************************************
// 						ALARM SYSTEM
// ******************************************************************************************************
// ******************************************************************************************************

#if (CONF_SYS_ALARMS_USED == 1)

COMP_PUSH_OPTIONS
COMP_OPTIONS_O3


void DummyAlarm(void *Argv);

// ------------------------------------------------------------------------------------------------------
// Create an Alarm
// ------------------------------------------------------------------------------------------------------
sys_AlarmRecord_t *sys_Alarm_Create_Item( const char * const Name, uint32_t RepeatMS, void (*pCode), sys_TaskArgs_t Argv)
{

#if defined(CONF_DEBUG_SUBSYS_TASK) && (CONF_DEBUG_SUBSYS_TASK == 1)
	bool res = false;	
	dbgprint("\r\n("SYS_OBJECTNAME")-Alarm - Create [%s]:%p", Name, pCode);
#endif
	
	//if(sys_FindIn_TaskList( &Alarms.AlarmList, pCode) != NULL) return;			// Alarm exist. Return
	
	sys_AlarmRecord_t *AlarmRecord;
	AlarmRecord = (void *) sys_malloc(sizeof(sys_AlarmRecord_t));					// allocate piece of memory for Alarm task
	
#if defined(CONF_DEBUG_SUBSYS_TASK) && (CONF_DEBUG_SUBSYS_TASK == 1)
	AlarmRecord->Name = Name;														// assign parameters
#endif	
	
	AlarmRecord->pCode = pCode;														// ptr to execute code
	AlarmRecord->Argv = Argv;														// arguments
	AlarmRecord->RepeatMS = RepeatMS;
	AlarmRecord->LastAction = 0;
	sys_DynCol_Add_Item (&sys_Alarms.AlarmList, AlarmRecord);						// add to collection as new Alarm
	return (AlarmRecord);
}


// ------------------------------------------------------------------------------------------------------
// Execute and destroy Task
// ------------------------------------------------------------------------------------------------------
void sys_Alarm_Destroy_Item(sys_AlarmRecord_t *AlarmRecord)
{
	sys_DynCol_Remove_Item (&sys_Alarms.AlarmList, AlarmRecord);					// remove alarm from collection	of alarms
}
COMP_POP_OPTIONS

#endif





// ******************************************************************************************************
// ******************************************************************************************************
//  					MEMORY FUNCTIONS (if don't use Memory Manager)
// ******************************************************************************************************
// ******************************************************************************************************
#if !defined(CONF_USE_MEM_MANAGER) || ( CONF_USE_MEM_MANAGER == 0)
#include <stdlib.h>
//void *memcpy(void *dest, const void *src, size_t n) __attribute__((weak, alias("sys_memcpy")));
//void *memset(void *s, int c, size_t n) __attribute__((weak, alias("sys_memset")));
//void *memmove(void *dest, const void *src, size_t n) __attribute__((weak, alias("sys_memmove")));
//int   memcmp(const void *s1, const void *s2, size_t n) __attribute__((weak, alias("sys_memcmp")));


#if defined(CONF_MEMALLOCHISTSIZE) && (CONF_MEMALLOCHISTSIZE > 0)
sys_Mem_Alloc_t		MemAllocHist[CONF_MEMALLOCHISTSIZE] __SECTION_DATA;
#endif

uint16_t		MemAllocLast = 0;
uint16_t		MemAllocCount = 0;
uint64_t		MemAllocOccupied = 0;

const sys_MemHist_t sys_MemHist = 
{
	.pOccupied = &MemAllocOccupied,
	.pAllocCount = &MemAllocCount,
	.pFragmLast  = &MemAllocLast,
#if defined(CONF_MEMALLOCHISTSIZE) && (CONF_MEMALLOCHISTSIZE > 0)
	.pHistory = (sys_Mem_Alloc_t*) &MemAllocHist,
#endif	
};

// ------------------------------------------------------------------------------------------------------
// save Allocation in history
// ------------------------------------------------------------------------------------------------------
#if defined(CONF_MEMALLOCHISTSIZE) && (CONF_MEMALLOCHISTSIZE > 0)
void sys_Alloc_Save_History(uint8_t *Address, size_t nbytes)
{

	MemAllocHist[MemAllocLast].Address = Address;									// save to history
	MemAllocHist[MemAllocLast].AllocTimeMark = *sys_Cnts.SysTickCount;				// save timemark
	MemAllocHist[MemAllocLast].nbytes = nbytes;										// save size in bytes
	MemAllocCount ++;
	MemAllocOccupied += nbytes;
	
	uint8_t totcnt=0;
	while(MemAllocHist[MemAllocLast].Address != NULL)								// find next free in array
	{
		if(++MemAllocLast > CONF_MEMALLOCHISTSIZE) MemAllocLast = 0;				// move to next free 
		
		totcnt ++;																	// rotate through array only one time
		if (totcnt > CONF_MEMALLOCHISTSIZE) 										// not free child in array!
		{
			sys_Err ("Mem Alloc history is FULL!");	
			break;
		}
	}
}

// ------------------------------------------------------------------------------------------------------
// remove Allocation from history
// ------------------------------------------------------------------------------------------------------
bool sys_Alloc_Remove_History(uint8_t *Address)
{
	uint8_t tmpptr =0;
	while(tmpptr <= CONF_MEMALLOCHISTSIZE)											// find Address 
	{
		if(MemAllocHist[tmpptr].Address == Address)									// found it
		{
			MemAllocHist[tmpptr].Address = NULL;									// clear record
			MemAllocOccupied -= MemAllocHist[tmpptr].nbytes;
			MemAllocCount --;
		
			return(true);
		}
		tmpptr ++;
	}
	return(false);
}
#endif

// ------------------------------------------------------------------------------------------------------
// allocate memory and fill to zero
// ------------------------------------------------------------------------------------------------------
void *sys_malloc_zero( unsigned int nbytes )
{
	unsigned char *p;

#if defined(CONF_DEBUG_SUBSYS_DYNMEM) && (CONF_DEBUG_SUBSYS_DYNMEM == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DYNMEM - Malloc+FillZero %d bytes", nbytes);
#endif
	
	p = sys_malloc(nbytes);
	if(p != NULL) 
	{
		sys_memset_zero (p, nbytes);
	}
	
#if defined(CONF_DEBUG_SUBSYS_DYNMEM) && (CONF_DEBUG_SUBSYS_DYNMEM == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DYNMEM - Malloc+FillZero %d bytes[@ %p]: %s", nbytes, p, p!=NULL? "Done":"Fail");
#endif
	return(p);
}

// ------------------------------------------------------------------------------------------------------
// allocate memory
// ------------------------------------------------------------------------------------------------------
void *sys_malloc( unsigned int nbytes )
{
	unsigned char *p = NULL;

#if defined(CONF_DEBUG_SUBSYS_DYNMEM) && (CONF_DEBUG_SUBSYS_DYNMEM == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DYNMEM - Malloc %d bytes", nbytes);
#endif

	if(MemAllocOccupied + nbytes < CONF_HEAP_SIZE)
	{
		SYS_CPUCRIT_START();
		p = malloc(nbytes);
		SYS_CPUCRIT_END();
	}
	
	if(p == NULL)
	{
#if defined(CONF_DEBUG_SUBSYS_DYNMEM) && (CONF_DEBUG_SUBSYS_DYNMEM == 1)
		dbgprint(" - Cannot Allocate");
#endif
	}
	
#if defined(CONF_MEMALLOCHISTSIZE) && (CONF_MEMALLOCHISTSIZE > 0)	
	else sys_Alloc_Save_History(p, nbytes);
#endif	
	
#if defined(CONF_DEBUG_SUBSYS_DYNMEM) && (CONF_DEBUG_SUBSYS_DYNMEM == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DYNMEM - Malloc %d bytes[@ %p]: %s", nbytes, p, p!=NULL? "Done":"Fail");
#endif
	return(p);
}

// ------------------------------------------------------------------------------------------------------
// reallocate memory
// ------------------------------------------------------------------------------------------------------
void *sys_realloc( void *mem, unsigned int nbytes )
{
#if defined(CONF_DEBUG_SUBSYS_DYNMEM) && (CONF_DEBUG_SUBSYS_DYNMEM == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DYNMEM - ReAlloc [@ %p] to %d bytes", mem, nbytes);
#endif

	if(MemAllocOccupied + nbytes < CONF_HEAP_SIZE)
	{
		SYS_CPUCRIT_START();
		mem = realloc( mem, nbytes);
		SYS_CPUCRIT_END();
	}
	
	if(mem == NULL)
	{
#if defined(CONF_DEBUG_SUBSYS_DYNMEM) && (CONF_DEBUG_SUBSYS_DYNMEM == 1)
		dbgprint(" - Cannot ReAllocate");
#endif
	}
	
#if defined(CONF_MEMALLOCHISTSIZE) && (CONF_MEMALLOCHISTSIZE > 0)	
	else sys_Alloc_Save_History(p, nbytes);
#endif	
	
#if defined(CONF_DEBUG_SUBSYS_DYNMEM) && (CONF_DEBUG_SUBSYS_DYNMEM == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DYNMEM - ReAlloc %d bytes[@ %p]: %s", nbytes, mem, mem!=NULL? "Done":"Fail");
#endif
	return(mem);
}
// ------------------------------------------------------------------------------------------------------
// release memory
// ------------------------------------------------------------------------------------------------------
void sys_free( void* mem )
{
#if defined(CONF_DEBUG_SUBSYS_DYNMEM) && (CONF_DEBUG_SUBSYS_DYNMEM == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DYNMEM - Free from %p ", mem);
#endif
	
	SYS_CPUCRIT_START();
#if defined(CONF_MEMALLOCHISTSIZE) && (CONF_MEMALLOCHISTSIZE > 0)
	if(sys_Alloc_Remove_History(mem) == false) sys_Err("sys_free error - Cannot find in history list!");
#endif	
	free(mem);
	SYS_CPUCRIT_END();

#if defined(CONF_DEBUG_SUBSYS_DYNMEM) && (CONF_DEBUG_SUBSYS_DYNMEM == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DYNMEM - Free from %p: %s", mem, "Done");
#endif	
}

// ------------------------------------------------------------------------------------------------------
// fill memory to zero
// ------------------------------------------------------------------------------------------------------
void sys_memset_zero( void *dest, unsigned int n )
{
	SYS_CPUCRIT_START();
	memset (dest, 0x00, n);
	SYS_CPUCRIT_END();
}

// ------------------------------------------------------------------------------------------------------
// fill memory with uint8_t
// ------------------------------------------------------------------------------------------------------
void sys_memset( void *dest, uint8_t Sample, unsigned int n )
{
	SYS_CPUCRIT_START();
	memset (dest, Sample, n);
	SYS_CPUCRIT_END();
}

// ------------------------------------------------------------------------------------------------------
// copy memory with uint8_t
// ------------------------------------------------------------------------------------------------------
void sys_memcpy( void *dest, const void *src, unsigned int n )
{
	SYS_CPUCRIT_START();
	memcpy (dest, src, n);
	SYS_CPUCRIT_END();
}

// ------------------------------------------------------------------------------------------------------
// fill memory with uin32_t Sample
// ------------------------------------------------------------------------------------------------------
void sys_memset_4(uint32_t *pDst, uint32_t Sample, uint32_t Count)
{
	SYS_CPUCRIT_START();
	while(Count--)
	{
		*pDst++ = Sample;
	}
	SYS_CPUCRIT_END();
}
	
#endif				// !defined(CONF_USE_MEM_MANAGER) || ( CONF_USE_MEM_MANAGER == 0)





// ******************************************************************************************************
// ******************************************************************************************************
//  					Some SYSTEM FUNCTIONS
// ******************************************************************************************************
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Read an LR register
// ------------------------------------------------------------------------------------------------------
//__attribute__( ( always_inline ) ) static inline uint32_t sysGet_LR(void) 
//{ 
////__return_address
//	register uint32_t result; 

//  __ASM volatile ("MOV %0, LR\n" : "=&r" (result) ); 
//  return(result); 
//} 


//unsigned int __return_address(void);

// ------------------------------------------------------------------------------------------------------
// Empty function. Set as default NULL function for module and driver manager.
// If was called, somthing happens wrong.
// ------------------------------------------------------------------------------------------------------
void sys_Empty(void)
{
	//uint32_t sLR = sysGet_LR();
#if defined(CONF_DEBUG_SUBSYS) && (CONF_DEBUG_SUBSYS == 1)	
//	dbgprint("\r\n |%llu|!!! Fun not set!! Caller address: 0x%04X \r\n", SysTickCnt, sysGet_LR);
	dbgprint("\r\n |%08lld|!!! System Warning! Fun not set:%s:%s %d\r\n", (long long) sys_Cnts.SysTickCount, __MODULE__,  __FUNCTION__, __LINE__);
#else
	ERR_BREAK;
#endif	
}

// ------------------------------------------------------------------------------------------------------
// Randomize generator
// ------------------------------------------------------------------------------------------------------
int sys_rand(void)
{
	return(rand());
}

// ------------------------------------------------------------------------------------------------------
// System Critical Error!!!!!
// Called if program cannot runt more.
// ------------------------------------------------------------------------------------------------------
int sys_Err(const char *fmt, ...) 												//__attribute__((__nonnull__(1)))
{
	sys_Status.RunState = sHALT ;
#if defined(CONF_DEBUG_SUBSYS) && (CONF_DEBUG_SUBSYS == 1)		
	va_list ap;
	//dbgprint("\r\n |%llu|!!! System error ! --> \r \n", SysTickCnt);
	dbgprint("\r\n |%08lld|!!! System error ! --> \r \n", (long long)sys_Cnts.SysTickCount);
    va_start(ap, fmt);
	dbgprint((char *) fmt, ap);
    va_end(ap);
#endif	
	
#ifdef NDEBUG
	sys_Reset_Handler();																// in Release, restart
#else	
	ERR_BREAK;																		// in DEBUG, stop here
#endif
	
	//sys_Reset_Handler();
	return(0);
}




















// ------------------------------------------------------------------------------------------------------
// DELAY microseconds
// waiting delay. minimum is 1 microsecond
// ------------------------------------------------------------------------------------------------------
COMP_PUSH_OPTIONS
COMP_OPTIONS_O0
void Delay_us(uint16_t us)
{
	volatile uint16_t cnt = ((sys_System.Clock->TickPerUS * us) / 11);
	while(cnt)
	{
		__ASM volatile("NOP");
		cnt --;
	};
}
COMP_POP_OPTIONS

// ------------------------------------------------------------------------------------------------------
// DELAY miliseconds
// Waiting delay. minimum is 1 milisecond
// ------------------------------------------------------------------------------------------------------
void Delay_ms(uint16_t ms)
{
	while(ms)
	{
		Delay_us(990);
		ms --;
	};
}


// ------------------------------------------------------------------------------------------------------
// Simulated RTC
// Called periodically from main
// irq_SysTickCnt - was periodicaly incremented in the SysTick Interrupt handler
// ------------------------------------------------------------------------------------------------------
COMP_PUSH_OPTIONS
COMP_OPTIONS_O3

inline void sys_IRTC_Update(void)
{
	static uint32_t			Local_SysTickCnt;										// localtime copy of counter
	
	if(irq_SysTickCnt_Updated)														// flag is set in SysTick interrupt each 1msec
	{
		SYS_CPUCRIT_START();
		Local_SysTickCnt = irq_SysTickCnt;											// store to local 
		irq_SysTickCnt = 0;															// and clear it
		SYS_CPUCRIT_END();

		sys_Cnts.SysTickCount += Local_SysTickCnt;
		irq_SysTickCnt_Updated = false;
	}
	else return;
	
	// 1000 ms Flag
	if ((sys_Cnts.SysTickCount % CONF_SYSTICK_FREQ) == 0)							// each one second indicator... set
	{
		sprintf((char*) &sys_Cnts.RTC_string[0],"%02d:%02d:%02d.%03d\r\n",sys_Cnts.RTC_Hour, sys_Cnts.RTC_Min, sys_Cnts.RTC_Sec, sys_Cnts.RTC_MSec);
		sys_Cnts.SecondChangeFlag = true;
	}
	else
	{
		if((sys_Cnts.SysTickCount & (CONF_SYSTICK_FREQ + 200)) == 0)				// each one second indicator... clear
		{
			sys_Cnts.SecondChangeFlag = false;
		}
	}

	// 500 ms Flag
	if ((sys_Cnts.SysTickCount % (CONF_SYSTICK_FREQ / 2)) == 0)						// each one halfsecond indicator... set
	{
		sys_Cnts.HalfSecondChangeFlag = true;
	}
	else
	{
		if((sys_Cnts.SysTickCount & ((CONF_SYSTICK_FREQ/2) + 100)) == 0)			// each half second indicator... clear
		{
			sys_Cnts.HalfSecondChangeFlag = false;
		}
	}
	
	
	

#if (CONF_SYS_ALARMS_USED == 1)
	if(sys_Alarms.AlarmList.Count)
	{
		void (*TargetFun)();
		_ColListRecord_t *tmpListRecord = NULL;
		sys_AlarmRecord_t *tmpAlarmRecord;
		uint8_t i, _argcount;
		
		do
		{
			tmpListRecord = sys_DynCol_Get_Record(&sys_Alarms.AlarmList, NULL);		// temporary pointer to parent module list and get next to tmpListRecord
		
			if (tmpListRecord == NULL) break;
			tmpAlarmRecord = tmpListRecord->pValue;									// create driver ptr			
			if(SysCnt % tmpAlarmRecord->RepeatMS) break;						// No time for alarm....
			if(SysCnt == tmpAlarmRecord->LastAction) break;						// Save action time
			
			_argcount = tmpAlarmRecord->Argv.ArgCount;								// Read_StackHeap argument count
			tmpAlarmRecord->LastAction = SysCnt;
			
			if(tmpAlarmRecord->Argv.ArgCount)
			{
				void *_arg[_argcount];												// create temporary argument array
				
				i = 0;
				do
				{
					_arg[i++] = tmpAlarmRecord->Argv.Arg;							// copy argument to array
				}while(--_argcount);
				TargetFun = (void (*)()) tmpAlarmRecord->pCode;						// assign function address
				TargetFun(_arg[0]);													// Call it with arguments.
			}
			else
			{	
				TargetFun = (void (*)()) tmpAlarmRecord->pCode;						// assign function address			
				TargetFun();														// Call it!
			}
		}while (tmpListRecord != NULL);
	}
#endif


	sys_Cnts.RTC_MSec += Local_SysTickCnt;
	
	if (sys_Cnts.RTC_MSec > 999)											// miliseconds overflows?
	{
		if (sys_Cnts.RTC_Sec >= 59)											// seconds overflows?
		{
			if (sys_Cnts.RTC_Min >= 59)										// minutes overflows?
			{
				if (sys_Cnts.RTC_Hour >= 23)								// hours overflows ?
				{
					sys_Cnts.RTC_Hour = 0;									// rotate to zero
					sys_Cnts.RTC_Min = 0;
					sys_Cnts.RTC_Sec = 0;
					sys_Cnts.RTC_MSec = 0;
				}
				else
				{
					sys_Cnts.RTC_Hour++;									// increment hours
					sys_Cnts.RTC_Min = 0;
					sys_Cnts.RTC_Sec = 0;
					sys_Cnts.RTC_MSec = 0;
				}
			}
			else
			{
				sys_Cnts.RTC_Min++;											// increment minutes
				sys_Cnts.RTC_Sec = 0;
				sys_Cnts.RTC_MSec = 0;
			}
		}
		else
		{
			//sys_System.Cnts->RTC_MSec = sys_System.Cnts->RTC_MSec / 1000;
			sys_Cnts.RTC_Sec += (sys_Cnts.RTC_MSec / 1000);
			sys_Cnts.RTC_MSec = sys_Cnts.RTC_MSec  - 1000;
		}
	}
}
COMP_POP_OPTIONS



// ------------------------------------------------------------------------------------------------------
// Refresh Clock information in Core structure
// ------------------------------------------------------------------------------------------------------
void sys_Clock_Refresh(void)
{
	// call a your clock refresh function. 
	CHAL_Chip_Update_SystemCoreClock();
	sys_System.Clock->SRC_Freq = CHAL_Chip_Read_SrcFreq();							// input frekvencia (IRC, OSC, Ext CLK...)
	sys_System.Clock->CPU_Freq = CHAL_Chip_Read_CoreFreq();							// core frequency
	sys_System.Clock->TickPerUS = CHAL_Chip_Read_TickPerUS();						// ticks per microseconds
}

// ------------------------------------------------------------------------------------------------------
// Refresh Part information in Core structure
// ------------------------------------------------------------------------------------------------------
#if CONF_SYS_PART_INFO & CONF_SYS_INFO
void sys_Part_Refresh(void)
{
	//	enable IRC before used of IAP !!!
	//	Chip_SYSCTL_PowerUp(SYSCTL_SLPWAKE_IRC_PD | SYSCTL_SLPWAKE_IRCOUT_PD);		// switch to internal RC oscillator
	
	// Core Information
	if (CHAL_Chip_IAP_Read_ID(&sys_System.Part->PARTID) == false)					// read CPUID
	 {
		sys_System.Part->PARTID = UINT32_MAX;										// fault
	 }


	if (CHAL_Chip_IAP_Read_SerialNum(&sys_System.Part->SERNUM[0]) == false)			// read serial number
	{
		memset(&sys_System.Part->SERNUM[0], 0xFF, 16);								// fault
	}

	if (CHAL_Chip_IAP_Read_BootCodeVersion(&sys_System.Part->BOOTCODE) == false)	// read serial number
	{
		sys_System.Part->BOOTCODE = 0xffffffff;										// Fault
	}
}
#endif	


#if CONF_SYS_MEM_INFO & CONF_SYS_INFO
	
	extern uint32_t Image$$ZERO$$Base;												// Execution address of the region.
	extern uint32_t Image$$region_name$$Length;										// Execution region length in bytes excluding ZI length.
	extern uint32_t Image$$region_name$$Limit;										// Address of the byte beyond the end of the non-ZI part of the execution region.
	extern uint32_t Image$$region_name$$RO$$Base;									// Execution address of the RO output section in this region.
	extern uint32_t Image$$region_name$$RO$$Length;									// Length of the RO output section in bytes.
	extern uint32_t Image$$region_name$$RO$$Limit;									// Address of the byte beyond the end of the RO output section in the execution region.
	extern uint32_t Image$$ZERO$$RW$$Base;											// Execution address of the RW output section in this region.
	extern uint32_t Image$$ZERO$$RW$$Length;										// Length of the RW output section in bytes.
	extern uint32_t Image$$ZERO$$RW$$Limit;											// Address of the byte beyond the end of the RW output section in the execution region.
	extern uint32_t Image$$NOINIT$$ZI$$Base;										// Execution address of the ZI output section in this region.
	extern uint32_t Image$$NOINIT$$ZI$$Length;										// Length of the ZI output section in bytes.
	extern uint32_t Image$$NOINIT$$ZI$$Limit;										// Address of the byte beyond the end of the ZI output section in the execution region.
	extern uint32_t Image$$ARM_LIB_HEAP$$Base;
	extern uint32_t Image$$ARM_LIB_HEAP$$ZI$$Limit;
#endif

// ------------------------------------------------------------------------------------------------------
// Refresh memory information in Core structure
// ------------------------------------------------------------------------------------------------------
void sys_Memory_Refresh(void)
{
#if CONF_SYS_MEM_INFO & CONF_SYS_INFO
	
//	#define	COMP_FLASH_LOW				Image$$ZERO$$Base
//	#define COMP_FLASH_HIGH				Image$$ZERO$$Base
//	#define COMP_FLASH_LAST				Image$$ZERO$$Base
//	#define COMP_BSS_LOW				Image$$ZERO$$Base
//	#define COMP_BSS_HIGH				Image$$ZERO$$Base
//	#define COMP_DATA_LOW				Image$$NOINIT$$ZI$$Base
//	#define COMP_DATA_HIGH				Image$$ZERO$$RW$$Base
//	#define COMP_HEAP_LOW				(unsigned long) &Image$$ARM_LIB_HEAP$$Base			// heap start address		- Low hodnota
//	#define	COMP_HEAP_HIGH				(unsigned long) &Image$$ARM_LIB_HEAP$$ZI$$Limit		// heap end address			- High hodnota
//	#define COMP_STACK_LOW				(unsigned long) &Image$$ARM_LIB_STACK$$Base  		//__stack end address		- Low hodnota
//	#define COMP_STACK_HIGH				(unsigned long) &Image$$ARM_LIB_STACK$$ZI$$Limit	// stack start address		- High hodnota	



//	sys_System.Part->Stack_Size = 0;												// Size of the Stack
//	sys_System.Part->Stack_Start = Image$$ARM_LIB_STACK$$Base;						// Address of Stack start
//	sys_System.Part->Stack_End = Image$$ARM_LIB_STACK$$ZI$$Limit;					// Address of Stacku end
//	sys_System.Part->Stack_Pos = 0;													// Last used position in Stack
//	sys_System.Part->Heap_Size = 0;													// Size of Heap
//	sys_System.Part->Heap_Start = Image$$ARM_LIB_HEAP$$Base;						// Address of Heap start
//	sys_System.Part->Heap_End = Image$$ARM_LIB_HEAP$$ZI$$Limit;						// Address of Heap end
//	sys_System.Part->Heap_Pos = 0;									// Last used position in Heap
//	sys_System.Part->Heap_Occupied_Perc = 0;							// Used Heap in perc.
//	sys_System.Part->Stack_Occupied_Perc = 0;						// Used Stack in perc.

	
//	sys_System.Memory->MEM_DATA_Low 		= COMP_DATA_LOW;						// 
//	sys_System.Memory->MEM_DATA_High 	= COMP_DATA_HIGH;							// 
//	sys_System.Memory->MEM_DATA_Size 	= COMP_DATA_HIGH - COMP_DATA_LOW;			// 
//	sys_System.Memory->MEM_BSS_Low 		= COMP_BSS_LOW;								// 
//	sys_System.Memory->MEM_BSS_High 		= COMP_BSS_HIGH;						// 
//	sys_System.Memory->MEM_BSS_Size 		= COMP_BSS_HIGH - COMP_BSS_LOW;			// 
	//sys_System.Memory->MEM_TEXT_Size 	= COMP_FLASH_LAST;							// 

//	sys_System.Memory->MEM_RAM_Free 		= sys_System.Memory->MEM_RAM_Size - (sys_System.Memory->MEM_BSS_Size + sys_System.Memory->MEM_DATA_Size);
//	sys_System.Memory->MEM_FLASH_Low 	= COMP_FLASH_LOW;
//	sys_System.Memory->MEM_FLASH_High 	= COMP_FLASH_HIGH;
//	sys_System.Memory->MEM_FLASH_Free 	= COMP_FLASH_HIGH - COMP_FLASH_LAST;
#endif
}





// ******************************************************************************************************
// ******************************************************************************************************
// 						DYNAMIC NODE COLLECTION SYSTEM
// ******************************************************************************************************
// ******************************************************************************************************
// inak nazyvany aj linked list alebo dynamicka kolekcia. 
// Je to zoznam premennych roznych typov (fyzicky sa uklada len pointer na samotnu hodnotu),
// kde kazda polozka zoznamu ukazuje cez ptr na dalsiu a predchadzajucu polozku.
#if (CONF_SYS_COLLECTIONS_USED == 1)

COMP_PUSH_OPTIONS
COMP_OPTIONS_O3
// ------------------------------------------------------------------------------------------------------
//  return record from Dynamic Collection
// ------------------------------------------------------------------------------------------------------
_ColListRecord_t *sys_DynCol_Get_Record(_ColList_t *pColList, _ColListRecord_t *pStartRecord)
{
	_ColListRecord_t *tmpColRecord = pStartRecord;

	if(pStartRecord == NULL) tmpColRecord = pColList->pFirst;
	else tmpColRecord = tmpColRecord->pNext;
	return(tmpColRecord);
}


// ------------------------------------------------------------------------------------------------------
//  Clear all items in collist
// ------------------------------------------------------------------------------------------------------
bool sys_DynCol_Clear_List(_ColList_t *pColList)
{
	bool res = false;
#if defined(CONF_DEBUG_SUBSYS_DYNCOL) && (CONF_DEBUG_SUBSYS_DYNCOL == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DynCol - Clear List %p", pColList);
#endif
	
	if(pColList->pFirst == NULL) return(true);										// collection is already empty
	
	_ColListRecord_t *tmpColRecord = pColList->pFirst;								// create local temporary Collection list
	
	do
	{
		if(tmpColRecord->pHead->pFirst != NULL) tmpColRecord = tmpColRecord->pHead->pFirst;		// select first item if still exists
		else break;
		if(sys_DynCol_Remove_Item(pColList, tmpColRecord->pValue) == false) break;	// remove item and free memory. if some problem here - return with false flag
		else res = true;
	}
	while(tmpColRecord);
	
#if defined(CONF_DEBUG_SUBSYS_DYNCOL) && (CONF_DEBUG_SUBSYS_DYNCOL == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DynCol - Clear List %p = %s", pColList, res==true? "Done":"Fail");
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
//  Find pItem in pColList. If found - result is ptr to record. otherwise NULL
// ------------------------------------------------------------------------------------------------------
_ColListRecord_t *sys_DynCol_Find_Item(_ColList_t *pColList, void *pItem)
{
	void* res = NULL;
	_ColListRecord_t *tmpListRecord = pColList->pFirst;								// create local temporary Collection list
	
#if defined(CONF_DEBUG_SUBSYS_DYNCOL) && (CONF_DEBUG_SUBSYS_DYNCOL == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DynCol - Find Item %p in List %p", pItem, pColList);
#endif
	
	if(pColList->pFirst == NULL) goto FindExit;										// collection is empty	
	if(pItem == NULL) goto FindExit;
	if(pColList == NULL) goto FindExit;
	
	tmpListRecord = pColList->pFirst;												// select first item
	while(tmpListRecord)
	{
		if(tmpListRecord->pValue == pItem) res = tmpListRecord;						// item found. Return address to stored record
		if(tmpListRecord->pNext != NULL) tmpListRecord = tmpListRecord->pNext;
		else break;
	}

FindExit:
	
#if defined(CONF_DEBUG_SUBSYS_DYNCOL) && (CONF_DEBUG_SUBSYS_DYNCOL == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DynCol - Find Item %p in List %p = Found: %p", pItem, pColList, res);
#endif
		
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// Add a new item to Dynamic Collection List.
// ------------------------------------------------------------------------------------------------------
bool sys_DynCol_Add_Item(_ColList_t *pColList, void *pItem)
{
	bool res = false;
	struct _ColListRecord *pNewRecord;

#if defined(CONF_DEBUG_SUBSYS_DYNCOL) && (CONF_DEBUG_SUBSYS_DYNCOL == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DynCol - Add Item %p to List %p", pItem, pColList);
#endif
	
	if(pItem == NULL) return(false);
	if(pColList == NULL) return(false);

	if(sys_DynCol_Find_Item( pColList, pItem) != NULL) res = true;					// check if item does not exist
	
	if(res == false)		// if item does not exist, create it. Otherwise return true - as exist
	{
		pNewRecord = (_ColListRecord_t *) sys_malloc_zero((uint32_t)sizeof(_ColListRecord_t));// create new struct in memory for new record/node
		if(pNewRecord != NULL) res = true;											// if memory created successfully
		if(res == true)
		{
			// fill newly created record:
			pNewRecord->pHead = pColList;											// Set Head
			pNewRecord->pValue = pItem;												// Save ptr to item content
			pNewRecord->pNext = NULL;												// next is nothing. We are last one.
			pNewRecord->pHead->Count ++;											// ... and ncrement count of recortds of course...
			pNewRecord->pPrev = NULL;
				
			_ColListRecord_t *tmpListRecord = pColList->pFirst;						// creste a temporary list	- dont't touch the original one!
			if(tmpListRecord == NULL) 												// we are a first record ???
			{
				tmpListRecord = pNewRecord;											// pFirst doesn't exist yet. create it
				pColList->pFirst = tmpListRecord;
			}
			else
			{																		// noo, find last one and add after it.
				while(tmpListRecord)
				{
					if(tmpListRecord->pNext != NULL) tmpListRecord = tmpListRecord->pNext;			// select last item in list	
					else break;
				}
				
				// add a last one node
				tmpListRecord->pNext = pNewRecord;									// insert a new one
				SYS_CPUCRIT_START();
				tmpListRecord->pNext->pPrev = tmpListRecord;						// set previous
				SYS_CPUCRIT_END();
			}
			res = true;
		}
	}

#if defined(CONF_DEBUG_SUBSYS_DYNCOL) && (CONF_DEBUG_SUBSYS_DYNCOL == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DynCol - Add Item %p to List %p = %s", pItem, pColList, res==true? "Done":"Fail");
#endif
	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// Remove an existing link to parent module from driver structure
// ------------------------------------------------------------------------------------------------------
bool sys_DynCol_Remove_Item(_ColList_t *pColList, void *pItem)
{
	bool res = false;
	
#if defined(CONF_DEBUG_SUBSYS_DYNCOL) && (CONF_DEBUG_SUBSYS_DYNCOL == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DynCol - Remove Item %p from List %p", pItem, pColList);
#endif
	if(pItem == NULL) return(false);
	if(pColList == NULL) return(false);
	if(pColList->pFirst == NULL) {sys_Err("Houston, we have a PROBLEM!"); return(false);}// WTF? from empty collection we need clear record ???? WHY? HOW? WHO?

	SYS_CPUCRIT_START();	
	
	_ColListRecord_t *tmpListRecord = sys_DynCol_Find_Item( pColList, pItem);			// check if item does exist
	if(tmpListRecord == NULL)	res = false;										// item is not in collection!
	else	
	{
		// remove tmpListRecord from chain
		if(tmpListRecord->pNext != NULL)											// record is notLast in chain
		{
			tmpListRecord->pNext->pPrev = tmpListRecord->pPrev;						// correct links
			tmpListRecord->pNext->pHead->pFirst = tmpListRecord->pNext;					// correct first
		}
		else tmpListRecord->pHead->pFirst = NULL;									// we was first. not is coll. empty.
		
		if(tmpListRecord->pPrev != NULL)											// record is not First in chain?
			tmpListRecord->pPrev->pNext = tmpListRecord->pNext;						// correct links
		
		tmpListRecord->pHead->Count --;												// decrease counter

		sys_free(tmpListRecord);													// clear record from memory
		res = true;
	}
	SYS_CPUCRIT_END();
	
#if defined(CONF_DEBUG_SUBSYS_DYNCOL) && (CONF_DEBUG_SUBSYS_DYNCOL == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-DynCol - Remove Item %p from List %p = %s", pItem, pColList, res==true? "Done":"Fail");
#endif


	return(res);
}
COMP_POP_OPTIONS

#endif		// if (CONF_SYS_COLLECTIONS_USED == 1)







// ******************************************************************************************************
// ******************************************************************************************************
//  					MEMORY BUFFERS - SYSTEM 
// ******************************************************************************************************
// ******************************************************************************************************

#if (CONF_SYS_BUFFERS_USED == 1)
COMP_PUSH_OPTIONS
COMP_OPTIONS_O3


// ------------------------------------------------------------------------------------------------------
// flush buffer. Reset and clear buffer
void sys_Buffer_Flush(sys_Buffer_t* pBuffer)
{
	// Check:
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)	
	SYS_ASSERT( pBuffer != NULL);													// check
	SYS_ASSERT( pBuffer->Ready != false);											// check
#endif

	if(pBuffer == NULL) return;														// wrong input
	
	pBuffer->ElementCount = 0;														// initialize stored element count
	pBuffer->pWr = pBuffer->pDataArray;												// init read to start
	pBuffer->pRd = pBuffer->pDataArray;												// init write to start
	//sys_memset(pBuffer->pDataArray, 0x00, pBuffer->ByteSize);						// physically clear buffer
	pBuffer->Empty = true;
	pBuffer->Full = false;
}



// ------------------------------------------------------------------------------------------------------
// Create buffer. Can create Linear Buffer or RingBuffer. Result is ptr to buffer struct.
// ElementSize minimal can be 1. If 0 - will be rounded to 1
// If MemArray is defined, buffer is allocated to it (linked) without clear and set ass not empty anf full.
// Elsewhere is created as new one.
sys_Buffer_t* sys_Buffer_Create(size_t MaxElementCount, uint8_t ElementByteSize, sys_BufferType_t Type, void *MemArray)
{
	sys_Buffer_t	*tmpBuff;
	uint32_t ByteSize;
	
#if defined(CONF_DEBUG_SUBSYS_BUFFER) && (CONF_DEBUG_SUBSYS_BUFFER == 1)
	bool res = false;	
	dbgprint("\r\n("SYS_OBJECTNAME")-Buffer - Create Item %d bytes", MaxElementCount*ElementByteSize);
#endif
	if(ElementByteSize == 0) ElementByteSize = 1;
	if(ElementByteSize > MaxElementCount) goto CreateExit;
	
	if(MemArray == NULL) ByteSize = (MaxElementCount * ElementByteSize) + sizeof(sys_Buffer_t);	// calculate of needed bytesize
	else ByteSize = sizeof(sys_Buffer_t);											// calculate of needed bytesize without mem array
	
	
	
	tmpBuff = sys_malloc(ByteSize);													// allocate mem for buffer and descriptor

	
	if(tmpBuff == NULL) goto CreateExit;											// allocation failed
	
	
	if(MemArray == NULL) 															// create new buffer
	{
		tmpBuff->pDataArray = (void *) (((uint8_t *) tmpBuff) + sizeof(sys_Buffer_t));  // allocate data buffer after buffer decsriptor
		tmpBuff->ElementCount = 0;													// initialize stored element count
		tmpBuff->Empty = true;
		tmpBuff->Full = false;
		tmpBuff->pEnd = tmpBuff->pStart + (MaxElementCount * ElementByteSize);		// store end of buffer
		tmpBuff->pWr = tmpBuff->pDataArray;											// init write to start
	}
	else																			// Link existing buffer 
	{		
		tmpBuff->pDataArray = MemArray;												// assign mem array if specified
		tmpBuff->ElementCount = MaxElementCount;									// initialize stored element count
		tmpBuff->Empty = false;
		tmpBuff->Full = true;
		tmpBuff->pEnd = tmpBuff->pStart + (MaxElementCount * ElementByteSize);		// store end of buffer
		tmpBuff->pWr = tmpBuff->pEnd;												// init write to end
	}

	tmpBuff->pStart = tmpBuff->pDataArray;
	tmpBuff->pRd = tmpBuff->pDataArray;												// init read to start
	tmpBuff->ElementByteSize = ElementByteSize;										// store element size
	tmpBuff->ByteSize = (MaxElementCount * ElementByteSize);						// store data size area
	
	
	//memset(tmpBuff->pDataArray, 0xFF, (MaxElementCount * ElementByteSize));			// fill databuffer with 0xff
	tmpBuff->Type = Type;
	tmpBuff->Ready = true;
#if defined(CONF_DEBUG_SUBSYS_BUFFER) && (CONF_DEBUG_SUBSYS_BUFFER == 1)
	res = true;
#endif

CreateExit:

#if defined(CONF_DEBUG_SUBSYS_BUFFER) && (CONF_DEBUG_SUBSYS_BUFFER == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-Buffer[%p] - Create Item = %s", tmpBuff, res==true? "Done":"Fail");
#endif

	return(tmpBuff);
}

//// ------------------------------------------------------------------------------------------------------
//// Change buffer. Can change existing buffer's properties
//void sys_Buffer_Change(sys_Buffer_t* pBuffer, size_t MaxElementCount, uint8_t ElementSize)
//{
//	sys_Buffer_t	*tmpBuff = NULL;
//#if defined(CONF_DEBUG_SUBSYS_BUFFER) && (CONF_DEBUG_SUBSYS_BUFFER == 1)
//	sys_Buffer_t	*oldBuff = pBuffer;
//	dbgprint("\r\n("SYS_OBJECTNAME")-Buffer[%p] - Change Item to %d bytes", pBuffer, MaxElementCount*ElementSize);
//#endif	
//	
//	tmpBuff = sys_Buffer_Create(MaxElementCount, ElementSize, pBuffer->Type);
//	if (tmpBuff)
//	{
//		sys_Buffer_Destroy(pBuffer);
//		pBuffer = tmpBuff;
//	}

//#if defined(CONF_DEBUG_SUBSYS_BUFFER) && (CONF_DEBUG_SUBSYS_BUFFER == 1)
//	dbgprint("\r\n("SYS_OBJECTNAME")-Buffer[%p] - Change Item to %p = %s", oldBuff, tmpBuff, tmpBuff==NULL? "Fail":"Done");
//	oldBuff = NULL;
//#endif	
//}


// ------------------------------------------------------------------------------------------------------
// Destroy buffer. Freeing Linear Buffer or RingBuffer. 
void sys_Buffer_Destroy(sys_Buffer_t* pBuffer)
{
#if defined(CONF_DEBUG_SUBSYS_BUFFER) && (CONF_DEBUG_SUBSYS_BUFFER == 1)
	sys_Buffer_t	*oldBuff = pBuffer;
	dbgprint("\r\n("SYS_OBJECTNAME")-Buffer[%p] - Destroy Item", &pBuffer);
#endif	
	
	if (pBuffer == NULL) return;
	if (pBuffer->Ready != true) return;												// bad descriptor
	
	//uint32_t offsize = pBuffer->ByteSize + sizeof(sys_Buffer_t);

	//sys_memset_zero(pBuffer, offsize);												// clear memory
	sys_free(pBuffer);
	
#if defined(CONF_DEBUG_SUBSYS_BUFFER) && (CONF_DEBUG_SUBSYS_BUFFER == 1)
	dbgprint("\r\n("SYS_OBJECTNAME")-Buffer[%p] - Destroy Item = %s", oldBuff, "Done");
#endif		
}



//// ------------------------------------------------------------------------------------------------------
//// Copy between two buffers
//// returns numbers of copied bytes
//uint32_t sys_Buffer_Copy_Buffers(sys_Buffer_t *pDstBuff, sys_Buffer_t *pSrcBuff, uint32_t ByteLen)
//{
//	uint32_t FreeLinearWr_Bytes;
//	uint32_t FreeLinearRd_Bytes;
//	uint32_t Copied;
//	uint32_t Result = 0;
//	
//	FreeLinearWr_Bytes = sys_Buffer_Get_Wr_LinearByteCount(pDstBuff);				// get linear size for one shot write
//	FreeLinearRd_Bytes = sys_Buffer_Get_Rd_LinearByteCount(pSrcBuff);				// get linear size of one shot read
//	if((FreeLinearWr_Bytes == 0) || (FreeLinearRd_Bytes == 0)) return(0);			// cannot copy. no free space in destination or no source data -> exit
//	
//	SYS_CPUCRIT_START();
//	
//	if(FreeLinearRd_Bytes <= FreeLinearWr_Bytes )									// dest. free space is bigger than source data count
//	{
//		if(FreeLinearRd_Bytes >= ByteCount) Copied = ByteLen;						// required data count is less than available
//		else Copied = FreeLinearRd_Bytes);											// available data count is less than required
//	}
//	else																			// readed data count is bigger than destination free space
//	{
//		if(FreeLinearWr_Bytes >= ByteCount) Copied = ByteLen;						
//		else Copied = FreeLinearWr_Bytes);
//	}

//	sys_memcpy(pDstBuff->pWr, pDstBuff->pRd, Copied);								// copy src buffer to dest buffer
//	Result += Copied;																// total counter increment

//	
//	
//	- dorob kopirovanie buffrov. Skopiruj len to co sa da a pocet vrat. vzor moze byt z system.h - sys_Buffer_Write_Bytes (je to podobne)
//	nasledne toto kopirovanie pouziv v UART Send, aby vedel odosielat aj pri odosielani - prikopiruje nove data a zvysi tym counter na vysielanie (ten musi byt zvlast, nie elemen count z buffera!)
//	Pri UART Send skontroluj DUPLEX a ak je len half a je aktivny prijem - zablokuj vysielanie - musi sa vyslat neskor - neviem ako este. Mozno v preruseni RXIDLE skontrolovat TxBuff atak...
//	
//	
//	
//	
//	if(pDstBuff->ElementCount > (pBuff->ByteSize/pBuff->ElementSize)) return(0);	// cannot copy, buffer small.
//	
//	if(pBuff->ElementCount > (pBuff->ByteSize/pBuff->ElementSize)) return(0);		// cannot copy, buffer small.
//	
//	SYS_CPUCRIT_START();
//	FreeLinearWr_Bytes = sys_Buffer_Get_Wr_LinearByteCount(pBuff);					// get linear area for write
//	if(ByteLen <= FreeLinearWr_Bytes)												// Can I write all data?
//	{
//		Copied = ByteLen;
//	}
//	else																			// Write only 1-st part of data
//	{
//		Copied = FreeLinearWr_Bytes;												// return number of free data
//	}

//	sys_memcpy(pBuff->pWr, pWorkPos, Copied);										// copy data to buffer from pSrc
//	pWorkPos += Copied;																// increase work position
//	
//	// increment write pointer:
//	if((pBuff->pWr + Copied) ==  pBuff->pEnd) pBuff->pWr = pBuff->pStart;			// Rotate!
//	else pBuff->pWr += Copied;														// Update wr pointer, don't rotate
//		
//	if(Copied < ByteLen)															// write All ?
//	{																				// Ok, write 2-nd part of data
//		Copied = ByteLen - Copied;													// calc. remaining data count
//		// write again:
//		sys_memcpy(pBuff->pWr, pWorkPos, Copied);									// copy remaining data to buffer from pSrc
//		pBuff->pWr += Copied;														// Update wr pointer
//	}
//	
//	pBuff->ElementCount += (ByteLen / pBuff->ElementSize);							// increase element counter
//	SYS_CPUCRIT_END();
//	
//	return(ByteLen);
//}

// ------------------------------------------------------------------------------------------------------
// return read Byte count in linear area
uint32_t sys_Buffer_Get_Rd_LinearByteCount(sys_Buffer_t *pBuff)
{
	uint8_t *pRd = pBuff->pRd;
	uint8_t *pWr = pBuff->pWr;

	switch (pBuff->Type)
	{
		case sBuffRing:
			if((pWr == pRd) & (pBuff->Empty)) 
			{
				return(0);
			}
			if((pWr == pRd) & (pBuff->Full)) 
			{
				return(pBuff->pEnd - pRd);
			}
			if(pWr < pRd) return (pBuff->pEnd - pRd);
			else return(pWr - pRd);
		case sBuffLinear:
			return(pBuff->pEnd - pRd);
		default:
			return(0);																// error, don't read!
	}
}


// ------------------------------------------------------------------------------------------------------
// return write free Byte count in linear area
uint32_t sys_Buffer_Get_Wr_LinearByteCount(sys_Buffer_t *pBuff)
{
	uint8_t *pRd = pBuff->pRd;
	uint8_t *pWr = pBuff->pWr;
	
	switch (pBuff->Type)
	{
		case sBuffRing:
			if((pRd == pWr) & (pBuff->Full)) 
			{
				return(0);
			}
			if((pRd == pWr) & (pBuff->Empty)) 
			{
				return(pBuff->pEnd - pWr);
			}
			if(pRd < pWr) return(pBuff->pEnd - pWr);
			else return (pRd - pWr);
		case sBuffLinear:
			return(pBuff->pEnd - pWr);
		default:
			return(0);																// error, don't write!
	}
}



// ------------------------------------------------------------------------------------------------------
// write Bytes to buffer with update buffer information
// return number of written bytes. If cannot write all - don't write something !!!
uint32_t sys_Buffer_Write_Bytes(sys_Buffer_t *pBuff, uint8_t *pSrc, uint32_t ByteLen)
{
	uint32_t FreeLinearWr_Bytes;
	uint32_t Copied; 
	uint8_t *pWorkPos = pSrc;

	// Check:
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)	
	SYS_ASSERT( pBuff != NULL);														// check
	SYS_ASSERT( pBuff->Ready != false);												// check
#endif
	
	if(pBuff == NULL) return(0);
	
	if(ByteLen > (pBuff->ByteSize/pBuff->ElementByteSize)) return(0);		// cannot copy, buffer small.
	
	SYS_CPUCRIT_START();
	FreeLinearWr_Bytes = sys_Buffer_Get_Wr_LinearByteCount(pBuff);					// get linear area for write
	if(ByteLen <= FreeLinearWr_Bytes)												// Can I write all data?
	{
		Copied = ByteLen;
	}
	else																			// Write only 1-st part of data
	{
		Copied = FreeLinearWr_Bytes;												// return number of free data
	}

	if(Copied)
	{
		sys_memcpy(pBuff->pWr, pWorkPos, Copied);									// copy data to buffer from pSrc
		pBuff->Empty = false;														// data saved
	}
	else
	{
		sys_memcpy(pBuff->pWr, pWorkPos, 1);										// copy byte to buffer from pSrc
	}

	
	pWorkPos += Copied;																// increase work position
	pBuff->ElementCount += (Copied / pBuff->ElementByteSize);						// increase element counter
	if(pBuff->ElementCount > (pBuff->ByteSize/pBuff->ElementByteSize)) sys_Err("Elements count is bigger than buffer size");
	
	// increment write pointer:
	if((pBuff->pWr + Copied) ==  pBuff->pEnd) pBuff->pWr = pBuff->pStart;			// Rotate!
	else pBuff->pWr += Copied;														// Update wr pointer, don't rotate
	
	if(pBuff->pWr == pBuff->pRd) pBuff->Full = true;
	
	if(Copied < ByteLen)															// write All ?
	{																				// Ok, write 2-nd part of data
		Copied = ByteLen - Copied;													// calc. remaining data count
		// write again:
		FreeLinearWr_Bytes = sys_Buffer_Get_Wr_LinearByteCount(pBuff);					// get linear area for write
		if(Copied < FreeLinearWr_Bytes)												// Can I write data?
		{
			//Copied = FreeLinearWr_Bytes;											// return number of free data
			sys_memcpy(pBuff->pWr, pWorkPos, Copied);								// copy remaining data to buffer from pSrc
			pBuff->pWr += Copied;													// Update wr pointer
			if(pBuff->pWr == pBuff->pRd) pBuff->Full = true;
			pBuff->ElementCount += (Copied / pBuff->ElementByteSize);				// increase element counter
			if(pBuff->ElementCount > (pBuff->ByteSize/pBuff->ElementByteSize)) sys_Err("Elements count is bigger than buffer size");
		
		}
			
	}
	
	//pBuff->ElementCount += (ByteLen / pBuff->ElementSize);							// increase element counter
	SYS_CPUCRIT_END();
	
	return(ByteLen);
}



// ------------------------------------------------------------------------------------------------------
// write Elements to buffer with update buffer information
// return number of written elements
uint32_t sys_Buffer_Write_Element(sys_Buffer_t *pBuff, uint8_t *pSrc, uint32_t ElementCount)
{
	// Check:
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)	
	SYS_ASSERT( pBuff != NULL);														// check
	SYS_ASSERT( pBuff->Ready != false);												// check
#endif

	uint32_t ByteLen = ElementCount * pBuff->ElementByteSize;
	uint32_t Result;
	
	if(pBuff == NULL) return(0);
	Result = sys_Buffer_Write_Bytes (pBuff, pSrc, ByteLen);
	
	return(Result / pBuff->ElementByteSize);
}


// ------------------------------------------------------------------------------------------------------
// Read n Bytes from buffer with update buffer information
// return number of readed bytes
uint32_t sys_Buffer_Read_Bytes( sys_Buffer_t *pBuff, uint8_t *pDst, uint32_t ByteLen)
{
	uint32_t FreeLinearRd_Bytes;
	uint32_t Result;
	uint8_t *pWorkPos = pDst;

	// Check:
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)	
	SYS_ASSERT( pBuff != NULL);														// check
	SYS_ASSERT( pBuff->Ready != false);												// check
#endif
	
	
	if(pBuff == NULL) return(0);


	
	SYS_CPUCRIT_START();
	
	if(pBuff->Type == sBuffLinear)
	{
		
		Result = sys_Buffer_Get_Rd_LinearByteCount(pBuff);							// Count all available data
		if(ByteLen <= Result)														// All required data available?
		{
			sys_memcpy(pDst, pBuff->pRd, ByteLen);									// copy data to pDst from buffer
			Result = ByteLen;
		}
		else																		// No, not enought data
		{
			sys_memcpy(pDst, pBuff->pRd, Result);									// copy all available data to pDst from buffer
		}
		pBuff->pRd += Result;														// move read pointer
		pBuff->ElementCount -= (Result / pBuff->ElementByteSize);					// decrease element counter
		if(pBuff->pRd == pBuff->pWr) {pBuff->Empty = true; pBuff->Full = false;}	// set apropriate flags
		else if(pBuff->pRd == pBuff->pStart) {pBuff->Empty = false; pBuff->Full = true;}
		 else {pBuff->Empty = false; pBuff->Full = false;}
		
		// and goto exit ....
	}
	
	
	if(pBuff->Type == sBuffRing)
	{
		FreeLinearRd_Bytes = sys_Buffer_Get_Rd_LinearByteCount(pBuff);					// get linear area for read
		if(ByteLen <= FreeLinearRd_Bytes)												// Can I Read all data?
		{
			Result = ByteLen;
		}
		else																			// Write only part of data
		{
			Result = FreeLinearRd_Bytes;												// return number of read data
		}
		
		if(Result)
		{
			sys_memcpy(pWorkPos, pBuff->pRd, Result);									// copy data to pDst from buffer
			pBuff->Full = false;														//	some data was read!
		}
		else 		// read byte and DON'T increment position!
		{
			sys_memcpy(pWorkPos, pBuff->pRd, 1);										// copy data to pDst from buffer	
		}
		
		
		pWorkPos += Result;																// increment working postition
		pBuff->pRd += Result;															// Update rd pointer
		if(pBuff->pRd == pBuff->pWr) {pBuff->Empty = true; pBuff->Full = false;}
		pBuff->ElementCount -= (Result / pBuff->ElementByteSize);						// decrease element counter
		if(pBuff->ElementCount > (pBuff->ByteSize/pBuff->ElementByteSize)) sys_Err("Elements count is bigger than buffer size");
		if(Result < ByteLen)															// Read All ?
		{
			ByteLen -= Result;															// minus moved data yet....
			if(pBuff->pRd >= pBuff->pEnd)												// Rotate?
			{
				pBuff->pRd = pBuff->pStart;												// Rotate
				FreeLinearRd_Bytes = sys_Buffer_Get_Rd_LinearByteCount(pBuff);			// get linear area for Read again
				// Read again:
				if(ByteLen <= FreeLinearRd_Bytes)										// Can I Read all data?
				{
					Result = ByteLen;
				}
				else																	// Write only part of data
				{
					Result = FreeLinearRd_Bytes;										// return number of Read data
				}

				if(Result)
				{
					sys_memcpy(pWorkPos, pBuff->pRd, Result);								// copy data to pDst from buffer
					pBuff->Full = false;
				}
				pBuff->pRd += Result;													// Update rd pointer
				if(pBuff->pRd == pBuff->pWr) {pBuff->Empty = true; pBuff->Full = false;}
				pBuff->ElementCount -= (Result / pBuff->ElementByteSize);				// decrease element counter
				if(pBuff->ElementCount > (pBuff->ByteSize/pBuff->ElementByteSize)) sys_Err("Elements count is bigger than buffer size");
			}
		}
			// and goto exit ....
	}	// endif buff type
	

	SYS_CPUCRIT_END();
	
	return(Result);
}

// ------------------------------------------------------------------------------------------------------
// Read Elements to Dst with update buffer information
// return number of readed elements
uint32_t sys_Buffer_Read_Element(sys_Buffer_t *pBuff, uint8_t *pDst, uint32_t ElementCount)
{
	// Check:
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)	
	SYS_ASSERT( pBuff != NULL);														// check
	SYS_ASSERT( pBuff->Ready != false);												// check
#endif
	
	uint32_t ByteLen = ElementCount * pBuff->ElementByteSize;						// recalcul to bytes
	uint32_t Result;

	
	if(pBuff == NULL) return(0);
	
	Result = sys_Buffer_Read_Bytes(pBuff, pDst, ByteLen);							// call byte manipulation
	
	return(Result / pBuff->ElementByteSize);											// return
}


COMP_POP_OPTIONS





// ------------------------------------------------------------------------------------------------------
// Get Byte from buffer WITHOUT update buffer information. (ptr to content only).
// return pointer to byte in buffer. Start from ReadStart position + Offset.
uint8_t *sys_Buffer_Get_RdBytePtr( sys_Buffer_t *pBuff, uint32_t RdOffset)
{
	uint8_t	*ReadPos;
	
	// Check:
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)	
	SYS_ASSERT( pBuff != NULL);														// check
	SYS_ASSERT( pBuff->Ready != false);												// check
#endif
	
	
	if(pBuff == NULL) return(0);
	
	SYS_CPUCRIT_START();
	
	//FreeLinearRd_Bytes = sys_Buffer_Get_Rd_LinearByteCount(pBuff) - RdOffset;		// get linear area for read (minus Offset)
	
	if((pBuff->pRd + RdOffset) > pBuff->pEnd)									// ReadPos from beginnig?
	{
		ReadPos = pBuff->pStart + (RdOffset - (pBuff->pEnd - pBuff->pRd));	// offset from beginning....
	}
	else	
	{
		ReadPos = pBuff->pRd + RdOffset;
	}
	
		//pBuff->ElementCount ostane cislo aj ked sys_Buffer_Get_Rd_LinearByteCount vracia O
	SYS_CPUCRIT_END();
	
	return(ReadPos);
}
#endif		// CONF_SYS_BUFFERS_USED



// ******************************************************************************************************
// ******************************************************************************************************
//  					WATCHDOG - SYSTEM 
// ******************************************************************************************************
// ******************************************************************************************************
#if defined(CONF_SYS_WATCHDOG_USED) && (CONF_SYS_WATCHDOG_USED == 1)
void sys_WatchDog_Init(void)
{
	SYS_CPUCRIT_START();
	sys_WatchDog.pPeri = CHAL_WDT_Init(0);									// try to initialize WDT 0
	if (sys_WatchDog.pPeri)
	{
		sys_WatchDog.Initialized = CHAL_WDT_Configure( sys_WatchDog.pPeri, 0, 0xffffff, 0x0000, true);
	}
	sys_WatchDog.Enable = false;
	SYS_CPUCRIT_END();
}
#endif



// ------------------------------------------------------------------------------------------------------
// System Runtime Error processing handler
// ------------------------------------------------------------------------------------------------------
#if defined(CONF_SYS_ERROR_PROCS) && (CONF_SYS_ERROR_PROCS == 1)

// ------------------------------------------------------------------------------------------------------
// Assert function prototype
void (*sys_Assert_Fn)(const char *pCond_String, const char *pFile, const char *pFn, uint32_t line); 

// ------------------------------------------------------------------------------------------------------
// set assert function - will be called if assert occur
void sys_Assert_Set_Fn(void (*dstFn)(const char *pCond_String, const char *pFile, const char *pFn, uint32_t line))
{
    sys_Assert_Fn = dstFn;
}

// ------------------------------------------------------------------------------------------------------
// The sys_Assert handler.
// Cond_String the assertion condition string
// func the function name when assertion.
// line the file line number when assertion.
void sys_Assert_Handler(const char *pCond_String, const char *pFile, const char *pFn, uint32_t line)
{
	volatile char dummy = 0;

	if (sys_Assert_Fn == NULL)
	{
		dbgprint("\r\n %s, Fn:%s, line %d - assertion failed %s", pFile, pFn, line, pCond_String);
		while (dummy == 0);															// STOP !!!!!!!!!
    }
	else
	{
		sys_Assert_Fn(pCond_String, pFile, pFn, line);
	}
} 
#endif
