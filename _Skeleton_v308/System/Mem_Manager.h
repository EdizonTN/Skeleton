// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: Mem_Manger.c
// 	   Version: 1.0
//  Created on: 01.05.2014
//      Author: EdizonTN 
//		  Desc: Memory Manager (based on https://github.com/vitiral/tinymem  VERSION 0.0.2dev)
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 
#ifndef __MEM_MANAGER_H_
#define __MEM_MANAGER_H_

#include "Skeleton.h"

#if defined(CONF_USE_MEM_MANAGER) && (CONF_USE_MEM_MANAGER == 1)



// ************************************************************************************************
// Default CONFIGURATION
// ************************************************************************************************
// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************
#ifndef CONF_DEBUG_MEM_MANAGER
#define CONF_DEBUG_MEM_MANAGER			0											// default off
#endif

#ifndef TM_POOL_SIZE
#define TM_POOL_SIZE            		(CONF_MM_DYNMEM_SIZE - (CONF_MM_DYNMEM_SIZE % 4))	// size of memory pool. This is the maximum amount of memory that can be allocated in a memory pool
#endif
//TODO: not yet used	#define TM_NUM_POOLS            (1)							// number of memory pools to create

#ifndef TM_MAX_POOL_PTRS
#define TM_MAX_POOL_PTRS        		(32)										// Maximum number of pointers that can be allocated
// This is the maximum number of pointers that a system can allocate. For instance, if TM_MAX_POOL_PTRS == 3 and you allocated an integer, a 1000 character array and
// a 30 byte struct, then you would be unable to allocate any more data. On memory constrained systems this number should be low, possibly 20-25 or less
// This value is highly dependent on implementation details, however, it is advisable to leave a large headroom Each pool_ptr uses 50 bits
#endif

#ifndef FREED_BINS
#define FREED_BINS          			(12)                                		// bins to store freed values
#endif

// ************************************************************************************************
// Prelinks
// ************************************************************************************************
#define memcpy			mem_memcpy
#define memset			mem_memset

// ************************************************************************************************
// Declarations
// ************************************************************************************************
//#ifndef NDEBUG
//#define tm_debug(...)      do{printf("\r\n\t\t[DEBUG](%s,%u):", __FILE__, __LINE__); printf(__VA_ARGS__); printf("\n");}while(0)
//#endif


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
//#undef NDEBUG

#include <assert.h>
#include <signal.h>

// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************
typedef uint16_t        tm_index_t;
typedef uint32_t        tm_size_t;
typedef uint8_t         tm_status_type;

// ************************************************************************************************
// PUBLIC 
// ************************************************************************************************
extern inline tm_size_t tm_sizeof(const tm_index_t index);									
extern tm_index_t tm_alloc(tm_size_t size);
//extern tm_index_t tm_realloc(tm_index_t index, tm_size_t size);
extern void tm_free(const tm_index_t index);
extern inline void*        tm_void_p(const tm_index_t index);
extern void MemManager_Init(void);

extern void *mem_memcpy(void * Dst, const void * Src, size_t Len) __attribute__((__nonnull__(1,2)));
extern void *mem_memset(void * Dst, int Sample, size_t Len) __attribute__((__nonnull__(1)));
extern char *test_tinymem(void);	



#endif		// defined(CONF_USE_MEM_MANAGER) && (CONF_USE_MEM_MANAGER == 1)
#endif		// __MEM_MANAGER_H_
