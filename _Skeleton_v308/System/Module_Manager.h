// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: Module_Manager.h
// 	   Version: 3.03
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: 
//
// Notice:
//
// Usage:
//			
// ToDo:
//				- kill mod_pVoid_Api - it's useless
// Changelog:
//				2022.12.09	- v3.03 - add optimalization (if-else) switches
//				2022.10.31	- Callbacky modulov zrusene
//				2021.07.12	- retry operation for init and enable module changed - add CONF_MOD_MNGR_INIT_RETRY and CONF_MOD_MNGR_ENAB_RETRY const


#ifndef __MODULE_MANAGER_H_
#define __MODULE_MANAGER_H_

#include "Skeleton.h"

#if defined(CONF_USE_MODDRV_MANAGER) && (CONF_USE_MODDRV_MANAGER == 1)

// ******************************************************************************************************
// CONFIGURATION	
// ******************************************************************************************************
#ifndef CONF_DEBUG_MOD_MANAGER
#define CONF_DEBUG_MOD_MANAGER			0											// defaultne vypni debug
#endif

#ifndef CONF_MOD_MNGR_INIT_RETRY
#define CONF_MOD_MNGR_INIT_RETRY		3											// Retry count for module initialization if failed
#endif

#ifndef CONF_MOD_MNGR_ENAB_RETRY
#define CONF_MOD_MNGR_ENAB_RETRY		3											// Retry count for module enabling if failed
#endif

#ifndef CONF_USE_MOD_MANAGER_EXT
#define CONF_USE_MOD_MANAGER_EXT		0											// extended functions and module listing
#endif


// ******************************************************************************************************
// Checks
// ******************************************************************************************************
#if (CONF_SYS_COLLECTIONS_USED == 0) && (CONF_USE_MOD_MANAGER_EXT == 1) 
 #error "Dynamic collections needs for MOD_MANAGER_EXT system. Use CONF_SYS_COLLECTIONS_USED=1"
#endif


// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************
struct _mod_If;																		// empty declaration

// struktura uzivatelskych API funkcii pre MODUL. Ak nie su zadane (NULL) nepouziju sa.
typedef struct mod_Api																// struktura API funkcii 
{
	bool	(*Init)(struct _mod_If *pModIf); 										// Init - inicializacia modulu a jeho driverov, Priznak Quiet - init bez uzivatelom viditelnej zmeny
	bool	(*Restart) (struct _mod_If *pModIf, bool Quiet);						// Reinit HW - pokus o restart modulu/periferii - mozna strata dat
	bool	(*Release) (struct _mod_If *pModIf); 									// Release - uvolnenie periferie
	bool	(*Enable)(struct _mod_If *pModIf);										// Enable zariadenia
	bool	(*Disable)(struct _mod_If *pModIf);										// Disable zariadenia
	bool    (*Write_Data)(struct _mod_If *pModIf, _drv_If_t *pDstDrv, sys_Buffer_t *pbuff, uint32_t buflen, _XFerType_t XferType); 	// zapis data - zmena vlastnosti pripojeneho HW zariadenia - jas LED, otacky motora,...
	uint32_t(*Read_Data)(struct _mod_If *pModIf, _drv_If_t *pSrcDrv, sys_Buffer_t *pbuff, uint32_t maxlen, _XFerType_t XferType); 	// citaj data

//	void    (*CallBack_ChangedState)(void); 										// callback - State changed
//	void    (*CallBack_DataReceived)(void); 										// Callback - Data was received
//	void    (*CallBack_DataTransmitted)(void); 										// Callback - Data was transferred
//	void	(*CallBack_ErrorState)(struct _mod_If *pModIf);							// callback na f-ciu, vola sa po chybe stavu modulu (nepripraveny, bez drivera, a pod)
	
} _mod_Api_t;


typedef struct _mod_sys_If
{
#if (CONF_SYS_COLLECTIONS_USED == 1)	
	_ColList_t							DrvLinked;									// linked drivers for current module
#endif	
	struct _If_Status					Stat;										// struktura so stavom
	uint8_t								DataBuffer;									// lokalna premenna - buffer o dlzke 1 byte - pouzity pre cinanie-zapis z/do modulu
}_mod_sys_If_t;


// struktura MODULU 
typedef struct _mod_If
{
	_mod_sys_If_t						Sys;										// system's struct
	_mod_Api_t							*pAPI;										// pointer to module API
	const _drv_If_t						**pRequiredDrvs;							// pole pozadovanych driverov	
#if defined(CONF_DEBUG_STRING_NAME_SIZE) && (CONF_DEBUG_STRING_NAME_SIZE > 0)		// if text naming are enabled	
	const char							Name[CONF_DEBUG_STRING_NAME_SIZE];			// interface name - text - only for ID
#endif	
} _mod_If_t;


// ******************************************************************************************************
// PUBLIC 
// ******************************************************************************************************

extern bool modmgr_Load (_mod_If_t *pModIf, _drv_If_t **pRequiredDrvs);				// Load required drivers
extern bool modmgr_Init (_mod_If_t *pIf);											// init 
extern bool modmgr_Release (_mod_If_t *pIf);										// Release - uvolnenie modulu a pripojenych driverov
extern bool modmgr_Enable (_mod_If_t *pIf);											// Enable modulu
extern bool modmgr_Disable (_mod_If_t *pIf);										// Disable modulu
extern bool modmgr_Write_Data (_mod_If_t *pIf, _drv_If_t *pDstDrv, sys_Buffer_t *pbuff, uint32_t buflen, _XFerType_t XferType); // zapis data
extern uint32_t modmgr_Read_Data (_mod_If_t *pIf, _drv_If_t *pSrcDrv, sys_Buffer_t *pbuff, uint32_t maxlen, _XFerType_t XferType); // Citaj data s moznostou vyberu drivera
extern void modmgr_CallBack_ChangedState (void); 									// CallBack - zmena stavu
extern _ColList_t 	modmgr_List;													// global loaded module list
extern _mod_Api_t	mod_pVoid_Api;													// Create a Void Api f-cions. Use in your own modules as empty.

#endif		// defined(CONF_USE_MODDRV_MANAGER) && (CONF_USE_MODDRV_MANAGER == 1)
#endif 		// __MODULE_MANAGER_H_
