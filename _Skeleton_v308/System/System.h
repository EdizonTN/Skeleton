// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: System.c
// 	   Version: 3.21
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: System's functions and definitions
//
// Notice:
//
// Usage:
//			
// ToDo:
//
// Changelog:	
//				2024.03.09	- v3.21 - add HalfSecondTimer Flag
//									- add sys_NULL_Function
//				2023.12.06	- v3.20	- upgrade to minamilistic version (without drvmgr and modmgr)
//				2023.11.05	- v3.13	- Runtime error processing disabled by option CONF_SYS_ERROR_PROCS=0
//									- systick rewrited
//									- remove CONF_DEBUG_IRQ_MANAGER
//									- rename handler to sys_xxx_handler
//				2022.12.09	- v3.12 - add optimalization (if-else) switches
//				2022.12.08	- v3.11 - add sys_Buffer_Copy_Bytes
//				2022.11.28	- v3.10 - optimize to sie. Chnage CONF_PART_INFO to CONF_SYS_PART_INFO, CONF_MEM_INFO to CONF_SYS_MEM_INFO
//									- created structure sys_Clock_t
//									- Clocks related members are moved to sys_Clock_t
//				2020.11.26	- v3.08 - change interrupt enable/disable system
//				2020.10.20	- v3.07 - add an event state variable
//				2020.09.16	- v3.06 - add full and empty flag. Corrected mistake with overflow. Add sys_Buffer_Flush
//				2020.06.08	- v3.04 - Add assertion
// 				2019.11.11 	- create buffer definition: sys_Buffer_t
//				2020.05.24	- Watch Dog added. Sys_System_Run added

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

#include "Skeleton.h"

// ******************************************************************************************************
// Version
// ******************************************************************************************************
#define SYSTEM_NAME_STRING				"Skeleton"									// System name string
#define SYSTEM_VERSION_MAJOR			03											// major version - integer
#define SYSTEM_VERSION_MINOR			08											// minor version - integer


#define SYSTEM_VERSION_STRING			STR(SYSTEM_VERSION_MAJOR) "." STR(SYSTEM_VERSION_MINOR)	// concatenate integer values into string


// ******************************************************************************************************
// Default CONFIGURATION
// ******************************************************************************************************
#ifndef BSP_EXQUARTZFREQ
#define	EXQUARTZFREQ					12000000									// pouzi defaultnu freq.
#else
#define EXQUARTZFREQ					BSP_EXQUARTZFREQ							// Ak je v BSP definovana, pouzi ju
#endif

#ifndef BSP_RTCQUARTZFREQ
#define RTCQUARTZFREQ					32768
#else
#define	RTCQUARTZFREQ					BSP_RTCQUARTZFREQ							// Ak je v BSP definovana, pouzi ju
#endif

#ifndef MEMMARK_SAMPLE
#define MEMMARK_SAMPLE					0xaa55aa55									// memory mark sample
#endif

#ifndef CONF_DEBUG_SUBSYS
#define CONF_DEBUG_SUBSYS				0											// switch on debug print fot sys_err and unused activated interrupt
#endif

#ifndef CONF_DEBUG_SUBSYS_DYNCOL
#define	CONF_DEBUG_SUBSYS_DYNCOL		0											// debug subsys: Dynamic Collection 
#endif

#ifndef CONF_DEBUG_SUBSYS_TASK
#define CONF_DEBUG_SUBSYS_TASK			0											// debug subsys: Tasks
#endif

#ifndef CONF_DEBUG_SUBSYS_DYNMEM
#define CONF_DEBUG_SUBSYS_DYNMEM		0											// debug subsys: Dynamic Memory
#endif

#ifndef CONF_DEBUG_SUBSYS_BUFFER
#define	CONF_DEBUG_SUBSYS_BUFFER		0											// debug subsys: Buffers
#endif

#ifndef CONF_CATCH_RESET_VECTOR
#define	CONF_CATCH_RESET_VECTOR			1											// osetrenie RESET vektora - prevezmes kontrolu voci keil_startup_lpcXXXX.s
#endif

#ifndef CONF_SYS_STACKCHECK
#define CONF_SYS_STACKCHECK				0											// Kontrolujeme stack? /taky maly memory manager/
#endif

#ifndef CONF_SYS_PART_INFO
#define	CONF_SYS_PART_INFO				0											// pozbierat informacie o MCU?
#endif

#ifndef CONF_SYS_MEM_INFO	
#define	CONF_SYS_MEM_INFO				0											// pozbierat informacie o pamati?
#endif

#ifndef CONF_SYS_APP_INFO
#define CONF_SYS_APP_INFO				0
#endif


#ifndef CONF_USE_IRQ_MANAGER
#define	CONF_USE_IRQ_MANAGER			0											// use interrupt manager? - not functional yet!
#endif

#ifndef CONF_DEBUG_STRING_NAME_SIZE
#define	CONF_DEBUG_STRING_NAME_SIZE		10											// dlzka stringov v nazvoch struktur
#endif

#ifndef CONF_SYSTICK_FREQ
#define CONF_SYSTICK_FREQ				1000
#endif


#ifndef CONF_SYS_WATCHDOG_USED
#define CONF_SYS_WATCHDOG_USED			0											// WatchDog system are used ????????
#endif

#ifndef CONF_SYS_TASKS_USED
#define CONF_SYS_TASKS_USED				0											// Task system are used ????????
#endif

#ifndef CONF_SYS_ALARMS_USED
#define CONF_SYS_ALARMS_USED			0											// Alarms system are used ????????
#endif

#ifndef CONF_SYS_COLLECTIONS_USED
#define CONF_SYS_COLLECTIONS_USED		0											// Dynamic collections are used ????????
#endif

#ifndef	CONF_SYS_BUFFERS_USED
#define	CONF_SYS_BUFFERS_USED			0											// Do not use system Buffers
#endif

#ifndef CONF_SYS_ERROR_PROCS
#define CONF_SYS_ERROR_PROCS			0											// System errors processing
#endif



// ******************************************************************************************************
// Checks
// ******************************************************************************************************
#if (CONF_SYS_COLLECTIONS_USED == 0) && (CONF_SYS_ALARMS_USED == 1) 
 #error "Dynamic collections needs for Alarm system. Use CONF_SYS_COLLECTIONS_USED=1"
#endif

#if (CONF_SYS_COLLECTIONS_USED == 0) && (CONF_SYS_TASKS_USED == 1) 
 #error "Dynamic collections needs for Task system. Use CONF_SYS_COLLECTIONS_USED=1"
#endif



// ******************************************************************************************************
// Declarations and Helpers
// ******************************************************************************************************

#if !defined(ARRAY_SIZE)
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))
#endif


//#define i80_ctrl_command(i80, cmd, seq...) \
//({ \
//	u8 d[] = { seq }; \
//	i80_ctrl_command_buf(i80, cmd, d, ARRAY_SIZE(d)); \
//})

#define GET_BYTES(bits)		((bits-1) / 8)



// ******************************************************************************************************
// Export
// ******************************************************************************************************

extern void 							Delay_us(uint16_t us);						// cakacia slucka - 1 - 65535 mikrosekund
extern void 							Delay_ms(uint16_t us);						// cakacia slucka - 1 - 65535 milisekund
extern uint32_t 						cpuSR[8];
extern uint8_t							cpuSR_Lock;
extern volatile uint8_t 				IRQ_Status;

static inline void SYS_CPUCRIT_START(void)
{
#if defined(CPUCRIT_TEST) && (CPUCRIT_TEST == 1)
	CHAL_Signal_Set_Pin(sig_STATUS2.Std);
#endif
	
	cpuSR[cpuSR_Lock ++] = IRQ_Status;	//__get_PRIMASK(); 
	IRQ_Status = 0;
	__disable_irq();
}

static inline void SYS_CPUCRIT_END(void) 				
{

	//if(cpuSR[cpuSR_Lock] == 0)
	if(cpuSR[--cpuSR_Lock] == 1)
	{
		IRQ_Status = 1;
		__enable_irq();
#if defined(CPUCRIT_TEST) && (CPUCRIT_TEST == 1)	
		CHAL_Signal_Clear_Pin(sig_STATUS2.Std);
#endif
	}
		
	//__set_PRIMASK(cpuSR[-- cpuSR_Lock]);
}


extern void SystemInit(void);
extern void sys_System_Info_Print(void);
extern void sys_System_Load(void);
extern void sys_System_Start(void);
extern void sys_System_Execute(void);
	
extern void sys_Memory_Refresh(void);

#if CONF_USE_DEBUG == 0
extern void sys_HardFault_Handler(void);
#endif
extern void sys_NMI_Handler(void);
extern void sys_MemManage_Handler(void);
extern void sys_BusFault_Handler(void);
extern void sys_UsageFault_Handler(void);
extern void sys_SVC_Handler(void);
extern void sys_DebugMon_Handler(void);
extern void sys_PendSV_Handler(void);
extern void sys_SysTick_Handler(void);
extern void Default_Handler(void);

extern void sys_NULL_Function(void);


// ******************************************************************************************************
// 										DYNAMIC NODE COLLECTION SYSTEM 
// ******************************************************************************************************
#if (CONF_SYS_COLLECTIONS_USED == 1)
COMP_PACKED_BEGIN
typedef struct _ColListRecord
{
	void								*pValue;									// pointer na ulozenu hodnotu
	struct _ColList						*pHead;
	struct _ColListRecord				*pNext;
	struct _ColListRecord				*pPrev;
} _ColListRecord_t;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct _ColList
{
	struct _ColListRecord				*pFirst;									// ptr to first record
	uint16_t							Count;										// number of records
} _ColList_t;
COMP_PACKED_END
extern _ColListRecord_t *sys_DynCol_Get_Record(_ColList_t *pColList, _ColListRecord_t *pStartRecord);
extern bool sys_DynCol_Clear_List(_ColList_t *pColList);
extern bool sys_DynCol_Add_Item (_ColList_t *pColList, void *pItem);
extern bool sys_DynCol_Remove_Item (_ColList_t *pColList, void *pItem);
extern _ColListRecord_t *sys_DynCol_Find_Item(_ColList_t *pColList, void *pItem);
//extern uint32_t sys_CountItem_DynCol(_ColList_t *pColList);
#endif


// ******************************************************************************************************
// 										BUFFERS
// ******************************************************************************************************
#if (CONF_SYS_BUFFERS_USED == 1)
typedef enum
{
		sBuffLinear		= 	(uint8_t)	1,											// classic linear buffer
		sBuffRing		= 				2,											// ring buffer
} sys_BufferType_t;

COMP_PACKED_BEGIN
typedef struct
{
	void 								*pDataArray;								// ptr to buffer memory area
	uint32_t							ByteSize;									// buffer memory area size in bytes	
	sys_BufferType_t					Type;										// buff type
	uint8_t								ElementByteSize;							// element size in bytes
	volatile uint32_t					ElementCount;
	uint8_t								*pWr;
	uint8_t								*pRd;
	uint8_t								*pStart;
	uint8_t								*pEnd;
	bool								Full;
	bool								Empty;
	bool								Ready;
} sys_Buffer_t;
COMP_PACKED_END


	extern sys_Buffer_t* 	sys_Buffer_Create(size_t MaxElementCount, uint8_t ElementByteSize, sys_BufferType_t Type, void *MemArray);
	extern void 			sys_Buffer_Change(sys_Buffer_t* pBuffer, size_t MaxElementCount, uint8_t ElementSize);
	extern void				sys_Buffer_Destroy(sys_Buffer_t* pBuffer);
	extern void 			sys_Buffer_Flush(sys_Buffer_t* pBuffer);
	extern uint32_t 		sys_Buffer_Read_Bytes( sys_Buffer_t *pBuff, uint8_t *pDst, uint32_t ByteLen);
	extern uint32_t 		sys_Buffer_Write_Bytes(sys_Buffer_t *pBuff, uint8_t *pSrc, uint32_t ByteLen);
	extern uint32_t 		sys_Buffer_Read_Element(sys_Buffer_t *pBuff, uint8_t *pDst, uint32_t ElementCount);
	extern uint32_t 		sys_Buffer_Write_Element(sys_Buffer_t *pBuff, uint8_t *pSrc, uint32_t ElementCount);
	extern uint32_t 		sys_Buffer_Copy_Bytes( sys_Buffer_t *pBuff, uint8_t *pDst, uint32_t RdOffset, uint32_t ByteLen);
	extern uint8_t*			sys_Buffer_Get_RdBytePtr( sys_Buffer_t *pBuff, uint32_t RdOffset);
#else
	typedef unsigned char	sys_Buffer_t;
#endif		// CONF_SYS_BUFFERS_USED


// ******************************************************************************************************
// 										EVENTS SYSTEM 
// ******************************************************************************************************
COMP_PACKED_BEGIN
typedef struct
{
	uint16_t 							Event_ID;
#if defined(CONF_DEBUG_STRING_NAME_SIZE) && (CONF_DEBUG_STRING_NAME_SIZE > 0)		// if text naming are enabled
	uint8_t  							Name[CONF_DEBUG_STRING_NAME_SIZE];
#endif	
	uint32_t 							Event_Counter;
} sys_Events_t;
COMP_PACKED_END

typedef enum EventType																// periferna struktura Eventov
{
	EV_NoChange							= 0,
	EV_DataReceived,																// new data available
//	EV_TransmitReady,																// send data ready
	EV_Transmitted,																	// send data done
	EV_StateChanged,																// Perihery was changed our state
} _EventType_t;



// ******************************************************************************************************
// 										TASK SYSTEM 
// ******************************************************************************************************
#if (CONF_SYS_TASKS_USED == 1)
COMP_PACKED_BEGIN
typedef struct 
{
	void 								*Arg;
	uint8_t 							ArgCount;
} sys_TaskArgs_t;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct
{
#if defined(CONF_DEBUG_STRING_NAME_SIZE) && (CONF_DEBUG_STRING_NAME_SIZE > 0)		// if text naming are enabled	
	const char 							*Name;
#endif	
	void 								*pCode;
	sys_TaskArgs_t						Argv;
} sys_TaskRecord_t;
COMP_PACKED_END

#define									NOARGS				(sys_TaskArgs_t) { NULL, 0}


COMP_PACKED_BEGIN
typedef struct
{
	_ColList_t							TaskList;									// Task list
} sys_Tasks_t;
COMP_PACKED_END
extern sys_Tasks_t 						sysTasks;

void sys_Task_Create_Item( const char * const Name, void (*pCode), sys_TaskArgs_t Argv);
void sys_Task_Execute_Item_First(void);
#endif		// if (CONF_SYS_TASKS_USED == 1)




// ******************************************************************************************************
// 										MODULE/DRIVER FUNCTIONS
// ******************************************************************************************************

#define IRQ_Handler_Bottom(x)			{}
	
typedef struct _If_Status															// periferna struktura
{
	bool 								Loaded;										// Load in list ?
	bool								Enabled; 									// Periferia povolena ??
	bool								Initialized;								// Periferia inicializovana ??
	volatile _EventType_t				LastEvent;									// last fired event
	volatile uint32_t					ChannelStatus;								// peripherial IRQ channel Status - depends on periphery
} _If_Status_t;
#define		_PERI_DEFAULT		{false, false, false, EV_NoChange}					// pouzite pri inicializacii struktury - default hodnoty


#if !defined(CONF_USE_MODDRV_MANAGER) || (CONF_USE_MODDRV_MANAGER == 0)
typedef struct _mod_sys_If
{
#if (CONF_SYS_COLLECTIONS_USED == 1)	
	_ColList_t							DrvLinked;									// linked drivers for current module
#endif	
	struct _If_Status					Stat;										// struktura so stavom
//	uint8_t								DataBuffer;									// lokalna premenna - buffer o dlzke 1 byte - pouzity pre cinanie-zapis z/do modulu
}_mod_sys_If_t;

// MODULE structure - easiest as if modules are used
typedef struct _mod_If
{
	_mod_sys_If_t						Sys;										// system's struct
#if defined(CONF_DEBUG_STRING_NAME_SIZE) && (CONF_DEBUG_STRING_NAME_SIZE > 0)		// if text naming are enabled	
	const char							Name[CONF_DEBUG_STRING_NAME_SIZE];			// interface name - text - only for ID
#endif	
} _mod_If_t;
#endif

struct _drv_If;																		// dummy structure 
struct _mod_If;																		// dummy structure 

typedef enum XferType
{
	XFer_Blocking						= 0,										// function wait for end transfer
	XFer_IRQ,																		// Function starting transfer. All othger goes through interrupt
//	XFer_DMA																		// Function start transfer via DMA (if Available)
} _XFerType_t;

// struktura API funkcii pre VSETKY drivery. 
typedef bool 	(*pInit_Handler_t)	 	( struct _mod_If *pRequestorModIf, struct _drv_If *pDrvIf);	// INIT
typedef bool 	(*pRestart_Handler_t)	( struct _mod_If *pRequestorModIf, struct _drv_If *pIf, bool Quiet); 	// RESTART
typedef bool 	(*Release_Handler_t)	( struct _mod_If *pRequestorModIf, struct _drv_If *pDrvIf); 	// Release - uvolnenie zariadenia
typedef bool	(*Enable_Handler_t) 	( struct _mod_If *pRequestorModIf, struct _drv_If *pDrvIf, uint32_t ChannelBitMask);				// Enable device
typedef bool	(*Disable_Handler_t)	( struct _mod_If *pRequestorModIf, struct _drv_If *pDrvIf, uint32_t ChannelBitMask);				// Disable device
typedef bool 	(*Write_Data_Handler_t)	( struct _mod_If *pRequestorModIf, struct _drv_If *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType); // Write data

typedef uint32_t(*Read_Data_Handler_t)	( struct _mod_If *pRequestorModIf, struct _drv_If *pDrvIf, sys_Buffer_t *pbuff, uint32_t RdLen, _XFerType_t XferType); // Read data
typedef void	(*pDrvIRQ_Handler_t)	( struct _drv_If *pDrvIf);
typedef	void 	(*pDrvEvent_Handler_t) 	( struct _drv_If *pDrvIf, _EventType_t Event);	// event handler function define type

typedef struct _Api
{
	pInit_Handler_t 				Init;											// Init - inicializacia modulu a jeho driverov
	pRestart_Handler_t				Restart;										// Restart - Restart of the driver, Quiet - init without public change
	Release_Handler_t				Release; 										// Release - uvolnenie zariadenia
	Enable_Handler_t				Enable;											// Enable device
	Disable_Handler_t				Disable;										// Disable device
	Write_Data_Handler_t			Write_Data;										// Write data
	Read_Data_Handler_t				Read_Data; 										// Read data
	pDrvEvent_Handler_t 			Events;
	void	*Ext;																	// Driver Specific API
} _drv_Api_t;


typedef struct _drv_sys_If
{
//		_ColList_t					ParentModLinked;	
	struct _drv_If					*pFirst;										// pointer to first interface
	struct _drv_If					*pPrev;											// pointer to previous interface
	struct _drv_If					*pNext;											// pointer to next interface
	struct _If_Status				Stat;											// status 
}_drv_sys_If_t;


COMP_PACKED_BEGIN
typedef struct _drv_If
{
	_drv_sys_If_t					Sys;											// system's struct
	const _drv_Api_t				*pAPI_STD;										// pointer to driver's API
	_drv_Api_t				 		*pAPI_USR;										// pointer to user's API
	void 							(*pIRQ_USR_HandlerTop)(struct _drv_If *pDrvIf);		// pointer to overrided IRQ Handler Top	
	void 							(*pIRQ_USR_HandlerBottom)(struct _drv_If *pDrvIf);	// pointer to overrided IRQ Handler Bottom
	const uint32_t 					IRQ_VecNum;										// IRQ Vector number
	const uint32_t					IRQ_Priority;									// priority of this handler
	const void						*pDrvSpecific;									// generic pointer to periphery specific structure
#if defined(CONF_DEBUG_STRING_NAME_SIZE) && (CONF_DEBUG_STRING_NAME_SIZE > 0)		// if text naming are enabled
	const char						Name[CONF_DEBUG_STRING_NAME_SIZE];				// interface name - text - only for ID	
#endif	
}_drv_If_t;
COMP_PACKED_END



// ******************************************************************************************************
// 										SYSTEM'S STRUCTURES
// ******************************************************************************************************
typedef enum
{
		sRESET		= 		(uint8_t) 	1,											// we are in Reset vectro (only if CATCH_RESET_VECTOR=1) !
		sSYSINIT	=					2,											// we are in sysinit and ChipInit
		sBSPINIT	= 					3,											// we are in BSP init
		sDBGINIT	= 					4,											// we are in Debug Init
		sMMGRINIT	= 					5,											// we are in Memory Manager Init
		sBOOT		= 					6,											// we are in main( )
		sMAIN		= 					7,											// we are in main loop
		sHALT		= 					8,											// we are stop in fault handler
		sEXIT		= 					9											// program exit
} sys_State_t;

typedef struct
{
		void*							pPeri;										// PTRDIFF_MAX to peripherial
		uint32_t						Timeout_ms;									// Interval for Reset Generation
		bool							Enable;										// Enabled?
		bool							Initialized;								// Initialized ? - ready to use?
} sys_WatchDog_t;



typedef struct
{
	CHAL_RST_Src_t						Reset;										// RESET signal source
	PRE_PACK sys_State_t				RunState;		POST_PACK					// Actual running state
	PRE_PACK volatile uint16_t			*pTemp;			POST_PACK					// teplota systemu (MCU)
	PRE_PACK bool						Error;			POST_PACK
} sys_Status_t;


extern sys_Status_t						sys_Status;




// ******************************************************************************************************
// 										MCU CHIP PART
// ******************************************************************************************************
#if defined (CONF_SYS_PART_INFO) && (CONF_SYS_PART_INFO == 1)
typedef struct
{
#if defined (CHIP_RTC_COUNT) && (CHIP_RTC_COUNT > 0)
	uint32_t							RTC_Freq;									// RTC frequency - crystal value. If not used, write = 0
#endif
#if defined (CHIP_USB_COUNT) && (CHIP_USB_COUNT > 0)
	uint32_t							USB_Freq;									// USB Src frequency
	uint8_t								USB_Dev_Address;
#endif
	uint32_t							PARTID;										// MCU silicone ID - read through IAP
	uint32_t							BOOTCODE;									// BootCode version - read through IAP
	uint32_t							SERNUM[4];									// MCU Silicone serial number - read through IAP
	const uint32_t						IFlash;										// internal memory size (FLASH) - described in datasheet
	const uint32_t						IRam;										// internal memory size (RAM) - described in datasheet
	const uint32_t						IEeprom;									// internal memory size (EEPROM) - described in datasheet
	const uint32_t						IEeprom_Start;								// internal eeprom memory start address
	void								(*Part_Refresh)();							// reload values in this structure
#if defined(CONF_SYS_STACKCHECK) && (CONF_SYS_STACKCHECK == 1)
	uint32_t							Stack_Size;									// Size of the Stack
	uint32_t							Stack_Start;								// Address of Stack start
	uint32_t							Stack_End;									// Address of Stacku end
	uint32_t							Stack_Pos;									// Last used position in Stack
	uint32_t							Heap_Size;									// Size of Heap
	uint32_t							Heap_Start;									// Address of Heap start
	uint32_t							Heap_End;									// Address of Heap end
	uint32_t							Heap_Pos;									// Last used position in Heap
	uint8_t								Heap_Occupied_Perc;							// Used Heap in perc.
	uint8_t								Stack_Occupied_Perc;						// Used Stack in perc.
#endif
} sys_Part_t;
#endif				//	CONF_SYS_PART_INFO

COMP_PACKED_BEGIN
typedef struct
{
	uint32_t							SRC_Freq;									// clock source frequency (crystal, IRC, ext, clock,...)
	uint32_t							CPU_Freq;									// Core CPU clock - (Core, CPU,...)
	uint32_t							TickPerUS;									// Number of CPU ticks per 1 usec	
	void								(*Clock_Refresh)();							// Clocks reload
} sys_Clock_t;
COMP_PACKED_END




// ******************************************************************************************************
// 										APPLICATION 
// ******************************************************************************************************
#if defined(CONF_SYS_APP_INFO) && (CONF_SYS_APP_INFO == 1)
typedef struct App
{
	const uint8_t						System_ID[sizeof(SYSTEM_NAME_STRING)];		// System ID String
	const uint8_t						System_Ver[sizeof(SYSTEM_VERSION_STRING)];	// System version
	const uint8_t						ID[sizeof(CONF_APP_NAME_STRING)];			// identifikacny retazec
	const uint8_t 						Ver[sizeof(CONF_APP_VERSION_STRING)];		// verziu aplikacie - meni sa s kazdou kompilaciou!
	const uint8_t						Target[sizeof(CONF_APP_TARGET)];			// target configuration "Debug/Release"
	const uint8_t						BSP_ID[sizeof(BSP_TYPE_STRING)];			// identifikacny retazec BSP HW
	const uint8_t 						BSP_Ver[sizeof(BSP_VERSION_STRING)];		// verziu BSP
	const uint8_t 						Builder_Ver[sizeof(TOOLCHAIN_VERSION_STRING)]; // verziu aplikacie - meni sa s kazdou kompilaciou!
	const uint8_t						Builder_Parm[sizeof(TOOLCHAIN_PARM_STRING)];// parametre kompilacie
	const uint8_t						Builder_Lib[sizeof(TOOLCHAIN_LIB_STRING)];	// kniznica pouzita pri kompilacii
	const uint8_t						Builder_DateTime[sizeof(BUILD_DATETIME_STRING)]; // Datum kompilacie
	const uint32_t						BuildDateInt;								// YYYYMMDD
	const uint32_t						BuildTimeInt;								// HHMMSS
} sys_App_t;
extern const sys_App_t 					sysApp;										// Public to whole project
#endif



// ******************************************************************************************************
// 										MEMORY 
// ******************************************************************************************************
#if CONF_SYS_MEM_INFO
COMP_PACKED_BEGIN
typedef struct
{
#if defined (CONF_SYS_MEM_INFO) && (CONF_SYS_MEM_INFO == 1)
	uint32_t							MEM_DATA_Low;								//	initialized data section
	uint32_t							MEM_DATA_Size;								//
	uint32_t							MEM_DATA_High;
	uint32_t							MEM_BSS_Low;								//	uninitialized data section
	uint32_t							MEM_BSS_Size;								//
	uint32_t							MEM_BSS_High;
	uint32_t							MEM_TEXT_Size;								//
	uint32_t							MEM_RAM_Size;								// Internal memory size
	uint32_t							MEM_RAM_Free;								// volne v RAM pamati
	uint32_t							MEM_FLASH_Low;								// Start of flash
	uint32_t							MEM_FLASH_Size;								// total size of flash
	uint32_t							MEM_FLASH_High;								// End of flash
	uint32_t							MEM_FLASH_Free;								// free in FLASH memory
	void							 	(*Memory_Refresh)();						// reload this structure
#endif
} sys_Memory_t;
COMP_PACKED_END
#endif

#if !defined(CONF_USE_MEM_MANAGER) || ( CONF_USE_MEM_MANAGER == 0)

#ifndef CONF_MEMALLOCHISTSIZE
	#define		CONF_MEMALLOCHISTSIZE			100
#endif

typedef struct sys_Mem_Alloc
{
	size_t 								nbytes;
	uint8_t 							*Address;
	volatile uint_fast64_t				AllocTimeMark;
}sys_Mem_Alloc_t;

typedef struct sys_MemHist
{
	uint64_t							*pOccupied;
	uint16_t							*pAllocCount;
	uint16_t							*pFragmLast;
#if defined(CONF_MEMALLOCHISTSIZE) && (CONF_MEMALLOCHISTSIZE > 0)
	sys_Mem_Alloc_t						*pHistory;
#endif	
}sys_MemHist_t;
extern const sys_MemHist_t 				sys_MemHist;
#endif



// ******************************************************************************************************
// 										SYS MEMORY FUNCTIONS 
// ******************************************************************************************************
extern void 			*sys_malloc_zero( unsigned nbytes );						//	Fill memory area to zeros
extern void 			*sys_malloc( unsigned nbytes );								//	Allocates memory in heap
extern void 			*sys_realloc( void *mem, unsigned int nbytes );				//	Reallocate selected memory area to new size
extern void 			sys_free( void* mem );										//	Freeing memory in heap
extern void 			sys_memset_zero( void *dest, unsigned int n );				//	Allocates memory in heap and clear to zero
extern void 			sys_memset( void *dest,uint8_t Sample, unsigned int n );	//  Fill memory witn sample uint8_t
extern void 			sys_memset_4(uint32_t *pDst, uint32_t Sample, uint32_t Count);	//  fill memory with uint32_ sample
extern void 			sys_memcpy( void *dest, const void *src, unsigned int n );	//  copy memory






// ******************************************************************************************************
// 										ALARM SYSTEM 
// ******************************************************************************************************
#if (CONF_SYS_ALARMS_USED == 1)
COMP_PACKED_BEGIN
typedef struct
{
	const char 							*Name;
	void 								*pCode;
	sys_TaskArgs_t						Argv;	
	uint32_t 							RepeatMS;									// alarm tick value
	uint64_t							LastAction;									// Tick value of last action
} sys_AlarmRecord_t;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct
{
	_ColList_t							AlarmList;									// Alarm list
} sys_Alarms_t;
COMP_PACKED_END


extern sys_AlarmRecord_t *sys_Alarm_Create_Item( const char * const Name, uint32_t RepeatMS, void (*pCode), sys_TaskArgs_t Argv);
extern void sys_Alarm_Destroy_Item(sys_AlarmRecord_t *AlarmRecord);
#endif		// if (CONF_SYS_ALARMS_USED == 1)

// ******************************************************************************************************
// 										RTC, COUNTERS
// ******************************************************************************************************

COMP_PACKED_BEGIN
typedef struct
{
	char 								RTC_string[20];								// HH:MM:SS.xxx , xxx=systickcount/1000
	uint16_t							RTC_MSec;									// miliseconds 0-999
	uint8_t								RTC_Sec;									// sekundy
	uint8_t								RTC_Min;									// minuty
	uint8_t								RTC_Hour;									// hodiny
	bool								SecondChangeFlag;							// Flag - set on each seconf change. Cleared after 200ms automatically
	bool								HalfSecondChangeFlag;						// Flag - set on each 500 ms change. Cleared after 200ms automatically
	volatile uint_fast32_t				SysTickCount;								// Systick counter
} sys_Counters_t;
COMP_PACKED_END

extern sys_Counters_t	sys_Cnts;
//extern volatile uint_fast32_t			SysTickCnt;									// fast counter
extern void sys_IRTC_Update(void);													// volane z SysTick prerusenia



// ******************************************************************************************************
// 										SYSTEM 
// ******************************************************************************************************
COMP_PACKED_BEGIN
typedef struct
{
#if defined(CONF_SYS_APP_INFO) && (CONF_SYS_APP_INFO == 1)
	const 	sys_App_t					*App;										// aplikacne info
#endif	
#if defined(CONF_SYS_MEM_INFO) && (CONF_SYS_MEM_INFO == 1)
	volatile sys_Memory_t 				*Memory;									// memory info
#endif	
#if defined(CONF_SYS_PART_INFO) && (CONF_SYS_PART_INFO == 1)	
	sys_Part_t							*Part;										// kremikove info
#endif	
	sys_Clock_t							*Clock;										// clock informations
	volatile sys_Counters_t				*Cnts;										// systemove pocitadla, aj RTC
	sys_Status_t						*Status;									// ptr to Status struct
	uint32_t							*MemMark;									// Pointer na Memory Mark v NoInit oblasti
#if defined(CONF_USE_DRV_MANAGER_EXT) && (CONF_USE_DRV_MANAGER_EXT == 1)
	_ColList_t							*DrvLoaded;
#endif	
#if defined(CONF_USE_MOD_MANAGER_EXT) && (CONF_USE_MOD_MANAGER_EXT == 1)
	_ColList_t							*ModLoaded;
#endif	
#if (CONF_SYS_TASKS_USED == 1)	
	sys_Tasks_t							*Tasks;
#endif
#if (CONF_SYS_ALARMS_USED == 1)
	sys_Alarms_t 						*Alarms;
#endif	
	sys_WatchDog_t						WatchDog;
} sys_System_t;
COMP_PACKED_END

extern const sys_System_t 				sys_System;									// Public to whole project





// ******************************************************************************************************
// 										OTHER SYSTEM FUNCTIONS
// ******************************************************************************************************
//					Example of __DATE__ string: "Jul 27 2012"
// 					Example of __TIME__ string: "21:06:19"
#define COMPUTE_BUILD_YEAR 				((__DATE__[ 7] - '0') * 1000 + (__DATE__[ 8] - '0') *  100 + (__DATE__[ 9] - '0') *   10 + (__DATE__[10] - '0'))
#define COMPUTE_BUILD_DAY				(((__DATE__[4] >= '0') ? (__DATE__[4] - '0') * 10 : 0) + (__DATE__[5] - '0'))
#define BUILD_MONTH_IS_JAN 				(__DATE__[0] == 'J' && __DATE__[1] == 'a' && __DATE__[2] == 'n')
#define BUILD_MONTH_IS_FEB 				(__DATE__[0] == 'F')
#define BUILD_MONTH_IS_MAR 				(__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'r')
#define BUILD_MONTH_IS_APR 				(__DATE__[0] == 'A' && __DATE__[1] == 'p')
#define BUILD_MONTH_IS_MAY 				(__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'y')
#define BUILD_MONTH_IS_JUN 				(__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'n')
#define BUILD_MONTH_IS_JUL 				(__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'l')
#define BUILD_MONTH_IS_AUG 				(__DATE__[0] == 'A' && __DATE__[1] == 'u')
#define BUILD_MONTH_IS_SEP 				(__DATE__[0] == 'S')
#define BUILD_MONTH_IS_OCT 				(__DATE__[0] == 'O')
#define BUILD_MONTH_IS_NOV 				(__DATE__[0] == 'N')
#define BUILD_MONTH_IS_DEC 				(__DATE__[0] == 'D')
#define COMPUTE_BUILD_MONTH 			( \
											(BUILD_MONTH_IS_JAN) ?  1 : \
											(BUILD_MONTH_IS_FEB) ?  2 : \
											(BUILD_MONTH_IS_MAR) ?  3 : \
											(BUILD_MONTH_IS_APR) ?  4 : \
											(BUILD_MONTH_IS_MAY) ?  5 : \
											(BUILD_MONTH_IS_JUN) ?  6 : \
											(BUILD_MONTH_IS_JUL) ?  7 : \
											(BUILD_MONTH_IS_AUG) ?  8 : \
											(BUILD_MONTH_IS_SEP) ?  9 : \
											(BUILD_MONTH_IS_OCT) ? 10 : \
											(BUILD_MONTH_IS_NOV) ? 11 : \
											(BUILD_MONTH_IS_DEC) ? 12 : \
											/* error default */  99 \
										)
#define COMPUTE_BUILD_HOUR 				((__TIME__[0] - '0') * 10 + __TIME__[1] - '0')
#define COMPUTE_BUILD_MIN  				((__TIME__[3] - '0') * 10 + __TIME__[4] - '0')
#define COMPUTE_BUILD_SEC  				((__TIME__[6] - '0') * 10 + __TIME__[7] - '0')
#define BUILD_DATE_IS_BAD 				(__DATE__[0] == '?')
#define BUILD_YEAR  					((BUILD_DATE_IS_BAD) ? 99 : COMPUTE_BUILD_YEAR)
#define BUILD_MONTH 					((BUILD_DATE_IS_BAD) ? 99 : COMPUTE_BUILD_MONTH)
#define BUILD_DAY   					((BUILD_DATE_IS_BAD) ? 99 : COMPUTE_BUILD_DAY)
#define BUILD_TIME_IS_BAD 				(__TIME__[0] == '?')
#define BUILD_HOUR  					((BUILD_TIME_IS_BAD) ? 99 :  COMPUTE_BUILD_HOUR)
#define BUILD_MIN   					((BUILD_TIME_IS_BAD) ? 99 :  COMPUTE_BUILD_MIN)
#define BUILD_SEC   					((BUILD_TIME_IS_BAD) ? 99 :  COMPUTE_BUILD_SEC)
#define BUILD_DATE						STR(BUILD_YEAR) "." STR(BUILD_MONTH) "." STR(BUILD_DAY)
#define BUILD_TIME						STR(BUILD_HOUR) "." STR(BUILD_MIN) "." STR(BUILD_SEC)

extern int sys_Err(const char *fmt, ...);
extern int sys_rand(void);
extern void sys_Empty(void);

#define GET_VAR_NAME_PTR(sig, index)	CONCAT(&sig,index)							// vytvor nazov premennej a vrat jej adresu ex: &sig_RX0


extern void (*sys_Assert_Fn)(const char *pCond_String, const char *pFile, const char *pFn, uint32_t line); 
extern void sys_Assert_Set_Fn(void (*dstFn)(const char *pCond_String, const char *pFile, const char *pFn, uint32_t line));


#endif // __SYSTEM_H_
