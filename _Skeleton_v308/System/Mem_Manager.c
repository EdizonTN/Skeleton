// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: Mem_Manager.h
// 	   Version: 1.0
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: Memory manager subsystem. based on Memory manager [based on https://github.com/vitiral/tinymem]
//			ROM:3696, RO:0, RW: 16508*, ZI:0 bytes. *depend on TM_POOL_SIZE value
//			memory pool are allocated in INRAM1 (0x0200 4000)
//
// Notice:
//
// Usage: - mem_index=tm_malloc(wantedsize);
//			
// ToDo: -	rewrite to use without absolute location address. Needs recalculate and find free space and use it (depends on tm_pool example).
//
// Changelog:
//


/*
ALOKACIA PAMATE
 po alokacii vrati index alokovanej pamate 
 Tento index v tmpool.pointers[index] ukazuje na .next -> to ak bol alokovany aj dalsi nezavisly blok (ak 0, tak toto je posledny prideleny index), 
 a .loc -> ktory ukazuje na offset tm_pool.pool[offset] kde lezi fizicky alokovana pamat.
 takze: allocated memory address =  addressof(tm_pool.pool[ (tm_pool.pointers[index].loc) ]);
 po alokacii sa meni:
	.filled[] |= (1<<index)  -  bitova kopia pouzitych indexov
	.points[] |= (1<<index)  -  bitova kopia pouzitych a volnych indexov
	.pointers[index].loc = offset v .poole, kde je zaciatok fyzicky alokovanej pamate
    .filled_blocks += pocet zabratych blokov
	.ptrs_filled += 1
	.last_index +=1 
	.first_index = ostava prvy prideleny. ak je toto prva alokacia potom = index

Ak ALOKUJE predtym UVOLNENU pamat (nachadzajuca sa v BINe)
tak do pointra


 
UVOLNENIE PAMATE
 pri pouziti tm_free, oznaci prvy blok z uvolnovanych na 0x00000000, takze   addressof(tm_pool.pool[ (tm_pool.pointers[index].loc) ]) = 0;
 taktiez je zmazany z bitovej kopie pouzitych indexov z tm_pool.filled    (& ~(1<<index))
 Dalej sa znizi pocitadlo: ptrs_filled-=1, a filled_blocks -= pocet uvolnenych blokov
 
 Po uvolneni pamate sa tieto informacie pricitaju do premennych s free blokmi (pre defragmentaciu):
 A to do bitovej kopie uvolnenych blokov 
 A zvysia sa pocidatla uvolnenych pointrov: ptrs_freed += 1, a pocitadlo uvolnenych blokov: freeds_blocks += pocet uvolnenych blokov
 Po uvolneni (free) sa meni:
	.filled[] &= ~(1<<index)  -  zmaze sa z bitovej kopia pouzitych indexov
	.pointers[index].loc = 0 znuluje sa, .next stale ukazuje na dalsi aktivny pointer
    .filled_blocks -= pocet uvolnenych blokov
	.freed_blocks += pocet uvolnenych blokov
	.ptrs_filled -= 1
	.ptrs_freed += 1
	.last_index -= 1
	.first_index = 0x0001 - asi by po uvolneni prveho indexu, sa mal zmenit na druhy vyssi aktivny index !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
 
 premenne tm_pool:
 .pool[n]: fyzicka pamat pre alokaciu, rozdelena na array - bloky
 .filled[2]: 0x0000 0000 0000 0001, binarny zapis uvolnenych indexov
 .points[2]: 0x0000 0000 0000 0001, binarnych zapis uvolnenych a pouzitych indexov spolu
 .pointers[idx]: all 0x0000, .loc= offset v poole ukazuje na fyzicky alokovanu pamat, .next = ak je nenulove,ukazuje ze toto nie je posledna alokacia
 .freed[12]: all 0x0000, Kos s uvolnenymi indexami. Pre 12 roznych velkosti....
 .filled_blocks: 0x0000, pocitadlo alokovanych blokov
 .freed_blocks: 0x0000, pocitadlo uvolnenych blokov (pred defragmentaciou)
 .ptrs_filled: 0x0001, pocitadlo pouzitych pointrov na alokaciu
 .ptrs_freed: 0x0000, pocitadlo uvolnenych pointrov predtym pouzitych na alokaciu (pred defragmentaciou)
 .find_index: 0x0000, pre potreby defragmentacie
 .last_index: 0x0000, posledny aktivny index prideleny po poziadavke na alokaciu
 .first_index: 0x0000, 
 .status: 0x00, poziadavka a priebeh defragmentacie
 .defrag_index: 0x0000, pre potreby defragmentacie
 find_index_bit: 0x01, pre potreby defragmentacie

*/

#include "Skeleton.h"

#if defined(CONF_USE_MEM_MANAGER) && (CONF_USE_MEM_MANAGER == 1)

// ******************************************************************************************************
// DEFAULT CONFIGURATION
// ******************************************************************************************************
#define MEM_MANAGER_OBJECTNAME			"Memory Manager"

#if TM_POOL_SIZE % (4)
#error "Invalid pool size, must be divisible by free_block"
#endif

/*#if (TM_MAX_POOL_PTRS % (8 * sizeof(int)))*/
#if (TM_MAX_POOL_PTRS % (8 * 4))
#error "Invalid pool ptrs size, must be divisible by int"
#endif
#ifndef CONF_MM_DYNMEM_SIZE
#error "Memory size for manager not defined!"
#endif
#ifndef CONF_MM_DYNMEM_ADR
#error "Memory location for manager not defined!"
#endif
#ifdef __MICROLIB	
#undef 	assert
#define assert(x)				__nop();
#undef 	exit
#define exit(x)					return(x);
#endif

// ******************************************************************************************************
// PUBLIC Variables and initialization
// ******************************************************************************************************
void *mem_memcpy(void * Dst, const void * Src, size_t Len) __attribute__((__nonnull__(1,2)));
void *mem_memset(void * Dst, int Sample, size_t Len) __attribute__((__nonnull__(1)));

// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************
#define TM_POOL_SIZE            		(CONF_MM_DYNMEM_SIZE - (CONF_MM_DYNMEM_SIZE % 4))			// size of memory pool. This is the maximum amount of memory that can be allocated in a memory pool
//		status bitcodes
#define TM_DEFRAG_FULL      			(1<<0)										// a full defrag has been requested
#define TM_DEFRAG_FAST      			(1<<1)										// a fast defrag has been requested
#define TM_DEFRAG_FULL_IP   			(1<<2)  									// A defrag is in progress
#define TM_DEFRAG_FAST_IP   			(1<<3)  									// A defrag is in progress
#define TM_MOVING           			(1<<4)  									// the memory manager is currently moving a block
#define TM_DEFRAG_FULL_DONE 			(1<<5)  									// this will be set after a full defrag has happend
#define TM_DEFRAG_FAST_DONE 			(1<<6)  									// this will be set after a fast defrag has happened.
#define TM_ERROR            			(1<<7)  									// a memory manager internal error occurred
#define TM_DEFRAG_IP        			(TM_DEFRAG_FULL_IP | TM_DEFRAG_FAST_IP) 	// defrag is in progress
#define TM_ANY_DEFRAG       			(TM_DEFRAG_FULL | TM_DEFRAG_FAST | TM_DEFRAG_IP) // some defrag has been requested
typedef uint16_t        				tm_blocks_t;

// TODO: packed!
typedef struct 																		// poolptr is used by Pool to track memory location and size
{
    tm_blocks_t loc;
    tm_index_t next;
} poolptr;

// TODO: packed!
typedef struct 																		// free_block is stored INSIDE of freed memory as a linked list of all freed data
{
    tm_index_t prev;
    tm_index_t next;
} free_block;
#define CEILING(x, y)           		(((x) % (y)) ? (x)/(y) + 1 : (x)/(y))
#define POOL_BLOCKS         			(TM_POOL_SIZE / sizeof(free_block))         // total blocks available
#define MAX_BIT_INDEXES     			(TM_MAX_POOL_PTRS / (8 * sizeof(int)))      // for filled/points
#define MAXUINT             			((unsigned int) 0xFFFFFFFFFFFFFFFF)
#define INTBITS             			(sizeof(int) * 8)                           // bits in an integer
// Masks of an integer
#define LOWER_MASK          			(((unsigned int)0xFFFFFFFFFFFFFFFF) >> (sizeof(int) * 8 / 2))
#define UPPER_MASK          			(LOWER_MASK << (sizeof(int) * 8 / 2))
// data is aligned on blocks
#define BLOCK_SIZE          			sizeof(free_block)                  		// size of a block in bytes
#define ALIGN_BLOCKS(size)  			CEILING(size, BLOCK_SIZE)           		// get block value that can encompase size
#define ALIGN_BYTES(size)   			(ALIGN_BLOCKS(size) * BLOCK_SIZE)   		// get value in bytes
#define DEFRAG_NOT_STARTED  			((tm_index_t) 0xFFFFFFFFFFFFFFFF)

#define tm_char_p(index)        		((char *)tm_void_p(index))
#define tm_int8_p(index)        		((int8_t *)tm_void_p(index))
#define tm_uint8_p(index)       		((uint8_t *)tm_void_p(index))
#define tm_int16_p(index)       		((int16_t *)tm_void_p(index))
#define tm_uint16_p(index)      		((uint16_t *)tm_void_p(index))
#define tm_int32_p(index)       		((int32_t *)tm_void_p(index))
#define tm_uint32_p(index)      		((uint32_t *)tm_void_p(index))


// ******************************************************************************************************
// PUBLIC Variables and initialization
// ******************************************************************************************************
																					// Initialize (reset) the pool
#define tm_init  			 {                                  \
    .filled = {1},                      /*NULL is taken*/       \
    .points = {1},                      /*NULL is taken*/       \
    .pointers = {{0, 0}},               /*heap = 0*/            \
    .freed = {0},                                               \
    .filled_blocks = {0},                                       \
    .freed_blocks = {0},                                        \
    .ptrs_filled = 1,                    /*NULL is "filled"*/   \
    .ptrs_freed = 0,                                            \
    .find_index = 0,                                            \
    .last_index = 0,                                            \
    .first_index = 0,                                           \
    .status = 0,                                                \
    .find_index_bit = 1,                /*index 0 is invalid*/  \
}	

typedef struct {																	// Pool object to track all memory usage. This is the main object used by tinymem to do memory management
    free_block      pool[POOL_BLOCKS];           									//	Actual memory pool (very large)
    unsigned int    filled[MAX_BIT_INDEXES];     									// bit array of filled pointers (only used, not freed)
    unsigned int    points[MAX_BIT_INDEXES];     									// bit array of used pointers (both used and freed)
    poolptr         pointers[TM_MAX_POOL_PTRS];     								// This is the index lookup location
    tm_index_t      freed[FREED_BINS];           									// binned storage of all freed indexes
    tm_blocks_t     filled_blocks;                									// total amount of data allocated
    tm_blocks_t     freed_blocks;                 									// total amount of data freed
    tm_index_t      ptrs_filled;                    								// total amount of pointers allocated
    tm_index_t      ptrs_freed;                     								// total amount of pointers freed
    tm_index_t      find_index;                     								// speed up find index
    tm_index_t      last_index;                     								// required to allocate off heap
    tm_index_t      first_index;                    								// required to start defrag
    uint8_t         status;                         								// status byte. Access with Pool_status macros
    tm_index_t      defrag_index;                									// used during defrag
    uint8_t         find_index_bit;                 								// speed up find index
} Pool;

Pool tm_pool __attribute__((at(CONF_MM_DYNMEM_ADR))) = tm_init;								// pool allocation and init - zablok tuto pamat pre ostatne pouzitie kompilerom


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
tm_index_t      find_index(void);
uint8_t         freed_bin(const tm_blocks_t blocks);
uint8_t         freed_bin_get(const tm_blocks_t blocks);
inline void     freed_remove(const tm_index_t index);
inline void     freed_insert(const tm_index_t index);
tm_index_t      freed_get(const tm_blocks_t size);
void		 	index_join(const tm_index_t index, const tm_index_t with_index);
bool 			index_split(const tm_index_t index, const tm_blocks_t blocks);
void 			index_remove(const tm_index_t index, const tm_index_t prev_index);
void 			index_extend(const tm_index_t index, const tm_blocks_t blocks, const bool filled);
#define 		free_p(index)  ((free_block *)tm_void_p(index))
void     		freed_remove(const tm_index_t index);
void     		freed_insert(const tm_index_t index);
void        	index_print(tm_index_t index);
tm_size_t		tm_sizeof(const tm_index_t index);	


// ******************************************************************************************************
// For testing
// ******************************************************************************************************
void            freed_print(void);
void            freed_full_print(bool full);
tm_index_t      freed_count_print(tm_size_t *size, bool pnt);
inline void     index_print(tm_index_t index);

tm_index_t      freed_count(tm_size_t *size);
tm_index_t      freed_count_bin(uint8_t bin, tm_size_t *size, bool pnt);
bool            freed_isvalid(void);
bool            freed_isin(const tm_index_t index);
bool            pool_isvalid(void);
void            fill_index(tm_index_t index);
bool            check_index(tm_index_t index);


// Access Pool Characteristics
#define BLOCKS_LEFT                 	(POOL_BLOCKS - tm_pool.filled_blocks)
#define BYTES_LEFT                  	(BLOCKS_LEFT * BLOCK_SIZE)
#define PTRS_USED                   	(tm_pool.ptrs_filled + tm_pool.ptrs_freed)
#define PTRS_LEFT                   	(TM_MAX_POOL_PTRS - PTRS_USED)
#define HEAP_LEFT                   	(POOL_BLOCKS - HEAP)
#define HEAP_LEFT_BYTES             	(HEAP_LEFT * BLOCK_SIZE)

// Get, set or clear the status bit (0 or 1) of name
#define STATUS(name)                	((tm_pool.status) & (name))
#define STATUS_SET(name)            	(tm_pool.status |= (name))
#define STATUS_CLEAR(name)          	(tm_pool.status &= ~(name))

// Access index characteristics
#define LOCATION(index)             	(tm_pool.pointers[index].loc)
#define HEAP                        	(tm_pool.pointers[0].loc)
#define NEXT(index)                 	(tm_pool.pointers[index].next)
#define FREE_NEXT(index)            	((free_p(index))->next)
#define FREE_PREV(index)            	((free_p(index))->prev)
#define BLOCKS(index)               	(LOCATION(tm_pool.pointers[index].next) - LOCATION(index))  // sizeof index in blocks
#define LOC_VOID(loc)               	(void*)(tm_pool.pool + ((loc) * BLOCK_SIZE))

// FILLED* does operations on Pool's `filled` bit array
// POINTS* does operations on Pool's `points` bit array
#define BITARRAY_INDEX(index)       	((index) / (sizeof(int) * 8))
#define BITARRAY_BIT(index)         	(1 << ((index) % (sizeof(int) * 8)))
#define FILLED(index)               	(tm_pool.filled[BITARRAY_INDEX(index)] &   BITARRAY_BIT(index))
#define FILLED_SET(index)           	(tm_pool.filled[BITARRAY_INDEX(index)] |=  BITARRAY_BIT(index))
#define FILLED_CLEAR(index)         	(tm_pool.filled[BITARRAY_INDEX(index)] &= ~BITARRAY_BIT(index))
#define POINTS(index)               	(tm_pool.points[BITARRAY_INDEX(index)] &   BITARRAY_BIT(index))
#define POINTS_SET(index)           	(tm_pool.points[BITARRAY_INDEX(index)] |=  BITARRAY_BIT(index))
#define POINTS_CLEAR(index)         	(tm_pool.points[BITARRAY_INDEX(index)] &= ~BITARRAY_BIT(index))



// ------------------------------------------------------------------------------------------------------
// memory copy - running in disabled interrupt
// ------------------------------------------------------------------------------------------------------
void *mem_memcpy(void * Dst, const void * Src, size_t Len)
{
	void *res;
	
	int irq_mask = __disable_irq();													// odloz stav prerusenia a zakaz ho
#undef memcpy																		// zrus presmerovanie - aby volal povodnu funkciu, inak sa tu zacykli
	res = memcpy(Dst, Src, Len);													// volaj povodny m,emcpy
#define memcpy			mem_memcpy	
	if(!irq_mask) __enable_irq();													// vrat povodny stav prerusenia
	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// funkcia memory Fill - odohrava sa v zakazanom preruseni
// ------------------------------------------------------------------------------------------------------
void *mem_memset(void * Dst, int Sample, size_t Len)
{
	void *res;
	
	int irq_mask = __disable_irq();													// odloz stav prerusenia a zakaz ho
#undef memset																		// zrus presmerovanie - aby volal povodnu funkciu, inak sa tu zacykli
	res = memset(Dst, Sample, Len);													// volaj povodny memset
#define memset			mem_memset	
	if(!irq_mask) __enable_irq();													// vrat povodny stav prerusenia
	
	return(res);	
}

// ------------------------------------------------------------------------------------------------------
//	Move memory from one index to another
// ------------------------------------------------------------------------------------------------------
#define MEM_MOVE(index_to, index_from)  memmove	(tm_void_p(index_to), tm_void_p(index_from), tm_sizeof(index_from))

// ------------------------------------------------------------------------------------------------------
// Get the sizeof data at index in bytes. 
// Return the sizeof the data pointed to by index.
// ------------------------------------------------------------------------------------------------------
inline tm_size_t tm_sizeof(const tm_index_t index)
{
    return BLOCKS(index) * BLOCK_SIZE;
}

// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
inline void MemManager_Init()
{
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)
	dbgprint("\r\n("MEM_MANAGER_OBJECTNAME") INIT:LOC:0x%X, SIZE:0x%X", &tm_pool, sizeof(tm_pool));
#endif
	memset(&tm_pool, 0x00, sizeof(tm_pool));
	tm_pool.filled[0] = 1;                      									// NULL is taken
    tm_pool.points[0] = 1;                      									// NULL is taken
    tm_pool.ptrs_filled = 1;                    									// NULL is "filled"
    tm_pool.find_index_bit = 1;                										// index 0 is invalid	
}

// ------------------------------------------------------------------------------------------------
//	cast a void pointer from index
//	pool      pointer to Pool struct
//	index     tm_index_t to get pointer to
// 	return     void* pointer to actual data
// ------------------------------------------------------------------------------------------------------
void *tm_void_p(const tm_index_t index)
{																					// Note: index 0 has location == heap (it is where Pool_heap is stored)
    if(LOCATION(index) >= HEAP) return NULL;
    return tm_pool.pool + LOCATION(index);
}

// ------------------------------------------------------------------------------------------------
// allocate memory from pool
// pool		pointer to Pool struct
// size		size of pointer to allocate
// return	tm_index_t corresponding to memory location
// 			On error or if not enough memory, return value == 0
// ------------------------------------------------------------------------------------------------------
tm_index_t tm_alloc(tm_size_t size)
{
    tm_index_t index;
	
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)
    	dbgprint("\r\n("MEM_MANAGER_OBJECTNAME") Alloc: %d bytes", size);
#endif		
    size = ALIGN_BLOCKS(size);  													// convert from bytes to blocks
    if(BLOCKS_LEFT < size) 
	{
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)
		dbgprint("\r\n("MEM_MANAGER_OBJECTNAME") Alloc: Err: not enought free blocks!");
#endif		
		return 0;
	}
    index = freed_get(size);
    if(index)
	{
        if(BLOCKS(index) != size)													// Split the index if it is too big
		{
            if(!index_split(index, size))
			{																		// Split can fail if there are not enough pointers
                tm_free(index);
                STATUS_SET(TM_DEFRAG_FAST);  										// need more indexes
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)
				dbgprint("\r\n("MEM_MANAGER_OBJECTNAME") Alloc: Err: not enought free indexes!");
#endif					
                return 0;
            }
        }
        return index;
    }
    if(HEAP_LEFT < size)
	{
        STATUS_SET(TM_DEFRAG_FAST);  												// need less fragmentation
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)
		dbgprint("\r\n("MEM_MANAGER_OBJECTNAME") Alloc: Err: need defrag!");
#endif				
        return 0;
    }
    index = find_index();
    if(!index)
	{
        STATUS_SET(TM_DEFRAG_FAST);  												// need more indexes
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)
		dbgprint("\r\n("MEM_MANAGER_OBJECTNAME") Alloc: Err: not enought free indexes!");
#endif				
        return 0;
    }
    index_extend(index, size, true);  												// extend index onto heap
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)
	dbgprint(" - OK. Index: %d!", index);
#endif		
    return index;
}


// ------------------------------------------------------------------------------------------------
// Changes the size of memory in the pool. See standard documentation on realloc for more info
//	pool      pointer to Pool struct
//	index     tm_index_t to realloc. If 0: acts as tm_alloc(size).
//	size      new requested size of index. If 0: acts as tm_free(index).
//	return    index with new size of memory. If this index has changed, the previous index is been freed.
//            If return value == 0, then no change has been done (or index has been freed if size=0).
// ------------------------------------------------------------------------------------------------------
tm_index_t tm_realloc(tm_index_t index, tm_size_t size)
{
    tm_index_t new_index;
    tm_blocks_t prev_size;
    size = ALIGN_BLOCKS(size);

    assert(0);
    if(!index) return tm_alloc(size);
    if(!FILLED(index)) return 0;
    if(!size)
	{
        tm_free(index);
        return 0;
    }
    new_index = NEXT(index);
    if(!FILLED(new_index))
	{
        index_join(index, new_index);												// If next index is free, always join it first
    }
    prev_size = BLOCKS(index);
    if(size == BLOCKS(index)) return index;
    if(size < prev_size)
	{  																				// shrink data
        if(!index_split(index, size)) return 0;
        return index;
    } else
	{  																				// grow data
        new_index = tm_alloc(size * BLOCK_SIZE);
        if(!new_index) return 0;
        MEM_MOVE(new_index, index);
        tm_free(index);
        return new_index;
    }
}

// ------------------------------------------------------------------------------------------------
//	free allocated memory from pool
//	pool      pointer to Pool struct
//	index     tm_index_t to free
// ------------------------------------------------------------------------------------------------------
void tm_free(const tm_index_t index)
{
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)
    	dbgprint("\r\n("MEM_MANAGER_OBJECTNAME") Free[%d]", index);
#endif			
    if(LOCATION(index) >= HEAP) 
	{	
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)		
		dbgprint("\r\n("MEM_MANAGER_OBJECTNAME") Free: Err: Index is out of heap.");		
#endif		
		return;
	}
    if((index >= TM_MAX_POOL_PTRS) || (!FILLED(index))) 
	{	
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)		
		dbgprint("\r\n("MEM_MANAGER_OBJECTNAME") Free: Err: Index was not used.");		
#endif
		return;
	}		
    assert(pool_isvalid());
    FILLED_CLEAR(index);
    tm_pool.filled_blocks -= BLOCKS(index);
    tm_pool.freed_blocks += BLOCKS(index);
    tm_pool.ptrs_filled--;
    tm_pool.ptrs_freed++;

    freed_insert(index);
    assert(pool_isvalid());
    if(!FILLED(NEXT(index)))
	{
        index_join(index, NEXT(index));
    }
    assert(pool_isvalid());
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)		
		dbgprint("OK.\r\n");
#endif	

}

// ------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
bool tm_defrag()				
{																					// TODO: implement re-entrant threaded
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)
    tm_index_t i = 0;
    tm_blocks_t used = tm_pool.filled_blocks;
    tm_blocks_t available = BLOCKS_LEFT, heap = HEAP_LEFT, freed = tm_pool.freed_blocks;
#endif
    tm_blocks_t blocks;
    tm_blocks_t location;
    tm_index_t prev_index = 0;
	
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)
    	dbgprint("\r\n("MEM_MANAGER_OBJECTNAME")  DEFRAG Started ");
#endif		
	
    if(!STATUS(TM_DEFRAG_IP))
	{
        tm_pool.defrag_index = tm_pool.first_index;
        STATUS_CLEAR(TM_ANY_DEFRAG);
        STATUS_SET(TM_DEFRAG_FULL_IP);
    }
    if(!tm_pool.defrag_index) goto done;
    while(NEXT(tm_pool.defrag_index))
	{
        if(!FILLED(tm_pool.defrag_index))
		{
            if(!FILLED(NEXT(tm_pool.defrag_index)))
			{
                // printf("  joining above\n", tm_pool.defrag_index);
                index_join(tm_pool.defrag_index, NEXT(tm_pool.defrag_index));
            }

            // printf("### Defrag: loop=%-11u", i); index_print(tm_pool.defrag_index);
            assert(FILLED(NEXT(tm_pool.defrag_index)));
            blocks = BLOCKS(NEXT(tm_pool.defrag_index));        					// store size of actual data
            location = LOCATION(NEXT(tm_pool.defrag_index));    					// location of actual data

            // Make index "filled", we will split it up later
            freed_remove(tm_pool.defrag_index);
            FILLED_SET(tm_pool.defrag_index);
            tm_pool.ptrs_filled++, tm_pool.filled_blocks+=BLOCKS(tm_pool.defrag_index);
            tm_pool.ptrs_freed--, tm_pool.freed_blocks-=BLOCKS(tm_pool.defrag_index);

            // Do an odd join, where the locations are just equal
            // printf("  New bef join  :           "); index_print(NEXT(tm_pool.defrag_index));
            LOCATION(NEXT(tm_pool.defrag_index)) = LOCATION(tm_pool.defrag_index);
            // printf("  After odd join:           "); index_print(tm_pool.defrag_index);
            // printf("  New after join:           "); index_print(NEXT(tm_pool.defrag_index));

            // Now remove the index. Note that the size is == 0
            //      Also note that even though it was removed, it's NEXT and LOCATION
            //      are still valid (not changed in remove index)
            index_remove(tm_pool.defrag_index, prev_index);
            prev_index = NEXT(tm_pool.defrag_index);  								// defrag_index was removed

            memmove(LOC_VOID(LOCATION(prev_index)), LOC_VOID(location), ((tm_size_t)blocks) * BLOCK_SIZE);
            // printf("  New index, before split:  "); index_print(prev_index);
            if(!index_split(NEXT(tm_pool.defrag_index), blocks))
			{
                assert(0); 
				exit(-1);
            }
            assert(pool_isvalid());
            // printf("  New index, after split:   "); index_print(prev_index);
            // tm_debug("blocks=%u, freespace=%u, %u==%u", blocks, BLOCKS(NEXT(prev_index)), used, tm_pool.filled_blocks);
            // tm_debug("%u==%u", BLOCKS(prev_index), blocks);
            assert(BLOCKS(prev_index) == blocks);
			tm_pool.defrag_index = NEXT(prev_index);
            // tm_debug("%u==%u", used, tm_pool.filled_blocks);
            assert(!FILLED(tm_pool.defrag_index));
        } else
		{
            prev_index = tm_pool.defrag_index;
            tm_pool.defrag_index = NEXT(tm_pool.defrag_index);
        }
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)	
        assert(prev_index != tm_pool.defrag_index);
        assert((i++, used == tm_pool.filled_blocks));
        assert(available == BLOCKS_LEFT);
        assert(pool_isvalid());
#endif 		
    }
done:
    if(!FILLED(tm_pool.defrag_index))
	{
        // tm_debug("removing last index");
        // printf("index       :"); index_print(tm_pool.defrag_index);
        // printf("prev_index  :"); index_print(prev_index);
        index_remove(tm_pool.defrag_index, prev_index);
    }
    STATUS_CLEAR(TM_DEFRAG_IP);
    STATUS_SET(TM_DEFRAG_FULL_DONE);
#if defined(CONF_DEBUG_MEM_MANAGER) && (CONF_DEBUG_MEM_MANAGER == 1)
	dbgprint("\r\n("MEM_MANAGER_OBJECTNAME") Defarg Done. Heap left: start=%u, end=%u, recovered=%u, wasfree=%u was_avail=%u isavail=%u", heap, HEAP_LEFT, HEAP_LEFT - heap, freed, available, BLOCKS_LEFT);
    assert(HEAP_LEFT - heap == freed);
    assert(tm_pool.freed_blocks == 0);
    assert(HEAP_LEFT == BLOCKS_LEFT);
#endif		
    return 0;
}



// ------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
tm_index_t find_index()
{
    uint8_t loop;
    unsigned int bits;
    uint8_t bit;
    uint8_t i;
    if(!PTRS_LEFT) return 0;
    for(loop=0; loop<2; loop++){
        for(; tm_pool.find_index < MAX_BIT_INDEXES; tm_pool.find_index++)
		{
            bits = tm_pool.points[tm_pool.find_index];
            if(bits != MAXUINT){
                bit = 0;
                // tm_debug("bits=%x", bits);
                // tm_debug("lower mask=%x", LOWER_MASK);
                // tm_debug("operation=%x", bits & LOWER_MASK);
                if((bits & LOWER_MASK) == LOWER_MASK)
				{
                    // bits are in the second half
                    bit += sizeof(int)  * 8 / 2;
                    bits = bits >> (sizeof(int) * 8 / 2);
                    // tm_debug("ubits=%x", bits);
                }
                // tm_debug("for bits=%x", bits);
                for(i=0; i < sizeof(int) * 2; i++){
                    switch(bits & 0xF)
					{
                        case 0: case 2: case 4: case 6:
                        case 8: case 10: case 12: case 14:
                            goto found;
                        case 1: case 5: case 9: case 13:
                            bit += 1;
                            goto found;
                        case 3: case 11:
                            bit += 2;
                            goto found;
                        case 7:
                            bit += 3;
                            goto found;
                    }
                    bit += 4;
                    bits = bits >> 4;
                    // tm_debug("breaking bits=%x", bits);
                }
                assert(0);
found:
                // tm_debug("bit=%u, true_bits=%x", bit, tm_pool.points[tm_pool.find_index]);
                // tm_debug("index=%u", tm_pool.find_index * INTBITS + bit);
                assert(!POINTS(tm_pool.find_index * INTBITS + bit));
                assert(!FILLED(tm_pool.find_index * INTBITS + bit));
                return tm_pool.find_index * INTBITS + bit;
            }
        }
        tm_pool.find_index = 0;
    }
    assert(0);
	return(0);
}


// ------------------------------------------------------------------------------------------------
//      get the freed bin for blocks
// ------------------------------------------------------------------------------------------------------
uint8_t freed_bin(const tm_blocks_t blocks)
{
    switch(blocks)
	{
        case 1:                     return 0;
        case 2:                     return 1;
        case 3:                     return 2;
    }
    if(blocks < 64)
	{
        if(blocks < 8)              return 3;
        else if(blocks < 16)        return 4;
        else if(blocks < 32)        return 5;
        else                        return 6;
    }
    else if(blocks < 1024)
	{
        if(blocks < 128)            return 7;
        else if(blocks < 256)       return 8;
        else if(blocks < 512)       return 9;
        else                        return 10;
    }
    else                            return 11;
}


// ------------------------------------------------------------------------------------------------
// Used for getting the bin to return. Makes certain that the size
// value is >= blocks
// ------------------------------------------------------------------------------------------------------
uint8_t freed_bin_get(const tm_blocks_t blocks)
{
   // If it aligns perfectly with power of 2, we can fit it
    switch(blocks)
	{
        case 1:                     return 0;
        case 2:                     return 1;
        case 3:                     return 2;
        case 4:                     return 3;
        case 8:                     return 4;
        case 16:                    return 5;
        case 32:                    return 6;
        case 64:                    return 7;
        case 128:                   return 8;
        case 256:                   return 9;
        case 512:                   return 10;
        case 1024:                  return 11;
    }

    // Else, we only know that the bin above it will fit it
    //      User has to make sure bin != 12, if it does it has to check the size
    return freed_bin(blocks) + 1;
}


// ------------------------------------------------------------------------------------------------
// remove the index from the freed array. This doesn't do anything else
//      It is very important that this is called BEFORE any changes
//      to the index's size
// ------------------------------------------------------------------------------------------------------
inline void freed_remove(const tm_index_t index)
{
    assert(!FILLED(index));
    if(FREE_PREV(index))
	{
																					// if previous exists, move it's next as index's next
        //tm_debug("index=%u, location=%u, prev=%u", index, LOCATION(index), FREE_PREV(index));
        assert(FREE_NEXT(FREE_PREV(index)) == index);
        FREE_NEXT(FREE_PREV(index)) = FREE_NEXT(index);
    } else
	{ 																				// free is first element in the bin
        assert(tm_pool.freed[freed_bin(BLOCKS(index))] == index);
        tm_pool.freed[freed_bin(BLOCKS(index))] = FREE_NEXT(index);
    }
    if(FREE_NEXT(index)) 
	{	
		FREE_PREV(FREE_NEXT(index)) = FREE_PREV(index);
	}
}


// ------------------------------------------------------------------------------------------------------
// Insert the index onto the correct freed bin
//      (inserts at position == 0)
// Does not do ANY other record keeping (no adding ptrs, blocks, etc)
// ------------------------------------------------------------------------------------------------------
inline void freed_insert(const tm_index_t index)
{
    uint8_t bin = freed_bin(BLOCKS(index));
    assert(!FILLED(index));
    *free_p(index) = (free_block){.next=tm_pool.freed[bin], .prev=0};
    if(tm_pool.freed[bin])
	{																				// If a previous index exists, update it's previous value to be index
        FREE_PREV(tm_pool.freed[bin]) = index;
    }
    tm_pool.freed[bin] = index;
}


// ------------------------------------------------------------------------------------------------------
// Get an index from the freed array of the specified size. The
//      index settings are automatically set to filled
// ------------------------------------------------------------------------------------------------------
tm_index_t freed_get(const tm_blocks_t blocks)
{
    tm_index_t index;
    uint8_t bin = freed_bin_get(blocks);
    if(bin == FREED_BINS)
	{  																				// size is off the binning charts
        index = tm_pool.freed[FREED_BINS-1];
        while(index)
		{
            if(BLOCKS(index) >= blocks) goto found;
            index = FREE_NEXT(index);
        }
        // no need to return here: bin == FREED_BINS
    }
    for(; bin<FREED_BINS; bin++)
	{
        index = tm_pool.freed[bin];
        if(index)
		{
found:      assert(!FILLED(index));
            assert(POINTS(index));
            freed_remove(index);
            FILLED_SET(index);
            // Mark the index as filled
            tm_pool.filled_blocks += BLOCKS(index);
            tm_pool.freed_blocks -= BLOCKS(index);
            tm_pool.ptrs_filled++;
            tm_pool.ptrs_freed--;
            return index;
        }
    }
    return 0;
}


// ------------------------------------------------------------------------------------------------------
// join index with_index. with_index will be removed
// ------------------------------------------------------------------------------------------------------
void index_join(tm_index_t index, tm_index_t with_index)
{
	//tm_index_t temp;
    do
	{
        assert(!FILLED(with_index));
        assert(LOCATION(index) <= LOCATION(with_index));
        if(!FILLED(index)) freed_remove(index); 									// index has to be rebinned, remove before changing size
        index_remove(with_index, index);											// Remove and combine the index
        if(!FILLED(index)) freed_insert(index); 									// rebin the index
        with_index = NEXT(index);
    }while(!FILLED(with_index));
}


// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
bool index_split(const tm_index_t index, const tm_blocks_t blocks)
{
    assert(blocks < BLOCKS(index));
    tm_index_t new_index = NEXT(index);
    if(!FILLED(new_index))															// If next index is free, always join it first. This also frees up new_index to use however we want!
	{
        //tm_debug("split: joining");
        index_join(index, new_index);
    } else{
        //tm_debug("split: getting new index");
        new_index = find_index();
        if(!new_index) return false;
    }

    assert(!POINTS(new_index));
    assert(!FILLED(new_index));
    POINTS_SET(new_index);															// update new index for freed data

    if(FILLED(index))																// there will be some newly freed data
	{ 
        tm_pool.freed_blocks += BLOCKS(index) - blocks;
        tm_pool.filled_blocks -= BLOCKS(index) - blocks;
    }

    tm_pool.ptrs_freed++;
    tm_pool.pointers[new_index] = (poolptr) {.loc = LOCATION(index) + blocks, .next = NEXT(index)};
    NEXT(index) = new_index;

    freed_insert(new_index);														// mark changes
    if(tm_pool.last_index == index)
	{
        tm_pool.last_index = new_index;
    }
    else
	{
        assert(NEXT(index));
        assert(NEXT(new_index));
    }
    return true;
}


// ------------------------------------------------------------------------------------------------------
// Completely remove the index. Used for combining indexes and when defragging
//      from end (to combine heap).
//      This function also combines the indexes (NEXT(prev_index) = NEXT(index))
// Note: NEXT(index) and LOCATION(index) **must not change**. They are used by other code
// ------------------------------------------------------------------------------------------------------
void index_remove(const tm_index_t index, const tm_index_t prev_index)
{
    //  Update information
    // possibilities:
    //      both are free:              this happens when joining free indexes
    //      prev filled, index free:    this happens for realloc and all the time
    //      prev free, index filled:    this should only happen during defrag
    //      both filled:                this should never happen
    switch(((FILLED(prev_index) ? 1:0) << 1) + (FILLED(index) ? 1:0))
	{
        case 0:  																	// merging two free values
            assert(freed_isin(index));
            tm_pool.ptrs_freed--;
            if(!NEXT(index)) tm_pool.freed_blocks -= BLOCKS(index);					// if index is last value, freed_blocks will be reduced
		tm_pool.freed[freed_bin(BLOCKS(index))] = FREE_NEXT(index);					// EdizonTN: remove from BIN
            break;
        case 2:  																	// growing prev_index "up"
            freed_remove(index);
            tm_pool.freed_blocks -= BLOCKS(index);
            if(NEXT(index)) tm_pool.filled_blocks += BLOCKS(index);					// grow prev_index, unless index is last value
            tm_pool.ptrs_freed--;
            break;
        case 3:																		// combining two filled indexes, used ONLY in defrag
            assert(STATUS(TM_DEFRAG_IP));  											// TODO: have a better way to check when threading
            tm_pool.ptrs_filled--;
            break;
        default:
            assert(0);
    }

    if(index == tm_pool.first_index) tm_pool.first_index = NEXT(index);
    if(NEXT(index)) 																// Combine indexes (or heap)
	{
        NEXT(prev_index) = NEXT(index);
    } 
	else
	{ 																				// this is the last index
        assert(tm_pool.last_index == index);
        tm_pool.last_index = prev_index;
        if(prev_index)  NEXT(prev_index) = 0;
        else            tm_pool.first_index = 0;
        HEAP = LOCATION(index);
    }
    FILLED_CLEAR(index);
    POINTS_CLEAR(index);
}


// ------------------------------------------------------------------------------------------------------
// extend index onto the heap
// ------------------------------------------------------------------------------------------------------
void index_extend(const tm_index_t index, const tm_blocks_t blocks, const bool filled)
{
    assert(!POINTS(index));
    assert(!FILLED(index));
    POINTS_SET(index);
    tm_pool.pointers[index] = (poolptr) {.loc = HEAP, .next = 0};
    HEAP += blocks;
    if(tm_pool.last_index) NEXT(tm_pool.last_index) = index;
    tm_pool.last_index = index;
    if(!tm_pool.first_index) tm_pool.first_index = index;
    if(filled)
	{
        FILLED_SET(index);
        tm_pool.filled_blocks += blocks;
        tm_pool.ptrs_filled++;
    }
    else{
        assert(!FILLED(index));
        tm_pool.freed_blocks += blocks;
        tm_pool.ptrs_freed++;
    }
}

//      For Debug and Test
#define PRIME       (65599)
bool testing = false;


// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
void pool_print(void)
{
    printf("## Pool (status=%x):\n", tm_pool.status);
    printf("    mem blocks: heap=  %-7u     filled=%-7u  freed=%-7u     total=%-7u\n", HEAP, tm_pool.filled_blocks, tm_pool.freed_blocks, POOL_BLOCKS);
    printf("    avail ptrs: filled=  %-7u   freed= %-7u   used=%-7u,    total= %-7u\n", tm_pool.ptrs_filled, tm_pool.ptrs_freed, PTRS_USED, TM_MAX_POOL_PTRS);
    printf("    indexes   : first=%u, last=%u\n", tm_pool.first_index, tm_pool.last_index);
}


// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
void freed_print(void)
{
    freed_full_print(false);
}


// ------------------------------------------------------------------------------------------------------
// 
// ------------------------------------------------------------------------------------------------------

void freed_full_print(bool full)
{
    uint8_t bin;
    tm_size_t size = 0, size_get;
    tm_index_t count = 0, count_get;
	
    printf("## Freed Bins:\n");
    for(bin=0; bin<FREED_BINS; bin++)
	{
        count_get = freed_count_bin(bin, &size_get, full);
        if(count_get) printf("    bin %4u: size=%-8u count=%-8u\n", bin, size_get, count_get);
        count += count_get;
        size += size_get;
    }
    printf("TOTAL: size=%u, count=%u\n", size, count);
}

// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
tm_index_t freed_count_print(tm_size_t *size, bool pnt)
{
    uint8_t bin;
    tm_size_t size_get;
    tm_index_t count = 0;
    *size = 0;
    for(bin=0; bin<FREED_BINS; bin++)
	{
        count += freed_count_bin(bin, &size_get, pnt);
        *size += size_get;
    }
    assert(count==tm_pool.ptrs_freed);
    assert(*size==tm_pool.freed_blocks * BLOCK_SIZE);
    return count;
}


// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
inline void index_print(tm_index_t index)
{
    printf("index %-5u(%u,%u):bl=%-4u, l=%-5u, n=%-5u, f/l=%u,%u", index, !!POINTS(index), !!FILLED(index), BLOCKS(index), LOCATION(index), NEXT(index), tm_pool.first_index == index, tm_pool.last_index == index);
    if(FILLED(index)) printf("\n");
    else printf(" free:b=%u p=%u n=%u\n", freed_bin(BLOCKS(index)), FREE_PREV(index), FREE_NEXT(index));
}

// ------------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
tm_index_t freed_count(tm_size_t *size)
{
    return freed_count_print(size, false);
}


// ------------------------------------------------------------------------------------------------------
// Get the number and the size of the items in bin
// ------------------------------------------------------------------------------------------------------
tm_index_t freed_count_bin(uint8_t bin, tm_size_t *size, bool pnt)
{
    tm_index_t index = tm_pool.freed[bin];
    tm_index_t count = 0;
    *size = 0;
    if(!index) return 0;
    assert(FREE_PREV(index)== 0);
    while(index){
        // tm_debug("loc=%u, blocks=%u", LOCATION(index), POOL_BLOCKS);
        assert(LOCATION(index) < POOL_BLOCKS);
        assert(POINTS(index));
        assert(!FILLED(index));
        if(pnt) printf("        prev=%u, ind=%u, next=%u\n", FREE_PREV(index), index, FREE_NEXT(index));
        *size += tm_sizeof(index);
        count++;
        if(FREE_NEXT(index)) assert(index == FREE_PREV(FREE_NEXT(index)));
        index = FREE_NEXT(index);
    }
    return count;
}

// ------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
bool freed_isvalid()
{
    tm_size_t size;
    tm_index_t count = freed_count(&size);
    size = ALIGN_BLOCKS(size);
    if(!((count==tm_pool.ptrs_freed) && (size==tm_pool.freed_blocks)))
	{
//        tm_debug("freed: %u==%u", count, tm_pool.ptrs_freed);
//        tm_debug("size:  %u==%u", size, tm_pool.freed_blocks);
        return false;
    }
    return true;
}

// ------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
bool freed_isin(const tm_index_t index)
{
    tm_index_t findex = tm_pool.freed[freed_bin(BLOCKS(index))];
    while(findex)
	{
        assert(findex != FREE_NEXT(findex));
        if(findex==index) return true;
        findex = FREE_NEXT(findex);
    }
    return false;
}

// ------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
bool pool_isvalid()
{
    tm_blocks_t filled = 0, freed=0;
    tm_index_t ptrs_filled = 1, ptrs_freed = 0;
    tm_index_t index;
    bool flast = false; 															// found first
	bool ffirst = false;  															// found last
    bool freed_first[FREED_BINS] = {0};  											// found freed first bin
    bool freed_last[FREED_BINS] = {0};   											// found freed last bin
    uint8_t bin;

    assert(HEAP <= POOL_BLOCKS); assert(BLOCKS_LEFT <= POOL_BLOCKS);
    assert(PTRS_LEFT < TM_MAX_POOL_PTRS);
    
    for(index=1; index<TM_MAX_POOL_PTRS; index++)									// do a basic complete check on ALL indexes
	{
        if((!POINTS(index)) && FILLED(index)){
            //tm_debug("[ERROR] index=%u is filled but doesn't point", index);
            index_print(index);
            return false;
        }
        if(POINTS(index) && (!FILLED(index)))
		{
			// Tadaaaa....
        }
        if(POINTS(index))
		{
            assert(NEXT(index) < TM_MAX_POOL_PTRS);
            if(!NEXT(index))
			{
                if(flast || (tm_pool.last_index != index))
				{
                    //tm_debug("last index error");
                    index_print(index);
                    return 0;
                }
                flast = true;
            } 
			if(tm_pool.first_index == index)
			{
                assert(!ffirst); ffirst = true;
            }
            if(FILLED(index))
			{
				filled+=BLOCKS(index); 
				ptrs_filled++;
			}
            else
			{
                freed+=BLOCKS(index); 
				ptrs_freed++;
                // Check to make sure it is in the correct areas
                assert(FREE_NEXT(index) < TM_MAX_POOL_PTRS);
                assert(FREE_PREV(index) < TM_MAX_POOL_PTRS);

                if(!freed_isin(index))
				{
                    //tm_debug("[ERROR] index=%u is freed but isn't in freed array", index);
                    index_print(index);
                    return false;
                }
                // Make sure the freed arrays have one first and one last
                bin = freed_bin(BLOCKS(index));
                assert(freed_isin(index));
                if(!FREE_PREV(index))
				{
                    if((tm_pool.freed[bin] != index) || freed_first[bin])
					{
                        //tm_debug("bin=%u, %u==%u", bin, tm_pool.freed[bin], index);
                        //tm_debug("[ERROR] index=%u has no prev but isn't first bin", index);
                        index_print(index);
                        return 0;
                    }
                    freed_first[bin] = true;
                } 
				else if(FREE_NEXT(FREE_PREV(index)) != index)
				{
                    //tm_debug("free array is corrupted. index=%u: %u!=%u", index, FREE_NEXT(FREE_PREV(index)), index);
                    index_print(index);
                    return 0;
                }
                if(!FREE_NEXT(index))
				{
                    assert(!freed_last[bin]); freed_last[bin] = true;
                } 
				else if(FREE_PREV(FREE_NEXT(index)) != index)
				{
                    //tm_debug("free array is corrupted. index=%u: %u!=%u", index, FREE_PREV(FREE_NEXT(index)), index);
                    index_print(index);
                    index_print(FREE_NEXT(index));
                    return 0;
                }
            }
        } 
		else
		{
            assert(tm_pool.last_index != index); 
			assert(tm_pool.first_index != index);
        }
    }
    // Make sure we found the first and last index (or no indexes exist)
    if(PTRS_USED > 1) assert(flast && ffirst);
//    else 
//	{
//		assert(!(tm_pool.last_index || tm_pool.first_index));
//	}

    // Make sure we found all the freed values
    for(bin=0; bin<FREED_BINS; bin++)
	{
        // tm_debug("free find: bin=%u first=%u, last=%u", bin, freed_first[bin], freed_last[bin]);
        if(tm_pool.freed[bin]){
            if(!(freed_first[bin] && freed_last[bin]))
			{
                //tm_debug("did not find first of bin=%u", bin);
                return 0;
            }
        }
        else assert(!(freed_first[bin] || freed_last[bin]));
    }

    // check that we have proper count of filled and freed
    if((filled != tm_pool.filled_blocks) || (freed != tm_pool.freed_blocks))
	{
        //tm_debug("filled and/or freed blocks don't match count filled=%u, freed=%u", filled, freed);
        pool_print();
        return 0;
    } if((ptrs_filled != tm_pool.ptrs_filled) || (ptrs_freed != tm_pool.ptrs_freed))
	{
        //tm_debug("filled and/or freed ptrs don't match count filled_p=%u, freed_p=%u", ptrs_filled, ptrs_freed);
        pool_print();
        return 0;
    }

    filled=0, freed=0, ptrs_freed=0, ptrs_filled=1;
    index = tm_pool.first_index;
    while(index)
	{
        if(FILLED(index)) {filled+=BLOCKS(index); ptrs_filled++;}
        else {freed+=BLOCKS(index); ptrs_freed++;}
        index = NEXT(index);
    }
    if((filled != tm_pool.filled_blocks) || (freed != tm_pool.freed_blocks))
	{
        //tm_debug("2nd test: filled and/or freed blocks don't match count: filled=%u, freed=%u", filled, freed);
        pool_print();
        return 0;
    } 
	if((ptrs_filled != tm_pool.ptrs_filled) || (ptrs_freed != tm_pool.ptrs_freed))
	{
        //tm_debug("2nd test: filled and/or freed ptrs don't match count: filled_p=%u, freed_p=%u", ptrs_filled, ptrs_freed);
        pool_print();
        return 0;
    }

    if(!freed_isvalid())
	{
		//tm_debug("[ERROR] general freed check failed"); 
		return false;
	}

    if(testing)
	{
        for(index=1; index<TM_MAX_POOL_PTRS; index++)								// if testing assume that all filled indexes should have correct "filled" data
		{
            if(FILLED(index))
			{
                if(!check_index(index))
				{
                    //tm_debug("[ERROR] index %u failed check", index);
                    index_print(index);
                    return 0;
                }
            }
        }
    }
    return true;
}


// ------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
void fill_index(tm_index_t index)
{
    uint32_t value = index * PRIME;
    uint32_t *data = tm_uint32_p(index);
    // index_print(index);
    assert(data);
    tm_blocks_t i;

    if(FILLED(index)) data[0] = value;
    for(i=1; i<BLOCKS(index); i++)
	{
        data[i] = value;
    }
    assert(check_index(index));
}


// ------------------------------------------------------------------------------------------------
//
// ------------------------------------------------------------------------------------------------------
bool check_index(tm_index_t index)
{
    uint32_t value = index * PRIME;
    uint32_t *data = tm_uint32_p(index);
    tm_blocks_t i;
    if(!POINTS(index)) return false;
    if(!data) return false;

    if(FILLED(index))
	{
        if(data[0] != value) return false;
    }
    for(i=1; i<BLOCKS(index); i++)
	{
        if(data[i] != value) return false;
    }
    return true;
}

#endif		//CONF_USE_MEM_MANAGER
