// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: Driver_Manager.c
// 	   Version: 3.40
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Driver manager subsystem.
// ******************************************************************************
// Info: 
//
// Notice:
//
// Usage:
//			
// ToDo:	
//
// Changelog:
//				2023.12.06	- v3.40	- upgrade to minamilistic version (without drvmgr and modmgr)
//									- add read/write for data and buffer
//				2022.12.09	- v3.31 - add optimalization (if-else) switches
//				2022.11.28	- v3.3 - add Extended function in driver manager (driver list and some chcecks) - CONF_USE_DRV_MANAGER_EXT
//				2020.06.24	- pAPI_USR deleted!
// 				2019.11.11	- Read/write - pbuff changet to sys_Buffer_t

#include "Skeleton.h"

#if defined(CONF_USE_MODDRV_MANAGER) && (CONF_USE_MODDRV_MANAGER == 1)


// ************************************************************************************************
// PRIVATE
// ************************************************************************************************
#if defined(CONF_USE_DRV_MANAGER_EXT) && (CONF_USE_DRV_MANAGER_EXT == 1)
_ColList_t 				drvmgr_List		__SECTION_ZEROINIT;							// create global driver list
#endif



// ************************************************************************************************
// Functions prototypes
// ************************************************************************************************
bool drvmgr_Load (_mod_If_t *pParentModIf, _drv_If_t *pDrvIf);
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
bool drvmgr_Check_SetFlag(_mod_If_t *pParentModIf, _drv_If_t *pDrvIf, struct _If_Status CheckedFlag);
#endif



// ------------------------------------------------------------------------------------------------------
// After handler
// fire event system
// ------------------------------------------------------------------------------------------------------
void IRQ_Handler_Bottom(_drv_If_t *pDrvIf)
{
	if(pDrvIf->pIRQ_USR_HandlerBottom) pDrvIf->pIRQ_USR_HandlerBottom(pDrvIf);			// override with USR handler
	else
	{
		// Generic interrupt routine:
		if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_StateChanged);	// ak je nastaveny event system, zavolaj ho
	}
}



// ------------------------------------------------------------------------------------------------
// Check driver for selected flags
// These flags mus be active also in Parent Module otherwise will not check!
// In case Flags in driver is not set, will called set routine (Load, Init, Enable) and check again (max 3x)
// If all flags from CheckedFlags are set, return is true, otherwise false.
// ------------------------------------------------------------------------------------------------
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
static bool drvmgr_Check_SetFlag(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, struct _If_Status CheckedFlag)
{
	bool res = true;
	_If_Status_t OldStat = pDrvIf->Sys.Stat;										// save old status
	if(pRequestorModIf == NULL) return(true);										// kontrola skipnuta
	
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> Loaded:%d, Initialized:%d, Enabled:%d", CheckedFlag.Loaded, CheckedFlag.Initialized, CheckedFlag.Enabled);
#endif	
	
	// If we need Check driver for flag LOADED
	// also parent module must have this flag set!
	if((CheckedFlag.Loaded == true) && (pRequestorModIf->Sys.Stat.Loaded == true)) 
	{
		uint16_t trycount = 3;														// How many times you try to call Load if need
		do
		{
			if(pDrvIf->Sys.Stat.Loaded == false) 									// ok, driver has not set this flag, call load it
			{
				res = drvmgr_Load (pRequestorModIf, pDrvIf);						// load driver
			}
			if(pDrvIf->Sys.Stat.Loaded == true) break; 								// ok, Driver was Loaded succesfully
		}while(--trycount);															// try again

		if(pDrvIf->Sys.Stat.Loaded == false) 										// Driver is still not Loaded ! 
		{
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
			dbgprint("\r\n("DRV_MANAGER_OBJECTNAME") Check Driver's Flags[%s]: Driver failed to Load!", pDrvIf->Name );
#endif		
			res = false;
			goto ExitCheck;															// exit
		}
		
	}
	
	// If we need Check driver for flag INITIALIZED or we need initialize after load before
	// also parent module must have this flag set!
	if(((CheckedFlag.Initialized == true) && (pRequestorModIf->Sys.Stat.Initialized == true)) || (OldStat.Initialized == true && pDrvIf->Sys.Stat.Initialized == false))
	{
		uint16_t trycount = 3;														// How many times you try to call Initialize if need
		do
		{
			if(pDrvIf->Sys.Stat.Initialized == false) 								// ok, driver has not set this flag, call load it
			{
				res = drvmgr_Init (pRequestorModIf, pDrvIf);						// Initialize driver /inside is check for STD or USR/
			}
			if(pDrvIf->Sys.Stat.Initialized == true) { break;}						// ok, Driver is Initialized
		}while(--trycount);															// Try again

		if(pDrvIf->Sys.Stat.Initialized == false) 									// Driver is still not Initialized ! 
		{
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
			dbgprint("\r\n("DRV_MANAGER_OBJECTNAME") Check Driver's Flags[%s]: Driver failed to Initialize!", pDrvIf->Name );
#endif		
			res = false;
			goto ExitCheck;															// exit
		}
	}
	
	// If we need Check driver for flag ENABLED or if driver has been enabled but we apply load and/or initialize, we need set Enable back.
	// also parent module must have this flag set!
	if(((CheckedFlag.Enabled == true) && (pRequestorModIf->Sys.Stat.Enabled == true)) || ((OldStat.Enabled == true) && pDrvIf->Sys.Stat.Enabled == false))
	{
		uint16_t trycount = 3;														// How many times you try to call Enable if need
		do
		{
			if(pDrvIf->Sys.Stat.Enabled == false) 									// ok, driver has not set this flag, call enable it
			{
				res = drvmgr_Enable (pRequestorModIf, pDrvIf, UINT32_MAX);			// Enabled driver - all channel
			}
			
			if(pDrvIf->Sys.Stat.Enabled == true) break;								// ok, Driver is Enabled
		}while(--trycount);															// try again

		if(pDrvIf->Sys.Stat.Enabled == false) 										// Driver is still not Enabled ! 
		{
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
			dbgprint("\r\n("DRV_MANAGER_OBJECTNAME") Check Driver's Flags[%s]: Driver failed to Enable!", pDrvIf->Name );
#endif		
			res = false;
			goto ExitCheck;															// exit

		}
	}
	
ExitCheck:	
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
#endif	
	return(res);
}
#endif 	// (CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)


// ------------------------------------------------------------------------------------------------
// Try to find ParentMod connection in pDrvIf. If present, result is True.
// ------------------------------------------------------------------------------------------------
bool drvmgr_CheckParentMod(_mod_If_t *pParentModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> ParentMod: %s", pParentModIf->Name);
#endif	

#if (CONF_SYS_COLLECTIONS_USED == 1)	
	if(sys_DynCol_Find_Item(&pDrvIf->Sys.ParentModLinked, pParentModIf) == NULL) res = false;
		else res = true;
#endif
	
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code	
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------
// Manager Routine
// Load driver into list
//	Input: pointer to driver structure. MUST be filled with minimal information..
//  Result: True if driver has been loaded, otherwise false
// ------------------------------------------------------------------------------------------------
bool drvmgr_Load (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;

#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
#endif	

#if (CONF_SYS_COLLECTIONS_USED == 1)	
#if defined(CONF_USE_DRV_MANAGER_EXT) && (CONF_USE_DRV_MANAGER_EXT == 1)
	res = sys_DynCol_Add_Item (&drvmgr_List, pDrvIf);								// add driver to global driver list
	if(res == true) sys_DynCol_Add_Item (&pDrvIf->Sys.ParentModLinked, pRequestorModIf);	// Create link to Parent Mod
	else sys_DynCol_Remove_Item (&drvmgr_List, pDrvIf);								// rollback 1
	if(res == true) sys_DynCol_Add_Item (&pRequestorModIf->Sys.DrvLinked, pDrvIf);	// Create link from Mod to drv
	else 
	{
		sys_DynCol_Remove_Item (&drvmgr_List, pDrvIf);								// rollback 2
		sys_DynCol_Remove_Item (&pDrvIf->Sys.ParentModLinked, pRequestorModIf);		// rollback 2
	}
#else
	res = sys_DynCol_Add_Item (&pDrvIf->Sys.ParentModLinked, pRequestorModIf);		// Create link to Parent Mod
	if(res) res = sys_DynCol_Add_Item (&pRequestorModIf->Sys.DrvLinked, pDrvIf);	// Create link from Mod to drv
#endif
#endif		// if (CONF_SYS_COLLECTIONS_USED == 1)	

	if(res == true)
	{
		pDrvIf->Sys.Stat.Loaded = true;												// and set flags
		pDrvIf->Sys.Stat.Initialized = false;
		pDrvIf->Sys.Stat.Enabled = false;
	}
	
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------
// Enable driver
// ------------------------------------------------------------------------------------------------
bool drvmgr_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;

#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);
#endif	
	


#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	if(drvmgr_Check_SetFlag(pRequestorModIf, pDrvIf, (struct _If_Status) {.Loaded = true, .Initialized = true, .Enabled = false}) == true)
#endif	
	{
		if((pDrvIf->pAPI_USR->Enable != NULL) && (pDrvIf->pAPI_USR != NULL)) 		// Is USR API function preset?
		{
			res = pDrvIf->pAPI_USR->Enable(pRequestorModIf, pDrvIf, ChannelBitMask);// Call a USR function
		}
		else
		{																			// no User function -> use default fnc.
			if((pDrvIf->pAPI_STD->Enable != NULL) && (pDrvIf->pAPI_STD != NULL))	// Is STD API function preset?
			{
				res = pDrvIf->pAPI_STD->Enable(pRequestorModIf, pDrvIf, ChannelBitMask);	// Call a STD function
			}
			else
			{
				// Generic enable code here:
				// -- nothing to enable
				// .
				// .
				pDrvIf->Sys.Stat.Enabled = true;									// nastav na pozadovany stav
				res = true;
			}
		}
	}
	
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
#endif	
	return(res);	
}


// ------------------------------------------------------------------------------------------------
// Disable driver
// ------------------------------------------------------------------------------------------------
bool drvmgr_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = true;
	

#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);
#endif	
	
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	if(drvmgr_Check_SetFlag(pRequestorModIf, pDrvIf, (struct _If_Status) {.Loaded = true, .Initialized = true, .Enabled = false}) == true)
#endif	
	{
		if((pDrvIf->pAPI_USR->Disable != NULL) && (pDrvIf->pAPI_USR != NULL)) 		// Is USR API function preset?
		{
			res = pDrvIf->pAPI_USR->Disable(pRequestorModIf, pDrvIf, ChannelBitMask);// Call a USR function
		}
		else
		{																			// no User function -> use default fnc.
			if((pDrvIf->pAPI_STD->Disable != NULL) && (pDrvIf->pAPI_STD != NULL))	// Is STD API function preset?
			{
				res = pDrvIf->pAPI_STD->Disable(pRequestorModIf, pDrvIf, ChannelBitMask);	// Call a STD function
			}
			else
			{
				// Generic disable code here:
				// -- nothing to disable
				// .
				// .
				pDrvIf->Sys.Stat.Enabled = false;									// Set flag
				res = true;
			}
		}
	}
	
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
#endif	
	return(res);	
}

// ------------------------------------------------------------------------------------------------
// Init driver
// ------------------------------------------------------------------------------------------------
bool drvmgr_Init(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
#endif	

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	if(drvmgr_Check_SetFlag(pRequestorModIf, pDrvIf, (struct _If_Status) {.Loaded = true, .Initialized = false, .Enabled = false}) == true)
#endif	
	{
		if((pDrvIf->pAPI_USR->Init != NULL) && (pDrvIf->pAPI_USR != NULL)) 			// Is USR API function preset?
		{
			res = pDrvIf->pAPI_USR->Init(pRequestorModIf, pDrvIf);					// Call a USR function
		}
		else
		{																			// No User function -> use default fnc.
			if((pDrvIf->pAPI_STD->Init != NULL) && (pDrvIf->pAPI_STD != NULL))		// Is STD API function preset?
			{
				res = pDrvIf->pAPI_STD->Init(pRequestorModIf, pDrvIf);				// Call a STD function
			}
			else
			{
				// Generic initialization code here:
				// .
				// .
				// .
				pDrvIf->Sys.Stat.Initialized = false;								// Set flag
				res = true;
			}
		}
	}
		
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------
// Release driver from parent module. 
// ------------------------------------------------------------------------------------------------
bool drvmgr_Release(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;

#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
#endif	

	bool unload = true;																// flag for completelly unload 
	// now get each pParentMod for this driver for chcek for complettely unload from system

#if (CONF_SYS_COLLECTIONS_USED == 1)	
	_ColListRecord_t *tmpListRecord = NULL;
	do
	{
		tmpListRecord = sys_DynCol_Get_Record(&pDrvIf->Sys.ParentModLinked, tmpListRecord);	// temporary pointer to parent module list and get next to tmpListRecord
		if (tmpListRecord == NULL) break;

		if(tmpListRecord->pValue != NULL)											// pParent is NULL?
		{
			// and check if pParent Mod is disabled too
			if(((_mod_If_t*)(tmpListRecord->pValue))->Sys.Stat.Loaded == true) {unload = false; break;}// we cannot unload driver. this module are used it and is Loaded
		}
	}while(tmpListRecord);
#endif		// if (CONF_SYS_COLLECTIONS_USED == 1)	
		
	if(unload == true) 																// Release confirmed?
	{
		if((pDrvIf->pAPI_USR->Release != NULL) && (pDrvIf->pAPI_USR != NULL)) 		// Is USR API function preset?
		{
			res = pDrvIf->pAPI_USR->Release(pRequestorModIf, pDrvIf);				// Call a USR function
		}
		else
		{																			// No User function -> use default fnc.
			if((pDrvIf->pAPI_STD->Release != NULL) && (pDrvIf->pAPI_STD != NULL))	// Is STD API function preset?
			{
				res = pDrvIf->pAPI_STD->Release(pRequestorModIf, pDrvIf);			// Is STD API function preset?
			}
			else
			{
				// Generic release code here:
				// -- nothing to release
				// .
				// .
				// .
				pDrvIf->Sys.Stat.Enabled = false;
				pDrvIf->Sys.Stat.Initialized = false;
				pDrvIf->Sys.Stat.Loaded = false;
				res = true;
			}
		}
		
		if(res == true)																// release was sucess?
		{
#if (CONF_SYS_COLLECTIONS_USED == 1)
#if defined(CONF_USE_DRV_MANAGER_EXT) && (CONF_USE_DRV_MANAGER_EXT == 1)
			sys_DynCol_Remove_Item(&drvmgr_List, pDrvIf);								// remove driver from global drivers list
#endif			
			sys_DynCol_Remove_Item(&pDrvIf->Sys.ParentModLinked, pRequestorModIf);		// remove link to parent module from list
			sys_DynCol_Remove_Item(&pRequestorModIf->Sys.DrvLinked,pDrvIf);				// remove driver from module LoadedDriver List
#endif		// if (CONF_SYS_COLLECTIONS_USED == 1)						
		}

	}
	
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------
// Write data. Driver must be loaded, initialized and enabled. othwrwise will csll load, init or enable function
// pDstDrv - generic pointer to dst driver.
// Src - ptr to source data buffer
// buflen - number of chars for transfer
// XferType - Block/IRQ/DMA
// ------------------------------------------------------------------------------------------------
bool drvmgr_Write_Data (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, unsigned char *Src, uint32_t WrLen, _XFerType_t XferType)
{
	bool res = false;
	Write_Data_Handler_t Write_Fn;													// speed up - create shortcut

#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pSrc: %p, BuffByteLen: %d, XFerType: 0x%.1X", pbuff, WrLen, XferType);
#endif	
	

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)	
	if(pRequestorModIf != NULL)
		if(drvmgr_Check_SetFlag(pRequestorModIf, pDrvIf, (struct _If_Status) {.Loaded = true, .Initialized = true, .Enabled = true}) == false) goto ExitWrite;
	//if(pbuff == NULL) goto ExitWrite;
	//if(pDrvIf == NULL) goto ExitWrite;
#endif
	
	Write_Fn = pDrvIf->pAPI_USR->Write_Data;										// use USR fn
		
	if(( Write_Fn != NULL) && (pDrvIf->pAPI_USR != NULL)) 							// Is USR API function preset?
	{
		res = Write_Fn(pRequestorModIf, pDrvIf, Src, WrLen, XferType);				// Call a USR function
	}
	else
	{																				// No User function -> use default fnc.
		Write_Fn = pDrvIf->pAPI_STD->Write_Data;									// use STD fn
		
		if((Write_Fn != NULL) && (pDrvIf->pAPI_STD != NULL))						// Is STD API function preset?
		{
			res = Write_Fn(pRequestorModIf, pDrvIf, Src, WrLen, XferType);			// Call a STD function
		}
		else
		{
			// Generic write data code here:
			// -- nothing to write
			// .
			// .
			// .
			res = true;
			goto ExitWrite;
		}
	}

ExitWrite:
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
    FN_DEBUG_EXIT(pDrvIf->Name);													// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif
	
	return(res);
}

// ------------------------------------------------------------------------------------------------
// Write Buff. Driver must be loaded, initialized and enabled. othwrwise will csll load, init or enable function
// pDstDrv - generic pointer to dst driver.
// pbuff - value/buffer of data. If pBuff is NULL, will be used buffer of appropriate device
// buflen - number of chars for transfer
// XferType - Block/IRQ/DMA
// ------------------------------------------------------------------------------------------------
bool drvmgr_Write_Buff (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType)
{
	bool res = false;
	Write_Data_Handler_t Write_Fn;													// speed up - create shortcut

#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pSrc: %p, BuffByteLen: %d, XFerType: 0x%.1X", pbuff, WrLen, XferType);
#endif	
	

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)	
	if(pRequestorModIf != NULL)
		if(drvmgr_Check_SetFlag(pRequestorModIf, pDrvIf, (struct _If_Status) {.Loaded = true, .Initialized = true, .Enabled = true}) == false) goto ExitWrite;
	//if(pbuff == NULL) goto ExitWrite;
	//if(pDrvIf == NULL) goto ExitWrite;
#endif
	
	Write_Fn = pDrvIf->pAPI_USR->Write_Data;										// use USR fn
		
	if(( Write_Fn != NULL) && (pDrvIf->pAPI_USR != NULL)) 							// Is USR API function preset?
	{
		res = Write_Fn(pRequestorModIf, pDrvIf, pbuff, WrLen, XferType);			// Call a USR function
	}
	else
	{																				// No User function -> use default fnc.
		Write_Fn = pDrvIf->pAPI_STD->Write_Data;									// use STD fn
		
		if((Write_Fn != NULL) && (pDrvIf->pAPI_STD != NULL))						// Is STD API function preset?
		{
			res = Write_Fn(pRequestorModIf, pDrvIf, pbuff, WrLen, XferType);		// Call a STD function
		}
		else
		{
			// Generic write data code here:
			// -- nothing to write
			// .
			// .
			// .
			res = true;
			goto ExitWrite;
		}
	}

ExitWrite:
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
    FN_DEBUG_EXIT(pDrvIf->Name);													// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif
	
	return(res);
}

// ------------------------------------------------------------------------------------------------
// Read Data. Driver must be loaded, initialized and enabled. othwrwise will csll load, init or enable function
// pDstDrv - generic pointer of src driver.
// Dst - ptr to Destination buffer
// maxlen - maximum number of chars for transfer
// ------------------------------------------------------------------------------------------------
uint32_t drvmgr_Read_Data(_mod_If_t *pRequestorModIf, _drv_If_t* pDrvIf, unsigned char *Dst, uint32_t RdLen, _XFerType_t XferType)
{
	uint32_t Result = 0;
	Read_Data_Handler_t Read_Fn;													// speed up - create shortcut

#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDst: %p, MaxLen: %d, XFerType: 0x%.1X", pbuff, RdLen, XferType);
#endif
		

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)	
	if(pRequestorModIf != NULL)
		if(drvmgr_Check_SetFlag(pRequestorModIf, pDrvIf, (struct _If_Status) {.Loaded = true, .Initialized = true, .Enabled = true}) == false) goto ExitRead;
	//if(pbuff == NULL) goto ExitRead;
	if(pDrvIf == NULL) goto ExitRead;
#endif
	
	Read_Fn = pDrvIf->pAPI_USR->Read_Data;											// use USR fn
	
	if((Read_Fn != NULL) && (pDrvIf->pAPI_USR != NULL)) 							// Is USR API function preset?
	{
		Result = Read_Fn(pRequestorModIf, pDrvIf, Dst, RdLen, XferType);			// Call a USR function
	}
	else
	{																				// No User function -> use default fnc.
		Read_Fn = pDrvIf->pAPI_STD->Read_Data;										// use STD fn
		
		if((Read_Fn != NULL) && (pDrvIf->pAPI_STD != NULL))							// Is STD API function preset?
		{
			Result = Read_Fn(pRequestorModIf, pDrvIf, Dst, RdLen, XferType);		// Call a STD function
		}
		else
		{
			// Generic read data code here:
			// -- nothing to read
			// .
			// .
			// .
			Result = 0;
			goto ExitRead;
		}
	}

	
ExitRead:	
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("Xfer: %d", Result);
#endif
	
	return (Result);
}

// ------------------------------------------------------------------------------------------------
// Read Buff. Driver must be loaded, initialized and enabled. othwrwise will csll load, init or enable function
// pDstDrv - generic pointer of src driver.
// pbuff - value/buffer of dst data. If pBuff is NULL, will be used buffer of appropriate device
// maxlen - maximum number of chars for transfer
// ------------------------------------------------------------------------------------------------
uint32_t drvmgr_Read_Buff(_mod_If_t *pRequestorModIf, _drv_If_t* pDrvIf, sys_Buffer_t *pbuff, uint32_t RdLen, _XFerType_t XferType)
{
	uint32_t Result = 0;
	Read_Data_Handler_t Read_Fn;													// speed up - create shortcut

#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDst: %p, MaxLen: %d, XFerType: 0x%.1X", pbuff, RdLen, XferType);
#endif
		

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)	
	if(pRequestorModIf != NULL)
		if(drvmgr_Check_SetFlag(pRequestorModIf, pDrvIf, (struct _If_Status) {.Loaded = true, .Initialized = true, .Enabled = true}) == false) goto ExitRead;
	//if(pbuff == NULL) goto ExitRead;
	if(pDrvIf == NULL) goto ExitRead;
#endif
	
	Read_Fn = pDrvIf->pAPI_USR->Read_Data;											// use USR fn
	
	if((Read_Fn != NULL) && (pDrvIf->pAPI_USR != NULL)) 							// Is USR API function preset?
	{
		Result = Read_Fn(pRequestorModIf, pDrvIf, pbuff, RdLen, XferType);			// Call a USR function
	}
	else
	{																				// No User function -> use default fnc.
		Read_Fn = pDrvIf->pAPI_STD->Read_Data;										// use STD fn
		
		if((Read_Fn != NULL) && (pDrvIf->pAPI_STD != NULL))							// Is STD API function preset?
		{
			Result = Read_Fn(pRequestorModIf, pDrvIf, pbuff, RdLen, XferType);		// Call a STD function
		}
		else
		{
			// Generic read data code here:
			// -- nothing to read
			// .
			// .
			// .
			Result = 0;
			goto ExitRead;
		}
	}

	
ExitRead:	
#if defined(CONF_DEBUG_DRV_MANAGER) && (CONF_DEBUG_DRV_MANAGER == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("Xfer: %d", Result);
#endif
	
	return (Result);
}

#endif // defined(CONF_USE_MODDRV_MANAGER) && (CONF_USE_MODDRV_MANAGER == 1)
