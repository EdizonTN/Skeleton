// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: Driver_Manager.h
// 	   Version: 3.40
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Driver manager subsystem.
// ******************************************************************************
// Info: 
//
// Notice:
//
// Usage:
//			
// ToDo:
//
// Changelog:	
//				2023.12.06	- v3.40	- upgrade to minamilistic version (without drvmgr and modmgr)
//									- add read/write for data and buffer
//				2022.12.09	- v3.21 - add optimalization (if-else) switches
//				2020.06.25	- *pAPI_USR zrusene!
// 				2019.11.11	- 11.11.2019 - Read/write - pbuff changet to sys_Buffer_t


#ifndef __DRIVER_MANAGER_H_
#define __DRIVER_MANAGER_H_

#include "Skeleton.h"


// ******************************************************************************************************
// CONFIGURATION	
// ******************************************************************************************************
#ifndef DEBUG_DRIVER_MANAGER
#define DEBUG_DRIVER_MANAGER			0											// disable debug by default
#endif

#ifndef CONF_USE_DRV_MANAGER_EXT
#define CONF_USE_DRV_MANAGER_EXT		0											// extended functions and driver listing
#endif


// ******************************************************************************************************
// Checks
// ******************************************************************************************************
#if (CONF_SYS_COLLECTIONS_USED == 0) && (CONF_USE_DRV_MANAGER_EXT == 1) 
 #error "Dynamic collections needs for DRV_MANAGER_EXT system. Use CONF_SYS_COLLECTIONS_USED=1"
#endif

// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************




#if defined(CONF_USE_MODDRV_MANAGER) && (CONF_USE_MODDRV_MANAGER == 1)
#include "Module_Manager.h"

// ******************************************************************************************************
// PUBLIC 
// ******************************************************************************************************
extern bool drvmgr_CheckParentMod(_mod_If_t *pParentModIf, _drv_If_t *pDrvIf);		// test if pParentMod is in driver list
#if defined(CONF_USE_DRV_MANAGER_EXT) && (CONF_USE_DRV_MANAGER_EXT == 1)
extern bool drvmgr_Check_SetFlag(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, struct _If_Status CheckedFlag); //check selected flags and call routine if not set
#endif
extern bool drvmgr_Load (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);			// Load driver to system
extern bool drvmgr_Release(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);			// UnInit - uvolnenie modulu a pripojenych driverov
extern bool drvmgr_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);			// init 
extern bool drvmgr_Restart (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, bool Quiet);							// Restart
extern bool drvmgr_Enable (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);				// Enable modulu
extern bool drvmgr_Disable (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);			// Disable modulu
extern bool drvmgr_Write_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, unsigned char *Src, uint32_t WrLen, _XFerType_t XferType); // Write data
extern bool drvmgr_Write_Buff(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType); // Write data system buffer
extern uint32_t drvmgr_Read_Data (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, unsigned char *Dst, uint32_t RdLen, _XFerType_t XferType); // Read data
extern uint32_t drvmgr_Read_Buff (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t RdLen, _XFerType_t XferType); // Read data system buffer

extern void IRQ_Handler_Bottom(_drv_If_t *pDrvIf);

extern _ColList_t 				drvmgr_List;										// create global driver list




// ******************************************************************************************************
// New proposal	v.2
// ******************************************************************************************************

typedef enum 
{	// can be selected only one
	UnLoad 								= 0x0000,
	Load								= 0x0001,
	Init								= 0x0002,
	Restart								= 0x0003,
	Disable								= 0x0004,
	Enable								= 0x0005,
} ActionType_t;

typedef enum 
{	// can be selected more than one
	Unknown 							= 0x0000,
	Loaded								= 1UL << 1,
	Initialized 						= 1UL << 2,
	Enabled								= 1UL << 3,
	WaitForIRQ							= 1UL << 4,
} StatusType_t;

typedef enum
{	// can be selected more than one
	NoChange							= 0x0000,									// without change
	RxDone								= 1UL << 1,									// new data available
	TxReady								= 1UL << 2,									// send data ready
	TxDone								= 1UL << 3,									// send data done
	NewState							= 1UL << 4,									// Device was changed our state
} EventType_t;

struct	drv_Str_t;

// general API functions for ALL drivers
typedef bool 	(*pAction_Fn_t)	 		( struct _mod_If *pRequestorModIf, struct drv_Str_t *pDrv, ActionType_t Action, uint32_t ChMask);
typedef	void 	(*pEvent_Hdlr_t) 		( struct drv_Str_t *pDrv, EventType_t Event);// event handler function define type

// system structures - valid for ALL drivers
typedef struct
{
	const pAction_Fn_t 					Action;										// Action function. Arguments describe action and additional parameters
		  StatusType_t					Stat;										// driver's status by type
		  pEvent_Hdlr_t					Event;										// Event handler
} drv_Sys_t;


// Driver structure - valid for ALL drivers
COMP_PACKED_BEGIN
typedef struct
{
#if defined(CONF_DEBUG_STRING_NAME_SIZE) && (CONF_DEBUG_STRING_NAME_SIZE > 0)		// if text naming are enabled
	const char							Name[CONF_DEBUG_STRING_NAME_SIZE];			// interface name - text - only for ID	
#endif	
	const void							**pParentIFace;								// Parent comm. interface/s for this driver (I2C for ext Chip, SPI for display,...)
	const drv_Sys_t						*pSys;										// pointer na systemovu strukturu - rovnaka pre vsetky drvs
	const struct																	// Specific drivers structure - content are hidden from system standart
	{
		const void						*pAPIs;										// Defined in particular driver. Ptr to specific API structure (Write, Scan, ... )
		const void						*pDefs;										// Defined in particular driver. Ptr to structure of constant definitions (IRQ num, ptr to peri, ...)
		const void						*pParms;									// Defined in particular driver. Ptr to parameters structure (XFer struct, speed, ...)
		const void						*pStats;									// Defined in particular driver. ptr to driver's specific status and results (Peri stats, XFer receive bytes,...)
			  void						*pEvents;									// Defined in particular driver. Ptr to driver's specific Events handlers (Scan complette,...)
	}SpecStr_t;
}drv_Str_t;
COMP_PACKED_END







#endif	//	#if defined(CONF_USE_MODDRV_MANAGER) && (CONF_USE_MODDRV_MANAGER == 1)
#endif // __DRIVER_MANAGER_H_
