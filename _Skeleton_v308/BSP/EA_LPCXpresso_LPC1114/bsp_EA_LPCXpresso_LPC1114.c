// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: bsp_EA_LPCXPRESSO_LPC1114.c
// 	   Version: 1.00
//  Created on: 01.05.2023
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Board support package file - hardware specific declaration and functions
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#include "Skeleton.h"

#ifdef __BSP_EA_LPCXPRESSO_LPC1114_H_

extern const CHAL_Signal_t 	sig_first;

// ------------------------------------------------------------------------------------------------
void bsp_Chip_Init(void) ATTR_ALWAYS_INLINE;
void bsp_Chip_Set_SystemClocking(void)	ATTR_ALWAYS_INLINE;

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// These functions are called from reset vector !!!!
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// MCU Chip init based on board HW
// If you need special settings in SystemInit, write your own code here.
// will be insert into SystemInit
// Ex. SWM remap
void bsp_Chip_Init(void)
{
	// write MCU Init according to board here:
	NVIC_SetPriority (MemoryManagement_IRQn, 0x0F);									// http://www.keil.com/appnotes/files/apnt209.pdf
	NVIC_SetPriority (BusFault_IRQn, 0x08);											
	NVIC_SetPriority (UsageFault_IRQn, 0x01);	
}

// ------------------------------------------------------------------------------------------------
// Set up and initialize clocking prior to call to main
void bsp_Chip_Set_SystemClocking(void)
{
	uint16_t 	i;
	uint32_t 	cpu_clk;
	
	// Basic MCU Clock settings
	// Run from external 12MHz Quartz by PLL0 with P and M settings
	LPC_SYSCON->PDRUNCFG &= !(1 << 21);												// Power Up
	
	for (i = 0; i < 0x500; i++) {}													// Wait 500us for OSC to be stablized, no status indication, dummy wait.
	
	LPC_SYSCON->SYSPLLCLKSEL = 0x01;												// Switch to Main External Crystal Oscilator
		
	LPC_SYSCON->SYSPLLCTRL = (BSP_FREQ_PLL_M_VAL & 0x3F) | ((BSP_FREQ_PLL_P_VAL & 0x3) << 6);// Setup  PLL to 5: 	CoreFreq = Main Crystal * PLL+1
		
	LPC_SYSCON->PDRUNCFG &= !(1 << 22);												// Power up system PLL		
		
	while ((LPC_SYSCON->SYSPLLSTAT & 1)) {}											// Wait for PLL FOR lock
	
	LPC_SYSCON->SYSAHBCLKDIV  = BSP_SYSAHBCLKDIV;									// Set system clock divider
		
	uint32_t tmp = LPC_SYSCON->FLASHCFG & (~(0x3 << 12));							// Setup FLASH access timing for 72MHz
	LPC_SYSCON->FLASHCFG = tmp | ((0x02) << 12);									// dont change other bits !

	LPC_SYSCON->CLKOUTSELB = 0x02;													// Main Clock from PLL Out
	
		
	cpu_clk = _Chip_Clock_Get_CoreClk_Rate();										// re-read clock freq.
		
	LPC_SYSCON->SYSTICKCLKDIV = 1;													// SystemTick timer divided by 1 to Main Clk
	CHAL_Chip_SWO_Init(cpu_clk);													// Change SWO clock
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// These functions are called as first from main()
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// IO Initialization according to Board
void bsp_Set_SystemPinDefault(void)
{
	// set some critical IO pins to default state here:

	uint32_t i = 0;

	do
	{
		CHAL_Signal_Init ( &Sig_Array[i] );
		i ++;
	}while ((Sig_Array[i].Std.Pin != 0xff) | (Sig_Array[i].Std.Port != 0xff));		// repeat until __last structure


}


// ------------------------------------------------------------------------------------------------
// Prepare Pins and core for LowLevel Debug if enabled
void bsp_Debug_Low(void)
{
	// SerialWire Debug - if enabled, activate appropriate pins
#if defined(BSP_DEBUG_SWD) && (BSP_DEBUG_SWD == 1)
	LPC_SYSCON->SYSAHBCLKCTRL0 |= 1<<12;											// Enable CLK for SWM - bit 12
	LPC_SWM->PINENABLE1 &= ~(1<<22);												// Enable SWCLK and Enable GPIO 0.19
	LPC_SWM->PINENABLE1 &= ~(1<<23);												// Enable SWDIO and Enable GPIO 0.20
	LPC_SYSCON->SYSAHBCLKCTRL0 &= ~(1<<12);											// Disable CLK for SWM - bit 12
	
#if defined(BSP_DEBUG_SWO) && (BSP_DEBUG_SWO == 1)									// Serial Wire Output
	LPC_SYSCON->SYSAHBCLKCTRL0 |= 1<<12;											// Enable CLK for SWM - bit 12
	
	//BSP_sig_SWDCLK			sig_SWCLK_PIO0_19
	//BSP_sig_SWDIO			sig_IF_SWDIO
	
	//LPC_SWM->PINENABLE0 |= 0x00000001;												// disable analog function on pin PIO0.8
	LPC_SWM->PINASSIGN15 = (LPC_SWM->PINASSIGN15 & 0xffff00ff) | ( ((BSP_sig_SWO.Std.Port * 32) + BSP_sig_SWO.Std.Pin) << 8);			// SWO nastav na konkretny pin
	LPC_SYSCON->SYSAHBCLKCTRL0 &= ~(1<<12);											// vypni CLK pre SWM - bit 12
#endif	
#endif	
}




// ------------------------------------------------------------------------------------------------
// Init CPU and connected hardware
void bsp_Init(void)
{
	sys_System.Part->Clock_Refresh();
	SysTick_Config(sys_System.Part->CPU_Freq / CONF_SYSTICK_FREQ);					// Enable systick timer
}



const CHAL_Signal_t 	Sig_Array[] ATTR_USED  __SECTION_RODATA = 
{
	_SP0_0_FILL,
	_SP0_1_FILL,
	_SP0_2_FILL,
	_SP0_3_FILL,
	_SP0_4_FILL,
	_SP0_5_FILL,
	_SP0_6_FILL,
	_SP0_7_FILL,
	_SP0_8_FILL,
	_SP0_9_FILL,
	_SP0_10_FILL,
	_SP0_11_FILL,
	_SP1_0_FILL,
	_SP1_1_FILL,
	_SP1_2_FILL,
	_SP1_3_FILL,
	_SP1_4_FILL,
	_SP1_5_FILL,
	_SP1_6_FILL,
	_SP1_7_FILL,
	_SP1_8_FILL,
	_SP1_9_FILL,
	_SP1_10_FILL,
	_SP1_11_FILL,
	_SP2_0_FILL,
	_SP2_1_FILL,
	_SP2_2_FILL,
	_SP2_3_FILL,
	_SP2_4_FILL,
	_SP2_5_FILL,
	_SP2_6_FILL,
	_SP2_7_FILL,
	_SP2_8_FILL,
	_SP2_9_FILL,
	_SP2_10_FILL,
	_SP2_11_FILL,
	_SP3_0_FILL,
	_SP3_1_FILL,
	_SP3_2_FILL,
	_SP3_3_FILL,
	_SP3_4_FILL,
	_SP3_5_FILL,
	{{ 255, 0, 0}},										// virtual
	{{ 255, 255, 0}},									// last	
};



#endif      // __BSP_EA_LPCXPRESSO_LPC1114_H_
