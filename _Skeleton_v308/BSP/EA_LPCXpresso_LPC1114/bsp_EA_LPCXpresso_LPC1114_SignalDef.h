// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: bsp_EA_LPCXPRESSO_LPC1114_SignalDef.h
// 	   Version: 1.00
//  Created on: 01.05.2023
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Pin Signal definition file for EA_LPCXPRESSO_LPC1114 board
// ******************************************************************************
// Usage:
//
// ToDo:
//
// Changelog:
//

#ifndef __BSP_EA_LPCXPRESSO_LPC1114_SIGNALDEF_H_
#define __BSP_EA_LPCXPRESSO_LPC1114_SIGNALDEF_H_

#include <Chip\Chip_HAL.h>


// OM13056 uses LPC1549JBD64
//   			64-pin LQFP package

// ------------------------------------------------------------------------------------------------
// IO Pin Signal human readable declaration. 
//	Will be good, if signals will have it same name as is in schematic - for quick knowing
// Or assign some logical names. Ex Button1, I2C_SDA, LCD_PCLK, or similar
// ------------------------------------------------------------------------------------------------

//	----------------------------------- Description of the naming and defining:
//		Readable name:						(_CHIP_GPIO_PINPERPORT * Port.Number) + Pin.Number			// Comment

#define	sig_PIO0_0_RESET                    Sig_Array[(_CHIP_GPIO_PINPERPORT * 0) + 0]						// P0_0 - PIO0_0-RESET
#define	sig_PIO0_1                          Sig_Array[(_CHIP_GPIO_PINPERPORT * 0) + 1]						// P0_1 - PIO0_1 - 32b Timer 0 Match 2
#define	sig_PIO0_2_SPI0_SSEL                Sig_Array[(_CHIP_GPIO_PINPERPORT * 0) + 2]						// P0_2 - PIO0_2 - SPI0 SSEL0, 16b Timer 0 Capture 0
#define	sig_PIO0_3                          Sig_Array[(_CHIP_GPIO_PINPERPORT * 0) + 3]						// P0_3 - PIO0_3
#define	sig_PIO0_4_I2C0_SCL					Sig_Array[(_CHIP_GPIO_PINPERPORT * 0) + 4]						// P0_4 - PIO0_4 - I2C0 SCL
#define	sig_PIO0_5_I2C0_SDA					Sig_Array[(_CHIP_GPIO_PINPERPORT * 0) + 5]						// P0_5 - PIO0_5 - I2C0 SDA
#define	sig_PIO0_6_SPI0_SCK                 Sig_Array[(_CHIP_GPIO_PINPERPORT * 0) + 6]						// P0_6 - PIO0_6 - SPI0 SCK
#define	sig_PIO0_8_SPI0_MISO                Sig_Array[(_CHIP_GPIO_PINPERPORT * 0) + 8]						// P0_8 - PIO0_8 - SPI0 MISO, 16b Timer 0 Match 0
#define	sig_PIO0_9_SPI0_MOSI                Sig_Array[(_CHIP_GPIO_PINPERPORT * 0) + 9]						// P0_9 - PIO0_9 - SPI0 MOSI, 16b Timer 0 Match 1
#define	sig_PIO0_10_SWCLK_SPI0_SCK          Sig_Array[(_CHIP_GPIO_PINPERPORT * 0) + 10]						// P0_10 - PIO0_10 - SWCLK, SPI0 SCK, 16b Timer 0 Match 2 
#define	sig_PIO0_11_ADC0_0					Sig_Array[(_CHIP_GPIO_PINPERPORT * 0) + 11]						// P0_11 - PIO0_11 - ADC0 inp.0, 32b Timer 0 Match 3

#define	sig_PIO1_0_ADC0_1					Sig_Array[(_CHIP_GPIO_PINPERPORT * 1) + 0]						// P1_0 - PIO1_0 - ADC0 inp.1, 32b Timer 0 Capture 1
#define	sig_PIO1_1_ADC0_2	          		Sig_Array[(_CHIP_GPIO_PINPERPORT * 1) + 1]						// P1_1 - PIO1_1 - ADC0 inp.2, 32b Timer 0 Match 0
#define	sig_PIO1_2_ADC0_3   				Sig_Array[(_CHIP_GPIO_PINPERPORT * 1) + 2]						// P1_2 - PIO1_2 - ADC0 inp.3, 32b Timer 1 Match 1
#define	sig_PIO1_3_SWDIO_ADC0_4				Sig_Array[(_CHIP_GPIO_PINPERPORT * 1) + 3]						// P1_3 - PIO1_3 - SWDIO, ADC0 inp.4, 32b Timer 1 Macth 2
#define	sig_PIO1_4_ADC0_5_WAKEUP			Sig_Array[(_CHIP_GPIO_PINPERPORT * 1) + 4]						// P1_4 - PIO1_4 - ADC0 inp.5, 32b Timer 1 Match 3
#define	sig_PIO1_5_UART0_RTS    			Sig_Array[(_CHIP_GPIO_PINPERPORT * 1) + 5]						// P1_5 - PIO1_5 - UART0 RTS, 32b Timer 0 Capture 0
#define	sig_PIO1_6_UART0_RXD    			Sig_Array[(_CHIP_GPIO_PINPERPORT * 1) + 6]						// P1_6 - PIO1_6 - UART0 RXD, 32b Timer 0 Match 0
#define	sig_PIO1_7_UART0_TXD				Sig_Array[(_CHIP_GPIO_PINPERPORT * 1) + 7]						// P1_7 - PIO1_7 - UART0 TXD, 32b Timer 0 Match 1
#define	sig_PIO1_8              			Sig_Array[(_CHIP_GPIO_PINPERPORT * 1) + 8]						// P1_8 - PIO1_8 - 16b Timer 1 Capture 0
#define	sig_PIO1_9_SPI1_MOSI                Sig_Array[(_CHIP_GPIO_PINPERPORT * 1) + 9]						// P1_9 - PIO1_9 - 16b Timer 1 Match 0, SPI1 MOSI
#define	sig_PIO1_10_ADC0_6      			Sig_Array[(_CHIP_GPIO_PINPERPORT * 1) + 10]						// P1_10 - PIO1_10 - ADC0 inp.6, 16b Timer 1 Match 1, SPI1 MISO
#define	sig_PIO1_11_ADC0_7      			Sig_Array[(_CHIP_GPIO_PINPERPORT * 1) + 11]						// P1_11 - PIO1_11 - ADC0 inp.7, 32b Timer 1 Capture 1

#define	sig_PIO2_0_UART0_DTR_SPI1_SSEL		Sig_Array[(_CHIP_GPIO_PINPERPORT * 2) + 0]						// P2_0 - PIO2_0 - UART0 DTR, SPI1 SSEL
#define	sig_PIO2_1_UART0_DSR_SPI1_SCK		Sig_Array[(_CHIP_GPIO_PINPERPORT * 2) + 1]						// P2_1 - PIO2_1 - UART0 DSR, SPI1 SCK
#define	sig_PIO2_2_UART0_DCD_SPI1_MISO		Sig_Array[(_CHIP_GPIO_PINPERPORT * 2) + 2]						// P2_2 - PIO2_2 - UART0 DCD, SPI1 MISO
#define	sig_PIO2_3_UART0_RI_SPI1_MOSI		Sig_Array[(_CHIP_GPIO_PINPERPORT * 2) + 3]						// P2_3 - PIO2_3 - UART0 RI, SPI1 MOSI
#define	sig_PIO2_4                  		Sig_Array[(_CHIP_GPIO_PINPERPORT * 2) + 4]						// P2_4 - PIO2_4 - 16b Timer 1 Match 1, SPI1 SSEL
#define	sig_PIO2_5                  		Sig_Array[(_CHIP_GPIO_PINPERPORT * 2) + 5]						// P2_5 - PIO2_5 - 32b Timer 0 Match 0
#define	sig_PIO2_6                  		Sig_Array[(_CHIP_GPIO_PINPERPORT * 2) + 6]						// P2_6 - PIO2_6 - 32b Timer 0 Match 1
#define	sig_PIO2_7_UART0_RXD          		Sig_Array[(_CHIP_GPIO_PINPERPORT * 2) + 7]						// P2_7 - PIO2_7 - 32b Timer 0 Match 2, UART0 RXD
#define	sig_PIO2_8_UART0_TXD           		Sig_Array[(_CHIP_GPIO_PINPERPORT * 2) + 8]						// P2_8 - PIO2_8 - 32b Timer 0 Match 3, UART0 TXD
#define	sig_PIO2_9                  		Sig_Array[(_CHIP_GPIO_PINPERPORT * 2) + 9]						// P2_9 - PIO2_9 - 32b Timer 0 Capture 0
#define	sig_PIO2_10                  		Sig_Array[(_CHIP_GPIO_PINPERPORT * 2) + 10]						// P2_10 - PIO2_10
#define	sig_PIO2_11_SPI0_SCK           		Sig_Array[(_CHIP_GPIO_PINPERPORT * 2) + 11]						// P2_11 - PIO2_11 - SPI0 SCK, 32b Timer 0 Capture 1

#define	sig_PIO3_0_UART0_DTR_TXD       		Sig_Array[(_CHIP_GPIO_PINPERPORT * 3) + 0]						// P3_0 - PIO3_0 - UART0 DTR, 16b Timer 0 Match 0, UART0 TXD
#define	sig_PIO3_1_UART0_DSR_RXD       		Sig_Array[(_CHIP_GPIO_PINPERPORT * 3) + 1]						// P3_1 - PIO3_1 - UART0 DSR, 16b Timer 0 Match 1, UART0 RXD
#define	sig_PIO3_2_UART0_DCD_SPI1_SCK  		Sig_Array[(_CHIP_GPIO_PINPERPORT * 3) + 2]						// P3_2 - PIO3_2 - UART0 DCD, 16b Timer 0 Match 2, SPI1 SCK
#define	sig_PIO3_3_UART0_RI           		Sig_Array[(_CHIP_GPIO_PINPERPORT * 3) + 3]						// P3_3 - PIO3_3 - UART0 RI, 16b Timer 0 Capture 0
#define	sig_PIO3_4_UART0_RXD          		Sig_Array[(_CHIP_GPIO_PINPERPORT * 3) + 4]						// P3_4 - PIO3_4 - 16b Timer 0 Capture 1, UART0 RXD
#define	sig_PIO3_5_UART0_TXD           		Sig_Array[(_CHIP_GPIO_PINPERPORT * 3) + 5]						// P3_5 - PIO3_5 - 16b Timer 1 Capture 1, UART0 TXD

#define	sig_virtual							Sig_Array[(_CHIP_GPIO_PINPERPORT * 3) + 6]						// virtual signal
#define	sig_last							Sig_Array[(_CHIP_GPIO_PINPERPORT * 3) + 7]						// virtual signal

	
// LED
#define	sig_PIO0_7_LED2_RED					Sig_Array[(_CHIP_GPIO_PINPERPORT * 0) + 7]						// P0_7 - PIO0_7



// SWD pindef
#define BSP_sig_SWDCLK		            	sig_PIO0_10_SWCLK_SPI0_SCK
#define BSP_sig_SWDIO			            sig_PIO1_3_SWDIO_ADC0_4
#define BSP_sig_SWO				            sig_virtual



// Preset Value for signals - config and value will be set after reset:
						// Port; Pin; Active; {Direction; IOCON reg				 			 		}   - for MCU without SWM Block!
						// Port; Pin; Active; {Direction; IOCON reg (16 bit); SWM reg (2x8 bits)	} 	- for MCU with SWM Block!

#define _SP0_0_FILL			{ 0,  0, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_1_FILL			{ 0,  1, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_2_FILL			{ 0,  2, 0, { CHAL_IO_Input, 	0x0000 | PFUN_ALT(1)}}	// Active in 0, Used as SPI0-SSEL
#define _SP0_3_FILL			{ 0,  3, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_4_FILL			{ 0,  4, 0, { CHAL_IO_Input, 	0x0000 | PFUN_ALT(1)}}	// Active in 0, Used as I2C0_SCL
#define _SP0_5_FILL			{ 0,  5, 0, { CHAL_IO_Input, 	0x0000 | PFUN_ALT(1)}}	// Active in 0, Used as I2C0-SDA
#define _SP0_6_FILL			{ 0,  6, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP0_7_FILL			{ 0,  7, 0, { CHAL_IO_Outut, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO for LED Red
#define _SP0_8_FILL			{ 0,  8, 0, { CHAL_IO_Input, 	0x0000 | PFUN_ALT(1)}}	// Active in 0, Used as SPI0-MISO
#define _SP0_9_FILL			{ 0,  9, 0, { CHAL_IO_Input, 	0x0000 | PFUN_ALT(1)}}	// Active in 0, Used as SPI0-MOSI
#define _SP0_10_FILL		{ 0,  0, 0, { CHAL_IO_Input, 	0x0000 | PFUN_ALT(0)}}	// used for SWCLK
#define _SP0_11_FILL		{ 0,  11, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO

#define _SP1_0_FILL			{ 1,  0, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_1_FILL			{ 1,  1, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_2_FILL			{ 1,  2, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_3_FILL			{ 1,  3, 0, { CHAL_IO_Input, 	0x0000 | PFUN_ALT(0)}}	// Used as SWDIO
#define _SP1_4_FILL			{ 1,  4, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_5_FILL			{ 1,  5, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_6_FILL			{ 1,  6, 0, { CHAL_IO_Input, 	0x0000 | PFUN_ALT(1)}}	// Active in 0, Used as UART0-RXD
#define _SP1_7_FILL			{ 1,  7, 0, { CHAL_IO_Input, 	0x0000 | PFUN_ALT(1)}}	// Active in 0, Used as UART0-TXD
#define _SP1_8_FILL			{ 1,  8, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_9_FILL			{ 1,  9, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_10_FILL		{ 1,  10, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP1_11_FILL		{ 1,  11, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO

#define _SP2_0_FILL		    { 2,  0, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_1_FILL		    { 2,  1, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_2_FILL		    { 2,  2, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_3_FILL		    { 2,  3, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_4_FILL		    { 2,  4, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_5_FILL		    { 2,  5, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_6_FILL		    { 2,  6, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_7_FILL		    { 2,  7, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_8_FILL		    { 2,  8, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_9_FILL		    { 2,  9, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_10_FILL	    { 2,  10, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP2_11_FILL	    { 2,  11, 0, { CHAL_IO_Input, 	0x0000 | PFUN_ALT(1)}}	// Active in 0, Used as SPI0-SCK

#define _SP3_0_FILL		    { 3,  0, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_1_FILL		    { 3,  1, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_2_FILL		    { 3,  2, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_3_FILL		    { 3,  3, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_4_FILL		    { 3,  4, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO
#define _SP3_5_FILL		    { 3,  5, 0, { CHAL_IO_Input, 	0x0000 | PFUN_GPIO}}	// Active in 0, Used as GPIO

// And declare as extern
extern const CHAL_Signal_t 				Sig_Array[];				// array of the used signals

#endif	//__BSP_EA_LPCXPRESSO_LPC1114_SIGNALDEF_H_
