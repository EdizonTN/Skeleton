// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: bsp_VRM_NXP_OM13098_SignalDef.c
// 	   Version: 3.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Board Support Package - Configuration header file example
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#include "Skeleton.h"

#ifdef __BSP_NXP_OM13098_H_

//const CHAL_Signal_t 	sig_first= { 255,255,0};	/* zaciatok structs; Port:255, Pin:255, ostatne nezadavaj	*/

// 180-pin TFBGA

const CHAL_Signal_t 	_SP0_0	= _SP0_0_FILL;		// makra _FILL sa naplnaju v 		bsp_xxx_SignalDef.h
const CHAL_Signal_t 	_SP0_1	= _SP0_1_FILL;
const CHAL_Signal_t 	_SP0_2  = _SP0_2_FILL;
const CHAL_Signal_t 	_SP0_3	= _SP0_3_FILL;
const CHAL_Signal_t 	_SP0_4	= _SP0_4_FILL;
const CHAL_Signal_t 	_SP0_5	= _SP0_5_FILL;
const CHAL_Signal_t 	_SP0_6	= _SP0_6_FILL;
const CHAL_Signal_t 	_SP0_7  = _SP0_7_FILL;
const CHAL_Signal_t 	_SP0_8	= _SP0_8_FILL;
const CHAL_Signal_t 	_SP0_9	= _SP0_9_FILL;
const CHAL_Signal_t 	_SP0_10	= _SP0_10_FILL;
const CHAL_Signal_t 	_SP0_11	= _SP0_11_FILL;
const CHAL_Signal_t 	_SP0_12	= _SP0_12_FILL;
const CHAL_Signal_t 	_SP0_13	= _SP0_13_FILL;
const CHAL_Signal_t 	_SP0_14	= _SP0_14_FILL;
const CHAL_Signal_t 	_SP0_15	= _SP0_15_FILL;
const CHAL_Signal_t 	_SP0_16	= _SP0_16_FILL;
const CHAL_Signal_t 	_SP0_17	= _SP0_17_FILL;
const CHAL_Signal_t 	_SP0_18	= _SP0_18_FILL;
const CHAL_Signal_t 	_SP0_19	= _SP0_19_FILL;
const CHAL_Signal_t 	_SP0_20	= _SP0_20_FILL;
const CHAL_Signal_t 	_SP0_21	= _SP0_21_FILL;
const CHAL_Signal_t 	_SP0_22	= _SP0_22_FILL;
const CHAL_Signal_t 	_SP0_23	= _SP0_23_FILL;
const CHAL_Signal_t 	_SP0_24	= _SP0_24_FILL;
const CHAL_Signal_t 	_SP0_25	= _SP0_25_FILL;
const CHAL_Signal_t 	_SP0_26	= _SP0_26_FILL;
const CHAL_Signal_t 	_SP0_27	= _SP0_27_FILL;
const CHAL_Signal_t 	_SP0_28	= _SP0_28_FILL;
const CHAL_Signal_t 	_SP0_29	= _SP0_29_FILL;
const CHAL_Signal_t 	_SP0_30	= _SP0_30_FILL;
const CHAL_Signal_t 	_SP0_31	= _SP0_31_FILL;

const CHAL_Signal_t 	_SP1_0	= _SP1_0_FILL;
const CHAL_Signal_t 	_SP1_1	= _SP1_1_FILL;
const CHAL_Signal_t 	_SP1_2	= _SP1_2_FILL;
const CHAL_Signal_t 	_SP1_3	= _SP1_3_FILL;
const CHAL_Signal_t 	_SP1_4	= _SP1_4_FILL;
const CHAL_Signal_t 	_SP1_5	= _SP1_5_FILL;
const CHAL_Signal_t 	_SP1_6	= _SP1_6_FILL;
const CHAL_Signal_t 	_SP1_7	= _SP1_7_FILL;
const CHAL_Signal_t 	_SP1_8	= _SP1_8_FILL;
const CHAL_Signal_t 	_SP1_9	= _SP1_9_FILL;
const CHAL_Signal_t 	_SP1_10	= _SP1_10_FILL;
const CHAL_Signal_t 	_SP1_11	= _SP1_11_FILL;
const CHAL_Signal_t 	_SP1_12	= _SP1_12_FILL;
const CHAL_Signal_t 	_SP1_13	= _SP1_13_FILL;
const CHAL_Signal_t 	_SP1_14	= _SP1_14_FILL;
const CHAL_Signal_t 	_SP1_15	= _SP1_15_FILL;
const CHAL_Signal_t 	_SP1_16	= _SP1_16_FILL;
const CHAL_Signal_t 	_SP1_17	= _SP1_17_FILL;
const CHAL_Signal_t 	_SP1_18	= _SP1_18_FILL;
const CHAL_Signal_t 	_SP1_19	= _SP1_19_FILL;
const CHAL_Signal_t 	_SP1_20	= _SP1_20_FILL;
const CHAL_Signal_t 	_SP1_21	= _SP1_21_FILL;
const CHAL_Signal_t 	_SP1_22	= _SP1_22_FILL;
const CHAL_Signal_t 	_SP1_23	= _SP1_23_FILL;
const CHAL_Signal_t 	_SP1_24	= _SP1_24_FILL;
const CHAL_Signal_t 	_SP1_25	= _SP1_25_FILL;
const CHAL_Signal_t 	_SP1_26	= _SP1_26_FILL;
const CHAL_Signal_t 	_SP1_27	= _SP1_27_FILL;
const CHAL_Signal_t 	_SP1_28	= _SP1_28_FILL;
const CHAL_Signal_t 	_SP1_29	= _SP1_29_FILL;
const CHAL_Signal_t 	_SP1_30	= _SP1_30_FILL;
const CHAL_Signal_t 	_SP1_31	= _SP1_31_FILL;

const CHAL_Signal_t 	_SP2_0	= _SP2_0_FILL;
const CHAL_Signal_t 	_SP2_1	= _SP2_1_FILL;
const CHAL_Signal_t 	_SP2_2	= _SP2_2_FILL;
const CHAL_Signal_t 	_SP2_3	= _SP2_3_FILL;
const CHAL_Signal_t 	_SP2_4	= _SP2_4_FILL;
const CHAL_Signal_t 	_SP2_5	= _SP2_5_FILL;
const CHAL_Signal_t 	_SP2_6	= _SP2_6_FILL;
const CHAL_Signal_t 	_SP2_7	= _SP2_7_FILL;
const CHAL_Signal_t 	_SP2_8	= _SP2_8_FILL;
const CHAL_Signal_t 	_SP2_9	= _SP2_9_FILL;
const CHAL_Signal_t 	_SP2_10	= _SP2_10_FILL;
const CHAL_Signal_t 	_SP2_11	= _SP2_11_FILL;
const CHAL_Signal_t 	_SP2_12	= _SP2_12_FILL;
const CHAL_Signal_t 	_SP2_13	= _SP2_13_FILL;
const CHAL_Signal_t 	_SP2_14	= _SP2_14_FILL;
const CHAL_Signal_t 	_SP2_15	= _SP2_15_FILL;
const CHAL_Signal_t 	_SP2_16	= _SP2_16_FILL;
const CHAL_Signal_t 	_SP2_17	= _SP2_17_FILL;
const CHAL_Signal_t 	_SP2_18	= _SP2_18_FILL;
const CHAL_Signal_t 	_SP2_19	= _SP2_19_FILL;
const CHAL_Signal_t 	_SP2_20	= _SP2_20_FILL;
const CHAL_Signal_t 	_SP2_21	= _SP2_21_FILL;
const CHAL_Signal_t 	_SP2_22	= _SP2_22_FILL;
const CHAL_Signal_t 	_SP2_23	= _SP2_23_FILL;
const CHAL_Signal_t 	_SP2_24	= _SP2_24_FILL;
const CHAL_Signal_t 	_SP2_25	= _SP2_25_FILL;
const CHAL_Signal_t 	_SP2_26	= _SP2_26_FILL;
const CHAL_Signal_t 	_SP2_27	= _SP2_27_FILL;
const CHAL_Signal_t 	_SP2_28	= _SP2_28_FILL;
const CHAL_Signal_t 	_SP2_29	= _SP2_29_FILL;
const CHAL_Signal_t 	_SP2_30	= _SP2_30_FILL;
const CHAL_Signal_t 	_SP2_31	= _SP2_31_FILL;

const CHAL_Signal_t 	_SP3_0	= _SP3_0_FILL;
const CHAL_Signal_t 	_SP3_1	= _SP3_1_FILL;
const CHAL_Signal_t 	_SP3_2	= _SP3_2_FILL;
const CHAL_Signal_t 	_SP3_3	= _SP3_3_FILL;
const CHAL_Signal_t 	_SP3_4	= _SP3_4_FILL;
const CHAL_Signal_t 	_SP3_5	= _SP3_5_FILL;
const CHAL_Signal_t 	_SP3_6	= _SP3_6_FILL;
const CHAL_Signal_t 	_SP3_7	= _SP3_7_FILL;
const CHAL_Signal_t 	_SP3_8	= _SP3_8_FILL;
const CHAL_Signal_t 	_SP3_9	= _SP3_9_FILL;
const CHAL_Signal_t 	_SP3_10	= _SP3_10_FILL;
const CHAL_Signal_t 	_SP3_11	= _SP3_11_FILL;
const CHAL_Signal_t 	_SP3_12	= _SP3_12_FILL;
const CHAL_Signal_t 	_SP3_13	= _SP3_13_FILL;
const CHAL_Signal_t 	_SP3_14	= _SP3_14_FILL;
const CHAL_Signal_t 	_SP3_15	= _SP3_15_FILL;
const CHAL_Signal_t 	_SP3_16	= _SP3_16_FILL;
const CHAL_Signal_t 	_SP3_17	= _SP3_17_FILL;
const CHAL_Signal_t 	_SP3_18	= _SP3_18_FILL;
const CHAL_Signal_t 	_SP3_19	= _SP3_19_FILL;
const CHAL_Signal_t 	_SP3_20	= _SP3_20_FILL;
const CHAL_Signal_t 	_SP3_21	= _SP3_21_FILL;
const CHAL_Signal_t 	_SP3_22	= _SP3_22_FILL;
const CHAL_Signal_t 	_SP3_23	= _SP3_23_FILL;
const CHAL_Signal_t 	_SP3_24	= _SP3_24_FILL;
const CHAL_Signal_t 	_SP3_25	= _SP3_25_FILL;
const CHAL_Signal_t 	_SP3_26	= _SP3_26_FILL;
const CHAL_Signal_t 	_SP3_27	= _SP3_27_FILL;
const CHAL_Signal_t 	_SP3_28	= _SP3_28_FILL;
const CHAL_Signal_t 	_SP3_29	= _SP3_29_FILL;
const CHAL_Signal_t 	_SP3_30	= _SP3_30_FILL;
const CHAL_Signal_t 	_SP3_31	= _SP3_31_FILL;

const CHAL_Signal_t 	_SP4_0	= _SP4_0_FILL;
const CHAL_Signal_t 	_SP4_1	= _SP4_1_FILL;
const CHAL_Signal_t 	_SP4_2	= _SP4_2_FILL;
const CHAL_Signal_t 	_SP4_3	= _SP4_3_FILL;
const CHAL_Signal_t 	_SP4_4	= _SP4_4_FILL;
const CHAL_Signal_t 	_SP4_5	= _SP4_5_FILL;
const CHAL_Signal_t 	_SP4_6	= _SP4_6_FILL;
const CHAL_Signal_t 	_SP4_7	= _SP4_7_FILL;
const CHAL_Signal_t 	_SP4_8	= _SP4_8_FILL;
const CHAL_Signal_t 	_SP4_9	= _SP4_9_FILL;
const CHAL_Signal_t 	_SP4_10	= _SP4_10_FILL;
const CHAL_Signal_t 	_SP4_11	= _SP4_11_FILL;
const CHAL_Signal_t 	_SP4_12	= _SP4_12_FILL;
const CHAL_Signal_t 	_SP4_13	= _SP4_13_FILL;
const CHAL_Signal_t 	_SP4_14	= _SP4_14_FILL;
const CHAL_Signal_t 	_SP4_15	= _SP4_15_FILL;
const CHAL_Signal_t 	_SP4_16	= _SP4_16_FILL;
#endif		// __BSP_NXP_OM13098_H_
