// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: bsp_NXP_OM13056.c
// 	   Version: 3.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Board support package file
//							hardware specific declaration and functions
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#include "Skeleton.h"

#ifdef __BSP_NXP_OM13098_H_

extern const CHAL_Signal_t 	sig_first;

// ------------------------------------------------------------------------------------------------
void bsp_Chip_Init(void) ATTR_ALWAYS_INLINE;
void bsp_Chip_Set_SystemClocking(void)	ATTR_ALWAYS_INLINE;

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------


// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// These functions are called from reset vector !!!!
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// MCU Chip init based on board HW
// If you need special settings in SystemInit, write your own code here.
// will be insert into SystemInit
// Ex. SWM remap
void bsp_Chip_Init(void)
{
	// write board init code here:
	
	
//#ifdef NDEBUG
//#else	
//	SCnSCB->ACTLR |= 1 << SCnSCB_ACTLR_DISDEFWBUF_Pos;								// disable write buffering - DEBUG ONLY !!!! 
//#endif

	
// USAGE, MEM and BUS FAULT IRQ
	SCB->SHCSR |= SCB_SHCSR_USGFAULTENA_Msk
		| SCB_SHCSR_BUSFAULTENA_Msk
		| SCB_SHCSR_MEMFAULTENA_Msk; 												// enable Usage-, Bus-, and MMU Fault	

	
	
	NVIC_SetPriority (MemoryManagement_IRQn, 0x0F);									// http://www.keil.com/appnotes/files/apnt209.pdf
	NVIC_SetPriority (BusFault_IRQn, 0x08);											
	NVIC_SetPriority (UsageFault_IRQn, 0x01);	
}



// ------------------------------------------------------------------------------------------------
// Sets up system pin muxing
void bsp_Chip_Set_SystemPinMux(void)
{

}

// ------------------------------------------------------------------------------------------------
// Set up and initialize clocking prior to call to main
void bsp_Chip_Set_SystemClocking(void)
{
	uint32_t cpu_clk;
	// This BSP used external 12MHz crystal as main clk input. Now we switch to use it...


    //POWER_DisablePD(kPDRUNCFG_PD_FRO_EN);                   						// Ensure FRO is on
	SYSCON->PDRUNCFGCLR[((uint32_t)kPDRUNCFG_PD_FRO_EN >> 8UL)] = (1UL << ((uint32_t)kPDRUNCFG_PD_FRO_EN & 0xffU));
	
    //CLOCK_AttachClk(kFRO12M_to_MAIN_CLK);                  						// Switch to FRO 12MHz first to ensure we can change voltage without accidentally being below the voltage for current speed
	SYSCON->MAINCLKSELA = SYSCON_MAINCLKSELA_SEL(0);								// MainCLK - select input as FRO12M
	SYSCON->MAINCLKSELB = SYSCON_MAINCLKSELB_SEL(0);								// MainCLK - select input as FRO12M

	SYSCON->PDRUNCFGCLR[((uint32_t)kPDRUNCFG_PD_SYS_OSC >> 8UL)] = (1UL << ((uint32_t)kPDRUNCFG_PD_SYS_OSC & 0xffU));
	
    SYSCON->SYSOSCCTRL = ((SYSCON->SYSOSCCTRL & ~SYSCON_SYSOSCCTRL_FREQRANGE_MASK) | SYSCON_SYSOSCCTRL_FREQRANGE(0U)); // Set system oscillator range
    POWER_SetVoltageForFreq(180000000U);             								// Set voltage for the one of the fastest clock outputs: System clock output
	_Chip_SetFLASHAccessCyclesForFreq(180000000U);									// Setup FLASH access timing

	SYSCON->SYSPLLCLKSEL = SYSCON_SYSPLLCLKSEL_SEL(1);								// System PLL - select input as CLK_IN
	
	_Chip_Clock_Set_PLLFreq (12, 540, 3);											// Set desired CPU Frequency 180MHz
	
	SYSCON->AHBCLKDIV = 0;															// divide by 1

	SYSCON->MAINCLKSELA = SYSCON_MAINCLKSELA_SEL(0);								// MainCLK - select input as FRO12M
	SYSCON->MAINCLKSELB = SYSCON_MAINCLKSELB_SEL(2);								// MainCLK - select input as PLL_CLK
	
	// MCU Core has set clock freq.
	// Select clock for other peripherials:
	SYSCON->ASYNCAPBCTRL = SYSCON_ASYNCAPBCTRL_ENABLE_MASK;							// APB Enable
	ASYNC_SYSCON->ASYNCAPBCLKSELA = 0x00;											// APB set to MainCLK

	cpu_clk = _Chip_Clock_Get_CoreClk_Rate();										// re-read clock freq.
	
	SYSCON->ARMTRACECLKDIV = 29;													// ARM trace clock divider register set to 1 - run it.
	TPI->ACPR = (cpu_clk / 2000000)-1;												// TPIU - Set the baud rate

	CHAL_Chip_SWO_Init(cpu_clk);													// Change SWO clock
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// These functions are called from reset vector !!!!
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// IO Initialization
void bsp_Set_SystemPinDefault(void)
{
	// set some critical IO pins to default state here:

	uint32_t i = 0;

	do
	{
		CHAL_Signal_Init ( &Sig_Array[i] );
		i ++;
	}while ((Sig_Array[i].Std.Pin != 0xff) | (Sig_Array[i].Std.Port != 0xff));		// repeat until __last structure


}
		

// ------------------------------------------------------------------------------------------------
// Prepare Pins and core for Debug if enabled
void bsp_Debug_Low(void)
{
	// SerialWire Debug - if enabled, activate appropriate pins
#if defined(BSP_DEBUG_SWD) && (BSP_DEBUG_SWD == 1)
	SYSCON->AHBCLKCTRLSET[0] = 1<<13;												// Switch On CLK for IOCON - bit 13
	
	// enable SWCLK and SWD pins:
	IOCON->PIO[0][11] = IOCON_PIO_FUNC(6) | IOCON_PIO_MODE(2) | IOCON_PIO_DIGIMODE(1) | IOCON_PIO_FILTEROFF(1); //SWCLK
	IOCON->PIO[0][12] = IOCON_PIO_FUNC(6) | IOCON_PIO_MODE(2) | IOCON_PIO_DIGIMODE(1) | IOCON_PIO_FILTEROFF(1);	//SWDIO
	
#if defined(BSP_DEBUG_SWO) && (BSP_DEBUG_SWO == 1)
	// Enable SWO Pin:
	IOCON->PIO[0][10] = IOCON_PIO_FUNC(6) | IOCON_PIO_MODE(2) | IOCON_PIO_DIGIMODE(1) | IOCON_PIO_FILTEROFF(1); //SWO
#endif	

	uint32_t sysclk = _Chip_Clock_Get_MainClk_Rate();
	_Chip_SWO_Init(sysclk);

	//SYSCON->AHBCLKCTRLCLR[0] = 1<<13;												// Switch Off CLK for IOCON - bit 13
#endif	
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// These functions are called as first from main()
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// Init CPU and connected hardware
void bsp_Init(void)
{
	bsp_Chip_Set_SystemPinMux();													// set core critical pins remap
	
	sys_System.Clock->Clock_Refresh();												// call clock refresh information from system.c
	
	SysTick_Config(sys_System.Clock->CPU_Freq / CONF_SYSTICK_FREQ);					// Enable systick timer
}


















const CHAL_Signal_t 	Sig_Array[] ATTR_USED  __SECTION_RODATA = 
{
	_SP0_0_FILL,
	_SP0_1_FILL,
	_SP0_2_FILL,
	_SP0_3_FILL,
	_SP0_4_FILL,
	_SP0_5_FILL,
	_SP0_6_FILL,
	_SP0_7_FILL,
	_SP0_8_FILL,
	_SP0_9_FILL,
	_SP0_10_FILL,
	_SP0_11_FILL,
	_SP0_12_FILL,
	_SP0_13_FILL,
	_SP0_14_FILL,
	_SP0_15_FILL,
	_SP0_16_FILL,
	_SP0_17_FILL,
	_SP0_18_FILL,
	_SP0_19_FILL,
	_SP0_20_FILL,
	_SP0_21_FILL,
	_SP0_22_FILL,
	_SP0_23_FILL,
	_SP0_24_FILL,
	_SP0_25_FILL,
	_SP0_26_FILL,
	_SP0_27_FILL,
	_SP0_28_FILL,
	_SP0_29_FILL,
	_SP0_30_FILL,
	_SP0_31_FILL,
	_SP1_0_FILL,
	_SP1_1_FILL,
	_SP1_2_FILL,
	_SP1_3_FILL,
	_SP1_4_FILL,
	_SP1_5_FILL,
	_SP1_6_FILL,
	_SP1_7_FILL,
	_SP1_8_FILL,
	_SP1_9_FILL,
	_SP1_10_FILL,
	_SP1_11_FILL,
	_SP1_12_FILL,
	_SP1_13_FILL,
	_SP1_14_FILL,
	_SP1_15_FILL,
	_SP1_16_FILL,
	_SP1_17_FILL,
	_SP1_18_FILL,
	_SP1_19_FILL,
	_SP1_20_FILL,
	_SP1_21_FILL,
	_SP1_22_FILL,
	_SP1_23_FILL,
	_SP1_24_FILL,
	_SP1_25_FILL,
	_SP1_26_FILL,
	_SP1_27_FILL,
	_SP1_28_FILL,
	_SP1_29_FILL,
	_SP1_30_FILL,
	_SP1_31_FILL,
	_SP2_0_FILL,
	_SP2_1_FILL,
	_SP2_2_FILL,
	_SP2_3_FILL,
	_SP2_4_FILL,
	_SP2_5_FILL,
	_SP2_6_FILL,
	_SP2_7_FILL,
	_SP2_8_FILL,
	_SP2_9_FILL,
	_SP2_10_FILL,
	_SP2_11_FILL,
	_SP2_12_FILL,
	_SP2_13_FILL,
	_SP2_14_FILL,
	_SP2_15_FILL,
	_SP2_16_FILL,
	_SP2_17_FILL,
	_SP2_18_FILL,
	_SP2_19_FILL,
	_SP2_20_FILL,
	_SP2_21_FILL,
	_SP2_22_FILL,
	_SP2_23_FILL,
	_SP2_24_FILL,
	_SP2_25_FILL,
	_SP2_26_FILL,
	_SP2_27_FILL,
	_SP2_28_FILL,
	_SP2_29_FILL,
	_SP2_30_FILL,
	_SP2_31_FILL,	
	_SP3_0_FILL,
	_SP3_1_FILL,
	_SP3_2_FILL,
	_SP3_3_FILL,
	_SP3_4_FILL,
	_SP3_5_FILL,
	_SP3_6_FILL,
	_SP3_7_FILL,
	_SP3_8_FILL,
	_SP3_9_FILL,
	_SP3_10_FILL,
	_SP3_11_FILL,
	_SP3_12_FILL,
	_SP3_13_FILL,
	_SP3_14_FILL,
	_SP3_15_FILL,
	_SP3_16_FILL,
	_SP3_17_FILL,
	_SP3_18_FILL,
	_SP3_19_FILL,
	_SP3_20_FILL,
	_SP3_21_FILL,
	_SP3_22_FILL,
	_SP3_23_FILL,
	_SP3_24_FILL,
	_SP3_25_FILL,
	_SP3_26_FILL,
	_SP3_27_FILL,
	_SP3_28_FILL,
	_SP3_29_FILL,
	_SP3_30_FILL,
	_SP3_31_FILL,	
	_SP4_0_FILL,
	_SP4_1_FILL,
	_SP4_2_FILL,
	_SP4_3_FILL,
	_SP4_4_FILL,
	_SP4_5_FILL,
	_SP4_6_FILL,
	_SP4_7_FILL,
	_SP4_8_FILL,
	_SP4_9_FILL,
	_SP4_10_FILL,
	_SP4_11_FILL,
	_SP4_12_FILL,
	_SP4_13_FILL,
	_SP4_14_FILL,
	_SP4_15_FILL,
	_SP4_16_FILL,
	{{ 255, 0, 0}},										// virtual
	{{ 255, 255, 0}},									// last	
};

//const	CHAL_Signal_t	*sig_last		=	&Sig_Array[sizeof(Sig_Array)/sizeof(Sig_Array[0])] - 1;


#endif      // __BSP_NXP_OM13098_H_
