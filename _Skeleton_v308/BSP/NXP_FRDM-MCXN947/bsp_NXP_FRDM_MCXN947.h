// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: bsp_NXP_FRDM_MCXN947.h
// 	   Version: 1.00
//        Date: 2024.09.09
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Board support package file - hardware specific declaration and functions - header
// ******************************************************************************
// Info: 
//
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#ifndef __BSP_NXP_FRDM_MCXN947_H_
#define __BSP_NXP_FRDM_MCXN947_H_

#include "bsp_NXP_FRDM_MCXN947_Config.h"												// load chip/board configuration settings


#ifndef LOAD_SCATTER


#ifdef COMP_TYPE_UV				// make path is different for various compilers:
	#define	CHIPDIRFILE 		<CONCAT(CONF_SKELETON_DIR_REL,\Chip\Chip.h)>		// Chip functions and declarations of peripherials for selected chip
#else
	#define	CHIPDIRFILE 		MAKEPATH(CONF_SKELETON_DIR_REL,\Chip\Chip.h)
#endif

// Chip functions and declarations of peripherials for selected chip

#include CHIPDIRFILE


// ************************************************************************************************
// Export
// ************************************************************************************************
// Called from reet vector
extern void bsp_Chip_Init(void);													// chip init
extern void bsp_Chip_Set_SystemClocking(void);										// Set Chip clocking

//Called from main:
extern void bsp_Set_SystemPinDefault(void);											// set/rest GPIO into safe state in according to BSP Hardware
extern void bsp_Debug_Low(void);													// prepare IO pin for debug in/out
extern void bsp_Init(void);															// Board Hardware initialization

// ************************************************************************************************
// Connection
// ************************************************************************************************


// here you can declare hardware connection. Ex. LED_Status_Port	as P1.0, ....
#include 	"bsp_NXP_FRDM_MCXN947_SignalDef.h"
#endif		// LOAD_SCATTER
#endif 		// __BSP_NXP_FRDM_MCXN947_H_
