// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: bsp_NXP_FRDM_MCXN947_SignalDef.h
// 	   Version: 1.00
//        Date: 2024.09.09
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Pin Signal definition file for CarBax Interface board
// ******************************************************************************
// Info: 
//
// Usage:
//
// ToDo:
//
// Changelog:
//

#ifndef __BSP_NXP_FRDM_MCXN947_SIGNALDEF_H_
#define __BSP_NXP_FRDM_MCXN947_SIGNALDEF_H_

#include <Chip\Chip_HAL.h>



// ------------------------------------------------------------------------------------------------
// IO Pin Signal human readable declaration. 
// Will be good, if signals will have it same name as is in schematic - for quick knowing
// Or assign some logical names. Ex Button1, I2C_SDA, LCD_PCLK, or similar
// ------------------------------------------------------------------------------------------------

//	----------------------------------- Description of the naming and defining:
//		Readable name:						(32* Port.Number) + Pin.Number			// Comment




// LED
//#define	sig_LED_RUN					Sig_Array[26]									// P0_28 - LED RUN






// Preset Value for signals - config and value will be set after reset:
						// Port; Pin; Active; {Direction; IOCON reg				 			 		}   - for MCU without SWM Block!
						// Port; Pin; Active; {Direction; IOCON reg (16 bit); SWM reg (2x8 bits)	} 	- for MCU with SWM Block!
						// Port: 0xff, Pin:0x00	=	pin is virtual.
						// PIN_ENABLE(xx): xx - Pin Enable Register number + pin shift		bit 31-29: PIENABLE register number [0-7]
						//																	bit 28-0: pin position in PINENABLE [0-31]
						// PIN_ASSIGN(xx,yy): xz - Pin Assign Reg Num., yy - pin number: 0 - 28 // for LPC with SWM !
						//					x - reg num., z - symbol offset [0..3]

//	uint8_t								Port;										// cislo portu
//	uint8_t								Pin;										// cislo pinu
//	bool								Act;										// active in...
//	CHAL_IO_Mode_t						Dir;										// Direction input/output - myslene hned po iniciallizacii, default stav. Definovane v _chip_xxx.h
//		uint16_t							IOCON_Reg;									// IOCON register hodnota. pouziva sa len 16 bit. Inak IOCON je 32 bitovy	 */
//		uint8_t								SWM_RegBlock;								// SWM - movable function/fixed function: vyber cisla registra + bloku ASSIGN/FIXED */
//		uint8_t								SWM_PinNum;									// SWM - movable function/fixed function: Hodnota registra Assign. Ak 0xFF porom pis do FIXED */


//#define _SP_1_FILL			0,  1, 0, {CHAL_IO_Input,  {IOCON_MODE_PULLUP, CHIP_PINSWMUNUSED}}			// Active in 0, Used as Standard GPIO - Button - Key.Dn

#define _VIRTUAL			0xff,0, 0, {CHAL_IO_Input, {0, 0, 0}}
#define _LAST				0xff,0xff,0,{CHAL_IO_Input,{0, 0, 0}}


// And declare as extern
extern const CHAL_Signal_t 				Sig_Array[];				// array of the used signals

#endif	//__BSP_NXP_FRDM_MCXN947_SIGNALDEF_H_
