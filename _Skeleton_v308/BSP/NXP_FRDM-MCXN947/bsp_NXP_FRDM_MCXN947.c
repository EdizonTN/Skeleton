// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: bsp_NXP_FRDM_MCXN947.c
// 	   Version: 1.00
//        Date: 2024.09.09
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Board support package file - hardware specific declaration and functions
// ******************************************************************************
// Info: 
//
// Usage:
// 
// ToDo:
// 
// Changelog:


#include "Skeleton.h"

#ifdef __BSP_NXP_FRDM_MCXN947_H_

extern const CHAL_Signal_t 	sig_first;

// ------------------------------------------------------------------------------------------------
void bsp_Chip_Init(void);
void bsp_Chip_Set_SystemClocking(void);

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------



// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// These functions are called from reset vector !!!!
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// MCU Chip init based on board HW
// If you need special settings in SystemInit, write your own code here.
// will be insert into SystemInit
// Ex. SWM remap
void bsp_Chip_Init(void)
{
	// write MCU Init according to board here:
#if (__CORTEX_M >= 0x03)
	NVIC_SetPriority (MemoryManagement_IRQn, 0x0F);									// http://www.keil.com/appnotes/files/apnt209.pdf
	NVIC_SetPriority (BusFault_IRQn, 0x08);											
	NVIC_SetPriority (UsageFault_IRQn, 0x01);	
#endif	
}

// ------------------------------------------------------------------------------------------------
// Set up and initialize clocking prior to call to main
void bsp_Chip_Set_SystemClocking(void)
{
//	uint16_t 	i;
	uint32_t	FAIM_Mirror;
	
	
	SYSCON->PDRUNCFG &= ~SYSCON_PDRUNCFG_FRO_PD(1);									// Power Up FRO
	SYSCON->PDRUNCFG &= ~SYSCON_PDRUNCFG_FROOUT_PD(1);								// Power Up FRO OUT
	
	_Chip_IAP_SetFroOscFreq(CONF_CHIP_FRORATE / 1000);								// SetState FRO OSC at 18MHz
	_Chip_IAP_ReadFAIMPage(0, &FAIM_Mirror);
	
	//Set froOsc reg. -> fro osc control
	//SYSCON->FROOSCCTRL = (1 << 17);												// fro is direct, not divided
	SYSCON->FRODIRECTCLKUEN &= ~(1 << 0);											// Update system clock setting
	SYSCON->FRODIRECTCLKUEN |= (1 << 0);

	// Prepare for external Quartz
	//SYSCON->PDRUNCFG &= ~SYSCON_PDRUNCFG_SYSOSC_PD(1);							// Power Up SysOSC
	//for (i = 0; i < 0x500; i++) {}												// Wait 500us for OSC to be stablized, no status indication, dummy wait.

	// PLL settings
	SYSCON->PDRUNCFG |= SYSCON_PDRUNCFG_SYSPLL_PD(1);								// Power Down PLL

	//Set SysPLLclkSel reg. -> system_pll source
	SYSCON->SYSPLLCLKSEL = SYSCON_SYSPLLCLKSEL_SEL(0x00);							// Switch to internal FRO oscillator
	SYSCON->SYSPLLCLKUEN &= ~(1 << 0);												// Update system clock setting
	SYSCON->SYSPLLCLKUEN |=	(1 << 0);
		
	SYSCON->SYSPLLCTRL = SYSCON_SYSPLLCTRL_MSEL(BSP_FREQ_PLL_M_VAL) | SYSCON_SYSPLLCTRL_PSEL(BSP_FREQ_PLL_P_VAL);// Setup  PLL : 	CoreFreq = Main Crystal * PLL+1
		
	SYSCON->PDRUNCFG &= ~SYSCON_PDRUNCFG_SYSPLL_PD(1);								// Power Up PLL
		
	while ((SYSCON->SYSPLLSTAT & SYSCON_SYSPLLSTAT_LOCK_MASK)) {}					// Wait for PLL FOR lock

		
	SYSCON->SYSAHBCLKDIV  = SYSCON_SYSAHBCLKDIV_DIV(BSP_SYSAHBCLKDIV);				// Set system clock divider
		
	//Set MainCLKSel reg. -> main_clk_pre_pll source
	SYSCON->MAINCLKSEL = SYSCON_MAINCLKSEL_SEL(0x00);								// set fro source
	SYSCON->MAINCLKUEN &= ~(1 << 0);												// Update system clock setting
	SYSCON->MAINCLKUEN |= (1 << 0);	
		
	//Set MainCLKPLLSel reg. -> main_clk source		
	SYSCON->MAINCLKPLLSEL = SYSCON_MAINCLKPLLSEL_SEL(0x01);							// Main Clock source = main_clk_pre_pll
	SYSCON->MAINCLKPLLUEN &= ~(1 << 0);												// Update system clock setting
	SYSCON->MAINCLKPLLUEN |= (1 << 0);	

	
	SYSCON->FCLKSEL[0] = SYSCON_FCLKSEL_SEL(1);										// UART0 - main_clk
	SYSCON->FCLKSEL[1] = SYSCON_FCLKSEL_SEL(1);										// UART1 - main_clk
	SYSCON->FCLKSEL[2] = SYSCON_FCLKSEL_SEL(1);										// UART2 - main_clk
	SYSCON->FCLKSEL[3] = SYSCON_FCLKSEL_SEL(1);										// UART3 - main_clk
	SYSCON->FCLKSEL[4] = SYSCON_FCLKSEL_SEL(1);										// UART4 - main_clk
	SYSCON->FCLKSEL[5] = SYSCON_FCLKSEL_SEL(1);										// I2C0 - main_clk
	SYSCON->FCLKSEL[6] = SYSCON_FCLKSEL_SEL(1);										// I2C1 - main_clk
	SYSCON->FCLKSEL[7] = SYSCON_FCLKSEL_SEL(1);										// I2C2 - main_clk
	SYSCON->FCLKSEL[8] = SYSCON_FCLKSEL_SEL(1);										// I2C3 - main_clk
	SYSCON->FCLKSEL[9] = SYSCON_FCLKSEL_SEL(1);										// SPI0 - main_clk
	SYSCON->FCLKSEL[10] = SYSCON_FCLKSEL_SEL(1);									// SPI1 - main_clk	

	//SYSCON->CLKOUTDIV = 100;
	//SYSCON->CLKOUTSEL = SYSCON_CLKOUTSEL_SEL(0x01);									// CLKOUT Clock source = main_clk_pre_pll
		
}

// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------
// These functions are called as first from main()
// ------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------

// ------------------------------------------------------------------------------------------------
// IO Initialization according to Board
void bsp_Set_SystemPinDefault(void)
{
	// set some critical IO pins to default state here:

	uint32_t i = 0;

	do
	{
		CHAL_Signal_Init ( &Sig_Array[i] );
		i ++;
	}while ((Sig_Array[i].Std.Pin != 0xff) | (Sig_Array[i].Std.Port != 0xff));		// repeat until __last structure


}


// ------------------------------------------------------------------------------------------------
// Prepare Pins and core for LowLevel Debug if enabled
void bsp_Debug_Low(void)
{
	// SerialWire Debug - if enabled, activate appropriate pins
#if defined(BSP_DEBUG_SWD) && (BSP_DEBUG_SWD == 1)
	CHAL_Signal_Init(&BSP_sig_SWDIO);												// init Serial Wire Debug Interface - data
	CHAL_Signal_Init(&BSP_sig_SWCLK);												// init Serial Wire Debug Interface - clock
#endif	
}




// ------------------------------------------------------------------------------------------------
// Init CPU and connected hardware
void bsp_Init(void)
{
	sys_System.Clock->Clock_Refresh();												// call clock refresh information from system.c
	SysTick_Config(sys_System.Clock->CPU_Freq / CONF_SYSTICK_FREQ);					// Enable systick timer and configure it
}


	// Port, Pin, Act, ( Mode,		 ( IO_Con,  SWM_Reg, SWM_Pin))
	// 0xff, 0xff, 1	,CHAL_IO_Input, 0xffff,   0xff,   0xff
const CHAL_Signal_t 	Sig_Array[]  = 
{
	_SP_0_FILL,
	_VIRTUAL,																		// virtual signal
	_LAST																			// last signal - mark as end 
};



#endif      // __BSP_NXP_FRDM_MCXN947_H_
