// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_ILI9806.c
// 	   Version: 3.01
//		  Desc: Driver for ILI9806 IC - TFT LCD Driver
// ******************************************************************************
// Info: SPI driver. 
//			based on: https://github.com/thinkoco/mi-lcd/blob/master/Linux_driver/4.19.x/tinydrm/fb_ili9806.c
// Notice:
//
// Usage:
//			
// ToDo:	
//
// Changelog:
//				2024.09.30	- v3.01 - add sys_NULL_Function instead a NULL pointer to a function in API structure

#include "Skeleton.h"

#if defined(USE_DRV_ILI9806) && (USE_DRV_ILI9806 == 1)

// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool ILI9806_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool ILI9806_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool ILI9806_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);

//uint32_t Decode_Batt_Voltage(_drv_If_t *pDrvIf);
void ILI9806_MirrorRegs(_drv_If_t *pDrvIf);
//void ILI9806_FastMonitor(_drv_If_t *pDrvIf);
//static bool ILI9806_Preset(_drv_If_t *pDrvIf);

// ******************************************************************************************************
// Public Function - specific for this driver
// ******************************************************************************************************
const drv_ILI9806_Ext_API_t	ILI9806_Ext_API 	=
{
//	.Preset								= ILI9806_Preset,
//	.Read_Batt_Voltage					= Decode_Batt_Voltage,
	.Reload_MirrorRegs					= ILI9806_MirrorRegs,
//	.FastMonitor   						= ILI9806_FastMonitor
};


// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 	drv_ILI9806_API_STD 	= 
{
 	.Init			=	ILI9806_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	ILI9806_Enable,
	.Disable		=	ILI9806_Disable,
	.Write_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Ext			=	(_drv_Api_t *) &ILI9806_Ext_API,
};
#pragma pop

sys_Buffer_t* 					Buff_ILI9806_Regs;
#define ILI9806_REGS 			((uint8_t *)(Buff_ILI9806_Regs->pDataArray))


// ******************************************************************************************************
// Functions prototypes
// ******************************************************************************************************




// ******************************************************************************************************
// Private Functions
// ******************************************************************************************************


static uint32_t ILI9806_read(_drv_If_t *pDrvIf, uint8_t SrcReg, sys_Buffer_t *pBuff, uint8_t RegsCount)
{
	drv_ILI9806_If_Spec_t* pDrvIf_Spec = (drv_ILI9806_If_Spec_t*) pDrvIf->pDrvSpecific;
	
	sys_Buffer_Set_pWr_Pos( pBuff, SrcReg);											// set start position for pbuff
	
	if(pDrvIf_Spec->pCallBack_ReadData) 
	{
		return(pDrvIf_Spec->pCallBack_ReadData(pDrvIf, (uint8_t) SrcReg, pBuff, RegsCount)?EC_SUCCESS:EC_ERROR_UNIMPLEMENTED);
	}
	else return(EC_ERROR_UNIMPLEMENTED);
}

static uint32_t ILI9806_write(_drv_If_t *pDrvIf, uint8_t DestReg, sys_Buffer_t *pBuff, uint8_t RegsCount)
{
	drv_ILI9806_If_Spec_t* pDrvIf_Spec = (drv_ILI9806_If_Spec_t*) pDrvIf->pDrvSpecific;
	
	sys_Buffer_Set_pRd_Pos( pBuff, DestReg);										// set start position for pbuff
	
	if(pDrvIf_Spec->pCallBack_WriteData) pDrvIf_Spec->pCallBack_WriteData(pDrvIf, DestReg, pBuff, RegsCount);
	else return(EC_ERROR_UNIMPLEMENTED);
	return(EC_SUCCESS);
}


//int ILI9806_device_id(_drv_If_t *pDrvIf, sys_Buffer_t *id)
//{
//	int res = ILI9806_read(pDrvIf, ILI9806_REG_ID, id, 1);
//	if (res == EC_SUCCESS)
//		*id->pRd &= ILI9806_DEVICE_ID_MASK;
//	return res;
//}
















//// ------------------------------------------------------------------------------------------------
//// Decode Battery voltage from mirrors regs
//uint32_t Decode_Batt_Voltage(_drv_If_t *pDrvIf)
//{
//	uint32_t Res=0;

//	Res = ((ILI9806_REGS[ILI9806_REG_ADC_BATT_VOLT] & 0x7f) * ILI9806_BATV_MVPERBIT) + ILI9806_BATV_OFFSET;
//	return (Res);
//}


// ------------------------------------------------------------------------------------------------
// Read all charger regs 
void ILI9806_MirrorRegs(_drv_If_t *pDrvIf)
{
//	Buff_ILI9806_Regs[ILI9806_REG_CFG1] |= ILI9806_CFG1_CONV_RATE;					// Start one-shot ADC
//	ILI9806_write(pDrvIf, ILI9806_REG_CFG1, &Buff_ILI9806_Regs[ILI9806_REG_CFG1], 1);	// write to dev

//	ILI9806_REGS[ILI9806_REG_CFG2] |= ILI9806_CFG2_WD_RST;							// reset WD
	
//	ILI9806_write(pDrvIf, 0, Buff_ILI9806_Regs, 1);					// write from position to register
	
//	do
//	{
//		ILI9806_read(pDrvIf, ILI9806_REG_CFG1, &Buff_ILI9806_Regs[ILI9806_REG_CFG1], 1);
//	}while(Buff_ILI9806_Regs[ILI9806_REG_CFG1] & 0x80);								// wait for ADC finish
	
	ILI9806_read(pDrvIf, 0x00, Buff_ILI9806_Regs, ILI9806_REG_COUNT);				// reread all regs
}


// ------------------------------------------------------------------------------------------------
// Read only ADC results registers and reset WD
//void ILI9806_FastMonitor(_drv_If_t *pDrvIf)
//{
//	ILI9806_REGS[ILI9806_REG_CFG2] |= ILI9806_CFG2_WD_RST;							// reset WD

//	ILI9806_write(pDrvIf, ILI9806_REG_CFG2, Buff_ILI9806_Regs, 1);					// write from position to register

//	//ILI9806_read(pDrvIf, 0x0b, &Buff_ILI9806_Regs[0x0b], 0x09);					// reload only ADC regs
//	
//	ILI9806_read(pDrvIf, 0x00, Buff_ILI9806_Regs, ILI9806_REG_COUNT);
//}




static bool ILI9806_Preset(_drv_If_t *pDrvIf)
{
//	uint8_t Reg_Val;
	bool res = false;
	
	sys_Buffer_t* onebyteBuff = sys_Buffer_Create(0x1, 1, sBuffLinear);				// create one byte buffer
	
//	// exit from shipping mode
//	CHAL_Signal_Set_Pin(pDrvIf_Spec->pPinQON->Std);									// Set QON to Low - Active is in Log 0 !!!
//	Delay_ms(1000);
//	CHAL_Signal_Clear_Pin(pDrvIf_Spec->pPinQON->Std);								// Set QON to High - Active is in Log 0 !!!
	
	//sys_Buffer_Fill(Buff_ILI9806_Regs, 0);											// Clear buff
	
	
	((drv_ILI9806_If_Spec_t*) pDrvIf->pDrvSpecific)->pMirrorRegs = Buff_ILI9806_Regs->pDataArray;
	
//	ILI9806_device_id(pDrvIf, onebyteBuff);											// Read Device ID
	
//	if (ILI9806_device_id(pDrvIf, onebyteBuff) != EC_SUCCESS) return(false);		// read error
//	if(*onebyteBuff->pRd != ILI9806_DEVICE_ID) return(false);						// wrong Device ID

	ILI9806_read(pDrvIf, 0x00, Buff_ILI9806_Regs, ILI9806_REG_COUNT);				// Reload All registers from Device to local Mirror


// Initial config write:
	// REG00: ILI9806_REG_INPUT_CURR
//	ILI9806_REGS[ILI9806_REG_INPUT_CURR] = 0;											// clear all
//	
//#if defined(CONFIG_CHARGER_HIZ_MODE_ENABLED) && (CONFIG_CHARGER_HIZ_MODE_ENABLED == 1)
//	ILI9806_REGS[ILI9806_REG_INPUT_CURR] |= ILI9806_INPUT_CURR_EN_HIZ;					// Enter HiZ mode (USB 1.00)
//#endif	
//#if defined(CONFIG_CHARGER_ILIM_PIN_ENABLED) && (CONFIG_CHARGER_ILIM_PIN_ENABLED == 1)
//	ILI9806_REGS[ILI9806_REG_INPUT_CURR] |= ILI9806_INPUT_CURR_EN_ILIM;					// ILIM setting by ILIM pin
//#else	
//	ILI9806_REGS[ILI9806_REG_INPUT_CURR] |= ((((uint32_t) CONFIG_CHARGER_INPUT_CMAX - ILI9806_ILIM_OFFSET)/ILI9806_ILIM_MINSTEP)) & 0x3f;
//#endif

//	// REG01: ILI9806_REG_VINDPM
//	
//	// REG02: ILI9806_REG_CFG1
//	ILI9806_REGS[ILI9806_REG_CFG1] = 0;													// clear all
//	ILI9806_REGS[ILI9806_REG_CFG1] |= ILI9806_CFG1_CONV_RATE;							// auto adc
//	ILI9806_REGS[ILI9806_REG_CFG1] |= ILI9806_CFG1_ICO_EN;								// Input current optimizer
//	ILI9806_REGS[ILI9806_REG_CFG1] |= (1 << 5);											// 500kHz BOOSTER
//	
//	// REG03: ILI9806_REG_CFG2
//	ILI9806_REGS[ILI9806_REG_CFG2] = 0;													// clear all
//	ILI9806_REGS[ILI9806_REG_CFG2] |= ILI9806_CFG2_WD_RST;								// WatchDog Reset
//	ILI9806_REGS[ILI9806_REG_CFG2] |= ILI9806_CFG2_CHG_CONFIG;							// Charge Enable
//	ILI9806_REGS[ILI9806_REG_CFG2] |= ((uint32_t)(CONFIG_CHARGER_SYS_VMIN - ILI9806_SYSMIN_OFFSET) / 100) << 1; // set Minimum SYS limit


//	// REG04: ILI9806_REG_PRE_CHG_CURR	- Fast Charge Current Limit
//	ILI9806_REGS[ILI9806_REG_PRE_CHG_CURR] = 0;											// clear all
//	ILI9806_REGS[ILI9806_REG_PRE_CHG_CURR] = ((uint32_t) CONFIG_BATT_CHARGE_CMAX / ILI9806_CHARGE_CMAX_MINSTEP) & 0x7f;
//	
//	// REG05: ILI9806_REG_CHG_CURR	- Precharge Current Limit, Termination Current Limit
//	ILI9806_REGS[ILI9806_REG_CHG_CURR] = 0;												// clear all
//	ILI9806_REGS[ILI9806_REG_CHG_CURR] |= (((uint32_t) (CONFIG_BATT_PRECHARGE_C - ILI9806_PRECHARGE_C_OFFSET) / ILI9806_PRECHARGE_C_MINSTEP) << 4) & 0xf0;
//	ILI9806_REGS[ILI9806_REG_CHG_CURR] |= ((uint32_t) (CONFIG_BATT_TERMINATION_C - ILI9806_TERMINATION_C_OFFSET) / ILI9806_TERMINATION_C_MINSTEP) & 0x0f;
//	
//	// REG06: ILI9806_REG_CHG_VOLT	- Charge Voltage Limit, Battery Precharge to Fast Charge Threshold, Battery Recharge Threshold Offset
//	ILI9806_REGS[ILI9806_REG_CHG_VOLT] = 0;												// clear all
//	ILI9806_REGS[ILI9806_REG_CHG_VOLT] |= (((uint32_t) (CONFIG_BATT_CHARGE_VMAX - ILI9806_CHARGE_V_OFFSET) / ILI9806_CHARGE_V_MINSTEP) << 2) & 0xfc;
//	ILI9806_REGS[ILI9806_REG_CHG_VOLT] |= ILI9806_CHG_VOLT_BATLOWV;						// Battery Precharge to Fast Charge Threshold: 3.0V
//	
//	// REG07: ILI9806_REG_TIMER
//	ILI9806_REGS[ILI9806_REG_TIMER] = 0;													// clear all
//	ILI9806_REGS[ILI9806_REG_TIMER] |= ILI9806_TIMER_TERM_EN;							// Charging Termination Enable
//	ILI9806_REGS[ILI9806_REG_TIMER] |= ILI9806_TIMER_PINSTAT_DIS;						// STAT Pin Disable
//	ILI9806_REGS[ILI9806_REG_TIMER] |= (CONFIG_CHARGER_I2C_WD << 4) & 0x30;				// I2C Watch dog settings
////	ILI9806_REGS[ILI9806_REG_TIMER] |= ILI9806_TIMER_CH_SAFETIMER_EN;					// Charging Safety Timer Enable
//	ILI9806_REGS[ILI9806_REG_TIMER] |= (CONFIG_CHARGER_MAXTIME << 1) & 0x06;				// FASTCharge max time 5-20 hrs
//	ILI9806_REGS[ILI9806_REG_TIMER] |= ILI9806_TIMER_JEITA_ISET;							// JEITA Low Temperature Current Setting
//	
//	// REG08: ILI9806_REG_IR_COMP
//	ILI9806_REGS[ILI9806_REG_IR_COMP] = 0;												// clear all
//	ILI9806_REGS[ILI9806_REG_IR_COMP] |= ILI9806_IR_BAT_COMP_0MOHM;						// IR Compensation Resistor Setting
//	ILI9806_REGS[ILI9806_REG_IR_COMP] |= ILI9806_IR_VCLAMP_0MV;							// IR Compensation Voltage Clamp
//	ILI9806_REGS[ILI9806_REG_IR_COMP] |= ILI9806_IR_TREG_80C;							// Thermal Regulation Threshold

//	// REG09: ILI9806_REG_FORCE
//	ILI9806_REGS[ILI9806_REG_FORCE] = 0;//~(1 << 2);										// Disable BATFET full system reset
//	ILI9806_REGS[ILI9806_REG_FORCE] |= 0x80;

//	// REG0A: ILI9806_REG_BOOST_MODE
//	// nothing to set

//	ILI9806_write(pDrvIf, 0, Buff_ILI9806_Regs, ILI9806_REG_BOOST_MODE + 1);		// write to dev from start position

//	// REG0B: ILI9806_REG_STATUS
//		// READ - ONLY

//	// REG0C: ILI9806_REG_FAULT
//		// READ - ONLY
//		
//	// REG0D: ILI9806_REG_VINDPM_THRESH
//		// nothing to set

//	// REG0E: ILI9806_REG_ADC_BATT_VOLT
//		// READ - ONLY

//	// REG0F: ILI9806_REG_ADC_SYS_VOLT
//		// READ - ONLY
//	
//	// REG10: ILI9806_REG_ADC_TS
//		// READ - ONLY

//	// REG11: ILI9806_REG_ADC_VBUS_VOLT
//		// READ - ONLY

//	// REG12: ILI9806_REG_ADC_CHG_CURR
//		// READ - ONLY

//	// REG13: ILI9806_REG_ADC_INPUT_CURR
//		// READ - ONLY
//	Delay_ms(500);
//	ILI9806_read(pDrvIf, 0x00, Buff_ILI9806_Regs, ILI9806_REG_COUNT);				// read to start position
	
	// ((drv_ILI9806_If_Spec_t*) pDrvIf->pDrvSpecific)->pCharger_Params = &Charger_Params;
	
	res = true;
	return(res);
//	CPRINTF("BQ2589%c initialized\n", ILI9806_DEVICE_ID == BQ25890_DEVICE_ID ? '0' : (ILI9806_DEVICE_ID == BQ25895_DEVICE_ID ? '5' : '2'));
}



// ******************************************************************************************************
// USR Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// USR
// Inicialization of the BQ2589x-u
// Remark! Set Signal Active to Log. 1 !!!
bool ILI9806_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;	//0
	
	drv_ILI9806_If_Spec_t* pDrvIf_Spec = (drv_ILI9806_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_BQ2589X) && (CONF_DEBUG_DRV_BQ2589X == 1)
	dbgprint("\r\n("DRV_ILI9806_OBJECTNAME") ID[%s]: Init", pDrvIf->Name);
#endif		

	// initialization code here:
	//if( pDrvIf_Spec->pPinPSEL != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pPinPSEL);

	Buff_ILI9806_Regs = sys_Buffer_Create(ILI9806_REG_COUNT, 1, sBuffLinear);		// initialize Buff_ILI9806_Regs mirror
	
	
	
//	i80_ctrl_command(ili9806, 0xFF, 0xFF, 0x98, 0x06);
//	i80_ctrl_command(ili9806, 0xBA, 0xE0); 											// SPI mode SDA
//	i80_ctrl_command(ili9806, 0xBC, 0x03, 0x0F, 0x63, 0x69, 0x01, 0x01, 0x1B, 0x11,
//		0x70, 0x73, 0xFF,0xFF, 0x08, 0x09, 0x05, 0x00, 0xEE, 0xE2, 0x01, 0x00, 0xC1);

//	i80_ctrl_command(ili9806, 0xBD, 0x01, 0x23, 0x45, 0x67, 0x01, 0x23, 0x45, 0x67);
//	i80_ctrl_command(ili9806, 0xBE, 0x00, 0x22, 0x27, 0x6A, 0xBC, 0xD8, 0x92, 0x22, 0x22);

//	i80_ctrl_command(ili9806, 0xC7, 0x1E); 											// VCOM Control 1
//	i80_ctrl_command(ili9806, 0xED, 0x7F, 0x0F, 0x00);

//	i80_ctrl_command(ili9806, 0xC0, 0xE3, 0x0B, 0x00); 								// Power Control 1
//	i80_ctrl_command(ili9806, 0xFC, 0x08); 											// LVGL Voltage Setting
//	i80_ctrl_command(ili9806, 0xDF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02); 			// Engineering Setting
//	i80_ctrl_command(ili9806, 0xF3, 0x74); 											// DVDD Voltage Setting
//	i80_ctrl_command(ili9806, 0xB4, 0x00, 0x00, 0x00); 								// Display Inversion Control
//	i80_ctrl_command(ili9806, 0xF7, 0x81); 											// Panel Resolution Selection Set 480x854 resolution
//	i80_ctrl_command(ili9806, 0xB1, 0x00, 0x10, 0x14);								// Frame Rate Control 1
//	i80_ctrl_command(ili9806, 0xF1, 0x29, 0x8A, 0x07); 								// Panel Timing Control 1
//	i80_ctrl_command(ili9806, 0xF2, 0x40, 0xD2, 0x50, 0x28);						// Panel Timing Control 2
//	i80_ctrl_command(ili9806, 0xC1, 0x17, 0X85, 0x85, 0x20); 						// Power Control 2
//	i80_ctrl_command(ili9806, 0xE0, 0x00, 0x0C, 0x15, 0x0D, 0x0F, 0x0C, 0x07, 0x05, 0x07,
// 			0x0B, 0x10, 0x10, 0x0D, 0x17, 0x0F, 0x00);  							// Positive Gamma Control
//	i80_ctrl_command(ili9806, 0xE1, 0x00, 0x0D, 0x15, 0x0E, 0x10, 0x0D, 0x08, 0x06, 0x07,
//			0x0C, 0x11, 0x11, 0x0E, 0x17, 0x0F, 0x00);  							// Negative Gamma Correction
//	i80_ctrl_command(ili9806, 0x35, 0x00); 											// Tearing Effect Line ON

//	//i80_ctrl_command(ili9806, 0x36, 0x60);

//	i80_ctrl_command(ili9806, 0x36, 0x22);
//	i80_ctrl_command(ili9806, 0x37, 0x00, 0x00);

//	i80_ctrl_command(ili9806, 0x2A, 0x00, 0x00, 0x03, 0x55);
//	i80_ctrl_command(ili9806, 0x2B, 0x00, 0x00, 0x01, 0xDF);

//	i80_ctrl_command(ili9806, 0x3A, 0x77);

//	i80_ctrl_command(ili9806, 0x11);												// Exit Sleep 
//        msleep(120);
//	i80_ctrl_command(ili9806, 0x29);												// Display On 

//	//tinydrm_ili9806_set_rotation(ili9806);
//	//tinydrm_ili9806_set_gamma(ili9806, gamma_curves);
//	ili9806->enabled = true;
	
	
	
	res = true;
	pDrvIf->Sys.Stat.Initialized = Buff_ILI9806_Regs->Ready;						// if buff is ready - all is OK
	
#if defined(CONF_DEBUG_DRV_BQ2589X) && (CONF_DEBUG_DRV_BQ2589X == 1)
	dbgprint("\r\n("DRV_ILI9806_OBJECTNAME") ID[%s]: Init Done.", pDrvIf->Name);
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// USR
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool ILI9806_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	//drv_ILI9806_If_Spec_t* pDrvIf_Spec = (drv_ILI9806_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_BQ2589X) && (CONF_DEBUG_DRV_BQ2589X == 1)
	dbgprint("\r\n("DRV_ILI9806_OBJECTNAME") ID[%s]: Set Enable CH:0x%08x", pDrvIf->Name, ChannelBitMask);
#endif

	// main code here:
	pDrvIf->Sys.Stat.Enabled = true;												// enable was succesfull
	res = true;
#if defined(CONF_DEBUG_DRV_BQ2589X) && (CONF_DEBUG_DRV_BQ2589X == 1)
	dbgprint("\r\n("DRV_ILI9806_OBJECTNAME") ID[%s]: Set Enable Done.", pDrvIf->Name);
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// USR
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool ILI9806_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	//drv_ILI9806_If_Spec_t* pDrvIf_Spec = (drv_ILI9806_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_BQ2589X) && (CONF_DEBUG_DRV_BQ2589X == 1)
	dbgprint("\r\n("DRV_ILI9806_OBJECTNAME") ID[%s]: Set Disable CH:0x%08x", pDrvIf->Name, ChannelBitMask);
#endif

	// main code here:
	pDrvIf->Sys.Stat.Enabled = false;												// disable was succesfull
	res = true;
	
#if defined(CONF_DEBUG_DRV_BQ2589X) && (CONF_DEBUG_DRV_BQ2589X == 1)
	dbgprint("\r\n("DRV_ILI9806_OBJECTNAME") ID[%s]: Set Disable Done.", pDrvIf->Name);
#endif	
	return(res);
}


// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// no interrupts in this driver

// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************

// no interrupts in this driver


// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
inline void ILI9806_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{
//	switch(EventType)
//	{
//		case EV_DataReceived:
//		case EV_TransmitReady:
//		case EV_TransmitDone:	
//		case EV_NoChange:
//		default: break;
//	}
}

// ------------------------------------------------------------------------------------------------------
// EXT


#endif	// USE_DRV_ILI9806
