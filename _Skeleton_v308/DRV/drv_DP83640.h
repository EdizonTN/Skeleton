// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_DP83640.h
// 	   Version: 1.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Driver for DP83640 IC - Header file
// ******************************************************************************
// Usage: 
// 
// ToDo: 
// 
// Changelog:
// 

#ifndef __DRV_DP83640_H_
#define __DRV_DP83640_H_

#include "Skeleton.h"


// ************************************************************************************************
// CONFIGURATION - connection to BSP
// ************************************************************************************************

#ifndef CONF_DEBUG_DRV_DP83640
#define CONF_DEBUG_DRV_DP83640				0										// default is OFF
#endif	


// ************************************************************************************************
// CONST for DP83640:

// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************
// Device specific control structure

// Device extended functions:
COMP_PACKED_BEGIN
typedef struct drv_DP83640_Ext_API
{
			bool					tmp;
//	size_t	(*Read_MemTotalSize)	( _drv_If_t *pDrvIf);							// Get Total Memory Size
//	size_t	(*Read_MemFreeSize)		( _drv_If_t *pDrvIf);							// Get Free Memory Size
	bool							(*Chip_Init)( _drv_If_t *pDrvIf);				// Chip initialization (int. registers,...)
} drv_DP83640_Ext_API_t;
COMP_PACKED_END


// Device specific structure
COMP_PACKED_BEGIN
typedef struct drv_DP83640_If_Spec
{
	const 	CHAL_Signal_t			*pSig_RST;										// DP83640 Input: Reset Input.Active Low
	
	const 	CHAL_Signal_t			*pSig_GPIO1;									// DP83640 I/O: GPIO1
	const 	CHAL_Signal_t			*pSig_GPIO2;									// DP83640 I/O: GPIO2
	const 	CHAL_Signal_t			*pSig_GPIO3;									// DP83640 I/O: GPIO3
	const 	CHAL_Signal_t			*pSig_GPIO4;									// DP83640 I/O: GPIO4
	const 	CHAL_Signal_t			*pSig_GPIO5;									// DP83640 I/O: GPIO5
	const 	CHAL_Signal_t			*pSig_GPIO6;									// DP83640 I/O: GPIO6
	const 	CHAL_Signal_t			*pSig_GPIO7;									// DP83640 I/O: GPIO7
	const 	CHAL_Signal_t			*pSig_GPIO8;									// DP83640 I/O: GPIO8
	const 	CHAL_Signal_t			*pSig_GPIO9;									// DP83640 I/O: GPIO9
	const 	CHAL_Signal_t			*pSig_GPIO10;									// DP83640 I/O: GPIO10
	const 	CHAL_Signal_t			*pSig_GPIO11;									// DP83640 I/O: GPIO11
	const 	CHAL_Signal_t			*pSig_CLK_IEEE;									// DP83640 I/O: CLKOUT/IEEECLK
	const 	CHAL_Signal_t			*pSig_PWDN_IRQ;									// DP83640 Input: PowerDown, Output - Ethernet interrupt
	
//	const 	CHAL_Signal_t			*pSig_TXD0;										// DP83640 Input: TXD0
//	const 	CHAL_Signal_t			*pSig_TXD1;										// DP83640 Input: TXD1
	const 	CHAL_Signal_t			*pSig_TXD2;										// DP83640 Input: TXD2
	const 	CHAL_Signal_t			*pSig_TXD3;										// DP83640 Input: TXD3
	const 	CHAL_Signal_t			*pSig_TXCLK;									// DP83640 Output: TXCLK
//	const 	CHAL_Signal_t			*pSig_TXEN;										// DP83640 Input: TXEN
	const 	CHAL_Signal_t			*pSig_TXER;										// DP83640 Output: TXER
	
//	const 	CHAL_Signal_t			*pSig_RXD0;										// DP83640 Input: RXD0
//	const 	CHAL_Signal_t			*pSig_RXD1;										// DP83640 Input: RXD1
	const 	CHAL_Signal_t			*pSig_RXD2;										// DP83640 Input: RXD2
	const 	CHAL_Signal_t			*pSig_RXD3;										// DP83640 Input: RXD3
	const 	CHAL_Signal_t			*pSig_RXCLK;									// DP83640 Output: RXCLK
//	const 	CHAL_Signal_t			*pSig_RXER;										// DP83640 Output: RXER
	const 	CHAL_Signal_t			*pSig_COL;										// DP83640 Output: COL

	_drv_If_t				*pParentInterface;								// ptr to master interface
//			ETH_Device_t			ETH_Device;	
} drv_DP83640_If_Spec_t;
COMP_PACKED_END

// ************************************************************************************************
// PRIVATE
// ************************************************************************************************



// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const _drv_Api_t drv_DP83640_API_STD;

#endif		// __DRV_DP83640_H_
