// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_DAC.h
// 	   Version: 3.0
//		  Desc: Generic Driver for DAC - header file
// ******************************************************************************
// Info: LCD driver. 
//
// Notice:
//
// Usage:
//			
// ToDo:	--- nerdokoncene. len vytvorena kostra !!!!
// 			
// Changelog:
// 

#if !defined(__DRV_DAC_H_) && (defined (_CHIP_HAVE_DAC) && _CHIP_HAVE_DAC == 1)
#define __DRV_DAC_H_

#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************

#ifndef CONF_DEBUG_DRV_DAC
#define CONF_DEBUG_DRV_DAC				0											// default je vypnuty
#endif	

// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************

// Extended API functions for this driver:
//COMP_PACKED_BEGIN
//typedef struct drv_LCD_Ext_API
//{
//	bool	(*SomeFunction)	( _drv_If_t *pDrvIf);									// Some function
//} drv_LCD_Ext_API_t;
//COMP_PACKED_END

// Additions for Generic driver structure here:
COMP_PACKED_BEGIN
typedef struct drv_DAC_If_Spec
{
	void								*pPeri;										// pointer for used periphery
	const 	uint8_t						PeriIndex;									// index of used periphery
			uint32_t					Ch_Active_Mask[2];							// active channels mask for each sequence
			uint8_t						Ch_Active;									// active channel count
	const	uint8_t						DMA_XFER_CH;								// DMA xfer channel num
//	volatile void						*pDstBuffer;								// pointer to dest buffer
} drv_DAC_If_Spec_t;
COMP_PACKED_END


// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const _drv_Api_t drv_DAC_API_STD;
extern void drv_DAC_Handler_Top(_drv_If_t *pDrvIf);
extern void drv_DAC_Handler_Bottom(_drv_If_t *pDrvIf);

#endif		// __DRV_DAC_H_
