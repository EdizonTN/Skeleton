// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_HX711.c
// 	   Version: 1.01
//		  Desc: Driver for HX711 IC - strain gauge ad converter
// ******************************************************************************
// Info: SPI driver. 
//			based on: https://github.com/dudapickler/hx711_SPI/blob/master/HX711_SPI.c
// Notice:
//
// Usage:
//			
// ToDo:	
//
// Changelog:
//				2024.09.30	- v 1.01 - add sys_NULL_Function instead a NULL pointer to a function in API structure

#include "Skeleton.h"

#if defined(USE_DRV_HX711) && (USE_DRV_HX711 == 1)

// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool HX711_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool HX711_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool HX711_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);

void HX711_MirrorRegs(_drv_If_t *pDrvIf);

// ******************************************************************************************************
// Public Function - specific for this driver
// ******************************************************************************************************
const drv_HX711_Ext_API_t	HX711_Ext_API 	=
{
	.Reload_MirrorRegs					= HX711_MirrorRegs,
};


// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 	drv_HX711_API_STD 	= 
{
 	.Init			=	HX711_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	HX711_Enable,
	.Disable		=	HX711_Disable,
	.Write_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Ext			=	(_drv_Api_t *) &HX711_Ext_API,
};
#pragma pop


sys_Buffer_t* 					Buff_HX711_Regs;
#define HX711_REGS 			((uint8_t *)(Buff_HX711_Regs->pDataArray))


// ******************************************************************************************************
// Functions prototypes
// ******************************************************************************************************




// ******************************************************************************************************
// Private Functions
// ******************************************************************************************************


//static uint32_t HX711_read(_drv_If_t *pDrvIf, uint8_t SrcReg, sys_Buffer_t *pBuff, uint8_t RegsCount)
//{
//	drv_HX711_If_Spec_t* pDrvIf_Spec = (drv_HX711_If_Spec_t*) pDrvIf->pDrvSpecific;
//	
//	sys_Buffer_Set_pWr_Pos( pBuff, SrcReg);											// set start position for pbuff
//	
//	if(pDrvIf_Spec->pCallBack_ReadData) 
//	{
//		return(pDrvIf_Spec->pCallBack_ReadData(pDrvIf, (uint8_t) SrcReg, pBuff, RegsCount)?EC_SUCCESS:EC_ERROR_UNIMPLEMENTED);
//	}
//	else return(EC_ERROR_UNIMPLEMENTED);
//}

//static uint32_t HX711_write(_drv_If_t *pDrvIf, uint8_t DestReg, sys_Buffer_t *pBuff, uint8_t RegsCount)
//{
//	drv_HX711_If_Spec_t* pDrvIf_Spec = (drv_HX711_If_Spec_t*) pDrvIf->pDrvSpecific;
//	
//	sys_Buffer_Set_pRd_Pos( pBuff, DestReg);										// set start position for pbuff
//	
//	if(pDrvIf_Spec->pCallBack_WriteData) pDrvIf_Spec->pCallBack_WriteData(pDrvIf, DestReg, pBuff, RegsCount);
//	else return(EC_ERROR_UNIMPLEMENTED);
//	return(EC_SUCCESS);
//}












// ------------------------------------------------------------------------------------------------
// Read all charger regs 
void HX711_MirrorRegs(_drv_If_t *pDrvIf)
{
//	Buff_HX711_Regs[HX711_REG_CFG1] |= HX711_CFG1_CONV_RATE;					// Start one-shot ADC
//	HX711_write(pDrvIf, HX711_REG_CFG1, &Buff_HX711_Regs[HX711_REG_CFG1], 1);	// write to dev

//	HX711_REGS[HX711_REG_CFG2] |= HX711_CFG2_WD_RST;							// reset WD
	
//	HX711_write(pDrvIf, 0, Buff_HX711_Regs, 1);					// write from position to register
	
//	do
//	{
//		HX711_read(pDrvIf, HX711_REG_CFG1, &Buff_HX711_Regs[HX711_REG_CFG1], 1);
//	}while(Buff_HX711_Regs[HX711_REG_CFG1] & 0x80);								// wait for ADC finish
	
//	HX711_read(pDrvIf, 0x00, Buff_HX711_Regs, HX711_REG_COUNT);				// reread all regs
}







// ******************************************************************************************************
// USR Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// USR
// Inicialization of the BQ2589x-u
// Remark! Set Signal Active to Log. 1 !!!
bool HX711_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;	//0
	
	//drv_HX711_If_Spec_t* pDrvIf_Spec = (drv_HX711_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_HX711) && (CONF_DEBUG_DRV_HX711 == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> ParentMod: %s", pRequestorModIf->Name);
#endif			

	// initialization code here:
	Buff_HX711_Regs = sys_Buffer_Create(HX711_REG_COUNT, 1, sBuffLinear);		// initialize Buff_HX711_Regs mirror
	res = true;
	pDrvIf->Sys.Stat.Initialized = Buff_HX711_Regs->Ready;						// if buff is ready - all is OK
	
#if defined(CONF_DEBUG_DRV_BQ2589X) && (CONF_DEBUG_DRV_BQ2589X == 1)
	dbgprint("\r\n("DRV_HX711_OBJECTNAME") ID[%s]: Init Done.", pDrvIf->Name);
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// USR
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool HX711_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	//drv_HX711_If_Spec_t* pDrvIf_Spec = (drv_HX711_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_HX711) && (CONF_DEBUG_DRV_HX711 == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> ParentMod: %s", pRequestorModIf->Name);
#endif	

	// main code here:
	pDrvIf->Sys.Stat.Enabled = true;												// enable was succesfull
	res = true;

#if defined(CONF_DEBUG_DRV_HX711) && (CONF_DEBUG_DRV_HX711 == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_RESULT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// USR
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool HX711_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
#if defined(CONF_DEBUG_DRV_HX711) && (CONF_DEBUG_DRV_HX711 == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> ParentMod: %s", pRequestorModIf->Name);
#endif		

	// main code here:
	pDrvIf->Sys.Stat.Enabled = false;												// disable was succesfull
	res = true;
	
#if defined(CONF_DEBUG_DRV_HX711) && (CONF_DEBUG_DRV_HX711 == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_RESULT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// no interrupts in this driver

// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************

// no interrupts in this driver


// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
inline void HX711_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{
//#if defined(CONF_IDIOTPROOF) && (CONF_IDIOTPROOF == 1)
//	SYS_ASSERT( pDrvIf != NULL);													// check
//#endif
//	
//	void* pCallBack = NULL;		
//	_ColListRecord_t *tmpListRecord = pDrvIf->Sys.ParentModLinked.pFirst;			// temporary pointer to parent module list
//	while(tmpListRecord)
//	{
//		switch(EventType)
//		{
//			case EV_StateChanged:
//			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_ChangedState;
//				break;
//			}
//			case EV_DataReceived:
//			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataReceived;
//				break;
//			}
//			case EV_Transmitted:
//			{
////				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataTransmitted;
//				break;
//			}
//			default: break;
//		}
//		
//		if(pCallBack)																// callback is set?
//		{
//			sys_Task_Create_Item( pDrvIf->Name, pCallBack, (sys_TaskArgs_t) {NULL, 0});	// create task for data read
//		}

//		if(tmpListRecord->pNext == NULL) break;										// loop while have associadet module
//		tmpListRecord = tmpListRecord->pNext;										// goto next associated module
//	}
}

// ------------------------------------------------------------------------------------------------------
// EXT


#endif	// USE_DRV_HX711
