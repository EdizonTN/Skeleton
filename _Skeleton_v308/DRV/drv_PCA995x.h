// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_PCA995x.h
// 	   Version: 1.01
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: User Driver for PCA995x - header file
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//				2024.03.03	-	v1.01	- add retry count const CONF_DRV_PCA995X_RETRY_COUNT for Read/Write operation if any error occured
//				2023.10.24	-	v1.00	- first working version

#if !defined(__DRV_PCA995X_H_) 														// prevent to cyclyc loads
 #if defined(USE_DRV_PCA995X) && (USE_DRV_PCA995X == 1)								// Need to load?
 // now -> load it.
#define __DRV_PCA995X_H_

#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************
#ifndef CONF_DRV_PCA995X_RETRY_COUNT
#define CONF_DRV_PCA995X_RETRY_COUNT			3										// read/write retry counter if not ack or other error occured
#endif				


// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************

typedef struct
{
	uint8_t Sel0: 2;
	uint8_t Sel1: 2;
	uint8_t Sel2: 2;
	uint8_t Sel3: 2;
	uint8_t Sel4: 2;
	uint8_t Sel5: 2;
	uint8_t Sel6: 2;
	uint8_t Sel7: 2;
	uint8_t Sel8: 2;
	uint8_t Sel9: 2;
	uint8_t Sel10: 2;
	uint8_t Sel11: 2;
	uint8_t Sel12: 2;
	uint8_t Sel13: 2;
	uint8_t Sel14: 2;
	uint8_t Sel15: 2;
} SBits_t;

	
typedef union
{
	uint32_t	Mem32;
	uint8_t		Array8[4];
	SBits_t		BitsOut;
}drv_PCA995x_LEDError_t;



// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_PCA995x_If_Spec
{
	uint8_t									PWM_RegsW[16];							// PWM Regs
	drv_PCA995x_LEDError_t					ErrFlag;								// Error flags
	const 	CHAL_Signal_t					*pSig_RST;								// reset is one for all connected devices
	_drv_If_t 								*pRootDrvIf;							// ptr to Owners driver struct
	I2C_Device_t							*pI2C_PCA995x_Device;					// connected devices
} drv_PCA995x_If_Spec_t;
COMP_PACKED_END


COMP_PACKED_BEGIN
typedef struct drv_PCA995x_Ext_API
{
	void		(*Reset)					( drv_PCA995x_If_Spec_t *pDrvIf_Spec);					// Reset chip
	bool		(*Init)						(_drv_If_t *pDrvIf, I2C_Device_t *pDevice, uint8_t ModeReg[2]);	
	uint32_t	(*Read)						( I2C_Device_t *pDevice, uint8_t SrcReg, sys_Buffer_t *Dst, uint8_t DstInc, uint8_t DataCount);	// read data
	uint32_t	(*Write)					( I2C_Device_t *pDevice, uint8_t DstReg, sys_Buffer_t *Src, uint8_t SrcInc, uint8_t DataCount);	// write data
	bool		(*Is_Error)					( I2C_Device_t *pDevice, drv_PCA995x_LEDError_t *ErrFlagRegs);		// check for errors and read error regs if any
} drv_PCA995x_Ext_API_t;
COMP_PACKED_END

// ************************************************************************************************
// PRIVATE
// ************************************************************************************************



// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
#define PCA995X_MODE1_AIF					1 << 7									// Read only - Register Auto-Increment
#define PCA995X_MODE1_AI1					1 << 6									// R/W		 - Auto-Increment bit 1
#define PCA995X_MODE1_AI0					1 << 5									// R/W		 - Auto-Increment bit 0
#define PCA995X_MODE1_SLEEP					1 << 4									// R/W		 - Low power mode. Oscillator off
#define PCA995X_MODE1_SUB1					1 << 3									// R/W		 - PCA9955B responds to I2C-bus subaddress 1
#define PCA995X_MODE1_SUB2					1 << 2									// R/W		 - PCA9955B responds to I2C-bus subaddress 2
#define PCA995X_MODE1_SUB3					1 << 1									// R/W		 - PCA9955B responds to I2C-bus subaddress 3
#define PCA995X_MODE1_ALLCALL				1 << 0									// R/W		 - PCA9955B responds to LED All Call I2C-bus address.

#define PCA995X_MODE2_OVERTEMP				1 << 7									// Read only - overtemperature condition
#define PCA995X_MODE2_ERROR					1 << 6									// Read only - any open or short-circuit detected inerror flag registers (EFLAGn)
#define PCA995X_MODE2_DMBLNK				1 << 5									// R/W		 - overtemperature condition
#define PCA995X_MODE2_CLRERR				1 << 4									// WriteOnly - Write �1� to clear all error status bits in EFLAGn register and ERROR (bit 6). The EFLAGn and ERROR bit sets to �1� if open or short-circuit is detected again.
#define PCA995X_MODE2_OCH					1 << 3									// R/W		 - outputs change on 1:ACK, 0:STOP
#define PCA995X_MODE2_EXP_EN				1 << 2									// R/W		 - 0:Linear brightness, 1: exponencial brightness

//#define PCA995x_REG_COUNT      				0x15

#define	PCA995X_REG_MODE1					0x00									// Register addres for MODE 1
#define	PCA995X_REG_MODE2					0x01									// Register addres for MODE 2
#define	PCA995X_REG_LEDOUT0					0x02									// Register addres for LEDOUT0
#define	PCA995X_REG_GRPPWM					0x06									// Register addres for GRPPWM
#define	PCA995X_REG_GRPFREQ					0x07									// Register addres for GRPFREQ
#define	PCA995X_REG_PWM0					0x08									// Register addres for LEDPWM0
#define	PCA995X_REG_IREF0					0x18									// Register addres for IREF0
#define	PCA995X_REG_EFLAG0					0x46									// Register addres for EFLAG0
#define	PCA995X_REG_EFLAG1					0x47									// Register addres for EFLAG1
#define	PCA995X_REG_EFLAG2					0x48									// Register addres for EFLAG2
#define	PCA995X_REG_EFLAG3					0x49									// Register addres for EFLAG3

#define PCA995X_AIF							1 << 7									// Write only - Auto-Increment flag

#define	PCA995X_ERR_NOT						0										// no error detected
#define	PCA995X_ERR_SHORT					1										// Short circuit on output detected
#define	PCA995X_ERR_OPEN					2										// Output is opened

extern const _drv_Api_t 					drv_PCA995x_API_STD;

#endif		// #if defined(USE_DRV_PCA995X)
#endif		// __DRV_PCA995X_H_
