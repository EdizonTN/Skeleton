// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_LCD.c
// 	   Version: 3.03
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for LCD on MCU
// ******************************************************************************
// Info: LCD driver
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:	
//				2024.09.30	- v3.03 - add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2023.12.10	- v3.02 - rename _CHIP_ADCx_IRQ_Handler to _Chip_ADCx_IRQ_Handler
//									- change bootom irq to 	IRQ_Handler_Bottom((void *) ADC_DriverIRQ[x]);
//				2020.06.16	- while(tmpListRecord) changed
// 

#include "Skeleton.h"

#if defined(USE_DRV_LCD) && (USE_DRV_LCD == 1)

// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool LCD_Init(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool LCD_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool LCD_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
void LCD_Events( struct _drv_If *pDrvIf, _EventType_t EventType);
void LCD_Handler_Top(_drv_If_t *pDrvIf);											// quick handler
void LCD_Handler_Bottom(_drv_If_t *pDrvIf);											// process handler

bool LCD_Clear(_drv_If_t *pDrvIf);
	
// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************
// structure and assign to extended API functions:
const drv_LCD_Ext_API_t LCD_Ext_API_STD = 
{
	.Clear			=	LCD_Clear													// Clear display
};

// structure and assign to standard API functions:
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_LCD_API_STD = 
{
	.Init			=	LCD_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	LCD_Enable,
	.Disable		=	LCD_Disable,
	.Write_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	LCD_Events,
	.Ext			= 	(drv_LCD_Ext_API_t *) &LCD_Ext_API_STD 						// connect extended API
};
#pragma pop

// Remark: these are standard function calling for this driver. Don't change during run of the aplication!
// If you need change some of these functions, create your own structure based on this (USR), and assign your functions.
									
const _drv_If_t *LCD_DriverIRQ[_CHIP_LCD_COUNT];

// ------------------------------------------------------------------------------------------------------
// INTERRUPT HANDLERS
// ------------------------------------------------------------------------------------------------------

#if defined(_CHIP_LCD_COUNT) && (_CHIP_LCD_COUNT > 0)
void _CHIP_LCD0_IRQ_Handler(void)	
{
	LCD_Handler_Top((void *) LCD_DriverIRQ[0]);
	//LCD_Handler_Bottom((void *) LCD_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) LCD_DriverIRQ[0]);
}
#endif

#if defined(_CHIP_LCD_COUNT) && (_CHIP_LCD_COUNT > 1)
void _CHIP_LCD0_IRQ_Handler(void)	
{
	LCD_Handler_Top((void *) LCD_DriverIRQ[1]);
	//LCD_Handler_Bottom((void *) LCD_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) LCD_DriverIRQ[1]);
}
#endif

// ------------------------------------------------------------------------------------------------------
// Interface initialization
bool LCD_Init(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	drv_LCD_If_Spec_t* pDrvIf_Spec = (drv_LCD_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer for specific struct of the periphery

#if defined(CONF_DEBUG_DRV_LCD) && (CONF_DEBUG_DRV_LCD == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> ParentMod: %s", pRequestorModIf->Name);
#endif		

	// initialization code here:
	pDrvIf_Spec->pPeri = CHAL_LCD_Init(pDrvIf_Spec->PeriIndex);	
	if(pDrvIf_Spec->pPeri) res = true;

	pDrvIf->Sys.Stat.Initialized  = res;											// Done !
	LCD_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;									// set parameter for interrupt routine
	
#if defined(CONF_DEBUG_DRV_LCD) && (CONF_DEBUG_DRV_LCD == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif			
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Generic
// ENABLE of periphery
// if ChannelBitMask == UINT32_MAX, then enable/disable for all channels
bool LCD_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	drv_LCD_If_Spec_t* pDrvIf_Spec = (drv_LCD_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer for specific struct of the periphery

#if defined(CONF_DEBUG_DRV_LCD) && (CONF_DEBUG_DRV_LCD == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);							// print arguments
#endif

	// main code here:
	res = CHAL_LCD_Enable(pDrvIf_Spec->pPeri, true); 								// enable LCD
	res |= CHAL_LCD_IRQ_Peri_Enable(pDrvIf_Spec->pPeri, ChannelBitMask, true); 		// enable LCD IRQ
	
	if(res == true) pDrvIf->Sys.Stat.Enabled = true;								// enable was succesfull
	NVIC_EnableIRQ((IRQn_Type) (pDrvIf->IRQ_VecNum));								// Enable LCD's interrupt
		
#if defined(CONF_DEBUG_DRV_LCD) && (CONF_DEBUG_DRV_LCD == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);              
}

// ------------------------------------------------------------------------------------------------------
// Generic
// DISABLE of periphery
// if ChannelBitMask == UINT32_MAX, then enable/disable for all channels
bool LCD_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	drv_LCD_If_Spec_t* pDrvIf_Spec = (drv_LCD_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer for specific struct of the periphery

#if defined(CONF_DEBUG_DRV_LCD) && (CONF_DEBUG_DRV_LCD == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);							// print arguments
#endif

	// main code here:
	res = CHAL_LCD_Enable(pDrvIf_Spec->pPeri, false); 								// disable LCD 
	res |= CHAL_LCD_IRQ_Peri_Enable(pDrvIf_Spec->pPeri, ChannelBitMask, false); 	// disable LCD IRQ
	
	if(res == true) pDrvIf->Sys.Stat.Enabled = false;								// enable was succesfull
	NVIC_EnableIRQ((IRQn_Type) (pDrvIf->IRQ_VecNum));								// Enable LCD interrupt
		
#if defined(CONF_DEBUG_DRV_LCD) && (CONF_DEBUG_DRV_LCD == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}

// ******************************************************************************************************
// Extended functions
// ******************************************************************************************************


// ------------------------------------------------------------------------------------------------------
// EXT
// Clear LCD area
// Return: True if Change was OK, otherwise False
bool LCD_Clear(_drv_If_t *pDrvIf)
{
	
	return(true);
}




// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************


// ------------------------------------------------------------------------------------------------------
// Quick handler
inline void LCD_Handler_Top(_drv_If_t *pDrvIf)										// Quick Handler  - Overrun irq
{
	if(pDrvIf == NULL) return;

	if(pDrvIf->pIRQ_USR_HandlerTop) pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);			// override with USR handler
	else
	{
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;

		// Generic interrupt routine:
		NVIC_ClearPendingIRQ( (IRQn_Type) (pDrvIf->IRQ_VecNum));					// Clear pending interrupt
	}	
}
// ------------------------------------------------------------------------------------------------------
// After handler
// fire event system
inline void LCD_Handler_Bottom(_drv_If_t *pDrvIf)									// Process Handler  - Overrun irq
{
	if(pDrvIf->pIRQ_USR_HandlerBottom) pDrvIf->pIRQ_USR_HandlerBottom(pDrvIf);		// override with USR handler
	else
	{
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;
		
		
		if((pDrvIf->pAPI_USR) && (pDrvIf->pAPI_USR->Events)) pDrvIf->pAPI_USR->Events( pDrvIf, EV_StateChanged);	// if is set USR event system, call it
		else
		{
			if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_StateChanged);	// if is set STD event system, call it
		}
	}
	
	CHAL_LCD_Clear_IRQ_Status(((drv_LCD_If_Spec_t*) pDrvIf->pDrvSpecific)->pPeri, CHAL_LCD_Get_IRQ_Status(((drv_LCD_If_Spec_t*) pDrvIf->pDrvSpecific)->pPeri));
}




// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------
// Event system - called still in inside interrupt routiune !!!
inline void LCD_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{
//#if defined(CONF_IDIOTPROOF) && (CONF_IDIOTPROOF == 1)
//	SYS_ASSERT( pDrvIf != NULL);													// check
//#endif

//	switch(EventType)
//	{
//		case EV_StateChanged:
//		{
//			void* CallBack  = NULL;
//			_ColListRecord_t *tmpListRecord = pDrvIf->Sys.ParentModLinked.pFirst;	// temporary pointer to parent module list
//			while(tmpListRecord)
//			{
//				//CallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_ChangedState;

//				if(CallBack)		// callback is set?
//				{
//					sys_Task_Create_Item("LCD_Event",CallBack, (sys_TaskArgs_t) {NULL, 0});// create task for data read
//				}

//				if(tmpListRecord->pNext == NULL) break;								// loop while have associadet module
//				tmpListRecord = tmpListRecord->pNext;								// goto next associated module
//			}
//			break;
//		}
//		case EV_NoChange:
//		default: break;
//	}
}

#endif	// USE_DRV_LCD
