// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_MRT.h
// 	   Version: 3.00
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for Multi Rate MRT - header file
// ******************************************************************************
// Info: MRT driver
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
// 

#if !defined(__DRV_MRT_H_) 															// prevent to cyclyc loads
 #if defined(USE_DRV_MRT) && (USE_DRV_MRT == 1)										// Need to load?
  #if (!defined (_CHIP_MRT_COUNT) || _CHIP_MRT_COUNT == 0)							// Neeed it, but MCU haven't this type of periphery!
	#error This driver is need to be used, but MCU doesn't have it!
  #else
	// now -> load it.
#define __DRV_MRT_H_

#include "Skeleton.h"

// ******************************************************************************************************
// CONFIGURATION
// ******************************************************************************************************
#ifndef 	CONF_DEBUG_DRV_MRT
 #define 	CONF_DEBUG_DRV_MRT			0											// default is OFF
#endif 

// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************

COMP_PACKED_BEGIN
typedef struct drv_MRT_If_Spec
{
			_CHIP_MRT_T						*pPeri;									// pointer to peripohery
	const 	uint8_t							PeriIndex;								// index of the used periphery
	const	uint8_t							Channel;								// channel
	_drv_If_t 								*pRootDrvIf;							// ptr to Owners driver struct
} drv_MRT_If_Spec_t;
COMP_PACKED_END

// ******************************************************************************************************
// PUBLIC 
// ******************************************************************************************************
extern const _drv_Api_t drv_MRT_API_STD;

// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************


#endif		// defined (_CHIP_zzz_COUNT)
#endif		// #if defined(USE_DRV_zzz)
#endif		// __DRV_ZZZ_H_
