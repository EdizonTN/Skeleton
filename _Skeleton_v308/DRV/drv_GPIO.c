// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_GPIO.c
// 	   Version: 3.42
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for GPIO
// ******************************************************************************
// Info: GPIO driver (GPIO Input) - for external pin interrupt
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:		
//				2024.09.30	- v3.42 - add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2023.12.10	- v3.41 - rename _CHIP_GPIOx_IRQ_Handler to _Chip_GPIOx_IRQ_Handler
//				2022.11.30	- v3.4 - change system of load
//				2022.11.28	- changed interrupt handler bottom
//				2020.06.24	- pAPI_USR deleted!
//				2020.06.16	- while(tmpListRecord) changed
// 

#include "Skeleton.h"

#ifdef __DRV_GPIO_H_																	// Header was loaded?


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool GPIO_Init(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool GPIO_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool GPIO_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
//uint32_t GPIO_Write_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t WrLen, _XFerType_t XferType);
//uint32_t GPIO_Read_Data (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t RdLen, _XFerType_t XferType);
void GPIO_Events( struct _drv_If *pDrvIf, _EventType_t EventType);
void GPIO_Handler_Top(_drv_If_t *pDrvIf);											// quick handler
//void GPIO_Handler_Bottom(_drv_If_t *pDrvIf);										// process handler


// ******************************************************************************************************
// PRIVATE Variables
// ******************************************************************************************************
// struktura a priradenie standardnych API funckii driveru:

// toto su STD volania pre tento driver. Mali by byt ako const. Nesmu sa menit, pretoze na ne moze ukazovat viacero subdriverov.
// ak chces USR volanie, vytvor novu strukturu, skopiruj do nej tuto STD a zmen konkretnu funkciu!

#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_GPIO_API_STD 				= 
{
	.Init			=	GPIO_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	GPIO_Enable,
	.Disable		=	GPIO_Disable,
	.Write_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	GPIO_Events,
};
#pragma pop
									

const _drv_If_t *GPIO_DriverIRQ[_CHIP_GPIO_COUNT];

// ------------------------------------------------------------------------------------------------------
// GPIO INTERRUPT HANDLER
// ------------------------------------------------------------------------------------------------------
#if defined(_CHIP_GPIO_COUNT) && (_CHIP_GPIO_COUNT > 0)
void _Chip_GPIO0_IRQ_Handler(void)
{
	GPIO_Handler_Top((void *) GPIO_DriverIRQ[0]);
	//GPIO_Handler_Bottom((void *) GPIO_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) GPIO_DriverIRQ[0]);
}
#endif

#if defined(_CHIP_GPIO_COUNT) && (_CHIP_GPIO_COUNT > 1)
void _Chip_GPIO1_IRQ_Handler(void)	
{
	GPIO_Handler_Top((void *) GPIO_DriverIRQ[1]);
	//GPIO_Handler_Bottom((void *) GPIO_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) GPIO_DriverIRQ[1]);
}
#endif

#if defined(_CHIP_GPIO_COUNT) && (_CHIP_GPIO_COUNT > 2)
void _Chip_GPIO2_IRQ_Handler(void)	
{
	GPIO_Handler_Top((void *) GPIO_DriverIRQ[2]);
	//GPIO_Handler_Bottom((void *) GPIO_DriverIRQ[2]);
	IRQ_Handler_Bottom((void *) GPIO_DriverIRQ[2]);
}
#endif

#if defined(_CHIP_GPIO_COUNT) && (_CHIP_GPIO_COUNT > 3)
void _Chip_GPIO3_IRQ_Handler(void)	
{
	GPIO_Handler_Top((void *) GPIO_DriverIRQ[3]);
	//GPIO_Handler_Bottom((void *) GPIO_DriverIRQ[3]);
	IRQ_Handler_Bottom((void *) GPIO_DriverIRQ[3]);
}
#endif

#if defined(_CHIP_GPIO_COUNT) && (_CHIP_GPIO_COUNT > 4)
void _Chip_GPIO4_IRQ_Handler(void)	
{
	GPIO_Handler_Top((void *) GPIO_DriverIRQ[4]);
	//GPIO_Handler_Bottom((void *) GPIO_DriverIRQ[4]);
	IRQ_Handler_Bottom((void *) GPIO_DriverIRQ[4]);
}
#endif

#if defined(_CHIP_GPIO_COUNT) && (_CHIP_GPIO_COUNT > 5)
void _Chip_GPIO5_IRQ_Handler(void)	
{
	GPIO_Handler_Top((void *) GPIO_DriverIRQ[5]);
	//GPIO_Handler_Bottom((void *) GPIO_DriverIRQ[5]);
	IRQ_Handler_Bottom((void *) GPIO_DriverIRQ[5]);
}
#endif

// ------------------------------------------------------------------------------------------------------
// Inicializacia interfejsu
// Force - inicializuj aj ked je nastaveny priznak ze uz je init hotovy
// ------------------------------------------------------------------------------------------------------
bool GPIO_Init(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	drv_GPIO_If_Spec_t* pDrvIf_Spec = (drv_GPIO_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_GPIO) && (CONF_DEBUG_DRV_GPIO == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> ParentMod: %s", pRequestorModIf->Name);
#endif		

	// initialization code here:
	res = CHAL_Signal_Init (pDrvIf_Spec->pSigIO );									// inicializuj pin
	if( res == false) goto gpioinitexit;											// Init bad - return
	
//	res = GPIO_Write_Data(pRequestorModIf, pDrvIf, (uint8_t*) &pDrvIf_Spec->Value, 1, XFer_Blocking);	// zapis predchadzajuci stav na port

	if(pDrvIf_Spec->G_INT.IsUsed == true)											// Group interrupt
	{
		pDrvIf_Spec->G_INT.pPeri = CHAL_Signal_Set_GINT(pDrvIf_Spec->pSigIO, pDrvIf_Spec->G_INT.Group, pDrvIf_Spec->G_INT.Sens); 	// if is used, set group interrupt
		if(pDrvIf_Spec->G_INT.pPeri != NULL) res = true;
		
#if defined (CONF_USE_IRQ_MANAGER) && (CONF_USE_IRQ_MANAGER == 1) && defined(GPIO_USE_IRQ_MANAGER) && (GPIO_USE_IRQ_MANAGER == 1)
		if(res == true) res = irqmgr_isr_Handler_Install(pDrvIf->IRQ_VecNum, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerTop, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerBottom, pDrvIf->IRQ_Priority, (void *)pDrvIf);
#else
		GPIO_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;
#endif			
	}
	
	if(pDrvIf_Spec->P_INT.IsUsed == true)											// Pin interrupt
	{
		pDrvIf_Spec->P_INT.pPeri = CHAL_Signal_Set_PINT(pDrvIf_Spec->pSigIO, pDrvIf_Spec->P_INT.Select, pDrvIf_Spec->P_INT.Sens); 	// if is used, set group interrupt
		if(pDrvIf_Spec->P_INT.pPeri != NULL) res = true;
		
#if defined (CONF_USE_IRQ_MANAGER) && (CONF_USE_IRQ_MANAGER == 1) && defined(GPIO_USE_IRQ_MANAGER) && (GPIO_USE_IRQ_MANAGER == 1)
		if(res == true) res = irqmgr_isr_Handler_Install(pDrvIf->IRQ_VecNum, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerTop, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerBottom, pDrvIf->IRQ_Priority, (void *)pDrvIf);
#else
		GPIO_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;
#endif			
	}	
	
	
	pDrvIf->Sys.Stat.Initialized  = res;											// Done !

gpioinitexit:	
#if defined(CONF_DEBUG_DRV_GPIO) && (CONF_DEBUG_DRV_GPIO == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif			
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// Generic
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
// ------------------------------------------------------------------------------------------------------
bool GPIO_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	drv_GPIO_If_Spec_t* pDrvIf_Spec = (drv_GPIO_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_GPIO) && (CONF_DEBUG_DRV_GPIO == 1)
    FN_DEBUG_ENTRY(pDrvIf->Name)
#endif

	// main code here:
	if(pDrvIf_Spec->G_INT.IsUsed == true)
	{
		NVIC_EnableIRQ( (IRQn_Type) (pDrvIf->IRQ_VecNum));							// Enable PINTn interrupt
	}
	
	if(pDrvIf_Spec->P_INT.IsUsed == true)
	{
#if defined(LPC_PINT)																// Have this MCU PINT?
		LPC_PINT->IENR |= (1 << pDrvIf_Spec->P_INT.Select);
#endif		
		NVIC_EnableIRQ( (IRQn_Type) (pDrvIf->IRQ_VecNum));							// Enable PINTn interrupt
	}	
	
	res = true;
	pDrvIf->Sys.Stat.Enabled = true;
	
#if defined(CONF_DEBUG_DRV_GPIO) && (CONF_DEBUG_DRV_GPIO == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// Generic
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
// ------------------------------------------------------------------------------------------------------
bool GPIO_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	drv_GPIO_If_Spec_t* pDrvIf_Spec = (drv_GPIO_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_GPIO) && (CONF_DEBUG_DRV_GPIO == 1)
    FN_DEBUG_ENTRY(pDrvIf->Name)
#endif

	// main code here:
	if(pDrvIf_Spec->G_INT.IsUsed == true)
	{
		NVIC_DisableIRQ( (IRQn_Type) (pDrvIf->IRQ_VecNum));							// Enable GINT0 interrupt
	}

	if(pDrvIf_Spec->P_INT.IsUsed == true)
	{
#if defined(LPC_PINT)																// Have this MCU PINT?
		LPC_PINT->IENR &= ~(1 << pDrvIf_Spec->P_INT.Select);
#endif		
		NVIC_EnableIRQ( (IRQn_Type) (pDrvIf->IRQ_VecNum));							// Enable PINTn interrupt
	}	
	
	res = true;
	pDrvIf->Sys.Stat.Enabled = false;
	
#if defined(CONF_DEBUG_DRV_GPIO) && (CONF_DEBUG_DRV_GPIO == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}





// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************


// ------------------------------------------------------------------------------------------------------
// Quick handler
inline void GPIO_Handler_Top(_drv_If_t *pDrvIf)										// Quick Handler  - Overrun irq
{
	if(pDrvIf == NULL) return;

	if(pDrvIf->pIRQ_USR_HandlerTop) pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);			// override with USR handler
	else
	{
		if(pDrvIf->Sys.Stat.Initialized == false) return;
		if(pDrvIf->Sys.Stat.Loaded == false) return;

		// Generic interrupt routine:
		NVIC_ClearPendingIRQ( (IRQn_Type) (pDrvIf->IRQ_VecNum));					// Clear pending interrupt
	}	
}



//// ------------------------------------------------------------------------------------------------------
//// After handler
//// fire event system
//inline void GPIO_Handler_Bottom(_drv_If_t *pDrvIf)									// Process Handler  - Overrun irq
//{
//	if(pDrvIf->pIRQ_USR_HandlerBottom) pDrvIf->pIRQ_USR_HandlerBottom(pDrvIf);		// override with USR handler
//	else
//	{
//		if(pDrvIf->Sys.Stat.Initialized == false) return;
//		if(pDrvIf->Sys.Stat.Loaded == false) return;
//		
//		
////		if((pDrvIf->pAPI_USR) && (pDrvIf->pAPI_USR->Events)) pDrvIf->pAPI_USR->Events( pDrvIf, EV_DataReceived);	// if is set USR event system, call it
////		else
//		{
//			if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_DataReceived);	// if is set STD event system, call it
//		}
//	}
//}





// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
inline void GPIO_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{
//#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
//	SYS_ASSERT( pDrvIf != NULL);													// check
//#endif

//	void* pCallBack = NULL;		
//	_ColListRecord_t *tmpListRecord = pDrvIf->Sys.ParentModLinked.pFirst;			// temporary pointer to parent module list
//	while(tmpListRecord)
//	{
//		switch(EventType)
//		{
//			case EV_StateChanged:
//			{
//				//pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_ChangedState;
//				break;
//			}
//			case EV_DataReceived:
//			{
//				//pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataReceived;
//				break;
//			}
//			case EV_Transmitted:
//			{
//				//pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataTransmitted;
//				break;
//			}
//			default: break;
//		}
//		
//		if(pCallBack)																// callback is set?
//		{
//			sys_Task_Create_Item( pDrvIf->Name, pCallBack, (sys_TaskArgs_t) {NULL, 0});	// create task for data read
//		}

//		if(tmpListRecord->pNext == NULL) break;										// loop while have associadet module
//		tmpListRecord = tmpListRecord->pNext;										// goto next associated module
//	}
}

#endif	// ifdef __DRV_GPIO_H_
