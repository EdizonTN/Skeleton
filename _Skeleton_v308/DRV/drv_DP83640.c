// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_DP83640.c
// 	   Version: 1.01
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Driver for Phy family DP83640 
// ******************************************************************************
// Usage: 
//
// ToDo:
// 
// Changelog:
//				2024.09.30	- v1.01 - add sys_NULL_Function instead a NULL pointer to a function in API structure
// 

#include "Skeleton.h"

#if defined(USE_DRV_DP83640) && (USE_DRV_DP83640 == 1)

// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
static bool phyDP83640_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
static bool phyDP83640_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
//static uint32_t phyDP83640_Write_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t WrLen, _XFerType_t XferType);
//static uint32_t phyDP83640_Read_Data (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t RdLen, _XFerType_t XferType);
static bool phyDP83640_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
//static size_t phyDP83640_MemTotalSize( _drv_If_t *pDrvIf);
//static size_t phyDP83640_MemFreeSize( _drv_If_t *pDrvIf);

static bool phyDP83640_ChipInit(_drv_If_t *pDrvIf);
// ******************************************************************************************************
// Public Function - specific for this driver
// ******************************************************************************************************
const drv_DP83640_Ext_API_t	phyDP83640_Ext_API 	=
{
	.Chip_Init			= phyDP83640_ChipInit
		
//	.Read_MemTotalSize	= phyDP83640_MemTotalSize,
//	.Read_MemFreeSize	= phyDP83640_MemFreeSize
};

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_DP83640_API_STD 		= 
{
 	.Init			=	phyDP83640_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	phyDP83640_Enable,
	.Disable		=	phyDP83640_Disable,
	.Write_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Ext			=	(_drv_Api_t *) &phyDP83640_Ext_API,
};
#pragma pop


// ******************************************************************************************************
// Private Functions
// ******************************************************************************************************
// ------------------------------------------------------------------------------------------------------
// EXT
// Inicialization internal registers of the DP83640-u
static bool phyDP83640_ChipInit(_drv_If_t *pDrvIf)
{
	bool res = false;
    drv_DP83640_If_Spec_t* pDrvIf_Spec = (drv_DP83640_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie
	
#if defined(CONF_DEBUG_DRV_DP83640) && (CONF_DEBUG_DRV_DP83640 == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif		

#ifdef FNET_CFG_CPU_ETH_RMII
			// definovany stav pre RMII iface a DP83640
			// toto plati pre DP83640 ! Mozno nie pre DP83848		 http://www.ti.com/lit/an/snla101a/snla101a.pdf		
			// nepopuzite, ale treba ich dat na definovanu uroven. Inak to ries hardwerovo.....
			// Jedna sa o nevyuzite signaly: sig_ETH_TXD2, sig_ETH_TXD3, sig_ETH_TXCLK, sig_ETH_RXD2, sig_ETH_RXD3 a sig_ETH_COL 
			// sig_ETH_IMODE nastav na Log.0
			
			CHAL_Signal_t	tmpSignal;
		
			tmpSignal = *pDrvIf_Spec->pSig_TXD2;
			tmpSignal.Ext.Conf.IOCON_Reg = 0x0000;									// Set as GPIO pin
			tmpSignal.Ext.Dir = CHAL_IO_Output;										// Ako vystup
			CHAL_Signal_Init(&tmpSignal);											// Reinit signalu do neaktivneho stavu
			CHAL_Signal_Clear_Pin(tmpSignal.Std);									// deactivate signal
		
			tmpSignal = *pDrvIf_Spec->pSig_TXD3;
			tmpSignal.Ext.Conf.IOCON_Reg = 0x0000;									// Set as GPIO pin
			tmpSignal.Ext.Dir = CHAL_IO_Output;										// Ako vystup
			CHAL_Signal_Init(&tmpSignal);											// Reinit signalu do neaktivneho stavu
			CHAL_Signal_Clear_Pin(tmpSignal.Std);									// deactivate signal

			tmpSignal = sig_ETH_TXCLK;
			tmpSignal.Ext.Conf.IOCON_Reg = 0x0000 | IOCON_MODE_PULLUP;				// Set as GPIO pin	- in RMII is UNUSED
			tmpSignal.Ext.Dir = CHAL_IO_Input;										// Ako vstup s PullUp
			CHAL_Signal_Init(&tmpSignal);											// Reinit signalu do neaktivneho stavu
			CHAL_Signal_Clear_Pin(tmpSignal.Std);									// deactivate signal

	// RX
			tmpSignal = *pDrvIf_Spec->pSig_RXD2;
			tmpSignal.Ext.Conf.IOCON_Reg = 0x0000 | IOCON_MODE_PULLDOWN;			// Set as GPIO pin
			tmpSignal.Ext.Dir = CHAL_IO_Input;										// Ako vstup
			CHAL_Signal_Init(&tmpSignal);											// Reinit signalu do neaktivneho stavu
			CHAL_Signal_Clear_Pin(tmpSignal.Std);									// deactivate signal

			tmpSignal = *pDrvIf_Spec->pSig_RXD3;
			tmpSignal.Ext.Conf.IOCON_Reg = 0x0000 | IOCON_MODE_PULLDOWN;			// Set as GPIO pin
			tmpSignal.Ext.Dir = CHAL_IO_Input;										// Ako vstup
			CHAL_Signal_Init(&tmpSignal);											// Reinit signalu do neaktivneho stavu
			CHAL_Signal_Clear_Pin(tmpSignal.Std);									// deactivate signal

//			tmpSignal = sig_ETH_RXCLK;
//			tmpSignal.Ext.Conf.IOCON_Reg = 0x0000 | IOCON_MODE_PULLDOWN;			// Set as GPIO pin	- in RMII is UNUSED
//			tmpSignal.Ext.Dir = CHAL_IO_Input;										// Ako vstup
//			CHAL_Signal_Init(&tmpSignal);											// Reinit signalu do neaktivneho stavu
//			CHAL_Signal_Clear_Pin(tmpSignal.Std);									// deactivate signal

			tmpSignal = sig_ETH_COL;
			tmpSignal.Ext.Conf.IOCON_Reg = 0x0000 | IOCON_MODE_PULLDOWN;			// Set as GPIO pin	- in RMII is UNUSED
			tmpSignal.Ext.Dir = CHAL_IO_Input;										// Ako vstup
			CHAL_Signal_Init(&tmpSignal);											// Reinit signalu do neaktivneho stavu
			CHAL_Signal_Clear_Pin(tmpSignal.Std);									// deactivate signal
			
			res = true;

#endif	

#if defined(CONF_DEBUG_DRV_DP83640) && (CONF_DEBUG_DRV_DP83640 == 1)
	FN_DEBUG_EXIT(pDrvIf->Name)
#endif		
	return(res);	
}






// ******************************************************************************************************
// USR Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// USR
// Inicialization of the DP83640-u
// Remark! Set Signal Active to Log. 1 !!!
static bool phyDP83640_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;	//0
	
	drv_DP83640_If_Spec_t* pDrvIf_Spec = (drv_DP83640_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie
	
	if( pDrvIf_Spec->pSig_RST != NULL) 												// Reset IC
	{
		CHAL_Signal_Set_Pin( pDrvIf_Spec->pSig_RST->Std);
		Delay_us(5);
		CHAL_Signal_Clear_Pin( pDrvIf_Spec->pSig_RST->Std);
	}
	
	
#if defined(CONF_DEBUG_DRV_DP83640) && (CONF_DEBUG_DRV_DP83640 == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif		

	
	
//		const 	CHAL_Signal_t			*pSig_RST;										// DP83640 Input: Reset Input.Active Low
//	
//	const 	CHAL_Signal_t			*pSig_GPIO1;									// DP83640 I/O: GPIO1
//	const 	CHAL_Signal_t			*pSig_GPIO2;									// DP83640 I/O: GPIO2
//	const 	CHAL_Signal_t			*pSig_GPIO3;									// DP83640 I/O: GPIO3
//	const 	CHAL_Signal_t			*pSig_GPIO4;									// DP83640 I/O: GPIO4
//	const 	CHAL_Signal_t			*pSig_GPIO5;									// DP83640 I/O: GPIO5
//	const 	CHAL_Signal_t			*pSig_GPIO6;									// DP83640 I/O: GPIO6
//	const 	CHAL_Signal_t			*pSig_GPIO7;									// DP83640 I/O: GPIO7
//	const 	CHAL_Signal_t			*pSig_GPIO8;									// DP83640 I/O: GPIO8
//	const 	CHAL_Signal_t			*pSig_GPIO9;									// DP83640 I/O: GPIO9
//	const 	CHAL_Signal_t			*pSig_GPIO10;									// DP83640 I/O: GPIO10
//	const 	CHAL_Signal_t			*pSig_GPIO11;									// DP83640 I/O: GPIO11
//	const 	CHAL_Signal_t			*pSig_CLK_IEEE;									// DP83640 I/O: CLKOUT/IEEECLK
//	const 	CHAL_Signal_t			*pSig_PWDN_IRQ;									// DP83640 Input: PowerDown, Output - Ethernet interrupt
	
	
	
	// initialization code here:
	if( pDrvIf_Spec->pSig_RST != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_RST);
	
	if( pDrvIf_Spec->pSig_GPIO1 != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_GPIO1);
	if( pDrvIf_Spec->pSig_GPIO2 != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_GPIO2);
	if( pDrvIf_Spec->pSig_GPIO3 != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_GPIO3);
	if( pDrvIf_Spec->pSig_GPIO4 != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_GPIO4);
	if( pDrvIf_Spec->pSig_GPIO5 != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_GPIO5);
	if( pDrvIf_Spec->pSig_GPIO6 != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_GPIO6);
	if( pDrvIf_Spec->pSig_GPIO7 != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_GPIO7);
	if( pDrvIf_Spec->pSig_GPIO8 != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_GPIO8);
	if( pDrvIf_Spec->pSig_GPIO9 != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_GPIO9);
	if( pDrvIf_Spec->pSig_GPIO10 != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_GPIO10);
	if( pDrvIf_Spec->pSig_GPIO11 != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_GPIO11);
	
	if( pDrvIf_Spec->pSig_CLK_IEEE != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_CLK_IEEE);
	if( pDrvIf_Spec->pSig_PWDN_IRQ != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_PWDN_IRQ);
	

	if(pDrvIf_Spec->pParentInterface == NULL)  res = false;							// Parent Interface MUST be set!

	res |= phyDP83640_ChipInit(pDrvIf);
	
	pDrvIf->Sys.Stat.Initialized = res;

	
#if defined(CONF_DEBUG_DRV_DP83640) && (CONF_DEBUG_DRV_DP83640 == 1)
	FN_DEBUG_EXIT(pDrvIf->Name)
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// USR
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
static bool phyDP83640_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
#if defined(CONF_DEBUG_DRV_DP83640) && (CONF_DEBUG_DRV_DP83640 == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

	// main code here:
	pDrvIf->Sys.Stat.Enabled = true;												// enable was succesfull
	res = true;
#if defined(CONF_DEBUG_DRV_DP83640) && (CONF_DEBUG_DRV_DP83640 == 1)
	FN_DEBUG_EXIT(pDrvIf->Name)
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// USR
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
static bool phyDP83640_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
#if defined(CONF_DEBUG_DRV_DP83640) && (CONF_DEBUG_DRV_DP83640 == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

	// main code here:
	pDrvIf->Sys.Stat.Enabled = false;												// disable was succesfull
	res = true;
	
#if defined(CONF_DEBUG_DRV_DP83640) && (CONF_DEBUG_DRV_DP83640 == 1)
	FN_DEBUG_EXIT(pDrvIf->Name)
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// USR
// pbuff - hodnota
// BuffByteLen - pocet znakov na prenesenie (uint8_t)
//static uint32_t phyDP83640_Write_Data( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t WrLen, _XFerType_t XferType)
//{
//	uint32_t Result = 0;

//#if defined(CONF_DEBUG_DRV_DP83640) && (CONF_DEBUG_DRV_DP83640 == 1)
//	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
//#endif

////	drv_DP83640_If_Spec_t* pDrvIf_Spec = (drv_DP83640_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie
//	
////	if( pDrvIf_Spec->pSig_WP != NULL) CHAL_Signal_Clear_Pin( pDrvIf_Spec->pSig_WP->Std);	// Deactivate WriteProtect if available
////	
////	// Write data
////	Result = drvmgr_Write_Data(pRequestorModIf, pDrvIf_Spec->pCommInterface, pbuff, WrLen, XferType);

////	if( pDrvIf_Spec->pSig_WP != NULL) CHAL_Signal_Set_Pin( pDrvIf_Spec->pSig_WP->Std);		// Activate WriteProtect if available

//	
//	//if(BuffByteLen)res = false;
//	//else res = true;
//	
//#if defined(CONF_DEBUG_DRV_DP83640) && (CONF_DEBUG_DRV_DP83640 == 1)
//	FN_DEBUG_EXIT(pDrvIf->Name)
//	FN_DEBUG_FMTPRINT("Xfer: %d", Result);
//#endif		
//	return(Result);
//}

// ------------------------------------------------------------------------------------------------
// Citanie dat zo zbernice pDrvIf_Spec->pCommInterface
// ak maxlen=0 a pbuff=NULL, nastava flush receive buffera - znuluje prijem.
// vracia pocet nacitanych byte do pbuff
//static uint32_t phyDP83640_Read_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t RdLen, _XFerType_t XferType)
//{
//	uint32_t Result = 0;
//	
//#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
//		bool res = false;
//	FN_DEBUG_ENTRY(pDrvIf->Name);
//	FN_DEBUG_FMTPRINT(" >> pDst: %p, MaxLen: %d, XFerType: 0x%.1X", pbuff, maxlen, XferType);
//#endif	

////	drv_DP83640_If_Spec_t* pDrvIf_Spec = (drv_DP83640_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie
//    //Result = drvmgr_Read_Data(pRequestorModIf, pDrvIf_Spec->pParentInterface, pbuff, RdLen, XferType);
//	//use:  pDrvIf_Spec->pParentInterface->pAPI_STD->Read_Data(
//	
//#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
//	if(rcount == maxlen) res = true;
//	else res = false;
//	FN_DEBUG_RES("Xfer: %d", Result);
//	FN_DEBUG_EXIT(pDrvIf->Name);
//#endif			
//	
//	return(Result);
//}



// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// no interrupts in this driver

// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************

// no interrupts in this driver


// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
inline void phyDP83640_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{
//	switch(EventType)
//	{
//		case EV_DataReceived:
//		case EV_TransmitReady:
//		case EV_TransmitDone:	
//		case EV_NoChange:
//		default: break;
//	}
}


#endif	// USE_DRV_DP83640
