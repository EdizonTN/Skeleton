// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_24XX1025.c
// 	   Version: 1.01
//  Created on: 01.05.2014
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Driver for I2C serial EEPROM family 24XX1025 
// ******************************************************************************
// Usage: 24xx1025 works as two 64k memory, because internal adressing above 0xffff is done by one address bit in I2C Address byte....
//
// ToDo:
// 
// Changelog:
//				2024.09.30	- v1.01	- add sys_NULL_Function instead a NULL pointer to a function in API structure
// 
#include "Skeleton.h"

#if defined(USE_DRV_24XX1025) && (USE_DRV_24XX1025 == 1)

// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
static bool ee24xx1025_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
static bool ee24xx1025_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
static uint32_t ee24xx1025_Write_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t WrLen, _XFerType_t XferType);
static uint32_t ee24xx1025_Read_Data (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t RdLen, _XFerType_t XferType);
static bool ee24xx1025_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
static size_t ee24xx1025_MemTotalSize( _drv_If_t *pDrvIf);
static size_t ee24xx1025_MemFreeSize( _drv_If_t *pDrvIf);

// ******************************************************************************************************
// Public Function - specific for this driver
// ******************************************************************************************************
const drv_24xx1025_Ext_API_t	ee24xx1025_Ext_API 	=
{
	.Read_MemTotalSize	= ee24xx1025_MemTotalSize,
	.Read_MemFreeSize	= ee24xx1025_MemFreeSize
};

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_24xx1025_API_STD = 
{
 	.Init			=	ee24xx1025_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	ee24xx1025_Enable,
	.Disable		=	ee24xx1025_Disable,
	.Write_Data		=	ee24xx1025_Write_Data,
	.Read_Data		=	ee24xx1025_Read_Data,
	.Events			=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Ext			=	(_drv_Api_t *) &ee24xx1025_Ext_API,
};
#pragma pop


// ******************************************************************************************************
// Private Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// EXT
// Report total memory size as is defined in header
static size_t ee24xx1025_MemTotalSize( _drv_If_t *pDrvIf)
{
	return(DEF_24XX1025_CAPACITY);
}

// ------------------------------------------------------------------------------------------------------
// EXT
// Report free memory in device
static size_t ee24xx1025_MemFreeSize( _drv_If_t *pDrvIf)
{
	return(0);
}


// ******************************************************************************************************
// USR Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// USR
// Inicialization of the 24xx1025-u
// Remark! Set Signal Active to Log. 1 !!!
static bool ee24xx1025_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;	//0
	
	drv_24xx1025_If_Spec_t* pDrvIf_Spec = (drv_24xx1025_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_24XX1025) && (CONF_DEBUG_DRV_24XX1025 == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif		

	// initialization code here:
	if( pDrvIf_Spec->pSig_WP != NULL) res |= CHAL_Signal_Init( pDrvIf_Spec->pSig_WP);
	if( pDrvIf_Spec->pSig_WP != NULL) CHAL_Signal_Set_Pin( pDrvIf_Spec->pSig_WP->Std);	// Activate WriteProtect if available

	if(pDrvIf_Spec->pCommInterface == NULL)  res = false;							// Interface MUST be set!
	
	pDrvIf->Sys.Stat.Initialized = res;

	pDrvIf_Spec->I2C_Device.Dev_Parms.RegAdrSize = DEF_24XX1025_MEMADR_SIZEBYTE;
	pDrvIf_Spec->I2C_Device.Dev_Parms.SpeedMode = I2C_GET_MODE(DEF_24XX1025_BUS_MAXFREQKHZ);
	
#if defined(CONF_DEBUG_DRV_24XX1025) && (CONF_DEBUG_DRV_24XX1025 == 1)
	FN_DEBUG_EXIT(pDrvIf->Name)
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// USR
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
static bool ee24xx1025_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
#if defined(CONF_DEBUG_DRV_24XX1025) && (CONF_DEBUG_DRV_24XX1025 == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

	// main code here:
	pDrvIf->Sys.Stat.Enabled = true;												// enable was succesfull
	res = true;
#if defined(CONF_DEBUG_DRV_24XX1025) && (CONF_DEBUG_DRV_24XX1025 == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// USR
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
static bool ee24xx1025_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
#if defined(CONF_DEBUG_DRV_24XX1025) && (CONF_DEBUG_DRV_24XX1025 == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

	// main code here:
	pDrvIf->Sys.Stat.Enabled = false;												// disable was succesfull
	res = true;
	
#if defined(CONF_DEBUG_DRV_24XX1025) && (CONF_DEBUG_DRV_24XX1025 == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// USR
// pbuff - hodnota
// BuffByteLen - pocet znakov na prenesenie (uint8_t)
static uint32_t ee24xx1025_Write_Data( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t WrLen, _XFerType_t XferType)
{
	uint32_t Result = 0;

#if defined(CONF_DEBUG_DRV_24XX1025) && (CONF_DEBUG_DRV_24XX1025 == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

	drv_24xx1025_If_Spec_t* pDrvIf_Spec = (drv_24xx1025_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie
	
	if( pDrvIf_Spec->pSig_WP != NULL) CHAL_Signal_Clear_Pin( pDrvIf_Spec->pSig_WP->Std);	// Deactivate WriteProtect if available
	
	// Write data
	Result = drvmgr_Write_Data(pRequestorModIf, pDrvIf_Spec->pCommInterface, pbuff, WrLen, XferType);

	if( pDrvIf_Spec->pSig_WP != NULL) CHAL_Signal_Set_Pin( pDrvIf_Spec->pSig_WP->Std);		// Activate WriteProtect if available
	
	//if(BuffByteLen)res = false;
	//else res = true;
	
#if defined(CONF_DEBUG_DRV_24XX1025) && (CONF_DEBUG_DRV_24XX1025 == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
	return(Result);
}

// ------------------------------------------------------------------------------------------------
// Citanie dat zo zbernice pDrvIf_Spec->pCommInterface
// ak maxlen=0 a pbuff=NULL, nastava flush receive buffera - znuluje prijem.
// vracia pocet nacitanych byte do pbuff
static uint32_t ee24xx1025_Read_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, void *pbuff, uint32_t RdLen, _XFerType_t XferType)
{
	uint32_t Result = 0;
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDst: %p, MaxLen: %d, XFerType: 0x%.1X", pbuff, RdLen, XferType);
#endif	

	drv_24xx1025_If_Spec_t* pDrvIf_Spec = (drv_24xx1025_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie
    Result = drvmgr_Read_Data(pRequestorModIf, pDrvIf_Spec->pCommInterface, pbuff, RdLen, XferType);
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("Xfer: %d", Result);
#endif			
	
	return(Result);
}



// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// no interrupts in this driver

// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************

// no interrupts in this driver


// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
inline void ee24xx1025_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{
//	switch(EventType)
//	{
//		case EV_DataReceived:
//		case EV_TransmitReady:
//		case EV_TransmitDone:	
//		case EV_NoChange:
//		default: break;
//	}
}


#endif	// USE_DRV_24XX1025
