// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_OneWire.h
// 	   Version: 3.1
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for OneWire - header file
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
//

#ifndef __DRV_ONEWIRE_H_
#define __DRV_ONEWIRE_H_

#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************

#ifndef CONF_DEBUG_DRV_ONEWIRE
#define CONF_DEBUG_DRV_ONEWIRE			0											// default je vypnuty
#endif	

#if !defined(CONF_USE_LIB_RINGBUFFER) || (CONF_USE_LIB_RINGBUFFER == 0)
 #ifndef CONF_DRV_ONEWIRE_BUFFERSIZE
 #define CONF_DRV_ONEWIRE_BUFFERSIZE		100											// velkost buffera prijatych znakov
 #endif	
#endif

#ifndef CONF_DRV_ONEWIRE_TIMEOUT
 #define CONF_DRV_ONEWIRE_TIMEOUT		5000										// timeout
#endif	
 

// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************
//const typedef struct drv_OneWire_Config
//{
//	uint32_t							BaudRate; 									// Baud Rate value
//	uint8_t								Address;									// Address (9-bit mode)
//} drv_OneWire_Config_t;

//typedef struct drv_OneWire_Data
//{
//	uint8_t LastDiscrepancy;       /*!< Search private */
//	uint8_t LastFamilyDiscrepancy; /*!< Search private */
//	uint8_t LastDeviceFlag;        /*!< Search private */
//	uint8_t ROM_NO[8];             /*!< 8-bytes address of last search device */
//} drv_OneWire_Data_t;


COMP_PACKED_BEGIN
typedef struct OneWire_Device
{
	uint8_t DeviceID;																// Device ID - number of device detected on onewire interface
	uint8_t ROM_NO[8];																// Device 8 byte memory content from last read
	bool 	CRC_Check;
}OneWire_DevData_t;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct drv_OneWire_Ext_API
{
	uint8_t (*Scan) ( struct _drv_If *pDrvIf, uint8_t MaxDevs);						// Scan OneWire IFace and finfd connected devices
	void*   (*GetDeviceData)( struct _drv_If *pDrvIf, uint8_t DeviceNum);			// Read OneWire devices memory (8 bytes)
} drv_OneWire_Ext_API_t;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct OneWire_Priv
{
	uint8_t LastDiscrepancy;       /*!< Search private */
	uint8_t LastFamilyDiscrepancy; /*!< Search private */
	uint8_t LastDeviceFlag;        /*!< Search private */
	uint8_t ROM_NO[8];
}OneWire_Priv_t;
COMP_PACKED_END

// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_OneWire_If_Spec
{
	const 	CHAL_Signal_t				*sigComm;									// fyzicky Port RX/TX
//	struct  drv_OneWire_Config			Conf;										// pointer na konfiguracnu strukturu
			_ColList_t 					Dev_List;									// list of detected devices - temporary... we will clear this struct for future!
			OneWire_Priv_t				Priv;										// private data
	//struct 	drv_OneWire_Data			Data;									// data structure
} drv_OneWire_If_Spec_t;
COMP_PACKED_END



// ************************************************************************************************
// PRIVATE
// ************************************************************************************************
#define ONEWIRE_CMD_RSCRATCHPAD			0xBE
#define ONEWIRE_CMD_WSCRATCHPAD			0x4E
#define ONEWIRE_CMD_CPYSCRATCHPAD		0x48
#define ONEWIRE_CMD_RECEEPROM			0xB8
#define ONEWIRE_CMD_RPWRSUPPLY			0xB4
#define ONEWIRE_CMD_SEARCHROM			0xF0
#define ONEWIRE_CMD_READROM				0x33
#define ONEWIRE_CMD_MATCHROM			0x55
#define ONEWIRE_CMD_SKIPROM				0xCC

// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const _drv_Api_t drv_OneWire_API_STD;


#endif		// __DRV_ONEWIRE_H_

