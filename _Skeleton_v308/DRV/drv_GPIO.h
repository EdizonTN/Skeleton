// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_GPIO.h
// 	   Version: 3.1
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for GPIO - header file
// ******************************************************************************
// Info:
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
// 				2024.02.26	- v3.2 - fixed filtering signal (Filter_Val)


#if !defined(__DRV_GPIO_H_) 														// prevent to cyclyc loads
 #if defined(USE_DRV_GPIO) && (USE_DRV_GPIO == 1)									// Need to load?
  #if (!defined (_CHIP_GPIO_COUNT) || _CHIP_GPIO_COUNT == 0)						// Neeed it, but MCU haven't this type of periphery!
	#error This driver is need to be used, but MCU doesn't have it!
  #else
	// now -> load it.
#define __DRV_GPIO_H_

#include "Skeleton.h"

// ******************************************************************************************************
// CONFIGURATION
// ******************************************************************************************************

#ifndef CONF_DEBUG_DRV_GPIO
#define CONF_DEBUG_DRV_GPIO			0												// default is OFF
#endif 

#ifndef GPIO_USE_IRQ_MANAGER
#define GPIO_USE_IRQ_MANAGER		1												// defaulkt iuse IRQ MANAGER
#endif
// ******************************************************************************************************
// PUBLIC Defines
// ******************************************************************************************************
COMP_PACKED_BEGIN
typedef struct																		// Additional GPIO structure based on chip specific requirements */
{
		  _CHIP_GPIO_T					*pPeri;										// pointer to GINT
	const uint8_t						Group;										// Group interrtupt - group number
	const CHAL_SIG_IRQ_SENS_t			Sens;										// Sensitivity - condition for IRQ fire(Level low, Level high, Edge rise or Edge fall)
	const bool							IsUsed;										// Is Group Interrupt used?
}ps_IO_GINT_t;
COMP_PACKED_END

COMP_PACKED_BEGIN
typedef struct																		// Additional GPIO structure based on chip specific requirements */
{
	void								*pPeri;										// pointer to PINT
	const uint8_t						Select;										// Pin interrtupt Select 0-7
	const CHAL_SIG_IRQ_SENS_t			Sens;										// Sensitivity - condition for IRQ fire(Level low, Level high, Edge rise or Edge fall)
	const bool							IsUsed;										// Is Group Interrupt used?
}ps_IO_PINT_t;
COMP_PACKED_END

// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_GPIO_If_Spec
{
	const 	uint8_t						PeriIndex;									// index pouzitej periferie
	const CHAL_Signal_t					*pSigIO;									// IO struktura kompletna
//	uint8_t								Value;										// posledna zapisana hodnota
			ps_IO_GINT_t				G_INT;										// group Interrupt config
			ps_IO_PINT_t				P_INT;										// Pin Interrupt
	uint8_t								Phase;										// pressed? how long?,....
	uint8_t 							Filter_Val;									// Filter Value - how many cycles signal must stable
	uint8_t								Filter_Additive;
//	volatile uint_fast64_t				FilterStartCounter;							// start value of counter for filter
} drv_GPIO_If_Spec_t;
COMP_PACKED_END

// ******************************************************************************************************
// PUBLIC 
// ******************************************************************************************************
extern const _drv_Api_t drv_GPIO_API_STD;

// ******************************************************************************************************
// PRIVATE Defines
// ******************************************************************************************************


#endif		// defined (_CHIP_zzz_COUNT)
#endif		// #if defined(USE_DRV_zzz)
#endif		// __DRV_ZZZ_H_
