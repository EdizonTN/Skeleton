// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_DAC.c
// 	   Version: 3.02
//		  Desc: Generic Driver for DAC
// ******************************************************************************
// Info: DAC driver
//
// Notice:
//
// Usage:
//
// ToDo:	--- nedokoncene. len vytvorena kostra !!!!
//
// Changelog:
//				2024.09.30	- v3.02 - add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2023.12.10	- v3.01 - rename _CHIP_DACx_IRQ_Handler to _Chip_DACx_IRQ_Handler
//									- change bootom irq to 	IRQ_Handler_Bottom((void *) DAC_DriverIRQ[x]);
//

#include "Skeleton.h"

#if defined(USE_DRV_DAC) && (USE_DRV_DAC == 1) && (defined (_CHIP_HAVE_DAC) && _CHIP_HAVE_DAC == 1)


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool DAC_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool DAC_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool DAC_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
void DAC_Handler_Top(_drv_If_t *pDrvIf);											// Quick Handler
void DAC_Handler_Bottom(_drv_If_t *pDrvIf);											// Process Handler
void DAC_Events( struct _drv_If *pDrvIf, _EventType_t EventType);

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_DAC_API_STD 		=
{
    .Init			=	DAC_Init,
    .Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
    .Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
    .Enable			=	DAC_Enable,
    .Disable		=	DAC_Disable,
    .Write_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
    .Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
    .Events			=	DAC_Events,
};
#pragma pop

const _drv_If_t *DAC_DriverIRQ[_CHIP_DAC_COUNT];									// Num of the DACs

// ------------------------------------------------------------------------------------------------------
// DAC INTERRUPT HANDLERS
// ------------------------------------------------------------------------------------------------------
#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 0)
void _Chip_DAC0_IRQ_Handler(void)
{
    DAC_Handler_Top((void *) DAC_DriverIRQ[0]);
    //DAC_Handler_Bottom((void *) DAC_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) DAC_DriverIRQ[0]);	
}
#endif

#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 1)
void _Chip_DAC0_IRQ_Handler(void)
{
    DAC_Handler_Top((void *) DAC_DriverIRQ[1]);
    //DAC_Handler_Bottom((void *) DAC_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) DAC_DriverIRQ[1]);	
}
#endif

#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 2)
void _Chip_DAC0_IRQ_Handler(void)
{
    DAC_Handler_Top((void *) DAC_DriverIRQ[2]);
    //DAC_Handler_Bottom((void *) DAC_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) DAC_DriverIRQ[2]);	
}
#endif

#if defined(_CHIP_DAC_COUNT) && (_CHIP_DAC_COUNT > 3)
void _Chip_DAC0_IRQ_Handler(void)
{
    DAC_Handler_Top((void *) DAC_DriverIRQ[3]);
    //DAC_Handler_Bottom((void *) DAC_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) DAC_DriverIRQ[3]);	
}
#endif

// ------------------------------------------------------------------------------------------------------
// Generic
// Inicializacia DAC-u
// ------------------------------------------------------------------------------------------------------
bool DAC_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
    bool res = false;

    drv_DAC_If_Spec_t* pDrvIf_Spec = (drv_DAC_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_DAC) && (CONF_DEBUG_DRV_DAC == 1)
    FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

    // initialization code here:
    pDrvIf_Spec->pPeri = CHAL_DAC_Init(pDrvIf_Spec->PeriIndex);
    if(pDrvIf_Spec->pPeri != NULL) res = true;
    pDrvIf->Sys.Stat.Initialized = res;

    DAC_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;
	
#if defined(CONF_DEBUG_DRV_DAC) && (CONF_DEBUG_DRV_DAC == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif
    return(res);
}


// ------------------------------------------------------------------------------------------------------
// Generic
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
// ------------------------------------------------------------------------------------------------------
bool DAC_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
    bool res = false;

    drv_DAC_If_Spec_t* pDrvIf_Spec = (drv_DAC_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_DAC) && (CONF_DEBUG_DRV_DAC == 1)
    FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
    FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);							// print arguments
#endif

    // main code here:
    res = CHAL_DAC_Enable(pDrvIf_Spec->pPeri, ChannelBitMask, true); 				// enable DAC
    pDrvIf->Sys.Stat.Enabled = res;

#if defined(CONF_DEBUG_DRV_DAC) && (CONF_DEBUG_DRV_DAC == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif
    return(res);
}

// ------------------------------------------------------------------------------------------------------
// Generic
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
// ------------------------------------------------------------------------------------------------------
bool DAC_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
    bool res = false;

    drv_DAC_If_Spec_t* pDrvIf_Spec = (drv_DAC_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_DAC) && (CONF_DEBUG_DRV_DAC == 1)
    FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
    FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);							// print arguments
#endif

    // main code here:
    res = CHAL_DAC_Enable(pDrvIf_Spec->pPeri, ChannelBitMask, false); 				// Complette disable
    pDrvIf->Sys.Stat.Enabled = res;
#if defined(CONF_DEBUG_DRV_DAC) && (CONF_DEBUG_DRV_DAC == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif
    return(res);
}



// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Quick handler
// ------------------------------------------------------------------------------------------------------
inline void DAC_Handler_Top(_drv_If_t *pDrvIf)
{
    drv_DAC_If_Spec_t* pDrvIf_Spec = (drv_DAC_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie
    uint32_t pending = CHAL_DAC_Get_CHIRQ_Status(pDrvIf_Spec->pPeri);

    /* Clear any pending interrupts */
    CHAL_DAC_Clear_CHIRQ_Status(pDrvIf_Spec->pPeri, pending);
}

// ------------------------------------------------------------------------------------------------------
// After handler
// fire event system
// ------------------------------------------------------------------------------------------------------
inline void DAC_Handler_Bottom(_drv_If_t *pDrvIf)
{
   if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_DataReceived);// ak je nastaveny event system, zavolaj ho
}



// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
// ------------------------------------------------------------------------------------------------------
inline void ADC_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system
{

//    switch(EventType)
//    {
//    case EV_DataReceived:
//    case EV_TransmitReady:
//    case EV_TransmitDone:
//    case EV_NoChange:
//    default:
//        break;
//    }
}




#endif	// USE_DRV_DAC
