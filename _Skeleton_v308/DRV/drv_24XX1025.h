// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_24XX1025.h
// 	   Version: 3.0
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Driver for 24XX1025 IC - Header file
// ******************************************************************************
// Info: LCD driver. 
//
// Notice:
//
// Usage: http://ww1.microchip.com/downloads/en/devicedoc/21203m.pdf
//			
// ToDo:
// 			
// Changelog:
// 

#ifndef __DRV_24XX1025_H_
#define __DRV_24XX1025_H_

#include "Skeleton.h"


// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************
#ifndef CONF_DEBUG_DRV_24XX1025
#define CONF_DEBUG_DRV_24XX1025				0										// default is OFF
#endif	

// ************************************************************************************************
// CONST for 24XX1025:
#define DEF_24XX1025_CAPACITY				(size_t)((1*1024*1024)/8)				// EEPROM Capacity [bytes] = 1Mbit	(Max Address for uint8_t: 0x1FFFF)
#define DEF_24XX1025_BUS_MAXFREQKHZ			400										// EEPROM interface Max frequency [kHz] *Datasheet Value

//http://ww1.microchip.com/downloads/en/devicedoc/21203m.pdf
//Part			VCC			Max. Clock		Temp.
//Number		Range		Frequency		Ranges
//24AA256 		1.8-5.5V 	400 kHz(1) 		I
//24LC256 		2.5-5.5V 	400 kHz 		I, E
//24FC256 		1.8-5.5V 	1 MHz(2) 		I
//Note 1: 100 kHz for VCC < 2.5V.
//2: 400 kHz for VCC < 2.5V.
#define DEF_24XX1025_MEMADR_SIZEBYTE		2										// EEPROM internal address size in byte



// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************

// Extended API functions for this driver:
COMP_PACKED_BEGIN
typedef struct drv_24xx1025_Ext_API
{
	size_t	(*Read_MemTotalSize)	( _drv_If_t *pDrvIf);							// Get Total Memory Size
	size_t	(*Read_MemFreeSize)		( _drv_If_t *pDrvIf);							// Get Free Memory Size
} drv_24xx1025_Ext_API_t;
COMP_PACKED_END


// Additions for Generic driver structure here:
COMP_PACKED_BEGIN
typedef struct drv_24xx1025_If_Spec
{
	const 	CHAL_Signal_t			*pSig_WP;										// 24xx1025 Input - Write Protect
			_drv_If_t				*pCommInterface;
			I2C_Device_t			I2C_Device;	
} drv_24xx1025_If_Spec_t;
COMP_PACKED_END

// ************************************************************************************************
// PRIVATE
// ************************************************************************************************



// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const _drv_Api_t drv_24xx1025_API_STD;

#endif		// __DRV_24XX1025_H_

