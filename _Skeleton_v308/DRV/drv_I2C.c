// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_I2C.c
// 	   Version: 5.51
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for I2C (Ex: http://files.svenbrauch.de/h1telescope/receiver-firmware/lpc_core/lpc_ip/i2c_001.c)
// ******************************************************************************
// Info: I2C driver
//
// Notice:	ONLY I2C MASTER MODE YET !!!!
//
// Usage:
//	
// ToDo:	- znizenie CONF_DRV_I2C_PING_WAIT_TIMEOUT_CLKS na 10 znamena nefunkcny PING - aky to ma suvis????
//			- AUTOCONFIGURE doesn't working!
// 			- Scan_I2C must create new found device collection. Dont' Erase old one! Return will not bool, byt ptr to device collection.
//			- add slave/monitor mode
//			- finalize interrupt and DMA transfer !
//
// Changelog:	
//				2024.09.30	- v5.51 - add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2024.02.18	- v5.5	- fix bus speed setting from speed mode
//				2024.02.03	- v5.4	- modify to new sys_buffer_create parameter
//				2023.12.07	- v5.3 	- update to using without collection also
//									- fix typo
//									- fix set/get speed error, fix interrupt process CHIP_I2C_MSSTATE_MASK
//									- add set pDrvIf_Spec->pDrvIf to Tnit
//				2022.11.30	- v5.2 - change system of load
//				2022.11.28	- changed interrupt handler bottom
//				2021.03.09	- Add slave mode
//				2020.11.12	- Callback system rewritted
//				2020.11.11	- CONF_DRV_I2C_ARG_REG_ADR_SIZE constant added
// 				2020.08.18	- Moved I2C_Xfer_t struct def from Chip.Hal.h 
//				2020.06.16	- while(tmpListRecord) changed
//				2020.07.01	- rename Old: CHAL_xxx_IRQ_Enable   ----->  CHAL_xxx_Enable_IRQ_NVIC
//							- rename Old: _Chip_xxx_IRQMask_Enable --> 	_Chip_xxx_IRQ_Peri_Enable
//							- rename Old: _Chip_xxx_IRQ_Enable_CHIRQ --> _Chip_xxx_IRQ_Peri_Enable  also parameters renamed
// 				2019.11.11	- Read/write - pbuff changed to sys_Buffer_t


#include "Skeleton.h"

#ifdef __DRV_I2C_H_																	// Header was loaded?


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool I2C_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool I2C_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool I2C_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool I2C_Write_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType);
uint32_t I2C_Read_Data (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t RdLen, _XFerType_t XferType);
void I2C_Events( struct _drv_If *pDrvIf, _EventType_t EventType);
void I2C_Handler_Top(_drv_If_t *pDrvIf);											// Quick Handler

uint8_t I2C_Scan (struct _drv_If *pDrvIf, uint16_t MaxAddr, uint16_t MaxDevs, bool CreateDevs);
bool I2C_Ping (struct _drv_If *pDrvIf, uint16_t Dev_HWAddr);
bool I2C_Set_BusSpeed(struct _drv_If *pDrvIf, uint32_t MaxSpeed_kHz);
bool I2C_Device_AutoConfigure (struct _drv_If *pDrvIf, I2C_Device_t *pDevice);
I2C_Device_t* I2C_Get_DeviceByAddr (struct _drv_If *pDrvIf, uint16_t DevHWAddr, bool Rescan);
bool I2C_WaitForSetStat (drv_I2C_If_Spec_t *pDrvIf_Spec, uint32_t WaitFor_State, uint32_t NumOfTimeOutCLKPulses);

// ******************************************************************************************************
// Public Function - specific for this driver
// ******************************************************************************************************
const drv_I2C_Ext_API_t	I2C_Ext_API_STD 	=  										// Extended (non-standard) API's for this driver
{
	.Scan			=	I2C_Scan,													// Bus scan for device
	.Ping			=	I2C_Ping,													// Ping device - adrees it and chceck ACK.
	.Set_BusSpeed	=	I2C_Set_BusSpeed,											// change transfer speed
	.Device_AutoConfigure = I2C_Device_AutoConfigure,
	.Get_DeviceByAddr=	I2C_Get_DeviceByAddr
};

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_I2C_API_STD 		= 
{
 	.Init			=	I2C_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	I2C_Enable,
	.Disable		=	I2C_Disable,
	.Write_Data		=	I2C_Write_Data,
	.Read_Data		=	I2C_Read_Data,
	.Events			=	I2C_Events,
	.Ext			=	(drv_I2C_Ext_API_t*) &I2C_Ext_API_STD
};
#pragma pop




// ******************************************************************************************************
// Private
// ******************************************************************************************************
const _drv_If_t *I2C_DriverIRQ[_CHIP_I2C_COUNT];													// I2C





// ------------------------------------------------------------------------------------------------------
// I2C INTERRUPT HANDLER
// ------------------------------------------------------------------------------------------------------
#if defined(_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 0)
void _Chip_I2C0_IRQ_Handler(void)	
{
	I2C_Handler_Top((void *) I2C_DriverIRQ[0]);
	//I2C_Handler_Bottom((void *) I2C_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) I2C_DriverIRQ[0]);
}
#endif

#if defined(_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 1)
void _Chip_I2C1_IRQ_Handler(void)	
{
	I2C_Handler_Top((void *) I2C_DriverIRQ[1]);
	//I2C_Handler_Bottom((void *) I2C_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) I2C_DriverIRQ[1]);
}
#endif

#if defined(_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 2)
void _Chip_I2C2_IRQ_Handler(void)	
{
	I2C_Handler_Top((void *) I2C_DriverIRQ[2]);
	//I2C_Handler_Bottom((void *) I2C_DriverIRQ[2]);
	IRQ_Handler_Bottom((void *) I2C_DriverIRQ[2]);
}
#endif

#if defined(_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 3)
void _Chip_I2C3_IRQ_Handler(void)	
{
	I2C_Handler_Top((void *) I2C_DriverIRQ[3]);
	//I2C_Handler_Bottom((void *) I2C_DriverIRQ[3]);
	IRQ_Handler_Bottom((void *) I2C_DriverIRQ[3]);
}
#endif

#if defined(_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 4)
void _Chip_I2C4_IRQ_Handler(void)	
{
	I2C_Handler_Top((void *) I2C_DriverIRQ[4]);
	//I2C_Handler_Bottom((void *) I2C_DriverIRQ[4]);
	IRQ_Handler_Bottom((void *) I2C_DriverIRQ[4]);
}
#endif

#if defined(_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 5)
void _Chip_I2C5_IRQ_Handler(void)	
{
	I2C_Handler_Top((void *) I2C_DriverIRQ[5]);
	//I2C_Handler_Bottom((void *) I2C_DriverIRQ[5]);
	IRQ_Handler_Bottom((void *) I2C_DriverIRQ[5]);
}
#endif

#if defined(_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 6)
void _Chip_I2C6_IRQ_Handler(void)	
{
	I2C_Handler_Top((void *) I2C_DriverIRQ[6]);
	//I2C_Handler_Bottom((void *) I2C_DriverIRQ[6]);
	IRQ_Handler_Bottom((void *) I2C_DriverIRQ[6]);
}
#endif

#if defined(_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 7)
void _Chip_I2C7_IRQ_Handler(void)	
{
	I2C_Handler_Top((void *) I2C_DriverIRQ[7]);
	//I2C_Handler_Bottom((void *) I2C_DriverIRQ[7]);
	IRQ_Handler_Bottom((void *) I2C_DriverIRQ[7]);
}
#endif

#if defined(_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 8)
void _Chip_I2C8_IRQ_Handler(void)	
{
	I2C_Handler_Top((void *) I2C_DriverIRQ[8]);
	//I2C_Handler_Bottom((void *) I2C_DriverIRQ[8]);
	IRQ_Handler_Bottom((void *) I2C_DriverIRQ[8]);
}
#endif

#if defined(_CHIP_I2C_COUNT) && (_CHIP_I2C_COUNT > 9)
void _Chip_I2C9_IRQ_Handler(void)	
{
	I2C_Handler_Top((void *) I2C_DriverIRQ[9]);
	//I2C_Handler_Bottom((void *) I2C_DriverIRQ[9]);
	IRQ_Handler_Bottom((void *) I2C_DriverIRQ[9]);
}
#endif

// ------------------------------------------------------------------------------------------------------
// Generic
// Inicializacia I2C-u
bool I2C_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;
	
	drv_I2C_If_Spec_t* pDrvIf_Spec = (drv_I2C_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> ParentMod: %s", pRequestorModIf->Name);
#endif		

	// initialization code here:

	if(((drv_I2C_If_Spec_t*) pDrvIf->pDrvSpecific)->pDrvIf == NULL)	((drv_I2C_If_Spec_t*) pDrvIf->pDrvSpecific)->pDrvIf = pDrvIf;					// set root driver
	
	res = CHAL_Signal_Init(pDrvIf_Spec->pSig_SDA);
	res |= CHAL_Signal_Init(pDrvIf_Spec->pSig_SCL);
	
	
	pDrvIf_Spec->pPeri = CHAL_I2C_Init(pDrvIf_Spec->PeriIndex);
	if(pDrvIf_Spec->pPeri) res |= true;
	if(pDrvIf_Spec->BusConf.CurrentSpeedMode == 0) pDrvIf_Spec->BusConf.CurrentSpeedMode = I2C_SPEED_MODE_STANDARD;	// if not set, select STANDARD mode
	
	if(res == true)
	{
		res = CHAL_I2C_Configure (pDrvIf_Spec->pPeri, I2C_GET_SPEED_KHZ_FROM_MODE(I2C_SPEED_MODE_STANDARD), pDrvIf_Spec->BusMode, pDrvIf_Spec->Address);	// configure to standard bus speed
	}
	pDrvIf_Spec->BusConf.CurrentSpeed_kHz = CHAL_I2C_Get_BusSpeed( pDrvIf_Spec->pPeri);		// read current speed in kHz
	pDrvIf_Spec->BusConf.CurrentSpeedMode =  I2C_GET_MODE_FROM_SPEED_KHZ(pDrvIf_Spec->BusConf.CurrentSpeed_kHz);	// read current nominal mode
	// Speed setting will be changet based on target device and their settings. If device settings doesn't exist, I2C will use this default one.
	
	
	pDrvIf->Sys.Stat.Initialized = res;

	if(res == true) I2C_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;

	pDrvIf_Spec->pXFer_Dev = NULL;													// No selected device for XFer
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}



// ------------------------------------------------------------------------------------------------------
// Generic
// ENABLE periphery
// ChannelBitMask is ignored for this driver
bool I2C_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	drv_I2C_If_Spec_t* pDrvIf_Spec = (drv_I2C_If_Spec_t*) pDrvIf->pDrvSpecific;		// ptr to specific struct of driver
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDev: %p, chMask: 0x%.8x", pDrvIf_Spec->pXFer_Dev, ChannelBitMask);
#endif	

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	if(pDrvIf->Sys.Stat.Initialized == true)
#endif	
	{
		// main code here:
		
		if(pDrvIf_Spec->BusMode == CHAL_I2C_Slave) 									// slave I2C
		{
			res = CHAL_I2CSLV_Enable(pDrvIf_Spec->pPeri, true); 
			if(res) res = CHAL_I2CSLV_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, true); 	// enable I2C Slave IRQ
		}
		if(pDrvIf_Spec->BusMode == CHAL_I2C_Monitor) 								// Monitor I2C
		{
			res = CHAL_I2CMON_Enable(pDrvIf_Spec->pPeri, true); 
			if(res) res = CHAL_I2CMON_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, true); 	// enable I2C Monitor IRQ
		}
		
		//in MASTER mode is this enabled in read/write fn
		if(res == true) pDrvIf->Sys.Stat.Enabled = true;							// Enable was succesfull
	}
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// Generic
// DISABLE funkcie zariadenia.
// ChannelBitMask is ignored for this driver
bool I2C_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	drv_I2C_If_Spec_t* pDrvIf_Spec = (drv_I2C_If_Spec_t*) pDrvIf->pDrvSpecific;		// ptr to specific struct of driver

#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDev: %p, chMask: 0x%.8x", pDrvIf_Spec->pXFer_Dev, ChannelBitMask);
#endif	

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	if(pDrvIf->Sys.Stat.Initialized == true)
#endif	
	{
		// main code here:
		if(pDrvIf_Spec->BusMode == CHAL_I2C_Slave) 									// slave I2C
		{
			res = CHAL_I2CSLV_Enable(pDrvIf_Spec->pPeri, false); 
			if(res) res = CHAL_I2CSLV_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false); 	// disable I2C Slave IRQ
		}
		if(pDrvIf_Spec->BusMode == CHAL_I2C_Monitor) 								// Monitor I2C
		{
			res = CHAL_I2CMON_Enable(pDrvIf_Spec->pPeri, false); 
			if(res) res = CHAL_I2CMON_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false); 	// disable I2C Monitor IRQ
		}
		if(pDrvIf_Spec->BusMode == CHAL_I2C_Master) 								// Master I2C
		{
			res = CHAL_I2CMON_Enable(pDrvIf_Spec->pPeri, false); 
			if(res) res = CHAL_I2CMON_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false); 	// disable I2C Monitor IRQ
		}
	
		if(res == true) pDrvIf->Sys.Stat.Enabled = false;							// disable was succesfull
	}
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
	return(res);
}



// ------------------------------------------------------------------------------------------------------
// USR
// pbuff - Value to write
// BuffByteLen - number of bytes for write to device regs or to device memory
// pDrvIf_Spec->pXFerDev must be set (dest. I2C Device)!
// pbuff[0] start of data array to send
// Write Address first and next xfer data array
bool I2C_Write_Data( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t WrLen, _XFerType_t XferType)
{
	bool res = false;
	drv_I2C_If_Spec_t* pDrvIf_Spec = (drv_I2C_If_Spec_t*) pDrvIf->pDrvSpecific;
		
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDev: %p, pSrc: %p, BuffByteLen: %d, XFerType: 0x%.1X", pDrvIf_Spec->pXFer_Dev, pbuff, WrLen, XferType);
#endif	

	
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	if(pDrvIf_Spec->pXFer_Dev == NULL) res = false;									// I2C Device not set
	else res = true;
#endif		
	
	if(pDrvIf_Spec->XFer_Rec.Status == CHAL_I2C_STAT_BUSY) res = false;				// I2C device is occupied by another process
	else res = true;
	
	if(res == true) 
	{
		if(pDrvIf_Spec->pXFer_Dev->Dev_Parms.SpeedMode != pDrvIf_Spec->BusConf.CurrentSpeedMode)	// check speed settings for this device
			res = I2C_Set_BusSpeed(pDrvIf, I2C_GET_SPEED_KHZ_FROM_MODE(pDrvIf_Spec->pXFer_Dev->Dev_Parms.SpeedMode));	// Set Bus speed by device maximum to std speed.
	}
	
	if(res == true)
	{
		// Write Data to I2C device

		// prepare for send:
		if(pDrvIf_Spec->pXFer_Dev->TxBuffIncrement > CONF_DRV_I2C_MAX_DATA_SIZE) pDrvIf_Spec->pXFer_Dev->TxBuffIncrement = CONF_DRV_I2C_MAX_DATA_SIZE;
		
		pDrvIf_Spec->XFer_Rec.DstAdd = pDrvIf_Spec->pXFer_Dev->Dev_HWAddr;			// Set I2C device adress
		pDrvIf_Spec->XFer_Rec.pRxBuff = NULL;										// receive not active now
		//pDrvIf_Spec->XFer_Rec.pTxBuff = pbuff->pDataArray;						// Set pointer to read position of the src data buffer
		pDrvIf_Spec->XFer_Rec.pTxBuff = pbuff;										// Set pointer to read position of the src data buffer
#if (CONF_SYS_BUFFERS_USED == 1)
		pbuff->pRd = pbuff->pDataArray;												// Set rd pointer to start
#endif		
		pDrvIf_Spec->XFer_Rec.pTxDevReg = &pDrvIf_Spec->pXFer_Dev->Arg_RegAdr[0];	// address of register in device, or Dst address of device if it memory - read from arguments
		pDrvIf_Spec->XFer_Rec.TxDevRegSize = pDrvIf_Spec->pXFer_Dev->Dev_Parms.RegAdrSize;// size of these registers/address fields
		
		pDrvIf_Spec->XFer_Rec.RxBuffSize = 0;										// without receive data
		pDrvIf_Spec->XFer_Rec.TxSize = WrLen;										// Xfer data size
			
		// prepare for send:
		pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_BUSY;							// Status to BUSY

		// Send as Master:
		CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);						// Disable I2C IRQ
		
		CHAL_I2CMST_Clear_IRQ_Status(pDrvIf_Spec->pPeri, UINT32_MAX);				// Clear all I2C IRQ
		CHAL_I2CTimeout_Clear_IRQ_Status(pDrvIf_Spec->pPeri, UINT32_MAX);				// Clear all I2C Timeout IRQ Status

		CHAL_I2CMST_Enable(pDrvIf_Spec->pPeri, true);								// Enable Master I2C		
		
		CHAL_I2CTimeout_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_I2C_IRQSTAT_EVENTTIMEOUT, true);		
		
		CHAL_I2CMST_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_I2C_IRQSTAT_MSTPENDING | CHAL_I2C_IRQSTAT_MSTRARBLOSS | CHAL_I2C_IRQSTAT_MSTSTSTPERR, true);
		
		CHAL_I2CTimeout_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_I2C_IRQSTAT_EVENTTIMEOUT, true);

		CHAL_I2CMST_Set_Start(pDrvIf_Spec->pPeri);									// Create Start if Status is PENDING
		
		CHAL_I2CMST_Put_Data(pDrvIf_Spec->pPeri, pDrvIf_Spec->XFer_Rec.DstAdd | 0x00);// write address + RW bit:0

		CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, true);						// Enable I2C IRQ
		
		// all next transfer is controlled by Interrupt handler
		if(XferType == XFer_Blocking) 
		{
			res = I2C_WaitForSetStat( pDrvIf_Spec, CHAL_I2C_STAT_OK, CONF_DRV_I2C_WRADDR_WAIT_TIMEOUT_CLKS);			// wait for OK. If timeout or error ocurred, result is false
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
			if(res == false) FN_DEBUG_FMTPRINT(" >> Xfer timeout!")
#endif		
			CHAL_I2CMST_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_I2C_IRQSTAT_MSTPENDING | CHAL_I2C_IRQSTAT_MSTRARBLOSS | CHAL_I2C_IRQSTAT_MSTSTSTPERR, false);
			CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);					// Disable I2C Master IRQ NVIC
			pDrvIf_Spec->XFer_Rec.Status &= ~CHAL_I2C_STAT_BUSY;					// clear busy flag, store last error only
		}else res = true;															// XFer was started, but not finishet yet.....
	}
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------
// Read from I2C BUS Device
// MaxLen - number of bytes for read from device regs or from device memory 
// pDrvIf_Spec->pXFerDev must be set (dest. I2C Device)!
// return: Result: readed data count. Data will be placed into pbuf from [0]
uint32_t I2C_Read_Data(_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, sys_Buffer_t *pbuff, uint32_t RdLen, _XFerType_t XferType)
{
	drv_I2C_If_Spec_t* pDrvIf_Spec = (drv_I2C_If_Spec_t*) pDrvIf->pDrvSpecific;
	bool res = false;
	uint32_t Result = 0;
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> pDev: %p, pDst: %p, MaxLen: %d, XFerType: 0x%.1X", pDrvIf_Spec->pXFer_Dev, pbuff, RdLen, XferType);
#endif	

#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	if(pDrvIf_Spec->pXFer_Dev == NULL) res = false;									// I2C Device not set
	else res = true;
#endif	
	
	if(pDrvIf_Spec->XFer_Rec.Status == CHAL_I2C_STAT_BUSY) res = false;				// I2C device is occupied by another process
	else res = true;
	
	if(res == true) 
	{																				// Chage I2C Bus speed if needs
		if(pDrvIf_Spec->pXFer_Dev->Dev_Parms.SpeedMode != pDrvIf_Spec->BusConf.CurrentSpeedMode)	// check speed settings for this device
			res = I2C_Set_BusSpeed(pDrvIf,I2C_GET_SPEED_KHZ_FROM_MODE(pDrvIf_Spec->pXFer_Dev->Dev_Parms.SpeedMode));	// Set Bus speed by device maximum to std speed.
	}
	
	if(res == true)																	// input parameters looks OK
	{
		// Write Data to I2C device

		// prepare for send:
		// firstly I2C send I2C address + Device internal regs/address (1-x bytes)
		// next will be read data from Device
		Result = RdLen;
		
		if(pDrvIf_Spec->pXFer_Dev->TxBuffIncrement > CONF_DRV_I2C_MAX_DATA_SIZE) pDrvIf_Spec->pXFer_Dev->TxBuffIncrement = CONF_DRV_I2C_MAX_DATA_SIZE;
		
		pDrvIf_Spec->XFer_Rec.DstAdd = pDrvIf_Spec->pXFer_Dev->Dev_HWAddr;			// Set I2C device adress		
		//pDrvIf_Spec->XFer_Rec.pRxBuff = pbuff->pDataArray;						// Set write position of the destination buffer
		pDrvIf_Spec->XFer_Rec.pRxBuff = pbuff;										// Set write position of the destination buffer
		pDrvIf_Spec->XFer_Rec.pTxBuff = NULL;										// pointer for register/address buffer

		pDrvIf_Spec->XFer_Rec.pTxDevReg = &pDrvIf_Spec->pXFer_Dev->Arg_RegAdr[0];	// address of register in device, or Dst address of device if it memory - read from arguments
		pDrvIf_Spec->XFer_Rec.TxDevRegSize = pDrvIf_Spec->pXFer_Dev->Dev_Parms.RegAdrSize;// size of these registers/address fields
		
		pDrvIf_Spec->XFer_Rec.RxBuffSize = RdLen;									// without receive data
		pDrvIf_Spec->XFer_Rec.TxSize = 0;											// Lenght of Data
		
		// prepare for send Address and next read:
		pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_BUSY;							// Status to BUSY

		CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);						// Disable I2C IRQ NVIC
		
		CHAL_I2CMST_Clear_IRQ_Status(pDrvIf_Spec->pPeri, UINT32_MAX);				// Clear all I2C MASTER IRQ
		CHAL_I2CTimeout_Clear_IRQ_Status(pDrvIf_Spec->pPeri, UINT32_MAX);			// Clear all I2C Timeout IRQ
		
		CHAL_I2CMST_Enable(pDrvIf_Spec->pPeri, true);								// Enable Master I2C
		
		CHAL_I2CMST_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_I2C_IRQSTAT_MSTPENDING | CHAL_I2C_IRQSTAT_MSTRARBLOSS | CHAL_I2C_IRQSTAT_MSTSTSTPERR, true);
		
		CHAL_I2CTimeout_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_I2C_IRQSTAT_EVENTTIMEOUT, true);
		
		CHAL_I2CMST_Set_Start(pDrvIf_Spec->pPeri);									// Create Start if Status is PENDING
		CHAL_I2CMST_Put_Data(pDrvIf_Spec->pPeri, pDrvIf_Spec->XFer_Rec.DstAdd | 0x00);// write address + RW bit:0

		CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, true);						// Enable I2C Master IRQ
		
		// all next transfer is controlled by Interrupt handler
		if(XferType == XFer_Blocking) 
		{
			res = I2C_WaitForSetStat( pDrvIf_Spec, CHAL_I2C_STAT_OK, CONF_DRV_I2C_WRADDR_WAIT_TIMEOUT_CLKS);			// wait for OK. If timeout or error ocurred, result is false
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
			if(res == false) FN_DEBUG_FMTPRINT(" >> Xfer timeout!")
#endif
			CHAL_I2CMST_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_I2C_IRQSTAT_MSTPENDING | CHAL_I2C_IRQSTAT_MSTRARBLOSS | CHAL_I2C_IRQSTAT_MSTSTSTPERR, false);
			CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);					// Disable I2C Master IRQ NVIC
			pDrvIf_Spec->XFer_Rec.Status &= ~CHAL_I2C_STAT_BUSY;					// clear busy flag, store last error only
			Result = RdLen - pDrvIf_Spec->XFer_Rec.RxBuffSize;						// pDrvIf_Spec->XferRec.RxBuff Size must be Zero.
		}
	}
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("Xfer: %d", Result);
#endif		
	
	return(Result);																	// return count of physically readed data
}


// ------------------------------------------------------------------------------------------------------
// PRIVATE
// WaitFor set selected State. With timeout counting. Timeout_Counter is cleared in each peri Interrupt.
// Return: True if State was cleared, False if timeout occured
// Used only for Blocked Transfer Type
COMP_PUSH_OPTIONS
COMP_OPTIONS_O0
bool I2C_WaitForSetStat (drv_I2C_If_Spec_t *pDrvIf_Spec, uint32_t WaitFor_State, uint32_t NumOfTimeOutCLKPulses)
{
	uint32_t Timeout_Value;
	Timeout_Value = (sys_System.Clock->CPU_Freq / 1000) ;
	Timeout_Value = Timeout_Value / CHAL_I2C_Get_BusSpeed(pDrvIf_Spec->pPeri); // get number of CPU tick per one peri CLK pulse
	Timeout_Value = (Timeout_Value * NumOfTimeOutCLKPulses) / 13;					// wait n cycles. 13 is asm instruction in do...while
	pDrvIf_Spec->Timeout_Counter = 0;												// reset counter
	while((pDrvIf_Spec->XFer_Rec.Status & WaitFor_State) != WaitFor_State) 			// wait while STAT is occured
	{ 
		if(pDrvIf_Spec->Timeout_Counter < Timeout_Value) pDrvIf_Spec->Timeout_Counter ++;
		else 																		// timout occured !!!!!!!!!
		{
			pDrvIf_Spec->XFer_Rec.Status |= (CHAL_I2C_Xfr_Status_t) (CHAL_I2C_STAT_ERROR | CHAL_I2C_STAT_TIMEOUT);	// add a timeout error also
			return(false);															// Timeout Error
		}
		if(pDrvIf_Spec->XFer_Rec.Status & CHAL_I2C_STAT_ERROR) return(false);		// or some error occured
	}	
	if(pDrvIf_Spec->XFer_Rec.Status & CHAL_I2C_STAT_ERROR) return(false);			// some error occured
	return(true);
}
COMP_POP_OPTIONS



// ------------------------------------------------------------------------------------------------------
// PRIVATE
// Create device - allocate memory and return handle. Set DevID also.
#pragma diag_suppress 550	
I2C_Device_t* I2C_CreateDevice(struct _drv_If *pDrvIf, uint16_t I2CHWAddr)
{
	bool res = false;
	I2C_Device_t*	result;
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> HWADDR: 0x%.2X", I2CHWAddr);
#endif	
	
	
	result = (void *)sys_malloc(sizeof(I2C_Device_t));								// allocate piece of memory
	if (result != NULL) 
	{
		res = true;
		result->Dev_HWAddr = I2CHWAddr;												// Save Address
		result->Dev_Config.CurrentSpeedMode = I2C_SPEED_MODE_UNKNOWN;
		result->Dev_Config.CurrentSpeed_kHz = 1;
		result->Dev_Parms.SpeedMode = I2C_SPEED_MODE_UNKNOWN;						// set minimal speed
		result->Dev_Parms.XferMode = CHAL_I2C_Slave;								// default, device is in Slave Mode
		result->Dev_Parms.RegAdrSize = 0;
		sys_memset(result->Arg_RegAdr,0,CONF_DRV_I2C_ARG_REG_ADR_SIZE);
#if (CONF_SYS_BUFFERS_USED == 1)
		result->pBuff = sys_Buffer_Create(CONF_DRV_I2C_BUFFERSIZE, CONF_DRV_I2C_BUFFERELEMENTSIZE, sBuffRing, NULL);	// comm buffer
#else
		result->pBuff = sys_malloc(CONF_DRV_I2C_BUFFERSIZE * CONF_DRV_I2C_BUFFERELEMENTSIZE);		// simply allocate memory
#endif		
		if(result->pBuff == NULL)													// cannot create buffer
		{
			result = NULL;
		}
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
		else FN_DEBUG_FMTPRINT(" >> pBuff: %p", result->pBuff);
#endif		
	}

#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("%s, Ptr: %p", FN_DEBUG_RES_BOOL(res), result);
#endif		

	return(result);
}
#pragma diag_default 550


// ------------------------------------------------------------------------------------------------------
// PRIVATE
// Destroy device - deallocate memory.
#pragma diag_suppress 550
void I2C_DestroyDevice(struct _drv_If *pDrvIf, I2C_Device_t *pDev)
{
	bool res = false;
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> Ptr: %p", pDevice);
#endif	

	if(pDev) 
	{
		sys_free(pDev);																// clear memory
		res = true;
	}
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif		
}
#pragma diag_default 550




// ******************************************************************************************************
// Extended functions
// ******************************************************************************************************


// ------------------------------------------------------------------------------------------------------
// EXT
// Change I2C Bus speed and check change
// Return: True if Change was OK, otherwise False
bool I2C_Set_BusSpeed(struct _drv_If *pDrvIf, uint32_t MaxSpeed_kHz)
{
	bool res = false;
	drv_I2C_If_Spec_t* pDrvIf_Spec = (drv_I2C_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie
	uint32_t BusSpeed_kHz;
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >>MewSpeed: %d kHz", MaxSpeed_kHz);
#endif	
	SYS_CPUCRIT_START();	
	res = CHAL_I2C_Set_BusSpeed(pDrvIf_Spec->pPeri, MaxSpeed_kHz);
	SYS_CPUCRIT_END();	

	
	BusSpeed_kHz = CHAL_I2C_Get_BusSpeed(pDrvIf_Spec->pPeri);
	if((res == true) && (BusSpeed_kHz <= MaxSpeed_kHz)) res = true;
	else res = false;
	
	pDrvIf_Spec->pXFer_Dev->Dev_Config.CurrentSpeed_kHz = BusSpeed_kHz;				// save current Speed to device
	pDrvIf_Spec->pXFer_Dev->Dev_Config.CurrentSpeedMode = I2C_GET_MODE_FROM_SPEED_KHZ(BusSpeed_kHz);	// save current Speed Mode to device

	
	pDrvIf_Spec->BusConf.CurrentSpeed_kHz = BusSpeed_kHz;							// store actual speed in kHz
	pDrvIf_Spec->BusConf.CurrentSpeedMode = I2C_GET_MODE_FROM_SPEED_KHZ(BusSpeed_kHz);	// store comm. mode

	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("NewSpeed:: %d", BusSpeed_kHz);
#endif	
	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// EXT
// Address device Dev_HWAddr and check ACKnowledge.
// Retur: ture if device is alive, otherwise false
bool I2C_Ping (struct _drv_If *pDrvIf, uint16_t Dev_HWAddr)
{
	drv_I2C_If_Spec_t* pDrvIf_Spec = (drv_I2C_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer to specific struct of periphery
	bool res = false;
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >>DevAdr: 0x%.4X", Dev_HWAddr);
#endif	

	pDrvIf_Spec->XFer_Rec.DstAdd = Dev_HWAddr;										// set dev address
	pDrvIf_Spec->XFer_Rec.pRxBuff = NULL; //(uint8_t*) &RXBuff;						// Set destination buffer
	pDrvIf_Spec->XFer_Rec.pTxBuff = NULL;											// pointer for register/address buffer

	pDrvIf_Spec->XFer_Rec.pTxDevReg = NULL;											// adresa registra v device
	pDrvIf_Spec->XFer_Rec.TxDevRegSize = 0;											// velkost tohoto registra
	
	pDrvIf_Spec->XFer_Rec.RxBuffSize = 0;											// without receive data
	pDrvIf_Spec->XFer_Rec.TxSize = 0;												// Lenght of Data
	
	// prepare for send:
	pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_BUSY;								// Status to BUSY

	// Send address without R/W data and wait for ACK:
	CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);							// Disable I2C IRQ
	
	CHAL_I2CMST_Clear_IRQ_Status(pDrvIf_Spec->pPeri, UINT32_MAX);					// Clear all I2C MASTER IRQ Status
	CHAL_I2CTimeout_Clear_IRQ_Status(pDrvIf_Spec->pPeri, UINT32_MAX);				// Clear all I2C Timeout IRQ Status
	
	CHAL_I2CMST_Enable(pDrvIf_Spec->pPeri, true);									// Enable I2C


	CHAL_I2CMST_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_I2C_IRQSTAT_MSTPENDING | CHAL_I2C_IRQSTAT_MSTRARBLOSS | CHAL_I2C_IRQSTAT_MSTSTSTPERR, true);
	CHAL_I2CTimeout_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_I2C_IRQSTAT_EVENTTIMEOUT, true);

	CHAL_I2CMST_Set_Start(pDrvIf_Spec->pPeri);										// Create Start if Status is PENDING
	
	CHAL_I2CMST_Put_Data(pDrvIf_Spec->pPeri, pDrvIf_Spec->XFer_Rec.DstAdd | 0x01);	// write address + RW bit:1
	//CHAL_I2CMST_Put_Data(pDrvIf_Spec->pPeri, 0x90 | 0x01);	// write address + RW bit:1

	CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, true);							// Enable I2C IRQ

	// all next transfer is controlled by Interrupt handler, now wait for end xfer.
	res = I2C_WaitForSetStat( pDrvIf_Spec, CHAL_I2C_STAT_OK, CONF_DRV_I2C_PING_WAIT_TIMEOUT_CLKS);	// wait for OK. If timeout or error ocurred, result is false
	CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);							// Disable I2C IRQ

	if(pDrvIf_Spec->XFer_Rec.Status == CHAL_I2C_STAT_OK)							// Read transfer status. Was are OK?
		res = true;				
	else 
	{
		res = false;
		pDrvIf_Spec->XFer_Rec.Status &= ~CHAL_I2C_STAT_BUSY;						// clear busy flag, store last error only
		CHAL_I2CMST_Set_Stop(pDrvIf_Spec->pPeri);										// Create Stop

		// use wait for to fully generating stiop impulse
		I2C_WaitForSetStat (pDrvIf_Spec, 0xFF, CONF_DRV_I2C_PING_WAIT_TIMEOUT_CLKS);	// timeout occured? Wait 64 I2C CLK pulses
	}

	CHAL_I2CMST_Enable_IRQ(pDrvIf_Spec->pPeri, CHAL_I2C_IRQSTAT_MSTPENDING | CHAL_I2C_IRQSTAT_MSTRARBLOSS | CHAL_I2C_IRQSTAT_MSTSTSTPERR, false);
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif	
		
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// EXT
// Address device Dev_HWAddr and check ACKnowledge. a try in some I2C speed and find fastest one
// Return: True if configuration was changed, otherwise false
bool I2C_Device_AutoConfigure (struct _drv_If *pDrvIf, I2C_Device_t *pDevice)
{
	bool res;
	uint32_t CurrSpeed_kHz;
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >>DevAdr: 0x%.4X", pDevice->Dev_HWAddr);
#endif	
	
	drv_I2C_If_Spec_t* pDrvIf_Spec = (drv_I2C_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer to specific struct of periphery
	uint32_t OldSpeed = CHAL_I2C_Get_BusSpeed( pDrvIf_Spec->pPeri);
	
	CurrSpeed_kHz = 1;
	
	do
	{
		res = CHAL_I2C_Set_BusSpeed(pDrvIf_Spec->pPeri, CurrSpeed_kHz);				// try to set new speed
		if(res == false) 
		{
			if(pDevice->Dev_Config.CurrentSpeed_kHz < 100) pDevice->Dev_Config.CurrentSpeed_kHz = 100;	// write 100kHz
			res = CHAL_I2C_Set_BusSpeed(pDrvIf_Spec->pPeri, pDevice->Dev_Config.CurrentSpeed_kHz);
			if(res == false) break;													// cannot set minimal I2C Speed!
		}
		
		//pDevice->Dev_Config.CurrentSpeed_kHz = CHAL_I2C_Get_BusSpeed( pDrvIf_Spec->pPeri);	// read wroted bus speed.
		//I2C_Ping (pDrvIf, pDevice->Dev_HWAddr);
		//I2C_Ping (pDrvIf, pDevice->Dev_HWAddr);
		__NOP();
		if(I2C_Ping (pDrvIf, pDevice->Dev_HWAddr) == true)							// try to ping device
		{
			//MaxSpeed_kHz = pDevice->Dev_Config.CurrentSpeed_kHz;					// save top working speed
			CurrSpeed_kHz += 10;
		}
		else break;			
	}while(1);
	
	res = CHAL_I2C_Set_BusSpeed(pDrvIf_Spec->pPeri, CurrSpeed_kHz);					// back to last good speed
	pDevice->Dev_Parms.SpeedMode =  I2C_GET_MODE_FROM_SPEED_KHZ(CHAL_I2C_Get_BusSpeed( pDrvIf_Spec->pPeri));	// save Speed Mode to device

	// reconfigure I2C BUS to back value
	res = CHAL_I2C_Set_BusSpeed(pDrvIf_Spec->pPeri, OldSpeed);						// try to set


	pDrvIf_Spec->BusConf.CurrentSpeed_kHz = CHAL_I2C_Get_BusSpeed( pDrvIf_Spec->pPeri);// I2C BUS - read current speed in kHz
	pDrvIf_Spec->BusConf.CurrentSpeedMode =  I2C_GET_MODE_FROM_SPEED_KHZ(pDrvIf_Spec->BusConf.CurrentSpeed_kHz);// I2C BUS - read current speed mode
	
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("MaxSpeed: %d kHz", CurrSpeed_kHz);
#endif	
	
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// EXT
// scan I2C for connected devices through I2C Address (0 - 255). MaxDevs is number of max found devices.
// return is number of devices found
// If create Devs is true, scanner also create structure for each device found
#pragma diag_suppress 550
uint8_t I2C_Scan (struct _drv_If *pDrvIf, uint16_t MaxAddr, uint16_t MaxDevs, bool CreateDevs)
{
	bool res = false;
	uint16_t count = 0;
	uint16_t DevHWAddr = 0;
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> MaxAddr:0x%.4X >> MaxDevs:%d >> CreateDevs:%s", MaxAddr, MaxDevs, CreateDevs?"True":"False");
#endif	

	
	if(pDrvIf->Sys.Stat.Initialized == true)
	{
		drv_I2C_If_Spec_t* pDrvIf_Spec = (drv_I2C_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer to specific struct of periphery
		
		I2C_Device_t 		*pDeviceFound;

#if (CONF_SYS_COLLECTIONS_USED == 1)
		_ColListRecord_t	*tmpRecord;
		
		tmpRecord = NULL;
		do
		{
			tmpRecord = sys_DynCol_Get_Record(&pDrvIf_Spec->Device_List, tmpRecord);// return pointers to old devices. Needs free memory before scan new ones.
			if(tmpRecord == NULL) break;
			if(tmpRecord->pValue != NULL) I2C_DestroyDevice(pDrvIf, tmpRecord->pValue); // clear device handle
		}while(pDeviceFound);
		
		sys_DynCol_Clear_List(&pDrvIf_Spec->Device_List);							// Now, we can clear old device list		
#else
		sys_memset( &pDrvIf_Spec->Device_List[count], 0x00, sizeof(I2C_Device_t) * CONF_DRV_I2C_MAX_DEV_ON_BUS);// clear array
#endif

		uint32_t OldSpeed = CHAL_I2C_Get_BusSpeed( pDrvIf_Spec->pPeri);				// Save preset of the bus speed
		CHAL_I2C_Set_BusSpeed(pDrvIf_Spec->pPeri, CONF_DRV_I2C_SCAN_BUS_SPEED_KHZ);	// try to set new speed


		do
		{
			if(I2C_Ping(pDrvIf, DevHWAddr) == true)									// device with address DevHWAddr exists
			{
				if(CreateDevs)
				{
					pDeviceFound = I2C_CreateDevice(pDrvIf, pDrvIf_Spec->XFer_Rec.DstAdd);// create new Device
					if(pDeviceFound != NULL)											// Handle created ?
					{
						//I2C_Device_AutoConfigure(pDrvIf, DeviceFound);				// Autoconfigure
#if (CONF_SYS_COLLECTIONS_USED == 1)						
						sys_DynCol_Add_Item (&pDrvIf_Spec->Device_List, pDeviceFound);// Add device to list
#else
						if(count < CONF_DRV_I2C_MAX_DEV_ON_BUS)
							sys_memcpy( &pDrvIf_Spec->Device_List[count], pDeviceFound, sizeof(I2C_Device_t));// copy new device to array
#endif						
					}
				}
				if(count++ > MaxDevs) break;										// increase number of connected devices
			}
			DevHWAddr += 2;															// next I2C address. Skip write address
			Delay_us(500);
		}while (DevHWAddr < MaxAddr);												// repeat while address is not last one
		
		CHAL_I2C_Set_BusSpeed(pDrvIf_Spec->pPeri, OldSpeed);						// Set speed back
	}	
	res = true;

#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("Devs: %d Found", count);
#endif	
	
	return(count);
}  
#pragma diag_default 550	


// ------------------------------------------------------------------------------------------------------
// EXT
// If rescan is true, I2C will be scanned firstly.
// function return a pointer to I2C device, based on HW Address
I2C_Device_t* I2C_Get_DeviceByAddr (struct _drv_If *pDrvIf, uint16_t DevHWAddr, bool Rescan)
{
	drv_I2C_If_Spec_t* pDrvIf_Spec = (drv_I2C_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer to specific struct of periphery
	I2C_Device_t *DeviceFound = NULL;
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_ENTRY(pDrvIf->Name);
	FN_DEBUG_FMTPRINT(" >> HWAddr:0x%.4X", HWAddr);
#endif	

	if(Rescan == true) ((drv_I2C_Ext_API_t *) pDrvIf->pAPI_STD->Ext)->Scan(pDrvIf, 0xFF, 0xFF, true);

#if (CONF_SYS_COLLECTIONS_USED == 1)	
	if(pDrvIf_Spec->Device_List.Count > 0)											// if we have any device
	{
		_ColListRecord_t *tmpListRecord = pDrvIf_Spec->Device_List.pFirst;			// temporary pointer to device list
		do
		{
			DeviceFound = tmpListRecord->pValue;									// ptr to device
			if(DeviceFound->Dev_HWAddr == DevHWAddr) break;							// device found
			DeviceFound = NULL;
			if(tmpListRecord->pNext == NULL) break;									// finish - return
			tmpListRecord = tmpListRecord->pNext;									// go next
		}while(tmpListRecord);
	}
#else
	uint8_t dev_id = 0;
	
	//dev_id = sizeof(pDrvIf_Spec->Device_List) / sizeof(I2C_Device_t);				// get count of devices
	
	while(dev_id < CONF_DRV_I2C_MAX_DEV_ON_BUS)
	{
		//DeviceFound = tmpListRecord->pValue;										// ptr to device
		if(pDrvIf_Spec->Device_List[dev_id].Dev_HWAddr == DevHWAddr) 				// device found
		{
			DeviceFound = &pDrvIf_Spec->Device_List[dev_id];						// returmn ptr to device
			break;
		}
		DeviceFound = NULL;
		if(pDrvIf_Spec->Device_List[dev_id].Dev_HWAddr == 0x00) break;				// finish - return
		dev_id ++;																	// go next
	};
#endif	
	
#if defined(CONF_DEBUG_DRV_I2C) && (CONF_DEBUG_DRV_I2C == 1)
	FN_DEBUG_EXIT(pDrvIf->Name);
	FN_DEBUG_FMTPRINT("Device ptr: %p", DeviceFound);
#endif	
	return(DeviceFound);
}  



// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Quick handler
inline void I2C_Handler_Top(_drv_If_t *pDrvIf)
{
	if(pDrvIf == NULL) return;
	
	if(pDrvIf->pIRQ_USR_HandlerTop) 												// Handler is overriden?
	{
		pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);										// Call USR handler
		return;
	}
	
	if(pDrvIf->Sys.Stat.Initialized == false) return;
	if(pDrvIf->Sys.Stat.Loaded == false) return;

	// Generic interrupt routine:
	drv_I2C_If_Spec_t* pDrvIf_Spec = (drv_I2C_If_Spec_t*) pDrvIf->pDrvSpecific;
	uint32_t IRQ_MST_Status = CHAL_I2CMST_Get_IRQ_Status(pDrvIf_Spec->pPeri);
	uint32_t IRQ_Timeout_Status = CHAL_I2CTimeout_Get_IRQ_Status(pDrvIf_Spec->pPeri);
	
	//pDrvIf_Spec->Timeout_Counter = 0;

	
#ifdef CHIP_I2C_IRQSTAT_MSARBLOSS
	if (IRQ_MST_Status & CHIP_I2C_IRQSTAT_MSARBLOSS) 								// IRQ FLAG = Master Lost Arbitration
	{
		pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_ERROR; 						// Set Error Flag
		pDrvIf_Spec->XFer_Rec.Status |= CHAL_I2C_STAT_ARBITR_LOSS;					// Set transfer status as Arbitration Lost
//		pDrvIf->Sys.Stat.LastEvent = EV_StateChanged;								// Set Event
		CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);						// disable I2C - timeout
	}
#endif		
	
#ifdef CHIP_I2C_IRQSTAT_MSSTSTPERR		
	if (IRQ_MST_Status & CHIP_I2C_IRQSTAT_MSSTSTPERR) 								// IRQ FLAG = Master Start Stop Error
	{
		pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_ERROR; 						// Set Error Flag
		pDrvIf_Spec->XFer_Rec.Status |= CHAL_I2C_STAT_STSTERR;						// Set transfer status as Bus Error
//		pDrvIf->Sys.Stat.LastEvent = EV_StateChanged;								// Set Event
		CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);						// disable I2C - timeout
	}
#endif
	

	// DECODE I2C MASTER State machine:
	if (IRQ_MST_Status & CHIP_I2C_IRQSTAT_MSTPENDING)								// Master state changed ????
	{
		uint32_t Stat = CHAL_I2C_Get_Peri_Status(pDrvIf_Spec->pPeri) & CHIP_I2C_MSSTATE_MASK;
		
		switch (Stat)																// Branch based on Master State Code
		{
			case 0x00:																// Master idle - from LPC804 UM11065
			{
				break;																// Do Nothing
			}
			case CHAL_I2C_PERISTAT_MSSTATE_RX:										// Receive data is available
			{
				if(pDrvIf_Spec->XFer_Rec.pRxBuff == NULL)							// dst buffer not set
				{
					pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_OK; 				// Set OK Flag
//					pDrvIf->Sys.Stat.LastEvent = EV_NoChange;						// Set Event
					CHAL_I2CMST_Set_Stop(pDrvIf_Spec->pPeri);						// No data to read send Stop
					CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);			// disable I2C interrupt
					break;
				}
//	I2C driver pouziva v preruseni RAW buffer le read/write sys_Buffer !!!! zla konverzia je tam!

				
				// Read data from I2C register
				uint8_t tmp = CHAL_I2CMST_Get_Data(pDrvIf_Spec->pPeri);
#if (CONF_SYS_BUFFERS_USED == 1)				
				sys_Buffer_Write_Bytes(pDrvIf_Spec->XFer_Rec.pRxBuff, &tmp, pDrvIf_Spec->pXFer_Dev->TxBuffIncrement);
#else
				*(pDrvIf_Spec->XFer_Rec.pRxBuff) = tmp;
				pDrvIf_Spec->XFer_Rec.pRxBuff += pDrvIf_Spec->pXFer_Dev->TxBuffIncrement;
#endif			
				pDrvIf_Spec->XFer_Rec.RxBuffSize--;
				if (pDrvIf_Spec->XFer_Rec.RxBuffSize) 
				{
					pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_BUSY;				// Still in progress
//					pDrvIf->Sys.Stat.LastEvent = EV_NoChange;						// Clear Event
					CHAL_I2CMST_Set_Cont(pDrvIf_Spec->pPeri);						// Set Continue if there is more data to read 
				}
				else 
				{
					pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_OK; 				// Set OK Flag
//					pDrvIf->Sys.Stat.LastEvent = EV_DataReceived;					// Set Event
					CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);			// disable I2C interrupt
					CHAL_I2CMST_Set_Stop(pDrvIf_Spec->pPeri);						// No data to read send Stop
				}
				break;
			}
			case CHAL_I2C_PERISTAT_MSSTATE_TX:										// Master Transmit  - from LPC804 UM11065
			{
				if(pDrvIf_Spec->XFer_Rec.TxDevRegSize)								// sending Device register address?
				{
					pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_BUSY;				// Still in progress
					// Write data to I2C register
					CHAL_I2CMST_Put_Data(pDrvIf_Spec->pPeri, *pDrvIf_Spec->XFer_Rec.pTxDevReg++);// Send Register address
					
					if(--pDrvIf_Spec->XFer_Rec.TxDevRegSize == 0)
						CHAL_I2CMST_Set_Cont(pDrvIf_Spec->pPeri);
				}
				else	// OK, device address was sent, now send Data to this register
				{
					if (pDrvIf_Spec->XFer_Rec.TxSize) 
					{
						pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_BUSY;			// Still in progress
						
						uint8_t tmp;
#if (CONF_SYS_BUFFERS_USED == 1)
						sys_Buffer_Read_Bytes(pDrvIf_Spec->XFer_Rec.pTxBuff, &tmp, pDrvIf_Spec->pXFer_Dev->TxBuffIncrement);
#else
						tmp = *(pDrvIf_Spec->XFer_Rec.pTxBuff);						// Read_Data_Handler_t buffer value
						pDrvIf_Spec->XFer_Rec.pTxBuff += pDrvIf_Spec->pXFer_Dev->TxBuffIncrement;	// increment source buffer
#endif						
						CHAL_I2CMST_Put_Data(pDrvIf_Spec->pPeri, (uint32_t) tmp);
						pDrvIf_Spec->XFer_Rec.TxSize--;
						CHAL_I2CMST_Set_Cont(pDrvIf_Spec->pPeri);
					}
					else 
					{
						if (pDrvIf_Spec->XFer_Rec.RxBuffSize) 						// If receive queued after transmit then initiate master receive transfer
						{
							CHAL_I2CMST_Set_Start(pDrvIf_Spec->pPeri);				// Enter to Master Transmitter mode ///////// BLOCKKKKKKK pri read.
							CHAL_I2CMST_Put_Data(pDrvIf_Spec->pPeri, (pDrvIf_Spec->XFer_Rec.DstAdd) | 0x1);	// Write Address and RW bit to data register
							pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_BUSY;		// Still in progress
						}
						else 
						{
							pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_OK; 		// If no receive queued then set transfer status as OK
//							pDrvIf->Sys.Stat.LastEvent = EV_Transmitted;			// Set Event
							CHAL_I2CMST_Set_Stop(pDrvIf_Spec->pPeri);				// Send Stop
							CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);	// disable I2C interrupt
						}
					}
				}
				break;
			}
			case CHAL_I2C_PERISTAT_MSSTATE_NACK_ADR:								// Set transfer status as NACK on address - from LPC804 UM11065
			{
				pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_ERROR; 				// Set Error Flag
				pDrvIf_Spec->XFer_Rec.Status |= CHAL_I2C_STAT_NAK_ADR;
//				pDrvIf->Sys.Stat.LastEvent = EV_StateChanged;						// Set Event
				CHAL_I2CMST_Set_Stop(pDrvIf_Spec->pPeri);
				CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);				// disable I2C interrupt
				break;
			}
			case CHAL_I2C_PERISTAT_MSSTATE_NACK_DATA:								// Set transfer status as NACK on data - from LPC804 UM11065
			{
				pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_ERROR; 				// Set Error Flag
				pDrvIf_Spec->XFer_Rec.Status |= CHAL_I2C_STAT_NAK_DAT;
//				pDrvIf->Sys.Stat.LastEvent = EV_StateChanged;						// Set Event
				CHAL_I2CMST_Set_Stop(pDrvIf_Spec->pPeri);
				CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);				// disable I2C interrupt
				break;
			}
			default:																// Default case should not occur
			{
				pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_ERROR;
//				pDrvIf->Sys.Stat.LastEvent = EV_StateChanged;						// Set Event
				CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);				// disable I2C interrupt
				break;
			}
		}
	}
	else																			// Default case should not occur
	{
		pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_ERROR;
//		pDrvIf->Sys.Stat.LastEvent = EV_StateChanged;								// Set Event
		CHAL_I2CMST_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);						// disable I2C interrupt
	}	
	
	
//  TIMEOUTS
#ifdef CHIP_I2C_IRQSTAT_SCLTIMEOUT
	if (IRQ_Timeout_Status & CHIP_I2C_IRQSTAT_SCLTIMEOUT) 								// IRQ FLAG = SCL timeout
	{
		pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_ERROR; 						// Set Error Flag
		pDrvIf_Spec->XFer_Rec.Status |= CHAL_I2C_STAT_SCLLONG;						// Set transfer status as I2C_STAT_SCLLONG
//		pDrvIf->Sys.Stat.LastEvent = EV_StateChanged;								// Set Event
		CHAL_I2CTimeout_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);					// disable I2C - timeout
	}
#endif

#ifdef CHIP_I2C_IRQSTAT_EVENTTIMEOUT
	if (IRQ_Timeout_Status & CHIP_I2C_IRQSTAT_EVENTTIMEOUT) 							// IRQ FLAG = SCL timeout
	{
		pDrvIf_Spec->XFer_Rec.Status = CHAL_I2C_STAT_ERROR; 						// Set Error Flag
		pDrvIf_Spec->XFer_Rec.Status |= CHAL_I2C_STAT_TIMEOUT;						// Set transfer status as I2C_STAT_TIMEOUT
//		pDrvIf->Sys.Stat.LastEvent = EV_StateChanged;								// Set Event
		CHAL_I2CMST_Reset(pDrvIf_Spec->pPeri);										// reset transfer
		CHAL_I2CTimeout_Enable_IRQ_NVIC(pDrvIf_Spec->pPeri, false);					// disable I2C - timeout
	}
#endif


//	if(pDrvIf->Sys.Stat.LastEvent > EV_NoChange)
//	{
//		if((pDrvIf->pAPI_USR) && (pDrvIf->pAPI_USR->Events)) pDrvIf->pAPI_USR->Events( pDrvIf, pDrvIf->Sys.Stat.LastEvent);	// if is set USR event system, call it
//		else
//		{
//			if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, pDrvIf->Sys.Stat.LastEvent);	// if is set STD event system, call it
//		}
//	}
}


// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************
// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
inline void I2C_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{
//	void* pCallBack;		
//	_ColListRecord_t *tmpListRecord = pDrvIf->Sys.ParentModLinked.pFirst;			// temporary pointer to parent module list
//	while(tmpListRecord)
//	{
//		switch(EventType)
//		{
//			case EV_StateChanged:
//			{
//				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_ChangedState;
//				break;
//			}
//			case EV_DataReceived:
//			{
//				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataReceived;
//				break;
//			}
//			case EV_Transmitted:
//			{
//				pCallBack = ((_mod_If_t *) tmpListRecord->pValue)->pAPI->CallBack_DataTransmitted;
//				break;
//			}
//			default: break;
//		}
//		
//		if(pCallBack)																// callback is set?
//		{
//			sys_Task_Create_Item( pDrvIf->Name, pCallBack, (sys_TaskArgs_t) {NULL, 0});	// create task for data read
//		}

//		if(tmpListRecord->pNext == NULL) break;										// loop while have associadet module
//		tmpListRecord = tmpListRecord->pNext;										// goto next associated module
//	}
}

#endif	// ifdef __DRV_I2C_H_
