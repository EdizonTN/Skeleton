// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_SCT.h
// 	   Version: 3.1
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for SCT - header file
// ******************************************************************************
// Usage:
// 
// ToDo:
// 
// Changelog:
// 

#if !defined(__DRV_SCT_H_) 															// prevent to cyclyc loads
 #if defined(USE_DRV_SCT) && (USE_DRV_SCT == 1)										// Need to load?
  #if (!defined (_CHIP_SCT_COUNT) || _CHIP_SCT_COUNT == 0)							// Neeed it, but MCU haven't this type of periphery!
	#error This driver is need to be used, but MCU doesn't have it!
  #else
	// now -> load it.
#define __DRV_SCT_H_


#include "Skeleton.h"

// ************************************************************************************************
// CONFIGURATION
// ************************************************************************************************

#ifndef CONF_DEBUG_DRV_SCT
#define CONF_DEBUG_DRV_SCT				0											// default je vypnuty
#endif	

//#ifndef SCT_USE_IRQ_MANAGER 
//#define SCT_USE_IRQ_MANAGER 			0											// If IRQ Manager is used, SCT can be leaved from IRQ Managaer setting to "0"
//#endif	

#ifndef CONF_DRV_SCT_RWBUFFERSIZE
 #define CONF_DRV_SCT_RWBUFFERSIZE		(4*4)										// R/W buffer size
#endif	


// ************************************************************************************************
// PUBLIC Defines
// ************************************************************************************************

												// && (_CHIP_HAVE_SCT == 1)
const typedef struct drv_SCT_Config
{
	uint8_t 	Counter_SizeInBytes;												// pouzivam ako unified ???
} drv_SCT_Config_t;


// ku generic strukture treba este pridat:
COMP_PACKED_BEGIN
typedef struct drv_SCT_If_Spec
{
			void						*pPeri;										// pointer na periferiu	
	const 	uint8_t						PeriIndex;									// index pouzitej periferie	
	const 	CHAL_Signal_t				*pSignal_Out;								// fyzicky Pin vystupu SCT
	const 	uint8_t						SCT_Out;									// cislo vystupu
	const 	uint8_t						EV_Fix_Num;									// ktory event nastavuje OUT
	const 	uint8_t						EV_Var_Num;									// ktory event nastavuje OUT
	const 	uint8_t						EV_DMA_Num;									// ktory event aktivujeDMA prenos
	const 	uint8_t						CNT_Fix_Num;								// ktory MATCH nastavuje sa porovnava s EV_Set
	const 	uint8_t						CNT_Var_Num;								// ktory MATCH nastavuje sa porovnava s EV_Clr
	const 	uint8_t						CNT_DMA_Num;								// ktory MATCH aktivuje novy DMA prenos
//	const 	_If_t						*pDMA_Channel;	
	sys_Buffer_t						*pRWBuff;									// Pointer to R/W Buffer
//	struct  drv_SCT_Config				Conf;										// pointer na konfiguracnu strukturu
} drv_SCT_If_Spec_t;
COMP_PACKED_END

// ************************************************************************************************
// PUBLIC
// ************************************************************************************************
extern const _drv_Api_t drv_SCT_API_STD;
extern void drv_SCT_Handler_Top(_drv_If_t *pDrvIf);
extern void drv_SCT_Handler_Bottom(_drv_If_t *pDrvIf);


// ************************************************************************************************
// BIT Definition - INTENCLR register
// ************************************************************************************************
#endif		// defined (_CHIP_zzz_COUNT)
#endif		// #if defined(USE_DRV_zzz)
#endif		// __DRV_ZZZ_H_

