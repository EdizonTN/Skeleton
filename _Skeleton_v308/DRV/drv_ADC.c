// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_ADC.c
// 	   Version: 1.12
//      Author: EdizonTN (Skeleton licenced under MIT License. More you can find at LICENSE file)
//		  Desc: Generic Driver for ADC
// ******************************************************************************
// Info: ADC driver
//
// Notice:
//
// Usage:
//				- wruite to result structure:  (*pDrvIf_Spec->Result_CH)[i] = newvalue
//
// ToDo:
//				- dorob interrupt handler aj na ostatne typy (OVR, SEQA, THCMP,...)
// Changelog:
//				2024.09.13	- v1.12	- fiw reading ADC Value
//									- add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2023.12.10	- v1.11 - rename _CHIP_ADCx_IRQ_Handler to _Chip_ADCx_IRQ_Handler
//									- change bootom irq to 	IRQ_Handler_Bottom((void *) ADC_DriverIRQ[x]);
//				2022.11.30	- v1.1  - change system of load
//

#include "Skeleton.h"

#ifdef __DRV_ADC_H_																	// Header was loaded?


// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool ADC_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool ADC_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool ADC_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
void ADC_Events( struct _drv_If *pDrvIf, _EventType_t EventType);
void ADC_SEQA_Handler_Top(_drv_If_t *pDrvIf);										// Quick Handler  - sequencer A
void ADC_SEQA_Handler_Bottom(_drv_If_t *pDrvIf);									// Process Handler  - sequencer A
void ADC_SEQB_Handler_Top(_drv_If_t *pDrvIf);										// Quick Handler  - sequencer B
void ADC_SEQB_Handler_Bottom(_drv_If_t *pDrvIf);									// Process Handler  - sequencer B
void ADC_THCMP_Handler_Top(_drv_If_t *pDrvIf);										// Quick Handler  - Threshold compare
void ADC_THCMP_Handler_Bottom(_drv_If_t *pDrvIf);									// Process Handler  - Threshold compare
void ADC_OVR_Handler_Top(_drv_If_t *pDrvIf);										// Quick Handler  - Overrun irq
void ADC_OVR_Handler_Bottom(_drv_If_t *pDrvIf);										// Process Handler  - Overrun irq

// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 		drv_ADC_API_STD 		=
{
    .Init			=	ADC_Init,
    .Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
    .Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
    .Enable			=	ADC_Enable,
    .Disable		=	ADC_Disable,
    .Write_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
    .Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
    .Events			=	ADC_Events,
};
#pragma pop

const _drv_If_t *ADC_DriverIRQ[_CHIP_ADC_COUNT];									// num of the ADCs

// ------------------------------------------------------------------------------------------------------
// ADC INTERRUPT HANDLERS
// ------------------------------------------------------------------------------------------------------
#if defined(_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 0)
void _Chip_ADC0_SEQA_IRQ_Handler(void)
{
    ADC_SEQA_Handler_Top((void *) ADC_DriverIRQ[0]);
    //ADC_SEQA_Handler_Bottom((void *) ADC_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) ADC_DriverIRQ[0]);
}

void _Chip_ADC0_SEQB_IRQ_Handler(void)
{
    ADC_SEQB_Handler_Top((void *) ADC_DriverIRQ[0]);
    //ADC_SEQB_Handler_Bottom((void *) ADC_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) ADC_DriverIRQ[0]);	
}

void _Chip_ADC0_THCMP_IRQ_Handler(void)
{
    ADC_THCMP_Handler_Top((void *) ADC_DriverIRQ[0]);
    //ADC_THCMP_Handler_Bottom((void *) ADC_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) ADC_DriverIRQ[0]);	
}

void _Chip_ADC0_OVR_IRQ_Handler(void)
{
    ADC_OVR_Handler_Top((void *) ADC_DriverIRQ[0]);
    //ADC_OVR_Handler_Bottom((void *) ADC_DriverIRQ[0]);
	IRQ_Handler_Bottom((void *) ADC_DriverIRQ[0]);
}
#endif

#if defined(_CHIP_ADC_COUNT) && (_CHIP_ADC_COUNT > 1)
void _Chip_ADC1_SEQA_IRQ_Handler(void)
{
    ADC_SEQA_Handler_Top((void *) ADC_DriverIRQ[1]);
    //ADC_SEQA_Handler_Bottom((void *) ADC_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) ADC_DriverIRQ[1]);	
}

void _Chip_ADC1_SEQB_IRQ_Handler(void)
{
    ADC_SEQB_Handler_Top((void *) ADC_DriverIRQ[1]);
    //ADC_SEQB_Handler_Bottom((void *) ADC_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) ADC_DriverIRQ[1]);	
}

void _Chip_ADC1_THCMP_IRQ_Handler(void)
{
    ADC_THCMP_Handler_Top((void *) ADC_DriverIRQ[1]);
    //ADC_THCMP_Handler_Bottom((void *) ADC_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) ADC_DriverIRQ[1]);
}

void _Chip_ADC1_OVR_IRQ_Handler(void)
{
    ADC_OVR_Handler_Top((void *) ADC_DriverIRQ[1]);
    //ADC_OVR_Handler_Bottom((void *) ADC_DriverIRQ[1]);
	IRQ_Handler_Bottom((void *) ADC_DriverIRQ[1]);
}
#endif


// ------------------------------------------------------------------------------------------------------
// Generic
// Inicializacia ADC-u
// ------------------------------------------------------------------------------------------------------
bool ADC_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
    bool res = false;
	//drv_ADC_If_Channel_t	tmp[_CHIP_ADC_CHANNELS_COUNT];							// allocate array of all channels structs
	
	uint8_t	ChannelCount = 0;
    drv_ADC_If_Spec_t* pDrvIf_Spec = (drv_ADC_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_ADC) && (CONF_DEBUG_DRV_ADC == 1)
    FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
#endif

    // initialization code here:
    pDrvIf_Spec->pPeri = CHAL_ADC_Init(pDrvIf_Spec->PeriIndex);
    if(pDrvIf_Spec->pPeri != NULL) res = true;
	
	CHAL_ADC_Enable_CH( pDrvIf_Spec->pPeri, *pDrvIf_Spec->Ch_Active_Mask, true);	// set active channels

	for(uint8_t i=0; i<_CHIP_ADC_CHANNELS_COUNT; i++)
	{
		if(*pDrvIf_Spec->Ch_Active_Mask & (1<<i))									// if current channel is active, fill in the array next record
		{
			//tmp[ChannelCount].ADC_ChannelNum = i;									// store channel number
			//tmp[ChannelCount].ADC_Result = 0x0000;									// result set to zero
			pDrvIf_Spec->Result_CH[i].ADC_ChannelNum = i;
			pDrvIf_Spec->Result_CH[i].ADC_Result = 0x0000;
			
			ChannelCount ++;
		}
	}

//	(pDrvIf_Spec->Result_CH) = sys_malloc_zero(sizeof(tmp)); 			// allocate array of active only channels structs
//	*(pDrvIf_Spec->Result_CH) = tmp;										// init all record
	
//	for(uint8_t i=0; i< ChannelCount; i++)											// now copy searched channel to new array
//	{
//		(pDrvIf_Spec->Result_CH)[i]->ADC_ChannelNum = i;
//		(pDrvIf_Spec->Result_CH)[i]->ADC_Result = 0x0000;
//		//*(pDrvIf_Spec->Result_CH)[i] = tmp[i];											// copy content of the record. Because tmp will be freeed after return from this function
//	}
	pDrvIf_Spec->Ch_Active = ChannelCount;
	
    pDrvIf->Sys.Stat.Initialized = res;
	
#if defined (CONF_USE_IRQ_MANAGER) && (CONF_USE_IRQ_MANAGER == 1) && defined(UART_USE_IRQ_MANAGER) && (UART_USE_IRQ_MANAGER == 1)
	if(res == true) res = irqmgr_isr_Handler_Install(pDrvIf->IRQ_VecNum, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerTop, (void (*)(void *Parm))pDrvIf->pIRQ_USR_HandlerBottom, pDrvIf->IRQ_Priority, (void *)pDrvIf);
#else
	ADC_DriverIRQ[pDrvIf_Spec->PeriIndex] = pDrvIf;								// set parameter for interrupt routine
#endif		
	
#if defined(CONF_DEBUG_DRV_ADC) && (CONF_DEBUG_DRV_ADC == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif
    return(res);
}


// ------------------------------------------------------------------------------------------------------
// Generic
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
// ------------------------------------------------------------------------------------------------------
bool ADC_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
    bool res = false;

    drv_ADC_If_Spec_t* pDrvIf_Spec = (drv_ADC_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_ADC) && (CONF_DEBUG_DRV_ADC == 1)
    FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
    FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);						// print arguments
#endif

    // main code here:
    res = CHAL_ADC_Enable_CH(pDrvIf_Spec->pPeri, ChannelBitMask, true); 			// enable ADC
    pDrvIf->Sys.Stat.Enabled = res;

#if defined(CONF_DEBUG_DRV_ADC) && (CONF_DEBUG_DRV_ADC == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif
    return(res);
}

// ------------------------------------------------------------------------------------------------------
// Generic
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
// ------------------------------------------------------------------------------------------------------
bool ADC_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
    bool res = false;

    drv_ADC_If_Spec_t* pDrvIf_Spec = (drv_ADC_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_ADC) && (CONF_DEBUG_DRV_ADC == 1)
    FN_DEBUG_ENTRY(pDrvIf->Name)													// dbg Entry print
    FN_DEBUG_FMTPRINT(" >> chMask: 0x%.8x", ChannelBitMask);						// print arguments
#endif

    // main code here:
    res = CHAL_ADC_Enable_CH(pDrvIf_Spec->pPeri, ChannelBitMask, false); 			// Complette disable
    pDrvIf->Sys.Stat.Enabled = res;
#if defined(CONF_DEBUG_DRV_ADC) && (CONF_DEBUG_DRV_ADC == 1)
    FN_DEBUG_EXIT(pDrvIf->Name)														// dbg exit print
	FN_DEBUG_FMTPRINT(FN_DEBUG_RES_BOOL(res));										// dbg print result boolean code
#endif
    return(res);
}



// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// Quick handler
// ------------------------------------------------------------------------------------------------------
inline void ADC_SEQA_Handler_Top(_drv_If_t *pDrvIf)
{
#if defined(CONF_SYS_IDIOTPROOF) && (CONF_SYS_IDIOTPROOF == 1)
	if(pDrvIf == NULL) return;														// Driver Device not set
	if(pDrvIf->Sys.Stat.Initialized == false) return;								// Device not initialized
	if(pDrvIf->Sys.Stat.Loaded == false) return;									// Device not loaded
#endif

	
	if(pDrvIf->pIRQ_USR_HandlerTop) 												// override with USR handler ????
	{
		pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);			
		return;
	}
	
	//Standard handler:
	
    drv_ADC_If_Spec_t* pDrvIf_Spec = (drv_ADC_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie
    uint32_t pending = CHAL_ADC_Get_IRQ_Status_CH(pDrvIf_Spec->pPeri, UINT32_MAX);	// read pending interrupt flags for all channels

	uint8_t CurrentADCChannel;

	for(uint8_t i=0; i < pDrvIf_Spec->Ch_Active; i ++)
	{
		//CurrentADCChannel = (*pDrvIf_Spec->Result_CH)[i].ADC_ChannelNum;
		CurrentADCChannel = pDrvIf_Spec->Result_CH[i].ADC_ChannelNum;
		//if(((_CHIP_ADC_T *)pDrvIf_Spec->pPeri)->DAT[CurrentADCChannel] & ADC_DAT_DATAVALID_MASK) // Data valid for selected channel?
		if(CHAL_ADC_Get_DataValidFlag_CH( pDrvIf_Spec->pPeri, CurrentADCChannel) == true) // Data valid for selected channel?
		{
			pDrvIf_Spec->Result_CH[i].ADC_Result = (uint16_t) CHAL_ADC_Get_Data_CH( pDrvIf_Spec->pPeri, CurrentADCChannel);	// read ADC Result
		}
	}
	
    /* Clear any pending interrupts */
    CHAL_ADC_Clear_IRQ_Status_CH(pDrvIf_Spec->pPeri, pending);						// clear all penging flags
}

// ------------------------------------------------------------------------------------------------------
// After handler
// fire event system
// ------------------------------------------------------------------------------------------------------
inline void ADC_SEQA_Handler_Bottom(_drv_If_t *pDrvIf)
{
}


// ------------------------------------------------------------------------------------------------------
// Quick handler
// ------------------------------------------------------------------------------------------------------
inline void ADC_SEQB_Handler_Top(_drv_If_t *pDrvIf)									// Quick Handler  - sequencer B
{
    drv_ADC_If_Spec_t* pDrvIf_Spec = (drv_ADC_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie
    uint32_t pending = CHAL_ADC_Get_IRQ_Status_CH(pDrvIf_Spec->pPeri, UINT32_MAX);	// read pending interrupt flags for all channels

    /* Clear any pending interrupts */
    CHAL_ADC_Clear_IRQ_Status_CH(pDrvIf_Spec->pPeri, pending);						// clear all penging flags
}


// ------------------------------------------------------------------------------------------------------
// After handler
// fire event system
// ------------------------------------------------------------------------------------------------------
inline void ADC_SEQB_Handler_Bottom(_drv_If_t *pDrvIf)								// Process Handler  - sequencer B
{
}


// ------------------------------------------------------------------------------------------------------
// Quick handler
// ------------------------------------------------------------------------------------------------------
inline void ADC_THCMP_Handler_Top(_drv_If_t *pDrvIf)								// Quick Handler  - Threshold compare
{
    drv_ADC_If_Spec_t* pDrvIf_Spec = (drv_ADC_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie
    uint32_t pending = CHAL_ADC_Get_IRQ_Status_CH(pDrvIf_Spec->pPeri, UINT32_MAX);	// read pending interrupt flags for all channels

    /* Clear any pending interrupts */
    CHAL_ADC_Clear_IRQ_Status_CH(pDrvIf_Spec->pPeri, pending);
}


// ------------------------------------------------------------------------------------------------------
// After handler
// fire event system
// ------------------------------------------------------------------------------------------------------
inline void ADC_THCMP_Handler_Bottom(_drv_If_t *pDrvIf)								// Process Handler  - Threshold compare
{
}


// ------------------------------------------------------------------------------------------------------
// Quick handler
// ------------------------------------------------------------------------------------------------------
inline void ADC_OVR_Handler_Top(_drv_If_t *pDrvIf)									// Quick Handler  - Overlay
{
    drv_ADC_If_Spec_t* pDrvIf_Spec = (drv_ADC_If_Spec_t*) pDrvIf->pDrvSpecific;		// pointer na specific struct periferie

    if(pDrvIf == NULL) return;

    if(pDrvIf->pIRQ_USR_HandlerTop) pDrvIf->pIRQ_USR_HandlerTop(pDrvIf);			// override with USR handler
    else
    {
        if(pDrvIf->Sys.Stat.Initialized == false) return;
        if(pDrvIf->Sys.Stat.Loaded == false) return;

        // Generic interrupt routine:
        uint32_t pending = CHAL_ADC_Get_IRQ_Status_CH(pDrvIf_Spec->pPeri, UINT32_MAX);	// read pending interrupt flags for all channels
        CHAL_ADC_Clear_IRQ_Status_CH(pDrvIf_Spec->pPeri, pending);					// Clear any pending interrupts
    }
}


// ------------------------------------------------------------------------------------------------------
// After handler
// fire event system
// ------------------------------------------------------------------------------------------------------
inline void ADC_OVR_Handler_Bottom(_drv_If_t *pDrvIf)								// Process Handler  - Overlay
{
    if(pDrvIf->pAPI_STD->Events) pDrvIf->pAPI_STD->Events( pDrvIf, EV_DataReceived);// ak je nastaveny event system, zavolaj ho
}


// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************

// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
// ------------------------------------------------------------------------------------------------------
inline void ADC_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system
{

//    switch(EventType)
//    {
//    case EV_DataReceived:
//    case EV_NoChange:
//    default:
//        break;
//    }
}


#endif	// ifdef __DRV_ADC_H_
