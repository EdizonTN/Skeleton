// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: drv_OTM8009.c
// 	   Version: 1.01
//		  Desc: Driver for OTM8009 IC - TFT LCD Driver
// ******************************************************************************
// Info: 
//			https://github.com/STMicroelectronics/stm32-otm8009a/blob/main/otm8009a.c
// Notice:
//
// Usage:
//			
// ToDo:	
//
// Changelog:
//				2024.09.30	- v1.01	- add sys_NULL_Function instead a NULL pointer to a function in API structure
//				2021.05.05 	- v1.00	- Initial release 

#include "Skeleton.h"

#if defined(USE_DRV_OTM8009) && (USE_DRV_OTM8009 == 1)

// ******************************************************************************************************
// PRIVATE Functions
// ******************************************************************************************************
bool OTM8009_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf);
bool OTM8009_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool OTM8009_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask);
bool OTM8009_Write_Reg(_drv_If_t *pDrvIf, uint16_t Reg, uint8_t *srcMem, uint8_t BytesCount);

void OTM8009_MirrorRegs(_drv_If_t *pDrvIf);

// ******************************************************************************************************
// Public Function - specific for this driver
// ******************************************************************************************************
const drv_OTM8009_Ext_API_t	OTM8009_Ext_API 	=
{
	.Reload_MirrorRegs					= OTM8009_MirrorRegs,
	.Write_Reg							= OTM8009_Write_Reg,
};


// ******************************************************************************************************
// PUBLIC Variables
// ******************************************************************************************************

// structure and assign to standard API functions:
#pragma push
#pragma diag_suppress 144															// disable warning 144 (sys_NULL_Function) locally
const _drv_Api_t 	drv_OTM8009_API_STD 	= 
{
 	.Init			=	OTM8009_Init,
	.Restart		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Release		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Enable			=	OTM8009_Enable,
	.Disable		=	OTM8009_Disable,
	.Write_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Read_Data		=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Events			=	sys_NULL_Function,											// USE_DRV_ADC default NULL handler
	.Ext			=	(_drv_Api_t *) &OTM8009_Ext_API,
};
#pragma pop


//sys_Buffer_t 							*Buff_OTM8009_Regs;
#define OTM8009_REGS 					((uint8_t *)(Buff_OTM8009_Regs->pDataArray))

// Create destination device for OTM8009 colnfiguration interface:
#ifdef CONF_DRV_OTM8009_USE_I2C
// Create device definition structure for OTM8009 SPI interface:
	I2C_Device_t						pOTM8009_Conf_Device = NULL;				// I2C device structure - must be cleared first!
#endif

#ifdef CONF_DRV_OTM8009_USE_SPI
// Create device definition structure for OTM8009 SPI interface:
	SPI_Device_t						OTM8009_Conf_Device =
	{
		.MaxSpeed_kHz					=	DEF_DRV_OTM8009_SPI_BUS_RATE_MAX_HZ,	// based on datasheet
		.Rel_Mode						= 	CHAL_SPI_Slave,
		.ReverseBits					=	false,
		.DevDataBitSize					=	16,										// OTM8009 require 16 bit SPI transfer
		.Dev_SSEL_ID					=	BSP_UIFLCD_SPI_SSELID
	};
#endif

// ******************************************************************************************************
// Functions prototypes
// ******************************************************************************************************




// ******************************************************************************************************
// Private Functions
// ******************************************************************************************************


//// ------------------------------------------------------------------------------------------------
//// read from OTM8009 via configuration interface
//static uint32_t OTM8009_Read_Reg(_drv_If_t *pDrvIf, uint16_t Reg, sys_Buffer_t *pBuff, uint8_t RegsCount)
//{
//	drv_OTM8009_If_Spec_t* pDrvIf_Spec = (drv_OTM8009_If_Spec_t*) pDrvIf->pDrvSpecific;

//	if(pDrvIf_Spec->pCommInterface) 
//	{
//		
//		//drvmgr_Read_Data( NULL, pDrvIf_Spec->pCommInterface, Buff_OTM8009_Regs, DEF_DRV_OTM8009_REG_COUNT, XFer_Blocking);	// create mirror of the regs.
////		sys_Buffer_Read_Bytes(Buff_OTM8009_Regs, &tmp[SrcReg], RegsCount);
////		sys_Buffer_Flush(Buff_OTM8009_Regs);										// clear others
////		sys_Buffer_Write_Bytes(pBuff, &tmp[SrcReg], RegsCount);						// save to destination
//		
//		return(RegsCount);
//	}
//	else return(0);
//}


//// ------------------------------------------------------------------------------------------------
//// Write to OTM8009 via confguration interface
static bool OTM8009_Write_Reg(_drv_If_t *pDrvIf, uint16_t Reg, uint8_t *srcMem, uint8_t BytesCount)
{
	uint32_t res = false;
	uint8_t count = BytesCount;
	
	drv_OTM8009_If_Spec_t* pDrvIf_Spec = (drv_OTM8009_If_Spec_t*) pDrvIf->pDrvSpecific;
	
	sys_Buffer_Write_Bytes( OTM8009_Conf_Device.pTxBuff, (uint8_t[]) {DEF_DRV_OTM8009_BIT_DEF_HL, Reg >> 8}, 2);// 1. Storte High Register Address
	sys_Buffer_Write_Bytes( OTM8009_Conf_Device.pTxBuff, (uint8_t[]) {0, Reg & 0xff}, 2);						// 2. Store Low Register Address

	while(count)
	{
		sys_Buffer_Write_Bytes( OTM8009_Conf_Device.pTxBuff, (uint8_t[]) {DEF_DRV_OTM8009_BIT_DEF_DCX} , 1);	// Store 
		sys_Buffer_Write_Bytes( OTM8009_Conf_Device.pTxBuff, srcMem ++ , 1);									// Store data byte
		count --;
	}
	
	
	res = drvmgr_Write_Data( NULL, pDrvIf_Spec->pCommInterface, OTM8009_Conf_Device.pTxBuff, (BytesCount + 2), XFer_Blocking);

	return(res);
}


// ------------------------------------------------------------------------------------------------
// Read all charger regs 
void OTM8009_MirrorRegs(_drv_If_t *pDrvIf)
{
}

//static bool OTM8009_Preset(_drv_If_t *pDrvIf)
//{
//}


//Pri vypnuti debugu to prestalo SPI-ckovat. Sekne sa.... asi to ma nieco spolocne so sekanim pri zvyseni rychlosti SPI Bus.....  nejaky timeout alebo kieho frasa.....


// ******************************************************************************************************
// USR Functions
// ******************************************************************************************************

// ------------------------------------------------------------------------------------------------------
// USR
bool OTM8009_Init (_mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf)
{
	bool res = false;	//0
//	uint32_t Result = 0;
	drv_OTM8009_If_Spec_t* pDrvIf_Spec = (drv_OTM8009_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_OTM8009) && (CONF_DEBUG_DRV_OTM8009 == 1)
	dbgprint("\r\n("DRV_OTM8009_OBJECTNAME") ID[%s]: Init", pDrvIf->Name);
#endif		

	// initialization code here:

//	Buff_OTM8009_Regs = sys_Buffer_Create(DEF_DRV_OTM8009_REG_COUNT, DEF_DRV_OTM8009_REG_ELEMENT_SIZE, sBuffRing);
	
	if(pDrvIf_Spec->pCommInterface) 
	{
		drvmgr_Enable( NULL, pDrvIf_Spec->pCommInterface, true);
		
#ifdef CONF_DRV_OTM8009_USE_I2C		
		if(pOTM8009_Conf_Device) ((drv_I2C_Ext_API_t *) pDrvIf_Spec->pCommInterface->pAPI_STD->Ext)->Destroy_Device(pDrvIf_Spec->pCommInterface, pOTM8009_Conf_Device);
		pOTM8009_Conf_Device = ((drv_I2C_Ext_API_t *) pDrvIf_Spec->pCommInterface->pAPI_STD->Ext)->Get_DeviceByAddr(pDrvIf_Spec->pCommInterface, CONF_DRV_OTM8009_I2C_ADDR, true);
#endif
#ifdef CONF_DRV_OTM8009_USE_SPI
		// add call to autoconfigure device 
		res = ((drv_SPI_Ext_API_t *) pDrvIf_Spec->pCommInterface->pAPI_STD->Ext)->Create_Device(pDrvIf_Spec->pCommInterface, &OTM8009_Conf_Device, OTM8009_Conf_Device.Dev_SSEL_ID); // create dev.
#endif
		
	}

#ifdef CONF_DRV_OTM8009_USE_I2C
	if(OTM8009_I2C_Device)
	{
		// set required parameters:
		OTM8009_I2C_Device->Dev_Parms.RegAdrSize = 1;
		OTM8009_I2C_Device->Dev_Parms.SpeedMode = I2C_SPEED_MODE_STANDARD; 			//I2C_SPEED_MODE_FAST;
		OTM8009_I2C_Device->Dev_Parms.XferMode = CHAL_I2C_Slave;
		OTM8009_I2C_Device->pBuff = sys_Buffer_Create(CONF_APP_II2C_RXBUFF_CAPACITY, CONF_APP_II2C_RXBUFF_ELEMENT_SIZE, sBuffRing);
		
		// init Chip via I2C :::::::::::::::::::::::::::::::::::::::::::::::::::
		((drv_I2C_If_Spec_t*) pDrvIf_Spec->pCommInterface->pDrvSpecific)->pXFer_Dev = OTM8009_I2C_Device;								// set transfer device
		sys_Buffer_Write_Element( OTM8009_I2C_Device->pBuff, (uint8_t[]) {0x00, 0x00} , 2);
		res = drvmgr_Write_Data( pRequestorModIf, pDrvIf_Spec->pCommInterface, OTM8009_I2C_Device->pBuff, 2, XFer_Blocking);
		
		pDrvIf->Sys.Stat.Initialized = res;											// set status
	}
#endif	
	
	
#ifdef CONF_DRV_OTM8009_USE_SPI
	((drv_SPI_If_Spec_t*) (pDrvIf_Spec->pCommInterface)->pDrvSpecific)->pXFer_Dev = (SPI_Device_t*) &OTM8009_Conf_Device;		// Assign XFer Device to tranxfer
	((drv_SPI_If_Spec_t*) (pDrvIf_Spec->pCommInterface)->pDrvSpecific)->pPeri->CFG &= ~(SPI_CFG_SPOL0_MASK | SPI_CFG_SPOL1_MASK | SPI_CFG_SPOL2_MASK | SPI_CFG_SPOL3_MASK);
	
/*	
	[15]	[14]	[13]	[12]	[11]	[10]	[9]		[8]		[7 .... 0]		- bit order
	[R/!W]	[DCX]	[H/!L] 	[x] 	[x]		[x]		[x]		[x]		[ DATA ]		- bit means
	R/W 1: Read, 0: Write
	DCX 1: Data, 0: Command
	H/L 1: High bytes (15:8), : Low bytes (7:0)
	x 	 : doesn't matter
	DATA : Data value. Write order: RED, GREEN, BLUE. 
*/	
	
	res = true;
	if(res) pDrvIf->Sys.Stat.Initialized = true;									// set status
#endif	


// Write init sequence:

	// SRC:    https://github.com/ZinggJM/GxTFT/blob/master/src/GxCTRL/GxCTRL_OTM8009A/GxCTRL_OTM8009A.cpp
	//============ OTM8009A+HSD3.97 20140613 ===============================================//
  OTM8009_Write_Reg( pDrvIf, 0xff00, (uint8_t[]) {0x80}, 1);    //enable access command2
  OTM8009_Write_Reg( pDrvIf, 0xff01, (uint8_t[]) {0x09}, 1);    //enable access command2
  OTM8009_Write_Reg( pDrvIf, 0xff02, (uint8_t[]) {0x01}, 1);    //enable access command2
  OTM8009_Write_Reg( pDrvIf, 0xff80, (uint8_t[]) {0x80}, 1);    //enable access command2
  OTM8009_Write_Reg( pDrvIf, 0xff81, (uint8_t[]) {0x09}, 1);    //enable access command2
  OTM8009_Write_Reg( pDrvIf, 0xff03, (uint8_t[]) {0x01}, 1);    //
  OTM8009_Write_Reg( pDrvIf, 0xc5b1, (uint8_t[]) {0xA9}, 1);    //power control
  OTM8009_Write_Reg( pDrvIf, 0xc591, (uint8_t[]) {0x0F}, 1);               //power control
  OTM8009_Write_Reg( pDrvIf, 0xc0B4, (uint8_t[]) {0x50}, 1);
  //panel driving mode : column inversion
  //////  gamma
  OTM8009_Write_Reg( pDrvIf, 0xE100, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE101, (uint8_t[]) {0x09}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE102, (uint8_t[]) {0x0F}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE103, (uint8_t[]) {0x0E}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE104, (uint8_t[]) {0x07}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE105, (uint8_t[]) {0x10}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE106, (uint8_t[]) {0x0B}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE107, (uint8_t[]) {0x0A}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE108, (uint8_t[]) {0x04}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE109, (uint8_t[]) {0x07}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE10A, (uint8_t[]) {0x0B}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE10B, (uint8_t[]) {0x08}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE10C, (uint8_t[]) {0x0F}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE10D, (uint8_t[]) {0x10}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE10E, (uint8_t[]) {0x0A}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE10F, (uint8_t[]) {0x01}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE200, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE201, (uint8_t[]) {0x09}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE202, (uint8_t[]) {0x0F}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE203, (uint8_t[]) {0x0E}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE204, (uint8_t[]) {0x07}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE205, (uint8_t[]) {0x10}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE206, (uint8_t[]) {0x0B}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE207, (uint8_t[]) {0x0A}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE208, (uint8_t[]) {0x04}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE209, (uint8_t[]) {0x07}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE20A, (uint8_t[]) {0x0B}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE20B, (uint8_t[]) {0x08}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE20C, (uint8_t[]) {0x0F}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE20D, (uint8_t[]) {0x10}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE20E, (uint8_t[]) {0x0A}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xE20F, (uint8_t[]) {0x01}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xD900, (uint8_t[]) {0x4E}, 1);    //vcom setting
  OTM8009_Write_Reg( pDrvIf, 0xc181, (uint8_t[]) {0x66}, 1);    //osc=65HZ
  OTM8009_Write_Reg( pDrvIf, 0xc1a1, (uint8_t[]) {0x08}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xc592, (uint8_t[]) {0x01}, 1);    //power control
  OTM8009_Write_Reg( pDrvIf, 0xc595, (uint8_t[]) {0x34}, 1);    //power control
  OTM8009_Write_Reg( pDrvIf, 0xd800, (uint8_t[]) {0x79}, 1);    //GVDD / NGVDD setting
  OTM8009_Write_Reg( pDrvIf, 0xd801, (uint8_t[]) {0x79}, 1);    //GVDD / NGVDD setting
  OTM8009_Write_Reg( pDrvIf, 0xc594, (uint8_t[]) {0x33}, 1);    //power control
  OTM8009_Write_Reg( pDrvIf, 0xc0a3, (uint8_t[]) {0x1B}, 1);    //panel timing setting
  OTM8009_Write_Reg( pDrvIf, 0xc582, (uint8_t[]) {0x83}, 1);    //power control
  OTM8009_Write_Reg( pDrvIf, 0xc481, (uint8_t[]) {0x83}, 1);    //source driver setting
  OTM8009_Write_Reg( pDrvIf, 0xc1a1, (uint8_t[]) {0x0E}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xb3a6, (uint8_t[]) {0x20}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xb3a7, (uint8_t[]) {0x01}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xce80, (uint8_t[]) {0x85}, 1);    // GOA VST
  OTM8009_Write_Reg( pDrvIf, 0xce81, (uint8_t[]) {0x01}, 1);  	// GOA VST
  OTM8009_Write_Reg( pDrvIf, 0xce82, (uint8_t[]) {0x00}, 1);    // GOA VST
  OTM8009_Write_Reg( pDrvIf, 0xce83, (uint8_t[]) {0x84}, 1);    // GOA VST
  OTM8009_Write_Reg( pDrvIf, 0xce84, (uint8_t[]) {0x01}, 1);    // GOA VST
  OTM8009_Write_Reg( pDrvIf, 0xce85, (uint8_t[]) {0x00}, 1);    // GOA VST
  OTM8009_Write_Reg( pDrvIf, 0xcea0, (uint8_t[]) {0x18}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcea1, (uint8_t[]) {0x04}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcea2, (uint8_t[]) {0x03}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcea3, (uint8_t[]) {0x39}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcea4, (uint8_t[]) {0x00}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcea5, (uint8_t[]) {0x00}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcea6, (uint8_t[]) {0x00}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcea7, (uint8_t[]) {0x18}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcea8, (uint8_t[]) {0x03}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcea9, (uint8_t[]) {0x03}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceaa, (uint8_t[]) {0x3a}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceab, (uint8_t[]) {0x00}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceac, (uint8_t[]) {0x00}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcead, (uint8_t[]) {0x00}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceb0, (uint8_t[]) {0x18}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceb1, (uint8_t[]) {0x02}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceb2, (uint8_t[]) {0x03}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceb3, (uint8_t[]) {0x3b}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceb4, (uint8_t[]) {0x00}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceb5, (uint8_t[]) {0x00}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceb6, (uint8_t[]) {0x00}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceb7, (uint8_t[]) {0x18}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceb8, (uint8_t[]) {0x01}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceb9, (uint8_t[]) {0x03}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xceba, (uint8_t[]) {0x3c}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcebb, (uint8_t[]) {0x00}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcebc, (uint8_t[]) {0x00}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcebd, (uint8_t[]) {0x00}, 1);    // GOA CLK
  OTM8009_Write_Reg( pDrvIf, 0xcfc0, (uint8_t[]) {0x01}, 1);    // GOA ECLK
  OTM8009_Write_Reg( pDrvIf, 0xcfc1, (uint8_t[]) {0x01}, 1);    // GOA ECLK
  OTM8009_Write_Reg( pDrvIf, 0xcfc2, (uint8_t[]) {0x20}, 1);    // GOA ECLK
  OTM8009_Write_Reg( pDrvIf, 0xcfc3, (uint8_t[]) {0x20}, 1);    // GOA ECLK
  OTM8009_Write_Reg( pDrvIf, 0xcfc4, (uint8_t[]) {0x00}, 1);    // GOA ECLK
  OTM8009_Write_Reg( pDrvIf, 0xcfc5, (uint8_t[]) {0x00}, 1);    // GOA ECLK
  OTM8009_Write_Reg( pDrvIf, 0xcfc6, (uint8_t[]) {0x01}, 1);    // GOA other options
  OTM8009_Write_Reg( pDrvIf, 0xcfc7, (uint8_t[]) {0x00}, 1);
  // GOA signal toggle option setting
  OTM8009_Write_Reg( pDrvIf, 0xcfc8, (uint8_t[]) {0x00}, 1);    //GOA signal toggle option setting
  OTM8009_Write_Reg( pDrvIf, 0xcfc9, (uint8_t[]) {0x00}, 1);
  //GOA signal toggle option setting
  OTM8009_Write_Reg( pDrvIf, 0xcfd0, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb80, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb81, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb82, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb83, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb84, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb85, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb86, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb87, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb88, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb89, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb90, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb91, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb92, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb93, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb94, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb95, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb96, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb97, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb98, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb99, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb9a, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb9b, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb9c, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb9d, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcb9e, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcba0, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcba1, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcba2, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcba3, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcba4, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcba5, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcba6, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcba7, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcba8, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcba9, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbaa, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbab, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbac, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbad, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbae, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbb0, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbb1, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbb2, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbb3, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbb4, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbb5, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbb6, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbb7, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbb8, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbb9, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbc0, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbc1, (uint8_t[]) {0x04}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbc2, (uint8_t[]) {0x04}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbc3, (uint8_t[]) {0x04}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbc4, (uint8_t[]) {0x04}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbc5, (uint8_t[]) {0x04}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbc6, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbc7, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbc8, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbc9, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbca, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbcb, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbcc, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbcd, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbce, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbd0, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbd1, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbd2, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbd3, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbd4, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbd5, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbd6, (uint8_t[]) {0x04}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbd7, (uint8_t[]) {0x04}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbd8, (uint8_t[]) {0x04}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbd9, (uint8_t[]) {0x04}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbda, (uint8_t[]) {0x04}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbdb, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbdc, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbdd, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbde, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbe0, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbe1, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbe2, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbe3, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbe4, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbe5, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbe6, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbe7, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbe8, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbe9, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbf0, (uint8_t[]) {0xFF}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbf1, (uint8_t[]) {0xFF}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbf2, (uint8_t[]) {0xFF}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbf3, (uint8_t[]) {0xFF}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbf4, (uint8_t[]) {0xFF}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbf5, (uint8_t[]) {0xFF}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbf6, (uint8_t[]) {0xFF}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbf7, (uint8_t[]) {0xFF}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbf8, (uint8_t[]) {0xFF}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcbf9, (uint8_t[]) {0xFF}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc80, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc81, (uint8_t[]) {0x26}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc82, (uint8_t[]) {0x09}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc83, (uint8_t[]) {0x0B}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc84, (uint8_t[]) {0x01}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc85, (uint8_t[]) {0x25}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc86, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc87, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc88, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc89, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc90, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc91, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc92, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc93, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc94, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc95, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc96, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc97, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc98, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc99, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc9a, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc9b, (uint8_t[]) {0x26}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc9c, (uint8_t[]) {0x0A}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc9d, (uint8_t[]) {0x0C}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcc9e, (uint8_t[]) {0x02}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcca0, (uint8_t[]) {0x25}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcca1, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcca2, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcca3, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcca4, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcca5, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcca6, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcca7, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcca8, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcca9, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccaa, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccab, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccac, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccad, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccae, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccb0, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccb1, (uint8_t[]) {0x25}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccb2, (uint8_t[]) {0x0C}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccb3, (uint8_t[]) {0x0A}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccb4, (uint8_t[]) {0x02}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccb5, (uint8_t[]) {0x26}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccb6, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccb7, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccb8, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccb9, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccc0, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccc1, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccc2, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccc3, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccc4, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccc5, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccc6, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccc7, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccc8, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccc9, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccca, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcccb, (uint8_t[]) {0x25}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcccc, (uint8_t[]) {0x0B}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xcccd, (uint8_t[]) {0x09}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccce, (uint8_t[]) {0x01}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccd0, (uint8_t[]) {0x26}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccd1, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccd2, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccd3, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccd4, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccd5, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccd6, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccd7, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccd8, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccd9, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccda, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccdb, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccdc, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccdd, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0xccde, (uint8_t[]) {0x00}, 1);
  OTM8009_Write_Reg( pDrvIf, 0x3A00, (uint8_t[]) {0x55}, 1);
  OTM8009_Write_Reg( pDrvIf, 0x1100, NULL, 0);
  Delay_ms(100);
  OTM8009_Write_Reg( pDrvIf, 0x2900, NULL, 0);
  Delay_ms(50);

	
	
	
	
	
	
	
	
	
	
	
#if defined(CONF_DEBUG_DRV_OTM8009) && (CONF_DEBUG_DRV_OTM8009 == 1)
	dbgprint("\r\n("DRV_OTM8009_OBJECTNAME") ID[%s]: Init Done.", pDrvIf->Name);
#endif		
	return(res);
}


// ------------------------------------------------------------------------------------------------------
// USR
// ENABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool OTM8009_Enable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	//drv_OTM8009_If_Spec_t* pDrvIf_Spec = (drv_OTM8009_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_OTM8009) && (CONF_DEBUG_DRV_OTM8009 == 1)
	dbgprint("\r\n("DRV_OTM8009_OBJECTNAME") ID[%s]: Set Enable CH:0x%08x", pDrvIf->Name, ChannelBitMask);
#endif

	// main code here:
	pDrvIf->Sys.Stat.Enabled = true;												// enable was succesfull
	res = true;
#if defined(CONF_DEBUG_DRV_OTM8009) && (CONF_DEBUG_DRV_OTM8009 == 1)
	dbgprint("\r\n("DRV_OTM8009_OBJECTNAME") ID[%s]: Set Enable Done.", pDrvIf->Name);
#endif	
	return(res);
}

// ------------------------------------------------------------------------------------------------------
// USR
// DISABLE funkcie zariadenia.
// ak je ChannelBitMask == UINT32_MAX, potom enable/disable vsetky kanale
bool OTM8009_Disable( _mod_If_t *pRequestorModIf, _drv_If_t *pDrvIf, uint32_t ChannelBitMask)
{
	bool res = false;
	
	//drv_OTM8009_If_Spec_t* pDrvIf_Spec = (drv_OTM8009_If_Spec_t*) pDrvIf->pDrvSpecific;	// pointer na specific struct periferie

#if defined(CONF_DEBUG_DRV_OTM8009) && (CONF_DEBUG_DRV_OTM8009 == 1)
	dbgprint("\r\n("DRV_OTM8009_OBJECTNAME") ID[%s]: Set Disable CH:0x%08x", pDrvIf->Name, ChannelBitMask);
#endif

	// main code here:
	pDrvIf->Sys.Stat.Enabled = false;												// disable was succesfull
	res = true;
	
#if defined(CONF_DEBUG_DRV_OTM8009) && (CONF_DEBUG_DRV_OTM8009 == 1)
	dbgprint("\r\n("DRV_OTM8009_OBJECTNAME") ID[%s]: Set Disable Done.", pDrvIf->Name);
#endif	
	return(res);
}


// ******************************************************************************************************
// INTERRUPT HANDLERS
// ******************************************************************************************************

// no interrupts in this driver

// ************************************************************************************************
// EVENTS Functions
// ************************************************************************************************

// no interrupts in this driver


// ------------------------------------------------------------------------------------------------
// Event system - volane v preruseni !!!
inline void OTM8009_Events( struct _drv_If *pDrvIf, _EventType_t EventType) 			// routine  for event system 
{
//	switch(EventType)
//	{
//		case EV_DataReceived:
//		case EV_TransmitReady:
//		case EV_TransmitDone:	
//		case EV_NoChange:
//		default: break;
//	}
}

// ------------------------------------------------------------------------------------------------------
// EXT


#endif	// USE_DRV_OTM8009
