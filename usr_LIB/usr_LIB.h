// ******************************************************************************
// Skeleton App System v3 - for Cortex M0/M3/M4 - (https://gitlab.com/EdizonTN/Skeleton)
//    Filename: usr_LIB.h
// 	   Version: 3.0
//      Author: EdizonTN
// Licenced under MIT License. More you can find at LICENSE file 
// ******************************************************************************
// Info: User's private Libraries include
//
// Notice:
//
// Usage:
//			
// ToDo:
// 			
// Changelog:
// 


#ifndef __USR_LIB_H_
#define __USR_LIB_H_


// ******************************************************************************************************
// USER'S PRIVATE LIBRARIES		- Load enabled libraries. Libraries are enabled in App_Config.h.
// ******************************************************************************************************
#if defined(CONF_USE_LIB_MYLIB_1) && (CONF_USE_LIB_MYLIB_1 == 1)
	#include ".\usr_LIB\lib_MyLib_1.h"												// Load custom library
#endif

#endif	// __USR_LIB_H_
